package initial3d;

import java.util.Arrays;

import initial3d.detail.ArrayMath;
import initial3d.detail.StringUtil;

public class Mat2f {

	public static final Mat2f zero = new Mat2f();
	public static final Mat2f eye = new Mat2f(1);
	
	// column-major
	private final float[] e;

	private static int index(int r, int c) {
		return 2 * c + r;
	}
	
	private static int arraySize() {
		return 4;
	}
	
	public Mat2f() {
		e = new float[arraySize()];
	}

	public Mat2f(float x) {
		e = new float[] { x, 0, 0, x };
	}
	
	public Mat2f(float e00_, float e01_, float e10_, float e11_) {
		e = new float[] { e00_, e01_, e10_, e11_ };
	}

	public float get(int r, int c) {
		return e[index(r, c)];
	}

	public Mat2f with(int r, int c, float val) {
		Mat2f m = new Mat2f();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(r, c)] = val;
		return m;
	}

	public Vec2f col(int c) {
		return new Vec2f(get(0, c), get(1, c));
	}

	public Vec2f row(int r) {
		return new Vec2f(get(r, 0), get(r, 1));
	}

	public Mat2f withCol(int c, Vec2f v) {
		Mat2f m = new Mat2f();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(0, c)] = v.x;
		m.e[index(1, c)] = v.y;
		return m;
	}

	public Mat2f withRow(int r, Vec2f v) {
		Mat2f m = new Mat2f();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(r, 0)] = v.x;
		m.e[index(r, 1)] = v.y;
		return m;
	}

	public Mat2f add(Mat2f rhs) {
		Mat2f m = new Mat2f();
		ArrayMath.add(arraySize(), m.e, 0, e, 0, rhs.e, 0);
		return m;
	}

	public Mat2f add(float rhs) {
		Mat2f m = new Mat2f();
		ArrayMath.add(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat2f sub(Mat2f rhs) {
		Mat2f m = new Mat2f();
		ArrayMath.sub(arraySize(), m.e, 0, e, 0, rhs.e, 0);
		return m;
	}

	public Mat2f sub(float rhs) {
		Mat2f m = new Mat2f();
		ArrayMath.sub(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat2f mul(float rhs) {
		Mat2f m = new Mat2f();
		ArrayMath.mul(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat2f div(float rhs) {
		Mat2f m = new Mat2f();
		ArrayMath.div(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat2f mul(Mat2f rhs) {
		Mat2f m = new Mat2f();
		float r;
		float t0;
		float t1;
		r = rhs.get(0, 0);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		r = rhs.get(1, 0);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		m.e[index(0, 0)] = t0;
		m.e[index(1, 0)] = t1;
		r = rhs.get(0, 1);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		r = rhs.get(1, 1);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		m.e[index(0, 1)] = t0;
		m.e[index(1, 1)] = t1;
		return m;
	}

	public Vec2f mul(Vec2f rhs) {
		float r;
		float t0;
		float t1;
		r = rhs.x;
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		r = rhs.y;
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		return new Vec2f(t0, t1);
	}
	
	@Override
	public String toString() {
		return StringUtil.matrixToString(e, 2);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(e);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Mat2f other = (Mat2f) obj;
		if (!Arrays.equals(e, other.e)) return false;
		return true;
	}
	
}
