package initial3d.detail;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class IndexAllocator {

	private AtomicInteger next = new AtomicInteger();
	private BlockingQueue<Integer> reuse = new LinkedBlockingQueue<>();
	
	public IndexAllocator(int first) {
		next.set(first);
	}
	
	public int alloc() {
		Integer i = reuse.poll();
		if (i == null) return next.getAndIncrement();
		return i;
	}
	
	public void free(int i) {
		reuse.offer(i);
	}
	
}
