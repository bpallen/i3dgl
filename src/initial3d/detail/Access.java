package initial3d.detail;

public enum Access {
	READ, SHARED_WRITE, UNIQUE_WRITE;
}
