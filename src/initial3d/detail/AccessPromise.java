package initial3d.detail;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 * 
 * @author ben
 *
 */
public class AccessPromise {

	private final AccessArbitrator arbitrator;
	private final Access access;
	private final AtomicInteger satisfaction_required = new AtomicInteger(1);
	private volatile boolean granted = false;
	private boolean waiting = false;
	
	public AccessPromise(AccessArbitrator arbitrator_, Access access_) {
		arbitrator = Objects.requireNonNull(arbitrator_);
		access = Objects.requireNonNull(access_);
	}
	
	public Access access() {
		return access;
	}
	
	public boolean granted() {
		return granted;
	}
	
	public boolean satisfied() {
		return satisfaction_required.get() == 0;
	}
	
	/**
	 * On failure, a new promise must be created.
	 * 
	 * @return true iff successful
	 */
	public boolean requireMoreSatisfaction(int limit) {
		int sr = 0;
		do {
			sr = satisfaction_required.get();
			// if already satisfied completely, nothing can be done
			if (sr <= 0) return false;
			// if more satisfaction would be required than allowed, nothing can be done
			if (sr >= limit) return false;
		} while (!satisfaction_required.compareAndSet(sr, sr + 1));
		return true;
	}
	
	/**
	 * @return True if promise is now already satisfied
	 */
	public synchronized boolean grant() {
		granted = true;
		if (waiting) notifyAll();
		return satisfied();
	}
	
	public synchronized void waitGranted() {
		arbitrator.checkAcquire(this);
		waiting = true;
		while (!granted) {
			try {
				wait();
			} catch (InterruptedException e) {
				// no.
			}
		}
	}
	
	public void onAcquire(AccessFuture f) {
		arbitrator.onAcquire(this);
	}
	
	public void onRelease(AccessFuture f) {
		arbitrator.onRelease(this);
	}
	
	public void onSatisfied(AccessFuture f) {
		if (satisfaction_required.decrementAndGet() == 0) {
			arbitrator.onSatisfied(this);
		}
	}
}
