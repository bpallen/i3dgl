package initial3d.detail;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * TODO remove this class
 * 
 * @author ben
 *
 */
public class SemaphoreLock implements Lock {
	
	private final Semaphore sem;
	private int count;
	
	public SemaphoreLock(Semaphore sem_, int count_) {
		sem = sem_;
		count = count_;
	}
	
	@Override
	public void lock() {
		sem.acquireUninterruptibly(count);
	}

	@Override
	public void lockInterruptibly() throws InterruptedException {
		sem.acquire(count);
	}

	@Override
	public Condition newCondition() {
		return null;
	}

	@Override
	public boolean tryLock() {
		if (sem.tryAcquire(count)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean tryLock(long arg0, TimeUnit arg1) throws InterruptedException {
		if (sem.tryAcquire(count, arg0, arg1)) {
			return true;
		}
		return false;
	}

	@Override
	public void unlock() {
		sem.release(count);
	}
	
}
