package initial3d.detail;

import java.util.Objects;

public abstract class AbstractAccessible implements Accessible {

	protected final AccessArbitrator arbitrator;
	
	protected AbstractAccessible(AccessArbitrator arbitrator_) {
		arbitrator = Objects.requireNonNull(arbitrator_);
	}
	
	@Override
	public void arbitrate() {
		arbitrator.arbitrate();
	}
	
	@Override
	public AccessFuture requestAccess(Access a, boolean arbitrate_now) {
		AccessFuture f = new AccessFuture(arbitrator, a);
		f.request(arbitrate_now);
		return f;
	}
	
	@Override
	public AccessFuture requestAccessDeferred(Access a) {
		return new AccessFuture(arbitrator, a);
	}
	
}
