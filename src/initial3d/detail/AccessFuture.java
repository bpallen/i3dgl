package initial3d.detail;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicInteger;

public class AccessFuture {

	private static final int STATE_PRE_REQUIRE = 0;
	private static final int STATE_PRE_ACQUIRE = 1;
	private static final int STATE_ACQUIRED = 2;
	private static final int STATE_SATISFIED = 3;
	
	private final AccessArbitrator arbitrator;
	private final Access access;
	private final AtomicInteger state = new AtomicInteger(STATE_PRE_REQUIRE);
	private final AtomicInteger acquire_count = new AtomicInteger(0);
	private volatile WeakReference<AccessPromise> promise = new WeakReference<>(null);
	
	public AccessFuture(AccessArbitrator arbitrator_, Access access_) {
		arbitrator = arbitrator_;
		access = access_;
	}
	
	public Access access() {
		return access;
	}
	
	/**
	 * Request access from the arbitrator now.
	 * Must complete before any attempted aquisisiton or cancellation.
	 * 
	 * @param arbitrate_now
	 */
	public void request(boolean arbitrate_now) {
		if (!state.compareAndSet(STATE_PRE_REQUIRE, STATE_PRE_ACQUIRE)) {
			throw new IllegalStateException("future not requirable");
		}
		promise = new WeakReference<>(arbitrator.requestAccess(access, arbitrate_now));
	}
	
	public void request() {
		request(true);
	}
	
	public boolean satisfied() {
		return state.get() == STATE_SATISFIED;
	}
	
	private void acquireImpl(AccessPromise p) {
		if (satisfied()) throw new SatisfiedPromiseException();
		if (acquire_count.getAndIncrement() == 0) {
			if (!state.compareAndSet(STATE_PRE_ACQUIRE, STATE_ACQUIRED)) {
				throw new IllegalStateException("future not acquirable");
			}
		}
		p.onAcquire(this);
	}
	
	public boolean tryAcquire() {
		AccessPromise p = promise.get();
		if (p == null) throw new BrokenPromiseException();
		// checkAcquire is not needed here because we never block
		if (!p.granted()) return false;
		acquireImpl(p);
		return true;
	}
	
	public void acquire() {
		AccessPromise p = promise.get();
		if (p == null) throw new BrokenPromiseException();
		p.waitGranted();
		acquireImpl(p);
	}
	
	public void release() {
		AccessPromise p = promise.get();
		if (p == null) throw new BrokenPromiseException();
		int c = acquire_count.decrementAndGet();
		if (c < 0) throw new IllegalStateException("unbalanced release");
		if (p != null) p.onRelease(this);
		if (c == 0) {
			if (!state.compareAndSet(STATE_ACQUIRED, STATE_SATISFIED)) {
				throw new IllegalStateException("future not acquired");
			}
			if (p != null) p.onSatisfied(this);
		}
	}
	
	public void cancel() {
		if (satisfied()) return;
		AccessPromise p = promise.get();
		if (p == null) throw new BrokenPromiseException();
		if (!state.compareAndSet(STATE_PRE_ACQUIRE, STATE_SATISFIED)) {
			throw new IllegalStateException("future not cancellable");
		}
		p.onSatisfied(this);
	}
	
}
