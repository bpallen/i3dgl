package initial3d.detail;

public class ArrayMath {

	private ArrayMath() {
		throw new AssertionError("Not constructible");
	}

	public static void add(int count, double[] dest, int di, double[] lhs, int li, double[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] + rhs[ri + i];
		}
	}

	public static void add(int count, double[] dest, int di, double[] lhs, int li, double rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] + rhs;
		}
	}

	public static void sub(int count, double[] dest, int di, double[] lhs, int li, double[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] - rhs[ri + i];
		}
	}

	public static void sub(int count, double[] dest, int di, double[] lhs, int li, double rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] - rhs;
		}
	}

	public static void sub(int count, double[] dest, int di, double lhs, double[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs - rhs[ri + i];
		}
	}

	public static void mul(int count, double[] dest, int di, double[] lhs, int li, double[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] * rhs[ri + i];
		}
	}

	public static void mul(int count, double[] dest, int di, double[] lhs, int li, double rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] * rhs;
		}
	}

	public static void div(int count, double[] dest, int di, double[] lhs, int li, double[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] / rhs[ri + i];
		}
	}

	public static void div(int count, double[] dest, int di, double[] lhs, int li, double rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] / rhs;
		}
	}

	public static void div(int count, double[] dest, int di, double lhs, double[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs / rhs[ri + i];
		}
	}

	public static void add(int count, float[] dest, int di, float[] lhs, int li, float[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] + rhs[ri + i];
		}
	}

	public static void add(int count, float[] dest, int di, float[] lhs, int li, float rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] + rhs;
		}
	}

	public static void sub(int count, float[] dest, int di, float[] lhs, int li, float[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] - rhs[ri + i];
		}
	}

	public static void sub(int count, float[] dest, int di, float[] lhs, int li, float rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] - rhs;
		}
	}

	public static void sub(int count, float[] dest, int di, float lhs, float[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs - rhs[ri + i];
		}
	}

	public static void mul(int count, float[] dest, int di, float[] lhs, int li, float[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] * rhs[ri + i];
		}
	}

	public static void mul(int count, float[] dest, int di, float[] lhs, int li, float rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] * rhs;
		}
	}

	public static void div(int count, float[] dest, int di, float[] lhs, int li, float[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] / rhs[ri + i];
		}
	}

	public static void div(int count, float[] dest, int di, float[] lhs, int li, float rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] / rhs;
		}
	}

	public static void div(int count, float[] dest, int di, float lhs, float[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs / rhs[ri + i];
		}
	}

	public static void add(int count, int[] dest, int di, int[] lhs, int li, int[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] + rhs[ri + i];
		}
	}

	public static void add(int count, int[] dest, int di, int[] lhs, int li, int rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] + rhs;
		}
	}

	public static void sub(int count, int[] dest, int di, int[] lhs, int li, int[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] - rhs[ri + i];
		}
	}

	public static void sub(int count, int[] dest, int di, int[] lhs, int li, int rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] - rhs;
		}
	}

	public static void sub(int count, int[] dest, int di, int lhs, int[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs - rhs[ri + i];
		}
	}

	public static void mul(int count, int[] dest, int di, int[] lhs, int li, int[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] * rhs[ri + i];
		}
	}

	public static void mul(int count, int[] dest, int di, int[] lhs, int li, int rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] * rhs;
		}
	}

	public static void div(int count, int[] dest, int di, int[] lhs, int li, int[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] / rhs[ri + i];
		}
	}

	public static void div(int count, int[] dest, int di, int[] lhs, int li, int rhs) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs[li + i] / rhs;
		}
	}

	public static void div(int count, int[] dest, int di, int lhs, int[] rhs, int ri) {
		for (int i = 0; i < count; i++) {
			dest[di + i] = lhs / rhs[ri + i];
		}
	}

}
