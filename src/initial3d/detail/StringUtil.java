package initial3d.detail;

public class StringUtil {

	public static String formatDouble(double f, int prec) {
		// format %f/%g always emits specified number of digits, remove unnecessary trailing zeros
		return String.format("%." + prec + "g", f).replaceAll("(?<=\\.)((?:[0-9]*[1-9])|0)0+(?![0-9])", "$1");
	}

	public static String formatFloat(float f, int prec) {
		// format %f/%g always emits specified number of digits, remove unnecessary trailing zeros
		return String.format("%." + prec + "g", f).replaceAll("(?<=\\.)((?:[0-9]*[1-9])|0)0+(?![0-9])", "$1");
	}

	public static String matrixToString(double[] data, int cols) {
		final int prec = 5;
		int[] colwidths = new int[cols];
		// find required column widths
		for (int i = 0; i < cols; i++) {
			for (int j = i; j < data.length; j += cols) {
				colwidths[i] = Math.max(colwidths[i], formatDouble(data[j], prec).length());
			}
		}
		// stringify rows
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < data.length; i += cols) {
			sb.append(i == 0 ? "/ " : i >= data.length - cols ? "\\ " : "| ");
			for (int j = 0; j < cols; j++) {
				String x = formatDouble(data[i + j], prec);
				for (int k = 0; k < colwidths[j] - x.length(); k++) {
					sb.append(" ");
				}
				sb.append(x);
				if (j < cols - 1) {
					sb.append(", ");
				}
			}
			sb.append(i == 0 ? " \\" : i >= data.length - cols ? " /" : " |");
			if (i < data.length - cols) {
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	public static String matrixToString(float[] data, int cols) {
		final int prec = 5;
		int[] colwidths = new int[cols];
		// find required column widths
		for (int i = 0; i < cols; i++) {
			for (int j = i; j < data.length; j += cols) {
				colwidths[i] = Math.max(colwidths[i], formatFloat(data[j], prec).length());
			}
		}
		// stringify rows
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < data.length; i += cols) {
			sb.append(i == 0 ? "/ " : i >= data.length - cols ? "\\ " : "| ");
			for (int j = 0; j < cols; j++) {
				String x = formatFloat(data[j + i], prec);
				for (int k = 0; k < colwidths[j] - x.length(); k++) {
					sb.append(" ");
				}
				sb.append(x);
				if (j < cols - 1) {
					sb.append(", ");
				}
			}
			sb.append(i == 0 ? " \\" : i >= data.length - cols ? " /" : " |");
			if (i < data.length - cols) {
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	public static String randomID(int size) {
		String charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++) {
			sb.append(charset.charAt((int) (Math.random() * charset.length())));
		}
		return sb.toString();
	}
	
}
