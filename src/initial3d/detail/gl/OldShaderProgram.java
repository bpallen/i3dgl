package initial3d.detail.gl;

import initial3d.detail.Util;
import sun.misc.Unsafe;

public abstract class OldShaderProgram {
	
	protected static final Unsafe unsafe = Util.unsafe;
	
	/**
	 * Size of the vertex output struct expected by the vertex shader. The first 16 bytes are used for gl_Position.
	 * Any data after those 16 bytes will be interpolated as floats and fed to the fragment shader.
	 * i.e. pinterp_in == pvert_out + 16.
	 */
	public abstract int vertexOutSize();
	
	/**
	 * The first 16 bytes of pvert_in are reserved, and hold the vertex id and instance id (int x 2).
	 * Fetched, decoded vertex attributes appear in subsequent 16-byte locations regardless of the number of
	 * components in that attribute.
	 * 
	 * @param pvert_in
	 * @param pvert_out
	 */
	public abstract void vertexShade(long pvert_in, long pvert_out);
	
	public abstract void fragmentShade(long pinterp_in, float z, float w, long pfrag_out);
	
}














