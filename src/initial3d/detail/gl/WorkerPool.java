package initial3d.detail.gl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

public class WorkerPool {
	
	public static final int SCRATCH_STORAGE_SIZE = 1024 * 1024;
	
	private static class TicketEntry {
		private Command command;
		private Runnable task;
		
		public TicketEntry(Command c, Runnable r) {
			command = c;
			task = r;
			command.usageInc();
		}
		
		public void destroy() {
			command.usageDec();
		}
	}
	
	public static class Ticket {
		private boolean invocable = false;
		private boolean invoke_all = false;
		private final ArrayList<TicketEntry> entries = new ArrayList<>();
		
		private Ticket() { }
	}
	
	// associate a command with a task so that worker threads can lock the data stores used by a command
	private static class CommandTask implements Runnable {
		
		private Command command;
		private Runnable task;
		
		public CommandTask(Command command_, Runnable task_) {
			command = command_;
			task = task_;
			command.usageInc();
		}
		
		@Override
		public void run() {
			try {
				task.run();
			} finally {
				command.usageDec();
			}
		}
		
	}
	
	private static class WorkerThread extends Thread {
		
		private final int index;
		private final Semaphore something_interesting = new Semaphore(0);
		private final BlockingQueue<Runnable> local_tasks = new LinkedBlockingQueue<>();
		
		public WorkerThread(int i) {
			index = i;
			setName("Initial3D GL worker " + i);
			setDaemon(true);
		}
		
		public void wake() {
			// TODO this might be a bottleneck
			something_interesting.release();
		}
		
		public void offer(Runnable t) {
			local_tasks.offer(t);
			wake();
		}
		
		@Override
		public void run() {
			// init threadlocal
			worker_id.set(index);
			worker_scratch.set(new DataStore(SCRATCH_STORAGE_SIZE));
			// do tasks
			while (true) {
				try {
					Runnable t = local_tasks.poll();
					if (t == null) t = global_tasks.poll();
					if (t == null) {
						something_interesting.acquire();
						something_interesting.drainPermits();
					} else {
						t.run();
					}
				} catch (Exception e) {
					System.err.println("Initial3D GL worker thread internal error:");
					e.printStackTrace();
				}
			}
		}
	}
	
	private static final ArrayList<WorkerThread> workers = new ArrayList<>();
	private static final BlockingQueue<Runnable> global_tasks = new LinkedBlockingQueue<>();
	private static final ThreadLocal<Integer> worker_id = ThreadLocal.withInitial(() -> { return -1; });
	private static final ThreadLocal<DataStore> worker_scratch = new ThreadLocal<>();
	private static final BlockingQueue<Ticket> tickets = new LinkedBlockingQueue<>();
	private static final Object ticket_sync = new Object();
	
	private WorkerPool() {
		throw new AssertionError("not constructible");
	}
	
	public static int numWorkers() {
		return workers.size();
	}
	
	/**
	 * @return ID of current worker thread or -1 if not a worker
	 */
	public static int id() {
		return worker_id.get();
	}
	
	/**
	 * Get scratch storage for current worker thread. Storage size is {@link #SCRATCH_STORAGE_SIZE}.
	 * 
	 * @return Storage or null if not a worker
	 */
	public static DataStore scratchStorage() {
		return worker_scratch.get();
	}
	
	public static void init(int num_threads) {
		if (workers.size() > 0) return;
		for (int i = 0; i < num_threads; i++) {
			WorkerThread w = new WorkerThread(i);
			w.start();
			workers.add(w);
		}
		
	}
	
	private static boolean invocable(Ticket tk) {
		return tk != null && tk.invocable;
	}
	
	private static void invokeTickets(Ticket tk) {
		synchronized (ticket_sync) {
			if (tickets.peek() != tk) return;
			while (invocable(tickets.peek())) {
				tk = tickets.poll();
				if (tk.invoke_all) {
					for (TicketEntry e : tk.entries) {
						invokeAll(e.command, e.task);
						e.destroy();
					}
				} else {
					for (TicketEntry e : tk.entries) {
						invoke(e.command, e.task);
						e.destroy();
					}
				}
			}
		}
	}
	
	public static void invoke(Command c, Runnable r) {
		global_tasks.offer(new CommandTask(c, r));
		for (WorkerThread w : workers) {
			w.wake();
		}
	}
	
	public static void invokeAll(Command c, Runnable r) {
		for (WorkerThread w : workers) {
			// need to construct a separate commandtask for each worker so that locking works
			w.offer(new CommandTask(c, r));
		}
	}
	
	/**
	 * Add a task to a ticket for later invocation.
	 * 
	 * @param tk
	 * @param c
	 * @param r
	 */
	public static void invokeLater(Ticket tk, Command c, Runnable r) {
		Objects.requireNonNull(tk);
		if (tk.invocable) throw new RuntimeException("ticket has already been invoked");
		tk.entries.add(new TicketEntry(c, r));
	}
	
	/**
	 * Begin ordered invocation of tasks queued to a ticket. 
	 * 
	 * @param tk
	 */
	public static void invoke(Ticket tk) {
		Objects.requireNonNull(tk);
		tk.invocable = true;
		invokeTickets(tk);
	}
	
	/**
	 * Create an ordered-invocation ticket.
	 * Actual invocation of ticketed tasks will be globally ordered in the same
	 * order as the creation of their tickets with this method.
	 * Can be used for multiple tasks with {@link #invokeLater}.
	 * Must be manually invoked with {@link invoke(Ticket)} when ready.
	 * 
	 * @return
	 */
	public static Ticket newTicket(boolean invoke_all) {
		Ticket tk = new Ticket();
		tk.invoke_all = invoke_all;
		tickets.offer(tk);
		return tk;
	}

}
