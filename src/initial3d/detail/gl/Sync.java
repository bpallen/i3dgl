package initial3d.detail.gl;

import initial3d.detail.Access;

/**
 * This is a special case of Resource in that it is (mostly) able to be used
 * safely from the driver thread for signalling purposes.
 * 
 * @author ben
 *
 */
public class Sync extends Resource {

	public Sync(I3DGL gl_, int name_) {
		super(gl_, name_);
	}

	private volatile boolean signaled = false;
	
	/**
	 * Can be called from any thread.
	 */
	public synchronized void signal() {
		signaled = true;
		notify();
	}

	/**
	 * Can be called from any thread.
	 * 
	 * @return
	 */
	public boolean signaled() {
		return signaled;
	}
	
	/**
	 * Wait for this Sync to be signaled. Timeout is specified in nanoseconds,
	 * but implementation granularity is only milliseconds.
	 * 
	 * Should only be called from the user thread.
	 * 
	 * @param timeout in nanos
	 */
	public synchronized boolean waitSignaled(long timeout_nanos) {
		gl().driver().flush();
		long now = System.currentTimeMillis();
		final long time0 = now;
		final long time1 = time0 + (timeout_nanos / 1000000L);
		while (now < time1) {
			if (signaled) return true;
			try {
				wait(time1 - now);
			} catch (InterruptedException e) {
				// oh no
			}
			now = System.currentTimeMillis();
		}
		return signaled;
	}

	@Override
	public void prepare(Command c, Access a) {
		
	}
	
}
