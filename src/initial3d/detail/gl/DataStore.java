package initial3d.detail.gl;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import initial3d.detail.Access;
import initial3d.detail.AccessQueue;
import initial3d.detail.AbstractAccessible;
import initial3d.detail.Util;

import static initial3d.GLEnum.*;

public class DataStore extends AbstractAccessible implements CommandPrereq {

	// pool size limits, must be power-of-two
	public static final int POOL_MIN_SIZE = 1024;
	public static final int POOL_MAX_SIZE = 1024 * 1024 * 128;
	
	// pool of power-of-two sized datastores
	@SuppressWarnings("rawtypes")
	private static final LinkedBlockingQueue[] pool;
	// number of pool allocations
	private static final AtomicInteger pool_alloc_count = new AtomicInteger(0);
	// total pool bytes allocated
	private static final AtomicInteger pool_alloc_bytes = new AtomicInteger(0);
	
	static {
		int bins = 32 - Integer.numberOfLeadingZeros(POOL_MAX_SIZE / POOL_MIN_SIZE);
		pool = new LinkedBlockingQueue[bins];
		for (int i = 0; i < bins; i++) {
			pool[i] = new LinkedBlockingQueue<DataStore>();
		}
	}
	
	// we use direct bytebuffers for their better cleanup behaviour (they don't use finalizers)
	private ByteBuffer data = null;
	// base address; aligned, may be greater than buffer's address
	private final long address;
	private final int size;
	
	public DataStore() {
		super(new AccessQueue());
		address = 0;
		size = 0;
	}
	
	public DataStore(int size_) {
		super(new AccessQueue());
		// cache-line alignment
		final int align = 64;
		final long mask = ~((long) (align - 1));
		// extra size to allow for alignment
		data = ByteBuffer.allocateDirect(size_ + align);
		data.order(ByteOrder.nativeOrder());
		size = size_;
		try {
			// get the address out of the DirectByteBuffer
			// because of jni reasons, the field is in the base buffer class
			Field f = java.nio.Buffer.class.getDeclaredField("address");
			f.setAccessible(true);
			// get address
			long p0 = (Long) f.get(data);
			if (p0 == 0) throw new AssertionError("failed to get buffer address");
			// align address, rounding up
			address = mask & (p0 + align - 1);
			// set buffer's position to reflect alignment
			data.position((int) (address - p0));
		} catch (NoSuchFieldException e) {
			throw new AssertionError(e);
		} catch (SecurityException e) {
			throw new AssertionError(e);
		} catch (IllegalArgumentException e) {
			throw new AssertionError(e);
		} catch (IllegalAccessException e) {
			throw new AssertionError(e);
		}
	}
	
	public long address() {
		return address;
	}
	
	public int size() {
		return size;
	}
	
	public ByteBuffer map() {
		return map(0);
	}
	
	public ByteBuffer map(int offset) {
		return map(offset, size - offset);
	}
	
	public ByteBuffer map(int offset, int length) {
		offset = Math.min(Math.max(offset, 0), size);
		length = Math.min(size - offset, length);
		ByteBuffer b1 = data.slice();
		b1.position(offset);
		b1.limit(offset + length);
		// slice does not preserve byte order
		return b1.slice().order(data.order());
	}
	
	public void memset(int offset, int length, byte val) {
		offset = Math.min(Math.max(offset, 0), size);
		length = Math.min(size - offset, length);
		Util.unsafe.setMemory(address + offset, length, val);
	}
	
	public void memset(int offset, byte val) {
		memset(offset, size() - offset, val);
	}
	
	public void memset(byte val) {
		memset(0, size(), val);
	}
	
	@Override
	public void prepare(Command c, Access a) {
		c.require(this, a);
	}
	
	@Override
	public String toString() {
		return String.format("DataStore[address=0x%016x, size=%d]", address, size);
	}
	
	public static DataStore alloc(int size, String reason) {
		if (size < POOL_MIN_SIZE) size = POOL_MIN_SIZE;
		if (size > POOL_MAX_SIZE) throw new APIException(
			GL_OUT_OF_MEMORY,
			String.format("alloc size (%d) larger than max allowed (%d)", size, POOL_MAX_SIZE)
		);
		int bin = 31 - Integer.numberOfLeadingZeros(2 * ((size + POOL_MIN_SIZE - 1) / POOL_MIN_SIZE) - 1);
		if ((POOL_MIN_SIZE << bin) < size) throw new AssertionError();
		DataStore ds = (DataStore) pool[bin].poll();
		if (ds != null) return ds;
		ds = new DataStore(POOL_MIN_SIZE << bin);
		// TODO suppress?
		System.err.printf(
			"Initial3D GL pool alloc for '%s' (#%d, %dkB total): %s\n",
			reason, pool_alloc_count.getAndIncrement(), pool_alloc_bytes.addAndGet(ds.size()) / 1024, ds
		);
		return ds;
	}
	
	@SuppressWarnings("unchecked")
	public static void free(DataStore ds) {
		if (ds == null) return;
		if (ds.address() == 0) return;
		if (ds.size() < POOL_MIN_SIZE || ds.size() > POOL_MAX_SIZE) throw new RuntimeException("?");
		int bin = 31 - Integer.numberOfLeadingZeros(ds.size() / POOL_MIN_SIZE);
		pool[bin].offer(ds);
	}
}
