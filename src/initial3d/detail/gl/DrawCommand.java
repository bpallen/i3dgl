package initial3d.detail.gl;

import initial3d.GLEnum;
import initial3d.Vec2i;
import initial3d.detail.Access;
import initial3d.detail.Util;
import sun.misc.Unsafe;

import static initial3d.Functions.*;
import static initial3d.GLEnum.*;
import static initial3d.GLEnumValues.*;

public class DrawCommand extends Command {

	private static final Unsafe unsafe = Util.unsafe;
	
	/** tune with respect to batch prim counts */
	private static final int RASTERBLOCKBUF_NORMAL_SIZE = 1024 * 128;
	
	/** this has to be big enough for a full screen raster block buffer at max res */
	private static final int RASTERBLOCKBUF_WORST_SIZE = 1024 * 1024;
	
	private final FramebufferAccessor fba;
	private final VertexArrayAccessor vaa;
	private final ShaderProgramAccessor spa;
	private final int first_index;
	private final int index_count;
	private final int first_prim;
	private final int prim_count;
	private final boolean last_batch;
	private final Vec2i viewport_pos;
	private final Vec2i viewport_size;
	private final GLEnum front_tri_mode;
	private final GLEnum back_tri_mode;
	private final float half_line_width;
	private final float half_point_size;
	
	private WorkerPool.Ticket rasterticket = null;
	
	/**
	 * A DrawCommand draws a subrange of the primitives from a draw call.
	 * 
	 * @param viewport_
	 * @param fbo
	 * @param vao
	 * @param prog_
	 * @param first_index_
	 * @param index_count_
	 * @param first_prim_
	 * @param prim_count_
	 */
	private DrawCommand(
		I3DGL gl_, Framebuffer fbo, VertexArray vao, ShaderProgram spo,
		int first_index_, int index_count_, int first_prim_, int prim_count_, boolean last_batch_
	) {
		require(fbo, Access.SHARED_WRITE);
		require(vao, Access.READ);
		require(spo, Access.READ);
		fba = (FramebufferAccessor) fbo.accessor();
		vaa = (VertexArrayAccessor) vao.accessor();
		spa = (ShaderProgramAccessor) spo.accessor();
		first_index = first_index_;
		index_count = index_count_;
		first_prim = first_prim_;
		prim_count = prim_count_;
		last_batch = last_batch_;
		viewport_pos = gl_.viewport_pos;
		viewport_size = gl_.viewport_size;
		front_tri_mode = gl_.glIsEnabled(GL_CULL_FACE) && gl_.cull_front ? GL_NONE : gl_.front_tri_mode;
		back_tri_mode = gl_.glIsEnabled(GL_CULL_FACE) && gl_.cull_back ? GL_NONE : gl_.back_tri_mode;
		half_line_width = 0.5f * gl_.line_width;
		half_point_size = 0.5f * gl_.point_size;
	}
	
	/**
	 * THIS IS NO LONGER RELEVANT
	 * 
	 * Get number of primitives per batch for some size of transform buffer.
	 * 
	 * @param bufsize
	 * @return
	 */
	@Deprecated
	private static int batchPrimitiveCount(VertexArrayAccessor vaa, ShaderProgramAccessor spa, int bufsize) {
		// --- max size consumed by transform buffer ---
		// np = total primitives from vao
		// nvv = vertices per primitive from vao
		// ngv = vertices per primitive from geom shader
		// mgv = max geometry vertices
		// mgp = max geometry primitives
		// sv = vertex out size
		// sg = geometry out size
		// vertex indices (as shorts): np * mgp * ngv * 2
		// vert shader vertices: np * nvv * sv
		// geom shader vertices: np * mgv * sg
		// transform buffer: np * (mgp * ngv * 2 + nvv * sv + mgv * sg)
		final int vao_prim_verts = vaa.primitiveVertexCount();
		// divide by 2 to allow space for clipping
		// TODO account for clipping less pessimistically
		return bufsize / (2 * (
			spa.geometryMaxPrimitives(vao_prim_verts) * spa.geometryPrimitiveVertexCount(vao_prim_verts) * 2 +
			vao_prim_verts * spa.vertexOutSize() +
			spa.geometryMaxVertices(vao_prim_verts) * spa.geometryOutSize()
		));
	}
	
	public static void invokeDraw(I3DGL gl_, Framebuffer fbo, VertexArray vao, ShaderProgram spo, int first_index_, int index_count_) {
		VertexArrayAccessor vaa = (VertexArrayAccessor) vao.accessor();
		ShaderProgramAccessor spa = (ShaderProgramAccessor) spo.accessor();
		final int pc = vaa.primitiveCount(index_count_);
		// TODO tune batch primitive count
		// this has a bearing on suitable raster block buffer normal sizes
		final int bpc = clamp(pc / (WorkerPool.numWorkers() * 10), 100, 5000);
		for (int i = 0; i < pc; i += bpc) {
			DrawCommand dc = new DrawCommand(gl_, fbo, vao, spo, first_index_, index_count_, i, Math.min(pc - i, bpc), i + bpc >= pc);
			gl_.driver().invoke(dc);
		}
	}
	
	private static void interpolateVertex(long pvert_out, long pv0, long pv1, float t, ShaderProgramAccessor spa) {
		final float s = 1.f - t;
		final float w0 = unsafe.getFloat(pv0 + 12);
		final float w1 = unsafe.getFloat(pv1 + 12);
		final float w = w0 * s + w1 * t;
		unsafe.putFloat(pvert_out +  0, unsafe.getFloat(pv0 +  0) * s + unsafe.getFloat(pv1 +  0) * t);
		unsafe.putFloat(pvert_out +  4, unsafe.getFloat(pv0 +  4) * s + unsafe.getFloat(pv1 +  4) * t);
		unsafe.putFloat(pvert_out +  8, unsafe.getFloat(pv0 +  8) * s + unsafe.getFloat(pv1 +  8) * t);
		unsafe.putFloat(pvert_out + 12, w);
		spa.varyingInterpolate2(pvert_out + 16, pv0 + 16, pv1 + 16, s, t, w0, w1, w);
	}
	
	/**
	 * 1 good vertex (0) -> 1 tri
	 */
	private static void clipTriangle1(long ppverts_out, long pverts_out, long ppv0, long ppv1, long ppv2, float cd0, float cd1, float cd2, ShaderProgramAccessor spa) {
		final long pv0 = unsafe.getAddress(ppv0);
		final long mark0 = unsafe.getLong(ppv0 + 8);
		final long pv1 = unsafe.getAddress(ppv1);
		final long mark1 = unsafe.getLong(ppv1 + 8);
		final long pv2 = unsafe.getAddress(ppv2);
		final long mark2 = unsafe.getLong(ppv2 + 8);
		final long pv3 = pverts_out;
		interpolateVertex(pv3, pv0, pv1, cd0 / (cd0 - cd1), spa);
		final long pv4 = pverts_out + spa.geometryOutSize();
		interpolateVertex(pv4, pv0, pv2, cd0 / (cd0 - cd2), spa);
		// prim
		unsafe.putAddress(ppverts_out +  0, pv0);
		unsafe.putLong(   ppverts_out +  8, mark0);
		unsafe.putAddress(ppverts_out + 16, pv3);
		unsafe.putLong(   ppverts_out + 24, 0x1L);
		unsafe.putAddress(ppverts_out + 32, pv4);
		unsafe.putLong(   ppverts_out + 40, mark2);
	}
	
	/**
	 * 2 good vertices (0, 2) -> 2 tris
	 */
	private static void clipTriangle2(long ppverts_out, long pverts_out, long ppv0, long ppv1, long ppv2, float cd0, float cd1, float cd2, ShaderProgramAccessor spa) {
		final long pv0 = unsafe.getAddress(ppv0);
		final long mark0 = unsafe.getLong(ppv0 + 8);
		final long pv1 = unsafe.getAddress(ppv1);
		final long mark1 = unsafe.getLong(ppv1 + 8);
		final long pv2 = unsafe.getAddress(ppv2);
		final long mark2 = unsafe.getLong(ppv2 + 8);
		final long pv3 = pverts_out;
		interpolateVertex(pv3, pv0, pv1, cd0 / (cd0 - cd1), spa);
		final long pv4 = pverts_out + spa.geometryOutSize();
		interpolateVertex(pv4, pv2, pv1, cd2 / (cd2 - cd1), spa);
		// prim 0
		unsafe.putAddress(ppverts_out +  0, pv0);
		unsafe.putLong(   ppverts_out +  8, mark0);
		unsafe.putAddress(ppverts_out + 16, pv3);
		unsafe.putLong(   ppverts_out + 24, 0x1L);
		unsafe.putAddress(ppverts_out + 32, pv2);
		unsafe.putLong(   ppverts_out + 40, mark2);
		// prim 1
		unsafe.putAddress(ppverts_out + 48, pv3);
		unsafe.putLong(   ppverts_out + 56, 0x1L);
		unsafe.putAddress(ppverts_out + 64, pv4);
		unsafe.putLong(   ppverts_out + 72, mark1);
		unsafe.putAddress(ppverts_out + 80, pv2);
		unsafe.putLong(   ppverts_out + 88, mark2 | 0x1L);
	}
	
	/**
	 * 3 good vertices (0, 1, 2) -> 1 tri
	 */
	private static void clipTriangle3(long ppverts_out, long pverts_out, long ppv0, long ppv1, long ppv2, float cd0, float cd1, float cd2, ShaderProgramAccessor spa) {
		final long pv0 = unsafe.getAddress(ppv0);
		final long mark0 = unsafe.getLong(ppv0 + 8);
		final long pv1 = unsafe.getAddress(ppv1);
		final long mark1 = unsafe.getLong(ppv1 + 8);
		final long pv2 = unsafe.getAddress(ppv2);
		final long mark2 = unsafe.getLong(ppv2 + 8);
		// prim
		unsafe.putAddress(ppverts_out +  0, pv0);
		unsafe.putLong(   ppverts_out +  8, mark0);
		unsafe.putAddress(ppverts_out + 16, pv1);
		unsafe.putLong(   ppverts_out + 24, mark1);
		unsafe.putAddress(ppverts_out + 32, pv2);
		unsafe.putLong(   ppverts_out + 40, mark2);
	}
	
	/**
	 * 1 good vertex (0) -> 1 line
	 */
	private static void clipLine1(long ppverts_out, long pverts_out, long ppv0, long ppv1, float cd0, float cd1, ShaderProgramAccessor spa) {
		final long pv0 = unsafe.getAddress(ppv0);
		final long mark0 = unsafe.getLong(ppv0 + 8);
		final long pv1 = unsafe.getAddress(ppv1);
		final long mark1 = unsafe.getLong(ppv1 + 8);
		final long pv2 = pverts_out;
		interpolateVertex(pv2, pv0, pv1, cd0 / (cd0 - cd1), spa);
		// prim
		unsafe.putAddress(ppverts_out +  0, pv0);
		unsafe.putLong(   ppverts_out +  8, mark0);
		unsafe.putAddress(ppverts_out + 16, pv2);
		unsafe.putLong(   ppverts_out + 24, 0);
	}
	
	/**
	 * 2 good vertices (0, 1) -> 1 line
	 */
	private static void clipLine2(long ppverts_out, long pverts_out, long ppv0, long ppv1, float cd0, float cd1, ShaderProgramAccessor spa) {
		final long pv0 = unsafe.getAddress(ppv0);
		final long mark0 = unsafe.getLong(ppv0 + 8);
		final long pv1 = unsafe.getAddress(ppv1);
		final long mark1 = unsafe.getLong(ppv1 + 8);
		// prim
		unsafe.putAddress(ppverts_out +  0, pv0);
		unsafe.putLong(   ppverts_out +  8, mark0);
		unsafe.putAddress(ppverts_out + 16, pv1);
		unsafe.putLong(   ppverts_out + 24, mark1);
	}
	
	private void runTransform() {
		
		// size of a clip function struct
		final int clipsize = 12;
		
		// constant cache
		final int num_workers = WorkerPool.numWorkers();
		final VertexArrayAccessor vaa = this.vaa;
		final ShaderProgramAccessor spa = this.spa;
		final int first_prim = this.first_prim;
		final int prim_count = this.prim_count;
		final int first_index = this.first_index;
		final int index_count = this.index_count;
		final int vpvc = vaa.primitiveVertexCount();
		final int gpvc = spa.geometryPrimitiveVertexCount(vpvc);
		final int vsz = spa.vertexOutSize();
		final int gsz = spa.geometryOutSize();
		final int front_tri_mode = this.front_tri_mode.valueInt();
		final int back_tri_mode = this.back_tri_mode.valueInt();
		final int vp_x = viewport_pos.x;
		final int vp_y = viewport_pos.y;
		final int vp_w = viewport_size.x;
		final int vp_h = viewport_size.y;
		final float hlwidth = half_line_width;
		
		// shader uniforms
		final long puniforms = uniforms().address();
		
		// local temp
		final long ptemp = WorkerPool.scratchStorage().address();
		// primitive vertex indices
		final long pprim0 = ptemp;
		// clip functions
		final long pclip0 = pprim0 + 32;
		// fetched attributes for one vertex
		final long pattrib0 = ~15L & (pclip0 + clipsize * 6 + 15);
		// vertex pointers for geometry processing
		// TODO enforce max gs vertices
		// max count = vpvc + 2 * gmaxv + sum[x=1:clipcount](gpvc * 2^x)
		//  = vpvc + 2 * gmaxv + 2 * gpvc * (2^clipcount - 1)
		//  = 6 + 2 * 128 + 2 * 3 * (2^6 - 1)
		//  = 640
		final long ppvert0 = pattrib0 + vaa.vertexSize();
		// vertex storage for geometry processing
		// max count = vpvc + gmaxv + sum[x=1:clipcount]((gpvc - 1) * 2^x)
		//  = vpvc + gmaxv + 2 * (gpvc - 1) * (2^clipcount - 1)
		//  = 6 + 128 + 2 * 2 * (2^6 - 1)
		//  = 386
		// TODO check space required
		final long pvert0 = ppvert0 + 640 * 16;
		// temp storage for subsequent function calls
		final long ptemp_next = pvert0 + Math.max(spa.vertexOutSize(), spa.geometryOutSize()) * 400;
		
		// raster block buffer
		DataStore rasterblockbuf = allocStorage(RASTERBLOCKBUF_NORMAL_SIZE);
		long prasterblock_base = rasterblockbuf.address();
		long prasterblock_limit = rasterblockbuf.address() + rasterblockbuf.size();
		long prasterblock_next = prasterblock_base;
		
		// init clip functions
		// TODO clip triangles less pessimistically so we can do fullscreen in one
		// - byte offset into vertices from the geometry stage, specifying a float 'f'
		// - float 'a' multiplier for 'f'
		// - float 'b' multiplier for 'w'
		// clipdist = f * a + w * b
		// clip if clipdist < 0
		unsafe.putInt(  pclip0 + clipsize * 0 + 0, 0); // gl_Position.x
		unsafe.putFloat(pclip0 + clipsize * 0 + 4, 1.f);
		unsafe.putFloat(pclip0 + clipsize * 0 + 8, 1.f);
		unsafe.putInt(  pclip0 + clipsize * 1 + 0, 0); // -gl_Position.x
		unsafe.putFloat(pclip0 + clipsize * 1 + 4, -1.f);
		unsafe.putFloat(pclip0 + clipsize * 1 + 8, 1.f);
		unsafe.putInt(  pclip0 + clipsize * 2 + 0, 4); // gl_Position.y
		unsafe.putFloat(pclip0 + clipsize * 2 + 4, 1.f);
		unsafe.putFloat(pclip0 + clipsize * 2 + 8, 1.f);
		unsafe.putInt(  pclip0 + clipsize * 3 + 0, 4); // -gl_Position.y
		unsafe.putFloat(pclip0 + clipsize * 3 + 4, -1.f);
		unsafe.putFloat(pclip0 + clipsize * 3 + 8, 1.f);
		unsafe.putInt(  pclip0 + clipsize * 4 + 0, 8); // gl_Position.z
		unsafe.putFloat(pclip0 + clipsize * 4 + 4, 1.f);
		unsafe.putFloat(pclip0 + clipsize * 4 + 8, 1.f);
		unsafe.putInt(  pclip0 + clipsize * 5 + 0, 8); // -gl_Position.z
		unsafe.putFloat(pclip0 + clipsize * 5 + 4, -1.f);
		unsafe.putFloat(pclip0 + clipsize * 5 + 8, 1.f);
		
		// for each input primitive
		for (int ip = first_prim; ip <= first_prim + prim_count; ip++) {
			
			// vertex storage for this input primitive
			long pvert_next = pvert0;
			
			// vertex pointers that will make up the output primitives
			// start of used range
			long ppvert_base = ppvert0;
			// next vert pointer: inc after write
			long ppvert_next = ppvert0;
			
			// load vertex indices for this primitive
			if (ip < first_prim + prim_count) {
				// normal behaviour
				vaa.loadPrimitive(pprim0, first_index, index_count, ip);
			} else {
				// special last case
				if (!last_batch || !vaa.loadLastPrimitive(pprim0, first_index, index_count)) break;
			}
			
			// for each vertex
			for (int j = 0; j < vpvc; j++) {
				// (post-transform cache goes here, eventually)
				// load vertex from vao
				int iv = unsafe.getInt(pprim0 + j * 4);
				vaa.loadVertex(pattrib0, iv);
				// invoke vertex shader
				spa.shadeVertex(pvert_next, ptemp_next, puniforms, pattrib0);
				unsafe.putAddress(ppvert_next, pvert_next);
				pvert_next += vsz;
				ppvert_next += 8;
			}
			
			// invoke geometry shader
			final int gres = spa.shadeGeometry(ppvert_next, pvert_next, ptemp_next, puniforms, ppvert_base, vpvc);
			// unpack result value and apply
			final int gverts = gres & 0xFF;
			final int gprims = gres >> 16;
			pvert_next += gverts * gsz;
			ppvert_base = ppvert_next;
			ppvert_next += 8 * gprims * gpvc;
			
			// expand vertex pointers to include mark words
			// after this point, vertex pointers are effectively 16 bytes
			// the mark word allows marking non-boundary edges during clipping
			if (true) {
				// primitives to iterate over
				final long ppvert_beg = ppvert_base;
				final long ppvert_end = ppvert_next;
				// re-base prim output
				ppvert_base = ppvert_next;
				for (long ppvert = ppvert_beg; ppvert < ppvert_end; ppvert += 8) {
					unsafe.putAddress(ppvert_next, unsafe.getAddress(ppvert));
					unsafe.putLong(ppvert_next + 8, 0);
					ppvert_next += 16;
				}
			}
			
			// save state after geometry shader but before any clipping
			final long pvert_clipbase = pvert_next;
			final long ppvert_clipbase = ppvert_next;
			
			// for each geometry shader output primitive
			// TODO test with real geo shader
			final long ppvert_gprim_beg = ppvert_base;
			final long ppvert_gprim_end = ppvert_next;
			for (long ppvert_gprim = ppvert_gprim_beg; ppvert_gprim < ppvert_gprim_end; ppvert_gprim += 16 * gpvc) {
				
				// TODO probably should do initial face-cull here
				
				// discard interpolated vertices and clipped primitives after each geometry shader output primitive
				pvert_next = pvert_clipbase;
				ppvert_next = ppvert_clipbase;
				ppvert_base = ppvert_gprim;
				
				// clip against each clip func, possibly generating more vertices by interpolation
				switch (gpvc) {
				case 1:
					// TODO clip points
					ppvert_base = ppvert_next;
					break;
				case 2:
					// TODO test this
					// for each clip function
					for (long pclip = pclip0; pclip < pclip0 + clipsize * 6; pclip += clipsize) {
						// clip function
						final int clip_offset = unsafe.getInt(pclip);
						final float clip_a = unsafe.getFloat(pclip + 4);
						final float clip_b = unsafe.getFloat(pclip + 8);
						// primitives to iterate over (special first case)
						final long ppvert_beg = ppvert_base;
						final long ppvert_end = ppvert_base == ppvert_gprim ? ppvert_base + 16 * 2 : ppvert_next;
						// re-base prim output
						ppvert_base = ppvert_next;
						// for each line
						for (long ppvert = ppvert_beg; ppvert < ppvert_end; ppvert += 16 * 2) {
							// vertex pointers
							final long ppv0 = ppvert;
							final long ppv1 = ppvert + 16;
							final long pv0 = unsafe.getAddress(ppv0);
							final long pv1 = unsafe.getAddress(ppv1);
							// eval clip distance
							final float cd0 = unsafe.getFloat(pv0 + clip_offset) * clip_a + unsafe.getFloat(pv0 + 12) * clip_b;
							final float cd1 = unsafe.getFloat(pv1 + clip_offset) * clip_a + unsafe.getFloat(pv1 + 12) * clip_b;
							// which vertices are good?
							final int vmask = (cd1 >= 0 ? 2 : 0) | (cd0 >= 0 ? 1 : 0);
							// 0 good vertices : do nothing
							// 1 good vertex   : clip method 1, 1 prim, 1 new vert
							// 2 good vertices : clip method 2, 1 prim, 0 new vert
							// re-order vertices to start at the last good vertex (with correct winding)
							switch (vmask) {
							case 1:
								clipLine1(ppvert_next, pvert_next, ppv0, ppv1, cd0, cd1, spa);
								ppvert_next += 16 * 2;
								pvert_next += gsz * 1;
								break;
							case 2:
								clipLine1(ppvert_next, pvert_next, ppv1, ppv0, cd1, cd0, spa);
								ppvert_next += 16 * 2;
								pvert_next += gsz * 1;
								break;
							case 3:
								clipLine2(ppvert_next, pvert_next, ppv0, ppv1, cd0, cd1, spa);
								ppvert_next += 16 * 2;
								break;
							default:
								break;
							}
						}
					}
					break;
				case 3:
					// for each clip function
					for (long pclip = pclip0; pclip < pclip0 + clipsize * 6; pclip += clipsize) {
						// clip function
						final int clip_offset = unsafe.getInt(pclip);
						final float clip_a = unsafe.getFloat(pclip + 4);
						final float clip_b = unsafe.getFloat(pclip + 8);
						// primitives to iterate over (special first case)
						final long ppvert_beg = ppvert_base;
						final long ppvert_end = ppvert_base == ppvert_gprim ? ppvert_base + 16 * 3 : ppvert_next;
						// re-base prim output
						ppvert_base = ppvert_next;
						// for each triangle
						for (long ppvert = ppvert_beg; ppvert < ppvert_end; ppvert += 16 * 3) {
							// vertex pointers
							final long ppv0 = ppvert;
							final long ppv1 = ppvert + 16;
							final long ppv2 = ppvert + 32;
							final long pv0 = unsafe.getAddress(ppv0);
							final long pv1 = unsafe.getAddress(ppv1);
							final long pv2 = unsafe.getAddress(ppv2);
							// eval clip distance
							final float cd0 = unsafe.getFloat(pv0 + clip_offset) * clip_a + unsafe.getFloat(pv0 + 12) * clip_b;
							final float cd1 = unsafe.getFloat(pv1 + clip_offset) * clip_a + unsafe.getFloat(pv1 + 12) * clip_b;
							final float cd2 = unsafe.getFloat(pv2 + clip_offset) * clip_a + unsafe.getFloat(pv2 + 12) * clip_b;
							// which vertices are good?
							final int vmask = (cd2 >= 0 ? 4 : 0) | (cd1 >= 0 ? 2 : 0) | (cd0 >= 0 ? 1 : 0);
							// 0 good vertices : do nothing
							// 1 good vertex   : clip method 1, 1 prim, 2 new vert
							// 2 good vertices : clip method 2, 2 prim, 2 new vert
							// 3 good vertices : clip method 3, 1 prim, 0 new vert
							// re-order vertices to start at the last good vertex (with correct winding)
							switch (vmask) {
							case 1:
								clipTriangle1(ppvert_next, pvert_next, ppv0, ppv1, ppv2, cd0, cd1, cd2, spa);
								ppvert_next += 16 * 3;
								pvert_next += gsz * 2;
								break;
							case 2:
								clipTriangle1(ppvert_next, pvert_next, ppv1, ppv2, ppv0, cd1, cd2, cd0, spa);
								ppvert_next += 16 * 3;
								pvert_next += gsz * 2;
								break;
							case 4:
								clipTriangle1(ppvert_next, pvert_next, ppv2, ppv0, ppv1, cd2, cd0, cd1, spa);
								ppvert_next += 16 * 3;
								pvert_next += gsz * 2;
								break;
							case 3:
								clipTriangle2(ppvert_next, pvert_next, ppv1, ppv2, ppv0, cd1, cd2, cd0, spa);
								ppvert_next += 16 * 6;
								pvert_next += gsz * 2;
								break;
							case 5:
								clipTriangle2(ppvert_next, pvert_next, ppv0, ppv1, ppv2, cd0, cd1, cd2, spa);
								ppvert_next += 16 * 6;
								pvert_next += gsz * 2;
								break;
							case 6:
								clipTriangle2(ppvert_next, pvert_next, ppv2, ppv0, ppv1, cd2, cd0, cd1, spa);
								ppvert_next += 16 * 6;
								pvert_next += gsz * 2;
								break;
							case 7:
								clipTriangle3(ppvert_next, pvert_next, ppv0, ppv1, ppv2, cd0, cd1, cd2, spa);
								ppvert_next += 16 * 3;
								break;
							default:
								break;
							}
						}
					}
					break;
				default:
					break;
				}
				
				// convert everything to triangles
				// includes polygonmode triangle -> line/point -> triangle
				// this is specified to happen after triangle clipping
				// the mark word is now used to hold faceness and original primitive type
				// (in the first vertex of each triangle)
				switch (gpvc) {
				case 1:
					// TODO points to tris
					if (true) {
						// primitives to iterate over
						final long ppvert_beg = ppvert_base;
						final long ppvert_end = ppvert_next;
						// re-base prim output
						ppvert_base = ppvert_next;
						
					}
					break;
				case 2:
					if (true) {
						// primitives to iterate over
						final long ppvert_beg = ppvert_base;
						final long ppvert_end = ppvert_next;
						// re-base prim output
						ppvert_base = ppvert_next;
						// for each line
						for (long ppvert = ppvert_beg; ppvert < ppvert_end; ppvert += 16 * 2) {
							// vertex pointers and mark words
							final long pv0 = unsafe.getAddress(ppvert);
							final long pv1 = unsafe.getAddress(ppvert + 16);
							// put line
							unsafe.putAddress(ppvert_next +  0, pv0);
							unsafe.putLong(ppvert_next + 8, 0b101L);
							unsafe.putAddress(ppvert_next + 16, pv0);
							unsafe.putAddress(ppvert_next + 32, pv1);
							unsafe.putAddress(ppvert_next + 48, pv1);
							unsafe.putLong(ppvert_next + 56, 0b101L);
							unsafe.putAddress(ppvert_next + 64, pv1);
							unsafe.putAddress(ppvert_next + 80, pv0);
							ppvert_next += 96;
						}
					}
					break;
				case 3:
					if (true) {
						// primitives to iterate over
						final long ppvert_beg = ppvert_base;
						final long ppvert_end = ppvert_next;
						// re-base prim output
						ppvert_base = ppvert_next;
						// for each tri
						for (long ppvert = ppvert_beg; ppvert < ppvert_end; ppvert += 16 * 3) {
							// vertex pointers and mark words
							final long pv0 = unsafe.getAddress(ppvert);
							final long mark0 = unsafe.getLong(ppvert + 8);
							final long pv1 = unsafe.getAddress(ppvert + 16);
							final long mark1 = unsafe.getLong(ppvert + 24);
							final long pv2 = unsafe.getAddress(ppvert + 32);
							final long mark2 = unsafe.getLong(ppvert + 40);
							// eval front/back face-ness (after perspective division)
							final float w0 = unsafe.getFloat(pv0 + 12);
							final float w1 = unsafe.getFloat(pv1 + 12);
							final float w2 = unsafe.getFloat(pv2 + 12);
							float iw0 = 1.f / w0;
							float iw1 = 1.f / w1;
							float iw2 = 1.f / w2;
							final float x0 = iw0 * unsafe.getFloat(pv0 +  0);
							final float y0 = iw0 * unsafe.getFloat(pv0 +  4);
							final float x1 = iw1 * unsafe.getFloat(pv1 +  0);
							final float y1 = iw1 * unsafe.getFloat(pv1 +  4);
							final float x2 = iw2 * unsafe.getFloat(pv2 +  0);
							final float y2 = iw2 * unsafe.getFloat(pv2 +  4);
							final boolean frontface = (x1 - x0) * (y2 - y1) - (y1 - y0) * (x2 - x1) > 0.f;
							// process tri based on mode for its face-ness
							switch (frontface ? front_tri_mode : back_tri_mode) {
							case GL_FILL_value:
								// just copy the triangle
								unsafe.putAddress(ppvert_next +  0, pv0);
								unsafe.putLong(ppvert_next + 8, 0b110L | (frontface ? 1 : 0));
								unsafe.putAddress(ppvert_next + 16, pv1);
								unsafe.putAddress(ppvert_next + 32, pv2);
								ppvert_next += 48;
								break;
							case GL_LINE_value:
								// put lines, but only for boundary edges
								if ((mark0 & 0x1L) == 0) {
									unsafe.putAddress(ppvert_next +  0, pv0);
									unsafe.putLong(ppvert_next + 8, 0b100L | (frontface ? 1 : 0));
									unsafe.putAddress(ppvert_next + 16, pv0);
									unsafe.putAddress(ppvert_next + 32, pv1);
									unsafe.putAddress(ppvert_next + 48, pv1);
									unsafe.putLong(ppvert_next + 56, 0b100L | (frontface ? 1 : 0));
									unsafe.putAddress(ppvert_next + 64, pv1);
									unsafe.putAddress(ppvert_next + 80, pv0);
									ppvert_next += 96;
								}
								if ((mark1 & 0x1L) == 0) {
									unsafe.putAddress(ppvert_next +  0, pv1);
									unsafe.putLong(ppvert_next + 8, 0b100L | (frontface ? 1 : 0));
									unsafe.putAddress(ppvert_next + 16, pv1);
									unsafe.putAddress(ppvert_next + 32, pv2);
									unsafe.putAddress(ppvert_next + 48, pv2);
									unsafe.putLong(ppvert_next + 56, 0b100L | (frontface ? 1 : 0));
									unsafe.putAddress(ppvert_next + 64, pv2);
									unsafe.putAddress(ppvert_next + 80, pv1);
									ppvert_next += 96;
								}
								if ((mark2 & 0x1L) == 0) {
									unsafe.putAddress(ppvert_next +  0, pv2);
									unsafe.putLong(ppvert_next + 8, 0b100L | (frontface ? 1 : 0));
									unsafe.putAddress(ppvert_next + 16, pv2);
									unsafe.putAddress(ppvert_next + 32, pv0);
									unsafe.putAddress(ppvert_next + 48, pv0);
									unsafe.putLong(ppvert_next + 56, 0b100L | (frontface ? 1 : 0));
									unsafe.putAddress(ppvert_next + 64, pv0);
									unsafe.putAddress(ppvert_next + 80, pv2);
									ppvert_next += 96;
								}
								break;
							case GL_POINT_value:
								// TODO tris to points
								break;
							default:
								// cull
								break;
							}
						}
					}
					break;
				default:
					break;
				}
				
				// rasterize final triangles (but no shading or fbo writes)
				for (long ppvert = ppvert_base; ppvert < ppvert_next; ppvert += 16 * 3) {
					final long pv0 = unsafe.getAddress(ppvert +  0);
					final long pv1 = unsafe.getAddress(ppvert + 16);
					final long pv2 = unsafe.getAddress(ppvert + 32);
					final int mark0 = (int) unsafe.getLong(ppvert + 8);
					final int rpvc = (mark0 & 0b110) >> 1;
					
					// bytes written by rasterization
					long count = 0;
					
					do {
						
						count = Rasterizer.rasterizeTriangle(
							num_workers,
							prasterblock_next, prasterblock_limit, ptemp_next, fba, spa,
							pv0, pv1, pv2,
							vp_x, vp_y, vp_w, vp_h,
							(mark0 & 1) == 1, rpvc == 2 ? hlwidth : 0.f
						);
						
						if (count < 0) {
							// not enough space in buffer
							// submit current raster block buffer
							workerInvokeLater(rasterticket, new RasterCommitTask(prasterblock_base, prasterblock_next));
							// replace buffer
							if (prasterblock_limit - prasterblock_next < rasterblockbuf.size() / 4) {
								// buffer mostly full, use normal size
								rasterblockbuf = allocStorage(RASTERBLOCKBUF_NORMAL_SIZE);
							} else {
								// use worst-case sized buffer
								rasterblockbuf = allocStorage(RASTERBLOCKBUF_WORST_SIZE);
							}
							prasterblock_base = rasterblockbuf.address();
							prasterblock_limit = rasterblockbuf.address() + rasterblockbuf.size();
							prasterblock_next = prasterblock_base;
						} else {
							prasterblock_next += count;
						}
						
						// try rasterize again if we had to replace the buffer
					} while (count < 0);
					
				}
				
			} // for each geometry shader output primitive
			
		} // for each input primitive
		
		// submit any remaining raster blocks
		if (prasterblock_next > prasterblock_base) {
			workerInvokeLater(rasterticket, new RasterCommitTask(prasterblock_base, prasterblock_next));
		}
		
		// ticketing system ensures proper global ordering of raster commit tasks
		WorkerPool.invoke(rasterticket);
		
	}
	
	@Override
	public void run() {
		rasterticket = WorkerPool.newTicket(true);
		workerInvoke(this::runTransform);
	}
	
	private class RasterCommitTask implements Runnable {

		private final long prasterblock_beg;
		private final long prasterblock_end;
		
		public RasterCommitTask(long prasterblock_beg_, long prasterblock_end_) {
			prasterblock_beg = prasterblock_beg_;
			prasterblock_end = prasterblock_end_;
		}
		
		@Override
		public void run() {
			
			// constant cache
			final FramebufferAccessor fba = DrawCommand.this.fba;
			final ShaderProgramAccessor spa = DrawCommand.this.spa;
			final long prasterblock_beg = this.prasterblock_beg;
			final long prasterblock_end = this.prasterblock_end;
			final long puniforms = uniforms().address();
			final long ptemp = WorkerPool.scratchStorage().address();
			final int worker_id = WorkerPool.id();
			final int num_workers = WorkerPool.numWorkers();
			
			Rasterizer.commitRasterBlocks(worker_id, num_workers, fba, spa, prasterblock_beg, prasterblock_end, puniforms, ptemp);
			
		}
		
	}
	
}







