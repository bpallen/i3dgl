package initial3d.detail.gl;

import initial3d.GLEnum;
import initial3d.detail.Access;
import initial3d.detail.SimpleCloneable;
import initial3d.detail.bytecode.Flag;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;

import static initial3d.GLEnum.*;
import static initial3d.GLEnumValues.*;

public class VertexArray extends Resource implements BindState {

	public static final int MAX_ATTRIBUTES = 16;
	
	public class Attribute extends SimpleCloneable<Attribute> {
		
		public Buffer vbo = null;
		
		// TODO divisor
		public boolean enabled = false;
		public int components = 4;
		public int stride = 0;
		public GLEnum type = GL_FLOAT;
		public boolean normalized = true;
		public boolean integer = false;
		public int offset = 0;
		
		public Attribute() { }
		
	}
	
	private static final BindPoint element_array_bind_point = new BindPoint(GLEnum.GL_ELEMENT_ARRAY_BUFFER);
	
	// index buffer
	private Buffer ibo = null;
	
	// type of data in ibo; if null, ibo is not being used even if attached (glDrawArrays)
	private GLEnum ibo_type = null;
	
	// what kind of primitives to render
	private GLEnum prim_mode = GL_POINTS;
	
	private final Attribute[] attributes = new Attribute[MAX_ATTRIBUTES];
	
	public VertexArray(I3DGL gl_, int name_) {
		super(gl_, name_);
		for (int i = 0; i < attributes.length; i++) {
			attributes[i] = new Attribute();
		}
	}
	
	public GLEnum indexType() {
		return ibo_type;
	}
	
	public void indexType(GLEnum type) {
		gl().validateEnum("index type", type, GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT, GL_UNSIGNED_INT, null);
		if (type != ibo_type) {
			ibo_type = type;
			markAccessorDirty();
		}
	}
	
	public GLEnum primitiveMode() {
		return prim_mode;
	}
	
	public void primitiveMode(GLEnum mode) {
		gl().validateEnum(
			"primitive mode", mode,
			GL_POINTS,
			GL_LINES,
			GL_LINE_STRIP,
			GL_LINE_LOOP,
			GL_LINES_ADJACENCY,
			GL_LINE_STRIP_ADJACENCY,
			GL_TRIANGLES,
			GL_TRIANGLE_STRIP,
			GL_TRIANGLE_FAN,
			GL_TRIANGLES_ADJACENCY,
			GL_TRIANGLE_STRIP_ADJACENCY
		);
		if (mode != prim_mode) {
			prim_mode = mode;
			markAccessorDirty();
		}
	}
	
	public Buffer indexBuffer() {
		return ibo;
	}
	
	public Attribute attribute(int i) {
		gl().validateValueRange("vertex attrib index", i, 0, MAX_ATTRIBUTES - 1);
		return attributes[i].clone();
	}
	
	public void enable(int i, boolean b) {
		gl().validateValueRange("vertex attrib index", i, 0, MAX_ATTRIBUTES - 1);
		attributes[i].enabled = b;
		markAccessorDirty();
	}
	
	public void attributeFloat(int i, Buffer vbo, int components, int stride, GLEnum type, int offset) {
		gl().validateValueRange("vertex attrib index", i, 0, MAX_ATTRIBUTES - 1);
		gl().validateValueRange("vertex attrib size", components, 1, 4);
		gl().validateValueRange("vertex attrib stride", stride, 0);
		gl().validateValueRange("vertex attrib offset", offset, 0);
		markAccessorDirty();
		Attribute a = attributes[i];
		a.vbo = vbo;
		a.integer = false;
		a.normalized = false;
		a.components = components;
		a.stride = stride;
		a.type = type;
		a.offset = offset;
	}
	
	public void attributeNormalized(int i, Buffer vbo, int components, int stride, GLEnum type, int offset) {
		gl().validateValueRange("vertex attrib index", i, 0, MAX_ATTRIBUTES - 1);
		gl().validateValueRange("vertex attrib size", components, 1, 4);
		gl().validateValueRange("vertex attrib stride", stride, 0);
		gl().validateValueRange("vertex attrib offset", offset, 0);
		markAccessorDirty();
		Attribute a = attributes[i];
		a.vbo = vbo;
		a.integer = false;
		a.normalized = true;
		a.components = components;
		a.stride = stride;
		a.type = type;
		a.offset = offset;
	}
	
	public void attributeInteger(int i, Buffer vbo, int components, int stride, GLEnum type, int offset) {
		gl().validateValueRange("vertex attrib index", i, 0, MAX_ATTRIBUTES - 1);
		gl().validateValueRange("vertex attrib size", components, 1, 4);
		gl().validateValueRange("vertex attrib stride", stride, 0);
		gl().validateValueRange("vertex attrib offset", offset, 0);
		markAccessorDirty();
		Attribute a = attributes[i];
		a.vbo = vbo;
		a.integer = true;
		a.normalized = false;
		a.components = components;
		a.stride = stride;
		a.type = type;
		a.offset = offset;
	}
	
	@Override
	public void prepare(Command c, Access a) {
		c.require(ibo, a);
		for (Attribute at : attributes) {
			if (at.enabled) c.require(at.vbo, a);
		}
	}
	
	@Override
	public Resource boundResource(BindPoint bp) {
		if (element_array_bind_point.equals(bp)) return ibo;
		return null;
	}

	@Override
	public boolean bindResource(BindPoint bp, Resource r) {
		if (element_array_bind_point.equals(bp)) {
			markAccessorDirty();
			ibo = (Buffer) r;
			return true;
		}
		return false;
	}

	@Override
	public void unbindResource(Resource r) {
		if (ibo == r) {
			markAccessorDirty();
			ibo = null;
		}
	}

	@Override
	public Class<?> accessorInterface() {
		return VertexArrayAccessor.class;
	}
	
	@Override
	protected void buildAccessor(ClassBuilder cb) throws Exception {
		buildPrimitiveType(cb);
		buildPrimitiveVertexCount(cb);
		buildPrimitiveCount(cb);
		buildLoadPrimitive(cb);
		buildLoadLastPrimitive(cb);
		buildVertexSize(cb);
		buildLoadVertex(cb);
	}

	private void buildPrimitiveType(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("primitiveType_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, GLEnum.class);
		switch (prim_mode.valueInt()) {
		case GL_POINTS_value:
			mb.getStatic(GLEnum.class, "GL_POINTS");
			break;
		case GL_LINES_value:
		case GL_LINE_STRIP_value:
		case GL_LINE_LOOP_value:
			mb.getStatic(GLEnum.class, "GL_LINES");
			break;
		case GL_LINES_ADJACENCY_value:
		case GL_LINE_STRIP_ADJACENCY_value:
			mb.getStatic(GLEnum.class, "GL_LINES_ADJACENCY");
			break;
		case GL_TRIANGLES_value:
		case GL_TRIANGLE_STRIP_value:
		case GL_TRIANGLE_FAN_value:
			mb.getStatic(GLEnum.class, "GL_TRIANGLES");
			break;
		case GL_TRIANGLES_ADJACENCY_value:
		case GL_TRIANGLE_STRIP_ADJACENCY_value:
			mb.getStatic(GLEnum.class, "GL_TRIANGLES_ADJACENCY");
			break;
		default:
			mb.op(Op.ACONST_NULL);
			break;
		}
		mb.op(Op.ARETURN);
	}
	
	private void buildPrimitiveVertexCount(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("primitiveVertexCount_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, int.class);
		switch (prim_mode.valueInt()) {
		case GL_POINTS_value:
			mb.constant(1);
			break;
		case GL_LINES_value:
		case GL_LINE_STRIP_value:
		case GL_LINE_LOOP_value:
			mb.constant(2);
			break;
		case GL_LINES_ADJACENCY_value:
		case GL_LINE_STRIP_ADJACENCY_value:
			mb.constant(4);
			break;
		case GL_TRIANGLES_value:
		case GL_TRIANGLE_STRIP_value:
		case GL_TRIANGLE_FAN_value:
			mb.constant(3);
			break;
		case GL_TRIANGLES_ADJACENCY_value:
		case GL_TRIANGLE_STRIP_ADJACENCY_value:
			mb.constant(6);
			break;
		default:
			mb.constant(0);
			break;
		}
		mb.op(Op.IRETURN);
	}
	
	private void buildPrimitiveCount(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("primitiveCount_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, int.class, int.class);
		mb.declareParamNames("count");
		switch (prim_mode.valueInt()) {
		case GL_POINTS_value:
			mb.iload("count");
			break;
		case GL_LINES_value:
			mb.iload("count");
			mb.constant(2);
			mb.op(Op.IDIV);
			break;
		case GL_LINE_STRIP_value:
		case GL_LINE_LOOP_value:
			// loop has special last case
			mb.iload("count");
			mb.constant(1);
			mb.op(Op.ISUB);
			break;
		case GL_LINES_ADJACENCY_value:
			mb.constant(4);
			mb.op(Op.IDIV);
			break;
		case GL_LINE_STRIP_ADJACENCY_value:
			// TODO line_strip_adjacency
			mb.constant(0);
			break;
		case GL_TRIANGLES_value:
			mb.iload("count");
			mb.constant(3);
			mb.op(Op.IDIV);
			break;
		case GL_TRIANGLE_STRIP_value:
		case GL_TRIANGLE_FAN_value:
			// fan has special last case
			mb.iload("count");
			mb.constant(2);
			mb.op(Op.ISUB);
			break;
		case GL_TRIANGLES_ADJACENCY_value:
			mb.iload("count");
			mb.constant(6);
			mb.op(Op.IDIV);
			break;
		case GL_TRIANGLE_STRIP_ADJACENCY_value:
			// TODO triangle_strip_adjacency
			mb.constant(0);
			break;
		default:
			mb.constant(0);
			break;
		}
		mb.op(Op.IRETURN);
	}
	
	/**
	 * Translate an IBO index to a real vertex index.
	 * 
	 * [int] -> [int]
	 * 
	 * @param mb
	 * @throws Exception
	 */
	private void buildVertexIndex(MethodBuilder mb) throws Exception {
		if (ibo_type == null) {
			// no ibo - do nothing
		} else {
			// load source address 
			mb.constant(DataFormat.sizeof(ibo_type));
			mb.op(Op.IMUL);
			mb.op(Op.I2L);
			mb.constant(ibo.address());
			mb.op(Op.LADD);
			// load vertex index from ibo
			DataFormat.buildGetInt(mb, ibo_type);
		}
	}
	
	/**
	 * TODO this needs testing
	 * 
	 * @param cb
	 * @throws Exception
	 */
	private void buildLoadPrimitive(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("loadPrimitive_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, void.class, long.class, int.class, int.class, int.class);
		mb.declareParamNames("p_out", "first", "count", "ip");
		switch (prim_mode.valueInt()) {
		case GL_POINTS_value:
			mb.lload("p_out");
			mb.iload("first");
			mb.iload("ip");
			mb.op(Op.IADD);
			buildVertexIndex(mb);
			DataFormat.buildPutInt(mb, GL_INT, true);
			break;
		case GL_LINES_value:
			for (int i = 0; i < 2; i++) {
				// target address
				mb.lload("p_out");
				mb.constant((long) (i * 4));
				mb.op(Op.LADD);
				// vertex index
				mb.iload("first");
				mb.iload("ip");
				mb.constant(2);
				mb.op(Op.IMUL);
				mb.op(Op.IADD);
				mb.constant(i);
				mb.op(Op.IADD);
				buildVertexIndex(mb);
				DataFormat.buildPutInt(mb, GL_INT, true);
			}
			break;
		case GL_LINE_STRIP_value:
		case GL_LINE_LOOP_value:
			// strip and loop have same normal case
			for (int i = 0; i < 2; i++) {
				// target address
				mb.lload("p_out");
				mb.constant((long) (i * 4));
				mb.op(Op.LADD);
				// vertex index
				mb.iload("first");
				mb.iload("ip");
				mb.op(Op.IADD);
				mb.constant(i);
				mb.op(Op.IADD);
				buildVertexIndex(mb);
				DataFormat.buildPutInt(mb, GL_INT, true);
			}
			break;
		case GL_LINES_ADJACENCY_value:
			for (int i = 0; i < 4; i++) {
				// target address
				mb.lload("p_out");
				mb.constant((long) (i * 4));
				mb.op(Op.LADD);
				// vertex index
				mb.iload("first");
				mb.iload("ip");
				mb.constant(4);
				mb.op(Op.IMUL);
				mb.op(Op.IADD);
				mb.constant(i);
				mb.op(Op.IADD);
				buildVertexIndex(mb);
				DataFormat.buildPutInt(mb, GL_INT, true);
			}
			break;
		case GL_LINE_STRIP_ADJACENCY_value:
			// TODO line_strip_adjacency
			break;
		case GL_TRIANGLES_value:
			for (int i = 0; i < 3; i++) {
				// target address
				mb.lload("p_out");
				mb.constant((long) (i * 4));
				mb.op(Op.LADD);
				// vertex index
				mb.iload("first");
				mb.iload("ip");
				mb.constant(3);
				mb.op(Op.IMUL);
				mb.op(Op.IADD);
				mb.constant(i);
				mb.op(Op.IADD);
				buildVertexIndex(mb);
				DataFormat.buildPutInt(mb, GL_INT, true);
			}
			break;
		case GL_TRIANGLE_STRIP_value:
			// odd primitive index?
			mb.scope().alloc(int.class, "odd");
			mb.iload("ip");
			mb.constant(1);
			mb.op(Op.IAND);
			mb.istore("odd");
			for (int i = 0; i < 3; i++) {
				// target address
				mb.lload("p_out");
				mb.constant((long) (i * 4));
				mb.op(Op.LADD);
				// vertex index
				mb.iload("first");
				mb.iload("ip");
				mb.op(Op.IADD);
				mb.constant(i);
				mb.op(Op.IADD);
				// for odd-index triangles, need to swap order of first 2 verts
				// in order for them all to have the same winding
				switch (i) {
				case 0:
					mb.iload("odd");
					mb.op(Op.IADD);
					break;
				case 1:
					mb.iload("odd");
					mb.op(Op.ISUB);
					break;
				default:
					break;
				}
				buildVertexIndex(mb);
				DataFormat.buildPutInt(mb, GL_INT, true);
			}
			break;
		case GL_TRIANGLE_FAN_value:
			for (int i = 0; i < 3; i++) {
				// target address
				mb.lload("p_out");
				mb.constant((long) (i * 4));
				mb.op(Op.LADD);
				// vertex index
				mb.iload("first");
				// first vertex is always the same
				if (i > 0) {
					mb.iload("ip");
					mb.op(Op.IADD);
					mb.constant(i);
					mb.op(Op.IADD);
				}
				buildVertexIndex(mb);
				DataFormat.buildPutInt(mb, GL_INT, true);
			}
			break;
		case GL_TRIANGLES_ADJACENCY_value:
			for (int i = 0; i < 6; i++) {
				// target address
				mb.lload("p_out");
				mb.constant((long) (i * 4));
				mb.op(Op.LADD);
				// vertex index
				mb.iload("first");
				mb.iload("ip");
				mb.constant(6);
				mb.op(Op.IMUL);
				mb.op(Op.IADD);
				mb.constant(i);
				mb.op(Op.IADD);
				buildVertexIndex(mb);
				DataFormat.buildPutInt(mb, GL_INT, true);
			}
			break;
		case GL_TRIANGLE_STRIP_ADJACENCY_value:
			// TODO triangle_strip_adjacency
			break;
		default:
			break;
		}
		mb.op(Op.RETURN);
	}
	
	private void buildLoadLastPrimitive(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("loadLastPrimitive_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, boolean.class, long.class, int.class, int.class);
		mb.declareParamNames("p_out", "first", "count");
		switch (prim_mode.valueInt()) {
		case GL_LINE_LOOP_value:
			for (int i = 0; i < 2; i++) {
				// target address
				mb.lload("p_out");
				mb.constant((long) (i * 4));
				mb.op(Op.LADD);
				// vertex index
				mb.iload("first");
				// first vertex from last index
				// second vertex from first index
				if (i == 0) {
					mb.iload("count");
					mb.op(Op.IADD);
					mb.constant(1);
					mb.op(Op.ISUB);
				}
				buildVertexIndex(mb);
				DataFormat.buildPutInt(mb, GL_INT, true);
			}
			// return true, primitive loaded
			mb.constant(1);
			mb.op(Op.IRETURN);
			break;
		case GL_TRIANGLE_FAN_value:
			for (int i = 0; i < 3; i++) {
				// target address
				mb.lload("p_out");
				mb.constant((long) (i * 4));
				mb.op(Op.LADD);
				// vertex index
				mb.iload("first");
				switch (i) {
				case 0:
					// first vertex from first index
					break;
				case 1:
					// second vertex from last index
					mb.iload("count");
					mb.op(Op.IADD);
					mb.constant(1);
					mb.op(Op.ISUB);
					break;
				case 2:
					// third vertex from second index
					mb.constant(1);
					mb.op(Op.IADD);
					break;
				default:
					break;
				}
				buildVertexIndex(mb);
				DataFormat.buildPutInt(mb, GL_INT, true);
			}
			// return true, primitive loaded
			mb.constant(1);
			mb.op(Op.IRETURN);
			break;
		default:
			// return false, primitive not loaded
			mb.constant(0);
			mb.op(Op.IRETURN);
			break;
		}
	}
	
	private void buildVertexSize(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("vertexSize_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, int.class);
		// vertex id, instance id
		int size = 16;
		// attributes
		for (int i = 0; i < attributes.length; i++) {
			Attribute a = attributes[i];
			if (a.enabled && a.vbo != null) {
				size = Math.max(size, 32 + 16 * i);
			}
		}
		mb.constant(size);
		mb.op(Op.IRETURN);
	}
	
	private void buildLoadVertex(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("loadVertex_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, void.class, long.class, int.class);
		mb.declareParamNames("p_out", "iv");
		// first loc is vertex id, instance id
		// put vertex id
		mb.lload("p_out");
		mb.iload("iv");
		DataFormat.buildPutInt(mb, GL_INT, true);
		// put instance id (always 0 atm)
		mb.lload("p_out");
		mb.constant(4L);
		mb.op(Op.LADD);
		mb.constant(0);
		DataFormat.buildPutInt(mb, GL_INT, true);
		// decode other attributes into successive locs
		for (int i = 0; i < attributes.length; i++) {
			Attribute a = attributes[i];
			// TODO default values for disabled attribs?
			if (a.enabled && a.vbo != null) {
				// load attribute target address
				mb.lload("p_out");
				mb.constant(16L + 16L * i);
				mb.op(Op.LADD);
				// load components
				for (int j = 0; j < a.components; j++) {
					// load component target address
					mb.op(Op.DUP2);
					mb.constant(4L * j);
					mb.op(Op.LADD);
					// load component source address
					mb.constant(a.vbo.address() + a.offset + j * DataFormat.sizeof(a.type));
					mb.iload("iv");
					mb.constant(a.stride > 0 ? a.stride : a.components * DataFormat.sizeof(a.type));
					mb.op(Op.IMUL);
					mb.op(Op.I2L);
					mb.op(Op.LADD);
					// decode component
					if (a.integer) {
						DataFormat.buildGetInt(mb, a.type);
						DataFormat.buildPutInt(mb, GL_INT, true);
					} else {
						DataFormat.buildGetFloat(mb, a.type, a.normalized);
						DataFormat.buildPutFloat(mb, GL_FLOAT, false);
					}
				}
				// load default value 0 for remaining components, except the last
				for (int j = a.components; j < 3; j++) {
					// load component target address
					mb.op(Op.DUP2);
					mb.constant(4L * j);
					mb.op(Op.LADD);
					// constant value
					if (a.integer) {
						mb.constant(0);
						DataFormat.buildPutInt(mb, GL_INT, true);
					} else {
						// yes, 0.f is bitwise equal to 0
						// but to be clear, lets do it properly
						mb.constant(0.f);
						DataFormat.buildPutFloat(mb, GL_FLOAT, false);
					}
				}
				// load default value 1 (homogeneous coords) for last component if needed
				if (a.components < 4) {
					// load component target address
					mb.op(Op.DUP2);
					mb.constant(4L * 3);
					mb.op(Op.LADD);
					// constant value
					if (a.integer) {
						mb.constant(1);
						DataFormat.buildPutInt(mb, GL_INT, true);
					} else {
						// 1.f is NOT bitwise equal to 1
						mb.constant(1.f);
						DataFormat.buildPutFloat(mb, GL_FLOAT, false);
					}
				}
				mb.op(Op.POP2);
			}
		}
		mb.op(Op.RETURN);
	}
	
}
