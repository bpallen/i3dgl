package initial3d.detail.gl;

import java.util.Arrays;

public class BlendBufferState implements Cloneable {
	
	BlendChannelState[] channels = new BlendChannelState[4];
	
	BlendBufferState() {
		for (int i = 0; i < 4; i++) {
			channels[i] = new BlendChannelState();
		}
	}
	
	@Override
	public BlendBufferState clone() {
		BlendBufferState s = new BlendBufferState();
		for (int i = 0; i < 4; i++) {
			s.channels[i] = channels[i].clone();
		}
		return s;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(channels);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		BlendBufferState other = (BlendBufferState) obj;
		if (!Arrays.equals(channels, other.channels)) return false;
		return true;
	}
	
	@Override
	public String toString() {
		BlendChannelState r = channels[0];
		BlendChannelState g = channels[1];
		BlendChannelState b = channels[2];
		BlendChannelState a = channels[3];
		if (r.equals(g) && g.equals(b) && b.equals(a)) {
			return "BlendBuffer[rgba= " + r + "]";
		} else if (r.equals(g) && g.equals(b)) {
			return String.format("BlendBuffer[rgb= %s, a= %s]", r, a);
		} else {
			return String.format("BlendBuffer[r= %s, g= %s, b= %s, a= %s]", r, g, b, a);
		}
	}
}