package initial3d.detail.gl;

import initial3d.Functions;
import initial3d.Mat4f;

import static initial3d.detail.Util.*;

public class OldIdentityShaderProgram extends OldShaderProgram {

	private final long puniforms = sharedAlloc(16 * 4);
	
	public void modelViewProjection(Mat4f m) {
		putf(puniforms, m);
	}
	
	@Override
	public int vertexOutSize() {
		return 32;
	}

	@Override
	public void vertexShade(long pvert_in, long pvert_out) {
		// read position from first attribute
		//System.out.println("v0: " + unsafe.getFloat(pvert_in + 16));
		//System.out.println("v1: " + unsafe.getFloat(pvert_in + 20));
		//System.out.println("v2: " + unsafe.getFloat(pvert_in + 24));
		//System.out.println("v3: " + unsafe.getFloat(pvert_in + 28));
		
		unsafe.putFloat(pvert_in + 28, 1.f);
		mulMat4Vec4(pvert_out, puniforms, pvert_in + 16);
		
		// get vertex id
		int vid = unsafe.getInt(pvert_in);
		// write a color
		switch (vid % 3) {
		case 0:
			putf(pvert_out + 16, 1, 0, 0, 1);
			break;
		case 1:
			putf(pvert_out + 16, 0, 1, 0, 1);
			break;
		case 2:
			putf(pvert_out + 16, 0, 0, 1, 1);
			break;
		default:
			break;
		}
	}

	@Override
	public void fragmentShade(long pinterp_in, float z, float w, long pfrag_out) {
		float f0 = w * unsafe.getFloat(pinterp_in);
		float f1 = w * unsafe.getFloat(pinterp_in + 4);
		float f2 = w * unsafe.getFloat(pinterp_in + 8);
		unsafe.putFloat(pfrag_out, Functions.clamp(f0, 0, 1));
		unsafe.putFloat(pfrag_out + 4, Functions.clamp(f1, 0, 1));
		unsafe.putFloat(pfrag_out + 8, Functions.clamp(f2, 0, 1));
		unsafe.putFloat(pfrag_out + 12, 1.f);
	}

}
