package initial3d.detail.gl;

import initial3d.GLEnum;
import initial3d.detail.Access;
import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.StringInfoLog;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.preprocessor.Preprocessor;
import initial3d.detail.glsl.tokens.Tokenizer;

import static initial3d.GLEnum.*;
import static initial3d.GLEnumValues.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

public class Shader extends Resource {

	private StringInfoLog infolog = new StringInfoLog();
	private GLEnum shader_type = null;
	private String[] sources = null;
	private Scope globalscope = null;
	
	public Shader(I3DGL gl_, int name_) {
		super(gl_, name_);
	}

	public int parami(GLEnum pname) {
		gl().validateEnum("pname", pname, GL_COMPILE_STATUS, GL_INFO_LOG_LENGTH);
		switch (pname.valueInt()) {
		case GL_COMPILE_STATUS_value:
			return globalscope != null ? 1 : 0;
		case GL_INFO_LOG_LENGTH_value:
			return infolog.str().length() + 1;
		default:
			return 0;
		}
	}
	
	public String infoLog() {
		return infolog.str();
	}
	
	public GLEnum shaderType() {
		return shader_type;
	}
	
	public void shaderType(GLEnum stype) {
		shader_type = stype;
	}
	
	public void source(String ...strings) {
		sources = strings.clone();
	}
	
	public void compile() {
		infolog = new StringInfoLog();
		globalscope = null;
		Preprocessor pp = new Preprocessor(infolog);
		String envheader = "";
		switch (shader_type.valueInt()) {
		case GL_VERTEX_SHADER_value:
			envheader = "#pragma i3d_include <vertex>\n";
			break;
		case GL_GEOMETRY_SHADER_value:
			envheader = "#pragma i3d_include <geometry>\n";
			break;
		case GL_FRAGMENT_SHADER_value:
			envheader = "#pragma i3d_include <fragment>\n";
			break;
		default:
			throw gl().throwInvalidOperation("bad shader type %s", shader_type);
		}
		pp.addInput(new Tokenizer(infolog, "env", new StringReader(envheader)));
		for (int i = 0; i < sources.length; i++) {
			String source = sources[i];
			Tokenizer tok = new Tokenizer(infolog, "input" + i, new StringReader(source));
			pp.addInput(tok);
		}
		Parser p = new Parser(pp);
		try {
			p.parseAll();
		} catch (BuildException e) {
			// normal compilation failure
			infolog.error("error encountered, aborting");
		} catch (Exception e) {
			infolog.internalError("exception encountered while compiling: " + e.getMessage());
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			infolog.detail(sw.toString());
			try {
				infolog.detail("see approximate location of problematic input", pp.peekToken());
			} catch (IOException e1) {
				// .
			}
		}
		if (infolog.errorCount() > 0) {
			infolog.info(String.format(
				"Shader compilation for %s failed with %d errors and %d warnings",
				shader_type, infolog.errorCount(), infolog.warningCount())
			);
			gl().debugAPIInfo("compilation failed for shader %d", name());
			return;
		} else {
			globalscope = p.globalScope();
		}
	}
	
	public Scope globalScope() {
		return globalscope;
	}
	
	@Override
	public void prepare(Command c, Access a) {
		
	}

}
