package initial3d.detail.gl;

import java.util.Arrays;

import initial3d.Vec4f;

public class BlendState implements Cloneable {
	
	Vec4f blend_color = Vec4f.zero;
	BlendBufferState[] buffers = new BlendBufferState[8];
	
	BlendState() {
		for (int i = 0; i < 8; i++) {
			buffers[i] = new BlendBufferState();
		}
	}
	
	@Override
	public BlendState clone() {
		BlendState s = new BlendState();
		s.blend_color = blend_color;
		for (int i = 0; i < 8; i++) {
			s.buffers[i] = buffers[i].clone();
		}
		return s;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((blend_color == null) ? 0 : blend_color.hashCode());
		result = prime * result + Arrays.hashCode(buffers);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		BlendState other = (BlendState) obj;
		if (blend_color == null) {
			if (other.blend_color != null) return false;
		} else if (!blend_color.equals(other.blend_color)) return false;
		if (!Arrays.equals(buffers, other.buffers)) return false;
		return true;
	}
	
	@Override
	public String toString() {
		boolean all_same = true;
		for (int i = 0; i < 7; i++) {
			all_same &= buffers[i].equals(buffers[i+ 1]);
		}
		if (all_same) {
			return "Blend[color=" + blend_color + ", " + buffers[0] + "]";
		} else {
			StringBuffer sb = new StringBuffer();
			sb.append("Blend[color=");
			sb.append(blend_color);
			sb.append(", ");
			for (int i = 0; i < 8; i++) {
				sb.append(i);
				sb.append("= ");
				sb.append(buffers[i]);
				if (i < 7) sb.append(", ");
			}
			sb.append("]");
			return sb.toString();
		}
	}
}








