package initial3d.detail.gl;

public interface BindState {

	/**
	 * Get resource bound to binding point. Return null if none or not supported.
	 * 
	 * @param bp
	 * @return
	 */
	public Resource boundResource(BindPoint bp);
	
	/**
	 * Bind resource to binding point. Return false if not supported.
	 * If resource is null, remove binding. 
	 * 
	 * @param bp
	 * @param name
	 */
	public boolean bindResource(BindPoint bp, Resource r);
	
	/**
	 * Ensure the supplied resource is not bound.
	 * 
	 * @param name
	 */
	public void unbindResource(Resource r);
	
}
