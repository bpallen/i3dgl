package initial3d.detail.gl;

import static java.lang.Math.max;
import static java.lang.Math.min;

import initial3d.detail.Util;
import sun.misc.Unsafe;

/**
 * 
 * @author ben
 *
 */
public class Rasterizer {

	private static final Unsafe unsafe = Util.unsafe;
	
	public static int blockBits() {
		return 3;
	}
	
	public static int blockSize() {
		return 1 << blockBits();
	}
	
	public static int blockTexels() {
		return blockSize() * blockSize();
	}
	
	/**
	 * Mask that zeros out the lowest {@link #blockBits()} bits.
	 */
	public static int blockMask() {
		return ~(blockSize() - 1);
	}
	
	public static int blockRoundUp(int i) {
		return (i + blockSize() - 1) & blockMask();
	}
	
	public static int rasterWorkerIndex(int x, int y, int z, int num_workers) {
		// TODO randomized?
		return ((x >>> blockBits()) + (y >>> blockBits()) + z) % num_workers;
	}
	
	/**
	 * <p>
	 * Rasterizes a triangle (but does not shade fragments or write to the framebuffer).
	 * Writes interpolation setup data and block coverage masks for the triangles bounding rect.
	 * Block coverage masks are grouped by quads, so the lowest 4 bits are the
	 * coverage for the bottom-left quad.
	 * </p>
	 * 
	 * <p>
	 * <a href="http://forum.devmaster.net/t/advanced-rasterization/6145">Original algorithm (http://forum.devmaster.net/t/advanced-rasterization/6145).</a>
	 * The original implementation this was based on uses y-down coords, we use y-up.
	 * </p>
	 * 
	 * Raster block buffer structure:
	 * <pre>
	 * - [short4] bounding rect, block coords
	 * - [long] worker thread flags
	 * - [float2] origin iw, z
	 * - [float2] diw, dz dx
	 * - [float2] diw, dz dy
	 * - [varyings] origin varyings
	 * - [varyings] varyings dx
	 * - [varyings] varyings dy
	 * - [long...] block coverage masks*
	 * TODO faceness, other per-prim vars?
	 * </pre>
	 * 
	 * @return bytes written to <code>prasterblock</code> or &lt; 0 if not enough space
	 * (0 is valid meaning nothing needed to be written)
	 */
	public static long rasterizeTriangle(
		int num_workers,
		long prasterblock_next,
		final long prasterblock_limit,
		long ptemp,
		FramebufferAccessor fba,
		ShaderProgramAccessor spa,
		long pv0,
		long pv1,
		long pv2,
		int vp_x,
		int vp_y,
		int vp_w,
		int vp_h,
		boolean frontface,
		float halflinewidth
	) {
		
		// TODO some of the variable names in here are atrocious
		
		// block size
		final int q = blockSize();
		final int qmask = blockMask();
		
		// storage
		final int vsz = spa.geometryOutSize() - 16;
		final long prasterblock_base = prasterblock_next;
		final long pvary_origin = prasterblock_next + 40;
		final long pvary_ddx = pvary_origin + vsz;
		final long pvary_ddy = pvary_ddx + vsz;
		prasterblock_next = pvary_ddy + vsz;
		if (prasterblock_next + 8 > prasterblock_limit) return -1;
		
		// w values for perspective division
		float w0f = Util.unsafe.getFloat(pv0 + 12);
		float w1f = Util.unsafe.getFloat(pv1 + 12);
		float w2f = Util.unsafe.getFloat(pv2 + 12);
		
		// 1/w for perspective correct interpolation
		float iw0 = 1.f / w0f;
		float iw1 = 1.f / w1f;
		float iw2 = 1.f / w2f;
		
		// float pixel positions (homogenized)
		// -0.5 offset because we should test against pixel centres but the rasterizer tests bottom-left pixel corners
		final float hscale = 0.5f * vp_w;
		final float vscale = 0.5f * vp_h;
		final float hoffset = vp_x - 0.5f;
		final float voffset = vp_y - 0.5f;
		float x0f = (iw0 * Util.unsafe.getFloat(pv0) + 1.f) * hscale + hoffset;
		float y0f = (iw0 * Util.unsafe.getFloat(pv0 + 4) + 1.f) * vscale + voffset;
		float z0f = iw0 * Util.unsafe.getFloat(pv0 + 8);
		float x1f = (iw1 * Util.unsafe.getFloat(pv1) + 1.f) * hscale + hoffset;
		float y1f = (iw1 * Util.unsafe.getFloat(pv1 + 4) + 1.f) * vscale + voffset;
		float z1f = iw1 * Util.unsafe.getFloat(pv1 + 8);
		float x2f = (iw2 * Util.unsafe.getFloat(pv2) + 1.f) * hscale + hoffset;
		float y2f = (iw2 * Util.unsafe.getFloat(pv2 + 4) + 1.f) * vscale + voffset;
		float z2f = iw2 * Util.unsafe.getFloat(pv2 + 8);
		
		// float pixel deltas
		float dx01f = x1f - x0f;
		float dy01f = y1f - y0f;
		float dx12f = x2f - x1f;
		float dy12f = y2f - y1f;
		float dx20f = x0f - x2f;
		float dy20f = y0f - y2f;
		
		// adjustments for drawing lines
		float imd12 = 1.f / (float) Math.sqrt(dx12f * dx12f + dy12f * dy12f);
		float lwddx = -dy12f * imd12 * halflinewidth;
		float lwddy = dx12f * imd12 * halflinewidth;
		x0f += lwddx;
		y0f += lwddy;
		x1f += -lwddx;
		y1f += -lwddy;
		x2f += -lwddx;
		y2f += -lwddy;
		
		// swap vertex order if backface to make half-spaces behave
		if (!frontface) {
			float xxf = x1f;
			float yyf = y1f;
			float zzf = z1f;
			float wwf = w1f;
			long pvv = pv1;
			x1f = x2f;
			y1f = y2f;
			z1f = z2f;
			w1f = w2f;
			pv1 = pv2;
			x2f = xxf;
			y2f = yyf;
			z2f = zzf;
			w2f = wwf;
			pv2 = pvv;
		}
		
		// float pixel deltas after adjustments
		dx01f = x1f - x0f;
		dy01f = y1f - y0f;
		dx12f = x2f - x1f;
		dy12f = y2f - y1f;
		dx20f = x0f - x2f;
		dy20f = y0f - y2f;
		
		// use 28.4 fixed point for pixel positions
		final int x0 = (int) (x0f * 16.f + 0.5f);
		final int y0 = (int) (y0f * 16.f + 0.5f);
		final int x1 = (int) (x1f * 16.f + 0.5f);
		final int y1 = (int) (y1f * 16.f + 0.5f);
		final int x2 = (int) (x2f * 16.f + 0.5f);
		final int y2 = (int) (y2f * 16.f + 0.5f);
		
		// bounding rectangle, clamped to viewport
		// also skip if no visible area
		// add 0xF to the _min_ calculation too because here we're testing lower-left pixel corners
		int minx = (min(x0, min(x1, x2)) + 0xF) >> 4;
		int maxx = (max(x0, max(x1, x2)) + 0xF) >> 4;
		minx = max(minx, vp_x);
		maxx = min(maxx, vp_x + vp_w);
		if (minx >= maxx) return 0;
		int miny = (min(y0, min(y1, y2)) + 0xF) >> 4;
		int maxy = (max(y0, max(y1, y2)) + 0xF) >> 4;
		miny = max(miny, vp_y);
		maxy = min(maxy, vp_y + vp_h);
		if (miny >= maxy) return 0;
		
		// force start in corner of block
		final int minbx = minx & qmask;
		final int minby = miny & qmask;
		
		// number of blocks in bounding box
		final int xblocks = (maxx - minbx + q - 1) >> blockBits();
		final int yblocks = (maxy - minby + q - 1) >> blockBits();
		
		// check bounds against buffer space
		final long bufsizereq = prasterblock_next + 8 * xblocks * yblocks - prasterblock_base;
		if (prasterblock_base + bufsizereq > prasterblock_limit) return -1;
		unsafe.putShort(prasterblock_base + 0, (short) (minbx >> blockBits()));
		unsafe.putShort(prasterblock_base + 2, (short) (minby >> blockBits()));
		unsafe.putShort(prasterblock_base + 4, (short) xblocks);
		unsafe.putShort(prasterblock_base + 6, (short) yblocks);
		
		// barycentric things
		// TODO if points are colinear, idet is probably inf; do we need to deal with this?
		float idet = 1.f / (-dy12f * (x0f - x2f) + dx12f * (y0f - y2f));
		
		// compute barycentric coords at bottom left of triangle's bounding box +1px either way
		float l00_0 = (-dy12f * (minbx - x2f) + dx12f * (minby - y2f)) * idet;
		float l00_1 = (-dy20f * (minbx - x2f) + dx20f * (minby - y2f)) * idet;
		float l00_2 = 1.f - l00_1 - l00_0;
		float l10_0 = (-dy12f * (minbx + 1.f - x2f) + dx12f * (minby - y2f)) * idet;
		float l10_1 = (-dy20f * (minbx + 1.f - x2f) + dx20f * (minby - y2f)) * idet;
		float l10_2 = 1.f - l10_1 - l10_0;
		float l01_0 = (-dy12f * (minbx - x2f) + dx12f * (minby + 1.f - y2f)) * idet;
		float l01_1 = (-dy20f * (minbx - x2f) + dx20f * (minby + 1.f - y2f)) * idet;
		float l01_2 = 1.f - l01_1 - l01_0;
		
		// 1/w barycentric interpolation to bottom left
		float iw00 = l00_0 * iw0 + l00_1 * iw1 + l00_2 * iw2;
		float iw10 = l10_0 * iw0 + l10_1 * iw1 + l10_2 * iw2;
		float iw01 = l01_0 * iw0 + l01_1 * iw1 + l01_2 * iw2;
		
		// 1/w deltas
		float diw_dx = iw10 - iw00;
		float diw_dy = iw01 - iw00;
		
		// z barycentric interpolation to bottom left
		// we can linearly interpolate ndc z, because we've already divided it by w
		final float z00 = l00_0 * z0f + l00_1 * z1f + l00_2 * z2f;
		final float z10 = l10_0 * z0f + l10_1 * z1f + l10_2 * z2f;
		final float z01 = l01_0 * z0f + l01_1 * z1f + l01_2 * z2f;
		
		// z deltas
		final float dz_dx = z10 - z00;
		final float dz_dy = z01 - z00;
		
		// record iw, z origins and deltas
		unsafe.putFloat(prasterblock_base + 16, iw00);
		unsafe.putFloat(prasterblock_base + 20, z00);
		unsafe.putFloat(prasterblock_base + 24, diw_dx);
		unsafe.putFloat(prasterblock_base + 28, dz_dx);
		unsafe.putFloat(prasterblock_base + 32, diw_dy);
		unsafe.putFloat(prasterblock_base + 36, dz_dy);
		
		// varying interpolation to bottom left
		spa.varyingInterpolate3(pvary_origin, pv0 + 16, pv1 + 16, pv2 + 16, l00_0, l00_1, l00_2, iw0, iw1, iw2);
		spa.varyingInterpolate3(pvary_ddx, pv0 + 16, pv1 + 16, pv2 + 16, l10_0, l10_1, l10_2, iw0, iw1, iw2);
		spa.varyingInterpolate3(pvary_ddy, pv0 + 16, pv1 + 16, pv2 + 16, l01_0, l01_1, l01_2, iw0, iw1, iw2);
		
		// varying deltas
		spa.varyingSub(pvary_ddx, pvary_ddx, pvary_origin);
		spa.varyingSub(pvary_ddy, pvary_ddy, pvary_origin);
		
		// deltas 28.4
		final int dx01 = x1 - x0;
		final int dx12 = x2 - x1;
		final int dx20 = x0 - x2;
		final int dy01 = y1 - y0;
		final int dy12 = y2 - y1;
		final int dy20 = y0 - y2;
		
		// deltas 24.8, because multiplying two 28.4's gives a 24.8
		final int fdx01 = dx01 << 4;
		final int fdx12 = dx12 << 4;
		final int fdx20 = dx20 << 4;
		final int fdy01 = dy01 << 4;
		final int fdy12 = dy12 << 4;
		final int fdy20 = dy20 << 4;
		
		// half-edge constants 24.8
		// -1 so that we can use >= for testing, i.e. the sign bit
		int c0 = dy01 * x0 - dx01 * y0 - 1;
		int c1 = dy12 * x1 - dx12 * y1 - 1;
		int c2 = dy20 * x2 - dx20 * y2 - 1;
		
		// correct for fill convention
		// this uses bottom-left fill, as whichever horizontal edge is filled,
		// the block scan has to include that line of pixels. if we did top-fill,
		// we'd probably have to change the pixel coords to be top-left too.
		if (dy01 < 0 || (dy01 == 0 && dx01 > 0)) c0++;
		if (dy12 < 0 || (dy12 == 0 && dx12 > 0)) c1++;
		if (dy20 < 0 || (dy20 == 0 && dx20 > 0)) c2++;
		
		// record worker threads that need to process this set of blocks
		long workermask = 0;
		
		// loop through blocks
		for (int y = minby; y < maxy; y += q) {
			
			for (int x = minbx; x < maxx; x += q) {
				
				// corners of block 28.4
				// -1 because we want the corners _inside_ this block (lower-left corner testing)
				final int bx0 = x << 4;
				final int bx1 = (x + q - 1) << 4;
				final int by0 = y << 4;
				final int by1 = (y + q - 1) << 4;
				
				// eval half-space functions
				// TODO we could eval each half-space at only one (different) corner and interpolate the results
				// TODO what does that ^ comment mean?
				final int a00 = c0 + dx01 * by0 - dy01 * bx0 >= 0 ? 1 : 0;
				final int a10 = c0 + dx01 * by0 - dy01 * bx1 >= 0 ? 2 : 0;
				final int a01 = c0 + dx01 * by1 - dy01 * bx0 >= 0 ? 4 : 0;
				final int a11 = c0 + dx01 * by1 - dy01 * bx1 >= 0 ? 8 : 0;
				final int a = a00 | a10 | a01 | a11;
				
				final int b00 = c1 + dx12 * by0 - dy12 * bx0 >= 0 ? 1 : 0;
				final int b10 = c1 + dx12 * by0 - dy12 * bx1 >= 0 ? 2 : 0;
				final int b01 = c1 + dx12 * by1 - dy12 * bx0 >= 0 ? 4 : 0;
				final int b11 = c1 + dx12 * by1 - dy12 * bx1 >= 0 ? 8 : 0;
				final int b = b00 | b10 | b01 | b11;
				
				final int c00 = c2 + dx20 * by0 - dy20 * bx0 >= 0 ? 1 : 0;
				final int c10 = c2 + dx20 * by0 - dy20 * bx1 >= 0 ? 2 : 0;
				final int c01 = c2 + dx20 * by1 - dy20 * bx0 >= 0 ? 4 : 0;
				final int c11 = c2 + dx20 * by1 - dy20 * bx1 >= 0 ? 8 : 0;
				final int c = c00 | c10 | c01 | c11;
				
				// skip block when outside an edge
				if (a == 0 || b == 0 || c == 0) {
					// write nocover mask
					unsafe.putLong(prasterblock_next, 0);
					prasterblock_next += 8;
					continue;
				}
				
				// record which workers must touch this triangle
				workermask |= 1L << rasterWorkerIndex(x, y, 0, num_workers);
				
				final boolean fullcover = (a & b & c) == 0xF;
				
				// accept whole block when totally covered
				if (fullcover) {
					// write fullcover mask
					unsafe.putLong(prasterblock_next, -1);
					prasterblock_next += 8;
					continue;
				}
				
				// otherwise, partially covered block
				if (true) {
					
					// within-block bounds
					// for a fully covered block, these should always be 0 and q
					// for partially covered blocks, only scanning this region improves performance
					// (a little, or it did at one point anyway)
					// round min down to quad-alignment
					final int minxx = max(0, minx - x) & ~1;
					final int maxxx = min(q, maxx - x);
					final int minyy = max(0, miny - y) & ~1;
					final int maxyy = min(q, maxy - y);
					
					// coverage mask for this block
					long coverage = 0;
					
					// interpolation setup for half-space functions for quad y increment
					final int bby0 = by0 + (minyy << 4);
					final int bbx0 = bx0 + (minxx << 4);
					int cy0 = c0 + dx01 * bby0 - dy01 * bbx0;
					int cy1 = c1 + dx12 * bby0 - dy12 * bbx0;
					int cy2 = c2 + dx20 * bby0 - dy20 * bbx0;
					
					for (int yy = minyy; yy < maxyy; yy += 2) {
						
						// interpolation setup for quad x increment
						int cx0 = cy0;
						int cx1 = cy1;
						int cx2 = cy2;
						
						// interpolation for quad y increment
						cy0 += fdx01 + fdx01;
						cy1 += fdx12 + fdx12;
						cy2 += fdx20 + fdx20;
						
						for (int xx = minxx; xx < maxxx; xx += 2) {
							
							// TODO optimize - or then shift
							// coverage testing
							// these expressions or the sign bits together, giving 1 if any half-space was < 0
							// this is why we subtracted 1 from the half-space constants
							int discard = 0;
							// test 0
							discard |= ((cx0 >>> 31) | (cx1 >>> 31) | (cx2 >>> 31)) << 0;
							// y inc
							cx0 += fdx01;
							cx1 += fdx12;
							cx2 += fdx20;
							// test 2
							discard |= ((cx0 >>> 31) | (cx1 >>> 31) | (cx2 >>> 31)) << 2;
							// x inc
							cx0 -= fdy01;
							cx1 -= fdy12;
							cx2 -= fdy20;
							// test 3
							discard |= ((cx0 >>> 31) | (cx1 >>> 31) | (cx2 >>> 31)) << 3;
							// y dec
							cx0 -= fdx01;
							cx1 -= fdx12;
							cx2 -= fdx20;
							// test 1
							discard |= ((cx0 >>> 31) | (cx1 >>> 31) | (cx2 >>> 31)) << 1;
							// x inc; this leaves the half-space interpolation at the start point for the next quad
							cx0 -= fdy01;
							cx1 -= fdy12;
							cx2 -= fdy20;
							
							// interpolation for quad x increment
							// we took care of the half-spaces during coverage testing
							
							// add to coverage mask
							coverage |= ((long) (~discard & 0xF)) << (yy * 8 + xx * 2);
							
						}
						
					}
					
					// write coverage mask
					unsafe.putLong(prasterblock_next, coverage);
					prasterblock_next += 8;
					
				} // fullcover?
				
			} // for each block col
			
		} // for each block row
		
		// write worker thread mask
		unsafe.putLong(prasterblock_base + 8, workermask);
		
		return bufsizereq;
	}
	
	/**
	 * Take sets of rasterized blocks, execute fragment shaders and write to the framebuffer.
	 * 
	 * @param worker_id
	 * @param num_workers
	 * @param fba
	 * @param spa
	 * @param prasterblock_beg
	 * @param prasterblock_end
	 * @param puniforms
	 * @param ptemp
	 */
	public static void commitRasterBlocks(
		int worker_id,
		int num_workers,
		FramebufferAccessor fba,
		ShaderProgramAccessor spa,
		long prasterblock_beg,
		long prasterblock_end,
		long puniforms,
		long ptemp
	) {
		
		final int q = 8;
		
		for (long prasterblock = prasterblock_beg; prasterblock < prasterblock_end; ) {
			
			// bounds
			final int minbx = unsafe.getShort(prasterblock + 0) << blockBits();
			final int minby = unsafe.getShort(prasterblock + 2) << blockBits();
			final int xblocks = unsafe.getShort(prasterblock + 4);
			final int yblocks = unsafe.getShort(prasterblock + 6);
			final int maxx = minbx + xblocks * q;
			final int maxy = minby + yblocks * q;
			
			// input storage (we can't modify these! they are shared)
			final int vsz = spa.geometryOutSize() - 16;
			final long pvary_origin = prasterblock + 40;
			final long pvary_ddx = pvary_origin + vsz;
			final long pvary_ddy = pvary_ddx + vsz;
			final long pcoverage_base = pvary_ddy + vsz;
			
			// test if this worker can skip this set of blocks
			final long workermask = unsafe.getLong(prasterblock + 8);
			if (((workermask >>> worker_id) & 1) == 0) {
				prasterblock = pcoverage_base + xblocks * yblocks * 8;
				continue;
			}
			
			// local storage
			final long pblocktemp = ptemp;
			final long pvary_ddqx = pblocktemp + 128;
			final long pvary_ddqy = pvary_ddqx + vsz;
			final long pvary_ddbx = pvary_ddqy + vsz;
			final long pvary_ddby = pvary_ddbx + vsz;
			final long pvary_interp_bx = pvary_ddby + vsz;
			final long pvary_interp_by = pvary_interp_bx + vsz;
			final long pvary_interp_qx = pvary_interp_by + vsz;
			final long pvary_interp_qy = pvary_interp_qx + vsz;
			final long pfrag = pvary_interp_qy + vsz;
			// TODO frag out limits?
			final long ptemp_next = pfrag + 64 * 10;
			
			// get iw, z origins and deltas
			final float iw00 = unsafe.getFloat(prasterblock + 16);
			final float z00 = unsafe.getFloat(prasterblock + 20);
			final float diw_dx = unsafe.getFloat(prasterblock + 24);
			final float dz_dx = unsafe.getFloat(prasterblock + 28);
			final float diw_dy = unsafe.getFloat(prasterblock + 32);
			final float dz_dy = unsafe.getFloat(prasterblock + 36);
			
			// extra deltas (blocks and quads)
			final float diw_dbx = diw_dx * q;
			final float diw_dby = diw_dy * q;
			final float dz_dbx = dz_dx * q;
			final float dz_dby = dz_dy * q;
			spa.varyingMul(pvary_ddbx, pvary_ddx, q);
			spa.varyingMul(pvary_ddby, pvary_ddy, q);
			spa.varyingMul(pvary_ddqx, pvary_ddx, 2.f);
			spa.varyingMul(pvary_ddqy, pvary_ddy, 2.f);
			
			// interpolation setup for block y increment
			float iw_interp_by = iw00;
			float z_interp_by = z00;
			spa.varyingCopy(pvary_interp_by, pvary_origin);
			
			// loop through blocks
			long pcoverage_next = pcoverage_base;
			for (int y = minby; y < maxy; y += q) {
				
				// interpolation setup for block x increment
				float iw_interp_bx = iw_interp_by;
				float z_interp_bx = z_interp_by;
				spa.varyingCopy(pvary_interp_bx, pvary_interp_by);
				
				// interpolation for block y increment
				iw_interp_by += diw_dby;
				z_interp_by += dz_dby;
				spa.varyingAdd(pvary_interp_by, pvary_interp_by, pvary_ddby);
				
				for (int x = minbx; x < maxx; x += q) {
					
					// interpolation setup for quad y increment
					// move to qy and qx start within block
					float iw_interp_qy = iw_interp_bx;
					float z_interp_qy = z_interp_bx;
					spa.varyingCopy(pvary_interp_qy, pvary_interp_bx);
					
					// interpolation for block x increment
					// this has to happen before any possible block discard!
					iw_interp_bx += diw_dbx;
					z_interp_bx += dz_dbx;
					spa.varyingAdd(pvary_interp_bx, pvary_interp_bx, pvary_ddbx);
					
					// read block coverage
					// also has to happen before block discard
					final long coverage = unsafe.getLong(pcoverage_next);
					pcoverage_next += 8;
					
					// skip empty blocks
					if (coverage == 0) continue;
					
					// don't process blocks destined for another thread
					// TODO layer
					if (rasterWorkerIndex(x, y, 0, num_workers) != worker_id) continue;
					
					final boolean fullcover = coverage == -1;
					if (!fba.setupBlock(pblocktemp, x, y, 0, worker_id, fullcover)) continue;
					
					// accept whole block when totally covered
					if (fullcover) {
						
						for (int yy = 0; yy < q; yy += 2) {
							
							// interpolation setup for quad x increment
							float iw_interp_qx = iw_interp_qy;
							float z_interp_qx = z_interp_qy;
							spa.varyingCopy(pvary_interp_qx, pvary_interp_qy);
							
							// interpolation for quad y increment
							iw_interp_qy += diw_dy + diw_dy;
							z_interp_qy += dz_dy + dz_dy;
							spa.varyingAdd(pvary_interp_qy, pvary_interp_qy, pvary_ddqy);
							
							// apparently unrolling this makes it slower atm
							// what appears to happen is that the last 3 (of 16) calls to writefrag don't get inlined
							// but a half-unroll might still be interesting...
							for (int xx = 0; xx < q; xx += 2) {
								
								int accept = spa.shadeFragmentQuad(
									pfrag, ptemp_next, puniforms, pvary_interp_qx, pvary_ddx, pvary_ddy,
									x + xx, y + yy,
									z_interp_qx, dz_dx, dz_dy,
									iw_interp_qx, diw_dx, diw_dy
								);
								
								// FIXME write correct z values
								fba.writeFragment((accept & 0b0001) >>> 0, xx + 0, yy + 0, z_interp_qx, pblocktemp, pfrag);
								fba.writeFragment((accept & 0b0010) >>> 1, xx + 1, yy + 0, z_interp_qx, pblocktemp, pfrag + 4);
								fba.writeFragment((accept & 0b0100) >>> 2, xx + 0, yy + 1, z_interp_qx, pblocktemp, pfrag + 8);
								fba.writeFragment((accept & 0b1000) >>> 3, xx + 1, yy + 1, z_interp_qx, pblocktemp, pfrag + 12);
								
								// interpolation for quad x increment
								iw_interp_qx += diw_dx + diw_dx;
								z_interp_qx += dz_dx + dz_dx;
								spa.varyingAdd(pvary_interp_qx, pvary_interp_qx, pvary_ddqx);
								
							}
							
						}
						
					} else {
						// partially covered block
						
						// shift to read quad coverage one at a time
						long coverage_next = coverage;
						
						for (int yy = 0; yy < q; yy += 2) {
							
							// interpolation setup for quad x increment
							float iw_interp_qx = iw_interp_qy;
							float z_interp_qx = z_interp_qy;
							spa.varyingCopy(pvary_interp_qx, pvary_interp_qy);
							
							// interpolation for quad y increment
							iw_interp_qy += diw_dy + diw_dy;
							z_interp_qy += dz_dy + dz_dy;
							spa.varyingAdd(pvary_interp_qy, pvary_interp_qy, pvary_ddqy);
							
							for (int xx = 0; xx < q; xx += 2) {
								
								// read quad coverage
								int accept = 0xF & (int) coverage_next;
								coverage_next >>>= 4;
								
								// execute shader if any fragments covered
								if (accept != 0) {
									
									accept = accept & spa.shadeFragmentQuad(
										pfrag, ptemp_next, puniforms, pvary_interp_qx, pvary_ddx, pvary_ddy,
										x + xx, y + yy,
										z_interp_qx, dz_dx, dz_dy,
										iw_interp_qx, diw_dx, diw_dy
									);
									
									fba.writeFragment((accept & 0b0001) >>> 0, xx + 0, yy + 0, z_interp_qx, pblocktemp, pfrag);
									fba.writeFragment((accept & 0b0010) >>> 1, xx + 1, yy + 0, z_interp_qx, pblocktemp, pfrag + 4);
									fba.writeFragment((accept & 0b0100) >>> 2, xx + 0, yy + 1, z_interp_qx, pblocktemp, pfrag + 8);
									fba.writeFragment((accept & 0b1000) >>> 3, xx + 1, yy + 1, z_interp_qx, pblocktemp, pfrag + 12);
									
								}
								
								// interpolation for quad x increment
								iw_interp_qx += diw_dx + diw_dx;
								z_interp_qx += dz_dx + dz_dx;
								spa.varyingAdd(pvary_interp_qx, pvary_interp_qx, pvary_ddqx);
								
							}
							
						}
						
					} // fullcover?
					
				} // for each block col
				
			} // for each block row
			
			// next set of blocks follows immediately
			prasterblock = pcoverage_next;
			
		} // for each set of raster blocks
		
	}
	
	
	@Deprecated
	public static void rasterizeTriangleOld(
		int worker_id,
		int num_workers,
		FramebufferAccessor fba,
		ShaderProgramAccessor spa,
		long pv0,
		long pv1,
		long pv2,
		long ptemp,
		int vp_x,
		int vp_y,
		int vp_w,
		int vp_h,
		float halflinewidth
	) {
		
		//if (Boolean.TRUE) return;
		
		// http://forum.devmaster.net/t/advanced-rasterization/6145
		// the code this was based on uses y-down coords, we use y-up
		
		// TODO z values need to be converted from [-1,1] to [0,1]
		// ^ can happen before interpolation
		// TODO cull faces that go too far off screen
		// TODO proper face culling
		// TODO find first block for this worker (per row?) then jump to the correct block instead of testing
		
		// block size
		final int q = blockSize();
		final int qmask = blockMask();
		
		// allocate temp storage
		final int vsz = spa.geometryOutSize() - 16;
		final long pblocktemp = ptemp;
		final long pvary_ddx = pblocktemp + 128;
		final long pvary_ddy = pvary_ddx + vsz;
		final long pvary_ddqx = pvary_ddy + vsz;
		final long pvary_ddqy = pvary_ddqx + vsz;
		final long pvary_ddbx = pvary_ddqy + vsz;
		final long pvary_ddby = pvary_ddbx + vsz;
		final long pvary_interp_bx = pvary_ddby + vsz;
		final long pvary_interp_by = pvary_interp_bx + vsz;
		final long pvary_interp_qx = pvary_interp_by + vsz;
		final long pvary_interp_qy = pvary_interp_qx + vsz;
		final long pfrag = pvary_interp_qy + vsz;
		
		// w values for perspective division
		float w0f = Util.unsafe.getFloat(pv0 + 12);
		float w1f = Util.unsafe.getFloat(pv1 + 12);
		float w2f = Util.unsafe.getFloat(pv2 + 12);
		
		// 1/w for perspective correct interpolation
		float iw0 = 1.f / w0f;
		float iw1 = 1.f / w1f;
		float iw2 = 1.f / w2f;
		
		// float pixel positions (homogenized)
		// -0.5 offset because we should test against pixel centres but the rasterizer tests bottom-left pixel corners
		final float hscale = 0.5f * vp_w;
		final float vscale = 0.5f * vp_h;
		final float hoffset = vp_x - 0.5f;
		final float voffset = vp_y - 0.5f;
		float x0f = (iw0 * Util.unsafe.getFloat(pv0) + 1.f) * hscale + hoffset;
		float y0f = (iw0 * Util.unsafe.getFloat(pv0 + 4) + 1.f) * vscale + voffset;
		float z0f = iw0 * Util.unsafe.getFloat(pv0 + 8);
		float x1f = (iw1 * Util.unsafe.getFloat(pv1) + 1.f) * hscale + hoffset;
		float y1f = (iw1 * Util.unsafe.getFloat(pv1 + 4) + 1.f) * vscale + voffset;
		float z1f = iw1 * Util.unsafe.getFloat(pv1 + 8);
		float x2f = (iw2 * Util.unsafe.getFloat(pv2) + 1.f) * hscale + hoffset;
		float y2f = (iw2 * Util.unsafe.getFloat(pv2 + 4) + 1.f) * vscale + voffset;
		float z2f = iw2 * Util.unsafe.getFloat(pv2 + 8);
		
		// float pixel deltas
		float dx01f = x1f - x0f;
		float dy01f = y1f - y0f;
		float dx12f = x2f - x1f;
		float dy12f = y2f - y1f;
		float dx20f = x0f - x2f;
		float dy20f = y0f - y2f;
		
		// adjustments for drawing lines
		float imd12 = 1.f / (float) Math.sqrt(dx12f * dx12f + dy12f * dy12f);
		float lwddx = -dy12f * imd12 * halflinewidth;
		float lwddy = dx12f * imd12 * halflinewidth;
		x0f += lwddx;
		y0f += lwddy;
		x1f += -lwddx;
		y1f += -lwddy;
		x2f += -lwddx;
		y2f += -lwddy;
		
		// float pixel deltas after adjustments
		dx01f = x1f - x0f;
		dy01f = y1f - y0f;
		dx12f = x2f - x1f;
		dy12f = y2f - y1f;
		dx20f = x0f - x2f;
		dy20f = y0f - y2f;
		
		// determine if front face
		boolean frontface = dx01f * dy12f - dy01f * dx12f > 0.f;
		
		// swap vertex order if backface to make half-spaces behave
		// TODO recalc deltas afterwards?
		if (!frontface) {
			float xxf = x1f;
			float yyf = y1f;
			float zzf = z1f;
			float wwf = w1f;
			long pvv = pv1;
			x1f = x2f;
			y1f = y2f;
			z1f = z2f;
			w1f = w2f;
			pv1 = pv2;
			x2f = xxf;
			y2f = yyf;
			z2f = zzf;
			w2f = wwf;
			pv2 = pvv;
		}
		
		// temp: backface culling
		//if (!frontface) return;
		
		// use 28.4 fixed point for pixel positions
		final int x0 = (int) (x0f * 16.f + 0.5f);
		final int y0 = (int) (y0f * 16.f + 0.5f);
		final int x1 = (int) (x1f * 16.f + 0.5f);
		final int y1 = (int) (y1f * 16.f + 0.5f);
		final int x2 = (int) (x2f * 16.f + 0.5f);
		final int y2 = (int) (y2f * 16.f + 0.5f);
		
		// bounding rectangle, clamped to screen bounds
		// also skip if no visible area
		// add 0xF to the _min_ calculation too because here we're testing lower-left pixel corners
		int minx = (min(x0, min(x1, x2)) + 0xF) >> 4;
		int maxx = (max(x0, max(x1, x2)) + 0xF) >> 4;
		minx = max(minx, vp_x);
		maxx = min(maxx, vp_x + vp_w);
		if (minx >= maxx) return;
		int miny = (min(y0, min(y1, y2)) + 0xF) >> 4;
		int maxy = (max(y0, max(y1, y2)) + 0xF) >> 4;
		miny = max(miny, vp_y);
		maxy = min(maxy, vp_y + vp_h);
		if (miny >= maxy) return;
		
		// force start in corner of block
		final int minbx = minx & qmask;
		final int minby = miny & qmask;
		
		// number of blocks in bounding box
		final int xblocks = (maxx - minbx + q - 1) >> blockBits();
		final int yblocks = (maxy - minby + q - 1) >> blockBits();
		
		// range of worker threads hit by bounding box
		// relies on checkerboard block -> worker mapping
		// TODO layer
		final int base_worker_id = rasterWorkerIndex(minbx, minby, 0, num_workers);
		final int worker_range = xblocks + yblocks - 1;
		
		// if this worker thread is not hit by the bounding box, discard
		if (
			!(base_worker_id <= worker_id && worker_id < (base_worker_id + worker_range)) &&
			!(base_worker_id <= (worker_id + num_workers) && (worker_id + num_workers) < (base_worker_id + worker_range))
		) {
			return;
		}
		
		// barycentric things
//		float dy21f = y1f - y2f;
//		float dx12f = x2f - x1f;
//		float dy02f = y2f - y0f;
//		float dx20f = x0f - x2f;
		// TODO if points are colinear, idet is probably inf; do we need to deal with this?
		float idet = 1.f / (-dy12f * (x0f - x2f) + dx12f * (y0f - y2f));
		
		// compute barycentric coords at bottom left of triangle's bounding box +1px either way
		float l00_0 = (-dy12f * (minbx - x2f) + dx12f * (minby - y2f)) * idet;
		float l00_1 = (-dy20f * (minbx - x2f) + dx20f * (minby - y2f)) * idet;
		float l00_2 = 1.f - l00_1 - l00_0;
		float l10_0 = (-dy12f * (minbx + 1.f - x2f) + dx12f * (minby - y2f)) * idet;
		float l10_1 = (-dy20f * (minbx + 1.f - x2f) + dx20f * (minby - y2f)) * idet;
		float l10_2 = 1.f - l10_1 - l10_0;
		float l01_0 = (-dy12f * (minbx - x2f) + dx12f * (minby + 1.f - y2f)) * idet;
		float l01_1 = (-dy20f * (minbx - x2f) + dx20f * (minby + 1.f - y2f)) * idet;
		float l01_2 = 1.f - l01_1 - l01_0;
		
		// 1/w barycentric interpolation to bottom left
		float iw00 = l00_0 * iw0 + l00_1 * iw1 + l00_2 * iw2;
		float iw10 = l10_0 * iw0 + l10_1 * iw1 + l10_2 * iw2;
		float iw01 = l01_0 * iw0 + l01_1 * iw1 + l01_2 * iw2;
		
		// 1/w deltas
		float diw_dx = iw10 - iw00;
		float diw_dy = iw01 - iw00;
		float diw_dbx = diw_dx * q;
		float diw_dby = diw_dy * q;
		
		// z barycentric interpolation to bottom left
		// we can linearly interpolate ndc z, because we've already divided it by w
		final float z00 = l00_0 * z0f + l00_1 * z1f + l00_2 * z2f;
		final float z10 = l10_0 * z0f + l10_1 * z1f + l10_2 * z2f;
		final float z01 = l01_0 * z0f + l01_1 * z1f + l01_2 * z2f;
		
		// z deltas
		final float dz_dx = z10 - z00;
		final float dz_dy = z01 - z00;
		final float dz_dbx = dz_dx * q;
		final float dz_dby = dz_dy * q;
		
		// varying interpolation to bottom left
		spa.varyingInterpolate3(pvary_interp_by, pv0 + 16, pv1 + 16, pv2 + 16, l00_0, l00_1, l00_2, iw0, iw1, iw2);
		spa.varyingInterpolate3(pvary_ddx, pv0 + 16, pv1 + 16, pv2 + 16, l10_0, l10_1, l10_2, iw0, iw1, iw2);
		spa.varyingInterpolate3(pvary_ddy, pv0 + 16, pv1 + 16, pv2 + 16, l01_0, l01_1, l01_2, iw0, iw1, iw2);
		
		// varying deltas
		spa.varyingSub(pvary_ddx, pvary_ddx, pvary_interp_by);
		spa.varyingSub(pvary_ddy, pvary_ddy, pvary_interp_by);
		spa.varyingMul(pvary_ddbx, pvary_ddx, q);
		spa.varyingMul(pvary_ddby, pvary_ddy, q);
		spa.varyingMul(pvary_ddqx, pvary_ddx, 2.f);
		spa.varyingMul(pvary_ddqy, pvary_ddy, 2.f);
		
		// deltas 28.4
		final int dx01 = x1 - x0;
		final int dx12 = x2 - x1;
		final int dx20 = x0 - x2;
		final int dy01 = y1 - y0;
		final int dy12 = y2 - y1;
		final int dy20 = y0 - y2;
		
		// deltas 24.8, because multiplying two 28.4's gives a 24.8
		final int fdx01 = dx01 << 4;
		final int fdx12 = dx12 << 4;
		final int fdx20 = dx20 << 4;
		final int fdy01 = dy01 << 4;
		final int fdy12 = dy12 << 4;
		final int fdy20 = dy20 << 4;
		
		// half-edge constants 24.8
		// -1 so that we can use >= for testing, i.e. the sign bit
		int c0 = dy01 * x0 - dx01 * y0 - 1;
		int c1 = dy12 * x1 - dx12 * y1 - 1;
		int c2 = dy20 * x2 - dx20 * y2 - 1;
		
		// correct for (top-left) fill convention
		if (dy01 < 0 || (dy01 == 0 && dx01 < 0)) c0++;
		if (dy12 < 0 || (dy12 == 0 && dx12 < 0)) c1++;
		if (dy20 < 0 || (dy20 == 0 && dx20 < 0)) c2++;
		
		// interpolation setup for block y increment
		float iw_interp_by = iw00;
		float z_interp_by = z00;
		// varyings already setup
		
		// loop through blocks
		for (int y = minby; y < maxy; y += q) {
			
			// interpolation setup for block x increment
			float iw_interp_bx = iw_interp_by;
			float z_interp_bx = z_interp_by;
			spa.varyingCopy(pvary_interp_bx, pvary_interp_by);
			
			// interpolation for block y increment
			iw_interp_by += diw_dby;
			z_interp_by += dz_dby;
			spa.varyingAdd(pvary_interp_by, pvary_interp_by, pvary_ddby);
			
			for (int x = minbx; x < maxx; x += q) {
				
				// within-block bounds
				// for a fully covered block, these should always be 0 and q
				// for partially covered blocks, only scanning this region improves performance (a little)
				// round min down to quad-alignment
				final int minxx = max(0, minx - x) & ~1;
				final int maxxx = min(q, maxx - x);
				final int minyy = max(0, miny - y) & ~1;
				final int maxyy = min(q, maxy - y);
				
				// interpolation setup for quad y increment
				// move to qy and qx start within block
				float iw_interp_qy = diw_dy * minyy + diw_dx * minxx + iw_interp_bx;
				float z_interp_qy = dz_dy * minyy + dz_dx * minxx + z_interp_bx;
				//spa.varyingCopy(pvary_interp_qy, pvary_interp_bx);
				spa.varyingFma(pvary_interp_qy, pvary_ddy, minyy, pvary_interp_bx);
				spa.varyingFma(pvary_interp_qy, pvary_ddx, minxx, pvary_interp_qy);
				
				// interpolation for block x increment
				// this has to happen before any possible block discard!
				iw_interp_bx += diw_dbx;
				z_interp_bx += dz_dbx;
				spa.varyingAdd(pvary_interp_bx, pvary_interp_bx, pvary_ddbx);
				
				// don't process blocks destined for another thread
				// TODO layer
				if (rasterWorkerIndex(x, y, 0, num_workers) != worker_id) continue;
				
				// corners of block 28.4
				// -1 because we want the corners _inside_ this block (lower-left corner testing)
				final int bx0 = x << 4;
				final int bx1 = (x + q - 1) << 4;
				final int by0 = y << 4;
				final int by1 = (y + q - 1) << 4;
				
				// eval half-space functions
				// TODO we could eval each half-space at only one (different) corner and interpolate the results
				final int a00 = c0 + dx01 * by0 - dy01 * bx0 >= 0 ? 1 : 0;
				final int a10 = c0 + dx01 * by0 - dy01 * bx1 >= 0 ? 2 : 0;
				final int a01 = c0 + dx01 * by1 - dy01 * bx0 >= 0 ? 4 : 0;
				final int a11 = c0 + dx01 * by1 - dy01 * bx1 >= 0 ? 8 : 0;
				final int a = a00 | a10 | a01 | a11;
				
				final int b00 = c1 + dx12 * by0 - dy12 * bx0 >= 0 ? 1 : 0;
				final int b10 = c1 + dx12 * by0 - dy12 * bx1 >= 0 ? 2 : 0;
				final int b01 = c1 + dx12 * by1 - dy12 * bx0 >= 0 ? 4 : 0;
				final int b11 = c1 + dx12 * by1 - dy12 * bx1 >= 0 ? 8 : 0;
				final int b = b00 | b10 | b01 | b11;
				
				final int c00 = c2 + dx20 * by0 - dy20 * bx0 >= 0 ? 1 : 0;
				final int c10 = c2 + dx20 * by0 - dy20 * bx1 >= 0 ? 2 : 0;
				final int c01 = c2 + dx20 * by1 - dy20 * bx0 >= 0 ? 4 : 0;
				final int c11 = c2 + dx20 * by1 - dy20 * bx1 >= 0 ? 8 : 0;
				final int c = c00 | c10 | c01 | c11;
				
				// skip block when outside an edge
				if (a == 0 || b == 0 || c == 0) continue;
				
				final boolean fullcover = (a & b & c) == 0xF;
				if (!fba.setupBlock(pblocktemp, x, y, 0, worker_id, fullcover)) continue;
				
				// accept whole block when totally covered
				if (fullcover) {
					for (int yy = 0; yy < q; yy += 2) {
						
						// interpolation setup for quad x increment
						float iw_interp_qx = iw_interp_qy;
						float z_interp_qx = z_interp_qy;
						spa.varyingCopy(pvary_interp_qx, pvary_interp_qy);
						
						// interpolation for quad y increment
						iw_interp_qy += diw_dy + diw_dy;
						z_interp_qy += dz_dy + dz_dy;
						spa.varyingAdd(pvary_interp_qy, pvary_interp_qy, pvary_ddqy);
						
						// apparently unrolling this makes it slower atm
						// what appears to happen is that the last 3 (of 16) calls to writefrag don't get inlined
						// but a half-unroll might still be interesting...
						for (int xx = 0; xx < q; xx += 2) {
							
							int accept = spa.shadeFragmentQuad(
								pfrag, 0, 0, pvary_interp_qx, pvary_ddx, pvary_ddy,
								x + xx, y + yy,
								z_interp_qx, dz_dx, dz_dy,
								iw_interp_qx, diw_dx, diw_dy
							);
							
							// TODO write correct z values
							fba.writeFragment((accept & 0b0001) >>> 0, xx + 0, yy + 0, z_interp_qx, pblocktemp, pfrag);
							fba.writeFragment((accept & 0b0010) >>> 1, xx + 1, yy + 0, z_interp_qx, pblocktemp, pfrag + 4);
							fba.writeFragment((accept & 0b0100) >>> 2, xx + 0, yy + 1, z_interp_qx, pblocktemp, pfrag + 8);
							fba.writeFragment((accept & 0b1000) >>> 3, xx + 1, yy + 1, z_interp_qx, pblocktemp, pfrag + 12);
							
							// interpolation for quad x increment
							iw_interp_qx += diw_dx + diw_dx;
							z_interp_qx += dz_dx + dz_dx;
							spa.varyingAdd(pvary_interp_qx, pvary_interp_qx, pvary_ddqx);
							
						}
						
					}
					
				} else {
					// partially covered block
					// interpolation setup for half-space functions for quad y increment
					final int bby0 = by0 + (minyy << 4);
					final int bbx0 = bx0 + (minxx << 4);
					int cy0 = c0 + dx01 * bby0 - dy01 * bbx0;
					int cy1 = c1 + dx12 * bby0 - dy12 * bbx0;
					int cy2 = c2 + dx20 * bby0 - dy20 * bbx0;
					
					for (int yy = minyy; yy < maxyy; yy += 2) {
						
						// interpolation setup for quad x increment
						int cx0 = cy0;
						int cx1 = cy1;
						int cx2 = cy2;
						float iw_interp_qx = iw_interp_qy;
						float z_interp_qx = z_interp_qy;
						spa.varyingCopy(pvary_interp_qx, pvary_interp_qy);
						
						// interpolation for quad y increment
						cy0 += fdx01 + fdx01;
						cy1 += fdx12 + fdx12;
						cy2 += fdx20 + fdx20;
						iw_interp_qy += diw_dy + diw_dy;
						z_interp_qy += dz_dy + dz_dy;
						spa.varyingAdd(pvary_interp_qy, pvary_interp_qy, pvary_ddqy);
						
						for (int xx = minxx; xx < maxxx; xx += 2) {
							
							// coverage testing
							// these expressions or the sign bits together, giving 1 if any half-space was < 0
							// this is why we subtracted 1 from the half-space constants
							int discard = 0;
							// test 0
							discard |= ((cx0 >>> 31) | (cx1 >>> 31) | (cx2 >>> 31)) << 0;
							// y inc
							cx0 += fdx01;
							cx1 += fdx12;
							cx2 += fdx20;
							// test 2
							discard |= ((cx0 >>> 31) | (cx1 >>> 31) | (cx2 >>> 31)) << 2;
							// x inc
							cx0 -= fdy01;
							cx1 -= fdy12;
							cx2 -= fdy20;
							// test 3
							discard |= ((cx0 >>> 31) | (cx1 >>> 31) | (cx2 >>> 31)) << 3;
							// y dec
							cx0 -= fdx01;
							cx1 -= fdx12;
							cx2 -= fdx20;
							// test 1
							discard |= ((cx0 >>> 31) | (cx1 >>> 31) | (cx2 >>> 31)) << 1;
							// x inc; this leaves the half-space interpolation at the start point for the next quad
							cx0 -= fdy01;
							cx1 -= fdy12;
							cx2 -= fdy20;
							
							// execute shader if any fragments covered
							if (discard != 0xF) {
								
								int accept = ~discard & spa.shadeFragmentQuad(
									pfrag, 0, 0, pvary_interp_qx, pvary_ddx, pvary_ddy,
									x + xx, y + yy,
									z_interp_qx, dz_dx, dz_dy,
									iw_interp_qx, diw_dx, diw_dy
								);
								
								fba.writeFragment((accept & 0b0001) >>> 0, xx + 0, yy + 0, z_interp_qx, pblocktemp, pfrag);
								fba.writeFragment((accept & 0b0010) >>> 1, xx + 1, yy + 0, z_interp_qx, pblocktemp, pfrag + 4);
								fba.writeFragment((accept & 0b0100) >>> 2, xx + 0, yy + 1, z_interp_qx, pblocktemp, pfrag + 8);
								fba.writeFragment((accept & 0b1000) >>> 3, xx + 1, yy + 1, z_interp_qx, pblocktemp, pfrag + 12);
								
							}
							
							// interpolation for quad x increment
							// we took care of the half-spaces during coverage testing
							iw_interp_qx += diw_dx + diw_dx;
							z_interp_qx += dz_dx + dz_dx;
							spa.varyingAdd(pvary_interp_qx, pvary_interp_qx, pvary_ddqx);
							
						}
						
					}
				}
				
			}
			
		}
		
	}
	
}































