package initial3d.detail.gl;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import initial3d.GL;
import initial3d.GL.DebugProc;
import initial3d.GLBitfield;
import initial3d.GLEnum;
import initial3d.GLUtil;
import initial3d.Vec2i;
import initial3d.Vec4f;
import initial3d.Vec4i;
import initial3d.detail.Access;
import initial3d.detail.AccessFuture;
import initial3d.detail.IndexAllocator;
import initial3d.detail.Util;
import sun.misc.Unsafe;

import static initial3d.Functions.*;
import static initial3d.GLEnum.*;
import static initial3d.GLEnumValues.*;

/**
 * <h1>
 * Initial3D OpenGL implementation
 * </h1>
 * 
 * <p>
 * Resource names refer to only one resource object until deleted (no replacement)
 * so resources can hold on to other resources directly Resource objects are used
 * exclusively by the user thread. Command execution should not reference resources
 * (but command construction, on the user thread, can).
 * </p>
 * 
 * <p>
 * Resource accessors should have all needed resource state baked in and should not retain
 * references to resource objects. Accessors can use data store pointers, provided that
 * preparing a command to use a resource has the command require that data store.
 * Accessor implementation classes should declare static methods for each method in the interface and name
 * them with the suffix <code>_static</code>. Non-static interface methods will be built automagically
 * to forward to the static methods (unless {@link Resource#createAccessor()} is overridden).
 * See {@link Resource#buildAccessor(initial3d.detail.bytecode.ClassBuilder)}.
 * </p>
 * 
 * <p>
 * Data store access is managed by an access queue. Execution of commands by the driver
 * thread begins by waiting for the required access to all data stores used by the command.
 * The user thread (or any other thread) can access data store contents too by waiting
 * in the queue, but this will of course block if the data store is in use elsewhere.
 * </p>
 * 
 * - TODO faster raster (triangle setup needs parallelization to speed up lots of small tris)
 * - TODO shader linking, uniform and attribute names and locations
 * - TODO shader compilation...
 * - TODO be consistent about seeking with supplied nio buffers
 * - TODO rasterizing to 1d textures how? (or rather, anything not a multiple of the block size)
 * - TODO flat, noperspective interpolation
 * - TODO texture mapping
 * - TODO maybe generate rasterizer bytecode dynamically?
 * - TODO methodbuilder iload etc wide index support
 * 
 * 
 * @author ben
 *
 */
public class I3DGL implements GL {

	public static final int WARNING_BUFFER_ACCESS = 90000001;
	
	private static final GLEnum[] capabilities = new GLEnum[] {
		GL_CULL_FACE,
		GL_DEPTH_TEST,
		GL_BLEND
	};
	
	private static final GLEnum[] buffer_targets = new GLEnum[] {
		GL_ARRAY_BUFFER,
		GL_COPY_READ_BUFFER,
		GL_COPY_WRITE_BUFFER,
		GL_ELEMENT_ARRAY_BUFFER,
		GL_PIXEL_PACK_BUFFER,
		GL_PIXEL_UNPACK_BUFFER,
		GL_TEXTURE_BUFFER,
		GL_TRANSFORM_FEEDBACK_BUFFER,
		GL_UNIFORM_BUFFER
	};
	
	private static final GLEnum[] texture_targets = new GLEnum[] {
		GL_TEXTURE_1D,
		GL_TEXTURE_2D,
		GL_TEXTURE_3D
	};
	
	private static final GLEnum[] framebuffer_targets = new GLEnum[] {
		GL_DRAW_FRAMEBUFFER,
		GL_READ_FRAMEBUFFER
	};
	
	private static final GLEnum[] blend_modes = new GLEnum[] {
		GL_FUNC_ADD,
		GL_FUNC_SUBTRACT,
		GL_FUNC_REVERSE_SUBTRACT,
		GL_MIN,
		GL_MAX
	};
	
	private static final GLEnum[] blend_factors = new GLEnum[] {
		GL_ZERO,
		GL_ONE,
		GL_SRC_COLOR,
		GL_ONE_MINUS_SRC_COLOR,
		GL_DST_COLOR,
		GL_ONE_MINUS_DST_COLOR,
		GL_SRC_ALPHA,
		GL_ONE_MINUS_SRC_ALPHA,
		GL_DST_ALPHA,
		GL_ONE_MINUS_DST_ALPHA,
		GL_CONSTANT_COLOR,
		GL_ONE_MINUS_CONSTANT_COLOR,
		GL_CONSTANT_ALPHA,
		GL_ONE_MINUS_CONSTANT_ALPHA,
		GL_SRC_ALPHA_SATURATE
	};
	
	// debug things
	GL.DebugProc debug_callback = null;
	Object debug_userparam = null;
	GLEnum last_error = GL_NO_ERROR;
	
	// thread that actually executes rendering commands
	private final Driver driver = new Driver(this);
	
	// map of resource names to actual resource objects
	private final HashMap<Integer, Resource> resources = new HashMap<>();
	
	// allocator for integer resource names
	private final IndexAllocator namealloc = new IndexAllocator(1);
	
	// stack-ish of things that can handle binding of names to possibly indexed binding points
	// the first element is the highest priority
	private final LinkedList<BindState> bind_states = new LinkedList<>();
	
	// enabled states
	private final HashSet<GLEnum> enabled = new HashSet<>();
	
	// active texture unit
	private int active_texture = 0;
	
	// default fbo
	private Framebuffer default_framebuffer = null;
	
	// shader programs
	private ShaderProgram default_program = null;
	private ShaderProgram current_program = null;
	
	// misc state (package-private)
	Vec2i viewport_pos = Vec2i.zero;
	Vec2i viewport_size = Vec2i.zero;
	Vec4f clear_color = Vec4f.zero;
	float clear_depth = 1.f;
	boolean cull_front = false;
	boolean cull_back = true;
	GLEnum front_tri_mode = GL_FILL;
	GLEnum back_tri_mode = GL_FILL;
	float line_width = 1;
	float point_size = 1;
	GLEnum depth_func = GL_LESS;
	final BlendState blend_state = new BlendState();
	
	// cache for image used to blit default fbo to Graphics2D
	private BufferedImage blit_img = null;
	
	// sync object to prevent there being more than one pipelined blit from the default fbo
	private int blit_sync = 0;
	
	private class BaseBindState implements BindState {

		private final Map<BindPoint, Resource> bindings = new HashMap<>();
		
		@Override
		public Resource boundResource(BindPoint bp) {
			return bindings.get(bp);
		}

		@Override
		public boolean bindResource(BindPoint bp, Resource r) {
			Resource r0 = null;
			if (r == null) {
				r0 = bindings.remove(bp);
			} else {
				r0 = bindings.put(bp, r);
			}
			// modify the bind state 'stack' appropriately if needed
			// we should be able to do this without concurrent modification problems
			// because this will only happen on the last iteration over bind_states.
			// this should also be safe even if the same resource ends up in bind_states
			// multiple times, because remove only removes the first.
			if (r0 != null && r0 instanceof BindState) {
				bind_states.remove(r0);
			}
			if (r != null && r instanceof BindState) {
				bind_states.addFirst((BindState) r);
			}
			return true;
		}

		@Override
		public void unbindResource(Resource r) {
			for (Iterator<Map.Entry<BindPoint, Resource>> it = bindings.entrySet().iterator(); it.hasNext(); ) {
				Map.Entry<BindPoint, Resource> b = it.next();
				if (b.getValue() == r) it.remove();
			}
		}
		
	}
	
	public I3DGL(int w, int h, GLEnum depth_format) {
		bind_states.addFirst(new BaseBindState());
		default_program = resource(genName(), ShaderProgram.class);
		current_program = default_program;
		default_framebuffer = resource(genName(), Framebuffer.class);
		bindName(GL_DRAW_FRAMEBUFFER, default_framebuffer.name(), Framebuffer.class);
		bindName(GL_READ_FRAMEBUFFER, default_framebuffer.name(), Framebuffer.class);
		// proper attachment names: GL_BACK/GL_DEPTH (https://www.opengl.org/wiki/Default_Framebuffer)
		Texture ca = resource(genName(), Texture.class);
		ca.image(0).realloc(GL_RGBA8, vec3i(w, h, 1));
		Texture da = resource(genName(), Texture.class);
		da.image(0).realloc(depth_format, vec3i(w, h, 1));
		default_framebuffer.attach(GL_BACK, ca, 0);
		default_framebuffer.attach(GL_DEPTH, da, 0);
		default_framebuffer.drawBuffers(GL_BACK);
	}
	
	@Override
	protected void finalize() {
		// TODO automatic shutdown without requiring finalization
		driver.shutdown();
	}
	
	public void debug(GLEnum source, GLEnum type, GLEnum severity, int id, String msg, Object... fargs) {
		msg = String.format(msg, fargs);
		// TODO msg control
		if (debug_callback != null) {
			debug_callback.execute(source, type, id, severity, msg, debug_userparam);
		}
	}
	
	public void debugAPI(GLEnum type, GLEnum severity, int id, String msg, Object... fargs) {
		debug(GL_DEBUG_SOURCE_API, type, severity, id, msg, fargs);
		// temp
		System.err.println("GL: " + String.format(msg, fargs));
		if (type == GL_DEBUG_TYPE_ERROR) throw new RuntimeException();
	}
	
	public void debugAPIError(GLEnum err, String msg, Object... fargs) {
		last_error = err;
		debugAPI(GL_DEBUG_TYPE_ERROR, GL_DEBUG_SEVERITY_HIGH, err.valueInt(), msg, fargs);
	}
	
	public void debugAPIPerformanceWarning(int id, String msg, Object... fargs) {
		debugAPI(GL_DEBUG_TYPE_PERFORMANCE, GL_DEBUG_SEVERITY_MEDIUM, id, msg, fargs);
	}
	
	public void debugAPIPortabilityWarning(int id, String msg, Object... fargs) {
		debugAPI(GL_DEBUG_TYPE_PORTABILITY, GL_DEBUG_SEVERITY_MEDIUM, id, msg, fargs);
	}
	
	public void debugAPIUndefinedBehavior(int id, String msg, Object... fargs) {
		debugAPI(GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR, GLEnum.GL_DEBUG_SEVERITY_MEDIUM, id, msg, fargs);
	}
	
	public void debugAPIDeprecatedBehavior(int id, String msg, Object... fargs) {
		debugAPI(GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, GLEnum.GL_DEBUG_SEVERITY_MEDIUM, id, msg, fargs);
	}
	
	public void debugAPIInfo(String msg, Object... fargs) {
		debugAPI(GL_DEBUG_TYPE_OTHER, GL_DEBUG_SEVERITY_NOTIFICATION, 0, msg, fargs);
	}
	
	public void debugAPIError(APIException e) {
		debugAPIError(e.error(), e.getMessage());
	}
	
	public RuntimeException throwInvalidOperation(String msg, Object ...fargs) {
		throw new APIException(GL_INVALID_OPERATION, String.format(msg, fargs));
	}
	
	public RuntimeException throwInvalidEnum(String msg, Object ...fargs) {
		throw new APIException(GL_INVALID_ENUM, String.format(msg, fargs));
	}
	
	public RuntimeException throwInvalidValue(String msg, Object ...fargs) {
		throw new APIException(GL_INVALID_VALUE, String.format(msg, fargs));
	}
	
	public RuntimeException throwLimitation(String msg, Object ...fargs) {
		throw throwInvalidOperation("implementation limitation: " + msg, fargs);
	}
	
	public GLEnum validateEnum(String pname, GLEnum e, GLEnum ...es) {
		for (GLEnum ee : es) {
			if (e == ee) return e;
		}
		throw throwInvalidEnum("invalid enum for '%s'; was %s, must be one of %s", pname, e, Arrays.toString(es));
	}
	
	public int validateValue(String pname, int x, int ...xs) {
		for (int xx : xs) {
			if (x == xx) return x;
		}
		throw throwInvalidValue("invalid value for '%s'; was %d, must be one of %s", pname, x, Arrays.toString(xs));
	}
	
	public long validateValue(String pname, long x, long ...xs) {
		for (long xx : xs) {
			if (x == xx) return x;
		}
		throw throwInvalidValue("invalid value for '%s'; was %d, must be one of %s", pname, x, Arrays.toString(xs));
	}
	
	public int validateValueRange(String pname, int x, int l) {
		if (x < l) throw throwInvalidValue("invalid value for '%s'; was %d, must be in range [%d,+inf)", pname, x, l);
		return x;
	}
	
	public int validateValueRange(String pname, int x, int l, int u) {
		if (x < l || x > u) throw throwInvalidValue("invalid value for '%s'; was %d, must be in range [%d,%d]", pname, x, l, u);
		return x;
	}
	
	public long validateValueRange(String pname, long x, long l) {
		if (x < l) throw throwInvalidValue("invalid value for '%s'; was %d, must be in range [%d,+inf)", pname, x, l);
		return x;
	}
	
	public long validateValueRange(String pname, long x, long l, long u) {
		if (x < l || x > u) throw throwInvalidValue("invalid value for '%s'; was %d, must be in range [%d,%d]", pname, x, l, u);
		return x;
	}
	
	public Driver driver() {
		return driver;
	}
	
	public void bindResource(BindPoint bp, Resource r) {
		for (BindState s : bind_states) {
			if (s.bindResource(bp, r)) return;
		}
	}
	
	public <T extends Resource> T bindName(BindPoint bp, int name, Class<T> cls) {
		if (name == 0) {
			bindResource(bp, null);
			return null;
		} else {
			T t = resource(name, cls);
			bindResource(bp, t);
			return t;
		}
	}
	
	public <T extends Resource> T bindName(GLEnum target, int name, Class<T> cls) {
		return bindName(new BindPoint(target), name, cls);
	}
	
	public <T extends Resource> T bindName(GLEnum target, int index, int name, Class<T> cls) {
		return bindName(new BindPoint(target, index), name, cls);
	}
	
	public int genName() {
		int name = namealloc.alloc();
		// map name to null so resource() can check it
		resources.put(name, null);
		return name;
	}
	
	public void deleteName(int name) {
		try {
			Resource r = resource(name, Resource.class);
			if (r != null) {
				r.onDeleteName();
				for (BindState s : bind_states) {
					s.unbindResource(r);
				}
			}
			resources.remove(name);
			namealloc.free(name);
		} catch (APIException e) {
			// ignore
		}
	}
	
	public <T extends Resource> T resource(int name, Class<T> cls) {
		Resource r = resources.get(name);
		if (r == null) {
			// allocated names initially get mapped to null
			if (!resources.containsKey(name)) throw throwInvalidValue("name %d is not allocated", name);
			// create resource
			try {
				Constructor<T> ctor = cls.getConstructor(I3DGL.class, int.class);
				T t = ctor.newInstance(this, name);
				resources.put(name, t);
				return t;
			} catch (NoSuchMethodException e) {
				throw new AssertionError(e);
			} catch (SecurityException e) {
				throw new AssertionError(e);
			} catch (InstantiationException e) {
				throw new AssertionError(e);
			} catch (IllegalAccessException e) {
				throw new AssertionError(e);
			} catch (IllegalArgumentException e) {
				throw new AssertionError(e);
			} catch (InvocationTargetException e) {
				throw new AssertionError(e);
			}
		}
		try {
			return cls.cast(r);
		} catch (ClassCastException e) {
			throw throwInvalidValue("resource %d is not of type %s; was %s", name, cls.getSimpleName(), r.getClass().getSimpleName());
		}
	}
	
	public <T extends Resource> T boundResource(BindPoint bp, Class<T> cls) {
		Resource r = null;
		try {
			for (BindState s : bind_states) {
				r = s.boundResource(bp);
				if (r != null) return cls.cast(r);
			}
			throw throwInvalidOperation("no resource bound to %s", bp);
		} catch (ClassCastException e) {
			throw throwInvalidOperation("resource %d is not of type %s; was %s", r.name(), cls.getSimpleName(), r.getClass().getSimpleName());
		}
	}
	
	public <T extends Resource> T boundResource(GLEnum target, Class<T> cls) {
		return boundResource(new BindPoint(target), cls);
	}
	
	public <T extends Resource> T boundResource(GLEnum target, int index, Class<T> cls) {
		return boundResource(new BindPoint(target, index), cls);
	}
	
	public void resizeDefaultFramebuffer(int w, int h) {
		Image ca = default_framebuffer.attachment(GL_BACK).texture.image(0);
		Image da = default_framebuffer.attachment(GL_DEPTH).texture.image(0);
		ca.realloc(ca.format(), vec3i(w, h, 1));
		da.realloc(da.format(), vec3i(w, h, 1));
	}
	
	public void blitDefaultFramebuffer(final Graphics2D g, final int gx, final int gy, final int gw, final int gh) {
		if (blit_sync > 0) {
			// wait for previous blit to complete so that we don't let
			// the user thread get too far ahead of the driver
			// this avoids the need for glFinish() in normal scenarios
			glClientWaitSync(blit_sync, GLBitfield.ZERO, GL_TIMEOUT_IGNORED.valueLong());
			glDeleteSync(blit_sync);
			blit_sync = 0;
		}
		// i3d GL packing: [msb] A | B | G | R [lsb]
		// if we use java BGR, then we can memcpy pixel data (carefully, because blocks)
		// unfortunately, we then have to discard alpha
		Image ca = default_framebuffer.attachment(GL_BACK).image();
		// recreate temp image if needed
		if (blit_img == null || blit_img.getWidth() < ca.size().x || blit_img.getHeight() < ca.size().y) {
			blit_img = new BufferedImage(Rasterizer.blockRoundUp(ca.size().x), Rasterizer.blockRoundUp(ca.size().y), BufferedImage.TYPE_INT_BGR);
		}
		// stuff
		final BufferedImage img0 = blit_img;
		final int[] img_array = ((DataBufferInt) img0.getRaster().getDataBuffer()).getData();
		final long pca = ca.address();
		final int w = ca.size().x;
		final int h = ca.size().y;
		final int stride = Rasterizer.blockRoundUp(w);
		final int y0 = Rasterizer.blockRoundUp(h) - 1;
		// copy default fbo to temp image
		Command cmd0 = new Command() {
			@Override
			public void run() {
				workerInvokeAll(() -> {
					// copy default fbo color buffer to temp image, flipping vertically
					final int csize = Rasterizer.blockSize() * 4;
					final int abo = Unsafe.ARRAY_INT_BASE_OFFSET;
					long p = pca + 4 * stride * Rasterizer.blockSize() * WorkerPool.id();
					for (int y = Rasterizer.blockSize() * WorkerPool.id(); y < h; y += Rasterizer.blockSize() * WorkerPool.numWorkers()) {
						for (int x = 0; x < w; x += Rasterizer.blockSize()) {
							for (int i = 0; i < Rasterizer.blockSize(); i++) {
								Util.unsafe.copyMemory(null, p, img_array, abo + 4 * ((y0 - y - i) * stride + x), csize);
								p += csize;
							}
						}
						p += 4 * stride * Rasterizer.blockSize() * (WorkerPool.numWorkers() - 1);
					}
				});
			}
		};
		cmd0.require(default_framebuffer, Access.READ);
		driver.invoke(cmd0);
		// have driver wait for copy to finish
		driver.invokeWaitResource(default_framebuffer, Access.UNIQUE_WRITE);
		// blit temp image to screen
		Command cmd1 = new Command() {
			@Override
			public void run() {
				workerInvoke(() -> {
					// blit image (flipping vertically here is possible but quite slow, so we do it during the copy above)
					g.drawImage(img0, gx, gy, gx + gw, gy + gh, 0, Rasterizer.blockRoundUp(h) - h, w, Rasterizer.blockRoundUp(h), null);
				});
			}
		};
		driver.invoke(cmd1);
		// allow the next call to wait for this one
		blit_sync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, GLBitfield.ZERO);
	}
	
	@Override
	public GLEnum glGetError() {
		GLEnum err = last_error;
		last_error = GL_NO_ERROR;
		return err;
	}

	@Override
	public void glEnable(GLEnum cap) {
		try {
			validateEnum("cap", cap, capabilities);
			enabled.add(cap);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glDisable(GLEnum cap) {
		try {
			validateEnum("cap", cap, capabilities);
			enabled.remove(cap);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public boolean glIsEnabled(GLEnum cap) {
		try {
			validateEnum("cap", cap, capabilities);
			return enabled.contains(cap);
		} catch (APIException e) {
			debugAPIError(e);
			return false;
		}
	}

	@Override
	public void glPointSize(float size) {
		try {
			if (size <= 0) throw throwInvalidValue("invalid value for 'size'; was %f, must be in range (0,+inf]");
			point_size = size;
		} catch (APIException e) {
			debugAPIError(e);
		}
	}
	
	@Override
	public void glLineWidth(float width) {
		try {
			if (width <= 0) throw throwInvalidValue("invalid value for 'size'; was %f, must be in range (0,+inf]");
			line_width = width;
		} catch (APIException e) {
			debugAPIError(e);
		}
	}
	
	@Override
	public void glPolygonMode(GLEnum face, GLEnum mode) {
		try {
			validateEnum("face", face, GL_FRONT, GL_BACK, GL_FRONT_AND_BACK);
			validateEnum("mode", mode, GL_FILL, GL_LINE, GL_POINT);
			if (face == GL_FRONT || face == GL_FRONT_AND_BACK) {
				front_tri_mode = mode;
			}
			if (face == GL_BACK || face == GL_FRONT_AND_BACK) {
				back_tri_mode = mode;
			}
		} catch (APIException e) {
			debugAPIError(e);
		}
	}
	
	@Override
	public void glCullFace(GLEnum mode) {
		try {
			validateEnum("mode", mode, GL_FRONT, GL_BACK, GL_FRONT_AND_BACK);
			if (mode == GL_FRONT || mode == GL_FRONT_AND_BACK) {
				cull_front = true;
			}
			if (mode == GL_BACK || mode == GL_FRONT_AND_BACK) {
				cull_back = true;
			}
		} catch (APIException e) {
			debugAPIError(e);
		}
	}
	
	@Override
	public void glDebugMessageCallback(DebugProc callback, Object userparam) {
		debug_callback = callback;
		debug_userparam = userparam;
	}

	@Override
	public int glGenBuffer() {
		return genName();
	}

	@Override
	public void glDeleteBuffer(int name) {
		deleteName(name);
	}

	@Override
	public void glBindBuffer(GLEnum target, int name) {
		try {
			validateEnum("target", target, buffer_targets);
			bindName(target, name, Buffer.class);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glBufferData(GLEnum target, int size, ByteBuffer data, GLEnum usage) {
		try {
			validateEnum("target", target, buffer_targets);
			Buffer b = boundResource(target, Buffer.class);
			validateValueRange("size", size, 0);
			// use our size param, not data.remaining()
			if (data != null) {
				validateValueRange("data.remaining()", data.remaining(), size);
				// slice() does not preserve byte order, but it doesn't matter here
				data = data.slice();
				data.limit(size);
			}
			boolean done = false;
			// could we reuse existing storage?
			if (size <= b.size()) {
				if (data == null) {
					// don't need to write so don't bother
					// this does mean that glBufferData(null) won't cause a discard
					// if the existing buffer is big enough
					done = true;
				} else {
					// try to get access
					AccessFuture af = b.data().requestAccess(Access.UNIQUE_WRITE);
					if (af.tryAcquire()) {
						// can write now
						b.map().put(data);
						af.release();
						done = true;
					} else {
						// would have to wait, don't bother
						// instead, we'll discard the data store
						af.cancel();
					}
				}
			}
			// if we couldn't reuse existing storage, replace it
			if (!done) {
				b.realloc(size);
				b.map().put(data);
			}
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glBufferSubData(GLEnum target, int offset, int size, ByteBuffer data) {
		try {
			validateEnum("target", target, buffer_targets);
			Buffer b = boundResource(target, Buffer.class);
			validateValueRange("offset", offset, 0, b.size());
			validateValueRange("size", size, 0, b.size() - offset);
			// use our size param, not data.remaining()
			if (data == null) return;
			validateValueRange("data.remaining()", data.remaining(), size);
			// slice() does not preserve byte order, but it doesn't matter here
			data = data.slice();
			data.limit(size);
			// try to get access to existing buffer
			AccessFuture af = b.data().requestAccess(Access.UNIQUE_WRITE);
			if (af.tryAcquire()) {
				// can write now
				b.map(offset, size).put(data);
				af.release();
			} else {
				// would have to wait, don't bother
				af.cancel();
				// copy data and write from driver thread instead
				DataStore data2 = DataStore.alloc(size, "buffersubdata temp");
				data2.map().put(data);
				driver.invokeCopy(b.data(), offset, data2, 0, size);
				driver.invokeFree(data2);
			}
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public ByteBuffer glMapBuffer(GLEnum target, GLEnum access) {
		try {
			validateEnum("target", target, buffer_targets);
			validateEnum("access", access, GL_READ_ONLY, GL_READ_WRITE, GL_WRITE_ONLY);
			Buffer b = boundResource(target, Buffer.class);
			switch (access.valueInt()) {
			case GL_READ_ONLY_value:
				// try get read access
				if (!b.tryAcquireUserAccess(Access.READ)) {
					debugAPIPerformanceWarning(WARNING_BUFFER_ACCESS, "blocking for read access to buffer %d", b.name());
					// block until readable
					b.acquireUserAccess(Access.READ);
				}
				break;
			case GL_READ_WRITE_value:
				// try get write access (preserving current contents)
				if (!b.tryAcquireUserAccess(Access.UNIQUE_WRITE)) {
					debugAPIPerformanceWarning(WARNING_BUFFER_ACCESS, "blocking for read-write access to buffer %d", b.name());
					// block until writable
					b.acquireUserAccess(Access.UNIQUE_WRITE);
				}
				break;
			case GL_WRITE_ONLY_value:
				// try get write access
				if (!b.tryAcquireUserAccess(Access.UNIQUE_WRITE)) {
					// can't access existing data store now
					// discard and replace the data store (don't need to preserve contents)
					b.realloc(b.size());
					b.acquireUserAccess(Access.UNIQUE_WRITE);
				}
				break;
			default:
				throw throwInvalidEnum("%s is not an access mode", access);
			}
			return b.map();
		} catch (APIException e) {
			debugAPIError(e);
			return null;
		}
	}

	@Override
	public boolean glUnmapBuffer(GLEnum target) {
		try {
			validateEnum("target", target, buffer_targets);
			Buffer b = boundResource(target, Buffer.class);
			b.releaseUserAccess();
			return true;
		} catch (APIException e) {
			debugAPIError(e);
			return false;
		}
	}

	@Override
	public int glGenVertexArray() {
		return genName();
	}

	@Override
	public void glDeleteVertexArray(int name) {
		deleteName(name);
	}

	@Override
	public void glBindVertexArray(int name) {
		try {
			// this takes care of modifying bind_states
			bindName(GL_VERTEX_ARRAY, name, VertexArray.class);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glVertexAttribPointer(int index, int size, GLEnum type, boolean normalized, int stride, int offset) {
		try {
			VertexArray v = boundResource(GL_VERTEX_ARRAY, VertexArray.class);
			Buffer b = boundResource(GL_ARRAY_BUFFER, Buffer.class);
			if (normalized) {
				v.attributeNormalized(index, b, size, stride, type, offset);
			} else {
				v.attributeFloat(index, b, size, stride, type, offset);
			}
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glVertexAttribIPointer(int index, int size, GLEnum type, int stride, int offset) {
		try {
			VertexArray v = boundResource(GL_VERTEX_ARRAY, VertexArray.class);
			Buffer b = boundResource(GL_ARRAY_BUFFER, Buffer.class);
			v.attributeInteger(index, b, size, stride, type, offset);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glEnableVertexAttribArray(int index) {
		try {
			VertexArray v = boundResource(GL_VERTEX_ARRAY, VertexArray.class);
			v.enable(index, true);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glDisableVertexAttribArray(int index) {
		try {
			VertexArray v = boundResource(GL_VERTEX_ARRAY, VertexArray.class);
			v.enable(index, false);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public int glGenTexture() {
		return genName();
	}

	@Override
	public void glDeleteTexture(int name) {
		deleteName(name);
	}

	@Override
	public void glBindTexture(GLEnum target, int texture) {
		try {
			// https://www.khronos.org/opengl/wiki/Texture#Texture_image_units
			// Each texture image unit supports bindings to all targets. So a 2D texture and an array texture can be
			// bound to the same image unit, or different 2D textures can be bound in two different image units without
			// affecting each other. So which texture gets used when rendering? In GLSL, this depends on the type of
			// sampler that uses this texture image unit.
			// Note: This sounds suspiciously like you can use the same texture image unit for different samplers, as
			// long as they have different texture types. Do not do this. The spec explicitly disallows it; if two
			// different GLSL samplers have different texture types, but are associated with the same texture image
			// unit, then rendering will fail. Give each sampler a different texture image unit.
			validateEnum("target", target, texture_targets);
			Texture t = bindName(target, active_texture, texture, Texture.class);
			t.target(target);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glActiveTexture(GLEnum texture) {
		active_texture = texture.valueInt() - GLEnum.GL_TEXTURE0.valueInt();
	}

	@Override
	public void glTexImage1D(GLEnum target, int level, GLEnum internal_format, int width, int border, GLEnum format, GLEnum type, ByteBuffer data) {
		glTexImage3D(target, level, internal_format, width, 1, 1, border, format, type, data);
	}

	@Override
	public void glTexImage2D(GLEnum target, int level, GLEnum internal_format, int width, int height, int border, GLEnum format, GLEnum type, ByteBuffer data) {
		glTexImage3D(target, level, internal_format, width, height, 1, border, format, type, data);
	}

	@Override
	public void glTexImage3D(GLEnum target, int level, GLEnum internal_format, int width, int height, int depth, int border, GLEnum format, GLEnum type, ByteBuffer data) {
		try {
			validateEnum("target", target, texture_targets);
			Texture t = boundResource(target, active_texture, Texture.class);
			Image m = t.image(level);
			validateValueRange("width", width, 0, Texture.MAX_SIZE);
			validateValueRange("height", height, 0, Texture.MAX_SIZE);
			validateValueRange("depth", depth, 0, Texture.MAX_SIZE);
			// TODO check remaining() for supplied buffer
			boolean done = false;
			// could we reuse existing storage?
			if (Image.allocSize(internal_format, vec3i(width, height, depth)) <= m.data().size()) {
				if (data == null) {
					// don't need to write so don't bother
					m.reformat(internal_format, vec3i(width, height, depth));
					done = true;
				} else {
					// try to get access
					AccessFuture af = m.data().requestAccess(Access.UNIQUE_WRITE);
					if (af.tryAcquire()) {
						// can write now
						m.reformat(internal_format, vec3i(width, height, depth));
						// TODO pixel unpack
						done = true;
					} else {
						// would have to wait, don't bother
						// instead, we'll discard the data store
						af.cancel();
					}
				}
			}
			// if we couldn't reuse existing storage, replace it
			if (!done) {
				m.realloc(internal_format, vec3i(width, height, depth));
				// TODO pixel unpack
			}
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glTexParameteri(GLEnum target, GLEnum pname, int param) {
		try {
			validateEnum("target", target, texture_targets);
			Texture t = boundResource(target, active_texture, Texture.class);
			// TODO texparam
			
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glTexParameterf(GLEnum target, GLEnum pname, float param) {
		try {
			validateEnum("target", target, texture_targets);
			Texture t = boundResource(target, active_texture, Texture.class);
			// TODO texparam
			
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glGenerateMipmap(GLEnum target) {
		// TODO mipmap generation
		
	}

	@Override
	public int glGenFramebuffer() {
		return genName();
	}

	@Override
	public void glDeleteFramebuffer(int name) {
		deleteName(name);
	}

	@Override
	public void glBindFramebuffer(GLEnum target, int framebuffer) {
		if (target == GLEnum.GL_FRAMEBUFFER) {
			glBindFramebuffer(GLEnum.GL_DRAW_FRAMEBUFFER, framebuffer);
			glBindFramebuffer(GLEnum.GL_READ_FRAMEBUFFER, framebuffer);
			return;
		}
		try {
			validateEnum("target", target, framebuffer_targets);
			Framebuffer f0 = boundResource(target, Framebuffer.class);
			f0.invokeFlush("unbound");
			if (framebuffer == 0) framebuffer = default_framebuffer.name();
			bindName(target, framebuffer, Framebuffer.class);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glDrawBuffers(GLEnum... bufs) {
		try {
			Framebuffer f = boundResource(GL_DRAW_FRAMEBUFFER, Framebuffer.class);
			f.drawBuffers(bufs);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glFramebufferTexture(GLEnum target, GLEnum attachment, int texture, int level) {
		try {
			validateEnum("target", target, framebuffer_targets);
			Framebuffer f = boundResource(target, Framebuffer.class);
			Texture t = resource(texture, Texture.class);
			f.attach(attachment, t, level);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glDepthFunc(GLEnum func) {
		try {
			validateEnum("func", func, GL_NEVER, GL_ALWAYS, GL_LESS, GL_LEQUAL, GL_EQUAL, GL_NOTEQUAL, GL_GEQUAL, GL_GREATER);
			depth_func = func;
		} catch (APIException e) {
			debugAPIError(e);
		}
	}
	
	@Override
	public void glViewport(int x, int y, int w, int h) {
		try {
			Framebuffer f = boundResource(GL_DRAW_FRAMEBUFFER, Framebuffer.class);
			f.invokeFlush("viewport changed");
			viewport_pos = vec2i(x, y);
			viewport_size = vec2i(w, h);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glClearColor(float red, float green, float blue, float alpha) {
		clear_color = clamp(vec4f(red, green, blue, alpha), vec4f(0), vec4f(1));
	}

	@Override
	public void glClearDepth(float depth) {
		clear_depth = clamp(depth, 0.f, 1.f);
	}
	
	@Override
	public void glClear(GLBitfield mask) {
		try {
			Framebuffer f = boundResource(GL_DRAW_FRAMEBUFFER, Framebuffer.class);
			f.invokeClearDepth(clear_depth);
			// TODO clear stencil 
			f.invokeClearColorf(clear_color);
			f.invokeClear(mask.testAll(GL_DEPTH_BUFFER_BIT), mask.testAll(GL_STENCIL_BUFFER_BIT), mask.testAll(GL_COLOR_BUFFER_BIT), -1);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}
	
	@Override
	public void glBlendColor(float red, float green, float blue, float alpha) {
		blend_state.blend_color = vec4f(red, green, blue, alpha);
	}
	
	@Override
	public void glBlendEquation(GLEnum mode) {
		glBlendEquationSeparate(mode, mode);
	}
	
	@Override
	public void glBlendEquationSeparate(GLEnum mode_rgb, GLEnum mode_alpha) {
		try {
			validateEnum("mode_rgb", mode_rgb, blend_modes);
			validateEnum("mode_alpha", mode_alpha, blend_modes);
			for (int i = 0; i < 8; i++) {
				BlendBufferState b = blend_state.buffers[i];
				b.channels[0].mode = mode_rgb;
				b.channels[1].mode = mode_rgb;
				b.channels[2].mode = mode_rgb;
				b.channels[3].mode = mode_alpha;
			}
		} catch (APIException e) {
			debugAPIError(e);
		}
	}
	
	@Override
	public void glBlendFunc(GLEnum sfactor, GLEnum dfactor) {
		glBlendFuncSeparate(sfactor, dfactor, sfactor, dfactor);
	}
	
	@Override
	public void glBlendFuncSeparate(GLEnum sfactor_rgb, GLEnum dfactor_rgb, GLEnum sfactor_alpha, GLEnum dfactor_alpha) {
		try {
			// TODO dual-source blending?
			validateEnum("sfactor_rgb", sfactor_rgb, blend_factors);
			validateEnum("dfactor_rgb", dfactor_rgb, blend_factors);
			validateEnum("sfactor_alpha", sfactor_alpha, blend_factors);
			validateEnum("dfactor_alpha", dfactor_alpha, blend_factors);
			for (int i = 0; i < 8; i++) {
				BlendBufferState b = blend_state.buffers[i];
				b.channels[0].sfactor = sfactor_rgb;
				b.channels[0].dfactor = dfactor_rgb;
				b.channels[1].sfactor = sfactor_rgb;
				b.channels[1].dfactor = dfactor_rgb;
				b.channels[2].sfactor = sfactor_rgb;
				b.channels[2].dfactor = dfactor_rgb;
				b.channels[3].sfactor = sfactor_alpha;
				b.channels[3].dfactor = dfactor_alpha;
			}
		} catch (APIException e) {
			debugAPIError(e);
		}
	}
	
	@Override
	public void glDrawArrays(GLEnum mode, int first, int count) {
		try {
			Framebuffer f = boundResource(GL_DRAW_FRAMEBUFFER, Framebuffer.class);
			VertexArray v = boundResource(GL_VERTEX_ARRAY, VertexArray.class);
			validateValueRange("first", first, 0);
			validateValueRange("count", count, 0);
			// 'turn off' any ibo
			v.indexType(null);
			// set primitive mode
			v.primitiveMode(mode);
			DrawCommand.invokeDraw(this, f, v, current_program, first, count);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glDrawElements(GLEnum mode, int count, GLEnum type, int offset) {
		try {
			Framebuffer f = boundResource(GL_DRAW_FRAMEBUFFER, Framebuffer.class);
			VertexArray v = boundResource(GL_VERTEX_ARRAY, VertexArray.class);
			// don't need this buffer itself, just need to ensure it exists
			boundResource(GL_ELEMENT_ARRAY_BUFFER, Buffer.class);
			validateValueRange("count", count, 0);
			validateValueRange("offset", offset, 0);
			// switch ibo to correct type
			v.indexType(type);
			// set primitive mode
			v.primitiveMode(mode);
			// offset is in bytes, convert to vertex count in ibo
			// this will be fine as long as offset is a multiple of the ibo type's size
			final int first = offset / DataFormat.sizeof(type);
			if (first * DataFormat.sizeof(type) != offset) {
				throw throwLimitation("offset (%d), must be multiple of sizeof(type) (%d)", offset, DataFormat.sizeof(type));
			}
			DrawCommand.invokeDraw(this, f, v, current_program, first, count);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glFlush() {
		driver.flush();
	}

	@Override
	public void glFinish() {
		int name = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, GLBitfield.ZERO);
		glClientWaitSync(name, GLBitfield.ZERO, Long.MAX_VALUE);
		glDeleteSync(name);
	}
	
	@Override
	public int glFenceSync(GLEnum condition, GLBitfield flags) {
		try {
			validateEnum("condition", condition, GL_SYNC_GPU_COMMANDS_COMPLETE);
			validateValue("flags", flags.bits(), 0);
			int name = genName();
			Sync s = resource(name, Sync.class);
			driver.invokeFenceSync(s);
			return name;
		} catch (APIException e) {
			debugAPIError(e);
			return -1;
		}
	}

	@Override
	public void glDeleteSync(int sync) {
		try {
			// we are supposed to ignore 0, and...
			if (sync == 0) return;
			// generate INVALID_VALUE if its not a name of a sync object (kinda clumsy)
			resource(sync, Sync.class);
			deleteName(sync);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glWaitSync(int sync, GLBitfield flags, long timeout) {
		try {
			validateValue("flags", flags.bits(), 0);
			validateValue("timeout", timeout, GL_TIMEOUT_IGNORED_value);
			Sync s = resource(sync, Sync.class);
			driver.invokeWaitSync(s);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public GLEnum glClientWaitSync(int sync, GLBitfield flags, long timeout) {
		try {
			// timeout is supposed to be unsigned...
			if (timeout == GL_TIMEOUT_IGNORED.valueLong()) timeout = Long.MAX_VALUE;
			validateValueRange("timeout", timeout, 0L);
			Sync s = resource(sync, Sync.class);
			if (s.signaled()) return GL_ALREADY_SIGNALED;
			if (s.waitSignaled(timeout)) return GL_CONDITION_SATISFIED;
			return GL_TIMEOUT_EXPIRED;
		} catch (APIException e) {
			debugAPIError(e);
			return GL_WAIT_FAILED;
		}
	}

	@Override
	public int glCreateShader(GLEnum shadertype) {
		try {
			// TODO geometry shaders
			validateEnum("shadertype", shadertype, GL_VERTEX_SHADER, GL_FRAGMENT_SHADER);
			int name = genName();
			Shader s = resource(name, Shader.class);
			s.shaderType(shadertype);
			return name;
		} catch (APIException e) {
			debugAPIError(e);
			return 0;
		}
	}

	@Override
	public void glDeleteShader(int shader) {
		try {
			Shader s = resource(shader, Shader.class);
			// TODO delete shader
			// if attached to current program, have to mark for later deletion (name must remain valid)
			
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glShaderSource(int shader, String... strings) {
		try {
			Shader s = resource(shader, Shader.class);
			s.source(strings);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glCompileShader(int shader) {
		try {
			Shader s = resource(shader, Shader.class);
			s.compile();
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glGetShader(int shader, GLEnum pname, IntBuffer params) {
		try {
			Shader s = resource(shader, Shader.class);
			params.put(s.parami(pname));
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glGetShaderInfoLog(int shader, CharBuffer infolog) {
		try {
			Shader s = resource(shader, Shader.class);
			String text = s.infoLog();
			validateValueRange("remaining()", infolog.remaining(), text.length() + 1);
			infolog.put(text);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public int glCreateProgram() {
		try {
			int name = genName();
			resource(name, ShaderProgram.class);
			return name;
		} catch (APIException e) {
			debugAPIError(e);
			return 0;
		}
	}

	@Override
	public void glDeleteProgram(int program) {
		try {
			ShaderProgram p = resource(program, ShaderProgram.class);
			// TODO delete program
			// if in use, have to mark for later deletion (name must remain valid)
			
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glAttachShader(int program, int shader) {
		try {
			ShaderProgram p = resource(program, ShaderProgram.class);
			Shader s = resource(shader, Shader.class);
			p.attach(s);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glDetachShader(int program, int shader) {
		try {
			ShaderProgram p = resource(program, ShaderProgram.class);
			Shader s = resource(shader, Shader.class);
			p.detach(s);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glLinkProgram(int program) {
		try {
			ShaderProgram p = resource(program, ShaderProgram.class);
			p.link();
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glGetProgram(int program, GLEnum pname, IntBuffer params) {
		try {
			ShaderProgram p = resource(program, ShaderProgram.class);
			params.put(p.parami(pname));
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glGetProgramInfoLog(int program, CharBuffer infolog) {
		try {
			ShaderProgram p = resource(program, ShaderProgram.class);
			String text = p.infoLog();
			validateValueRange("remaining()", infolog.remaining(), text.length() + 1);
			infolog.put(text);
			infolog.put('\0');
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUseProgram(int program) {
		try {
			current_program = default_program;
			if (program == 0) return;
			ShaderProgram p = resource(program, ShaderProgram.class);
			p.validateLink();
			current_program = p;
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glProgramBinary(int program, int binaryformat, ByteBuffer binary, int length) {
		try {
			ShaderProgram p = resource(program, ShaderProgram.class);
			validateValue("binaryformat", binaryformat, GLUtil.GL_PROGRAM_CLASSNAME_I3D_value, GLUtil.GL_PROGRAM_CLASSFILE_I3D_value);
			switch (binaryformat) {
			case GLUtil.GL_PROGRAM_CLASSNAME_I3D_value:
				// load program accessor class from class name
				CharBuffer cb = binary.asCharBuffer();
				cb.limit(length / 2);
				String cname = cb.toString();
				Class<?> cls = null;
				try {
					 cls = Class.forName(cname);
				} catch (Exception e) {
					throw throwInvalidOperation("Could not load program class '%s': ", cname, e.getMessage());
				}
				p.accessorBody(cls);
				break;
			case GLUtil.GL_PROGRAM_CLASSFILE_I3D_value:
				// TODO classfile program binary format
				throw throwLimitation("program binary classfile not implemented yet");
			default:
				throw throwInvalidValue("unknown program binary format %d", binaryformat);
			}
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public int glGetUniformLocation(int program, String name) {
		try {
			ShaderProgram p = resource(program, ShaderProgram.class);
			return p.uniformLocation(name);
		} catch (APIException e) {
			debugAPIError(e);
			return -1;
		}
	}

	@Override
	public void glUniform1fv(int location, int count, FloatBuffer value) {
		try {
			current_program.uniform(location, count, 1, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform2fv(int location, int count, FloatBuffer value) {
		try {
			current_program.uniform(location, count, 2, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform3fv(int location, int count, FloatBuffer value) {
		try {
			current_program.uniform(location, count, 3, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform4fv(int location, int count, FloatBuffer value) {
		try {
			current_program.uniform(location, count, 4, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform1iv(int location, int count, IntBuffer value) {
		try {
			current_program.uniform(location, count, 1, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform2iv(int location, int count, IntBuffer value) {
		try {
			current_program.uniform(location, count, 2, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform3iv(int location, int count, IntBuffer value) {
		try {
			current_program.uniform(location, count, 3, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform4iv(int location, int count, IntBuffer value) {
		try {
			current_program.uniform(location, count, 4, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform1uiv(int location, int count, IntBuffer value) {
		try {
			current_program.uniform(location, count, 1, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform2uiv(int location, int count, IntBuffer value) {
		try {
			current_program.uniform(location, count, 2, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform3uiv(int location, int count, IntBuffer value) {
		try {
			current_program.uniform(location, count, 3, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniform4uiv(int location, int count, IntBuffer value) {
		try {
			current_program.uniform(location, count, 4, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniformMatrix2fv(int location, int count, boolean transpose, FloatBuffer value) {
		try {
			// TODO uniform matrix transpose
			if (transpose) throw throwLimitation("uniform matrix transpose not supported yet");
			current_program.uniform(location, 2 * count, 2, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniformMatrix3fv(int location, int count, boolean transpose, FloatBuffer value) {
		try {
			if (transpose) throw throwLimitation("uniform matrix transpose not supported yet");
			current_program.uniform(location, 3 * count, 3, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}

	@Override
	public void glUniformMatrix4fv(int location, int count, boolean transpose, FloatBuffer value) {
		try {
			if (transpose) throw throwLimitation("uniform matrix transpose not supported yet");
			current_program.uniform(location, 4 * count, 4, value);
		} catch (APIException e) {
			debugAPIError(e);
		}
	}
	
}





















