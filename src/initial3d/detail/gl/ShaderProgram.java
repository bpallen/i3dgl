package initial3d.detail.gl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.HashSet;

import initial3d.GLEnum;
import initial3d.GLUtil;
import initial3d.detail.Access;
import initial3d.detail.AccessFuture;
import initial3d.detail.Util;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.bytecode.Flag;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.StringInfoLog;
import initial3d.detail.glsl.asmjava.JavaProgramBuilder;
import initial3d.detail.glsl.intermediate.Environment;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.locations.ArrayLocation;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.Matrix;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Vector;

import static initial3d.GLEnum.*;
import static initial3d.GLEnumValues.*;

public class ShaderProgram extends Resource {
	
	public static final int UNIFORM_STORAGE_SIZE = 16384;

	private StringInfoLog infolog = new StringInfoLog();
	
	private final HashSet<Shader> shaders = new HashSet<>();
	private final HashMap<String, Integer> uniform_loc_cache = new HashMap<>();
	
	// we need to maintain uniform contents to allow partial updates from user
	// so we double buffer the uniforms, and replace the driver copy as needed to avoid stalling
	private final DataStore user_uniforms = DataStore.alloc(UNIFORM_STORAGE_SIZE, "uniforms");
	private DataStore driver_uniforms = DataStore.alloc(UNIFORM_STORAGE_SIZE, "uniforms");
	private boolean uniforms_dirty = false;
	
	// previously linked program needs to remain usable even after failed link
	private Class<?> accessor_class = ShaderProgramAccessor.class;
	private JavaProgramBuilder builder = null;
	boolean linked = false;
	int uniform_size = 0;
	
	public ShaderProgram(I3DGL gl_, int name_) {
		super(gl_, name_);
	}

	public int uniformLocation(String u) {
		// TODO querying locations after failed link?
		validateLink();
		if (builder == null) return -1;
		Integer li0 = uniform_loc_cache.get(u);
		if (li0 != null) return li0;
		// go through shaders to get proper names
		// note that uniform names could (edge cases) be different between shaders,
		// so we go through them until we find the first match and just use that.
		Location l = null;
		for (GLEnum stype : GLUtil.array(GL_VERTEX_SHADER, GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER)) {
			Environment env = builder.environment(stype);
			if (env != null) {
				l = env.uniforms().apiLocation(u);
				if (l != null) break;
			}
		}
		// FIXME array -> [0]
		int li = l != null ? l.id() : -1;
		uniform_loc_cache.put(u, li);
		return li;
	}
	
	public void attach(Shader s) {
		shaders.add(s);
	}
	
	public void detach(Shader s) {
		shaders.remove(s);
	}
	
	public void link() {
		infolog = new StringInfoLog();
		builder = new JavaProgramBuilder(infolog);
		linked = false;
		for (Shader s : shaders) {
			Scope gs = s.globalScope();
			if (gs == null) {
				// shader not compiled
				gl().debugAPIInfo("can't link: shader %d is not compiled", s.name());
				builder = null;
				return;
			}
			builder.addShader(s.shaderType(), gs);
		}
		try {
			builder.build();
			if (builder.uniforms().elementAllocSize(0) + JavaProgramBuilder.UNIFORM_BASE_OFFSET > UNIFORM_STORAGE_SIZE) {
				gl().debugAPIInfo("can't link: uniform storage limit exceeded");
				return;
			}
			// need to leave class intact on failure
			Class<?> cls = builder.programClass();
			// check class loads correctly
			cls.newInstance();
			accessor_class = cls;
			uniform_size = JavaProgramBuilder.UNIFORM_BASE_OFFSET + builder.uniforms().elementAllocSize(0);
			uniforms_dirty = true;
			// linking zeros all uniforms
			// TODO uniform variable initializers
			user_uniforms.memset((byte) 0);
			linked = true;
		} catch (BuildException e) {
			// normal link failure
			infolog.error("error encountered, aborting");
		} catch (Exception e) {
			infolog.internalError("exception encountered while linking: " + e.getMessage());
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			infolog.detail(sw.toString());
		}
		if (infolog.errorCount() > 0) {
			infolog.info(String.format(
				"Program linking failed with %d errors and %d warnings",
				infolog.errorCount(), infolog.warningCount())
			);
			gl().debugAPIInfo("linking failed for program %d", name());
			return;
		}
	}
	
	public void accessorBody(Class<?> cls) {
		// TODO this is currently broken; uniform locations arent compatible atm
		accessor_class = cls;
		builder = null;
		linked = true;
	}
	
	public int parami(GLEnum pname) {
		gl().validateEnum("pname", pname, GL_LINK_STATUS, GL_INFO_LOG_LENGTH);
		switch (pname.valueInt()) {
		case GL_LINK_STATUS_value:
			return linked ? 1 : 0;
		case GL_INFO_LOG_LENGTH_value:
			return infolog.str().length() + 1;
		default:
			return 0;
		}
	}
	
	public String infoLog() {
		return infolog.str();
	}
	
	public void validateLink() {
		if (!linked) throw gl().throwInvalidOperation("program %d is not linked", name());
	}
	
	private Location validateUniformLocation(int locid, int components, Class<?> cls) {
		// FIXME matrix locations
		if (builder == null) throw gl().throwInvalidOperation("program link not attempted");
		Location l = builder.uniforms().apiLocation(locid);
		String valtypestr = "";
		if (l.type().definition() instanceof Vector) {
			Vector vec = (Vector) l.type().definition();
			if (vec.vecSize() != components) {
				throw gl().throwInvalidOperation("location %d has type %s, %d components supplied", locid, l.type().plainText(), components);
			}
			valtypestr = vec.valType().name().plainText();
		} else if (l.type().definition() instanceof Matrix) {
			Matrix mat = (Matrix) l.type().definition();
			if (mat.matRows() != components) {
				throw gl().throwInvalidOperation("location %d has type %s, %d components supplied", locid, l.type().plainText(), components);
			}
			valtypestr = mat.valType().name().plainText();
		} else if (l.type().definition() instanceof BuiltinType) {
			if (components != 1) {
				throw gl().throwInvalidOperation("location %d has type %s, %d components supplied", locid, l.type().plainText(), components);
			}
			valtypestr = l.type().plainText();
		} else {
			throw gl().throwInvalidOperation("location %d is neither primitive nor vector");
		}
		boolean badtype = false;
		// TODO samplers
		if ("float".equals(valtypestr) && cls != float.class) badtype = true;
		if ("int".equals(valtypestr) && cls != int.class) badtype = true;
		if ("uint".equals(valtypestr) && cls != int.class) badtype = true;
		if ("bool".equals(valtypestr) && cls != int.class) badtype = true;
		if (badtype) throw gl().throwInvalidOperation("location %d has value type %s, %s supplied", locid, valtypestr, cls.getSimpleName());
		return l;
	}
	
	private int validateUniformArraySize(Location l, int count) {
		if (count == 1) return 1;
		gl().validateValueRange("count", count, 0);
		if (l.type().definition() instanceof Matrix) {
			Matrix mat = (Matrix) l.type().definition();
			if (mat.matCols() == count) return count;
		}
		if (l.parent() instanceof ArrayLocation) {
			ArrayLocation al = (ArrayLocation) l.parent();
			int asz = al.type().size();
			if (asz < 0) throw gl().throwInvalidOperation("location has bad array size %d", asz);
			return Math.min(count, asz);
		} else {
			throw gl().throwInvalidOperation("location %d (type %s) is not an array member", l.id(), l.type().plainText());
		}
	}
	
	public void uniform(int locid, int count, int components, FloatBuffer data) {
		if (locid == -1) return;
		validateLink();
		Location l = validateUniformLocation(locid, components, float.class);
		count = validateUniformArraySize(l, count);
		gl().validateValueRange("remaining()", data.remaining(), count * components);
		gl().validateValueRange("components", components, 0, 4);
		FloatBuffer f = user_uniforms.map(JavaProgramBuilder.UNIFORM_BASE_OFFSET + l.address()).asFloatBuffer();
		for (int i = 0; i < count; i++) {
			for (int j = 0; j < components; j++) {
				f.put(data.get());
			}
			// no padding
		}
		uniforms_dirty = true;
	}
	
	public void uniform(int locid, int count, int components, IntBuffer data) {
		if (locid == -1) return;
		validateLink();
		Location l = validateUniformLocation(locid, components, int.class);
		count = validateUniformArraySize(l, count);
		gl().validateValueRange("remaining()", data.remaining(), count * components);
		gl().validateValueRange("components", components, 0, 4);
		IntBuffer f = user_uniforms.map(JavaProgramBuilder.UNIFORM_BASE_OFFSET + l.address()).asIntBuffer();
		for (int i = 0; i < count; i++) {
			for (int j = 0; j < components; j++) {
				f.put(data.get());
			}
			// no padding
		}
		uniforms_dirty = true;
	}
	
	@Override
	public void prepare(Command c, Access a) {
		//validateLink();
		if (uniforms_dirty) {
			// try update existing driver-side uniform storage
			AccessFuture af = driver_uniforms.requestAccess(Access.UNIQUE_WRITE);
			if (!af.tryAcquire()) {
				// can't write atm => new storage
				af.cancel();
				af = null;
				gl().driver().invokeFree(driver_uniforms);
				driver_uniforms = null;
				driver_uniforms = DataStore.alloc(UNIFORM_STORAGE_SIZE, "uniforms");
			}
			// update
			driver_uniforms.map(0, uniform_size).put(user_uniforms.map(0, uniform_size));
			//System.err.println("uniform mat\n" + GLUtil.getMat4(Util.mapBuffer(driver_uniforms.address() + 128, 64).asFloatBuffer()));
			// done
			if (af != null) af.release();
			uniforms_dirty = false;
		}
		c.uniforms(driver_uniforms);
	}

	@Override
	public Class<?> accessorInterface() {
		return ShaderProgramAccessor.class;
	}
	
	@Override
	protected Object createAccessor() throws Exception {
		// if the supplied class implements the accessor interface, use directly
		// otherwise treat it as a container for static methods and build the accessor from them
		if (ShaderProgramAccessor.class.isAssignableFrom(accessor_class)) {
			return accessor_class.newInstance();
		} else {
			return super.createAccessor();
		}
	}
	
	@Override
	protected void buildAccessor(ClassBuilder cb) throws Exception {
		// for each static method in the interface, delegate to the user-specified
		// class if it has that method else delegate to the interface base method
		for (Method m : ShaderProgramAccessor.class.getDeclaredMethods()) {
			if (!Modifier.isPublic(m.getModifiers())) continue;
			if (!Modifier.isStatic(m.getModifiers())) continue;
			MethodBuilder mb = cb.declareMethod();
			mb.declareFlag(Flag.PUBLIC);
			mb.declareName(m.getName());
			mb.declareType(true, m.getReturnType(), m.getParameterTypes());
			// load params
			mb.maxStack(mb.scope().nextIndex());
			for (int i = 0; i < m.getParameterCount(); i++) {
				mb.load(m.getParameterTypes()[i], "arg" + i);
			}
			// forward
			try {
				mb.invokeStatic(accessor_class, m.getName(), m.getReturnType(), m.getParameterTypes());
			} catch (NoSuchMethodException e) {
				mb.invokeStatic(ShaderProgramAccessor.class, m.getName(), m.getReturnType(), m.getParameterTypes());
			}
			mb.ret(m.getReturnType());
		}
	}
	
}









