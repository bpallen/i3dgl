package initial3d.detail.gl;

import initial3d.detail.Access;

public interface CommandPrereq {

	public void prepare(Command c, Access a);
	
}
