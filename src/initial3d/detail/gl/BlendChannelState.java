package initial3d.detail.gl;

import static initial3d.GLEnum.*;
import static initial3d.GLEnumValues.*;

import initial3d.GLEnum;
import initial3d.detail.SimpleCloneable;

public class BlendChannelState extends SimpleCloneable<BlendChannelState> {
	
	GLEnum mode = GL_FUNC_ADD;
	GLEnum sfactor = GL_ONE;
	GLEnum dfactor = GL_ZERO;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dfactor == null) ? 0 : dfactor.hashCode());
		result = prime * result + ((mode == null) ? 0 : mode.hashCode());
		result = prime * result + ((sfactor == null) ? 0 : sfactor.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		BlendChannelState other = (BlendChannelState) obj;
		if (dfactor != other.dfactor) return false;
		if (mode != other.mode) return false;
		if (sfactor != other.sfactor) return false;
		return true;
	}
	
	@Override
	public String toString() {
		switch (mode.valueInt()) {
		case GL_FUNC_ADD_value:
			return "s*" + sfactor + " + " + "d*" + dfactor;
		case GL_FUNC_SUBTRACT_value:
			return "s*" + sfactor + " - " + "d*" + dfactor;
		case GL_FUNC_REVERSE_SUBTRACT_value:
			return "d*" + dfactor + " - " + "s*" + sfactor;
		case GL_MIN_value:
			return "min(s*" + sfactor + ", " + "d*" + dfactor + ")";
		case GL_MAX_value:
			return "max(s*" + sfactor + ", " + "d*" + dfactor + ")";
		default:
			return "?";	
		}
	}
}
