package initial3d.detail.gl;

public abstract class FramebufferAccessor {
	
	// TODO readpixels stuff?
	// TODO early frag tests
	
	public abstract void clearColorf(float r, float g, float b, float a);
	
	public abstract void clearColori(int r, int g, int b, int a);
	
	public abstract void clearColoru(int r, int g, int b, int a);
	
	public abstract void clearDepth(float z);
	
	/**
	 * 
	 * @param pblocktemp
	 * @param x window x-coord for block lower-left
	 * @param y window y-coord for block lower-left
	 * @param layer
	 * @param fullcover true if block will be fully covered by rasterizer with no shader discards
	 * @return true if rasterizer should proceed with this block
	 */
	public abstract boolean setupBlock(long pblocktemp, int x, int y, int layer, int worker_id, boolean fullcover);
	
	/**
	 * Fragment outputs for each draw buffer should appear in successive 4-component 64-byte locations
	 * where each component is 4 bytes followed immediately by 12 bytes of other data. This is as a
	 * result of interleaving 16-byte locations for 4 fragments. <code>pfrag_in</code> always points
	 * to the first component to be used.
	 * 
	 * @param accept if 0, fragment should be discarded; if 1, fragment should be accepted
	 * subject to passing further tests; anything else is undefined behaviour
	 * @param xx block local x-coord, ie [0, {@link Rasterizer#blockSize()})
	 * @param yy block local y-coord, ie [0, {@link Rasterizer#blockSize()})
	 * @param layer
	 * @param depth
	 * @param pblocktemp
	 * @param pfrag_in
	 * @return true if fragment passed all tests
	 */
	public abstract boolean writeFragment(int accept, int xx, int yy, float depth, long pblocktemp, long pfrag_in);
	
}
