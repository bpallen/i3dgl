package initial3d.detail.gl;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import initial3d.Functions;
import initial3d.GLEnum;
import initial3d.Vec2i;
import initial3d.Vec3f;
import initial3d.Vec3i;
import initial3d.detail.Access;
import initial3d.detail.SimpleCloneable;
import initial3d.detail.Util;
import initial3d.detail.bytecode.Flag;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;
import static initial3d.Functions.*;
import static initial3d.GLEnum.*;

public class Texture extends Resource {
	
	public static final int MAX_LEVEL = 12;
	public static final int MAX_SIZE = 1 << MAX_LEVEL;
	
	// missing: swizzles and some other stuff
	
	public Vec3f border_color = Vec3f.zero;
	// default should be LINEAR and NEAREST_MIPMAP_LINEAR
	public GLEnum mag_filter = GLEnum.GL_NEAREST;
	public GLEnum min_filter = GLEnum.GL_NEAREST;
	// default should be REPEAT except for rectangular textures where CLAMP_TO_EDGE
	// ... we probably won't support rectangular textures
	public GLEnum wrap_s = GLEnum.GL_CLAMP_TO_EDGE;
	public GLEnum wrap_t = GLEnum.GL_CLAMP_TO_EDGE;
	public GLEnum wrap_r = GLEnum.GL_CLAMP_TO_EDGE;
	// lod range for sampling
	public float min_lod = -1000.f;
	public float max_lod = 1000.f;
	// defined levels; base level is effectively lod0
	// these define the range of images that are actually used for sampling
	public int base_level = 0;
	public int max_level = 1000;
	
	// mipmap images
	// these images are owned by this texture; their names are not externally visible
	// if empty, the accessor pretends to be 0x0x0 and always returns (0,0,0,?)
	private final ArrayList<Image> images = new ArrayList<>();
	private GLEnum format = GL_RGBA8;
	
	private GLEnum target;
	
	public Texture(I3DGL gl_, int name_) {
		super(gl_, name_);
	}
	
	public void target(GLEnum e) {
		if (target != null) {
			throw gl().throwInvalidOperation("texture %d was created with target %s; cannot bind to target %s", name(), target, e);
		}
		target = e;
	}
	
	public GLEnum target() {
		return target;
	}
	
	public Image image(int level) {
		gl().validateValueRange("level", level, 0, MAX_LEVEL);
		// ensure images up to level all exist
		while (images.size() <= level) {
			images.add(gl().resource(gl().genName(), Image.class));
			markAccessorDirty();
		}
		
		// FIXME temp test
		accessor();
		
		return images.get(level);
	}
	
	@Override
	public void prepare(Command c, Access a) {
		// TODO check mipmap completeness
		// images at all required levels, right sequence of sizes, all same internal format
		for (Image img : images) {
			c.require(img, a);
		}
	}
	
	@Override
	public List<Image> accessorDependencies() {
		return Collections.unmodifiableList(images);
	}
	
	@Override
	public void onDeleteName() {
		for (Image img : images) {
			gl().deleteName(img.name());
		}
		super.onDeleteName();
	}
	
	@Override
	public Class<?> accessorInterface() {
		return SamplerAccessor.class;
	}
	
	@Override
	protected void buildAccessor(ClassBuilder cb) throws Exception {
		if (base_level >= images.size()) {
			gl().debugAPIInfo("texture %d has no image at base level %d", name(), base_level);
			return;
		}
		buildTexture(cb);
		buildTexelFetch(cb);
		buildTextureSize(cb);
	}
	
	private static boolean imgInterp(GLEnum filter) {
		return filter == GL_LINEAR || filter == GL_LINEAR_MIPMAP_NEAREST
			|| filter == GL_LINEAR_MIPMAP_LINEAR;
	}
	
	private static boolean mipInterp(GLEnum filter) {
		return filter == GL_NEAREST_MIPMAP_NEAREST || filter == GL_NEAREST_MIPMAP_LINEAR
			|| filter == GL_LINEAR_MIPMAP_NEAREST || filter == GL_LINEAR_MIPMAP_LINEAR;
	}
	
	/*
	 * [int] -> [int]
	 */
	private void buildWrap(MethodBuilder mb, GLEnum w, int size) throws Exception {
		if (w == GL_CLAMP_TO_EDGE) {
			mb.constant(0);
			mb.imax();
			mb.constant(size);
			mb.imin();
		} else if (w == GL_REPEAT) {
			// TODO optimize? pot should be ok
			mb.constant(size);
			mb.op(Op.IREM);
		} else {
			// TODO other texture wrappings
			throw new RuntimeException("unsupported texture wrapping");
		}
	}
	
	/**
	 * [] -> []
	 * 
	 * @param mb
	 * @param img
	 * @param locpos first of 3 local vars [int x, int y, int z] (unwrapped)
	 * @param locout first of 4 local vars for output
	 * @throws Exception
	 */
	private void buildImageLoad(MethodBuilder mb, Image img, int locpos, int locout) throws Exception {
		if (img.address() == 0 || any(img.size().eq(Vec3i.zero))) {
			// image is null-ish
			for (int i = 0; i < 4; i++) {
				if (DataFormat.integral(img.format())) {
					mb.constant(0);
					mb.istore(locout + i);
				} else {
					mb.constant(0.f);
					mb.fstore(locout + i);
				}
			}
			return;
		}
		mb.iload(locpos);
		buildWrap(mb, wrap_r, img.size().x);
		mb.iload(locpos + 1);
		buildWrap(mb, wrap_s, img.size().y);
		// TODO ignore z if 2d only (need support from buildtexelindex)
		mb.iload(locpos + 2);
		buildWrap(mb, wrap_t, img.size().z);
		img.buildTexelIndex(mb);
		mb.constant(DataFormat.sizeof(img.format()));
		mb.op(Op.IMUL);
		mb.op(Op.I2L);
		mb.constant(img.address());
		mb.op(Op.LADD);
		DataFormat.buildGetTexel(mb, locout, img.format());
	}
	
	/**
	 * [] -> []
	 * 
	 * @param mb
	 * @param img
	 * @param filter
	 * @param locposf first of 3 local vars [float x, float y, float z] (0..1)
	 * @param locout first of 4 local vars for output
	 * @throws Exception
	 */
	private void buildImageSample(MethodBuilder mb, Image img, GLEnum filter, int locposf, int locout) throws Exception {
		mb.pushScope();
		try {
			// convert float 0..1 to int texel coords (still unwrapped)
			int locposi = mb.scope().alloc(3);
			int locfrac = mb.scope().alloc(3);
			for (int i = 0; i < 3; i++) {
				mb.fload(locposf + i);
				mb.constant((float) img.size().x);
				mb.op(Op.FMUL);
				if (imgInterp(format)) {
					// if interpolating, we just want floor
					// dup to enable subsequent ops
					mb.op(Op.DUP);
				} else {
					// otherwise, we want nearest
					mb.constant(0.5f);
					mb.op(Op.FADD);
				}
				// java math has no floor(float) *tableflip*
				mb.op(Op.F2D);
				mb.dmath("floor", 1);
				mb.op(Op.D2F);
				if (imgInterp(format)) {
					// if interpolating, store the fractional coord too
					mb.op(Op.DUP);
					mb.op(Op.F2I);
					mb.istore(locposi + i);
					mb.op(Op.FSUB);
					mb.fstore(locfrac + i);
				} else {
					mb.op(Op.F2I);
					mb.istore(locposi + i);
				}
			}
			if (DataFormat.integral(img.format()) || !imgInterp(filter)) {
				// TODO test integral in imgInterp ?
				// couldnt or shouldnt interpolate
				buildImageLoad(mb, img, locposi, locout);
			} else {
				// TODO interpolation
				// TODO ignore z if 2d only
				
			}
			
		} finally {
			mb.popScope();
		}
	}
	
	private void buildTexture(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("texture_static");
		mb.declareFlag(Flag.PUBLIC);
		// void texture(long pout, float x, float y, float z, float lod);
		mb.declareType(true, void.class, long.class, float.class, float.class, float.class, float.class);
		mb.declareParamNames("pout", "x", "y", "z", "lod");
		
		// TODO need separate magnify/minify code paths
		// TODO properly
		
		// temp: just sample from base image
		int locout = mb.scope().alloc(4);
		buildImageSample(mb, images.get(base_level), mag_filter, mb.scope().get("x"), locout);
		mb.lload("pout");
		// TODO typechecking
		DataFormat.buildPutTexel(mb, locout, GL_RGBA32F);
		
		mb.op(Op.RETURN);
	}
	
	private void buildTexelFetch(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("texelFetch_static");
		mb.declareFlag(Flag.PUBLIC);
		// void texelFetch(long pout, int x, int y, int z, int lod);
		mb.declareType(true, void.class, long.class, int.class, int.class, int.class, int.class);
		mb.declareParamNames("pout", "x", "y", "z", "lod");
		
		// TODO
		
		mb.op(Op.RETURN);
	}
	
	private void buildTextureSize(ClassBuilder cb) {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("textureSize_static");
		mb.declareFlag(Flag.PUBLIC);
		// void textureSize(long pout, int lod);
		mb.declareType(true, void.class, long.class, int.class);
		mb.declareParamNames("pout", "lod");
		
		// TODO
		
		mb.op(Op.RETURN);
	}
	
}
