package initial3d.detail.gl;

import initial3d.detail.Util;

/**
 * <p>
 * Compiled shader program. Subclasses are expected to re-implement the static methods
 * and override the instance methods to delegate to the static ones.
 * See {@link Resource#buildAccessor(initial3d.detail.bytecode.ClassBuilder)}.
 * </p>
 * 
 * <p>
 * To implement a vertex shader, override:
 * <ul>
 * <li>{@link #vertexOutSize}</li>
 * <li>{@link #shadeVertex}</li>
 * </ul>
 * The default vertex behaviour is to copy the first vertex attribute as gl_Position.
 * </p>
 * 
 * <p>
 * To implement a geometry shader, override:
 * <ul>
 * <li>{@link #geometryOutSize}</li>
 * <li>{@link #geometryMaxPrimitives}</li>
 * <li>{@link #geometryMaxVertices}</li>
 * <li>{@link #geometryPrimitiveVertexCount}</li>
 * <li>{@link #shadeGeometry}</li>
 * </ul>
 * The default geometry behaviour is just to strip any adjacency.
 * </p>
 * 
 * <p>
 * When implementing vertex or geometry shaders, override:
 * <ul>
 * <li>{@link #geometryOutSize}</li>
 * <li>{@link #varyingInterpolate2}</li>
 * </ul>
 * {@link #geometryOutSize()} is used even if no geometry shader is implemented.
 * In that case, it should return the same value as {@link #vertexOutSize()}.
 * </p>
 * 
 * <p>
 * To implement a fragment shader, override:
 * <ul>
 * <li>{@link #varyingInterpolate3}</li>
 * <li>{@link #varyingSub}</li>
 * <li>{@link #varyingAdd}</li>
 * <li>{@link #varyingMul}</li>
 * <li>{@link #varyingCopy}</li>
 * <li>{@link #shadeFragmentQuad}</li>
 * </ul>
 * </p>
 * 
 * @author ben
 *
 */
public class ShaderProgramAccessor {

	public static int vertexOutSize_static() {
		return 16;
	}
	
	public static int geometryOutSize_static() {
		return vertexOutSize_static();
	}
	
	public static int geometryMaxPrimitives_static(int vao_prim_verts) {
		return 1;
	}
	
	public static int geometryMaxVertices_static(int vao_prim_verts) {
		// default does not produce new vertices
		return 0;
	}
	
	public static int geometryPrimitiveVertexCount_static(int vao_prim_verts) {
		switch (vao_prim_verts) {
		case 1:
			// points
			return 1;
		case 2:
			// lines
			return 2;
		case 3:
			// triangles
			return 3;
		case 4:
			// lines_adjacency
			return 2;
		case 6:
			// triangles_adjacency
			return 3;
		default:
			return 0;
		}
	}
	
	public static void varyingInterpolate2_static(long pvary_out, long pvary0_in, long pvary1_in, float k0, float k1, float w0, float w1, float w) {
		
	}
	
	public static void varyingInterpolate3_static(long pvary_out, long pvary0_in, long pvary1_in, long pvary2_in, float k0, float k1, float k2, float iw0, float iw1, float iw2) {
		
	}
	
	public static void varyingSub_static(long pderiv_out, long pvary0_in, long pvary1_in) {
		
	}
	
	public static void varyingAdd_static(long pvary_out, long pvary_in, long pderiv_in) {
		
	}
	
	public static void varyingMul_static(long pderiv_out, long pderiv_in, float k) {
		
	}
	
	@Deprecated
	public static void varyingFma_static(long pvary_out, long pderiv_in, float k, long pvary_in) {
		
	}
	
	public static void varyingCopy_static(long pvary_out, long pvary_in) {
		
	}
	
	public static void shadeVertex_static(long pvert_out, long ptemp, long puniforms, long pattrib_in) {
		Util.copy16(pvert_out, pattrib_in + 16);
	}
	
	public static int shadeGeometry_static(long ppverts_out, long pverts_out, long ptemp, long puniforms, long ppverts_in, int vao_prim_verts) {
		// TODO this switch could be baked into something somewhere
		switch (vao_prim_verts) {
		case 1:
			// points
			Util.copy8(ppverts_out, ppverts_in);
			break;
		case 2:
			// lines
			Util.copy16(ppverts_out, ppverts_in);
			break;
		case 3:
			// triangles
			Util.copy24(ppverts_out, ppverts_in);
			break;
		case 4:
			// lines_adjacency: outer vertices are adjacency
			Util.copy16(ppverts_out, ppverts_in + 8);
			break;
		case 6:
			// triangles_adjacency: every 2nd vertex is adjacency
			Util.copy8(ppverts_out, ppverts_in);
			Util.copy8(ppverts_out + 8, ppverts_in + 16);
			Util.copy8(ppverts_out + 16, ppverts_in + 32);
			break;
		default:
			break;
		}
		// default always produces 1 primitive and 0 new vertices
		return 1 << 16;
	}
	
	public static int shadeFragmentQuad_static(
		long pfrag_out, long ptemp, long puniforms, long pvary, long pvary_ddx, long pvary_ddy,
		int x, int y,
		float z, float dz_dx, float dz_dy,
		float iw, float diw_dx, float diw_dy
	) {
		return 0xF;
	}
	
	/**
	 * Get the size of a vertex produced by {@link #shadeVertex}. The first 16 bytes are used for <code>gl_Position</code>.
	 * 
	 * @return Size in bytes.
	 */
	public int vertexOutSize() {
		return vertexOutSize_static();
	}
	
	/**
	 * Get the size of a vertex produced by {@link #shadeGeometry}. The first 16 bytes are used for <code>gl_Position</code>.
	 * If the geometry shader is just passing through vertices from the vertex shader, this should return the same value
	 * as {@link #vertexOutSize}.
	 * 
	 * @return Size in bytes.
	 */
	public int geometryOutSize() {
		return geometryOutSize_static();
	}
	
	/**
	 * Get max number of primitives produced by one invocation of {@link #shadeGeometry},
	 * irrespective of how many new vertices are produced.
	 * 
	 * @param vao_prim_verts Number of vertices in input primitive
	 * @return
	 */
	public int geometryMaxPrimitives(int vao_prim_verts) {
		return geometryMaxPrimitives_static(vao_prim_verts);
	}
	
	/**
	 * Get max number of new vertices produced by one invocation of {@link #shadeGeometry}.
	 * 
	 * @param vao_prim_verts Number of vertices in input primitive
	 * @return
	 */
	public int geometryMaxVertices(int vao_prim_verts) {
		return geometryMaxVertices_static(vao_prim_verts);
	}
	
	/**
	 * Get the number of vertices in each primitive produced by {@link #shadeGeometry}.
	 * Returns 1 for points, 2 for lines, 3 for triangles. Only those three primitive
	 * types are allowed.
	 * 
	 * @param vao_prim_verts Number of vertices in input primitive
	 * @return 1, 2, or 3
	 */
	public int geometryPrimitiveVertexCount(int vao_prim_verts) {
		return geometryPrimitiveVertexCount_static(vao_prim_verts);
	}
	
	/**
	 * Interpolate varyings between 2 geometry stage outputs (or vertex stage if no geometry shader).
	 * Runs in clip space, so perspective varyings should be interpolated directly, while
	 * noperspective varyings should be multiplied by w first (and divided afterwards; results
	 * should represent 'complete' interpolation). By default, does nothing.
	 * 
	 * @param pvary_out
	 * @param pvary0_in
	 * @param pvary1_in
	 * @param k0
	 * @param k1
	 * @param w0
	 * @param w1
	 * @param w interpolated w value
	 */
	public void varyingInterpolate2(long pvary_out, long pvary0_in, long pvary1_in, float k0, float k1, float w0, float w1, float w) {
		varyingInterpolate2_static(pvary_out, pvary0_in, pvary1_in, k0, k1, w0, w1, w);
	}
	
	/**
	 * Interpolate varyings between 3 geometry stage outputs (or vertex stage if no geometry shader).
	 * Runs in screen space, so perspective varyings should be divided by w first,
	 * while noperspective varyings should be interpolated directly. Perspective varyings should be
	 * left divided by w, as interpolation will continue later. By default, does nothing.
	 * 
	 * @param pvary_out
	 * @param pvary0_in
	 * @param pvary1_in
	 * @param pvary2_in
	 * @param k0
	 * @param k1
	 * @param k2
	 * @param iw0 <code>1.f / w0</code>
	 * @param iw1 <code>1.f / w1</code>
	 * @param iw2 <code>1.f / w2</code>
	 */
	public void varyingInterpolate3(long pvary_out, long pvary0_in, long pvary1_in, long pvary2_in, float k0, float k1, float k2, float iw0, float iw1, float iw2) {
		varyingInterpolate3_static(pvary_out, pvary0_in, pvary1_in, pvary0_in, k0, k1, k2, iw0, iw1, iw2);
	}
	
	/**
	 * Subtract varyings for (screen-space) derivatives.
	 * By default, does nothing.
	 * 
	 * @param pvary_out
	 * @param pvary0_in
	 * @param pvary1_in
	 */
	public void varyingSub(long pvary_out, long pvary0_in, long pvary1_in) {
		varyingSub_static(pvary_out, pvary0_in, pvary1_in);
	}
	
	/**
	 * Add derivatives to varyings.
	 * By default, does nothing.
	 * 
	 * @param pvary_out
	 * @param pvary_in
	 * @param pderiv_in
	 */
	public void varyingAdd(long pvary_out, long pvary_in, long pderiv_in) {
		varyingAdd_static(pvary_out, pvary_in, pderiv_in);
	}
	
	/**
	 * Multiply varying derivatives by a constant.
	 * By default, does nothing.
	 * 
	 * @param pderiv_out
	 * @param pderiv_in
	 * @param k
	 */
	public void varyingMul(long pderiv_out, long pderiv_in, float k) {
		varyingMul_static(pderiv_out, pderiv_in, k);
	}
	
	/**
	 * Multiply derivatives by a constant and add to varyings.
	 * By default, does nothing.
	 * Only used by old rasterizer.
	 * 
	 * @param pvary_out
	 * @param pderiv_in
	 * @param k
	 * @param pvary_in
	 */
	@Deprecated
	public void varyingFma(long pvary_out, long pderiv_in, float k, long pvary_in) {
		varyingFma_static(pvary_out, pderiv_in, k, pvary_in);
	}
	
	/**
	 * Copy varyings.
	 * By default, does nothing.
	 * 
	 * @param pvary_out
	 * @param pvary_in
	 */
	public void varyingCopy(long pvary_out, long pvary_in) {
		varyingCopy_static(pvary_out, pvary_in);
	}
	
	/**
	 * Vertex shader. The first 16 bytes of <code>pvert_out</code> is used
	 * for <code>gl_Position</code>. The first 16 bytes of <code>pattrib_in</code>
	 * contains the vertex id (int) and instance id (int). Fetched, decoded vertex attributes
	 * appear at subsequent 16 byte offsets. Attributes are padded to 4 components with
	 * the default values (0,0,0,1), represented as int or float to match the
	 * attribute.
	 * 
	 * @param pvert_out Output vertex pointer
	 * @param ptemp Temporary storage
	 * @param puniforms Uniform storage
	 * @param pattrib_in Input vertex attributes pointer
	 */
	public void shadeVertex(long pvert_out, long ptemp, long puniforms, long pattrib_in) {
		shadeVertex_static(pvert_out, ptemp, puniforms, pattrib_in);
	}
	
	/**
	 * Geometry shader. Produces a sequence of primitives that are all either
	 * points, lines or triangles (as pointers to actual vertices).
	 * Must decode primitives like triangles_adjacency to one of the above types.
	 * Strips must therefore be represented as a sequence of the base primitive.
	 * The first 16 bytes of each new vertex is used for <code>gl_Position</code>.
	 * New vertices should be written to <code>pverts_out</code> at increasing addresses.
	 * 
	 * @param ppverts_out Pointer to array of output vertex pointers defining output primitives
	 * @param pverts_out Pointer to storage for new vertices
	 * @param ptemp Temporary storage
	 * @param puniforms Uniform storage
	 * @param ppverts_in Pointer to array of input vertex pointers defining input primitives
	 * @param vao_prim_verts Number of vertices in input primitive
	 * @return <code>(primitives << 16) | vertices</code> where <code>primitives</code> is the number
	 * of <i>primitives</i> written to <code>ppverts_out</code> and <code>vertices</code> is the number
	 * of new <i>vertices</i> written to <code>pverts_out</code>
	 */
	public int shadeGeometry(long ppverts_out, long pverts_out, long ptemp, long puniforms, long ppverts_in, int vao_prim_verts) {
		return shadeGeometry_static(ppverts_out, pverts_out, ptemp, puniforms, ppverts_in, vao_prim_verts);
	}
	
	/**
	 * <p>
	 * Shade a 2x2 quad of fragments. <code>pfrag_out</code> has all 4 fragments interleaved.
	 * The first fragment is at the lower-left, then lower-right, upper-left and upper-right.
	 * Fragment outputs are 4-interleaved vectors of int/float at 64 byte offsets.
	 * <code>pvary</code> only specifies the interpolated values for the lower-left fragment.
	 * The other fragments need to be reconstructed from the derivatives, multiplying by <code>w</code>
	 * if needed for perspective interpolation. <code>w</code> itself also needs to be reconstructed
	 * from <code>iw == 1.f / w</code> and its derivatives.
	 * </p>
	 * 
	 * <p>
	 * The return value is a bitmask that indicates which fragments were not discarded; bit 0 corresponds
	 * to the first fragment etc. A return of 0xF therefore means all fragments should be accepted,
	 * subject to triangle ownership and all other fragment tests.
	 * </p>
	 * 
	 * @param pfrag_out
	 * @param ptemp
	 * @param puniforms
	 * @param pvary
	 * @param pvary_ddx
	 * @param pvary_ddy
	 * @param x gl_FragCoord.x
	 * @param y gl_FragCoord.y
	 * @param z gl_FragCoord.z
	 * @param dz_dx
	 * @param dz_dy
	 * @param iw gl_FragCoord.w
	 * @param diw_dx
	 * @param diw_dy
	 * @return Bitmask for fragment acceptance
	 */
	public int shadeFragmentQuad(
		long pfrag_out, long ptemp, long puniforms, long pvary, long pvary_ddx, long pvary_ddy,
		int x, int y,
		float z, float dz_dx, float dz_dy,
		float iw, float diw_dx, float diw_dy
	) {
		return shadeFragmentQuad_static(pfrag_out, ptemp, puniforms, pvary, pvary_ddx, pvary_ddy, x, y, z, dz_dx, dz_dy, iw, diw_dx, diw_dy);
	}
}
