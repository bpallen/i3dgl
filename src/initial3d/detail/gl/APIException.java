package initial3d.detail.gl;

import initial3d.GLEnum;

public class APIException extends RuntimeException {

	private GLEnum error;
	
	public APIException(GLEnum error_, String msg_) {
		super(msg_);
		error = error_;
	}
	
	public GLEnum error() {
		return error;
	}
	
}
