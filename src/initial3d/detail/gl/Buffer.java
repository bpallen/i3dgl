package initial3d.detail.gl;

import java.nio.ByteBuffer;

import initial3d.detail.Access;
import initial3d.detail.AccessFuture;

public class Buffer extends Resource {
	
	private DataStore data = new DataStore();
	private AccessFuture user = null;
	
	public Buffer(I3DGL gl_, int name_) {
		super(gl_, name_);
	}
	
	public long address() {
		return data.address();
	}
	
	public int size() {
		return data.size();
	}
	
	public ByteBuffer map(int offset, int length) {
		return data.map(offset, length);
	}
	
	public ByteBuffer map(int offset) {
		return data.map(offset);
	}
	
	public ByteBuffer map() {
		return data.map();
	}
	
	public DataStore data() {
		return data;
	}
	
	/**
	 * Acquire new storage. Access from user thread is safe until required by a command.
	 *  
	 * @param size
	 */
	public void realloc(int size) {
		gl().driver().invokeFree(data);
		data = null;
		data = DataStore.alloc(size, "buffer");
	}

	@Override
	public void prepare(Command c, Access a) {
		if (user != null) gl().throwInvalidOperation("buffer %d is currently mapped", name());
		c.require(data, a);
	}
	
	@Override
	public void onDeleteName() {
		super.onDeleteName();
		gl().driver().invokeFree(data);
		data = null;
	}

	public boolean tryAcquireUserAccess(Access a) {
		if (user != null) gl().throwInvalidOperation("buffer %d is already mapped", name());
		AccessFuture af = data.requestAccess(a);
		if (af.tryAcquire()) {
			user = af;
			return true;
		}
		return false;
	}
	
	public void acquireUserAccess(Access a) {
		if (user != null) gl().throwInvalidOperation("buffer %d is already mapped", name());
		gl().driver().flush();
		user = data.requestAccess(a);
		user.acquire();
	}
	
	public void releaseUserAccess() {
		if (user == null) return;
		user.release();
		user = null;
	}
	
}
