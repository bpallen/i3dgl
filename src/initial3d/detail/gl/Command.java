package initial3d.detail.gl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import initial3d.detail.Access;
import initial3d.detail.AccessFuture;
import initial3d.detail.Accessible;

public abstract class Command {

	// global pool of temporary storage
	private static final Stack<DataStore> temp_storage_pool = new Stack<>();
	private static final AtomicInteger temp_storage_count = new AtomicInteger();
	
	// just need a use-count to determine when the command becomes idle
	private final AtomicInteger usecount = new AtomicInteger(0);
	
	// list of things requiring access arbitration before starting this command
	// also, ensure that the datastores don't get freed while this command is still around
	private final ArrayList<Accessible> dependencies = new ArrayList<>();
	
	// access futures that must be acquired before the command can run
	private final ArrayList<AccessFuture> access_acquire = new ArrayList<>();
	
	// access futures that need to be released
	private final ArrayList<AccessFuture> access_release = new ArrayList<>();
	
	// temp storage allocated to this command
	private final ArrayList<DataStore> temp_storage = new ArrayList<>();
	
	// uniform storage for shaders 
	private DataStore uniforms = null;
	
	private Driver driver;
	
	// altering this can have weird performance impacts
	private Exception stack_trace = new Exception();
	
	public Command() {
		
	}
	
	public Exception stackTrace() {
		return stack_trace;
	}
	
	public List<Accessible> dependencies() {
		return Collections.unmodifiableList(dependencies);
	}
	
	public void requestAccess() {
		for (AccessFuture f : access_acquire) {
			f.request();
		}
	}
	
	public void require(DataStore s, Access a) {
		dependencies.add(Objects.requireNonNull(s));
		// TODO possibly have access promises keep a weakref to the future so they can cancel if it gets lost
		access_acquire.add(s.requestAccessDeferred(a));
	}
	
	public void require(CommandPrereq o, Access a) {
		if (o == null) return;
		o.prepare(this, a);
	}
	
	/**
	 * Set the uniform storage for shaders.
	 * Has this command require the data store with read access.
	 * 
	 * @param s
	 */
	public void uniforms(DataStore s) {
		require(s, Access.READ);
		uniforms = s;
	}
	
	public DataStore uniforms() {
		return uniforms;
	}
	
	public Driver driver() {
		return driver;
	}
	
	/**
	 * Called (possibly many times) by the driver to start executing this command.
	 * Will attempt to acquire any pending access futures.
	 * Returns true iff no such futures are still pending.
	 * 
	 * @param driver_
	 */
	public boolean start(Driver driver_) {
		driver = driver_;
		for (Iterator<AccessFuture> it = access_acquire.iterator(); it.hasNext(); ) {
			AccessFuture f = it.next();
			if (f.tryAcquire()) {
				it.remove();
				access_release.add(f);
			}
		}
		return access_acquire.isEmpty();
	}
	
	/**
	 * Called by the driver to finish execution of this command when this command
	 * and <i>all previous commands</i> have completed. Calls {@link #onCompletion}.
	 */
	public void finish() {
		onCompletion();
		driver = null;
		// TODO not releasing access until completion is suboptimal
		// can be released as soon as idle
		for (AccessFuture f : access_release) {
			f.release();
		}
		access_release.clear();
	}
	
	/**
	 * Increment usage refcount for this command.
	 */
	public void usageInc() {
		if (usecount.getAndIncrement() == 0) {
			// inform the driver that async execution has started
			driver.commandAsyncStart();
		}
	}
	
	/**
	 * Decrement usage refcount for this command.
	 * When all calls to {@link #usageInc} have been matched, the command is considered {@link #idle}.
	 * {@link #usageInc()} and {@link #usageDec()} do not have to be matched on each thread.
	 * Releases temp storage to global pool when idle is reached.
	 */
	public void usageDec() {
		// grab driver reference before decrement
		// so that we don't nullpointer if the driver sees our idleness before we tell it
		Driver driver = this.driver;
		if (usecount.decrementAndGet() == 0) {
			// inform the driver that async execution has finished
			driver.commandAsyncFinish();
			// release temp storage
			synchronized (this) {
				for (DataStore s : temp_storage) {
					DataStore.free(s);
				}
				temp_storage.clear();
			}
		}
	}
	
	/**
	 * @return True when all calls to {@link #usageInc} have been matched by {@link #usageDec}.
	 */
	public boolean idle() {
		return usecount.get() == 0;
	}

	public abstract void run();

	/**
	 * Called by the driver when this command and <i>all previous commands</i> have completed.
	 */
	public void onCompletion() {
		
	}
	
	public void workerInvoke(Runnable task) {
		WorkerPool.invoke(this, task);
	}
	
	public void workerInvokeAll(Runnable task) {
		WorkerPool.invokeAll(this, task);
	}
	
	public void workerInvokeLater(WorkerPool.Ticket tk, Runnable task) {
		WorkerPool.invokeLater(tk, this, task);
	}
	
	/**
	 * Allocate a temporary data store. Only available for use by async worker tasks.
	 * Allocated storage is returned to the global pool when the command returns to idle.
	 * 
	 * @return
	 */
	public synchronized DataStore allocStorage(int size) {
		// what was the point of this check?
		if (WorkerPool.id() < 0) return null;
		DataStore s = DataStore.alloc(size, "command temp");
		temp_storage.add(s);
		return s;
	}
	
}
