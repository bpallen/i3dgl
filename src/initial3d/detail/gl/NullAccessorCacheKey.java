package initial3d.detail.gl;

public class NullAccessorCacheKey implements Cloneable {
	
	public static final NullAccessorCacheKey INSTANCE = new NullAccessorCacheKey();
	
	private NullAccessorCacheKey() {
		
	}
	
	@Override
	public NullAccessorCacheKey clone() {
		return this;
	}
	
	@Override
	public String toString() {
		return "nullkey";
	}
}
