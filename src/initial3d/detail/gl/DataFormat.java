package initial3d.detail.gl;

import initial3d.GLEnum;
import initial3d.detail.Util;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;
import sun.misc.Unsafe;

import static initial3d.GLEnum.*;
import static initial3d.GLEnumValues.*;

/**
 * Routines for memory i/o and format conversion/packing.
 * 
 * Special cases for floats to avoid reinterpreting them as ints for packing, because apparently that's a little slow.
 * 
 * TODO depth/stencil formats, formats like R10B10G10A2, gamma
 * 
 * @author ben
 *
 */
public class DataFormat {

	public static enum Encoding {
		UNSIGNED_INTEGER(true, false, false),
		SIGNED_INTEGER(true, true, false),
		UNSIGNED_UNNORMALIZED(false, false, false),
		SIGNED_UNNORMALIZED(false, true, false),
		UNSIGNED_NORMALIZED(false, false, true),
		SIGNED_NORMALIZED(false, true, true),
		FLOAT16(false, true, false),
		FLOAT32(false, true, false),
		GAMMA(false, false, true);
		
		public final boolean integral;
		public final boolean signed;
		public final boolean normalized;
		
		private Encoding(boolean integral_, boolean signed_, boolean normalized_) {
			integral = integral_;
			signed = signed_;
			normalized = normalized_;
		}
		
		public static Encoding integer(boolean signed) {
			return signed ? SIGNED_INTEGER : UNSIGNED_INTEGER;
		}
		
		public static Encoding fixedPoint(boolean signed, boolean normalized) {
			if (signed) {
				return normalized ? Encoding.SIGNED_NORMALIZED : Encoding.SIGNED_UNNORMALIZED;
			} else {
				return normalized ? Encoding.UNSIGNED_NORMALIZED : Encoding.UNSIGNED_UNNORMALIZED;
			}
		}
	}
	
	public static int sizeof(GLEnum type) {
		switch (type.valueInt()) {
		// user data types
		case GL_BYTE_value:
		case GL_UNSIGNED_BYTE_value:
			return 1;
		case GL_SHORT_value:
		case GL_UNSIGNED_SHORT_value:
		case GL_HALF_FLOAT_value:
			return 2;
		case GL_INT_value:
		case GL_UNSIGNED_INT_value:
		case GL_FLOAT_value:
			return 4;
		case GL_DOUBLE_value:
			return 8;
		// internal texture formats: 8-bit per channel
		case GL_R8_value:
		case GL_R8_SNORM_value:
		case GL_R8I_value:
		case GL_R8UI_value:
			return 1;
		case GL_RG8_value:
		case GL_RG8_SNORM_value:
		case GL_RG8I_value:
		case GL_RG8UI_value:
			return 2;
		case GL_RGB_value:
		case GL_RGB8_value:
		case GL_RGB8_SNORM_value:
		case GL_RGB8I_value:
		case GL_RGB8UI_value:
		case GL_RGBA_value:
		case GL_RGBA8_value:
		case GL_RGBA8_SNORM_value:
		case GL_RGBA8I_value:
		case GL_RGBA8UI_value:
			return 4;
		// internal texture formats: 16-bit per channel
		case GL_R16_value:
		case GL_R16_SNORM_value:
		case GL_R16I_value:
		case GL_R16UI_value:
		case GL_R16F_value:
			return 2;
		case GL_RG16_value:
		case GL_RG16_SNORM_value:
		case GL_RG16I_value:
		case GL_RG16UI_value:
		case GL_RG16F_value:
			return 4;
		case GL_RGB16_value:
		case GL_RGB16_SNORM_value:
		case GL_RGB16I_value:
		case GL_RGB16UI_value:
		case GL_RGB16F_value:
		case GL_RGBA16_value:
		case GL_RGBA16_SNORM_value:
		case GL_RGBA16I_value:
		case GL_RGBA16UI_value:
		case GL_RGBA16F_value:
			return 8;
		// internal texture formats: 32-bit per channel
		case GL_R32I_value:
		case GL_R32UI_value:
		case GL_R32F_value:
			return 4;
		case GL_RG32I_value:
		case GL_RG32UI_value:
		case GL_RG32F_value:
			return 8;
		case GL_RGB32I_value:
		case GL_RGB32UI_value:
		case GL_RGB32F_value:
			return 12;
		case GL_RGBA32I_value:
		case GL_RGBA32UI_value:
		case GL_RGBA32F_value:
			return 16;
		// internal texture formats: depth
		case GL_DEPTH_COMPONENT16_value:
			return 2;
		case GL_DEPTH_COMPONENT_value:
		case GL_DEPTH_COMPONENT24_value:
		case GL_DEPTH_COMPONENT32_value:
		case GL_DEPTH_COMPONENT32F_value:
			return 4;
		default:
			throw new RuntimeException("bad data type");
		}
	}
	
	public static boolean signed(GLEnum type) {
		switch (type.valueInt()) {
		case GL_BYTE_value:
		case GL_SHORT_value:
		case GL_INT_value:
		case GL_HALF_FLOAT_value:
		case GL_FLOAT_value:
		case GL_DOUBLE_value:
		case GL_R8_SNORM_value:
		case GL_RG8_SNORM_value:
		case GL_RGB8_SNORM_value:
		case GL_RGBA8_SNORM_value:
		case GL_R16_SNORM_value:
		case GL_RG16_SNORM_value:
		case GL_RGB16_SNORM_value:
		case GL_RGBA16_SNORM_value:
		case GL_R16F_value:
		case GL_RG16F_value:
		case GL_RGB16F_value:
		case GL_RGBA16F_value:
		case GL_R32F_value:
		case GL_RG32F_value:
		case GL_RGB32F_value:
		case GL_RGBA32F_value:
		case GL_R8I_value:
		case GL_RG8I_value:
		case GL_RGB8I_value:
		case GL_RGBA8I_value:
		case GL_R16I_value:
		case GL_RG16I_value:
		case GL_RGB16I_value:
		case GL_RGBA16I_value:
		case GL_R32I_value:
		case GL_RG32I_value:
		case GL_RGB32I_value:
		case GL_RGBA32I_value:
		case GL_DEPTH_COMPONENT32F_value:
			return true;
		case GL_UNSIGNED_BYTE_value:
		case GL_UNSIGNED_SHORT_value:
		case GL_UNSIGNED_INT_value:
		case GL_R8_value:
		case GL_RG8_value:
		case GL_RGB_value:
		case GL_RGB8_value:
		case GL_RGBA_value:
		case GL_RGBA8_value:
		case GL_R16_value:
		case GL_RG16_value:
		case GL_RGB16_value:
		case GL_RGBA16_value:
		case GL_R8UI_value:
		case GL_RG8UI_value:
		case GL_RGB8UI_value:
		case GL_RGBA8UI_value:
		case GL_R16UI_value:
		case GL_RG16UI_value:
		case GL_RGB16UI_value:
		case GL_RGBA16UI_value:
		case GL_R32UI_value:
		case GL_RG32UI_value:
		case GL_RGB32UI_value:
		case GL_RGBA32UI_value:
		case GL_DEPTH_COMPONENT16_value:
		case GL_DEPTH_COMPONENT_value:
		case GL_DEPTH_COMPONENT24_value:
		case GL_DEPTH_COMPONENT32_value:
			return false;
		default:
			throw new RuntimeException("bad data type");	
		}
	}
	
	public static boolean integral(GLEnum fmt) {
		switch (fmt.valueInt()) {
		case GL_R8I_value:
		case GL_R8UI_value:
		case GL_RG8I_value:
		case GL_RG8UI_value:
		case GL_RGB8I_value:
		case GL_RGB8UI_value:
		case GL_RGBA8I_value:
		case GL_RGBA8UI_value:
		case GL_R16I_value:
		case GL_R16UI_value:
		case GL_RG16I_value:
		case GL_RG16UI_value:
		case GL_RGB16I_value:
		case GL_RGB16UI_value:
		case GL_RGBA16I_value:
		case GL_RGBA16UI_value:
		case GL_R32I_value:
		case GL_R32UI_value:
		case GL_RG32I_value:
		case GL_RG32UI_value:
		case GL_RGB32I_value:
		case GL_RGB32UI_value:
		case GL_RGBA32I_value:
		case GL_RGBA32UI_value:
			return true;
		default:
			return false;
		}
	}
	
	/**
	 * Any texel format where each component has the same number of bits
	 * and the same encoding is <i>ordinary</i>.
	 * 
	 * @param fmt
	 * @return
	 */
	public static boolean ordinary(GLEnum fmt) {
		switch (fmt.valueInt()) {
		case GL_R8_value:
		case GL_R8_SNORM_value:
		case GL_RG8_value:
		case GL_RG8_SNORM_value:
		case GL_RGB_value:
		case GL_RGB8_value:
		case GL_RGB8_SNORM_value:
		case GL_RGBA_value:
		case GL_RGBA8_value:
		case GL_RGBA8_SNORM_value:
		case GL_R16_value:
		case GL_R16_SNORM_value:
		case GL_R16F_value:
		case GL_RG16_value:
		case GL_RG16_SNORM_value:
		case GL_RG16F_value:
		case GL_RGB16_value:
		case GL_RGB16_SNORM_value:
		case GL_RGB16F_value:
		case GL_RGBA16_value:
		case GL_RGBA16_SNORM_value:
		case GL_RGBA16F_value:
		case GL_R32F_value:
		case GL_RG32F_value:
		case GL_RGB32F_value:
		case GL_RGBA32F_value:
		case GL_R8I_value:
		case GL_R8UI_value:
		case GL_RG8I_value:
		case GL_RG8UI_value:
		case GL_RGB8I_value:
		case GL_RGB8UI_value:
		case GL_RGBA8I_value:
		case GL_RGBA8UI_value:
		case GL_R16I_value:
		case GL_R16UI_value:
		case GL_RG16I_value:
		case GL_RG16UI_value:
		case GL_RGB16I_value:
		case GL_RGB16UI_value:
		case GL_RGBA16I_value:
		case GL_RGBA16UI_value:
		case GL_R32I_value:
		case GL_R32UI_value:
		case GL_RG32I_value:
		case GL_RG32UI_value:
		case GL_RGB32I_value:
		case GL_RGB32UI_value:
		case GL_RGBA32I_value:
		case GL_RGBA32UI_value:
		case GL_DEPTH_COMPONENT16_value:
		case GL_DEPTH_COMPONENT_value:
		case GL_DEPTH_COMPONENT24_value:
		case GL_DEPTH_COMPONENT32_value:
		case GL_DEPTH_COMPONENT32F_value:
			return true;
		default:
			return false;
		}
	}
	
	public static int texelComponents(GLEnum type) {
		switch (type.valueInt()) {
		case GL_R8_value:
		case GL_R8_SNORM_value:
		case GL_R8I_value:
		case GL_R8UI_value:
		case GL_R16_value:
		case GL_R16_SNORM_value:
		case GL_R16I_value:
		case GL_R16UI_value:
		case GL_R16F_value:
		case GL_R32I_value:
		case GL_R32UI_value:
		case GL_R32F_value:
			return 1;
		case GL_RG8_value:
		case GL_RG8_SNORM_value:
		case GL_RG8I_value:
		case GL_RG8UI_value:
		case GL_RG16_value:
		case GL_RG16_SNORM_value:
		case GL_RG16I_value:
		case GL_RG16UI_value:
		case GL_RG16F_value:
		case GL_RG32I_value:
		case GL_RG32UI_value:
		case GL_RG32F_value:
			return 2;
		case GL_RGB_value:
		case GL_RGB8_value:
		case GL_RGB8_SNORM_value:
		case GL_RGB8I_value:
		case GL_RGB8UI_value:
		case GL_RGB16_value:
		case GL_RGB16_SNORM_value:
		case GL_RGB16I_value:
		case GL_RGB16UI_value:
		case GL_RGB16F_value:
		case GL_RGB32I_value:
		case GL_RGB32UI_value:
		case GL_RGB32F_value:
			return 3;
		case GL_RGBA_value:
		case GL_RGBA8_value:
		case GL_RGBA8_SNORM_value:
		case GL_RGBA8I_value:
		case GL_RGBA8UI_value:
		case GL_RGBA16_value:
		case GL_RGBA16_SNORM_value:
		case GL_RGBA16I_value:
		case GL_RGBA16UI_value:
		case GL_RGBA16F_value:
		case GL_RGBA32I_value:
		case GL_RGBA32UI_value:
		case GL_RGBA32F_value:
			return 4;
		case GL_DEPTH_COMPONENT16_value:
		case GL_DEPTH_COMPONENT_value:
		case GL_DEPTH_COMPONENT24_value:
		case GL_DEPTH_COMPONENT32_value:
		case GL_DEPTH_COMPONENT32F_value:
			return 1;
		default:
			throw new RuntimeException("bad data type");
		}
	}
	
	public static int texelIOSize(GLEnum fmt) {
		switch (fmt.valueInt()) {
		case GL_R32F_value:
		case GL_RG32F_value:
		case GL_RGB32F_value:
		case GL_RGBA32F_value:
			return 4;
		default:
			break;
		}
		switch (sizeof(fmt)) {
		case 1:
			return 1;
		case 2:
			return 2;
		case 4:
			return 4;
		case 8:
			return 8;
		case 12:
			return 4;
		case 16:
			return 8;
		default:
			throw new RuntimeException("bad data type");
		}
	}
	
	public static int texelComponentBits(GLEnum fmt) {
		if (!ordinary(fmt)) throw new RuntimeException("texel format is not ordinary");
		switch (fmt.valueInt()) {
		case GL_DEPTH_COMPONENT24_value:
			return 24;
		default:
			return sizeof(fmt) * 8 / texelComponents(fmt);
		}
	}
	
	public static Encoding texelEncoding(GLEnum fmt) {
		if (!ordinary(fmt)) throw new RuntimeException("texel format is not ordinary");
		switch (fmt.valueInt()) {
		// fixed point
		case GL_R8_value:
		case GL_R8_SNORM_value:
		case GL_RG8_value:
		case GL_RG8_SNORM_value:
		case GL_RGB_value:
		case GL_RGB8_value:
		case GL_RGB8_SNORM_value:
		case GL_RGBA_value:
		case GL_RGBA8_value:
		case GL_RGBA8_SNORM_value:
		case GL_R16_value:
		case GL_R16_SNORM_value:
		case GL_RG16_value:
		case GL_RG16_SNORM_value:
		case GL_RGB16_value:
		case GL_RGB16_SNORM_value:
		case GL_RGBA16_value:
		case GL_RGBA16_SNORM_value:
		case GL_DEPTH_COMPONENT16_value:
		case GL_DEPTH_COMPONENT_value:
		case GL_DEPTH_COMPONENT24_value:
		case GL_DEPTH_COMPONENT32_value:
			return Encoding.fixedPoint(signed(fmt), true);
		// float16
		case GL_R16F_value:
		case GL_RG16F_value:
		case GL_RGB16F_value:
		case GL_RGBA16F_value:
			return Encoding.FLOAT16;
		// float32
		case GL_R32F_value:
		case GL_RG32F_value:
		case GL_RGB32F_value:
		case GL_RGBA32F_value:
		case GL_DEPTH_COMPONENT32F_value:
			return Encoding.FLOAT32;
		// integral
		case GL_R8I_value:
		case GL_R8UI_value:
		case GL_RG8I_value:
		case GL_RG8UI_value:
		case GL_RGB8I_value:
		case GL_RGB8UI_value:
		case GL_RGBA8I_value:
		case GL_RGBA8UI_value:
		case GL_R16I_value:
		case GL_R16UI_value:
		case GL_RG16I_value:
		case GL_RG16UI_value:
		case GL_RGB16I_value:
		case GL_RGB16UI_value:
		case GL_RGBA16I_value:
		case GL_RGBA16UI_value:
		case GL_R32I_value:
		case GL_R32UI_value:
		case GL_RG32I_value:
		case GL_RG32UI_value:
		case GL_RGB32I_value:
		case GL_RGB32UI_value:
		case GL_RGBA32I_value:
		case GL_RGBA32UI_value:
			return Encoding.integer(signed(fmt));
		default:
			throw new RuntimeException("bad data type");
		}
	}
	
	/**
	 * [p] -> [int|long]
	 * 
	 * @param mb
	 * @param cls
	 * @throws Exception
	 */
	public static void buildGetRaw(MethodBuilder mb, int sz) throws Exception {
		switch (sz) {
		case 1:
			mb.invokeStatic(Util.class, "getb", byte.class, long.class);
			break;
		case 2:
			mb.invokeStatic(Util.class, "gets", short.class, long.class);
			break;
		case 4:
			mb.invokeStatic(Util.class, "geti", int.class, long.class);
			break;
		case 8:
			mb.invokeStatic(Util.class, "getl", long.class, long.class);
			break;
		default:
			throw new RuntimeException("bad data type");
		}
	}
	
	/**
	 * [p, int|long] -> []
	 * 
	 * @param mb
	 * @param cls
	 * @throws Exception
	 */
	public static void buildPutRaw(MethodBuilder mb, int sz) throws Exception {
		switch (sz) {
		case 1:
			mb.invokeStatic(Util.class, "putb", void.class, long.class, byte.class);
			break;
		case 2:
			mb.invokeStatic(Util.class, "puts", void.class, long.class, short.class);
			break;
		case 4:
			mb.invokeStatic(Util.class, "puti", void.class, long.class, int.class);
			break;
		case 8:
			mb.invokeStatic(Util.class, "putl", void.class, long.class, long.class);
			break;
		default:
			throw new RuntimeException("bad data type");
		}
	}
	
	/**
	 * [pout, pin] -> []
	 * 
	 * @param mb
	 * @param sz
	 * @throws Exception
	 */
	public static void buildCopy(MethodBuilder mb, int sz) throws Exception {
		mb.pushScope();
		mb.scope().alloc(long.class, "pout");
		mb.scope().alloc(long.class, "pin");
		mb.lstore("pin");
		mb.lstore("pout");
		final int csz = Util.gcd(sz, 8);
		for (int i = 0; i < sz / csz; i++) {
			mb.lload("pout");
			mb.constant((long) (csz * i));
			mb.op(Op.LADD);
			mb.lload("pin");
			mb.constant((long) (csz * i));
			mb.op(Op.LADD);
			buildGetRaw(mb, csz);
			buildPutRaw(mb, csz);
		}
		mb.popScope();
	}
	
	/**
	 * Pack an int into the specified number of bits.
	 * 
	 * [int] -> [int]
	 * 
	 * @param mb
	 * @param bits
	 * @param signed_output
	 */
	public static void buildPackInt(MethodBuilder mb, int bits, boolean signed_output, boolean signed_input) {
		if (bits == 32 && signed_output == signed_input) {
			// lets make sure this is a no-op
			return;
		}
		final int mask = (1 << bits) - 1;
		// if either output or input is unsigned, lower bound must be 0
		// otherwise, the lower bound is signed-min for bits
		// TODO technically, if input is unsigned we don't need to clamp the lower bound
		mb.constant((signed_output && signed_input) ? ((-1 & ~mask) + (1 << (bits - 1))) : 0);
		if (signed_input) {
			mb.imax();
		} else {
			// this only happens if lower bound is 0, so umax is safe
			mb.umax();
		}
		// if output is signed, upper bound must be signed-max for bits
		// if input is signed, upper bound must be <= int-max
		// otherwise, both are unsigned and upper bound is <= unsigned-max for bits
		mb.constant((!signed_output && !signed_input) ? mask : signed_output ? (mask >>> 1) : bits == 32 ? Integer.MAX_VALUE : mask);
		if (signed_input) {
			// fine for unsigned output
			mb.imin();
		} else {
			// upper bound still behaves for signed output
			mb.umin();
		}
		if (signed_output) {
			// FIXME (do we) have to mask to remove sign extension?
			mb.constant(mask);
			mb.op(Op.IAND);
		}
	}
	
	/**
	 * Unpack an int from the specified number of bits
	 * 
	 * [int] -> [int]
	 * 
	 * @param mb
	 * @param bits
	 * @param signed_input
	 */
	public static void buildUnpackInt(MethodBuilder mb, int bits, boolean signed_input) {
		if (bits == 32) {
			// lets make sure this is a no-op
			return;
		}
		final int mask = (1 << bits) - 1;
		if (signed_input) {
			// force sign extension with arithmetic shift
			mb.constant(32 - bits);
			mb.op(Op.ISHL);
			mb.constant(32 - bits);
			mb.op(Op.ISHR);
		} else {
			// mask to remove junk from high bits
			mb.constant(mask);
			mb.op(Op.IAND);
		}
	}
	
	/**
	 * Pack an int or float into the specified number of bits.
	 * The output encoding must match the value on the stack in integralness.
	 * 
	 * TODO deal with 32 bit unsigned output
	 * 
	 * [int|float] -> [int]
	 * 
	 * @param mb
	 * @param bits
	 * @param signed_output
	 * @param normalized
	 */
	public static void buildPack(MethodBuilder mb, int bits, Encoding out_enc, boolean signed_input) throws Exception {
		switch (out_enc) {
		case FLOAT16:
			throw new RuntimeException("float16 not supported yet");
		case FLOAT32:
			if (bits != 32) throw new RuntimeException("bad bits");
			mb.invokeStatic(Float.class, "floatToRawIntBits", int.class, float.class);
			break;
		case UNSIGNED_INTEGER:
		case SIGNED_INTEGER:
			buildPackInt(mb, bits, out_enc.signed, signed_input);
			break;
		default:
			final float k = out_enc.signed ? ((1 << (bits - 1)) - 1) : ((1 << bits) - 1);
			if (out_enc.normalized) {
				mb.constant(k);
				mb.op(Op.FMUL);
			}
			// TODO float to int unsigned
			mb.op(Op.F2I);
			buildPackInt(mb, bits, out_enc.signed, true);
			break;
		}
	}
	
	public static void buildPackFixed(MethodBuilder mb, int bits, boolean signed_output, boolean normalized) throws Exception {
		buildPack(mb, bits, Encoding.fixedPoint(signed_output, normalized), true);
	}
	
	/**
	 * Unpack an int or float from the specified number of bits.
	 * The output value on the stack will match the input encoding in integralness.
	 * 
	 * TODO deal with 32 bit unsigned input
	 * 
	 * [int] -> [int|float]
	 * 
	 * @param mb
	 * @param bits
	 * @param signed_input
	 * @param normalized
	 */
	public static void buildUnpack(MethodBuilder mb, int bits, Encoding in_enc) throws Exception {
		switch (in_enc) {
		case FLOAT16:
			throw new RuntimeException("float16 not supported yet");
		case FLOAT32:
			if (bits != 32) throw new RuntimeException("bad bits");
			mb.invokeStatic(Float.class, "intBitsToFloat", float.class, int.class);
			break;
		case SIGNED_INTEGER:
		case UNSIGNED_INTEGER:
			buildUnpackInt(mb, bits, in_enc.signed);
			break;
		default:
			final float k = in_enc.signed ? ((1 << (bits - 1)) - 1) : ((1 << bits) - 1);
			buildUnpackInt(mb, bits, in_enc.signed);
			mb.op(Op.I2F);
			if (in_enc.normalized) {
				mb.constant(1.f / k);
				mb.op(Op.FMUL);
			}
			break;	
		}
	}
	
	public static void buildUnpackFixed(MethodBuilder mb, int bits, boolean signed_input, boolean normalized) throws Exception {
		buildUnpack(mb, bits, Encoding.fixedPoint(signed_input, normalized));
	}
	
	/**
	 * [p] -> [int]
	 * 
	 * @param mb
	 * @param type
	 */
	public static void buildGetInt(MethodBuilder mb, GLEnum type) throws Exception {
		switch (type.valueInt()) {
		case GL_BYTE_value:
			buildGetRaw(mb, 1);
			break;
		case GL_UNSIGNED_BYTE_value:
			buildGetRaw(mb, 1);
			mb.constant(0xFF);
			mb.op(Op.IAND);
			break;
		case GL_SHORT_value:
			buildGetRaw(mb, 2);
			break;
		case GL_UNSIGNED_SHORT_value:
			buildGetRaw(mb, 2);
			mb.constant(0xFFFF);
			mb.op(Op.IAND);
			break;
		case GL_INT_value:
			buildGetRaw(mb, 4);
			break;
		case GL_UNSIGNED_INT_value:
			buildGetRaw(mb, 4);
			break;
		default:
			throw new RuntimeException("bad data type");
		}
	}
	
	/**
	 * [p, int] -> []
	 * 
	 * @param mb
	 * @param type
	 */
	public static void buildPutInt(MethodBuilder mb, GLEnum type, boolean signed_input) throws Exception {
		switch (type.valueInt()) {
		case GL_BYTE_value:
			buildPackInt(mb, 8, true, signed_input);
			buildPutRaw(mb, 1);
			break;
		case GL_UNSIGNED_BYTE_value:
			buildPackInt(mb, 8, false, signed_input);
			buildPutRaw(mb, 1);
			break;
		case GL_SHORT_value:
			buildPackInt(mb, 16, true, signed_input);
			buildPutRaw(mb, 2);
			break;
		case GL_UNSIGNED_SHORT_value:
			buildPackInt(mb, 16, false, signed_input);
			buildPutRaw(mb, 2);
			break;
		case GL_INT_value:
			buildPackInt(mb, 32, true, signed_input);
			buildPutRaw(mb, 4);
			break;
		case GL_UNSIGNED_INT_value:
			buildPackInt(mb, 32, false, signed_input);
			buildPutRaw(mb, 4);
			break;
		default:
			throw new RuntimeException("bad data type");
		}
	}
	
	/**
	 * [p] -> [float]
	 * 
	 * @param mb
	 * @param type
	 * @param normalized
	 */
	public static void buildGetFloat(MethodBuilder mb, GLEnum type, boolean normalized) throws Exception {
		// are we even supposed to support packing floats into 32 bit ints?
		switch (type.valueInt()) {
		case GL_BYTE_value:
			buildGetRaw(mb, 1);
			buildUnpackFixed(mb, 8, true, normalized);
			break;
		case GL_UNSIGNED_BYTE_value:
			buildGetRaw(mb, 1);
			buildUnpackFixed(mb, 8, false, normalized);
			break;
		case GL_SHORT_value:
			buildGetRaw(mb, 2);
			buildUnpackFixed(mb, 16, true, normalized);
			break;
		case GL_UNSIGNED_SHORT_value:
			buildGetRaw(mb, 2);
			buildUnpackFixed(mb, 16, false, normalized);
			break;
		case GL_HALF_FLOAT_value:
			buildGetRaw(mb, 2);
			buildUnpack(mb, 16, Encoding.FLOAT16);
			break;
		case GL_FLOAT_value:
			//buildGetRaw(mb, 4);
			//buildUnpack(mb, 32, Encoding.FLOAT32);
			mb.invokeStatic(Util.class, "getf", float.class, long.class);
			break;
		default:
			throw new RuntimeException("bad data type");
		}
	}
	
	/**
	 * [p, float] -> []
	 * 
	 * @param mb
	 * @param type
	 * @param normalized
	 */
	public static void buildPutFloat(MethodBuilder mb, GLEnum type, boolean normalized) throws Exception {
		// are we even supposed to support packing floats into 32 bit ints?
		switch (type.valueInt()) {
		case GL_BYTE_value:
			buildPackFixed(mb, 8, true, normalized);
			buildPutRaw(mb, 1);
			break;
		case GL_UNSIGNED_BYTE_value:
			buildPackFixed(mb, 8, false, normalized);
			buildPutRaw(mb, 1);
			break;
		case GL_SHORT_value:
			buildPackFixed(mb, 16, true, normalized);
			buildPutRaw(mb, 2);
			break;
		case GL_UNSIGNED_SHORT_value:
			buildPackFixed(mb, 16, false, normalized);
			buildPutRaw(mb, 2);
			break;
		case GL_HALF_FLOAT_value:
			buildPack(mb, 16, Encoding.FLOAT16, true);
			buildPutRaw(mb, 2);
			break;
		case GL_FLOAT_value:
			//buildPack(mb, 32, Encoding.FLOAT32, true);
			//buildPutRaw(mb, 4);
			mb.invokeStatic(Util.class, "putf", void.class, long.class, float.class);
			break;
		default:
			throw new RuntimeException("bad data type");	
		}
	}
	
	/**
	 * Unpacks one component at a time from the low bits of an int into n successive locations starting at loc0 and increasing,
	 * using <code>x >>> bits</code> after each component. The remainder after the final shift is left on the stack.
	 * 
	 * [int] -> [int]
	 * 
	 * @param mb
	 * @param loc0
	 * @param n
	 * @param bits
	 * @param in_enc
	 * @throws Exception
	 */
	public static void buildUnpackTexelInt(MethodBuilder mb, int loc0, int n, int bits, Encoding in_enc) throws Exception {
		for (int loc = loc0; loc < loc0 + n; loc++) {
			mb.op(Op.DUP);
			buildUnpack(mb, bits, in_enc);
			if (in_enc.integral) {
				mb.istore(loc);
			} else {
				mb.fstore(loc);
			}
			mb.constant(bits);
			mb.op(Op.IUSHR);
		}
	}
	
	/**
	 * Unpacks one component at a time from the low bits of a long into n successive locations starting at loc0 and increasing,
	 * using <code>x >>> bits</code> after each component. The remainder after the final shift is left on the stack.
	 * 
	 * [long] -> [long]
	 * 
	 * @param mb
	 * @param loc0
	 * @param n
	 * @param bits
	 * @param in_enc
	 * @throws Exception
	 */
	public static void buildUnpackTexelLong(MethodBuilder mb, int loc0, int n, int bits, Encoding in_enc) throws Exception {
		for (int loc = loc0; loc < loc0 + n; loc++) {
			mb.op(Op.DUP2);
			mb.op(Op.L2I);
			buildUnpack(mb, bits, in_enc);
			if (in_enc.integral) {
				mb.istore(loc);
			} else {
				mb.fstore(loc);
			}
			mb.constant(bits);
			mb.op(Op.LUSHR);
		}
	}
	
	/**
	 * Packs one component at a time into the low bits of an int from n successive locations starting at loc0 + n - 1 and decreasing,
	 * using <code>x << bits</code> before each component. The result is left on the stack.
	 * 
	 * [int] -> [int]
	 * 
	 * @param mb
	 * @param loc0
	 * @param n
	 * @param bits
	 * @param out_enc
	 * @throws Exception
	 */
	public static void buildPackTexelInt(MethodBuilder mb, int loc0, int n, int bits, Encoding out_enc, boolean signed_input) throws Exception {
		for (int loc = loc0 + n; loc --> loc0; ) {
			mb.constant(bits);
			mb.op(Op.ISHL);
			if (out_enc.integral) {
				mb.iload(loc);
			} else {
				mb.fload(loc);
			}
			buildPack(mb, bits, out_enc, signed_input);
			mb.op(Op.IOR);
		}
	}
	
	/**
	 * Packs one component at a time into the low bits of a long from n successive locations starting at loc0 + n - 1 and decreasing,
	 * using <code>x << bits</code> before each component. The result is left on the stack.
	 * 
	 * [long] -> [long]
	 * 
	 * @param mb
	 * @param loc0
	 * @param n
	 * @param bits
	 * @param out_enc
	 * @throws Exception
	 */
	public static void buildPackTexelLong(MethodBuilder mb, int loc0, int n, int bits, Encoding out_enc, boolean signed_input) throws Exception {
		for (int loc = loc0 + n; loc --> loc0; ) {
			mb.constant(bits);
			mb.op(Op.LSHL);
			if (out_enc.integral) {
				mb.iload(loc);
			} else {
				mb.fload(loc);
			}
			buildPack(mb, bits, out_enc, signed_input);
			mb.op(Op.I2L);
			// need to remove any sign extension
			mb.constant(0xFFFFFFFFL);
			mb.op(Op.LAND);
			mb.op(Op.LOR);
		}
	}
	
	public static int buildUnpackOrdinaryTexel(MethodBuilder mb, int loc0, int nmax, GLEnum fmt) throws Exception {
		final int bits = texelComponentBits(fmt);
		final int n = Math.min(nmax, Math.min(texelComponents(fmt), texelIOSize(fmt) * 8 / bits));
		if (texelIOSize(fmt) <= 4) {
			buildUnpackTexelInt(mb, loc0, n, bits, texelEncoding(fmt));
		} else {
			buildUnpackTexelLong(mb, loc0, n, bits, texelEncoding(fmt));
		}
		return n;
	}
	
	public static int buildPackOrdinaryTexel(MethodBuilder mb, int loc0, int nmax, GLEnum fmt) throws Exception {
		final int bits = texelComponentBits(fmt);
		final int n = Math.min(nmax, Math.min(texelComponents(fmt), texelIOSize(fmt) * 8 / bits));
		final boolean signed_input = integral(fmt) ? signed(fmt) : true;
		if (texelIOSize(fmt) <= 4) {
			buildPackTexelInt(mb, loc0, n, bits, texelEncoding(fmt), signed_input);
		} else {
			buildPackTexelLong(mb, loc0, n, bits, texelEncoding(fmt), signed_input);
		}
		return n;
	}
	
	/**
	 * Get a texel into [1,4] float or int contiguous local variables.
	 * The local variables must have the correct type.
	 * 
	 * [pin] -> []
	 * 
	 * @param mb
	 * @param fmt
	 */
	public static void buildGetTexel(MethodBuilder mb, int loc0, GLEnum fmt) throws Exception {
		mb.pushScope();
		mb.scope().alloc(long.class, "pin");
		mb.lstore("pin");
		final int n = texelComponents(fmt);
		int nrem = n;
		for (int i = 0; i < sizeof(fmt) / texelIOSize(fmt); i++) {
			// load input address
			mb.lload("pin");
			mb.constant((long) i * texelIOSize(fmt));
			mb.op(Op.LADD);
			switch (fmt.valueInt()) {
			case GL_R32F_value:
			case GL_RG32F_value:
			case GL_RGB32F_value:
			case GL_RGBA32F_value:
			case GL_DEPTH_COMPONENT32F_value:
				buildGetFloat(mb, GL_FLOAT, false);
				mb.fstore(loc0 + i);
				break;
			default:
				if (ordinary(fmt)) {
					// get input
					buildGetRaw(mb, texelIOSize(fmt));
					// unpack
					nrem -= buildUnpackOrdinaryTexel(mb, loc0 + n - nrem, nrem, fmt);
					// clean the stack
					if (texelIOSize(fmt) <= 4) {
						mb.op(Op.POP);
					} else {
						mb.op(Op.POP2);
					}
				} else {
					throw new RuntimeException("bad data type");
				}
			}
		}
		mb.popScope();
	}
	
	/**
	 * Just for testing.
	 * 
	 * @param mb
	 * @param loc0
	 */
	public static void buildPutRGBA8(MethodBuilder mb, int loc0) throws Exception {
		mb.lload("pout");
		mb.fload(loc0 + 3);
		mb.constant(255.f);
		mb.op(Op.FMUL);
		mb.op(Op.F2I);
		// nb: clamping as int is faster than float
		mb.constant(255);
		mb.imin();
		mb.constant(0);
		mb.imax();
		mb.constant(8);
		mb.op(Op.ISHL);
		mb.fload(loc0 + 2);
		mb.constant(255.f);
		mb.op(Op.FMUL);
		mb.op(Op.F2I);
		mb.constant(255);
		mb.imin();
		mb.constant(0);
		mb.imax();
		mb.op(Op.IOR);
		mb.constant(8);
		mb.op(Op.ISHL);
		mb.fload(loc0 + 1);
		mb.constant(255.f);
		mb.op(Op.FMUL);
		mb.op(Op.F2I);
		mb.constant(255);
		mb.imin();
		mb.constant(0);
		mb.imax();
		mb.op(Op.IOR);
		mb.constant(8);
		mb.op(Op.ISHL);
		mb.fload(loc0 + 0);
		mb.constant(255.f);
		mb.op(Op.FMUL);
		mb.op(Op.F2I);
		mb.constant(255);
		mb.imin();
		mb.constant(0);
		mb.imax();
		mb.op(Op.IOR);
		buildPutInt(mb, GL_UNSIGNED_INT, false);
	}
	
	/**
	 * Pack [1,4] floats or ints from contiguous local variables into a texel.
	 * The local variables must have the correct type.
	 * 
	 * [pout] -> []
	 * 
	 * @param mb
	 * @param fmt
	 */
	public static void buildPutTexel(MethodBuilder mb, int loc0, GLEnum fmt) throws Exception {
		mb.pushScope();
		mb.scope().alloc(long.class, "pout");
		mb.lstore("pout");
		
		// temp, testing things...
//		if (fmt == GL_RGBA8) {
//			buildPutRGBA8(mb, loc0);
//			return;
//		}
		
		final int n = texelComponents(fmt);
		int nrem = n;
		for (int i = 0; i < sizeof(fmt) / texelIOSize(fmt); i++) {
			// load output address
			mb.lload("pout");
			mb.constant((long) i * texelIOSize(fmt));
			mb.op(Op.LADD);
			switch (fmt.valueInt()) {
			case GL_R32F_value:
			case GL_RG32F_value:
			case GL_RGB32F_value:
			case GL_RGBA32F_value:
			case GL_DEPTH_COMPONENT32F_value:
				mb.fload(loc0 + i);
				buildPutFloat(mb, GL_FLOAT, false);
				break;
			default:
				if (ordinary(fmt)) {
					// init stack
					if (texelIOSize(fmt) <= 4) {
						mb.constant(0);
					} else {
						mb.constant(0L);
					}
					// pack
					nrem -= buildPackOrdinaryTexel(mb, loc0 + n - nrem, nrem, fmt);
					// put output
					buildPutRaw(mb, texelIOSize(fmt));
				} else {
					throw new RuntimeException("bad data type");
				}
			}
		}
		mb.popScope();
	}
}

















