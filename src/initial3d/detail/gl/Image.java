package initial3d.detail.gl;

import initial3d.GLEnum;
import initial3d.Vec3i;
import initial3d.detail.Access;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;

import static initial3d.Functions.*;
import static initial3d.GLEnum.*;

public class Image extends Resource {

	private DataStore data = new DataStore();
	private GLEnum format = GL_RGBA8;
	private Vec3i size = vec3i(0);
	
	public Image(I3DGL gl_, int name_) {
		super(gl_, name_);
	}
	
	public long address() {
		return data.address();
	}
	
	public DataStore data() {
		return data;
	}
	
	public GLEnum format() {
		return format;
	}
	
	public Vec3i size() {
		return size;
	}
	
	public void reformat(GLEnum format_, Vec3i size_) {
		if (allocSize(format_, size_) > data.size()) throw new IllegalArgumentException("data store too small");
		format = format_;
		size = size_;
		markAccessorDirty();
	}
	
	public void realloc(GLEnum format_, Vec3i size_) {
		format = format_;
		size = size_;
		gl().driver().invokeFree(data);
		data = null;
		data = DataStore.alloc(allocSize(format_, size_), "image");
		markAccessorDirty();
	}

	@Override
	public void prepare(Command c, Access a) {
		c.require(data, a);
	}
	
	@Override
	public void onDeleteName() {
		super.onDeleteName();
		gl().driver().invokeFree(data);
		data = null;
	}
	
	public static int allocSize(GLEnum format, Vec3i size) {
		// round w and h up to multiples of raster block size
		// TODO this is suboptimal for 1D textures
		return DataFormat.sizeof(format) * size.z * Rasterizer.blockRoundUp(size.x) * Rasterizer.blockRoundUp(size.y);
	}
	
	/**
	 * Texel coord to linear index conversion
	 * 
	 * [int x, int y, int z] -> [int index]
	 */
	public void buildTexelIndex(MethodBuilder mb) {
		final int layer_texels = Rasterizer.blockRoundUp(size.x) * Rasterizer.blockRoundUp(size.y);
		final int macro_row_texels = Rasterizer.blockRoundUp(size.x) * Rasterizer.blockRoundUp(1);
		final int micro_row_texels = Rasterizer.blockSize();
		final int block_texels = micro_row_texels * micro_row_texels;
		mb.pushScope();
		mb.scope().alloc(int.class, "index");
		// z
		mb.constant(layer_texels);
		mb.op(Op.IMUL);
		mb.istore("index");
		// y
		mb.op(Op.DUP);
		mb.constant(Rasterizer.blockBits());
		mb.op(Op.IUSHR);
		mb.constant(macro_row_texels);
		mb.op(Op.IMUL);
		mb.iload("index");
		mb.op(Op.IADD);
		mb.istore("index");
		mb.constant(~Rasterizer.blockMask());
		mb.op(Op.IAND);
		mb.constant(micro_row_texels);
		mb.op(Op.IMUL);
		mb.iload("index");
		mb.op(Op.IADD);
		mb.istore("index");
		// x
		mb.op(Op.DUP);
		mb.constant(Rasterizer.blockBits());
		mb.op(Op.IUSHR);
		mb.constant(block_texels);
		mb.op(Op.IMUL);
		mb.iload("index");
		mb.op(Op.IADD);
		mb.istore("index");
		mb.constant(~Rasterizer.blockMask());
		mb.op(Op.IAND);
		mb.iload("index");
		mb.op(Op.IADD);
		mb.popScope();
	}
	
}
