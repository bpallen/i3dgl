package initial3d.detail.gl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import initial3d.Functions;
import initial3d.GLEnum;
import initial3d.Vec2i;
import initial3d.Vec3i;
import initial3d.Vec4f;
import initial3d.Vec4i;
import initial3d.detail.Access;
import initial3d.detail.SimpleCloneable;
import initial3d.detail.Util;
import initial3d.detail.bytecode.Flag;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;
import sun.misc.Unsafe;

import static initial3d.Functions.*;
import static initial3d.GLEnum.*;
import static initial3d.GLEnumValues.*;

public class Framebuffer extends Resource {

	private class AccessorCacheKey extends SimpleCloneable<AccessorCacheKey> {
		
		private Vec2i viewport_pos = Vec2i.zero;
		private Vec2i viewport_size = Vec2i.zero;
		private GLEnum depth_func = null;
		
		private ArrayList<GLEnum> draw_buffers = new ArrayList<>();
		private BlendState blend_state = null;
		
		// TODO fragment tests
		
		public AccessorCacheKey() {
			refresh();
		}
		
		public void refresh() {
			viewport_pos = gl().viewport_pos;
			viewport_size = gl().viewport_size;
			depth_func = gl().glIsEnabled(GL_DEPTH_TEST) ? gl().depth_func : null;
			draw_buffers.clear();
			draw_buffers.addAll(Framebuffer.this.draw_buffers);
			// yes, we just grab this by reference, relying on clone() to be called where necessary
			// (creating a new BlendState every time a cache key is requested creates _lots_ of object churn)
			blend_state = gl().glIsEnabled(GL_BLEND) ? gl().blend_state : null;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public AccessorCacheKey clone() {
			AccessorCacheKey c = super.clone();
			c.draw_buffers = (ArrayList<GLEnum>) draw_buffers.clone();
			c.blend_state = blend_state != null ? blend_state.clone() : null;
			return c;
		}
		
		private Framebuffer getOuterType() {
			return Framebuffer.this;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((blend_state == null) ? 0 : blend_state.hashCode());
			result = prime * result + ((depth_func == null) ? 0 : depth_func.hashCode());
			result = prime * result + ((draw_buffers == null) ? 0 : draw_buffers.hashCode());
			result = prime * result + ((viewport_pos == null) ? 0 : viewport_pos.hashCode());
			result = prime * result + ((viewport_size == null) ? 0 : viewport_size.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			AccessorCacheKey other = (AccessorCacheKey) obj;
			if (!getOuterType().equals(other.getOuterType())) return false;
			if (blend_state == null) {
				if (other.blend_state != null) return false;
			} else if (!blend_state.equals(other.blend_state)) return false;
			if (depth_func != other.depth_func) return false;
			if (draw_buffers == null) {
				if (other.draw_buffers != null) return false;
			} else if (!draw_buffers.equals(other.draw_buffers)) return false;
			if (viewport_pos == null) {
				if (other.viewport_pos != null) return false;
			} else if (!viewport_pos.equals(other.viewport_pos)) return false;
			if (viewport_size == null) {
				if (other.viewport_size != null) return false;
			} else if (!viewport_size.equals(other.viewport_size)) return false;
			return true;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("AccessorCacheKey [viewport_pos=");
			builder.append(viewport_pos);
			builder.append(", viewport_size=");
			builder.append(viewport_size);
			builder.append(", draw_buffers=");
			builder.append(draw_buffers);
			builder.append(", depth_func=");
			builder.append(depth_func);
			builder.append(", blend_state=");
			builder.append(blend_state);
			builder.append("]");
			// TODO blend state
			return builder.toString();
		}

	}
	
	private class AccessorLocalStorage {
		public Vec2i minp;
		public Vec2i maxp;
		public boolean share_texel_index;
	}
	
	// metadata used by current accessors
	// if we replace this, we have to mark accessors dirty
	private DataStore meta = new DataStore();
	private int block_meta_size;
	private int worker_meta_xblocks;
	private int worker_meta_size;
	
	public class Attachment extends SimpleCloneable<Attachment> {

		public Texture texture = null;
		public int level;
		
		public Attachment() {
			
		}

		public Image image() {
			return texture.image(level);
		}
		
		public long imageAddress() {
			// TODO use image accessor instead
			//return ((Texture) gl().namedObject(texture)).imageAddress(level);
			return 0;
		}
		
	}
	
	// 
	
	// images attached to this fbo
	private final HashMap<GLEnum, Attachment> attachments = new HashMap<>();
	
	// which attachment points are selected for drawing to
	private final ArrayList<GLEnum> draw_buffers = new ArrayList<>();
	
	// has there been an op (e.g. clear) for which a flush is required?
	private boolean flush_required = false;
	
	public Framebuffer(I3DGL gl_, int name_) {
		super(gl_, name_);
	}
	
	private void initMeta() {
		markAccessorDirty();
		// block meta: flags, zmin, zmax
		block_meta_size = 12;
		final int xblocks = Rasterizer.blockRoundUp(minImageSize().x) >>> Rasterizer.blockBits();
		final int yblocks = Rasterizer.blockRoundUp(minImageSize().y) >>> Rasterizer.blockBits();
		// block meta is grouped by worker to avoid false sharing and allow small meta sizes
		worker_meta_xblocks = (xblocks + WorkerPool.numWorkers() - 1) / WorkerPool.numWorkers();
		// cache-align each worker's block meta storage
		worker_meta_size = (yblocks * worker_meta_xblocks * block_meta_size + 63) & ~63;
		final int total_worker_meta_size = worker_meta_size * WorkerPool.numWorkers();
		// first 256 bytes is for clear depth/stencil/color
		gl().driver().invokeFree(meta);
		meta = null;
		meta = DataStore.alloc(256 + total_worker_meta_size, "framebuffer meta");
		// init meta
		for (long pwm = meta.address() + 256; pwm < meta.address() + total_worker_meta_size; pwm += worker_meta_size) {
			for (long p = pwm; p < pwm + worker_meta_size; p += block_meta_size) {
				// flags
				Util.unsafe.putInt(p, 0);
				// minz, maxz
				Util.unsafe.putFloat(p + 4, Float.NEGATIVE_INFINITY);
				Util.unsafe.putFloat(p + 8, Float.POSITIVE_INFINITY);
			}
		}
	}
	
	public Vec3i minImageSize() {
		Vec3i minsize = vec3i(Integer.MAX_VALUE);
		for (Attachment a : attachments.values()) {
			minsize = min(minsize, a.image().size());
		}
		return minsize;
	}
	
	public Vec3i maxImageSize() {
		Vec3i maxsize = vec3i(0);
		for (Attachment a : attachments.values()) {
			maxsize = max(maxsize, a.image().size());
		}
		return maxsize;
	}
	
	private static GLEnum[] enums(GLEnum... a) {
		return a;
	}
	
	private static boolean isColorAttachment(GLEnum ap) {
		return ap.valueInt() >= GL_COLOR_ATTACHMENT0.valueInt() && (ap.valueInt() - GL_COLOR_ATTACHMENT0.valueInt()) < 16;
	}
	
	public GLEnum[] canonicalize(GLEnum ap) {
		// https://www.opengl.org/wiki/Default_Framebuffer
		if (isColorAttachment(ap)) return enums(ap); 
		switch (ap.valueInt()) {
		case GL_FRONT_value:
		case GL_FRONT_LEFT_value:
		case GL_FRONT_RIGHT_value:
		case GL_BACK_RIGHT_value:
			// these are valid, but don't actually name a buffer in this implementation
			return enums();
		case GL_FRONT_AND_BACK_value:
		case GL_BACK_value:
		case GL_BACK_LEFT_value:
			return enums(GL_COLOR_ATTACHMENT0);
		case GL_DEPTH_value:
		case GL_DEPTH_ATTACHMENT_value:
			return enums(GL_DEPTH_ATTACHMENT);
		case GL_STENCIL_value:
		case GL_STENCIL_ATTACHMENT_value:
			return enums(GL_STENCIL_ATTACHMENT);
		case GL_DEPTH_STENCIL_value:
		case GL_DEPTH_STENCIL_ATTACHMENT_value:
			return enums(GL_DEPTH_ATTACHMENT, GL_STENCIL_ATTACHMENT);
		default:
			throw gl().throwInvalidEnum("%s is not a valid framebuffer attachment point", ap);
		}
	}
	
	public void attach(GLEnum ap, Texture tex, int level) {
		invokeFlush("attachments changed");
		for (GLEnum e : canonicalize(ap)) {
			if (tex == null) {
				attachments.remove(e);
			} else {
				Attachment at = new Attachment();
				at.texture = tex;
				at.level = level;
				// validate image
				at.image();
				attachments.put(e, at);
			}
		}
		initMeta();
	}
	
	public Attachment attachment(GLEnum ap) {
		GLEnum[] es = canonicalize(ap);
		if (es.length == 0) return null;
		return attachments.get(es[0]).clone();
	}
	
	public void drawBuffers(GLEnum ...bufs) {
		invokeFlush("draw buffers changed");
		// draw_buffers is part of the accessor cache key
		draw_buffers.clear();
		for (GLEnum ap : bufs) {
			GLEnum[] es = canonicalize(ap);
			if (es.length == 0) continue;
			if (!isColorAttachment(es[0])) throw gl().throwInvalidEnum("%s is not a valid draw buffer", es[0]);
			draw_buffers.add(es[0]);
		}
	}
	
	public void invokeClearColorf(Vec4f c) {
		final FramebufferAccessor fba = (FramebufferAccessor) accessor();
		Command cmd = new Command() {
			@Override
			public void run() {
				fba.clearColorf(c.x, c.y, c.z, c.w);
			}
		};
		cmd.require(this, Access.UNIQUE_WRITE);
		gl().driver().invoke(cmd);
	}
	
	public void invokeClearColori(Vec4i c) {
		final FramebufferAccessor fba = (FramebufferAccessor) accessor();
		Command cmd = new Command() {
			@Override
			public void run() {
				fba.clearColori(c.x, c.y, c.z, c.w);
			}
		};
		cmd.require(this, Access.UNIQUE_WRITE);
		gl().driver().invoke(cmd);
	}
	
	public void invokeClearColoru(Vec4i c) {
		final FramebufferAccessor fba = (FramebufferAccessor) accessor();
		Command cmd = new Command() {
			@Override
			public void run() {
				fba.clearColoru(c.x, c.y, c.z, c.w);
			}
		};
		cmd.require(this, Access.UNIQUE_WRITE);
		gl().driver().invoke(cmd);
	}
	
	public void invokeClearDepth(float z) {
		final FramebufferAccessor fba = (FramebufferAccessor) accessor();
		Command cmd = new Command() {
			@Override
			public void run() {
				fba.clearDepth(z);
			}
		};
		cmd.require(this, Access.UNIQUE_WRITE);
		gl().driver().invoke(cmd);
	}
	
	public void invokeClear(boolean depth, boolean stencil, boolean color, int color_index) {
		accessor();
		// any shared_write commands issued before a clear must happen before it; take unique_write briefly to ensure that happens
		gl().driver().invokeWaitResource(this, Access.UNIQUE_WRITE);
		final int flags = (depth ? 1 : 0) | (stencil ? 2 : 0) | (color ? (color_index >= 0 ? (4 << color_index) : (0xFF << 2)) : 0);
		final long pmeta = meta.address();
		final int block_meta_size = this.block_meta_size;
		final int worker_meta_size = this.worker_meta_size;
		Command cmd = new Command() {
			@Override
			public void run() {
				workerInvokeAll(() -> {
					final long pwm = pmeta + 256 + worker_meta_size * WorkerPool.id();
					for (long p = pwm; p < pwm + worker_meta_size; p += block_meta_size) {
						int x = Util.unsafe.getInt(p);
						Util.unsafe.putInt(p, x | flags);
					}
				});
			}
		};
		cmd.require(this, Access.SHARED_WRITE);
		gl().driver().invoke(cmd);
		flush_required = true;
	}
	
	/**
	 * Flush any pending lazy writes to this fbo, e.g. clears.
	 */
	public void invokeFlush(String reason) {
		if (!flush_required) return;
		//gl().debugAPIInfo("Flushing Framebuffer@%d because %s", name(), reason);
		final FramebufferAccessor fba = (FramebufferAccessor) accessor();
		// should not need to ensure any happens-before for previous commands
		AccessorLocalStorage als = accessorLocalStorage();
		final Vec2i minp = als.minp;
		final Vec2i maxp = als.maxp;
		Command cmd = new Command() {
			@Override
			public void run() {
				workerInvokeAll(() -> {
					final int worker_id = WorkerPool.id();
					final int num_workers = WorkerPool.numWorkers();
					final long ptemp = WorkerPool.scratchStorage().address();
					final int q = Rasterizer.blockSize();
					final int minx = minp.x & q;
					final int miny = minp.y & q;
					final int maxx = maxp.x;
					final int maxy = maxp.y;
					for (int y = miny; y < maxy; y += q) {
						for (int x = minx; x < maxx; x += q) {
							if (Rasterizer.rasterWorkerIndex(x, y, 0, num_workers) != worker_id) continue;
							fba.setupBlock(ptemp, x, y, 0, worker_id, false);
						}
					}
					
				});
			}
		};
		cmd.require(this, Access.SHARED_WRITE);
		gl().driver().invoke(cmd);
		flush_required = false;
	}
	
	@Override
	public void prepare(Command c, Access a) {
		// ensure accessor exists
		accessor();
		if (a == Access.READ) {
			// flush required before any read op
			invokeFlush("read access required");
		}
		c.require(meta, a);
		for (Attachment at : attachments.values()) {
			c.require(at.image(), a);
		}
	}
	
	@Override
	public void onDeleteName() {
		super.onDeleteName();
		gl().driver().invokeFree(meta);
		meta = null;
	}
	
	@Override
	public Class<?> accessorInterface() {
		return FramebufferAccessor.class;
	}
	
	@Override
	protected Object accessorCacheKey() {
		return new AccessorCacheKey();
	}
	
	@Override
	protected Object accessorLocalStorageInit() {
		return new AccessorLocalStorage();
	}
	
	@Override
	protected AccessorLocalStorage accessorLocalStorage() {
		return (AccessorLocalStorage) super.accessorLocalStorage();
	}
	
	@Override
	protected void buildAccessor(ClassBuilder cb) throws Exception {
		AccessorLocalStorage als = accessorLocalStorage();
		// calculate bounds (half-open) for union of smallest attachment, viewport (and scissor test eventually)
		// TODO probably deal with the scissor test dynamically
		final Vec3i minsize = minImageSize();
		final int min_x = max(0, gl().viewport_pos.x);
		final int max_x = min(minsize.x, gl().viewport_pos.x + gl().viewport_size.x);
		final int min_y = max(0, gl().viewport_pos.y);
		final int max_y = min(minsize.y, gl().viewport_pos.y + gl().viewport_size.y);
		als.minp = vec2i(min_x, min_y);
		als.maxp = vec2i(max_x, max_y);
		// if all attachments are the same size, we can reuse the texel index calcluation
		als.share_texel_index = minsize.equals(maxImageSize());
		//build
		buildClearColorf(cb);
		buildClearColori(cb);
		buildClearColoru(cb);
		buildClearDepth(cb);
		buildWriteFragment(cb);
		buildSetupBlock(cb);
	}
	
	private void buildClearColorf(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("clearColorf_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, void.class, float.class, float.class, float.class, float.class);
		mb.declareParamNames("r", "g", "b", "a");
		for (int i = 0; i < draw_buffers.size(); i++) {
			Attachment a = attachments.get(draw_buffers.get(i));
			GLEnum fmt = a.image().format();
			if (DataFormat.integral(fmt)) continue;
			mb.constant(meta.address() + 16 + 16 * i);
			DataFormat.buildPutTexel(mb, mb.scope().get("r"), fmt);
		}
		mb.op(Op.RETURN);
	}
	
	private void buildClearColori(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("clearColori_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, void.class, int.class, int.class, int.class, int.class);
		mb.declareParamNames("r", "g", "b", "a");
		for (int i = 0; i < draw_buffers.size(); i++) {
			Attachment a = attachments.get(draw_buffers.get(i));
			GLEnum fmt = a.image().format();
			if (!DataFormat.integral(fmt) || !DataFormat.signed(fmt)) continue;
			mb.constant(meta.address() + 16 + 16 * i);
			DataFormat.buildPutTexel(mb, mb.scope().get("r"), fmt);
		}
		mb.op(Op.RETURN);
	}
	
	private void buildClearColoru(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("clearColoru_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, void.class, int.class, int.class, int.class, int.class);
		mb.declareParamNames("r", "g", "b", "a");
		for (int i = 0; i < draw_buffers.size(); i++) {
			Attachment a = attachments.get(draw_buffers.get(i));
			GLEnum fmt = a.image().format();
			if (!DataFormat.integral(fmt) || DataFormat.signed(fmt)) continue;
			mb.constant(meta.address() + 16 + 16 * i);
			DataFormat.buildPutTexel(mb, mb.scope().get("r"), fmt);
		}
		mb.op(Op.RETURN);
	}
	
	private void buildClearDepth(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("clearDepth_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, void.class, float.class);
		mb.declareParamNames("z");
		Attachment da = attachments.get(GL_DEPTH_ATTACHMENT);
		GLEnum fmt = da.image().format();
		mb.constant(meta.address());
		DataFormat.buildPutTexel(mb, mb.scope().get("z"), fmt);
		mb.op(Op.RETURN);
	}
	
	/**
	 * [pblockout] -> []
	 * 
	 * @param mb
	 * @param pvalue
	 */
	private void buildClearImageBlockUnchecked(MethodBuilder mb, GLEnum fmt, long pvalue) throws Exception {
		for (int j = 0; j < Rasterizer.blockTexels(); j++) {
			mb.op(Op.DUP2);
			mb.constant((long) (j * DataFormat.sizeof(fmt)));
			mb.op(Op.LADD);
			mb.constant(pvalue);
			DataFormat.buildCopy(mb, DataFormat.sizeof(fmt));
		}
		mb.op(Op.POP2);
	}
	
	/**
	 * [pblockout] -> []
	 * 
	 * @param mb
	 * @param pvalue
	 */
	private void buildClearImageBlockChecked(MethodBuilder mb, GLEnum fmt, long pvalue) throws Exception {
		mb.pushScope();
		// apply minyy to pdest
		mb.scope().alloc(long.class, "pdest_yy");
		mb.iload("minyy");
		mb.constant(DataFormat.sizeof(fmt) * Rasterizer.blockSize());
		mb.op(Op.IMUL);
		mb.op(Op.I2L);
		mb.op(Op.LADD);
		mb.lstore("pdest_yy");
		// coords within this block [0,Q)
		mb.scope().alloc(int.class, "xx");
		mb.scope().alloc(int.class, "yy");
		// for (yy = minyy; yy < maxyy; yy++)
		mb.iload("minyy");
		mb.istore("yy");
		mb.iload("yy").label("loop_yy");
		mb.iload("maxyy");
		mb.ilt();
		mb.branch(Op.IFEQ, "done_yy");
		// apply minxx to pdest
		mb.scope().alloc(long.class, "pdest_xx");
		mb.lload("pdest_yy");
		mb.iload("minxx");
		mb.constant(DataFormat.sizeof(fmt));
		mb.op(Op.IMUL);
		mb.op(Op.I2L);
		mb.op(Op.LADD);
		mb.lstore("pdest_xx");
		// for (xx = minxx; xx < maxxx; xx++)
		mb.iload("minxx");
		mb.istore("xx");
		mb.iload("xx").label("loop_xx");
		mb.iload("maxxx");
		mb.ilt();
		mb.branch(Op.IFEQ, "done_xx");
		// clear texel
		mb.lload("pdest_xx");
		mb.constant(pvalue);
		DataFormat.buildCopy(mb, DataFormat.sizeof(fmt));
		// inc pdest_xx
		mb.lload("pdest_xx");
		mb.constant((long) (DataFormat.sizeof(fmt)));
		mb.op(Op.LADD);
		mb.lstore("pdest_xx");
		// inc xx
		mb.iload("xx");
		mb.constant(1);
		mb.op(Op.IADD);
		mb.istore("xx");
		mb.branch(Op.GOTO, "loop_xx");
		mb.op(Op.NOP).label("done_xx");
		// end for xx
		// inc pdest_yy
		mb.lload("pdest_yy");
		mb.constant((long) (DataFormat.sizeof(fmt) * Rasterizer.blockSize()));
		mb.op(Op.LADD);
		mb.lstore("pdest_yy");
		// inc yy
		mb.iload("yy");
		mb.constant(1);
		mb.op(Op.IADD);
		mb.istore("yy");
		mb.branch(Op.GOTO, "loop_yy");
		mb.op(Op.NOP).label("done_yy");
		// end for yy
		mb.popScope();
		// pop2 not needed at the end here because we already consumed it
	}
	
	private void buildClearBlock(MethodBuilder mb) throws Exception {
		AccessorLocalStorage als = accessorLocalStorage();
		mb.op(Op.NOP).label("do_clear");
		// eval bounds within this block if needed
		mb.scope().alloc(int.class, "minxx");
		mb.scope().alloc(int.class, "maxxx");
		mb.scope().alloc(int.class, "minyy");
		mb.scope().alloc(int.class, "maxyy");
		mb.constant(0);
		mb.istore("minxx");
		mb.constant(Rasterizer.blockSize());
		mb.istore("maxxx");
		mb.constant(0);
		mb.istore("minyy");
		mb.constant(Rasterizer.blockSize());
		mb.istore("maxyy");
		mb.iload("bounds_ok");
		mb.branch(Op.IFNE, "do_clear_depth");
		// bounds within this block [0,Q]
		mb.constant(als.minp.x);
		mb.iload("x");
		mb.op(Op.ISUB);
		mb.constant(0);
		mb.imax();
		mb.istore("minxx");
		mb.constant(als.maxp.x);
		mb.iload("x");
		mb.op(Op.ISUB);
		mb.constant(Rasterizer.blockSize());
		mb.imin();
		mb.istore("maxxx");
		mb.constant(als.minp.y);
		mb.iload("y");
		mb.op(Op.ISUB);
		mb.constant(0);
		mb.imax();
		mb.istore("minyy");
		mb.constant(als.maxp.y);
		mb.iload("y");
		mb.op(Op.ISUB);
		mb.constant(Rasterizer.blockSize());
		mb.imin();
		mb.istore("maxyy");
		// start clearing
		mb.iload("blockflags").label("do_clear_depth");
		mb.constant(1);
		mb.op(Op.IAND);
		mb.branch(Op.IFEQ, "do_clear_stencil");
		// clear depth
		final Attachment da = attachments.get(GL_DEPTH_ATTACHMENT);
		if (da != null) {
			mb.pushScope();
			GLEnum fmt = da.image().format();
			mb.lload("pblocktemp");
			mb.constant(12L);
			mb.op(Op.LADD);
			DataFormat.buildGetRaw(mb, 4);
			mb.constant(DataFormat.sizeof(fmt));
			mb.op(Op.IMUL);
			mb.op(Op.I2L);
			mb.constant(da.image().address());
			mb.op(Op.LADD);
			mb.iload("bounds_ok");
			mb.branch(Op.IFNE, "unchecked");
			buildClearImageBlockChecked(mb, fmt, meta.address());
			mb.branch(Op.GOTO, "done");
			mb.op(Op.NOP).label("unchecked");
			buildClearImageBlockUnchecked(mb, fmt, meta.address());
			mb.op(Op.NOP).label("done");
			mb.popScope();
		}
		mb.iload("blockflags").label("do_clear_stencil");
		mb.constant(2);
		mb.op(Op.IAND);
		mb.branch(Op.IFEQ, "do_clear_color0");
		// TODO clear stencil
		
		// clear color buffers
		for (int i = 0; i < draw_buffers.size(); i++) {
			mb.op(Op.NOP).label("do_clear_color" + i);
			Attachment a = attachments.get(draw_buffers.get(i));
			if (a == null) continue;
			mb.pushScope();
			GLEnum fmt = a.image().format();
			mb.iload("blockflags");
			mb.constant(4 << i);
			mb.op(Op.IAND);
			mb.branch(Op.IFEQ, "do_clear_color" + (i + 1));
			mb.lload("pblocktemp");
			mb.constant(als.share_texel_index ? 12L : 16L + i * 4);
			mb.op(Op.LADD);
			DataFormat.buildGetRaw(mb, 4);
			mb.constant(DataFormat.sizeof(fmt));
			mb.op(Op.IMUL);
			mb.op(Op.I2L);
			mb.constant(a.image().address());
			mb.op(Op.LADD);
			mb.iload("bounds_ok");
			mb.branch(Op.IFNE, "unchecked");
			buildClearImageBlockChecked(mb, fmt, meta.address() + 16 + 16 * i);
			mb.branch(Op.GOTO, "done");
			mb.op(Op.NOP).label("unchecked");
			buildClearImageBlockUnchecked(mb, fmt, meta.address() + 16 + 16 * i);
			mb.op(Op.NOP).label("done");
			mb.popScope();
		}
		mb.op(Op.NOP).label("do_clear_color" + draw_buffers.size());
		// clear done
		mb.op(Op.NOP).label("clear_done");
	}
	
	private void buildSetupBlock(ClassBuilder cb) throws Exception {
		// block temp:
		//  0: flags-ish
		//  4: bx
		//  8: by
		// 12: depth texel offset
		// 16: color0 texel offset
		// 20: etc
		final AccessorLocalStorage als = accessorLocalStorage();
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("setupBlock_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, boolean.class, long.class, int.class, int.class, int.class, int.class, boolean.class);
		mb.declareParamNames("pblocktemp", "x", "y", "layer", "worker_id", "fullcover");
		// check and record if entire block is within bounds
		// TODO layer bounds, somewhere, probably not here (immediately after geo shader?)
		mb.lload("pblocktemp");
		mb.iload("x");
		mb.constant(als.minp.x);
		mb.ige();
		mb.iload("x");
		mb.constant(als.maxp.x - Rasterizer.blockSize() + 1);
		mb.ilt();
		mb.op(Op.IAND);
		mb.iload("y");
		mb.constant(als.minp.y);
		mb.ige();
		mb.op(Op.IAND);
		mb.iload("y");
		mb.constant(als.maxp.y - Rasterizer.blockSize() + 1);
		mb.ilt();
		mb.op(Op.IAND);
		mb.op(Op.DUP);
		mb.scope().alloc(int.class, "bounds_ok");
		mb.istore("bounds_ok");
		DataFormat.buildPutRaw(mb, 4);
		// record block coords
		mb.lload("pblocktemp");
		mb.constant(4L);
		mb.op(Op.LADD);
		mb.iload("x");
		DataFormat.buildPutRaw(mb, 4);
		mb.lload("pblocktemp");
		mb.constant(8L);
		mb.op(Op.LADD);
		mb.iload("y");
		DataFormat.buildPutRaw(mb, 4);
		// record block texel indices for attachments
		// if indices are shared between attachments, we only store it once
		boolean shared_index_done = false;
		// depth texel offset
		final Attachment da = attachments.get(GL_DEPTH_ATTACHMENT);
		final GLEnum dfmt = da.image().format();
		final boolean depth_enabled = da != null && gl().glIsEnabled(GL_DEPTH_TEST);
		if (da != null) {
			mb.lload("pblocktemp");
			mb.constant(12L);
			mb.op(Op.LADD);
			mb.iload("x");
			mb.iload("y");
			mb.iload("layer");
			da.image().buildTexelIndex(mb);
			DataFormat.buildPutRaw(mb, 4);
			// if texel indices are shared, they get written to the depth texel index
			shared_index_done = true;
		}
		// color texel offsets
		for (int i = 0; i < draw_buffers.size(); i++) {
			if (als.share_texel_index && shared_index_done) break;
			Attachment a = attachments.get(draw_buffers.get(i));
			if (a == null) continue;
			mb.lload("pblocktemp");
			mb.constant(als.share_texel_index ? 12L : 16L + i * 4);
			mb.op(Op.LADD);
			mb.iload("x");
			mb.iload("y");
			mb.iload("layer");
			a.image().buildTexelIndex(mb);
			DataFormat.buildPutRaw(mb, 4);
			shared_index_done = true;
		}
		// get block meta address
		// this relies on the checkerboard block -> worker mapping
		mb.iload("y");
		mb.constant(Rasterizer.blockBits());
		mb.op(Op.IUSHR);
		mb.constant(worker_meta_xblocks);
		mb.op(Op.IMUL);
		mb.iload("x");
		mb.constant(Rasterizer.blockBits());
		mb.op(Op.IUSHR);
		mb.constant(WorkerPool.numWorkers());
		mb.op(Op.IDIV);
		mb.op(Op.IADD);
		mb.constant(block_meta_size);
		mb.op(Op.IMUL);
		mb.iload("worker_id");
		mb.constant(worker_meta_size);
		mb.op(Op.IMUL);
		mb.op(Op.IADD);
		mb.op(Op.I2L);
		mb.constant(meta.address() + 256);
		mb.op(Op.LADD);
		mb.scope().alloc(long.class, "pblockmeta");
		mb.lstore("pblockmeta");
		// does this block need clearing?
		mb.lload("pblockmeta");
		DataFormat.buildGetRaw(mb, 4);
		mb.scope().alloc(int.class, "blockflags");
		mb.op(Op.DUP);
		mb.istore("blockflags");
		// TODO take advantage of 'fullcover' to maybe skip clearing
		// we need to know in advance if all fragments will pass their tests
		// i.e., if the depth range of this tri over this block always passes relative to the clear depth etc.
		//mb.iload("fullcover");
		//mb.constant(1);
		//mb.op(Op.ISUB);
		//mb.op(Op.IAND);
		
		mb.branch(Op.IFNE, "do_clear");
		mb.branch(Op.GOTO, "clear_done");
		buildClearBlock(mb);
		// unset clear flags
		mb.lload("pblockmeta");
		mb.constant(0);
		DataFormat.buildPutRaw(mb, 4);
		
		// TODO early depth test entire block
		
		mb.constant(1);
		mb.op(Op.IRETURN);
	}
	
	/**
	 * [float src, float dst] -> [bool]
	 * 
	 * @param mb
	 * @param func
	 */
	private void buildDepthTest(MethodBuilder mb, GLEnum func) {
		switch (func.valueInt()) {
		case GL_NEVER_value:
			mb.constant(0);
			break;
		case GL_ALWAYS_value:
			mb.constant(1);
			break;
		case GL_LESS_value:
			mb.flt();
			break;
		case GL_LEQUAL_value:
			mb.fle();
			break;
		case GL_EQUAL_value:
			mb.feq();
			break;
		case GL_NOTEQUAL_value:
			mb.fne();
			break;
		case GL_GEQUAL_value:
			mb.fge();
			break;
		case GL_GREATER_value:
			mb.fgt();
			break;
		default:
			throw new RuntimeException("bad depth func");
		}
	}
	
	/**
	 * [] -> [float factored_value]
	 * 
	 * @param mb
	 * @param i component index
	 * @param fdloc_factor base location of data to be factored 
	 * @param fdloc_src
	 * @param fdloc_dst
	 */
	private void buildBlendFactor(MethodBuilder mb, int i, int fdloc_factor, int fdloc_src, int fdloc_dst, GLEnum factor) {
		switch (factor.valueInt()) {
		case GL_ZERO_value:
			mb.constant(0.f);
			return;
		case GL_ONE_value:
			mb.fload(fdloc_factor + i);
			return;
		case GL_SRC_COLOR_value:
			mb.fload(fdloc_factor + i);
			mb.fload(fdloc_src + i);
			mb.op(Op.FMUL);
			return;
		case GL_ONE_MINUS_SRC_COLOR_value:
			mb.fload(fdloc_factor + i);
			mb.constant(1.f);
			mb.fload(fdloc_src + i);
			mb.op(Op.FSUB);
			mb.op(Op.FMUL);
			return;
		case GL_DST_COLOR_value:
			mb.fload(fdloc_factor + i);
			mb.fload(fdloc_dst + i);
			mb.op(Op.FMUL);
			return;
		case GL_ONE_MINUS_DST_COLOR_value:
			mb.fload(fdloc_factor + i);
			mb.constant(1.f);
			mb.fload(fdloc_dst + i);
			mb.op(Op.FSUB);
			mb.op(Op.FMUL);
			return;
		case GL_SRC_ALPHA_value:
			mb.fload(fdloc_factor + i);
			mb.fload(fdloc_src + 3);
			mb.op(Op.FMUL);
			return;
		case GL_ONE_MINUS_SRC_ALPHA_value:
			mb.fload(fdloc_factor + i);
			mb.constant(1.f);
			mb.fload(fdloc_src + 3);
			mb.op(Op.FSUB);
			mb.op(Op.FMUL);
			return;
		case GL_DST_ALPHA_value:
			mb.fload(fdloc_factor + i);
			mb.fload(fdloc_dst + 3);
			mb.op(Op.FMUL);
			return;
		case GL_ONE_MINUS_DST_ALPHA_value:
			mb.fload(fdloc_factor + i);
			mb.constant(1.f);
			mb.fload(fdloc_dst + 3);
			mb.op(Op.FSUB);
			mb.op(Op.FMUL);
			return;
		case GL_CONSTANT_COLOR_value:
			mb.fload(fdloc_factor + i);
			mb.constant(gl().blend_state.blend_color.get(i));
			mb.op(Op.FMUL);
			return;
		case GL_ONE_MINUS_CONSTANT_COLOR_value:
			mb.fload(fdloc_factor + i);
			mb.constant(1.f - gl().blend_state.blend_color.get(i));
			mb.op(Op.FMUL);
			return;
		case GL_CONSTANT_ALPHA_value:
			mb.fload(fdloc_factor + i);
			mb.constant(gl().blend_state.blend_color.get(3));
			mb.op(Op.FMUL);
			return;
		case GL_ONE_MINUS_CONSTANT_ALPHA_value:
			mb.fload(fdloc_factor + i);
			mb.constant(1.f - gl().blend_state.blend_color.get(3));
			mb.op(Op.FMUL);
			return;
		case GL_SRC_ALPHA_SATURATE_value:
			// what exactly is this for?
			mb.fload(fdloc_factor + i);
			if (i < 3) {
				// min(As, 1 - Ad)
				mb.fload(fdloc_src + 3);
				mb.constant(1.f);
				mb.fload(fdloc_dst + 3);
				mb.op(Op.FSUB);
				mb.fmin();
				mb.op(Op.FMUL);
			}
		default:
			mb.constant(0.f);
			return;
		}
	}
	
	/**
	 * [] -> []
	 * 
	 * @param mb
	 * @param fdloc_out blend output
	 * @param fdloc_src blend input from fragment shader
	 * @param fdloc_dst blend input from framebuffer
	 */
	private void buildBlend(MethodBuilder mb, int fdloc_out, int fdloc_src, int fdloc_dst, BlendBufferState bb) {
		for (int i = 0; i < 4; i++) {
			buildBlendFactor(mb, i, fdloc_src, fdloc_src, fdloc_dst, bb.channels[i].sfactor);
			buildBlendFactor(mb, i, fdloc_dst, fdloc_src, fdloc_dst, bb.channels[i].dfactor);
			switch (bb.channels[i].mode.valueInt()) {
			case GL_FUNC_ADD_value:
				mb.op(Op.FADD);
				break;
			case GL_FUNC_SUBTRACT_value:
				mb.op(Op.FSUB);
				break;
			case GL_FUNC_REVERSE_SUBTRACT_value:
				mb.op(Op.SWAP);
				mb.op(Op.FSUB);
				break;
			case GL_MIN_value:
				mb.fmin();
				break;
			case GL_MAX_value:
				mb.fmax();
				break;
			default:
				mb.op(Op.POP);
				break;
			}
			mb.fstore(fdloc_out + i);
		}
	}
	
	private void buildWriteFragment(ClassBuilder cb) throws Exception {
		final AccessorLocalStorage als = accessorLocalStorage();
		// TODO handle layered attachments
		MethodBuilder mb = cb.declareMethod();
		mb.declareName("writeFragment_static");
		mb.declareFlag(Flag.PUBLIC);
		mb.declareType(true, boolean.class, int.class, int.class, int.class, float.class, long.class, long.class);
		mb.declareParamNames("accept", "xx", "yy", "depth", "pblocktemp", "pfrag_in");
		// check if entire block in bounds
		// under normal circumstances, this should almost always be true
		mb.lload("pblocktemp");
		DataFormat.buildGetRaw(mb, 4);
		mb.branch(Op.IFNE, "bounds_pass");
		// get actual texel coords for bounds checking 
		mb.scope().alloc(int.class, "x");
		mb.scope().alloc(int.class, "y");
		mb.lload("pblocktemp");
		mb.constant(4L);
		mb.op(Op.LADD);
		DataFormat.buildGetRaw(mb, 4);
		mb.iload("xx");
		mb.op(Op.IADD);
		mb.istore("x");
		mb.lload("pblocktemp");
		mb.constant(8L);
		mb.op(Op.LADD);
		DataFormat.buildGetRaw(mb, 4);
		mb.iload("yy");
		mb.op(Op.IADD);
		mb.istore("y");
		// check if fragment is within bounds
		mb.iload("x");
		mb.constant(als.minp.x);
		mb.ige();
		mb.iload("x");
		mb.constant(als.maxp.x);
		mb.ile();
		mb.op(Op.IAND);
		mb.iload("y");
		mb.constant(als.minp.y);
		mb.ige();
		mb.op(Op.IAND);
		mb.iload("y");
		mb.constant(als.maxp.y);
		mb.ile();
		mb.op(Op.IAND);
		// return on bounds fail
		// we (kinda) need this 'second' branch to avoid trying to fetch out of bounds for depth testing etc.
		// i think hotspot uses branches for each of the bounds comparisons
		mb.branch(Op.IFNE, "bounds_pass");
		mb.constant(0);
		mb.op(Op.IRETURN);
		mb.op(Op.NOP).label("bounds_pass");
		// shared texel offset
		if (als.share_texel_index) {
			mb.lload("pblocktemp");
			mb.constant(12L);
			mb.op(Op.LADD);
			DataFormat.buildGetRaw(mb, 4);
			mb.iload("yy");
			mb.constant(Rasterizer.blockBits());
			mb.op(Op.ISHL);
			mb.iload("xx");
			mb.op(Op.IADD);
			mb.op(Op.IADD);
			mb.scope().alloc(int.class, "texelindex");
			mb.istore("texelindex");
		}
		// depth test		
		final Attachment da = attachments.get(GL_DEPTH_ATTACHMENT);
		final GLEnum dfmt = da!= null ? da.image().format() : null;
		final boolean depth_enabled = da != null && gl().glIsEnabled(GL_DEPTH_TEST);
		if (depth_enabled) {
			// get depth texel address
			if (als.share_texel_index) {
				mb.iload("texelindex");
			} else {
				mb.lload("pblocktemp");
				mb.constant(12L);
				mb.op(Op.LADD);
				DataFormat.buildGetRaw(mb, 4);
				mb.iload("yy");
				mb.constant(Rasterizer.blockBits());
				mb.op(Op.ISHL);
				mb.iload("xx");
				mb.op(Op.IADD);
				mb.op(Op.IADD);
			}
			mb.constant(DataFormat.sizeof(dfmt));
			mb.op(Op.IMUL);
			mb.op(Op.I2L);
			mb.constant(da.image().address());
			mb.op(Op.LADD);
			mb.scope().alloc(long.class, "pdepthtexel");
			mb.lstore("pdepthtexel");
			// test
			mb.fload("depth");
			mb.scope().alloc(float.class, "dstdepth");
			mb.lload("pdepthtexel");
			DataFormat.buildGetTexel(mb, mb.scope().get("dstdepth"), dfmt);
			mb.fload("dstdepth");
			// depth comparisons end up causing branches anyway. oh well.
			buildDepthTest(mb, gl().depth_func);
			mb.scope().alloc(int.class, "depth_result");
			mb.istore("depth_result");
		}
		
		// TODO stencil test and ops eventually
		
		// check if fragment passed tests
		mb.iload("accept");
		if (depth_enabled) {
			mb.iload("depth_result");
			mb.op(Op.IAND);
		}
		// return on test fail
		mb.branch(Op.IFNE, "test_pass");
		mb.constant(0);
		mb.op(Op.IRETURN);
		mb.op(Op.NOP).label("test_pass");
		// write depth
		if (depth_enabled) {
			mb.lload("pdepthtexel");
			DataFormat.buildPutTexel(mb, mb.scope().get("depth"), dfmt);
		}
		// write colors
		for (int i = 0; i < draw_buffers.size(); i++) {
			Attachment a = attachments.get(draw_buffers.get(i));
			if (a == null) continue;
			GLEnum fmt = a.image().format();
			mb.pushScope();
			// TODO blending
			// load frag data
			mb.lload("pfrag_in");
			mb.constant(64L * i);
			mb.op(Op.LADD);
			final int fdloc_src = mb.scope().alloc(4);
			// get interleaved components
			for (int j = 0; j < 4; j++) {
				mb.op(Op.DUP2);
				if (DataFormat.integral(fmt)) {
					DataFormat.buildGetRaw(mb, 4);
					mb.istore(fdloc_src + j);
				} else {
					DataFormat.buildGetFloat(mb, GL_FLOAT, false);
					mb.fstore(fdloc_src + j);
				}
				mb.constant(16L);
				mb.op(Op.LADD);
			}
			mb.op(Op.POP2);
			// get texel address
			if (als.share_texel_index) {
				mb.iload("texelindex");
			} else {
				mb.lload("pblocktemp");
				mb.constant(16L + i);
				mb.op(Op.LADD);
				DataFormat.buildGetRaw(mb, 4);
				mb.iload("yy");
				mb.constant(Rasterizer.blockBits());
				mb.op(Op.ISHL);
				mb.iload("xx");
				mb.op(Op.IADD);
				mb.op(Op.IADD);
			}
			mb.constant(DataFormat.sizeof(fmt));
			mb.op(Op.IMUL);
			mb.op(Op.I2L);
			mb.constant(a.image().address());
			mb.op(Op.LADD);
			mb.scope().alloc(long.class, "ptexel");
			mb.lstore("ptexel");
			if (gl().glIsEnabled(GL_BLEND) && !DataFormat.integral(fmt)) {
				// blending
				// TODO dual source blending?
				final int fdloc_dst = mb.scope().alloc(4);
				mb.lload("ptexel");
				DataFormat.buildGetTexel(mb, fdloc_dst, fmt);
				final int fdloc_out = mb.scope().alloc(4);
				buildBlend(mb, fdloc_out, fdloc_src, fdloc_dst, gl().blend_state.buffers[i]);
				mb.lload("ptexel");
				DataFormat.buildPutTexel(mb, fdloc_out, fmt);
			} else {
				// just write
				mb.lload("ptexel");
				DataFormat.buildPutTexel(mb, fdloc_src, fmt);
			}
			mb.popScope();
		}
		
		mb.constant(1);
		mb.op(Op.IRETURN);
	}

}
