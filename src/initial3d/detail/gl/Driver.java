package initial3d.detail.gl;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import initial3d.detail.Access;

public class Driver {

	private final Thread thread;
	private final I3DGL gl;
	
	private volatile boolean should_shutdown = false;
	
	// stats
	private long time_last = System.nanoTime();
	private long dur_idle = 0;
	private long completed = 0;
	
	// waitable condition for interesting things
	private final Semaphore something_interesting = new Semaphore(0);
	
	// commands that will actually be issued on next flush
	private final ArrayList<Command> incoming_commands = new ArrayList<>();
	
	// commands waiting for execution
	private final BlockingQueue<Command> pending_commands = new LinkedBlockingQueue<>();
	
	// commands that have started executing and are awaiting completion 
	private final ArrayList<Command> active_commands = new ArrayList<>();
	
	private final AtomicInteger async_active_commands = new AtomicInteger(0);
	
	public Driver(I3DGL gl_) {
		gl = gl_;
		thread = new Thread(this::run);
		thread.setDaemon(true);
		thread.setName("Initial3D GL driver");
		thread.setPriority(Thread.MAX_PRIORITY);
		thread.start();
	}
	
	public I3DGL gl() {
		return gl;
	}
	
	public void shutdown() {
		should_shutdown = true;
		fireEvent();
	}
	
	public void flush() {
		// even without doing discrete arbitration, flushing commands into the pending queue
		// reduces the number of fireEvent() calls that are issued, which may be helpful
		if (incoming_commands.isEmpty()) return;
		// actually issue commands
		pending_commands.addAll(incoming_commands);
		incoming_commands.clear();
		fireEvent();
	}
	
	public void invoke(Command c) {
		c.requestAccess();
		incoming_commands.add(c);
		if (incoming_commands.size() > 100) flush();
	}
	
	/**
	 * Invoke a command which puts the driver to sleep for the specified time.
	 * Mainly for debugging.
	 * 
	 * @param millis
	 */
	public void invokeSleep(int millis) {
		invoke(new Command() {
			@Override
			public void run() {
				try {
					System.err.println("GL driver sleeping for " + millis);
					Thread.sleep(millis);
					System.err.println("GL driver sleep finished");
				} catch (InterruptedException e) {
					// sdfgdfsg
				}
			}
		});
	}
	
	public void invokeFenceSync(Sync sync) {
		invoke(new Command() {
			@Override
			public void run() { }

			@Override
			public void onCompletion() {
				sync.signal();
			}
		});
	}
	
	public void invokeWaitSync(Sync sync) {
		invoke(new Command() {
			@Override
			public void run() {
				while (!sync.signaled()) {
					driver().checkCompletion();
					driver().waitEvent();
				}
			}
		});
	}
	
	public void invokeFinish() {
		Sync sync = gl().resource(gl().genName(), Sync.class);
		invokeFenceSync(sync);
		invokeWaitSync(sync);
		gl().deleteName(sync.name());
	}
	
	public void invokeWaitResource(CommandPrereq r, Access a) {
		Command c = new Command() {
			@Override
			public void run() { }
		};
		c.require(r, a);
		invoke(c);
	}
	
	public void invokeCopy(final DataStore dst, final int di, final DataStore src, final int si, final int size) {
		Command c = new Command() {
			@Override
			public void run() {
				ByteBuffer dst_buf = dst.map(di, size);
				ByteBuffer src_buf = src.map(si, size);
				dst_buf.put(src_buf);
			}
		};
		c.require(dst, Access.UNIQUE_WRITE);
		c.require(src, Access.READ);
		invoke(c);
	}
	
	public void invokeCopy(final DataStore dst, final int di, final ByteBuffer src) {
		Command c = new Command() {
			@Override
			public void run() {
				ByteBuffer dst_buf = dst.map(di);
				dst_buf.put(src);
			}
		};
		c.require(dst, Access.UNIQUE_WRITE);
	}
	
	/**
	 * Free a datastore when all previous commands have completed.
	 * 
	 * @param s
	 */
	public void invokeFree(final DataStore s) {
		if (s == null) return;
		if (s.address() == 0) return;
		invoke(new Command(){
			@Override
			public void run() {}
			
			@Override
			public void onCompletion() {
				DataStore.free(s);
			}
		});
	}
	
	/*
	 * Remove any active commands that have finished and call completion callbacks.
	 * Must only be called from the driver thread.
	 */
	public void checkCompletion() {
		for (Iterator<Command> it = active_commands.iterator(); it.hasNext(); ) {
			Command c = it.next();
			if (c.idle()) {
				// command is complete
				it.remove();
				c.finish();
				completed++;
			} else {
				// break when any command is not complete
				break;
			}
		}
	}
	
	/**
	 * Inform the driver that something interesting happened.
	 * Can be called from any thread.
	 */
	public void fireEvent() {
		something_interesting.release();
	}
	
	/**
	 * Wait for something interesting to happen.
	 * Must only be called from the driver thread.
	 */
	public void waitEvent() {
		try {
			long time0 = System.nanoTime();
			// use a semaphore so that fire can happen _before_ wait and still wake the driver
			something_interesting.acquire();
			long time1 = System.nanoTime();
			dur_idle += time1 - time0;
		} catch (InterruptedException e) {
			// bleh
		}
	}
	
	public void commandAsyncStart() {
		async_active_commands.incrementAndGet();
	}
	
	public void commandAsyncFinish() {
		if (async_active_commands.decrementAndGet() < 5) {
			fireEvent();
		}
	}
	
	private void printStats() {
		long time_now = System.nanoTime();
		if (time_now >= time_last + 1000000000L) {
			double a = (time_now - time_last - dur_idle) / (double) (time_now - time_last);
			System.err.printf(
				"Initial3D GL driver utilisation: %.2f%%; completed: %d; active: %d; pending: %d\n",
				a * 100, completed, active_commands.size(), pending_commands.size()
			);
			time_last = time_now;
			dur_idle = 0;
			completed = 0;
		}
	}
	
	private void run() {
		// process commands
		while (!should_shutdown) {
			try {
				printStats();
				checkCompletion();
				// grab a pending command
				Command c = pending_commands.poll();
				if (c == null) {
					// no pending command, wait for something interesting to happen
					waitEvent();
				} else {
					// init, wait for data store access
					// fire once so we will always retry immediately after the first failure
					fireEvent();
					while (!c.start(this)) {
						if (active_commands.isEmpty()) {
							// start should not fail when no commands are active
							// because there should be nothing to prevent acquiring all needed accesses
							throw new RuntimeException("failed to start command " + c, c.stackTrace());
						}
						// wait for something interesting (like command task completion)
						waitEvent();
						// wait _before_ check, because after a check that resulted in all
						// active commands completing there will likely be nothing to fire events
						checkCompletion();
					}
					// add to active list
					active_commands.add(c);
					// run it
					c.run();
				}
			} catch (Exception e) {
				System.err.println("Initial3D GL driver thread internal error: " + e.getMessage());
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

}










