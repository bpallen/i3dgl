package initial3d.detail.gl;

import initial3d.GLEnum;

public abstract class VertexArrayAccessor {
	
	/**
	 * Get the type of primitives loaded from this VAO.
	 * One of <code>GL_POINTS, GL_LINES, GL_LINES_ADJACENCY, GL_TRIANGLES, GL_TRIANGLES_ADJACENCY</code>.
	 * Things like <code>GL_LINE_STRIP</code> will be decoded by {@link #loadPrimitive}
	 * (in that case, to <code>GL_LINES</code>).
	 * 
	 * TODO May not need this as {@link #primitiveVertexCount()} is roughly equivalent
	 * 
	 * @return
	 */
	public abstract GLEnum primitiveType();
	
	/**
	 * @return Number of vertices in primitives loaded from this VAO.
	 */
	public abstract int primitiveVertexCount();
	
	/**
	 * Get total number of primitives to be loaded <i>excluding</i> any
	 * special case last primitive.
	 * 
	 * @param count total vertex indices
	 * @return number of primitives
	 */
	public abstract int primitiveCount(int count);
	
	/**
	 * Load the vertex indices for a primitve.
	 * 
	 * @param p_out output pointer
	 * @param first index (into IBO) of first vertex index
	 * @param count total vertex indices
	 * @param ip primitive index
	 */
	public abstract void loadPrimitive(long p_out, int first, int count, int ip);
	
	/**
	 * Load any special case last primitive (as for <code>GL_LINE_LOOP, GL_TRIANGLE_FAN</code>).
	 * 
	 * @param p_out output pointer
	 * @param first index (into IBO) of first vertex index
	 * @param count total vertex indices
	 * @return true iff a primitive was loaded
	 */
	public abstract boolean loadLastPrimitive(long p_out, int first, int count);
	
	/**
	 * @return Size of a loaded vertex in bytes.
	 */
	public abstract int vertexSize();
	
	/**
	 * Load vertex from index directly referencing vbos (does not touch ibo).
	 * 
	 * @param p_out output pointer
	 * @param iv vertex index
	 */
	public abstract void loadVertex(long p_out, int iv);
	
}
