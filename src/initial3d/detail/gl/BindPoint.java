package initial3d.detail.gl;

import initial3d.GLEnum;

public class BindPoint {
	
	private GLEnum target;
	private int index;
	
	public BindPoint(GLEnum target_, int index_) {
		target = target_;
		index = index_;
	}
	
	public BindPoint(GLEnum target_) {
		target = target_;
		index = -1;
	}
	
	public GLEnum target() {
		return target;
	}
	
	public int index() {
		return index;
	}
	
	@Override
	public String toString() {
		return target.toString() + (index >= 0 ? ("#" + index) : "");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + index;
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BindPoint other = (BindPoint) obj;
		if (index != other.index)
			return false;
		if (target != other.target)
			return false;
		return true;
	}
	
}
