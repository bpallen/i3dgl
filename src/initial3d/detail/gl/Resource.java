package initial3d.detail.gl;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import initial3d.detail.Access;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.bytecode.Flag;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;

/**
 * Base class for named GL resources.
 * 
 * @author ben
 *
 */
public abstract class Resource implements CommandPrereq {

	private static class ALSContainer {
		public Object accessor;
		public Object als;
		
		public ALSContainer(Object accessor_, Object als_) {
			accessor = accessor_;
			als = als_;
		}
	}
	
	// 'timestamp' for accessor builds
	private static final AtomicInteger build_counter = new AtomicInteger(1);
	
	private final I3DGL gl;
	private int name;
	
	// cached built accessors
	private HashMap<Object, ALSContainer> accessors = new HashMap<>();
	
	// timestamp for building the accessor
	private int accessor_time = 0;
	
	// does the accessor need rebuilding?
	private boolean accessor_dirty = true;
	
	/**
	 * All resources must have a constructor with this signature.
	 * 
	 * @param gl_
	 * @param name_
	 */
	public Resource(I3DGL gl_, int name_) {
		gl = gl_;
		name = name_;
	}
	
	public I3DGL gl() {
		return gl;
	}
	
	public int name() {
		return name;
	}
	
	/**
	 * Prepare a command for use of this resource. Should be overridden to behave
	 * transitively for all used resources.
	 * 
	 * @param c
	 */
	@Override
	public abstract void prepare(Command c, Access a);
	
	/**
	 * @return The superclass of built accessors representing their runtime interface.
	 */
	public Class<?> accessorInterface() {
		return Object.class;
	}
	
	/**
	 * Override to provide all resources on which an accessor would depend.
	 * 
	 * @return
	 */
	protected Iterable<? extends Resource> accessorDependencies() {
		return Collections.emptyList();
	}
	
	/**
	 * Get an object to use as a key for caching built accessors.
	 * The returned object should implement {@link Object#equals(Object)}, {@link Object#hashCode()}
	 * and {@link Object#clone()} but does not need to be immutable.
	 * It should encapsulate all state on which accessors will depend
	 * but changes to which will not cause them to be marked dirty.
	 * This allows accessors to be reused in the case that this state returns to a previous value.
	 * Objects returned from this method will be cloned before being stored.
	 * Return value must not be null.
	 * 
	 * Think FBOs and depth tests, blend functions etc.
	 * 
	 * The default return value is {@link NullAccessorCacheKey#INSTANCE}.
	 * 
	 * @return 
	 */
	protected Object accessorCacheKey() {
		return NullAccessorCacheKey.INSTANCE;
	}
	
	/**
	 * @return Initial accessor local storage object
	 */
	protected Object accessorLocalStorageInit() {
		return null;
	}
	
	/**
	 * @return Accessor local storage object as supplied by {@link #accessorLocalStorageInit()}
	 * for the accessor determined by the current value of {@link #accessorCacheKey()}.
	 */
	protected Object accessorLocalStorage() {
		ALSContainer alsc = accessors.get(accessorCacheKey());
		if (alsc == null) throw new IllegalStateException("can't use accessor local storage for unbuilt accessor");
		return alsc.als;
	}
	
	public Object accessor() {
		// see if any dependencies have been modified after our last build
		for (Resource r : accessorDependencies()) {
			if (r.accessor_time > accessor_time) {
				accessor_dirty = true;
				break;
			}
		}
		// if dirty, drop all cached accessors
		if (accessor_dirty) {
			if (!accessors.isEmpty()) gl().debugAPIInfo("Dropping dirty cached accessors for %s", this);
			accessors.clear();
		}
		// build accessor only if uncached
		Object ck = accessorCacheKey();
		if (ck == null) throw new RuntimeException("null accessor cache key");
		if (!accessors.containsKey(ck)) {
			try {
				// clone cache key before storing
				Method clone_func = Object.class.getDeclaredMethod("clone");
				clone_func.setAccessible(true);
				ck = clone_func.invoke(ck);
				gl().debugAPIInfo("Building new accessor for %s with cache key %s", this, ck);
				accessor_time = build_counter.getAndIncrement();
				// build and cache it
				// put als container in first so als is usable from createAccessor/buildAccessor
				ALSContainer alsc = new ALSContainer(null, accessorLocalStorageInit());
				accessors.put(ck, alsc);
				alsc.accessor = createAccessor();
			} catch (Exception e) {
				throw new RuntimeException("accessor build failed", e);
			}
		}
		accessor_dirty = false;
		return accessors.get(ck).accessor;
	}
	
	/**
	 * Force rebuilding of accessors.
	 */
	protected void markAccessorDirty() {
		accessor_dirty = true;
	}
	
	/**
	 * Override to specify accessor objects directly.
	 * 
	 * @return
	 * @throws Exception
	 */
	protected Object createAccessor() throws Exception {
		ClassBuilder cb = new ClassBuilder();
		cb.declareFlag(Flag.PUBLIC);
		cb.declareNameUnique("initial3d.detail.gen." + accessorInterface().getSimpleName());
		cb.declareSuperclass(accessorInterface());
		// build default ctor
		buildAccessorCtor(cb);
		// build class body
		buildAccessor(cb);
		// for each public non-static method in the interface class
		for (Method m : accessorInterface().getDeclaredMethods()) {
			if (!Modifier.isPublic(m.getModifiers())) continue;
			if (Modifier.isStatic(m.getModifiers())) continue;
			if (cb.method(m.getName(), m.getReturnType(), m.getParameterTypes()) == null) {
				// non-static interface method is not defined
				// create it automagically by calling static version
				MethodBuilder mb = cb.declareMethod();
				mb.declareFlag(Flag.PUBLIC);
				mb.declareName(m.getName());
				mb.declareType(false, m.getReturnType(), m.getParameterTypes());
				// load params
				mb.maxStack(mb.scope().nextIndex());
				for (int i = 0; i < m.getParameterCount(); i++) {
					mb.load(m.getParameterTypes()[i], "arg" + i);
				}
				// forward to static method
				mb.invokeStatic(m.getName() + "_static", m.getReturnType(), m.getParameterTypes());
				mb.ret(m.getReturnType());
			}
		}
		Class<?> acls = cb.build();
		return acls.newInstance();
	}
	
	/**
	 * Override to customize the default constructor build behaviour.
	 * 
	 * @param cb
	 * @throws Exception
	 */
	protected void buildAccessorCtor(ClassBuilder cb) throws Exception {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("<init>");
		mb.declareType(false, void.class);
		mb.aload(0);
		mb.invokeSpecial(accessorInterface(), "<init>", void.class);
		mb.op(Op.RETURN);
	}
	
	/**
	 * Override to build the body of this resource's accessor class.
	 * 
	 * Accessor classes need to encapsulate whatever resource state they need to function,
	 * and should not retain references to Resource objects.
	 * 
	 * They can rely on any DataStores being accessible from a Command provided the Resource
	 * has the Command require them.
	 * 
	 * Implementers should override {@link #accessorInterface()}. Accessor classes should
	 * declare static methods for each method in the interface and name them with the suffix
	 * <code>_static</code>. Non-static interface methods will be built automagically
	 * to forward to the static methods, but can be specified explictly if needed.
	 * 
	 * @return
	 */
	protected void buildAccessor(ClassBuilder cb) throws Exception {
		
	}

	/**
	 * Called immediately before the name -> resource mapping is deleted.
	 * Must call <code>super.onDelete()</code> if overriding.
	 * Sets this resource's name to -1.
	 */
	public void onDeleteName() {
		name = -1;
	}
	
	@Override
	public final int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public final boolean equals(Object o) {
		return super.equals(o);
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "@" + name();
	}
	
}
