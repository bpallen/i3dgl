package initial3d.detail.gl;

import initial3d.detail.Util;

/**
 * Accessor superclass for Texture, BufferTexture and Sampler (when they exist).
 * 
 * @author ben
 *
 */
public abstract class SamplerAccessor {
	
	public static void texture_static(long pout, float x, float y, float z, float lod) {
		Util.putf(pout, 0, 0, 0, 0);
	}
	
	public static void texelFetch_static(long pout, int x, int y, int z, int lod) {
		Util.putf(pout, 0, 0, 0, 0);
	}
	
	public static void textureSize_static(long pout, int lod) {
		Util.puti(pout, 0, 0, 0, 0);
	}
	
	public abstract void texture(long pout, float x, float y, float z, float lod);
	
	public abstract void texelFetch(long pout, int x, int y, int z, int lod);
	
	public abstract void textureSize(long pout, int lod);
	
}
