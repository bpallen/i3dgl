package initial3d.detail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Thing for queuing up access promises and granting them in turn.
 * 
 * Futures granted by this arbitrator require individual threads to balance acquires and releases.
 * 
 * @author ben
 *
 */
public class AccessQueue implements AccessArbitrator {

	// whether to enable acquire/release book-keeping for deadlock detection
	// and checking for unbalanced releases
	private static boolean debug = false;
	
	public static void debug(boolean b) {
		debug = b;
	}
	
	private static class ThreadData {
		// active promises and acquire counts
		public final HashMap<AccessPromise, AtomicInteger> promises = new HashMap<>();
	}
	
	private Access mode = Access.READ;
	private final ArrayList<AccessPromise> active_promises = new ArrayList<>();
	private final LinkedList<AccessPromise> pending_promises = new LinkedList<>();
	private int active_satisfied = 0;
	
	// most recent promise
	private AccessPromise last_promise = null;
	
	// per-thread data
	private final HashMap<Thread, ThreadData> active_threads = new HashMap<>();
	
	public AccessQueue() {
		
	}
	
	private static int maxConcurrentAccess(Access a) {
		switch (a) {
		case UNIQUE_WRITE:
			return 1;
		default:
			return Integer.MAX_VALUE;
		}
	}
	
	@Override
	public synchronized void arbitrate() {
		final ArrayList<AccessPromise> active_promises = this.active_promises;
		final LinkedList<AccessPromise> pending_promises = this.pending_promises;
		// remove any satisfied promises
		for (Iterator<AccessPromise> it = active_promises.iterator(); it.hasNext(); ) {
			AccessPromise p = it.next();
			if (p.satisfied()) {
				--active_satisfied;
				it.remove();
			}
		}
		// grant some number of promises
		for (; !pending_promises.isEmpty() && active_promises.size() < maxConcurrentAccess(mode); ) {
			final AccessPromise p = pending_promises.peek();
			// if next pending promise is already satisfied, remove and try again
			if (p.satisfied()) {
				pending_promises.poll();
				continue;
			}
			// if no active promises, maybe change access mode
			if (active_promises.isEmpty()) { 
				mode = p.access();
			}
			// if next pending promise is for a different mode, stop
			// there has to be at least one active promise for this
			if (p.access() != mode) break;
			// otherwise, grant it
			pending_promises.poll();
			// if promise is not satisfied upon granting, add to active list
			if (!p.grant()) active_promises.add(p);
		}
		// the end result should be no pending promises or at least one active promise
	}
	
	@Override
	public synchronized AccessPromise requestAccess(Access a, boolean arbitrate_now) {
		AccessPromise p = null;
		if (last_promise != null && last_promise.access() == a) {
			// attempt to reuse most recent promise
			// this is 'promise coalescing' which drastically reduces arbitration
			// load for large numbers of sequential requests for the same access mode 
			if (last_promise.requireMoreSatisfaction(maxConcurrentAccess(a))) {
				p = last_promise;
			}
		}
		if (p == null) {
			// need new promise
			p = new AccessPromise(this, a);
			pending_promises.offer(p);
			// reusing an existing promise should never require arbitration
			if (arbitrate_now) arbitrate();
		}
		last_promise = p;
		return p;
	}
	
	@Override
	public synchronized void checkAcquire(AccessPromise p) {
		// the acquire/release book-keeping allows this method to detect deadlock
		if (!debug) return;
		// (the order of these checks is important)
		// if promise has been granted => ok
		if (p.granted()) return;
		// no thread data => ok
		ThreadData td = active_threads.get(Thread.currentThread());
		if (td == null) return;
		// if no active promises on this thread => ok
		if (td.promises.isEmpty()) return;
		// if promise already active on this thread => ok
		if (td.promises.containsKey(p)) return;
		// if promise is for a different access mode to the active mode => bad
		if (p.access() != mode) throw new DeadlockException();
		// if promise can share access with active promises on this thread => ok
		// (can assume that all active_promises have the parent queue's access mode)
		if (td.promises.size() < maxConcurrentAccess(mode)) return;
		// otherwise => deadlock
		throw new DeadlockException();
	}
	
	@Override
	public synchronized void onAcquire(AccessPromise p) {
		if (!debug) return;
		// ensure thread data exists
		ThreadData td = active_threads.get(Thread.currentThread());
		if (td == null) {
			td = new ThreadData();
			active_threads.put(Thread.currentThread(), td);
		}
		// ensure count for this promise exists
		AtomicInteger ac = td.promises.get(p);
		if (ac == null) {
			// add promise to thread's active set
			ac = new AtomicInteger(0);
			td.promises.put(p, ac);
		}
		// increment acquire count
		ac.incrementAndGet();
	}

	@Override
	public synchronized void onRelease(AccessPromise p) {
		if (!debug) return;
		// no thread data => bad
		ThreadData td = active_threads.get(Thread.currentThread());
		if (td == null) throw new IllegalStateException("unbalanced release");
		// no acquire count => bad
		AtomicInteger ac = td.promises.get(p);
		if (ac == null) throw new IllegalStateException("unbalanced release");
		// decrement acquire count
		if (ac.decrementAndGet() == 0) {
			// remove promise from thread's active set
			td.promises.remove(p);
		}
		// maybe remove thread data
		if (td.promises.isEmpty()) {
			active_threads.remove(Thread.currentThread());
		}
	}
	
	@Override
	public synchronized void onSatisfied(AccessPromise p) {
		// only arbitrate if most active promises are satisfied
		if (++active_satisfied > 2 * active_promises.size() / 3) arbitrate();
	}
	
}





