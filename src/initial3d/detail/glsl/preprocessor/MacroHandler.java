package initial3d.detail.glsl.preprocessor;

import java.io.IOException;

import initial3d.detail.glsl.tokens.TokenInput;

public interface MacroHandler {
	
	public TokenInput expand(TokenInput in) throws IOException;
	
}
