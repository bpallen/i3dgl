package initial3d.detail.glsl.preprocessor;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.PragmaHandler;
import initial3d.detail.glsl.lang.source.BuiltinSource;
import initial3d.detail.glsl.tokens.AbstractTokenInput;
import initial3d.detail.glsl.tokens.CatTokenInput;
import initial3d.detail.glsl.tokens.EmptyTokenInput;
import initial3d.detail.glsl.tokens.IteratorTokenInput;
import initial3d.detail.glsl.tokens.OperatorIdentifierTokenInput;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenBuilder;
import initial3d.detail.glsl.tokens.TokenInput;
import initial3d.detail.glsl.tokens.Tokenizer;

public class Preprocessor implements TokenInput {
	
	// TODO more contexts for expectation errors
	
	private TokenInput input;
	private CatTokenInput file_inputs; 
	private InfoLog log;
	private Map<String, MacroHandler> macros = new HashMap<>();
	private Map<String, DirectiveHandler> directives = new HashMap<>();
	private Map<String, PragmaHandler> pragmas = new HashMap<>();
	private boolean has_next_line_offset = false;
	private int next_line_offset = 0;
	private int next_file = -1;
	
	public Preprocessor(InfoLog log_) {
		log = Objects.requireNonNull(log_);
		
		file_inputs = new CatTokenInput(log);
		input = new MacroExpansionTokenInput(new UnknownDirectiveHandlerTokenInput(file_inputs));
		
		// __FILE__ macro handler
		addMacroHandler("__FILE__", (TokenInput in) -> {
			TokenBuilder tb = new TokenBuilder(in.nextToken());
			if (tb.fileName() == null) return null;
			tb.text(tb.fileName());
			tb.type(Token.Type.INT_LITERAL);
			return new IteratorTokenInput(log, tb.toToken());
		});
		
		// __LINE__ macro handler
		addMacroHandler("__LINE__", (TokenInput in) -> {
			TokenBuilder tb = new TokenBuilder(in.nextToken());
			tb.text("" + tb.line());
			tb.type(Token.Type.INT_LITERAL);
			return new IteratorTokenInput(log, tb.toToken());
		});
		
		// __VERSION__ macro handler
		addMacroHandler("__VERSION__", (TokenInput in) -> {
			// TODO handle versions properly
			TokenBuilder tb = new TokenBuilder(in.nextToken());
			tb.text("330");
			tb.type(Token.Type.INT_LITERAL);
			return new IteratorTokenInput(log, tb.toToken());
		});
		
		// #line directive handler
		addDirectiveHandler("line", (Token t0, TokenInput in) -> {
			in.pushContext("handling #line directive", t0);
			try {
				in.expectNextToken("line");
				TokenInput in2 = in.readLineAsStream();
				in2.unexpectPeekToken(Token.Type.EOF);
				int line = evalExpression(in2);
				if (in2.hasNextToken(Token.Type.EOF)) {
					next_line_offset = line - t0.line() - 1;
					has_next_line_offset = true;
					next_file = -1;
				} else {
					int file = evalExpression(in2);
					next_line_offset = line - t0.line() - 1;
					has_next_line_offset = true;
					next_file = file;
				}
				in2.expectPeekToken(Token.Type.EOF);
				return in.asEmpty();
			} finally {
				in.popContext();
			}
		});
		
		// #define directive handler
		addDirectiveHandler("define", (Token t0, TokenInput in) -> {
			in.pushContext("handling #define directive", t0);
			try {
				in.expectNextToken("define");
				Token t2 = in.expectNextToken(Token.Type.IDENTIFIER);
				Token t3 = in.peekToken();
				if (t3.isText("(") && t3.col() == t2.col() + t2.text().length() && t3.line() == t2.line()) {
					// function macro - no space between identifier and '('
					ArrayList<Token> params = new ArrayList<>();
					in.expectNextToken("(");
					while (!in.hasNextToken(")")) {
						params.add(in.expectNextToken(Token.Type.IDENTIFIER));
						in.expectPeekToken(")", ",");
						if (in.hasNextToken(",")) in.nextToken();
					}
					in.expectNextToken(")");
					ArrayList<Token> body = new ArrayList<>();
					in.readLine((Token t) -> { body.add(t); });
					addMacroHandler(t2.text(), new FunctionMacroHandler(t2, params, body));
				} else {
					// plain macro
					ArrayList<Token> body = new ArrayList<>();
					in.readLine((Token t) -> { body.add(t); });
					addMacroHandler(t2.text(), new PlainMacroHandler(t2, body));
				}
				return in.asEmpty();
			} finally {
				in.popContext();
			}
		});
		
		// #undef directive handler
		addDirectiveHandler("undef", (Token t0, TokenInput in) -> {
			in.pushContext("handling #undef directive", t0);
			try {
				in.expectNextToken("undef");
				Token t2 = in.expectNextToken(Token.Type.IDENTIFIER);
				macros.remove(t2.text());
				return in.asEmpty();
			} finally {
				in.popContext();
			}
		});
		
		// #error directive handler
		addDirectiveHandler("error", (Token t0, TokenInput in) -> {
			in.expectNextToken("error");
			Token t2 = in.peekToken();
			String msg = "error directive: " + t2.lineText().substring(t2.col());
			log.error(msg, t2);
			throw new BuildException(msg);
		});
		
		// #pragma directive handler
		addDirectiveHandler("pragma", (Token t0, TokenInput in) -> {
			in.pushContext("handling #pragma directive", t0);
			try {
				Token t1 = in.expectNextToken("pragma");
				ArrayList<Token> ts = new ArrayList<>();
				ts.add(t1.withText("__pragma"));
				ts.add(t1.withText("("));
				in.readLine((Token t) -> ts.add(t));
				ts.add(t1.withText(")"));
				return new IteratorTokenInput(in.infoLog(), ts);
			} finally {
				in.popContext();
			}
		});
		
		// __pragma macro handler
		addMacroHandler("__pragma", (TokenInput in) -> {
			in.pushContext("handling __pragma() macro", in.peekToken());
			try {
				Token t0 = in.expectNextToken("__pragma");
				List<Token> ts = in.capture("(", ")");
				if (ts.size() > 2) {
					PragmaHandler ph = pragmas.get(ts.get(1).text());
					if (ph != null) {
						// strip parens, forward to handler
						ts.remove(0);
						ts.remove(ts.size() - 1);
						return ph.pragma(new IteratorTokenInput(in.infoLog(), ts));
					} else {
						// assume pragma is intender for compiler, re-emit
						ts.add(0, t0);
						return new IteratorTokenInput(in.infoLog(), ts);
					}
				}
				return in.asEmpty();
			} finally {
				in.popContext();
			}
		});
		
		// #extension directive handler
		addDirectiveHandler("extension", (Token t0, TokenInput in) -> {
			in.expectNextToken("extension");
			log.error("you wish!", in.peekToken());
			return in.asEmpty();
		});
		
		// #version directive handler
		addDirectiveHandler("version", (Token t0, TokenInput in) -> {
			in.pushContext("handling #version directive", t0);
			try {
				in.expectNextToken("version");
				if (!t0.fileContentStart()) throw new BuildException("'#version' must be first non-blank line in source");
				Token t2 = in.expectNextToken(Token.Type.INT_LITERAL);
				// version number
				int v = Integer.parseInt(t2.text());
				String p = "core";
				// profile name
				if (in.hasNextToken(Token.Type.IDENTIFIER)) {
					p = in.nextToken().text();
				}
				if (v != 330 || !p.equals("core")) {
					throw new BuildException("only '#version 330 core' is supported");
				}
			} finally {
				in.popContext();
			}
			return in.asEmpty();
		});
		
		// includes for builtin source files
		addPragmaHandler("i3d_include", (TokenInput in) -> {
			Token t0 = in.expectNextToken("i3d_include");
			String lstr = t0.lineText();
			String fname = lstr.substring(lstr.indexOf('<') + 1, lstr.lastIndexOf('>'));
			Reader r = BuiltinSource.open(fname + ".glsl");
			if (r == null) {
				infoLog().error("failed to include builtin source file <" + fname + ">", t0);
				return in.asEmpty();
			}
			Tokenizer tin = new Tokenizer(infoLog(), "<" + fname + ">", r);
			return prepareInput(tin);
		});
	}
	
	@Override
	public InfoLog infoLog() {
		return log;
	}
	
	private TokenInput prepareInput(TokenInput in) {
		return new GeneralDirectiveHandlerTokenInput(
			new ConditionalDirectiveHandlerTokenInput(
				new FileLineNumberTokenInput(
					new OperatorIdentifierTokenInput(Objects.requireNonNull(in))
				)
			)
		);
	}
	
	public void addInput(TokenInput in) {
		file_inputs.append(prepareInput(in));
	}
	
	public void addMacroHandler(String s, MacroHandler h) {
		macros.put(Objects.requireNonNull(s), Objects.requireNonNull(h));
	}
	
	public void addDirectiveHandler(String s, DirectiveHandler h) {
		directives.put(Objects.requireNonNull(s), Objects.requireNonNull(h));
	}
	
	public void addPragmaHandler(String s, PragmaHandler h) {
		pragmas.put(Objects.requireNonNull(s), Objects.requireNonNull(h));
	}
	
	@Override
	public Token nextToken() throws IOException {
		return input.nextToken();
	}

	@Override
	public Token peekToken() throws IOException {
		return input.peekToken();
	}
	
	protected int evalExpression(TokenInput in) throws IOException {
		Parser p = new Parser(in);
		Expression e = p.parseExpression(0);
		return e.eval();
	}
	
	protected class SubstitutionLinkageTokenInput extends AbstractTokenInput {
		
		private TokenInput in;
		private Token base;
		
		public SubstitutionLinkageTokenInput(TokenInput in_, Token base_) {
			super(log);
			in = Objects.requireNonNull(in_);
			base = base_;
		}
		
		@Override
		protected Token nextTokenImpl() throws IOException {
			TokenBuilder tb = new TokenBuilder(in.nextToken());
			tb.original(base);
			return tb.toToken();
		}
		
	}
	
	protected class TokenPastingTokenInput extends AbstractTokenInput {
		
		private TokenInput in;
		
		public TokenPastingTokenInput(TokenInput in_) {
			super(log);
			in = Objects.requireNonNull(in_);
		}
		
		@Override
		protected Token nextTokenImpl() throws IOException {
			Token t = in.nextToken();
			while (!t.isEmpty() && in.hasNextToken("##")) {
				in.pushContext("tokenpasting", t);
				try {
					in.nextToken();
					TokenBuilder tb = new TokenBuilder(t);
					Token t2 = in.unexpectNextEmptyToken();
					tb.appendText(t2.text());
					tb.classify();
					t = tb.toToken();
				} finally {
					in.popContext();
				}
			}
			return t;
		}
		
	}
	
	protected class PlainMacroHandler implements MacroHandler {

		private Token original;
		private List<Token> body;
		
		public PlainMacroHandler(Token orignal_, List<Token> body_) {
			original = orignal_;
			body = body_;
		}
		
		@Override
		public TokenInput expand(TokenInput in) throws IOException {
			Token t0 = in.expectNextToken(original.text());
			// get stream from body of macro
			TokenInput out = new IteratorTokenInput(log, body);
			// substitution linkage
			out = new SubstitutionLinkageTokenInput(out, t0);
			// tokenpaste
			out = new TokenPastingTokenInput(out);
			return out;
		}
		
	}
	
	protected class ParameterSubstitutionTokenInput implements TokenInput {
		
		List<Token> fparams;
		List<List<Token>> aparams;
		TokenInput in;
		CatTokenInput out = new CatTokenInput(log);
		private Map<String, Integer> param_indices = new HashMap<>();
		
		public ParameterSubstitutionTokenInput(TokenInput in_, List<Token> fparams_, List<List<Token>> aparams_) {
			in = Objects.requireNonNull(in_);
			fparams = Objects.requireNonNull(fparams_);
			aparams = Objects.requireNonNull(aparams_);
			for (int i = 0; i < fparams.size(); i++) {
				param_indices.put(fparams.get(i).text(), i);
			}
			out.refillHandler((CatTokenInput cts) -> {
				Token t0 = in.nextToken();
				
				if (t0.type() == Token.Type.EOF) return;
				
				// potentially start tokenpasting
				cts.append(substitute(t0, in.hasNextToken("##")));
				
				while (in.hasNextToken("##")) {
					// continue tokenpasting (note: we're not actually pasting tokens here!)
					cts.append(new IteratorTokenInput(log, in.expectNextToken("##")));
					cts.append(substitute(in.nextToken(), true));
				}
			});
		}
		
		@Override
		public InfoLog infoLog() {
			return log;
		}
		
		private TokenInput substitute(Token t, boolean tokenop) {
			if (param_indices.containsKey(t.text())) {
				// token is formal parameter
				try {
					// get actual parameter as stream
					TokenInput ts = new IteratorTokenInput(log, aparams.get(param_indices.get(t.text())));
					// substitution linkage
					ts = new SubstitutionLinkageTokenInput(ts, t);
					if (!tokenop) {
						// when not tokenpasting (or stringifying in C-land)
						// we perform expansion of macros in macro arguments
						return new MacroExpansionTokenInput(ts);
					} else {
						return ts;
					}
				} catch (IndexOutOfBoundsException e) {
					log.error("no actual param for formal param", t);
					return new EmptyTokenInput(log);
				}
			} else {
				// normal token
				return new IteratorTokenInput(log, t);
			}
		}

		@Override
		public Token peekToken() throws IOException {
			return out.peekToken();
		}

		@Override
		public Token nextToken() throws IOException {
			return out.nextToken();
		}
		
	}
	
	protected class FunctionMacroHandler implements MacroHandler {
		
		// TODO support __VA_ARGS__ in function macros
		
		private Token original;
		private List<Token> fparams;
		private List<Token> body;
		
		public FunctionMacroHandler(Token orignal_, List<Token> fparams_, List<Token> body_) {
			original = orignal_;
			fparams = fparams_;
			body = body_;
			for (Token t : fparams) {
				if (t.isText("...")) {
					log.error("__VA_ARGS__ is not supported", t);
				}
			}
		}
		
		@Override
		public TokenInput expand(TokenInput in) throws IOException {
			Token t0 = in.expectNextToken(original.text());
			
			// function macros are only expanded when followed by '('
			if (!in.hasNextToken("(")) {
				return new IteratorTokenInput(log, t0);
			}
			
			in.pushContext("invoking function macro", t0);
			
			try {
				
				// parse actual params
				in.expectNextToken("(");
				ArrayList<List<Token>> aparams = new ArrayList<>();
				while (!in.hasNextToken(")") && !in.hasNextToken(Token.Type.EOF)) {
					// parse one param
					int parens = 0;
					ArrayList<Token> ap = new ArrayList<>();
					while ((parens > 0 || !in.hasNextToken(")", ",")) && !in.hasNextToken(Token.Type.EOF)) {
						Token t = in.nextToken();
						if (t.isText("(")) parens++;
						if (t.isText(")")) parens--;
						if (parens < 0) {
							log.error("unbalanced closing parenthesis", t);
							parens = 0;
						}
						ap.add(t);
					}
					in.expectPeekToken(")", ",");
					if (in.hasNextToken(",")) in.nextToken();
					aparams.add(ap);
				}
				in.expectNextToken(")");
				
				// get stream from body of macro function
				TokenInput out = new IteratorTokenInput(log, body);
				
				// substitution linkage
				out = new SubstitutionLinkageTokenInput(out, t0);
				
				// substitute params
				out = new ParameterSubstitutionTokenInput(out, fparams, aparams);
				
				// tokenpaste
				out = new TokenPastingTokenInput(out);
				
				return out;
			
			} finally {
				in.popContext();
			}
		}
		
	}
	
	protected class EvalDefinedTokenInput extends AbstractTokenInput {
		
		private TokenInput in;
		
		public EvalDefinedTokenInput(TokenInput in_) {
			super(log);
			in = Objects.requireNonNull(in_);
		}
		
		@Override
		protected Token nextTokenImpl() throws IOException {
			if (in.hasNextToken("defined")) {
				Token t0 = in.nextToken();
				Token t1;
				if (in.hasNextToken("(")) {
					in.nextToken();
					t1 = in.expectNextToken(Token.Type.IDENTIFIER);
					in.expectNextToken(")");
				} else {
					t1 = in.expectNextToken(Token.Type.IDENTIFIER);
				}
				TokenBuilder tb = new TokenBuilder(t0);
				tb.type(Token.Type.INT_LITERAL);
				if (macros.containsKey(t1.text())) {
					tb.text("1");
				} else {
					tb.text("0");
				}
				return tb.toToken();
			}
			return in.nextToken();
		}
		
	}
	
	protected class MacroExpansionTokenInput extends AbstractTokenInput {
		
		private CatTokenInput in = new CatTokenInput(log);
		private Stack<Token> calls = new Stack<>();
		
		public MacroExpansionTokenInput(TokenInput in_) {
			super(log);
			in.append(Objects.requireNonNull(in_));
		}
		
		@Override
		protected Token nextTokenImpl() throws IOException {
			// handle macro expansion and emit replacements
			for (Token t0 = in.peekToken(); macros.containsKey(t0.text()); t0 = in.peekToken()) {
				// test for recursive expansion
				if (!t0.isText("__pragma")) {
					// pragma macros are used for includes, which need to allow recursion
					for (Token t : calls) {
						if (t.isText(t0)) return in.nextToken();
					}
				}
				// push macro, prevent recursive expansion
				// note: relies on the peeked token being retrieved directly from the replacement stream
				final Token tc = t0;
				MacroHandler mh = macros.get(tc.text());
				try {
					final TokenInput inr = mh.expand(in);
					if (inr == null) continue;
					// push macro after doing expansion to prevent out-of-order popping
					calls.push(tc);
					in.prepend(new AbstractTokenInput(log) {
						@Override
						protected Token nextTokenImpl() throws IOException {
							Token t = null;
							try {
								t = inr.nextToken();
							} catch (BuildException e) {
								log.error("macro expansion failed", tc);
							}
							if (t == null) {
								// expansion failed, return eof
								TokenBuilder tb = new TokenBuilder();
								tb.type(Token.Type.EOF);
								t = tb.toToken();
							}
							if (t.type() == Token.Type.EOF) {
								// finished expansion, pop macro
								if (calls.isEmpty() || calls.peek() != tc) {
									log.internalError("macro expansion callstack corruption", tc);
									log.detail(Arrays.toString(calls.toArray()), tc);
									throw new BuildException("macro expansion callstack corruption");
								}
								calls.pop();
							}
							return t;
						}
					});
				} catch (BuildException e) {
					log.error("failed to begin macro expansion", tc);
					return in.nextToken();
				}
			}
			return in.nextToken();
		}
		
	}
	
	protected abstract class AbstractDirectiveHandlerTokenInput extends AbstractTokenInput {

		private final CatTokenInput in;
		
		public AbstractDirectiveHandlerTokenInput(TokenInput in_) {
			super(log);
			in = new CatTokenInput(log);
			in.append(Objects.requireNonNull(in_));
		}
		
		/**
		 * Handle a directive. If not handled, stream must be left untouched.
		 * 
		 * @param t0
		 * @param in
		 * @return true if directive was handled
		 * @throws IOException
		 */
		protected abstract boolean directive(Token t0, CatTokenInput in) throws IOException;
		
		@Override
		protected Token nextTokenImpl() throws IOException {
			for (Token t0 = in.peekToken(); t0.lineContentStart() && t0.isText("#"); t0 = in.peekToken()) {
				in.nextToken();
				try {
					if (directive(t0, in)) {
						// directive was handled
						// can't skip line here because then we'd skip any replacement tokens
						// line skip is handled in the general/unknown directive handlers
					} else {
						// directive not handled, forward the '#' token
						return t0;
					}
				} catch (BuildException e) {
					log.error("handling directive failed", t0);
					in.skipLine();
				}
			}
			return in.nextToken();
		}
		
	}
	
	protected class ConditionalDirectiveHandlerTokenInput extends AbstractDirectiveHandlerTokenInput {

		private Map<String, DirectiveHandler> cond_directives = new HashMap<>();
		
		// 0 = not yet enabled, 1 = enabled, 2+ = previously enabled
		private Stack<Integer> conds = new Stack<>();
		
		public ConditionalDirectiveHandlerTokenInput(TokenInput in_) {
			super(in_);
			
			// #ifdef directive handler
			cond_directives.put("ifdef", (Token t0, TokenInput in) -> {
				in.expectNextToken("ifdef");
				if (!enabled()) {
					conds.add(2);
					return in.asEmpty();
				}
				Token t2 = in.expectNextToken(Token.Type.IDENTIFIER);
				if (macros.containsKey(t2.text())) {
					conds.add(1);
				} else {
					conds.add(0);
				}
				return in.asEmpty();
			});
			
			// #ifndef directive handler
			cond_directives.put("ifndef", (Token t0, TokenInput in) -> {
				in.expectNextToken("ifndef");
				if (!enabled()) {
					conds.add(2);
					return in.asEmpty();
				}
				Token t2 = in.expectNextToken(Token.Type.IDENTIFIER);
				if (macros.containsKey(t2.text())) {
					conds.add(0);
				} else {
					conds.add(1);
				}
				return in.asEmpty();
			});
			
			// #if directive handler
			cond_directives.put("if", (Token t0, TokenInput in) -> {
				in.expectNextToken("if");
				if (!enabled()) {
					conds.add(2);
					return in.asEmpty();
				}
				testCondition(in);
				return in.asEmpty();
			});
			
			// #elif directive handler
			cond_directives.put("elif", (Token t0, TokenInput in) -> {
				Token t1 = in.expectNextToken("elif");
				if (conds.isEmpty()) {
					log.error("unmatched #elif", t1);
					return in.asEmpty();
				}
				// remove condition state for current if-block
				int c = conds.pop();
				if (!enabled()) {
					conds.add(2);
					return in.asEmpty();
				}
				// can we try enabling current if-block?
				if (c == 0) {
					testCondition(in);
				} else {
					// 1 -> 2, 2 -> 2
					conds.add(2);
				}
				return in.asEmpty();
			});
			
			// #else directive handler
			cond_directives.put("else", (Token t0, TokenInput in) -> {
				Token t1 =in.expectNextToken("else");
				if (conds.isEmpty()) {
					log.error("unmatched #else", t1);
					return in.asEmpty();
				}
				// remove condition state for current if-block
				int c = conds.pop();
				if (!enabled()) {
					conds.add(2);
					return in.asEmpty();
				}
				// can we enable current if-block?
				if (c == 0) {
					conds.add(1);
				} else {
					// 1 -> 2, 2 -> 2
					conds.add(2);
				}
				return in.asEmpty();
			});
			
			// #endif directive handler
			cond_directives.put("endif", (Token t0, TokenInput in) -> {
				Token t1 = in.expectNextToken("endif");
				if (conds.isEmpty()) {
					log.error("unmatched #endif", t1);
					return in.asEmpty();
				}
				// terminate current #if
				conds.pop();
				return in.asEmpty();
			});
		}
		
		private void testCondition(TokenInput in) throws IOException {
			TokenInput in2 = new MacroExpansionTokenInput(new EvalDefinedTokenInput(in.readLineAsStream()));
			if (evalExpression(in2) != 0) {
				conds.push(1);
			} else {
				conds.push(0);
			}
			in2.expectPeekToken(Token.Type.EOF);
		}
		
		private boolean enabled() {
			if (conds.empty()) return true;
			return conds.peek() == 1;
		}
		
		@Override
		protected boolean directive(Token t0, CatTokenInput in) throws IOException {
			DirectiveHandler dh = cond_directives.get(in.peekToken().text());
			if (dh == null) return false;
			// conditional directive handlers dont emit replacements, so we discard
			dh.directive(t0, in);
			return true;
		}
		
		@Override
		protected Token nextTokenImpl() throws IOException {
			Token t = null;
			do {
				t = super.nextTokenImpl();
				if (t.type() == Token.Type.EOF && !conds.isEmpty()) {
					log.error("unclosed #if[def|ndef] at end-of-file", t);
					return t;
				}
			} while(!enabled());
			return t;
		}
	}

	protected class FileLineNumberTokenInput extends AbstractTokenInput {
		
		private TokenInput in;
		private String file = "?";
		private int line_offset = 0;
		
		public FileLineNumberTokenInput(TokenInput in_) {
			super(log);
			in = Objects.requireNonNull(in_);
		}
		
		@Override
		protected Token nextTokenImpl() throws IOException {
			TokenBuilder tb = new TokenBuilder(in.nextToken());
			// reset file and line offset at start of file
			if (tb.fileContentStart()) {
				line_offset = 0;
				has_next_line_offset = false;
				file = tb.fileName();
			}
			// recalculate line offset at newline if needed
			if (tb.lineContentStart() && has_next_line_offset) {
				// the next offset is relative to the reported line of the #line tokens
				// (which includes our current offset), so we need to add the new offset.
				// #line needs to specify the offset, because there can be blank lines between there and here
				line_offset += next_line_offset;
				has_next_line_offset = false;
			}
			// update filename at newline if needed
			if (tb.lineContentStart() && next_file > 0) {
				file = "" + next_file;
				next_file = -1;
			}
			// renumber by offset
			tb.line(tb.line() + line_offset);
			// set filename
			tb.fileName(file);
			return tb.toToken();
		}
		
	}
	
	protected class GeneralDirectiveHandlerTokenInput extends AbstractDirectiveHandlerTokenInput {

		public GeneralDirectiveHandlerTokenInput(TokenInput in_) {
			super(in_);
		}

		@Override
		protected boolean directive(Token t0, CatTokenInput in) throws IOException {
			DirectiveHandler dh = directives.get(in.peekToken().text());
			if (dh == null) return false;
			TokenInput ts = dh.directive(t0, in);
			in.skipLine();
			in.prepend(ts);
			return true;
		}
		
	}
	
	protected class UnknownDirectiveHandlerTokenInput extends AbstractDirectiveHandlerTokenInput {
		
		public UnknownDirectiveHandlerTokenInput(TokenInput in_) {
			super(in_);
		}
		
		@Override
		protected boolean directive(Token t0, CatTokenInput in) throws IOException {
			log.error("unknown directive", in.peekToken());
			in.skipLine();
			return true;
		}
		
	}
	
}
