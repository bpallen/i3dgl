package initial3d.detail.glsl.preprocessor;

public interface Expression {

	int eval();
	
	public static class IntExpression implements Expression {

		int x;
		
		public IntExpression(int x_) {
			x = x_;
		}
		
		@Override
		public int eval() {
			return x;
		}
		
	}
	
	public static interface UnaryOperator {
		public int eval(int l);
	}
	
	public static interface BinaryOperator {
		public int eval(int l, int r);
	}
	
	public static class UnaryExpression implements Expression {

		UnaryOperator op;
		Expression l;
		
		public UnaryExpression(UnaryOperator op_, Expression l_) {
			op = op_;
			l = l_;
		}
		
		@Override
		public int eval() {
			return op.eval(l.eval());
		}
		
	}
	
	public static class BinaryExpression implements Expression {

		BinaryOperator op;
		Expression l, r;
		
		public BinaryExpression(BinaryOperator op_, Expression l_, Expression r_) {
			op = op_;
			l = l_;
			r = r_;
		}
		
		@Override
		public int eval() {
			return op.eval(l.eval(), r.eval());
		}
		
	}
	
}
