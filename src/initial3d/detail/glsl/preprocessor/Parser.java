package initial3d.detail.glsl.preprocessor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenInput;

public class Parser {

	private static interface PrefixParselet {
		Expression parse(Parser p, Token t) throws IOException;
	}
	
	private static interface InfixParselet {
		Expression parse(Parser p, Expression l, Token t) throws IOException;
		int precedence();
	}
	
	private static class PrefixOperatorParselet implements PrefixParselet {

		private Expression.UnaryOperator op;
		
		public PrefixOperatorParselet(Expression.UnaryOperator op_) {
			op = op_;
		}
		
		@Override
		public Expression parse(Parser p, Token t) throws IOException {
			return new Expression.UnaryExpression(op, p.parseExpression(Precedence.PREFIX - 1));
		}
		
	}
	
	private static class BinaryOperatorParselet implements InfixParselet {

		private Expression.BinaryOperator op;
		int prec;
		
		public BinaryOperatorParselet(Expression.BinaryOperator op_, int prec_) {
			op = op_;
			prec = prec_;
		}
		
		@Override
		public Expression parse(Parser p, Expression l, Token t) throws IOException {
			Expression r = p.parseExpression(prec);
			return new Expression.BinaryExpression(op, l, r);
		}

		@Override
		public int precedence() {
			return prec;
		}
		
	}
	
	private static class ParenthesesParselet implements PrefixParselet {

		@Override
		public Expression parse(Parser p, Token t) throws IOException {
			Expression e = p.parseExpression(0);
			p.input().expectNextToken(")");
			return e;
		}
		
	}
	
	private static class SelectionExpression implements Expression {
		
		private Expression cond, true_arm, false_arm;
		
		public SelectionExpression(Expression cond_, Expression true_arm_, Expression false_arm_) {
			cond = cond_;
			true_arm = true_arm_;
			false_arm = false_arm_;
		}
		
		@Override
		public int eval() {
			return (cond.eval() != 0) ? true_arm.eval() : false_arm.eval();
		}
		
	}
	
	private static class SelectionParselet implements InfixParselet {

		@Override
		public Expression parse(Parser p, Expression l, Token t) throws IOException {
			Expression true_arm = p.parseExpression(Precedence.SELECTION - 1);
			p.input().expectNextToken(":");
			Expression false_arm = p.parseExpression(Precedence.SELECTION - 1);
			return new SelectionExpression(l, true_arm, false_arm);
		}

		@Override
		public int precedence() {
			return Precedence.SELECTION;
		}
		
	}
	
	private TokenInput input;
	private Map<String, PrefixParselet> prefix_operators = new HashMap<>();
	private Map<String, InfixParselet> infix_operators = new HashMap<>();
	
	public Parser(TokenInput in_) {
		input = Objects.requireNonNull(in_);
		prefixOperator("(", new ParenthesesParselet());
		prefixOperator("+", new PrefixOperatorParselet((int x) -> { return x; }));
		prefixOperator("-", new PrefixOperatorParselet((int x) -> { return -x; }));
		prefixOperator("~", new PrefixOperatorParselet((int x) -> { return ~x; }));
		prefixOperator("!", new PrefixOperatorParselet((int x) -> { return x == 0 ? 0 : 1; }));
		infixOperator("*", new BinaryOperatorParselet((int l, int r) -> { return l * r; }, Precedence.MULTIPLICATIVE));
		infixOperator("/", new BinaryOperatorParselet((int l, int r) -> { return l / r; }, Precedence.MULTIPLICATIVE));
		infixOperator("%", new BinaryOperatorParselet((int l, int r) -> { return l % r; }, Precedence.MULTIPLICATIVE));
		infixOperator("+", new BinaryOperatorParselet((int l, int r) -> { return l + r; }, Precedence.ADDITIVE));
		infixOperator("-", new BinaryOperatorParselet((int l, int r) -> { return l - r; }, Precedence.ADDITIVE));
		infixOperator("<<", new BinaryOperatorParselet((int l, int r) -> { return l << r; }, Precedence.BITSHIFT));
		infixOperator(">>", new BinaryOperatorParselet((int l, int r) -> { return l >> r; }, Precedence.BITSHIFT));
		infixOperator("<", new BinaryOperatorParselet((int l, int r) -> { return l < r ? 1 : 0; }, Precedence.RELATIONAL));
		infixOperator(">", new BinaryOperatorParselet((int l, int r) -> { return l > r ? 1 : 0; }, Precedence.RELATIONAL));
		infixOperator("<=", new BinaryOperatorParselet((int l, int r) -> { return l <= r ? 1 : 0; }, Precedence.RELATIONAL));
		infixOperator(">=", new BinaryOperatorParselet((int l, int r) -> { return l >= r ? 1 : 0; }, Precedence.RELATIONAL));
		infixOperator("==", new BinaryOperatorParselet((int l, int r) -> { return l == r ? 1 : 0; }, Precedence.EQUALITY));
		infixOperator("!=", new BinaryOperatorParselet((int l, int r) -> { return l != r ? 1 : 0; }, Precedence.EQUALITY));
		infixOperator("&", new BinaryOperatorParselet((int l, int r) -> { return l & r; }, Precedence.BITWISE_AND));
		infixOperator("^", new BinaryOperatorParselet((int l, int r) -> { return l ^ r; }, Precedence.BITWISE_XOR));
		infixOperator("|", new BinaryOperatorParselet((int l, int r) -> { return l | r; }, Precedence.BITWISE_OR));
		infixOperator("&&", new BinaryOperatorParselet((int l, int r) -> { return (l != 0) && (r != 0) ? 1 : 0; }, Precedence.LOGICAL_AND));
		infixOperator("^^", new BinaryOperatorParselet((int l, int r) -> { return (l != 0) != (r != 0) ? 1 : 0; }, Precedence.LOGICAL_XOR));
		infixOperator("||", new BinaryOperatorParselet((int l, int r) -> { return (l != 0) || (r != 0) ? 1 : 0; }, Precedence.LOGICAL_OR));
		infixOperator("?", new SelectionParselet());
	}
	
	private void prefixOperator(String op, PrefixParselet p) {
		prefix_operators.put(op, p);
	}
	
	private void infixOperator(String op, InfixParselet p) {
		infix_operators.put(op, p);
	}
	
	private int precedence(Token t) {
		InfixParselet infix = infix_operators.get(t.text());
		if (infix != null) return infix.precedence();
		return 0;
	}
	
	public TokenInput input() {
		return input;
	}
	
	public Expression parseExpression(int prec) throws IOException {
		Token t = input.expectNextToken(Token.Type.OPERATOR, Token.Type.INT_LITERAL);
		Expression l = null;
		if (t.type() == Token.Type.INT_LITERAL) {
			l = new Expression.IntExpression(Integer.parseInt(t.text()));
		} else {
			PrefixParselet prefix = prefix_operators.get(t.text());
			if (prefix == null) input.expectationFailure(t);
			l = prefix.parse(this, t);
		}
		while (precedence(t = input.peekToken()) > prec) {
			InfixParselet infix = infix_operators.get(t.text());
			input.nextToken();
			l = infix.parse(this, l, t);
		}
		return l;
	}
	
}
