package initial3d.detail.glsl.preprocessor;

import java.io.IOException;

import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenInput;

public interface DirectiveHandler {

	public TokenInput directive(Token t0, TokenInput in) throws IOException;
	
}
