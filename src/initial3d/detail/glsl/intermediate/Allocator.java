package initial3d.detail.glsl.intermediate;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.Layout;
import initial3d.detail.glsl.lang.names.TypeName;

public interface Allocator {
	
	public InfoLog infoLog();
	
	/**
	 * specify alignment for next (and only next) primitive.
	 * 
	 * @param a
	 */
	public void align(int a);
	
	public PrimitiveLocation allocPrimitive(BuiltinType type, Layout layout, Location parent);
	
	public Location alloc(TypeName type, Layout layout, Location parent);
	
	default Location allocVar(TypeName type, Layout layout) {
		return alloc(type, layout, null);
	}
	
	public int newID();
	
	public static boolean isPOT(int x) {
		return x == 0 || (x & (x - 1)) != 0;
	}
	
	public static int makeAligned(int off, int al) {
		if (isPOT(al)) throw new RuntimeException("bad alignment " + al);
		return (off + al - 1) & ~(al - 1);
	}
	
}
