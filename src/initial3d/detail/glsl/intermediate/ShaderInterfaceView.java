package initial3d.detail.glsl.intermediate;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;

import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.names.VariableName;

public class ShaderInterfaceView {

	// https://www.khronos.org/opengl/wiki/Program_Introspection
	// > So while within a particular shader stage only one instance name applies,
	// > at the program level multiple instance names could apply to the same block.
	// so, one interface view per interface per stage.
	// blocks cannot have different instance names in different translation units in one stage.
	
	private static class SpecialLocationPair {
		public final Location global;
		public final Location special;
		
		public SpecialLocationPair(Location global_, Location special_) {
			global = global_;
			special = special_;
		}
	}
	
	private ShaderInterface iface;
	
	// locations by variable
	private HashMap<Variable, Location> vars = new HashMap<>();
	
	// api names
	private HashMap<Integer, String> id2apiname = new HashMap<>();
	private HashMap<String, Location> apinames = new HashMap<>();
	
	// special variables
	private ArrayList<SpecialLocationPair> specials = new ArrayList<>();
	
	public ShaderInterfaceView(ShaderInterface iface_) {
		iface = iface_;
	}
	
	public ShaderInterface iface() {
		return iface;
	}
	
	public LocationNameMap apiNames() {
		return new LocationNameMap() {
			@Override
			public String get(int id) {
				return id2apiname.get(id);
			}
			@Override
			public void put(int id, String name) {
				// silently drop bad ids
				if (id < 0) return;
				id2apiname.put(id, name);
			}
			@Override
			public int rootLocation(String name) {
				Location l = apinames.get(name);
				if (l == null) return -1;
				return l.id();
			}
		};
	}
	
	public Location variable(Translator trans, VariableName var) {
		// try fetch by variable
		Location l = vars.get(var.definition());
		if (l != null) return l;
		// get the variable from the interface, may allocate (or not if already linked)
		l = iface.variable(var.definition());
		l.initMemberNames(apiNames());
		if (var.plainText().startsWith("__blockinst_")) {
			// 'unnamed' block instance, any members should be accessed directly
			// TODO maybe map such block instance locations to a special name to make this explicit?
			for (Location lc : l.children()) {
				apinames.put(id2apiname.get(lc.id()), lc);
			}
		} else if (var.definition().layout() != null && var.definition().layout().special){
			// special variable; no accessible api name
			// replace with ordinary global and pair with actual special location
			// this will get copied into / out of the global before / after main()
			Location l2 = trans.global(var.type());
			specials.add(new SpecialLocationPair(l2, l));
			l = l2;
		} else {
			// access any members behind link name
			id2apiname.put(l.id(), iface.linkNames().get(l.id()));
			apinames.put(iface.linkNames().get(l.id()), l);
		}
		// record variable
		vars.put(var.definition(), l);
		return l;
	}
	
	public Location apiLocation(String apiname) {
		return iface.apiLocation(apiNames(), apiname);
	}

	public Location apiLocation(int apiloc) {
		return iface.apiLocation(apiloc);
	}
	
	public void copySpecialInputs(Translator trans) {
		for (SpecialLocationPair p : specials) {
			trans.copy(p.global, p.special);
		}
	}
	
	public void copySpecialOutputs(Translator trans) {
		for (SpecialLocationPair p : specials) {
			trans.copy(p.special, p.global);
		}
	}
	
	public void printSummary(PrintWriter out) {
		iface.printSummary(out, apiNames());
	}
	
	public String printSummary() {
		StringWriter sw = new StringWriter();
		printSummary(new PrintWriter(sw));
		return sw.toString();
	}
}
