package initial3d.detail.glsl.intermediate;

import java.util.EnumSet;
import java.util.HashMap;

import initial3d.GLEnum;
import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.Scope;

public abstract class ProgramBuilder {

	private InfoLog infolog;
	private HashMap<GLEnum, Environment> envs = new HashMap<>();
	private boolean log_ifaces = true;
	private UniformInterface uniforms = null;
	
	public ProgramBuilder(InfoLog infolog_) {
		infolog = infolog_;
	}
	
	public InfoLog infoLog() {
		return infolog;
	}
	
	protected abstract Environment createEnv(GLEnum stype);
	
	public void addShader(GLEnum stype, Scope globalscope) {
		Environment env = envs.get(stype);
		if (env == null) {
			env = createEnv(stype);
			envs.put(stype, env);
		}
		env.translator().addGlobalScope(globalscope);
	}
	
	public Environment environment(GLEnum stype) {
		return envs.get(stype);
	}
	
	public UniformInterface uniforms() {
		return uniforms;
	}
	
	public void build() {
		Environment vert = envs.get(GLEnum.GL_VERTEX_SHADER);
		Environment geom = envs.get(GLEnum.GL_GEOMETRY_SHADER);
		Environment frag = envs.get(GLEnum.GL_FRAGMENT_SHADER);
		if (vert == null) throw new BuildException("no vertex shader");
		// init interfaces for present shader stages
		uniforms = new UniformInterface(infolog);
		ShaderInterface attrs = new ShaderInterface(infolog, "attribute", EnumSet.noneOf(PrimitiveLocation.Flags.class));
		ShaderInterface vert_out = new ShaderInterface(infolog, "vertex", EnumSet.noneOf(PrimitiveLocation.Flags.class));
		ShaderInterface geom_out = null;
		ShaderInterface frag_out = null;
		ShaderInterface out = vert_out;
		vert.initInterfaces(uniforms, attrs, vert_out);
		if (geom != null) {
			geom_out = new ShaderInterface(infolog, "geometry", EnumSet.noneOf(PrimitiveLocation.Flags.class));
			geom.initInterfaces(uniforms, out, geom_out);
			out = geom_out;
		}
		if (frag != null) {
			frag_out = new ShaderInterface(infolog, "fragment", EnumSet.noneOf(PrimitiveLocation.Flags.class));
			frag.initInterfaces(uniforms, out, frag_out);
			out = frag_out;
		}
		// translate shaders
		vert.translator().translateMain();
		if (geom != null) geom.translator().translateMain();
		if (frag != null) frag.translator().translateMain();
		if (infolog.errorCount() > 0) throw new BuildException("errors encountered, stopping");
		// assign interface addresses
		uniforms.assignAddresses();
		attrs.assignAddresses();
		vert_out.assignAddresses();
		if (geom_out != null) geom_out.assignAddresses();
		if (frag_out != null) frag_out.assignAddresses();
		// assemble binary
		vert.assembler().assemble(this, vert.translator());
		if (geom != null) geom.assembler().assemble(this, geom.translator());
		if (frag != null) frag.assembler().assemble(this, frag.translator());
		if (infolog.errorCount() > 0) throw new BuildException("errors encountered, stopping");
		if (log_ifaces) {
			infolog.info(uniforms.printSummary());
			infolog.info(vert.inputs().printSummary());
			infolog.info(vert.outputs().printSummary());
			if (geom != null) infolog.info(geom.outputs().printSummary());
			if (frag != null) infolog.info(frag.outputs().printSummary());
		}
		infolog.info(String.format("Shader program linked successfully (%d warnings)", infolog.warningCount()));
	}
	
}
