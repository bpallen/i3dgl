package initial3d.detail.glsl.intermediate;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import initial3d.detail.glsl.intermediate.locations.OffsetPrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.TokenInput;

public interface Location {

	public TypeName type();
	
	public boolean constexpr();
	
	public int size();
	
	public int align();
	
	/**
	 * @return Address relative to interface base address. Only for required interface locations.
	 */
	public int address();
	
	/**
	 * @return Per-allocator unique id for this location object. Only required for interface locations.
	 */
	public int id();
	
	/**
	 * Ensure this location and all sublocations are contiguous and randomly accessible.
	 * Must prevent these locations from being optimized out.
	 */
	public void enableRandomAccess();
	
	public Location parent();
	
	default Collection<Location> children() {
		return Collections.emptyList();
	}
	
	default void initMemberNames(LocationNameMap locnames) {
		
	}
	
	/**
	 * Get the full api name of a child of this location with the specified api name segment.
	 * 
	 * @param cseg
	 * @return
	 */
	public String apiChildName(LocationNameMap locnames, String cseg);
	
	/**
	 * @return The full api name of this location (including all parent segments)
	 */
	default String apiFullName(LocationNameMap locnames) {
		Location p = parent();
		String s = locnames.get(id());
		if (s == null) return null;
		if (p != null) {
			return p.apiChildName(locnames, s);
		} else {
			return s;
		}
	}
	
	public Location findApiLocation(LocationNameMap locnames, TokenInput tin) throws IOException;
	
	/**
	 * Copy of this location offset by <code>offset</code> elements of <code>array</code>.
	 * See {@link OffsetPrimitiveLocation}.
	 * 
	 * @param array
	 * @param offset
	 * @return
	 */
	public Location withOffset(Location array, PrimitiveLocation offset);
	
	/**
	 * Copy of <code>base</code> offset by <code>offset</code> elements of this,
	 * assuming this is some sort of array.
	 * 
	 * @param base
	 * @param offset
	 * @return
	 */
	default PrimitiveLocation applyOffset(PrimitiveLocation base, PrimitiveLocation offset) {
		throw new UnsupportedOperationException("only array-ish locations can apply offsets");
	}
	
	/**
	 * Translate array subscript, assuming this is some sort of array.
	 * 
	 * @param trans
	 * @param i
	 * @return
	 */
	default Location translateSubscript(Translator trans, PrimitiveLocation index) {
		throw new UnsupportedOperationException("only array-ish locations can translate array subscripts");
	}
	
	default boolean replaceSubLocation(Location l0, Location l1) {
		return false;
	}
	
}
