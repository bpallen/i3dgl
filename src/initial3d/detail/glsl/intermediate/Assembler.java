package initial3d.detail.glsl.intermediate;

import java.util.EnumSet;

import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.expressions.Instance;

public interface Assembler {

	public void assemble(ProgramBuilder builder, Translator trans);
	
	public Environment environment();
	
	public LocationAssembler localLoc(BuiltinType type, int regindex, boolean random_access);
	
	public LocationAssembler ifaceLoc(BuiltinType type, ShaderInterface iface, int ifaceindex, int address, EnumSet<PrimitiveLocation.Flags> flags);
	
	public LocationAssembler specialLoc(BuiltinType type, String linkname, int primindex);
	
	public LocationAssembler constantLoc(BuiltinType type, Instance x);
	
}
