package initial3d.detail.glsl.intermediate;

import initial3d.detail.glsl.BuildException;

public class TranslateException extends BuildException {

	public TranslateException(String msg) {
		super(msg);
	}
	
}
