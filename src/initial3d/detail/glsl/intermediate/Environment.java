package initial3d.detail.glsl.intermediate;

import initial3d.GLEnum;
import initial3d.detail.glsl.InfoLog;

public interface Environment {

	public InfoLog infoLog();
	
	public GLEnum shaderType();
	
	public void initInterfaces(ShaderInterface uniforms, ShaderInterface inputs, ShaderInterface outputs);
	
	public ShaderInterfaceView inputs();
	
	public ShaderInterfaceView outputs();
	
	public ShaderInterfaceView uniforms();
	
	public Translator translator();
	
	public Assembler assembler();
	
}
