package initial3d.detail.glsl.intermediate.locations;

import java.util.EnumSet;

import initial3d.detail.glsl.intermediate.Assembler;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.LocationAssembler;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.names.TypeName;

public class InterfaceOffsetPrimitiveLocation implements PrimitiveLocation {

	private InterfacePrimitiveLocation base;
	private PrimitiveLocation offset;
	private EnumSet<Flags> flags = EnumSet.noneOf(Flags.class);
	
	public InterfaceOffsetPrimitiveLocation(InterfacePrimitiveLocation base_, PrimitiveLocation offset_) {
		base = base_;
		offset = offset_;
	}
	
	@Override
	public TypeName type() {
		return base.type();
	}

	@Override
	public boolean constexpr() {
		return false;
	}

	@Override
	public int size() {
		return base.size();
	}

	@Override
	public int align() {
		return base.align();
	}

	@Override
	public void enableRandomAccess() {
		// nothing to do
	}

	@Override
	public BuiltinType primitiveType() {
		return base.primitiveType();
	}

	@Override
	public void addRead(MacroOp op) {
		offset.addRead(op);
	}

	@Override
	public void addWrite(MacroOp op) {
		offset.addRead(op);
	}

	@Override
	public int readCount() {
		// prevent any optimizations based on read count
		return 9001;
	}

	@Override
	public int writeCount() {
		// prevent any optimizations based on write count
		return 9001;
	}
	
	@Override
	public String toString() {
		return String.format("<%s:%d+%d>%s@%d %s", base.iface().iname(), base.ifaceIndex(), offset, base.type().mangledText(), base.address(), flags);
	}

	@Override
	public Location parent() {
		return null;
	}
	
	@Override
	public LocationAssembler assemble(Assembler asm) {
		LocationAssembler l = base.assemble(asm);
		l.ifaceOffset(offset.assemble(asm));
		return l;
	}
	
	@Override
	public EnumSet<Flags> flags() {
		return flags;
	}
	
}
