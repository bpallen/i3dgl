package initial3d.detail.glsl.intermediate.locations;

import java.util.EnumSet;

import initial3d.detail.glsl.intermediate.Assembler;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.LocationAssembler;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.names.TypeName;

public class SpecialPrimitiveLocation implements PrimitiveLocation {

	private BuiltinType type;
	private String linkname;
	private int locid;
	private int primid;
	private EnumSet<Flags> flags = EnumSet.noneOf(Flags.class);
	
	public SpecialPrimitiveLocation(BuiltinType type_, String linkname_, int locid_, int primid_) {
		type = type_;
		linkname = linkname_;
		locid = locid_;
		primid = primid_;
	}
	
	@Override
	public TypeName type() {
		return type.name();
	}
	
	@Override
	public boolean constexpr() {
		return false;
	}
	
	@Override
	public int id() {
		return locid;
	}
	
	public String specialName() {
		return linkname;
	}
	
	public int specialPrim() {
		return primid;
	}
	
	@Override
	public int size() {
		return type.allocSize(null);
	}

	@Override
	public int align() {
		return type.allocAlign(null);
	}

	@Override
	public void enableRandomAccess() {
		throw new UnsupportedOperationException("special primitives dont support random access");
	}

	@Override
	public Location parent() {
		throw new UnsupportedOperationException("special primitives aren't parented atm");
	}

	@Override
	public BuiltinType primitiveType() {
		return type;
	}

	@Override
	public String toString() {
		return String.format("<special:%s:%d>%s", linkname, primid, type.name().mangledText());
	}
	
	@Override
	public LocationAssembler assemble(Assembler asm) {
		return asm.specialLoc(type, linkname, primid);
	}
	
	@Override
	public EnumSet<Flags> flags() {
		return flags;
	}
	
}
