package initial3d.detail.glsl.intermediate.locations;

import java.util.EnumSet;

import initial3d.detail.glsl.intermediate.Assembler;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.LocationAssembler;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.names.TypeName;

public class ConstantPrimitiveLocation implements PrimitiveLocation {

	private Instance value;
	private BuiltinType type;
	private boolean random_access = false;
	private EnumSet<Flags> flags = EnumSet.of(Flags.GROUP_ONCE);
	
	public ConstantPrimitiveLocation(Instance value_) {
		value = value_;
		type = (BuiltinType) value.type().definition();
	}
	
	@Override
	public TypeName type() {
		return type.name();
	}
	
	@Override
	public boolean constexpr() {
		return true;
	}

	@Override
	public BuiltinType primitiveType() {
		return type;
	}
	
	@Override
	public int readCount() {
		// could implement this, but clearing the count
		// between updates could be hard.
		throw new AssertionError("IMPLEMENT ME?");
	}
	
	@Override
	public int writeCount() {
		// constants are never written to
		return 0;
	}
	
	public Instance value() {
		return value;
	}
	
	@Override
	public String toString() {
		return value.toString();
	}

	@Override
	public int size() {
		return type.allocSize(null);
	}

	@Override
	public int align() {
		return type.allocSize(null);
	}

	@Override
	public void enableRandomAccess() {
		random_access = true;
	}

	@Override
	public Location parent() {
		throw new UnsupportedOperationException("constant primitive locations aren't parented atm");
	}

	@Override
	public LocationAssembler assemble(Assembler asm) {
		return asm.constantLoc(type, value);
	}
	
	@Override
	public EnumSet<Flags> flags() {
		return flags;
	}
	
}
