package initial3d.detail.glsl.intermediate.locations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import initial3d.detail.glsl.intermediate.Allocator;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.LocationNameMap;
import initial3d.detail.glsl.intermediate.TranslateException;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Layout;
import initial3d.detail.glsl.lang.expressions.ArrayInstance;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenInput;

public class ArrayLocation implements Location {

	private ArrayTypeName type;
	private Layout layout;
	private Location parent;
	private ArrayList<Location> members = new ArrayList<>();
	private boolean contiguous = true;
	private boolean constexpr = false;
	private int align = 1, stride = 0;
	private boolean random_access = false;
	private int locid = -1;
	
	public ArrayLocation(ArrayTypeName type_, Allocator alloc_, Layout layout_, Location parent_) {
		type = type_;
		layout = layout_;
		parent = parent_;
		locid = alloc_.newID();
		align = type.definition().allocAlign(layout);
		// ensure array align
		alloc_.align(align);
		if (type.size() < 0) throw new IllegalArgumentException("bad array size");
		for (int i = 0; i < type.size(); i++) {
			Location ml = alloc_.alloc(type.innerType(), layout, this);
			members.add(ml);
			// update array stride
			stride = Allocator.makeAligned(Math.max(align, ml.size()), align);
			// ensure allocator pads to full alignment after each element (including the last)
			alloc_.align(align);
		}
	}
	
	public ArrayLocation(Translator trans, ArrayInstance x) {
		type = x.type();
		constexpr = true;
		for (int i = 0; i < type.size(); i++) {
			members.add(x.member(i).translate(trans));
		}
	}
	
	public ArrayLocation(ArrayLocation base_, Location array, PrimitiveLocation offset) {
		type = base_.type;
		constexpr = false;
		for (int i = 0; i < type.size(); i++) {
			members.add(base_.member(i).withOffset(array, offset));
		}
	}
	
	@Override
	public ArrayTypeName type() {
		return type;
	}
	
	@Override
	public boolean constexpr() {
		return constexpr;
	}

	public Location member(int i) {
		return members.get(i);
	}
	
	public Location member(PrimitiveLocation i) {
		return members.get(0).withOffset(this, i);
	}
	
	@Override
	public boolean replaceSubLocation(Location l0, Location l1) {
		if (random_access) throw new TranslateException("array is randomly accessible");
		boolean r = false;
		for (int i = 0; i < members.size(); i++) {
			if (members.get(i) == l0) {
				members.set(i, l1);
				r = true;
			} else {
				r |= members.get(i).replaceSubLocation(l0, l1);
			}
		}
		// if sublocation replacement happened, the layout should not be treated as contiguous
		contiguous &= !r;
		return r;
	}

	@Override
	public int size() {
		return stride * type.size();
	}

	@Override
	public int align() {
		return align;
	}

	public int stride() {
		return stride;
	}
	
	@Override
	public Location withOffset(Location array, PrimitiveLocation offset) {
		return new ArrayLocation(this, array, offset);
	}
	
	@Override
	public PrimitiveLocation applyOffset(PrimitiveLocation base, PrimitiveLocation offset) {
		return new OffsetPrimitiveLocation(base, this, offset);
	}

	@Override
	public Location translateSubscript(Translator trans, PrimitiveLocation index) {
		if (index.constexpr()) {
			ConstantPrimitiveLocation lx = (ConstantPrimitiveLocation) index;
			int i = (Integer) lx.value().javaEquivalent();
			return member(i);
		} else {
			enableRandomAccess();
			return member(index);
		}
	}
	
	@Override
	public void enableRandomAccess() {
		if (!contiguous) throw new TranslateException("array is not contiguous");
		for (Location l : members) {
			l.enableRandomAccess();
		}
		random_access = true;
	}

	@Override
	public int address() {
		// TODO what if no members?
		return member(0).address();
	}

	@Override
	public int id() {
		if (locid < 0) throw new IllegalStateException("no id");
		return locid;
	}

	@Override
	public Location parent() {
		return parent;
	}

	@Override
	public Collection<Location> children() {
		return Collections.unmodifiableList(members);
	}
	
	@Override
	public String apiChildName(LocationNameMap locnames, String cseg) {
		return apiFullName(locnames) + "[" + cseg + "]";
	}

	@Override
	public Location findApiLocation(LocationNameMap locnames, TokenInput tin) throws IOException {
		if (tin.hasNextToken(Token.Type.EOF)) return this;
		tin.expectNextToken("[");
		Token t = tin.expectNextToken(Token.Type.INT_LITERAL);
		tin.expectNextToken("]");
		return member(Integer.parseInt(t.text())).findApiLocation(locnames, tin);
	}
	
	@Override
	public void initMemberNames(LocationNameMap locnames) {
		for (int i = 0; i < members.size(); i++) {
			locnames.put(members.get(i).id(), "" + i);
			members.get(i).initMemberNames(locnames);
		}
	}
	
	@Override
	public String toString() {
		return type.text() + " (size=" + type.size() + ")";
	}
}
