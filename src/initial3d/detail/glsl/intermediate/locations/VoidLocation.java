package initial3d.detail.glsl.intermediate.locations;

import java.util.EnumSet;

import initial3d.detail.glsl.intermediate.Assembler;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.LocationAssembler;
import initial3d.detail.glsl.intermediate.LocationNameMap;
import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.names.TypeName;

public class VoidLocation implements PrimitiveLocation {

	private static final BuiltinType void_type = (BuiltinType) BuiltinScope.instance.declaredType("void", null).definition();
	
	private EnumSet<Flags> flags = EnumSet.noneOf(Flags.class);
	
	@Override
	public TypeName type() {
		return void_type.name();
	}
	
	@Override
	public boolean constexpr() {
		return true;
	}

	@Override
	public BuiltinType primitiveType() {
		return void_type;
	}

	@Override
	public String toString() {
		return "void";
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public int align() {
		return 1;
	}

	@Override
	public void enableRandomAccess() {
		
	}

	@Override
	public Location parent() {
		return null;
	}

	@Override
	public String apiChildName(LocationNameMap locnames, String cseg) {
		return null;
	}
	
	@Override
	public LocationAssembler assemble(Assembler asm) {
		return null;
	}
	
	@Override
	public EnumSet<Flags> flags() {
		return flags;
	}
	
}
