package initial3d.detail.glsl.intermediate.locations;

import java.io.IOException;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.LocationNameMap;
import initial3d.detail.glsl.intermediate.ShaderInterface;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.expressions.IntInstance;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenInput;

public class InterfaceArrayLocation implements Location {

	// geometry environment will use some sort of proxy allocator to wrap
	// these around the actual vertex output interface.
	// uniform block arrays will do something similar.
	
	private ArrayTypeName type;
	private ShaderInterface iface;
	private Location inner;
	
	protected InterfaceArrayLocation(ShaderInterface iface_, ArrayTypeName type_, Location inner_) {
		iface = iface_;
		type = type_;
		inner = inner_;
	}

	@Override
	public TypeName type() {
		return type;
	}

	@Override
	public boolean constexpr() {
		return false;
	}
	
	public Location member(int i) {
		IntInstance xi = new IntInstance(type().source(), type().scope(), false);
		xi.value(i);
		return member(new ConstantPrimitiveLocation(xi));
	}
	
	public Location member(PrimitiveLocation i) {
		return inner.withOffset(this, i);
	}
	
	@Override
	public int size() {
		throw new UnsupportedOperationException("interface arrays have no meaningful size");
	}
	
	@Override
	public int align() {
		return inner.align();
	}
	
	@Override
	public void enableRandomAccess() {
		throw new UnsupportedOperationException("interface arrays cannot be guaranteed contiguous");
	}
	
	@Override
	public Location withOffset(Location array, PrimitiveLocation offset) {
		throw new UnsupportedOperationException("interface arrays cannot be offset");
	}
	
	@Override
	public PrimitiveLocation applyOffset(PrimitiveLocation base, PrimitiveLocation offset) {
		return ((InterfacePrimitiveLocation) base).withInterfaceOffset(this, offset);
	}

	@Override
	public Location translateSubscript(Translator trans, PrimitiveLocation index) {
		inner.enableRandomAccess();
		return member(index);
	}

	@Override
	public int address() {
		throw new IllegalStateException("interface arrays dont have addresses");
	}

	@Override
	public int id() {
		throw new IllegalStateException("interface arrays dont have location ids");
	}

	@Override
	public Location parent() {
		return null;
	}

	@Override
	public String apiChildName(LocationNameMap locnames, String cseg) {
		return apiFullName(locnames) + "[" + cseg + "]";
	}

	@Override
	public Location findApiLocation(LocationNameMap locnames, TokenInput tin) throws IOException {
		if (tin.hasNextToken(Token.Type.EOF)) return this;
		// block array support
		if (tin.hasNextToken(".")) return inner.findApiLocation(locnames, tin);
		tin.expectNextToken("[");
		Token ti = tin.expectNextToken(Token.Type.INT_LITERAL);
		tin.expectNextToken("]");
		if (ti.text().equals("0")) return inner.findApiLocation(locnames, tin);
		// TODO interface array element introspection by index?
		throw new IllegalArgumentException("interface arrays don't support introspection of specific indices");
	}
	
	@Override
	public void initMemberNames(LocationNameMap locnames) {
		locnames.put(inner.id(), "0");
		inner.initMemberNames(locnames);
	}
	
}
