package initial3d.detail.glsl.intermediate.locations;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import initial3d.detail.glsl.intermediate.Allocator;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.LocationNameMap;
import initial3d.detail.glsl.intermediate.TranslateException;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Layout;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.expressions.StructInstance;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenInput;

public class StructLocation implements Location {

	private ScalarTypeName type;
	private Layout layout;
	private Location parent;
	private HashMap<Variable, Location> members = new HashMap<>();
	private boolean contiguous = true;
	private boolean constexpr = false;
	private int size = 0, align = 1;
	private boolean random_access = false;
	private int locid = -1;
	
	public StructLocation(ScalarTypeName type_, Allocator alloc_, Layout layout_, Location parent_) {
		type = type_;
		layout = layout_;
		parent = parent_;
		locid = alloc_.newID();
		align = type.definition().allocAlign(layout);
		// ensure struct align
		alloc_.align(align);
		for (VariableName vname : type_.definition().memberScope().localDeclaredNames(VariableName.class)) {
			Variable var = vname.definition();
			// allow members to override layout
			Location ml = alloc_.alloc(vname.type(), var.layout() != null ? var.layout() : layout, this);
			members.put(var, ml);
			// update struct size
			size = Allocator.makeAligned(size, ml.align());
			// TODO record offsets?
			size += ml.size();
		}
		// finalize size based on align
		size = Allocator.makeAligned(size, align);
		// ensure allocator pads to full alignment
		alloc_.align(align);
	}
	
	public StructLocation(Translator trans, StructInstance x) {
		type = (ScalarTypeName) x.type();
		constexpr = true;
		for (VariableName vname : type.definition().memberScope().localDeclaredNames(VariableName.class)) {
			Variable var = vname.definition();
			members.put(var, x.member(vname).translate(trans));
		}
	}
	
	public StructLocation(StructLocation base_, Location array, PrimitiveLocation offset) {
		type = base_.type;
		constexpr = false;
		for (VariableName vname : type.definition().memberScope().localDeclaredNames(VariableName.class)) {
			Variable var = vname.definition();
			members.put(var, base_.member(vname).withOffset(array, offset));
		}
	}
	
	@Override
	public TypeName type() {
		return type;
	}
	
	@Override
	public boolean constexpr() {
		return constexpr;
	}
	
	public Location member(VariableName vname) {
		Variable var = vname.definition();
		if (var == null) {
			throw new NullPointerException("variable " + vname + " was not defined");
		}
		Location l = members.get(var);
		if (l == null) {
			throw new NullPointerException("member " + vname + " of " + type() + " was null");
		}
		return l;
	}
	
	@Override
	public boolean replaceSubLocation(Location l0, Location l1) {
		if (random_access) throw new TranslateException("struct is randomly accessible");
		boolean r = false;
		for (Map.Entry<Variable, Location> me : members.entrySet()) {
			if (me.getValue() == l0) {
				me.setValue(l1);
				r = true;
			} else {
				r |= me.getValue().replaceSubLocation(l0, l1);
			}
		}
		// if sublocation replacement happened, the layout should not be treated as contiguous
		contiguous &= !r;
		return r;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public int align() {
		return align;
	}

	@Override
	public Location withOffset(Location array, PrimitiveLocation offset) {
		return new StructLocation(this, array, offset);
	}

	@Override
	public void enableRandomAccess() {
		if (!contiguous) throw new TranslateException("struct is not contiguous");
		for (Map.Entry<Variable, Location> me : members.entrySet()) {
			me.getValue().enableRandomAccess();
		}
		random_access = true;
	}

	@Override
	public int address() {
		// TODO what if no members?
		return members.get(type.definition().memberVariableNames().get(0).definition()).address();
	}

	@Override
	public int id() {
		if (locid < 0) throw new IllegalStateException("no id");
		return locid;
	}

	@Override
	public Location parent() {
		return parent;
	}

	@Override
	public Collection<Location> children() {
		return Collections.unmodifiableCollection(members.values());
	}

	@Override
	public String apiChildName(LocationNameMap locnames, String cseg) {
		// need to support blocks with no instance names
		if (locnames.get(id()) == null) return cseg;
		return apiFullName(locnames) + "." + cseg;
	}

	@Override
	public Location findApiLocation(LocationNameMap locnames, TokenInput tin) throws IOException {
		if (tin.hasNextToken(Token.Type.EOF)) return this;
		tin.expectNextToken(".");
		Token t = tin.expectNextToken(Token.Type.IDENTIFIER);
		for (Map.Entry<Variable, Location> me : members.entrySet()) {
			if (t.text().equals(locnames.get(me.getValue().id()))) return me.getValue().findApiLocation(locnames, tin);
		}
		throw new NoSuchElementException("struct has no member " + t.text());
	}
	
	@Override
	public void initMemberNames(LocationNameMap locnames) {
		for (Map.Entry<Variable, Location> me : members.entrySet()) {
			locnames.put(me.getValue().id(), me.getKey().name().plainText());
			me.getValue().initMemberNames(locnames);
		}
	}
	
	@Override
	public String toString() {
		return type.text();
	}
}
