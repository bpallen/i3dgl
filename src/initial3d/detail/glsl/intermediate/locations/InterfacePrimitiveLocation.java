package initial3d.detail.glsl.intermediate.locations;

import java.util.EnumSet;

import initial3d.detail.glsl.intermediate.Assembler;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.LocationAssembler;
import initial3d.detail.glsl.intermediate.LocationNameMap;
import initial3d.detail.glsl.intermediate.ShaderInterface;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.Layout;
import initial3d.detail.glsl.lang.names.TypeName;

public class InterfacePrimitiveLocation implements PrimitiveLocation {

	// the interface, eg 'vertex - geometry' or 'uniform'
	// interface instances are shared between interfacing shader stages,
	// so 'in' or 'out' depends on which shader.
	private ShaderInterface iface;
	
	// base interface index
	// ordinary uniforms use index 0; successive uniform block declarations will have incrementing base indices
	private int iface_index;
	
	// interface address (offset from buffer start)
	// delayed resolve to allow for user-specified locations
	private int address = -1;
	
	// location id (address independent)
	private int locid;
	
	private BuiltinType type;
	private Layout layout;
	private Location parent;
	private int align;
	private EnumSet<Flags> flags = EnumSet.noneOf(Flags.class);
	
	public InterfacePrimitiveLocation(ShaderInterface iface_, int iface_index_, int locid_, BuiltinType type_, Layout layout_, Location parent_, int align_) {
		if (align_ < type_.allocSize(layout_)) throw new IllegalArgumentException("align too small");
		iface = iface_;
		iface_index = iface_index_;
		locid = locid_;
		type = type_;
		layout = layout_;
		parent = parent_;
		align = align_;
	}
	
	public ShaderInterface iface() {
		return iface;
	}
	
	public int ifaceIndex() {
		return iface_index;
	}
	
	@Override
	public int id() {
		return locid;
	}
	
	@Override
	public int address() {
		return address;
	}
	
	public void address(int address_) {
		if (((align - 1) & address_) != 0) throw new IllegalArgumentException("address not aligned");
		address = address_;
	}
	
	@Override
	public TypeName type() {
		return type.name();
	}

	@Override
	public boolean constexpr() {
		return false;
	}
	
	@Override
	public int size() {
		return type.allocSize(layout);
	}

	@Override
	public int align() {
		return align;
	}
	
	@Override
	public int readCount() {
		// TODO interface read counts
		return 9001;
	}
	
	@Override
	public int writeCount() {
		// TODO interface write counts
		return 9001;
	}

	@Override
	public BuiltinType primitiveType() {
		return type;
	}

	@Override
	public void enableRandomAccess() {
		// TODO interfaces always have random access?
	}

	public InterfaceOffsetPrimitiveLocation withInterfaceOffset(InterfaceArrayLocation array, PrimitiveLocation offset) {
		// offsetting an interface primitive wrt an interface array sets the interface index
		// however, this can only be done once because we can't just repeatedly add such offsets
		return new InterfaceOffsetPrimitiveLocation(this, offset);
	}
	
	@Override
	public String toString() {
		return String.format("<%s:%d>%s@%d %s", iface.iname(), iface_index, type.name().mangledText(), address, flags);
	}

	@Override
	public Location parent() {
		return parent;
	}

	@Override
	public String apiChildName(LocationNameMap locnames, String cseg) {
		return null;
	}

	@Override
	public LocationAssembler assemble(Assembler asm) {
		return asm.ifaceLoc(type, iface, iface_index, address, flags);
	}
	
	@Override
	public EnumSet<Flags> flags() {
		return flags;
	}
	
}
