package initial3d.detail.glsl.intermediate.locations;

import java.io.IOException;
import java.util.EnumSet;

import initial3d.detail.glsl.intermediate.Assembler;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.LocationAssembler;
import initial3d.detail.glsl.intermediate.LocationNameMap;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenInput;

public interface PrimitiveLocation extends Location {

	public static enum Flags {
		GROUP_ONCE, FLAT, SMOOTH, NOPERSPECTIVE; 
	}
	
	@Override
	default int address() {
		throw new UnsupportedOperationException("no location address implemented");
	}
	
	@Override
	default int id() {
		throw new UnsupportedOperationException("no location id implemented");
	}
	
	@Override
	default String apiChildName(LocationNameMap locnames, String cseg) {
		return null;
	}
	
	@Override
	default Location findApiLocation(LocationNameMap locnames, TokenInput tin) throws IOException {
		tin.expectPeekToken(Token.Type.EOF);
		return this;
	}
	
	@Override
	default PrimitiveLocation withOffset(Location array, PrimitiveLocation offset) {
		return array.applyOffset(this, offset);
	}
	
	public BuiltinType primitiveType();
	
	default void addRead(MacroOp op) { }
	
	default void addWrite(MacroOp op) { }
	
	/**
	 * Clear any state about reads/writes.
	 */
	default void clearReadWrite() { }
	
	/**
	 * If this is 0, any writes to this location may be optimized out.
	 * @return
	 */
	default int readCount() {
		return 9001;
	}
	
	default int writeCount() {
		return 9001;
	}
	
	/**
	 * Maybe replace with an equivalent location (or return this).
	 * @return
	 */
	default PrimitiveLocation maybeReplace() {
		return this;
	}
	
	public LocationAssembler assemble(Assembler asm);
	
	public EnumSet<Flags> flags();
	
}
