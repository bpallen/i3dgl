package initial3d.detail.glsl.intermediate;

import initial3d.detail.glsl.intermediate.locations.ArrayLocation;

public interface LocationAssembler {
	
	public void arrayOffset(ArrayLocation a, LocationAssembler l);
	
	public void ifaceOffset(LocationAssembler l);
	
	public void load(int group);
	
	public void store(int group);
	
}
