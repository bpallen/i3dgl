package initial3d.detail.glsl.intermediate;

@Deprecated
public interface ShaderInterfaceElement {

	public Location apiLocation(String apiname);
	
	public Location apiLocation(int apiloc);
	
}
