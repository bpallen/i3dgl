package initial3d.detail.glsl.intermediate;

import java.util.EnumSet;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.BlockName;

public class UniformInterface extends ShaderInterface {

	public UniformInterface(InfoLog infolog_) {
		super(infolog_, "uniform", EnumSet.of(PrimitiveLocation.Flags.GROUP_ONCE));
	}
	
	@Override
	protected Location allocVar(Variable var, BlockName block) {
		if (block == null) return super.allocVar(var, block);
		// uniform block
		if (var.type() instanceof ArrayTypeName) {
			// block array
			ArrayTypeName atype = (ArrayTypeName) var.type();
			if (atype.size() < 0) throw new IllegalArgumentException("bad array size");
			Allocator alloc = createAllocator(block.plainText(), true, atype.size());
			return alloc.allocVar(var.type(), var.layout());
		} else {
			// single block
			Allocator alloc = createAllocator(block.plainText(), false, 1);
			return alloc.allocVar(var.type(), var.layout());
		}
	}

}
