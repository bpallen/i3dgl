package initial3d.detail.glsl.intermediate;

import java.util.EnumSet;

import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.VoidLocation;
import initial3d.detail.glsl.lang.BuiltinFunction;
import initial3d.detail.glsl.lang.names.FunctionName;

/**
 * All ops are expected to have some fixed number of destination operands (>=0)
 * followed by some possibly variable number of source operands (>=0).
 * Destination operands for an op are all either write-only or read-write.
 * 
 * @author ben
 *
 */
public class MacroOp {
	
	public static enum DestMode {
		WRITE(false, true), READWRITE(true, true);
		
		public final boolean read, write;
		
		private DestMode(boolean read_, boolean write_) {
			read = read_;
			write = write_;
		}
	}
	
	public static final EnumSet<Code> UNGROUPABLE = EnumSet.of(Code.NOP, Code.JMP, Code.JEZ, Code.JNZ);
	
	public static enum Code {
		// basic
		NOP(0),
		JMP(0),
		JEZ(0),
		JNZ(0),
		CALL,
		MOV,
		// bool
		BCMPEQ,
		BCMPNE,
		BNOT,
		BAND,
		BOR,
		BXOR,
		// int
		IADD,
		ISUB,
		IMUL,
		IDIV,
		IREM,
		INEG,
		INOT,
		ISL,
		ISR,
		IAND,
		IOR,
		IXOR,
		ICMPEQ,
		ICMPNE,
		ICMPLT,
		ICMPLE,
		ICMPGT,
		ICMPGE,
		IMIN,
		IMAX,
		IABS,
		// uint
		UADD,
		USUB,
		UMUL,
		UDIV,
		UREM,
		UNEG,
		UNOT,
		USL,
		USR,
		UAND,
		UOR,
		UXOR,
		UCMPEQ,
		UCMPNE,
		UCMPLT,
		UCMPLE,
		UCMPGT,
		UCMPGE,
		UMIN,
		UMAX,
		UABS,
		// float
		FADD,
		FSUB,
		FMUL,
		FDIV,
		FREM,
		FNEG,
		FCMPEQ,
		FCMPNE,
		FCMPLT,
		FCMPLE,
		FCMPGT,
		FCMPGE,
		FMIN,
		FMAX,
		FABS,
		// conversion
		I2F,
		F2I,
		U2F,
		F2U,
		// special
		DISCARD(0),
		DFDX,
		DFDY;
		
		public final int dest_args;
		public final DestMode dest_mode;
		
		private Code() {
			dest_args = 1;
			dest_mode = DestMode.WRITE;
		}
		
		private Code(int dest_args_) {
			dest_args = dest_args_;
			dest_mode = DestMode.WRITE;
		}
	}
	
	public final Code code;
	public final Label label;
	public final PrimitiveLocation[] args;
	
	public MacroOp(Code code_, Label label_, PrimitiveLocation ...args_) {
		code = code_;
		label = label_;
		args = args_;
		if (label != null) label.target(this);
	}
	
	public MacroOp(Code code_, PrimitiveLocation ...args_) {
		code = code_;
		label = null;
		args = args_;
	}
	
	public int destArgs() {
		return code.dest_args;
	}
	
	public void destroy() {
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(label == null ? "        " : label);
		sb.append(" : ");
		sb.append(code);
		for (PrimitiveLocation l : args) {
			sb.append(" ");
			sb.append(l);
		}
		return sb.toString();
	}
	
	public static class Branch extends MacroOp {
		
		public final Label target;
		
		public Branch(Code code_, Label target_, PrimitiveLocation arg) {
			super(code_, new PrimitiveLocation[] {arg == null ? new VoidLocation() : arg});
			target = target_;
			target.addSource(this);
		}
		
		@Override
		public String toString() {
			return super.toString() + " -> " + target;
		}
		
		@Override
		public void destroy() {
			target.removeSource(this);
		}
		
	}
	
	public static class Call extends MacroOp {

		public final BuiltinFunction func;
		
		public Call(Code code_, BuiltinFunction func_, PrimitiveLocation[] args_) {
			super(code_, args_);
			func = func_;
		}
		
		@Override
		public String toString() {
			return super.toString() + " -> " + func.name().text();
		}
		
	}
	
}
