package initial3d.detail.glsl.intermediate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import initial3d.detail.StringUtil;

public class Label {

	private MacroOp target;
	private ArrayList<MacroOp> sources = new ArrayList<>();
	private String id = StringUtil.randomID(8);
	
	public Label() {
		
	}
	
	public MacroOp target() {
		return target;
	}
	
	public void target(MacroOp op) {
		if (target != null) throw new IllegalStateException("label already has target");
		target = op;
	}
	
	public void addSource(MacroOp op) {
		sources.add(op);
	}
	
	public void removeSource(MacroOp op) {
		sources.remove(op);
	}
	
	public List<MacroOp> sources() {
		return Collections.unmodifiableList(sources);
	}
	
	@Override
	public String toString() {
		return id;
	}
	
}
