package initial3d.detail.glsl.intermediate;

public interface LocationNameMap {

	public String get(int id);
	
	default void put(int id, String name) {
		throw new UnsupportedOperationException("location name put not supported");
	}
	
	public int rootLocation(String name);
	
	default boolean linkOnly() {
		return false;
	}
	
}
