package initial3d.detail.glsl.intermediate;

import initial3d.detail.glsl.lang.EvalException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

/**
 * This is a translation aid that allows us to contruct function calls
 * just from locations in the intermediate repesentation.
 * 
 * @author ben
 *
 */
public class LocationExpression implements Expression {

	private Location loc;
	private boolean lvalue;
	
	public LocationExpression(Location loc_, boolean lvalue_) {
		loc = loc_;
		lvalue = lvalue_;
	}
	
	@Override
	public Token source() {
		return null;
	}

	@Override
	public Scope scope() {
		return null;
	}

	@Override
	public TypeName type() {
		return loc.type();
	}
	
	@Override
	public boolean lValue() {
		return lvalue;
	}

	@Override
	public Instance eval(VariableInstanceMap vars) {
		throw new EvalException("not intended for evaluation");
	}

	@Override
	public Location translate(Translator trans) {
		return loc;
	}

	@Override
	public void pushRead() {
		
	}

	@Override
	public void pushWrite() {
		
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		
	}

}
