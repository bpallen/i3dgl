package initial3d.detail.glsl.intermediate;

import static initial3d.Functions.all;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.locations.ConstantPrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.BuiltinFunction;
import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Function;
import initial3d.detail.glsl.lang.Layout;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Swizzle;
import initial3d.detail.glsl.lang.Type;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.NullInstance;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.FunctionName;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;

public class Translator {

	public static interface BranchHandler {
		public void branch(Translator trans, MacroOp.Code code, String lname, PrimitiveLocation arg);
	}
	
	private class TranslationScope {
		
		public final int depth;
		public final TranslationScope parent;
		public final HashMap<Variable, Location> vars;
		public final LocalAllocator alloc = new LocalAllocator(this);
		public final ArrayList<LocalPrimitiveLocation> locations = new ArrayList<>();
		public final HashMap<String, BranchHandler> branches = new HashMap<>();
		int regindex = -1;
		
		public TranslationScope(TranslationScope parent_, boolean statement) {
			parent = parent_;
			depth = parent == null ? 0 : parent.depth + 1;
			vars = statement ? null : new HashMap<>();
		}
		
		public boolean statement() {
			return vars == null;
		}
		
		public void assignRegisters() {
			regindex = parent == null ? 0 : parent.regindex;
			for (LocalPrimitiveLocation l : locations) {
				if (l.readCount() > 0 || l.writeCount() > 0) {
					// only give register indices when used
					l.regindex = regindex++;
				}
			}
		}
	}
	
	private class LocalAllocator implements Allocator {

		private final TranslationScope ascope;
		
		public LocalAllocator(TranslationScope ascope_) {
			ascope = ascope_;
		}
		
		@Override
		public InfoLog infoLog() {
			return Translator.this.infolog;
		}
		
		@Override
		public void align(int a) {
			// no align required or possible
		}
		
		@Override
		public PrimitiveLocation allocPrimitive(BuiltinType type, Layout layout, Location parent) {
			// TODO parent?
			LocalPrimitiveLocation loc = new LocalPrimitiveLocation(ascope, type);
			ascope.locations.add(loc);
			return loc;
		}

		@Override
		public Location alloc(TypeName type, Layout layout, Location parent) {
			return type.definition().allocate(this, type, layout, parent);
		}

		@Override
		public int newID() {
			return -1;
		}
		
	}
	
	private static class LocalPrimitiveLocation implements PrimitiveLocation {
		
		// FIXME min-align/min-size for when registers need to be in memory for random access
		
		private TranslationScope ascope;
		private BuiltinType type;
		private boolean random_access = false;
		private EnumSet<Flags> flags = EnumSet.noneOf(Flags.class);
		
		// register index, delayed resolve 
		public int regindex = -1;
		// number of reading ops
		private int reads = 0;
		// number of writing ops
		private int writes = 0;
		// number of movs from 'inexpensive' locations (other registers or constants)
		private int cheap_writes = 0;
		
		// maybe replace this location?
		public PrimitiveLocation cheap_source = null;
		private boolean relocated = false;
		
		public LocalPrimitiveLocation(TranslationScope ascope_, BuiltinType type_) {
			ascope = ascope_;
			type = type_;
		}

		@Override
		public TypeName type() {
			return type.name();
		}
		
		@Override
		public boolean constexpr() {
			return false;
		}

		@Override
		public BuiltinType primitiveType() {
			return type;
		}
		
		@Override
		public void addRead(MacroOp op) {
			reads++;
		}

		@Override
		public void addWrite(MacroOp op) {
			writes++;
			if (op.code == MacroOp.Code.MOV && op.args[0] == this) {
				//if (op.args[1] instanceof LocalPrimitiveLocation || op.args[1] instanceof ConstantPrimitiveLocation) {
				cheap_writes++;
				cheap_source = op.args[1];
				//}
			}
		}
		
		@Override
		public void clearReadWrite() {
			reads = 0;
			writes = 0;
			cheap_writes = 0;
		}
		
		@Override
		public int readCount() {
			// prevent location removal if random access required
			// and any other optimizations based on read count
			return reads + (random_access ? 9001 : 0);
		}
		
		@Override
		public int writeCount() {
			// prevent any optimizations based on write count
			return writes + (random_access ? 9001 : 0);
		}
		
		@Override
		public PrimitiveLocation maybeReplace() {
			if (random_access) return this;
			// if only write is cheap, can replace with source of write,
			// provided that the source itself is written at most once.
			// if the source was written after mov to this location, when replaced
			// this location would incorrectly evaluate to the source's new value.
			if (writes == 1 && cheap_writes == 1 && cheap_source.writeCount() <= 1) {
				if (cheap_source instanceof LocalPrimitiveLocation) {
					LocalPrimitiveLocation cheap_local = (LocalPrimitiveLocation) cheap_source;
					if (ascope.depth < cheap_local.ascope.depth) {
						// lifetimes! if replacement is from a deeper scope, relocate it up to this scope
						// fixing this is necessary, and i think this accomplishes it
						cheap_local.relocate(ascope);
					}
				}
				return cheap_source;
			}
			return this;
		}
		
		public void relocate(TranslationScope scope) {
			ascope.locations.remove(this);
			ascope = scope;
			ascope.locations.add(this);
			relocated = true;
		}

		@Override
		public String toString() {
			return "r" + regindex + type.name().mangledText();
			// + String.format("[r=%d,w=%d,cw=%d]" + (relocated ? "*" : ""), reads, writes, cheap_writes);
		}

		@Override
		public int size() {
			return type.allocSize(null);
		}

		@Override
		public int align() {
			return type.allocSize(null);
		}

		@Override
		public void enableRandomAccess() {
			random_access = true;
		}

		@Override
		public Location parent() {
			throw new UnsupportedOperationException("local primitive locations aren't parented atm");
		}
		
		@Override
		public LocationAssembler assemble(Assembler asm) {
			return asm.localLoc(type, regindex, random_access);
		}
		
		@Override
		public EnumSet<Flags> flags() {
			return flags;
		}
		
	}
	
	private InfoLog infolog;
	private Environment environ;
	
	// global variables, regardless of actual lexical scope (static duration)
	// separated to make static duration easier to do
	private TranslationScope globals = new TranslationScope(null, false);
	
	// local variables and temporaries (auto duration)
	private Stack<TranslationScope> locals = new Stack<>();
	
	// local variables, mapped by lexical scope
	private HashMap<Scope, Stack<TranslationScope>> scoped_locals = new HashMap<>();
	
	// all allocation scopes in allocation order
	private ArrayList<TranslationScope> all_allocs = new ArrayList<>();
	
	// function linkage
	private HashMap<FunctionName, Function> functions = new HashMap<>();
	private FunctionName fname_main = null;
	
	private ArrayList<MacroOp> ops = new ArrayList<>();
	
	public Translator(Environment environ_) {
		infolog = environ_.infoLog();
		environ = environ_;
		// simplify scope management by putting globals on locals stack
		locals.push(globals);
		all_allocs.add(globals);
	}
	
	public InfoLog infoLog() {
		return infolog;
	}
	
	public void addGlobalScope(Scope scope) {
		FunctionName fmain = scope.declaredFunction("_Fmain.v()", infolog);
		if (fmain != null && fname_main == null) fname_main = fmain;
		// TODO this could be done better (think member functions for example)
		for (FunctionName fname : scope.localDeclaredNames(FunctionName.class)) {
			Function func = fname.definition();
			if (func != null) functions.put(fname, func);
		}
	}
	
	public Function functionDefinition(FunctionName fname) {
		Function func = fname.definition();
		if (func == null) func = functions.get(fname);
		if (func != null) return func;
		infolog.error("undefined function", fname.source());
		throw new TranslateException("undefined function");
	}
	
	public void pushScope(Scope lex_scope) {
		TranslationScope parent = locals.lastElement(); 
		Stack<TranslationScope> ascopes = scoped_locals.get(lex_scope);
		if (ascopes == null) {
			ascopes = new Stack<>();
			scoped_locals.put(lex_scope, ascopes);
		}
		TranslationScope as = new TranslationScope(parent, false);
		ascopes.push(as);
		locals.push(as);
		all_allocs.add(as);
	}
	
	public void popScope(Scope lex_scope) {
		Stack<TranslationScope> ascopes = scoped_locals.get(lex_scope);
		if (ascopes == null || ascopes.isEmpty()) throw new IllegalArgumentException("no matching scope to pop");
		if (ascopes.lastElement() != locals.lastElement()) throw new IllegalArgumentException("bad scope to pop, is not most recent push");
		if (locals.size() <= 1) throw new IllegalStateException("no local scopes to pop");
		ascopes.pop();
		locals.pop();
	}
	
	public void pushStatement() {
		TranslationScope as = new TranslationScope(locals.lastElement(), true);
		locals.push(as);
		all_allocs.add(as);
	}
	
	public void popStatement() {
		if (locals.size() <= 1) throw new IllegalStateException("no local scopes to pop");
		TranslationScope as = locals.lastElement();
		if (!as.statement()) throw new IllegalStateException("no statement to pop");
		locals.pop();
	}
	
	private Location variable(TranslationScope ascope, Variable var, boolean nocreate) {
		if (ascope.statement()) throw new IllegalStateException("cannot alloc variable in statement");
		Location loc = ascope.vars.get(var);
		if (loc != null || nocreate) return loc;
		if (var.type().ref()) throw new IllegalArgumentException("cannot allocate variables of reference type");
		if (var.type() instanceof ArrayTypeName && ((ArrayTypeName) var.type()).size() < 0) {
			infolog.error("cannot allocate array variable of unknown size", var.source());
			throw new TranslateException("bad array size");
		}
		loc = ascope.alloc.allocVar(var.type(), var.layout());
		ascope.vars.put(var, loc);
		return loc;
	}
	
	public Location variable(VariableName var, boolean nocreate) {
		final Node owner = var.definition().scope().owner();
		if (var.definition().name().interfaceQualifier() != null) {
			// interface variable
			String iname = var.definition().name().interfaceQualifier().text();
			if ("in".equals(iname)) {
				return environ.inputs().variable(this, var);
			} else if ("out".equals(iname)) {
				return environ.outputs().variable(this, var);
			} else if ("uniform".equals(iname)) {
				return environ.uniforms().variable(this, var);
			} else {
				throw new IllegalArgumentException("bad interface qualifier");
			}
		} else if (owner instanceof Function || owner instanceof Swizzle) {
			// local variable
			Stack<TranslationScope> ascopes = scoped_locals.get(var.definition().scope());
			if (ascopes == null || ascopes.isEmpty()) throw new TranslateException("no alloc scope available for variable");
			return variable(ascopes.lastElement(), var.definition(), nocreate);
		} else if (owner instanceof Type) {
			infolog.error("member variables cannot be accessed directly", var.source());
			throw new TranslateException("member variables cannot be accessed directly");
		} else {
			// global variable
			return variable(globals, var.definition(), nocreate);
		}
	}
	
	public Location variable(VariableName var) {
		return variable(var, false);
	}
	
	private void bindReference(TranslationScope ascope, VariableName var, Location tloc) {
		if (tloc == null) throw new NullPointerException("cannot bind variable to null");
		if (ascope.statement()) throw new IllegalStateException("cannot bind reference in statement");
		if (ascope.vars.get(var.definition()) != null) {
			infolog.error("cannot bind reference; variable is already bound", var.source());
			throw new TranslateException("variable is already bound");
		}
		ascope.vars.put(var.definition(), tloc);
	}
	
	public void bindReference(VariableName var, Location tloc) {
		final Node owner = var.definition().scope().owner();
		// TODO it doesnt really have to have ref type
		//if (!var.type().ref()) throw new IllegalArgumentException("variable must have reference type");
		if (var.definition().name().interfaceQualifier() != null) {
			infolog.error("cannot bind interface variable as reference", var.source());
			throw new TranslateException("cannot bind interface variable as reference");
		} else if (owner instanceof Function || owner instanceof Swizzle) {
			// FIXME this 'owner instanceof X' stuff is terrible
			// local variable
			Stack<TranslationScope> ascopes = scoped_locals.get(var.definition().scope());
			if (ascopes == null || ascopes.isEmpty()) throw new TranslateException("no alloc scope available for variable");
			bindReference(ascopes.lastElement(), var, tloc);
		} else if (owner instanceof Type) {
			infolog.error("member variables cannot be accessed directly", var.source());
			throw new TranslateException("member variables cannot be accessed directly");
		} else {
			// global variable
			bindReference(globals, var, tloc);
		}
	}
	
	private void bindConstant(TranslationScope ascope, VariableName var, Instance value) {
		if (ascope.statement()) throw new IllegalStateException("cannot bind constant in statement");
		if (ascope.vars.get(var.definition()) != null) {
			infolog.error("cannot bind constant; variable is already bound", var.source());
			infolog.detail("see value of attempted constant binding", value.source());
			throw new TranslateException("variable is already bound");
		}
		ascope.vars.put(var.definition(), value.translate(this));
	}
	
	public void bindConstant(VariableName var, Instance value) {
		final Node owner = var.definition().scope().owner();
		// TODO it doesn't really have to be const
		//if (!var.type().qualified("const")) throw new IllegalArgumentException("variable must have const type");
		if (var.definition().name().interfaceQualifier() != null) {
			infolog.error("cannot bind interface variable as constant", var.source());
			throw new TranslateException("cannot bind interface variable as constant");
		} else if (owner instanceof Function || owner instanceof Swizzle) {
			// local variable
			Stack<TranslationScope> ascopes = scoped_locals.get(var.definition().scope());
			if (ascopes == null || ascopes.isEmpty()) throw new TranslateException("no alloc scope available for variable");
			bindConstant(ascopes.lastElement(), var, value);
		} else if (owner instanceof Type) {
			infolog.error("member variables cannot be accessed directly", var.source());
			throw new TranslateException("member variables cannot be accessed directly");
		} else {
			// global variable
			bindConstant(globals, var, value);
		}
	}
	
	public Location temporary(TypeName type) {
		if (locals.size() <= 1) throw new IllegalStateException("no local scopes for temporary alloc");
		if (type instanceof ArrayTypeName && ((ArrayTypeName) type).size() < 0) {
			infolog.error("cannot allocate array temporary of unknown size", type.source());
			throw new TranslateException("bad array size");
		}
		return locals.lastElement().alloc.alloc(type, null, null);
	}
	
	public Location global(TypeName type) {
		if (type instanceof ArrayTypeName && ((ArrayTypeName) type).size() < 0) {
			infolog.error("cannot allocate array global of unknown size", type.source());
			throw new TranslateException("bad array size");
		}
		return globals.alloc.alloc(type, null, null);
	}
	
	public void branchHandler(String lname, BranchHandler h) {
		locals.lastElement().branches.put(lname, h);
	}
	
	public void copy(Location dst, Location src) {
		Token tok = BuiltinScope.builtinToken("=");
		OverloadSet assign = dst.type().definition().memberScope().declaredOverload("operator=", infolog);
		Expression[] params = new Expression[2];
		params[0] = new LocationExpression(dst, true);
		params[1] = new LocationExpression(src, false);
		FunctionCall fc = assign.call(infolog, tok, dst.type().scope(), params);
		fc.translate(this);
	}
	
	public MacroOp op(MacroOp.Code code, PrimitiveLocation ...args) {
		// TODO grouping
		// TODO check dest operands arent constant
		MacroOp o = new MacroOp(code, args);
		ops.add(o);
		return o;
	}
	
	/**
	 * Inserts a NOP with the given label.
	 * 
	 * @param lab
	 * @return
	 */
	public MacroOp label(Label lab) {
		MacroOp o = new MacroOp(MacroOp.Code.NOP, lab);
		ops.add(o);
		return o;
	}
	
	public MacroOp branch(MacroOp.Code code, Label target, PrimitiveLocation arg) {
		MacroOp.Branch o = new MacroOp.Branch(code, target, arg);
		ops.add(o);
		return o;
	}
	
	public MacroOp branch(MacroOp.Code code, Label target) {
		return branch(code, target, null);
	}
	
	public void branch(MacroOp.Code code, String lname, PrimitiveLocation arg) {
		for (int i = locals.size(); i-- > 0; ) {
			BranchHandler h = locals.get(i).branches.get(lname);
			if (h != null) {
				h.branch(this, code, lname, arg);
				return;
			}
		}
		throw new IllegalArgumentException("label name not found");
	}
	
	public void branch(MacroOp.Code code, String lname) {
		branch(code, lname, null);
	}
	
	public MacroOp call(BuiltinFunction func, PrimitiveLocation ...args) {
		MacroOp.Call o = new MacroOp.Call(MacroOp.Code.CALL, func, args);
		ops.add(o);
		return o;
	}
	
	private void assignRegisters() {
		for (TranslationScope s : all_allocs) {
			s.assignRegisters();
		}
	}
	
	private void removeRedundantJumps() {
		for (int i = 0; i < ops.size(); i++) {
			MacroOp op0 = ops.get(i);
			if (op0.code == MacroOp.Code.JMP) {
				MacroOp.Branch op1 = (MacroOp.Branch) op0;
				for (int j = i + 1; j < ops.size(); j++) {
					MacroOp op2 = ops.get(j);
					if (op1.target == op2.label) {
						// found jump target separated only by nops, can remove
						ops.remove(i);
						op1.destroy();
						// recheck removed index
						i--;
					}
					if (op2.code != MacroOp.Code.NOP) break;
				}
			}
		}
	}
	
	private void updateReadWriteCounts() {
		// clear all counts
		// TODO clear counts for interface locations
		// TODO clear count for constants?
		for (TranslationScope scope : all_allocs) {
			for (LocalPrimitiveLocation loc : scope.locations) {
				loc.clearReadWrite();
			}
		}
		// add new counts
		for (int i = 0; i < ops.size(); i++) {
			MacroOp op = ops.get(i);
			for (int j = 0; j < op.code.dest_args; j++) {
				if (op.code.dest_mode.read) op.args[j].addRead(op);
				if (op.code.dest_mode.write) op.args[j].addWrite(op);
			}
			for (int j = op.code.dest_args; j < op.args.length; j++) {
				op.args[j].addRead(op);
			}
		}
	}
	
	private void replaceRedundantLocations() {
		for (int i = 0; i < ops.size(); i++) {
			MacroOp op = ops.get(i);
			// maybe replace source args
			for (int j = op.code.dest_args; j < op.args.length; j++) {
				op.args[j] = op.args[j].maybeReplace();
			}
		}
	}
	
	private void removeRedundantMovs() {
		for (int i = 0; i < ops.size(); i++) {
			MacroOp op = ops.get(i);
			if (op.code == MacroOp.Code.MOV) {
				// if any dest args needs to be written, keep the mov
				boolean keep = false;
				for (int j = 0; j < op.code.dest_args; j++) {
					keep |= op.args[j].readCount() > 0;
				}
				// if dst and src are the same, remove
				if (op.args[0] == op.args[1]) keep = false;
				// if mov can be subsumed by previous op, remove
				if (i > 0) {
					MacroOp op0 = ops.get(i - 1);
					if (op0.code.dest_args == 1 && op0.code.dest_mode == MacroOp.DestMode.WRITE && op0.args[0] == op.args[1]) {
						// mov immediately preceeded by an op with dest == mov source
						if (op.args[1].readCount() == 1) {
							// this mov is the only read of its source
							// replace dest of preceeding op
							op0.args[0] = op.args[0];
							keep = false;
						}
					}
				}
				if (!keep) {
					// remove dead mov
					ops.remove(i);
					op.destroy();
					// recheck removed index
					i--;
				}
			}
		}
	}
	
	private void removeRedundantNops() {
		for (int i = 0; i < ops.size(); i++) {
			MacroOp op = ops.get(i);
			if (op.code == MacroOp.Code.NOP && (op.label == null || op.label.sources().isEmpty())) {
				// nop with no label or a label that is never jumped to, can remove
				ops.remove(i);
				op.destroy();
				// recheck removed index
				i--;
			}
		}
	}
	
	private void translateSpecialInputs() {
		// save/restore actual ops
		// the ops for this need to go before main, but we can't compile them until after main
		ArrayList<MacroOp> realops = new ArrayList<>();
		realops.addAll(ops);
		ops.clear();
		environ.uniforms().copySpecialInputs(this);
		environ.inputs().copySpecialInputs(this);
		ops.addAll(realops);
	}
	
	private void translateSpecialOutputs() {
		environ.outputs().copySpecialOutputs(this);
	}
	
	public void translate(FunctionName fname) {
		if (fname == null) throw new NullPointerException("null function name");
		ops.clear();
		pushStatement();
		try {
			// translate main
			FunctionCall fc = new FunctionCall(infolog, BuiltinScope.builtinToken("("), fname.scope(), fname);
			fc.translate(this);
			// special variables
			translateSpecialInputs();
			translateSpecialOutputs();
			// simple optimizations
			for (int i = 0; i < 3; i++) {
				updateReadWriteCounts();
				replaceRedundantLocations();
				updateReadWriteCounts();
				removeRedundantMovs();
				updateReadWriteCounts();
				removeRedundantJumps();
				removeRedundantNops();
			}
			// TODO repeat optimizations until nothing happens?
			assignRegisters();
		} finally {
			popStatement();
		}
	}
	
	public void translateMain() {
		if (fname_main == null) {
			infolog.error("no main() function found for " + environ.shaderType());
			throw new TranslateException("no main");
		}
		translate(fname_main);
	}
	
	public List<MacroOp> ops() {
		return Collections.unmodifiableList(ops);
	}
	
	public int localCount() {
		int c = 0;
		for (TranslationScope as : all_allocs) {
			c = Math.max(c, as.regindex);
		}
		if (c < 0) throw new IllegalStateException("register assignment not run");
		return c;
	}
	
	public void printOps() {
		for (MacroOp op : ops) {
			System.err.println(op);
		}
	}
	
}




