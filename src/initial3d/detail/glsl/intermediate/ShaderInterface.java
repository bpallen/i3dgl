package initial3d.detail.glsl.intermediate;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.StringInfoLog;
import initial3d.detail.glsl.intermediate.locations.InterfacePrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.SpecialPrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation.Flags;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.Layout;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.BlockName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.StripEmptyTokenInput;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenInput;
import initial3d.detail.glsl.tokens.Tokenizer;

public class ShaderInterface {

	// shader interfaces should not have any inherent notion of group size;
	// they represent the common data representation shared between shader
	// stages (or a shader and the application), and group size is purely
	// an execution mechanism, which may (will) differ across the interface.
	
	// a shader environment may use a 'proxy interface (view)' around the actual
	// interface to simplify semantic requirements. e.g. the geometry
	// environment may use a proxy interface to deal with the use of arrays
	// to access one of many instances of the actual interface.
	// TODO how's that gonna deal with the variable definitions?
	// probably with 'fake' vars in a 'fake' scope
	
	// TODO uniform variable indices are a thing, see glGetActiveUniform
	// idk how to implement that functionality nicely yet (implying i know how to do any of this)
	
	// ---
	
	// - group execution should be handled by the assembler not the translator
	// - locations should be able to specify 'group once' behaviour
	// - need some sort of language __grouponce { } construct (not var qualifier, needs to affect temporaries)
	// - setting the contents of uniforms is required to be type-checked, and bounds checked for arrays
	// - api uniform locations (integers) do not seem to have any specific requirements
	// - api uniform locations could map to arbitrary location objects instead of directly or indirectly to buffer offsets
	//   i.e. multiple api uniform locations could refer to the same buffer offset, the location id effectively also encoding the type
	// - allow allocators to specify min alignment (struct/array member? global? layout?)
	// - interface allocator maybe has 'default align' and 'vector element align'
	// TODO make array .length() constexpr if length is known?
	// TODO from an api pov, array names should (probably?) refer to element [0]
	// TODO block array declaration parsing
	// TODO array length translation for interface arrays
	// - special interface variables: layout(__special), alloc to registers mapped to special locations, copy in before main, out after
	// TODO branchless ?: for int/float
	// TODO shortcircuiting for ?:, &&, || (need func purity test?) 
	// TODO random access constant arrays how?
	// TODO clean uses of TokenInput.pushContext
	// TODO give layout objects to all variables?
	
	private class InterfaceAllocator implements Allocator {

		private int iface_index;
		private int nextalign = 1;
		private int nextautoaddress = 0;
		private int nextuseraddress = -1;
		private boolean autoassigned = false;
		private ArrayList<InterfacePrimitiveLocation> primitives = new ArrayList<>();
		
		public InterfaceAllocator(int iface_index_) {
			iface_index = iface_index_;
		}
		
		@Override
		public InfoLog infoLog() {
			return ShaderInterface.this.infolog;
		}
		
		@Override
		public void align(int a) {
			nextalign = Math.max(nextalign, a);
		}
		
		@Override
		public PrimitiveLocation allocPrimitive(BuiltinType type, Layout layout, Location parent) {
			int al = Math.max(nextalign, type.allocAlign(layout));
			InterfacePrimitiveLocation l = new InterfacePrimitiveLocation(ShaderInterface.this, iface_index, newID(), type, layout, parent, al);
			// general flags
			l.flags().addAll(locflags);
			// interpolation flags
			if (layout != null) {
				if (layout.interp == Layout.Interpolation.FLAT) {
					l.flags().add(Flags.FLAT);
				} else if (layout.interp == Layout.Interpolation.SMOOTH) {
					l.flags().add(Flags.SMOOTH);
				} else if (layout.interp == Layout.Interpolation.NOPERSPECTIVE) {
					l.flags().add(Flags.NOPERSPECTIVE);
				}
			}
			if (nextuseraddress >= 0) {
				// assign address now for user-specified locations
				nextuseraddress = Allocator.makeAligned(nextuseraddress, al);
				l.address(nextuseraddress);
				nextuseraddress += l.size();
			}
			primitives.add(l);
			id2loc.put(l.id(), l);
			nextalign = 1;
			return l;
		}
		
		@Override
		public Location alloc(TypeName type, Layout layout, Location parent) {
			boolean startuser = false;
			if (layout != null && layout.location >= 0 && nextuseraddress < 0) {
				// start assigning addresses now for user-specified locations
				if (autoassigned) throw new IllegalStateException("cannot specify location after auto assignment has happened");
				nextuseraddress = layout.location * 16;
				startuser = true;
			}
			Location l = type.definition().allocate(this, type, layout, parent);
			if (startuser) {
				// if we started user address asignment, we need to stop too
				// move auto-assign range above user range
				nextautoaddress = Math.max(nextautoaddress, nextuseraddress);
				nextuseraddress = -1;
			}
			id2loc.put(l.id(), l);
			return l;
		}
		
		@Override
		public Location allocVar(TypeName type, Layout layout) {
			Location l = alloc(type, layout, null);
			return l;
		}
		
		public void assignAddresses() {
			autoassigned = true;
			for (InterfacePrimitiveLocation l : primitives) {
				if (l.address() < 0) {
					nextautoaddress = Allocator.makeAligned(nextautoaddress, l.align());
					l.address(nextautoaddress);
					nextautoaddress += l.size();
				}
			}
			// round buffer size up?
			nextautoaddress = Allocator.makeAligned(nextautoaddress, 16);
		}
		
		public int allocSize() {
			if (!autoassigned) throw new IllegalStateException("auto assignment has not been run");
			return nextautoaddress;
		}
		
		public List<InterfacePrimitiveLocation> primitives() {
			return primitives;
		}
		
		@Override
		public int newID() {
			return nextlocid++;
		}
		
	}
	
	private class SpecialAllocator implements Allocator {

		private String linkname;
		private int nextprimid = 0;
		
		public SpecialAllocator(String linkname_) {
			linkname = linkname_;
		}
		
		@Override
		public InfoLog infoLog() {
			return ShaderInterface.this.infolog;
		}
		
		@Override
		public void align(int a) {
			// no align possible
		}

		@Override
		public PrimitiveLocation allocPrimitive(BuiltinType type, Layout layout, Location parent) {
			SpecialPrimitiveLocation l = new SpecialPrimitiveLocation(type, linkname, newID(), nextprimid++);
			// general flags
			l.flags().addAll(locflags);
			return l;
		}

		@Override
		public Location alloc(TypeName type, Layout layout, Location parent) {
			return type.definition().allocate(this, type, layout, parent);
		}

		@Override
		public int newID() {
			return nextlocid++;
		}
		
	}
	
	private InfoLog infolog;
	private String iname;
	EnumSet<PrimitiveLocation.Flags> locflags;
	
	private InterfaceAllocator alloc0 = new InterfaceAllocator(0);
	private ArrayList<InterfaceAllocator> allocs = new ArrayList<>(Arrays.asList(alloc0));
	
	// locations by link name
	// blocks are always linked by block name (and _not_ variable name)
	private HashMap<String, Location> linkname2loc = new HashMap<>();
	
	// all locations (seen by alloc[prim]) by id
	private int nextlocid = 0;
	private HashMap<Integer, Location> id2loc = new HashMap<>();
	
	// link name segments by id
	private HashMap<Integer, String> id2linkname = new HashMap<>();
	
	// interface elements
	private ArrayList<String> elementnames = new ArrayList<>(Arrays.asList("__default"));
	private ArrayList<InterfaceAllocator> elementallocs = new ArrayList<>(Arrays.asList(alloc0));
	
	public ShaderInterface(InfoLog infolog_, String iname_, EnumSet<PrimitiveLocation.Flags> locflags_) {
		infolog = infolog_;
		iname = iname_;
		locflags = locflags_;
	}
	
	public InfoLog infoLog() {
		return infolog;
	}
	
	public String iname() {
		return iname;
	}

	/**
	 * Get or allocate variable by link name.
	 * 
	 * @param type
	 * @param block
	 * @return
	 */
	public Location variable(Variable var) {
		if (var.type().ref()) throw new IllegalArgumentException("interface variables cannot have reference type");
		BlockName block = var.type().definition().block();
		if (block == null && var.type() instanceof ArrayTypeName) {
			// for block arrays, the block name identifies the array
			block = ((ArrayTypeName) var.type()).innerType().definition().block();
		}
		if (block != null && !block.declared()) throw new IllegalArgumentException("block is not declared");
		// use block declaration name
		if (block != null) block = (BlockName) block.declaration();
		// blocks always linked by block name, others by var name (plain text)
		String lname = block == null ? var.name().plainText() : block.plainText();
		Location l = linkname2loc.get(lname);
		if (l != null) {
			// check found location matches expected type
			if (!var.type().declEquals(l.type())) {
				infolog.error(
					String.format("link name '%s' of type %s was already linked with type %s", lname, var.type().text(), l.type().text()),
					var.source()
				);
				throw new TranslateException("link type mismatch");
			}
			return l;
		}
		// need to allocate
		if (var.layout() != null && var.layout().special) {
			// alloc special interface variable
			l = new SpecialAllocator(lname).allocVar(var.type(), var.layout());
		} else {
			// alloc normal interface variable
			l = allocVar(var, block);
		}
		linkname2loc.put(lname, l);
		id2linkname.put(l.id(), lname);
		l.initMemberNames(linkNames());
		return l;			
	}
	
	/**
	 * Subclass allocation hook.
	 * For block arrays, <code>block</code> is the name of the member block.
	 * 
	 * @param type
	 * @param block
	 * @return
	 */
	protected Location allocVar(Variable var, BlockName block) {
		return alloc0.allocVar(var.type(), var.layout());
	}
	
	protected Allocator createAllocator(String name, boolean isarray, int count) {
		if (count < 1) throw new IllegalArgumentException("bad count");
		InterfaceAllocator a = new InterfaceAllocator(elementnames.size());
		allocs.add(a);
		if (isarray) {
			for (int i = 0; i < count; i++) {
				elementnames.add(name + "[" + i + "]");
				elementallocs.add(a);
			}
		} else {
			elementnames.add(name);
			elementallocs.add(a);
		}
		return a;
	}
	
	public void assignAddresses() {
		for (InterfaceAllocator a : allocs) {
			a.assignAddresses();
		}
	}
	
	public LocationNameMap linkNames() {
		return new LocationNameMap() {
			@Override
			public String get(int id) {
				return id2linkname.get(id);
			}
			@Override
			public void put(int id, String name) {
				// silently drop bad ids
				if (id < 0) return;
				id2linkname.put(id, name);
			}
			@Override
			public int rootLocation(String name) {
				Location l = linkname2loc.get(name);
				if (l == null) return -1;
				return l.id();
			}
			@Override
			public boolean linkOnly() {
				return true;
			}
		};
	}
	
	public Location apiLocation(LocationNameMap locnames, String apiname) {
		InfoLog infolog = new StringInfoLog();
		Tokenizer tok = new Tokenizer(infolog, "<api>", new StringReader(apiname));
		TokenInput tin = new StripEmptyTokenInput(tok);
		try {
			Token t0 = tin.expectNextToken(Token.Type.IDENTIFIER);
			Location l0 = id2loc.get(locnames.rootLocation(t0.text()));
			if (l0 == null) return null;
			return l0.findApiLocation(locnames, tin);
		} catch (IOException e) {
			throw new AssertionError(e);
		}
	}

	public Location apiLocation(int apiloc) {
		return id2loc.get(apiloc);
	}
	
	public int elementCount() {
		return elementnames.size();
	}
	
	public int elementIndex(String name) {
		return elementnames.indexOf(name);
	}
	
	public String elementName(int i) {
		return elementnames.get(i);
	}
	
	public int elementAllocSize(int i) {
		return elementallocs.get(i).allocSize();
	}
	
	public List<InterfacePrimitiveLocation> elementPrimitives(int i) {
		return Collections.unmodifiableList(elementallocs.get(i).primitives());
	}
	
	public void printSummary(PrintWriter out, LocationNameMap locnames) {
		out.println("interface " + iname + (locnames.linkOnly() ? " (link names, no api access)" : ""));
		for (int i = 0; i < elementnames.size(); i++) {
			out.printf("element %d : %s\n", i, elementnames.get(i));
		}
		for (Map.Entry<Integer, Location> me : id2loc.entrySet()) {
			String aname = me.getValue().apiFullName(locnames);
			out.printf("location %3d : %s : %s\n", me.getKey(), aname == null ? "?" : aname, me.getValue());
			if (aname != null) {
				// temp? check names resolve correctly
				Location aloc = apiLocation(locnames, aname);
				if (aloc != me.getValue()) {
					throw new IllegalStateException("location api name does not resolve to itself");
				}
			}
		}
		out.flush();
	}
	
	public String printSummary() {
		StringWriter sw = new StringWriter();
		printSummary(new PrintWriter(sw), linkNames());
		return sw.toString();
	}
}
