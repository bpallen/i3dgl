package initial3d.detail.glsl.intermediate;

import initial3d.GLEnum;
import initial3d.detail.glsl.InfoLog;

public abstract class AbstractEnvironment implements Environment {

	private InfoLog infolog;
	private GLEnum stype;
	private ShaderInterfaceView uniforms;
	private ShaderInterfaceView inputs;
	private ShaderInterfaceView outputs;
	private Translator translator;
	
	public AbstractEnvironment(GLEnum stype_, InfoLog infolog_) {
		stype = stype_;
		infolog = infolog_;
		translator = new Translator(this);
	}
	
	@Override
	public GLEnum shaderType() {
		return stype;
	}
	
	@Override
	public InfoLog infoLog() {
		return infolog;
	}
	
	@Override
	public void initInterfaces(ShaderInterface uniforms_, ShaderInterface inputs_, ShaderInterface outputs_) {
		uniforms = new ShaderInterfaceView(uniforms_);
		inputs = new ShaderInterfaceView(inputs_);
		outputs = new ShaderInterfaceView(outputs_);
	}
	
	@Override
	public ShaderInterfaceView inputs() {
		return inputs;
	}

	@Override
	public ShaderInterfaceView outputs() {
		return outputs;
	}

	@Override
	public ShaderInterfaceView uniforms() {
		return uniforms;
	}

	@Override
	public Translator translator() {
		return translator;
	}
	
}
