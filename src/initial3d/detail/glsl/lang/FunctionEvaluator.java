package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.lang.expressions.Instance;

public interface FunctionEvaluator {
	public Instance eval(VariableInstanceMap vars, Instance ...actual_params);
}