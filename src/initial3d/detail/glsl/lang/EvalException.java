package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.BuildException;

public class EvalException extends BuildException {

	private static final long serialVersionUID = 1L;

	public EvalException(String msg) {
		super(msg);
	}
	
}
