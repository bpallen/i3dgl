package initial3d.detail.glsl.lang;

import java.lang.reflect.Method;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.SysInfoLog;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.VoidLocation;
import initial3d.detail.glsl.lang.expressions.BoolInstance;
import initial3d.detail.glsl.lang.expressions.FloatInstance;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.IntInstance;
import initial3d.detail.glsl.lang.expressions.UIntInstance;
import initial3d.detail.glsl.lang.expressions.VoidInstance;
import initial3d.detail.glsl.lang.names.FunctionName;
import initial3d.detail.glsl.lang.names.Qualifier;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenBuilder;

public class BuiltinScope extends Scope {

	private static final String[] UNQUALIFIED = new String[]{};
	
	public static final BuiltinScope instance = new BuiltinScope();
	
	public static Token builtinToken(String text) {
		return new TokenBuilder().fileName("<builtin>").text(text).classify().toToken();
	}
	
	private void commonType(String a, String b, String res) {
		commonType(makeType(a), makeType(b), makeType(res));
	}
	
	private void defineType(String name, Class<? extends Instance> instcls, Class<?> equiv) {
		TypeName type = new ScalarTypeName(builtinToken(name), this);
		type.declare(InfoLog.SYSOUT);
		type.define(new BuiltinType(type, instcls, equiv), InfoLog.SYSOUT);
		// make all builtin types their own common type
		commonType(type, type, type);
	}
	
	private TypeName makeType(String declname, String ...declquals) {
		TypeName type = declaredType(declname, InfoLog.SYSOUT).copy();
		for (String q : declquals) {
			type.qualify(q);
		}
		return type;
	}
	
	private TypeName[] makeTypes() {
		return new TypeName[0];
	}
	
	private TypeName[] makeTypes(TypeName ...types) {
		return types;
	}
	
	private TypeName[] makeTypes(String ...ss) {
		TypeName[] types = new TypeName[ss.length];
		for (int i = 0; i < types.length; i++) {
			types[i] = makeType(ss[i]);
		}
		return types;
	}
	
	private void defineFunction(String name, String[] quals, TypeName rettype, TypeName[] paramtypes, FunctionEvaluator feval, FunctionTranslator ftrans) {
		FunctionName fname = new FunctionName(builtinToken(name), this, rettype, paramtypes);
		fname.qualify("__constexpr");
		for (String q : quals) fname.qualify(q);
		fname.declare(InfoLog.SYSOUT);
		BuiltinFunction func = new BuiltinFunction(fname, feval, ftrans);
		fname.define(func, InfoLog.SYSOUT);
	}
	
	private void defineFunction(String name, String quals[], TypeName rettype, TypeName[] paramtypes, Method method, MacroOp.Code opcode) {
		FunctionName fname = new FunctionName(builtinToken(name), this, rettype, paramtypes);
		fname.qualify("__constexpr");
		for (String q : quals) fname.qualify(q);
		fname.declare(InfoLog.SYSOUT);
		BuiltinFunction func = new BuiltinFunction(fname, method, opcode);
		fname.define(func, InfoLog.SYSOUT);
	}
	
	private void defineFunction(String name, String[] quals, TypeName rettype, TypeName[] paramtypes, String methodname) {
		Class<?>[] jparamtypes = new Class<?>[paramtypes.length];
		for (int i = 0; i < jparamtypes.length; i++) {
			jparamtypes[i] = paramtypes[i].definition().javaEquivalent();
		}
		Method method;
		try {
			method = BuiltinFunctions.class.getDeclaredMethod(methodname, jparamtypes);
		} catch (Exception e) {
			throw new AssertionError(e);
		}
		defineFunction(name, quals, rettype, paramtypes, method, MacroOp.Code.CALL);
	}
	
	// these have to be implemented directly by codegen, not java functions
	private void defineAssignments() {
		
		// bool assign
		defineFunction(
			"__builtin_assign", UNQUALIFIED,
			makeType("void"),
			makeTypes(makeType("bool", "__ref"), makeType("bool", "const", "__ref")),
			(VariableInstanceMap vars, Instance ...actual_params) -> {
				BoolInstance lhs = (BoolInstance) actual_params[0];
				BoolInstance rhs = (BoolInstance) actual_params[1];
				lhs.value(rhs.value());
				return null;
			},
			(Translator trans, Location ...actual_params) -> {
				trans.op(MacroOp.Code.MOV, (PrimitiveLocation) actual_params[0], (PrimitiveLocation) actual_params[1]);
				return new VoidLocation();
			}
		);
		
		// int assign
		defineFunction(
			"__builtin_assign", UNQUALIFIED,
			makeType("void"),
			makeTypes(makeType("int", "__ref"), makeType("int", "const", "__ref")),
			(VariableInstanceMap vars, Instance ...actual_params) -> {
				IntInstance lhs = (IntInstance) actual_params[0];
				IntInstance rhs = (IntInstance) actual_params[1];
				lhs.value(rhs.value());
				return null;
			},
			(Translator trans, Location ...actual_params) -> {
				trans.op(MacroOp.Code.MOV, (PrimitiveLocation) actual_params[0], (PrimitiveLocation) actual_params[1]);
				return new VoidLocation();
			}
		);
		
		// uint assign
		defineFunction(
			"__builtin_assign", UNQUALIFIED,
			makeType("void"),
			makeTypes(makeType("uint", "__ref"), makeType("uint", "const", "__ref")),
			(VariableInstanceMap vars, Instance ...actual_params) -> {
				UIntInstance lhs = (UIntInstance) actual_params[0];
				UIntInstance rhs = (UIntInstance) actual_params[1];
				lhs.value(rhs.value());
				return null;
			},
			(Translator trans, Location ...actual_params) -> {
				trans.op(MacroOp.Code.MOV, (PrimitiveLocation) actual_params[0], (PrimitiveLocation) actual_params[1]);
				return new VoidLocation();
			}
		);
		
		// float assign
		defineFunction(
			"__builtin_assign", UNQUALIFIED,
			makeType("void"),
			makeTypes(makeType("float", "__ref"), makeType("float", "const", "__ref")),
			(VariableInstanceMap vars, Instance ...actual_params) -> {
				FloatInstance lhs = (FloatInstance) actual_params[0];
				FloatInstance rhs = (FloatInstance) actual_params[1];
				lhs.value(rhs.value());
				return null;
			},
			(Translator trans, Location ...actual_params) -> {
				trans.op(MacroOp.Code.MOV, (PrimitiveLocation) actual_params[0], (PrimitiveLocation) actual_params[1]);
				return new VoidLocation();
			}
		);
	}
	
	private BuiltinScope() {
		super(null);
		
		allowDeclarations();
		
		// TODO builtins
		
		// primitive types
		defineType("void", VoidInstance.class, void.class);
		defineType("bool", BoolInstance.class, boolean.class);
		defineType("int", IntInstance.class, int.class);
		defineType("uint", UIntInstance.class, int.class);
		defineType("float", FloatInstance.class, float.class);
		
		commonType("float", "int", "float");
		commonType("float", "uint", "float");
		
		// primitive assignments
		defineAssignments();
		
		// builtin functions implemented in java
		for (Method m : BuiltinFunctions.class.getDeclaredMethods()) {
			ImplementBuiltin a = m.getAnnotation(ImplementBuiltin.class);
			if (a != null) {
				defineFunction(a.name(), a.qualifiers(), makeType(a.rettype()), makeTypes(a.paramtypes()), m, a.opcode());
			}
		}
		
		// default block layout params
		layout("uniform").packing = Layout.Packing.SHARED;
		layout("uniform").matrix_order = Layout.MatrixOrder.COLUMN_MAJOR;
		
	}

}
