package initial3d.detail.glsl.lang;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.names.VariableName;

public class VariableInstanceMap {
	
	private InfoLog infolog;
	
	// stack of entered scopes
	private Stack<Scope> scopes = new Stack<>();
	
	// map scopes to stacks of scope instances 
	private Map<Scope, Stack<Map<Variable, Instance>>> instances = new HashMap<>();
	
	public VariableInstanceMap(InfoLog infolog_, Scope scope_) {
		infolog = infolog_;
		// TODO why construct with a scope without instatiating the locals? 
		scopes.push(scope_);
	}
	
	public InfoLog infoLog() {
		return infolog;
	}
	
	public Scope currentScope() {
		return scopes.peek();
	}
	
	public void pushScope(Scope scope) {
		scopes.push(scope);
		Stack<Map<Variable, Instance>> stack = instances.get(scope);
		if (stack == null) {
			stack = new Stack<>();
			instances.put(scope, stack);
		}
		stack.push(new HashMap<>());
	}
	
	public void popScope(Scope scope) {
		Stack<Map<Variable, Instance>> stack = instances.get(scope);
		stack.pop();
		scopes.pop();
	}
	
	private Map<Variable, Instance> scopeLocals(VariableName vname) {
		if (!vname.declared()) throw new EvalException("variable name not declared");
		Stack<Map<Variable, Instance>> stack = instances.get(vname.declScope());
		Map<Variable, Instance> locals = stack == null ? null : stack.peek();
		if (locals == null) {
			infolog.error("no instance of containing scope for variable '${text}'", vname.source());
			infolog.detail("see declaration", vname.declaration().source());
			throw new EvalException("no scope instance");
		}
		return locals;
	}
	
	public Instance get(VariableName vname) {
		if (vname.definition().constexpr()) return vname.definition().initVal();
		Map<Variable, Instance> locals = scopeLocals(vname);
		Instance x = locals.get(vname.definition());
		if (x == null) {
			infolog.error("no instance of variable '${text}'", vname.source());
			if (vname.declared()) infolog.detail("see declaration", vname.declaration().source());
			throw new EvalException("no variable instance");
		}
		return x;
	}
	
	public Instance put(VariableName vname, Instance x) {
		Map<Variable, Instance> locals = scopeLocals(vname);
		return locals.put(vname.definition(), x);
	}
	
}
