package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.lang.names.ScalarTypeName;

public class TypeException extends NameException {

	private static final long serialVersionUID = 1L;

	public TypeException(String msg_, ScalarTypeName ...names_) {
		super(msg_, names_);
	}

}
