package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.lang.expressions.Instance;

public class ReturnValue extends RuntimeException {

	// abusing exceptions for control flow, feels good
	// TODO something better than this
	
	private Instance value;
	
	public ReturnValue(Instance value_) {
		value = value_;
	}
	
	public Instance value() {
		return value;
	}
	
}
