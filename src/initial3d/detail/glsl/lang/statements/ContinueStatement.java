package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.tokens.Token;

public class ContinueStatement implements Statement {

	private Token source;
	private Scope scope;
	
	public ContinueStatement(Token source_, Scope scope_) {
		source = source_;
		scope = scope_;
	}
	
	@Override
	public Token source() {
		return source;
	}

	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public String toString() {
		return "continue";
	}
	
	@Override
	public void eval(VariableInstanceMap vars) {
		throw new LoopContinue(this);
	}
	
	@Override
	public void translate(Translator trans) {
		trans.branch(MacroOp.Code.JMP, "continue");
	}

	@Override
	public void pushUsage() {
		
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		
	}
	
}
