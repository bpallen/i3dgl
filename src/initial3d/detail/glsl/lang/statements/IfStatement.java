package initial3d.detail.glsl.lang.statements;

import java.util.ArrayList;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Label;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.BoolInstance;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.tokens.Token;

public class IfStatement implements Statement {

	private static class Branch {
		public Expression cond;
		public ScopedStatement body;
	}
	
	private Token source;
	private Scope scope;
	private ArrayList<Branch> ifbranches = new ArrayList<>();
	private Branch elsebranch;
	
	public IfStatement(Token source_, Scope scope_) {
		source = source_;
		scope = scope_;
	}
	
	@Override
	public Token source() {
		return source;
	}

	@Override
	public Scope scope() {
		return scope;
	}

	public void addIfBranch(Expression cond, Statement body, Scope owned_scope) {
		Branch b = new Branch();
		b.cond = cond;
		b.body = new ScopedStatement(body, owned_scope);
		ifbranches.add(b);
	}
	
	public void elseBranch(Statement body, Scope owned_scope) {
		Branch b = new Branch();
		b.body = new ScopedStatement(body, owned_scope);
		elsebranch = b;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (Branch b : ifbranches) {
			sb.append(first ? "if[" : "elseif[");
			sb.append(b.cond);
			sb.append("]");
			sb.append(b.body);
			first = false;
		}
		if (elsebranch != null) {
			sb.append("else");
			sb.append(elsebranch.body);
		}
		return sb.toString();
	}
	
	private boolean evalBranch(Branch b, VariableInstanceMap vars) {
		vars.pushScope(b.body.ownedScope());
		try {
			if (b.cond != null && !((BoolInstance) b.cond.eval(vars)).value()) return false;
			b.body.inner().eval(vars);
			return true;
		} finally {
			vars.popScope(b.body.ownedScope());
		}
	}
	
	@Override
	public void eval(VariableInstanceMap vars) {
		for (Branch b : ifbranches) {
			if (evalBranch(b, vars)) return;
		}
		if (elsebranch != null) evalBranch(elsebranch, vars);
	}
	
	/**
	 * if cond is false, jump over branch body.
	 * otherwise, exec branch body then jump to end.
	 * 
	 * @param b
	 * @param trans
	 * @param end
	 */
	private void translateBranch(Branch b, Translator trans, Label end) {
		trans.pushScope(b.body.ownedScope());
		try {
			Label next = new Label();
			// eval cond
			// TODO translate 'if' with constexpr cond
			if (b.cond != null) {
				trans.pushStatement();
				try {
					trans.branch(MacroOp.Code.JEZ, next, (PrimitiveLocation) b.cond.translate(trans));
				} finally {
					trans.popStatement();
				}
			}
			// exec body
			b.body.inner().translate(trans);
			// jump to end of entire if statement
			trans.branch(MacroOp.Code.JMP, end);
			// branch target for else(if)
			trans.label(next);
		} finally {
			trans.popScope(b.body.ownedScope());
		}
	}
	
	@Override
	public void translate(Translator trans) {
		Label end_label = new Label();
		trans.pushStatement();
		try {
			for (int i = 0; i < ifbranches.size(); i++) {
				translateBranch(ifbranches.get(i), trans, end_label);
			}
			if (elsebranch != null) translateBranch(elsebranch, trans, end_label);
			trans.label(end_label);
		} finally {
			trans.popStatement();
		}
		
	}
	
	public Statement maybeReplace(InfoLog infolog) {
		if (ifbranches.size() == 1 && elsebranch == null) {
			Statement s = ifbranches.get(0).body.inner();
			if (s instanceof DiscardStatement) {
				// if has no else(if) branches, and if branch is just a discard
				// and our condition with the discard and replace with it
				DiscardStatement sd = (DiscardStatement) s;
				sd.condAnd(ifbranches.get(0).cond, infolog);
				return sd;
			}
		}
		return this;
	}

	@Override
	public void pushUsage() {
		for (Branch b : ifbranches) {
			b.body.pushUsage();
		}
		if (elsebranch != null) elsebranch.body.pushUsage();
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		for (Branch b : ifbranches) {
			b.body.visitVariableAccesses(v);
		}
		if (elsebranch != null) elsebranch.body.visitVariableAccesses(v);
	}
	
}






