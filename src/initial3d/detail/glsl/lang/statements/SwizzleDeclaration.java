package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Swizzle;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.names.SwizzleName;
import initial3d.detail.glsl.tokens.Token;

public class SwizzleDeclaration implements Declaration {

	private SwizzleName name;
	private Swizzle defn;
	
	public SwizzleDeclaration(SwizzleName name_, Swizzle defn_) {
		name = name_;
		defn = defn_;
	}
	
	@Override
	public Token source() {
		return name.source();
	}

	@Override
	public Scope scope() {
		return name.scope();
	}
	
	public SwizzleName name() {
		return name;
	}
	
	public Swizzle definition() {
		return defn;
	}

	@Override
	public void eval(VariableInstanceMap vars) {
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("declare[swizzle ");
		sb.append(name.text());
		sb.append(" = ");
		sb.append(defn);
		sb.append("]");
		return sb.toString();
	}

}
