package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess;

public interface Declaration extends Statement {

	@Override
	default void eval(VariableInstanceMap vars) { }
	
	@Override
	default void translate(Translator trans) { }
	
	@Override
	default void pushUsage() { }
	
	@Override
	default void visitVariableAccesses(VariableAccess.Visitor v) { }
	
}
