package initial3d.detail.glsl.lang.statements;

import java.util.ArrayList;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Label;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.EvalException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.BoolInstance;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.VariableAccess;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.tokens.Token;

public class LoopStatement implements Statement {

	private Token source;
	private Scope scope;
	
	private Statement init;
	private Expression cond;
	private boolean cond_last;
	private ScopedStatement body;
	private Expression incr;
	
	// unroll things
	private boolean should_unroll = false;
	private boolean can_unroll = false;
	private ArrayList<Instance> unroll_values = new ArrayList<>();
	
	public LoopStatement(Token source_, Scope scope_) {
		source = source_;
		scope = scope_;
	}
	
	@Override
	public Token source() {
		return source;
	}

	@Override
	public Scope scope() {
		return scope;
	}
	
	public void init(Statement s) {
		if (s != null && s.scope().parent() != scope) throw new IllegalArgumentException("bad loop init scope");
		init = s;
	}
	
	public void cond(Expression x, boolean last) {
		// should be at body scope
		cond = x;
		cond_last = last;
	}
	
	public void body(Statement s, Scope owned_scope) {
		// should be within init scope if it exists
		body = new ScopedStatement(s, owned_scope);
	}
	
	public void incr(Expression x) {
		// should be at body scope
		incr = x;
	}
	
	public void shouldUnroll(boolean b) {
		should_unroll = b;
	}
	
	public void build(InfoLog infolog) {
		checkUnroll(infolog);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("loop[");
		if (init != null) {
			sb.append("init: ");
			sb.append(init);
			sb.append(", ");
		}
		if (cond != null) {
			sb.append(cond_last ? "cond-last: " : "cond-first: ");
			sb.append(cond);
		} else {
			sb.append("cond: ?");
		}
		if (incr != null) {
			sb.append(", incr: ");
			sb.append(incr);
		}
		sb.append("]");
		sb.append(body);
		return sb.toString();
	}
	
	@Override
	public void eval(VariableInstanceMap vars) {
		// push init scope if needed, then loop ( push body scope, test cond, maybe exec body, pop scope )
		if (cond == null) {
			// TODO loop with no cond
			vars.infoLog().error("loop condition is null", source);
			throw new EvalException("loop condition is null");
		}
		if (init != null) vars.pushScope(init.scope());
		try {
			int count = 0;
			if (init != null) init.eval(vars);
			while (true) {
				vars.pushScope(body.ownedScope());
				try {
					if (!cond_last && !((BoolInstance) cond.eval(vars)).value()) break;
					try {
						body.inner().eval(vars);
					} catch (LoopContinue e) {
						// just keep going, increment must be eval'd
					} catch (LoopBreak e) {
						break;
					}
					if (incr != null) incr.eval(vars);
					if (cond_last && !((BoolInstance) cond.eval(vars)).value()) break;
				} finally {
					vars.popScope(body.ownedScope());
				}
				count++;
				if (count > 1000) {
					// TODO what limit?
					vars.infoLog().error("constexpr loop eval exceeded 1000 iterations, aborting", source);
					throw new EvalException("loop iteration limit exceeded");
				}
			}
		} finally {
			if (init != null) vars.popScope(init.scope());
		}
		
	}
	
	private void checkUnroll(InfoLog infolog) {
		infolog.pushContext(null);
		infolog.showParentContext(false);
		try {
			// prepare variable usage information
			// TODO where should pushUsage be invoked from?
			pushUsage();
			// must have condition
			if (cond == null) {
				if (should_unroll) infolog.warning("can't unroll: loop has no condition", source);
				return;
			}
			// cond must come before body
			// TODO could handle trailing conditions, but we can only unroll for-loops atm anyway
			if (cond_last) {
				if (should_unroll) infolog.warning("can't unroll: loop condition must come before body", source);
				return;
			}
			// must have single var declaration as init
			if (!(init instanceof VariableDeclaration)) {
				if (should_unroll) infolog.warning("can't unroll: loop initialization must declare loop variable", source);
				return;
			}
			final VariableDeclaration decl = (VariableDeclaration) init;
			final Variable var = decl.name().definition();
			// variable must have constant init
			if (!var.initExpr().constexpr()) {
				if (should_unroll) infolog.warning("can't unroll: loop variable is not initialized with a constant", decl.source());
				return;
			}
			// condition and increment must only access constants and the loop var
			final boolean[] var_ok = new boolean[]{true};
			cond.visitVariableAccesses((VariableAccess va) -> {
				if (va.variable().constexpr()) return;
				if (va.variable() == var) return;
				if (should_unroll) {
					infolog.warning("can't unroll: loop condition accesses non-constant variable other than loop variable", va.source());
					infolog.detail("see declaration", va.variable().source());
				}
				var_ok[0] = false;
			});
			if (incr != null) incr.visitVariableAccesses((VariableAccess va) -> {
				if (va.variable().constexpr()) return;
				if (va.variable() == var) return;
				if (should_unroll) {
					infolog.warning("can't unroll: loop increment accesses non-constant variable other than loop variable", va.source());
					infolog.detail("see declaration", va.variable().source());
				}
				var_ok[0] = false;
			});
			// body must not write to loop var
			body.visitVariableAccesses((VariableAccess va) -> {
				if (va.variable() != var) return;
				if (!va.write()) return;
				if (should_unroll) infolog.warning("can't unroll: loop body writes to loop variable", va.source());
				var_ok[0] = false;
			});
			if (!var_ok[0]) return;
			// otherwise, we should be able to unroll
			// break/continue/return are fine (they don't alter the sequence of values)
			// eval loop var sequence
			unroll_values.clear();
			infolog.pushContext("evaluating loop variable sequence", source, !should_unroll);
			try {
				VariableInstanceMap vars = new VariableInstanceMap(infolog, scope);
				vars.pushScope(init.scope());
				try {
					int count = 0;
					init.eval(vars);
					while (true) {
						vars.pushScope(body.ownedScope());
						try {
							// grab value of loop var
							// need to copy; var is modified by eval
							unroll_values.add(vars.get(var.name()).copy(vars));
							if (incr != null) incr.eval(vars);
							if (!((BoolInstance) cond.eval(vars)).value()) break;
						} finally {
							vars.popScope(body.ownedScope());
						}
						count++;
						if (count > 100) {
							// TODO what limit?
							infolog.warning("can't unroll: loop exceeded 100 iterations", source);
							return;
						}
					}
				} finally {
					vars.popScope(init.scope());
				}
			} catch (EvalException e) {
				// something wasn't constexpr
				infolog.warning("can't unroll: loop variable sequence evaluation failed", source);
				return;
			} finally {
				infolog.popContext();
			}
			// done, loop can be unrolled
			can_unroll = true;
			if (!should_unroll) {
				infolog.info(String.format("loop will be unrolled for %d iterations", unroll_values.size()), source);
				// TODO print entire list?
				if (!unroll_values.isEmpty()) infolog.detail(
					String.format(
						"values of variable %s: %s",
						var.name().plainText(), unroll_values
					)
				);
			}
		} finally {
			infolog.popContext();
		}
	}
	
	private void translateUnroll(Translator trans) {
		if (unroll_values.isEmpty()) return;
		Label end_label = new Label();
		trans.pushStatement();
		try {
			// unrolled loop
			// dont translate init; loop var needs manual binding as constant
			// if the loop var location isn't constant, it kinda defeats the point
			// note that we have to push init scope each iter because the loop var is declared there
			for (Instance x : unroll_values) {
				trans.pushScope(init.scope());
				trans.pushScope(body.ownedScope());
				try {
					// continue target (next iter)
					Label next_label = new Label();
					trans.branchHandler("break", (Translator trans2, MacroOp.Code code, String lname, PrimitiveLocation arg) -> {
						trans2.branch(code, end_label, arg);
					});
					trans.branchHandler("continue", (Translator trans2, MacroOp.Code code, String lname, PrimitiveLocation arg) -> {
						trans2.branch(code, next_label);
					});
					// bind loop var
					trans.bindConstant(((VariableDeclaration) init).name(), x);
					// body
					body.inner().translate(trans);
					// don't translate increment (should have no effect other than loop var)
					trans.label(next_label);
				} finally {
					trans.popScope(body.ownedScope());
					trans.popScope(init.scope());
				}
			}
			// end loop
			trans.label(end_label);
		} finally {
			trans.popStatement();
		}
	}
	
	@Override
	public void translate(Translator trans) {
		// unrolled?
		if (can_unroll) {
			translateUnroll(trans);
			return;
		}
		// otherwise, normal loop with condition branches and stuff
		Label end_label = new Label();
		Label loop_label = new Label();
		Label entry_label = new Label();
		trans.pushStatement();
		try {
			if (init != null) trans.pushScope(init.scope());
			try {
				// init if needed
				if (init != null) init.translate(trans);
				trans.branchHandler("break", (Translator trans2, MacroOp.Code code, String lname, PrimitiveLocation arg) -> {
					trans2.branch(code, end_label, arg);
				});
				trans.branchHandler("continue", (Translator trans2, MacroOp.Code code, String lname, PrimitiveLocation arg) -> {
					// loop increment is handled at each continue
					if (incr != null) {
						trans2.pushStatement();
						try {
							incr.translate(trans2);
						} finally {
							trans2.popStatement();
						}
					}
					trans2.branch(code, loop_label);
				});
				// if cond is trailing, jump over check on first iter
				if (cond_last) trans.branch(MacroOp.Code.JMP, entry_label);
				// begin loop
				trans.label(loop_label);
				trans.pushScope(body.ownedScope());
				try {
					// condition
					// TODO loop with no cond
					trans.pushStatement();
					try {
						trans.branch(MacroOp.Code.JEZ, end_label, (PrimitiveLocation) cond.translate(trans));
					} finally {
						trans.popStatement();
					}
					// entry target for trailing condition
					trans.label(entry_label);
					// body
					body.inner().translate(trans);
					// loop
					trans.branch(MacroOp.Code.JMP, "continue");
				} finally {
					trans.popScope(body.ownedScope());
				}
				// end loop
				trans.label(end_label);
			} finally {
				if (init != null) trans.popScope(init.scope());
			}
		} finally {
			trans.popStatement();
		}
	}

	@Override
	public void pushUsage() {
		if (init != null) init.pushUsage();
		if (cond != null) cond.pushRead();
		if (incr != null) incr.pushRead();
		body.pushUsage();
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		if (init != null) init.visitVariableAccesses(v);
		if (cond != null) cond.visitVariableAccesses(v);
		if (incr != null) incr.visitVariableAccesses(v);
		body.visitVariableAccesses(v);
	}
	
}
