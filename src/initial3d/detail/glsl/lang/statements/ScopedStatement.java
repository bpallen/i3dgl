package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.tokens.Token;

public class ScopedStatement implements Statement {

	private Statement inner;
	private Scope owned_scope;
	
	public ScopedStatement(Statement inner_, Scope owned_scope_) {
		inner = inner_;
		owned_scope = owned_scope_;
	}
	
	public Statement inner() {
		return inner;
	}
	
	public Scope ownedScope() {
		return owned_scope;
	}
	
	@Override
	public Token source() {
		return inner.source();
	}

	@Override
	public Scope scope() {
		return owned_scope.parent();
	}
	
	@Override
	public String toString() {
		String s0 = inner.toString();
		if (s0.isEmpty()) return "{}";
		StringBuilder sb = new StringBuilder("{ \n");
		for (String line : s0.split("\n")) {
			sb.append("    " + line);
			sb.append('\n');
		}
		sb.append('}');
		return sb.toString();
	}

	@Override
	public void eval(VariableInstanceMap vars) {
		vars.pushScope(owned_scope);
		try {
			inner.eval(vars);
		} finally {
			vars.popScope(owned_scope);
		}
	}
	
	@Override
	public void translate(Translator trans) {
		trans.pushScope(owned_scope);
		try {
			inner.translate(trans);
		} finally {
			trans.popScope(owned_scope);
		}
	}

	@Override
	public void pushUsage() {
		inner.pushUsage();
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		inner.visitVariableAccesses(v);
	}
	
}
