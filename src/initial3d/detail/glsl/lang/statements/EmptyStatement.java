package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.tokens.Token;

public class EmptyStatement implements Statement {

	private Token source;
	private Scope scope;
	
	public EmptyStatement(Token source_, Scope scope_) {
		source = source_;
		scope = scope_;
	}
	
	@Override
	public Token source() {
		return source;
	}
	
	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public String toString() {
		return "empty";
	}

	@Override
	public void eval(VariableInstanceMap vars) {
		
	}
	
	@Override
	public void translate(Translator trans) {
		
	}

	@Override
	public void pushUsage() {
		
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		
	}
	
}
