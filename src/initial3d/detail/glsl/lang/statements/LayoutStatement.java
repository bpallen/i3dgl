package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.lang.names.LayoutQualifier;
import initial3d.detail.glsl.lang.names.Qualifier;
import initial3d.detail.glsl.tokens.Token;

public class LayoutStatement implements Statement {

	private LayoutQualifier layout;
	private Qualifier iface;
	
	public LayoutStatement(LayoutQualifier layout_, Qualifier iface_) {
		layout = layout_;
		iface = iface_;
	}
	
	@Override
	public Token source() {
		return layout.source();
	}

	@Override
	public Scope scope() {
		return layout.scope();
	}

	@Override
	public String toString() {
		return "layout[" + iface.text() + " += " + layout.toString() + "]";
	}
	
	@Override
	public void eval(VariableInstanceMap vars) {
		// nothing to do - or rather, these should only occur at global scope and therefore never be eval'd
	}

	@Override
	public void translate(Translator trans) {
		// nothing to do
	}

	@Override
	public void pushUsage() {
		
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		
	}
	
}
