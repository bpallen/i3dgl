package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Struct;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.tokens.Token;

public class StructDeclaration implements Declaration {

	ScalarTypeName type;
	Struct defn;
	
	public StructDeclaration(ScalarTypeName type_, Struct defn_) {
		type = type_;
		defn = defn_;
	}
	
	public ScalarTypeName type() {
		return type;
	}

	public Struct definition() {
		return defn;
	}
	
	@Override
	public Token source() {
		return type.source();
	}
	
	@Override
	public Scope scope() {
		return type.scope();
	}

	@Override
	public String toString() {
		if (defn == null) {
			return "declare[struct " + type.text() + "]";
		} else {
			return "define[struct " + type.text() + " -> " + defn + "]";
		}
	}
	
}
