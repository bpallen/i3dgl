package initial3d.detail.glsl.lang.statements;

public class LoopContinue extends RuntimeException {

	// more abuse of exceptions for control flow
	// TODO something better
	
	private ContinueStatement statement;
	
	public LoopContinue(ContinueStatement s) {
		statement = s;
	}
	
	public ContinueStatement statement() {
		return statement;
	}
}
