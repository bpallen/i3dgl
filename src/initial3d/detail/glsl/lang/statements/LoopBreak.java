package initial3d.detail.glsl.lang.statements;

public class LoopBreak extends RuntimeException {

	// more abuse of exceptions for control flow
	// TODO something better
	
	private BreakStatement statement;
	
	public LoopBreak(BreakStatement s) {
		statement = s;
	}
	
	public BreakStatement statement() {
		return statement;
	}
}
