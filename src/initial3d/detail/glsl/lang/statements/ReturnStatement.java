package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.intermediate.TranslateException;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;

public class ReturnStatement implements Statement {

	private Token source;
	private Scope scope;
	private Expression expr;
	private VariableName rvar;
	
	public ReturnStatement(InfoLog infolog, Token source_, Scope scope_, Expression expr_) {
		source = source_;
		scope = scope_;
		expr = expr_;
		rvar = scope.useVariable(source.withText("__return"), infolog);
		if (rvar == null) {
			// this should be present in all ordinary functions, even ones returning void
			infolog.error("no return target available", source);
			throw new BuildException("no return target available");
		}
	}
	
	public Expression expression() {
		return expr;
	}
	
	@Override
	public Token source() {
		return source;
	}
	
	@Override
	public Scope scope() {
		return scope;
	}
	
	@Override
	public String toString() {
		return "return[" + (expr == null ? "" : expr.toString()) + "]";
	}

	@Override
	public void eval(VariableInstanceMap vars) {
		if (expr == null) {
			throw new ReturnValue(null);
		} else {
			throw new ReturnValue(expr.eval(vars));
		}
	}
	
	@Override
	public void translate(Translator trans) {
		if (expr != null) {
			// return expression present, handle return value
			if (!rvar.type().declQualEquals(expr.type())) {
				trans.infoLog().error("return expression does not match return type", source);
				throw new TranslateException("return expression does not match return type");
			}
			trans.pushStatement();
			try {
				if (rvar.type().ref()) {
					// bind return value as reference
					trans.bindReference(rvar, expr.translate(trans));
				} else {
					// copy to return value
					trans.copy(trans.variable(rvar, true), expr.translate(trans));
				}
				// return jump
				trans.branch(MacroOp.Code.JMP, "return");
			} finally {
				trans.popStatement();
			}
		} else {
			// just jump to return
			trans.branch(MacroOp.Code.JMP, "return");
		}
	}

	@Override
	public void pushUsage() {
		if (expr != null) {
			expr.pushRead();
			if (rvar.type().ref() && !rvar.type().qualified("const")) {
				// return by non-const ref -> write
				expr.pushWrite();
			}
		}
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		if (expr != null) expr.visitVariableAccesses(v);
	}
	
}
