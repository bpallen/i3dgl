package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.EvalException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.BoolInstance;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.tokens.Token;

public class DiscardStatement implements Statement {

	private Token source;
	private Scope scope;
	private Expression cond;
	
	public DiscardStatement(Token source_, Scope scope_) {
		source = source_;
		scope = scope_;
		BoolInstance cx = new BoolInstance(source, scope, false);
		cx.value(true);
		cond = cx;
	}
	
	@Override
	public Token source() {
		return source;
	}

	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public String toString() {
		return "discard[" + cond + "]";
	}
	
	@Override
	public void eval(VariableInstanceMap vars) {
		vars.infoLog().error("cannot evaluate discard statement at compile time", source);
	}
	
	@Override
	public void translate(Translator trans) {
		trans.pushStatement();
		try {
			trans.op(MacroOp.Code.DISCARD, (PrimitiveLocation) cond.translate(trans));
		} finally {
			trans.popStatement();
		}
	}
	
	public void condAnd(Expression cond2, InfoLog infolog) {
		if (cond.constexpr()) {
			cond = cond2.maybeEval(false, infolog);
		} else {
			OverloadSet o = scope.useOverload(source.withText("operator&&"), infolog);
			FunctionCall fc = o.call(infolog, source, scope, cond, cond2);
			cond = fc.maybeEval(false, infolog);
		}
	}

	@Override
	public void pushUsage() {
		cond.pushRead();
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		cond.visitVariableAccesses(v);
	}
	
}
