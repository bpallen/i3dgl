package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.lang.Function;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.names.FunctionName;
import initial3d.detail.glsl.tokens.Token;

public class FunctionDeclaration implements Declaration {

	private FunctionName name;
	private Function defn;
	
	public FunctionDeclaration(FunctionName name_, Function defn_) {
		name = name_;
		defn = defn_;
	}
	
	@Override
	public Token source() {
		return name.source();
	}
	
	@Override
	public Scope scope() {
		return name.scope();
	}
	
	public FunctionName name() {
		return name;
	}
	
	public Function definition() {
		return defn;
	}
	
	@Override
	public String toString() {
		if (defn == null) {
			return "declare[" + name.text() + "]";
		} else {
			return "define[" + name.text() + " -> " + defn + "]";
		}
	}

}
