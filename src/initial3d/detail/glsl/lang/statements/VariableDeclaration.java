package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Struct;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.BlockName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;

public class VariableDeclaration implements Declaration {

	private VariableName name;
	
	public VariableDeclaration(VariableName name_) {
		name = name_;
	}

	@Override
	public Token source() {
		return name.source();
	}
	
	@Override
	public Scope scope() {
		return name.scope();
	}
	
	public VariableName name() {
		return name;
	}
	
	@Override
	public void eval(VariableInstanceMap vars) {
		if (name.type().ref()) {
			// bind reference
			Instance x = name.definition().initExpr().eval(vars);
			vars.put(name, x);
		} else if (name.constexpr()) {
			// bind constant
			vars.put(name, name.definition().initVal());
		} else if (name.defined() && name.definition().initExpr() != null) {
			// eval and copy
			Instance x = name.definition().initExpr().eval(vars).copy(vars);
			vars.put(name, x);
		} else {
			// new instance
			Instance x = name.type().definition().instantiate(name.source(), name.type(), name.scope(), true);
			vars.put(name, x);
		}
	}
	
	@Override
	public void translate(Translator trans) {
		if (name.type().ref()) {
			// bind reference
			trans.pushStatement();
			try {
				trans.bindReference(name, name.definition().initExpr().translate(trans));
			} finally {
				trans.popStatement();
			}
		} else if (name.constexpr()) {
			// bind constant
			trans.bindConstant(name, name.definition().initVal());
		} else if (name.defined() && name.definition().initExpr() != null) {
			// eval and copy
			trans.pushStatement();
			try {
				trans.copy(trans.variable(name), name.definition().initExpr().translate(trans));
			} finally {
				trans.popStatement();
			}
		} else {
			// unitialized, will be allocated lazily
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("declare[");
		sb.append(name.text());
		if (name.constexpr()) {
			sb.append(" = ");
			sb.append(name.definition().initVal());
		} else if (name.defined() && name.definition().initExpr() != null) {
			sb.append(" = ");
			sb.append(name.definition().initExpr());
		}
		if (name.type() instanceof ArrayTypeName) {
			sb.append(" (size=");
			sb.append(((ArrayTypeName) name.type()).size());
			sb.append(")");
		}
		sb.append("]");
		if (name.defined() && name.definition().layout() != null) {
			sb.append(" ");
			sb.append(name.definition().layout());
		}
		if (name.type().defined() && name.type().definition() instanceof Struct) {
			// if a block instance, also show the block struct (because otherwise we'll never see it)
			Struct s = (Struct) name.type().definition();
			BlockName b = s.block();
			if (b != null) {
				sb.append("\n");
				sb.append(b);
				sb.append("\n");
				sb.append(s);
			}
		}
		return sb.toString();
	}
	
}
