package initial3d.detail.glsl.lang.statements;

import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.tokens.Token;

public class ExpressionStatement implements Statement {

	private Expression expr;
	
	public ExpressionStatement(Expression expr_) {
		expr = expr_;
	}
	
	@Override
	public Token source() {
		return expr.source();
	}
	
	@Override
	public Scope scope() {
		return expr.scope();
	}

	public Expression expression() {
		return expr;
	}
	
	@Override
	public String toString() {
		return "statement[" + expr + "]";
	}

	@Override
	public void eval(VariableInstanceMap vars) {
		expr.eval(vars);
	}
	
	@Override
	public void translate(Translator trans) {
		trans.pushStatement();
		try {
			expr.translate(trans);
		} finally {
			trans.popStatement();
		}
	}

	@Override
	public void pushUsage() {
		expr.pushRead();
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		expr.visitVariableAccesses(v);
	}
	
}
