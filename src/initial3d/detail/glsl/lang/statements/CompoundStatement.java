package initial3d.detail.glsl.lang.statements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.tokens.Token;

public class CompoundStatement implements Statement {

	private Token source;
	private Scope scope;
	private List<Statement> statements = new ArrayList<>();
	
	public CompoundStatement(Token source_, Scope scope_) {
		source = source_;
		scope = scope_;
	}
	
	@Override
	public Token source() {
		return source;
	}
	
	@Override
	public Scope scope() {
		return scope;
	}

	public void add(Statement s) {
		statements.add(s);
	}
	
	public int size() {
		return statements.size();
	}
	
	public Statement get(int i) {
		return statements.get(i);
	}
	
	public List<Statement> statements() {
		return Collections.unmodifiableList(statements);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (Statement s : statements) {
			if (!first) sb.append("\n");
			sb.append(s);
			first = false;
		}
		return sb.toString();
	}

	@Override
	public void eval(VariableInstanceMap vars) {
		for (Statement s : statements) {
			s.eval(vars);
		}
	}
	
	@Override
	public void translate(Translator trans) {
		for (Statement s : statements) {
			s.translate(trans);
		}
	}

	@Override
	public void pushUsage() {
		for (Statement s : statements) {
			s.pushUsage();
		}
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		for (Statement s : statements) {
			s.visitVariableAccesses(v);
		}
	}
	
}
