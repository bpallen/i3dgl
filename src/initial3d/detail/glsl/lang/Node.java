package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.tokens.Token;

public interface Node {

	public Token source();
	
	public Scope scope();
	
	default void acceptVisitor(NodeVisitor visitor) {
		visitor.visit(this);
	}
	
}
