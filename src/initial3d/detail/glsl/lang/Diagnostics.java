package initial3d.detail.glsl.lang;

import initial3d.detail.SimpleCloneable;
import initial3d.detail.glsl.tokens.Token;

public class Diagnostics extends SimpleCloneable<Diagnostics> {

	public Scope parsedump_scope = null;
	
	public void diag_parsedump(Parser p, Token t0, boolean b) {
		parsedump_scope = b ? p.currentScope() : null;
		if (b) {
			p.input().infoLog().warning("Beginning parse dump in scope", t0);
			if (parsedump_scope.owner() != null) {
				p.input().infoLog().detail("See scope owner", parsedump_scope.owner().source());
			}
		}
	}
}
