package initial3d.detail.glsl.lang;

import static initial3d.detail.glsl.intermediate.MacroOp.Code.*;

import initial3d.detail.bytecode.MethodBuilder;

public class BuiltinFunctions {

	private BuiltinFunctions() {
		throw new AssertionError("not constructible");
	}
	
	@ImplementBuiltin(name="__ctor_int", rettype="int", paramtypes={"uint"}, opcode=MOV)
	public static int ctorIntu(int x) {
		return x;
	}
	
	@ImplementBuiltin(name="__ctor_int", rettype="int", paramtypes={"float"}, opcode=F2I)
	public static int ctorIntf(float x) {
		return (int) x;
	}
	
	@ImplementBuiltin(name="__ctor_uint", rettype="uint", paramtypes={"int"}, opcode=MOV)
	public static int ctorUInti(int x) {
		return x;
	}

	@ImplementBuiltin(name="__ctor_uint", rettype="uint", paramtypes={"float"}, opcode=F2U)
	public static int ctorUIntf(float x) {
		// this could be done better?
		return (int) x;
	}
	
	@ImplementBuiltin(name="__ctor_float", rettype="float", paramtypes={"int"}, opcode=I2F)
	public static float ctorFloati(int x) {
		return x;
	}
	
	@ImplementBuiltin(name="__ctor_float", rettype="float", paramtypes={"uint"}, opcode=U2F)
	public static float ctorFloatu(int x) {
		// this could be done better?
		return x;
	}
	
	@ImplementBuiltin(name="operator==", rettype="bool", paramtypes={"bool", "bool"}, qualifiers={"__arith"}, opcode=BCMPEQ)
	public static boolean opEqb(boolean a, boolean b) {
		return a == b;
	}
	
	@ImplementBuiltin(name="operator!=", rettype="bool", paramtypes={"bool", "bool"}, qualifiers={"__arith"}, opcode=BCMPNE)
	public static boolean opNeb(boolean a, boolean b) {
		return a != b;
	}
	
	@ImplementBuiltin(name="operator!", rettype="bool", paramtypes={"bool"}, opcode=BNOT)
	public static boolean opNotb(boolean a) {
		return !a;
	}
	
	@ImplementBuiltin(name="operator&&", rettype="bool", paramtypes={"bool", "bool"}, opcode=BAND)
	public static boolean opAndb(boolean a, boolean b) {
		return a && b;
	}
	
	@ImplementBuiltin(name="operator||", rettype="bool", paramtypes={"bool", "bool"}, opcode=BOR)
	public static boolean opOrb(boolean a, boolean b) {
		return a || b;
	}
	
	@ImplementBuiltin(name="operator^^", rettype="bool", paramtypes={"bool", "bool"}, opcode=BXOR)
	public static boolean opXorb(boolean a, boolean b) {
		return a != b;
	}
	
	@ImplementBuiltin(name="operator==", rettype="bool", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=ICMPEQ)
	public static boolean opEqi(int a, int b) {
		return a == b;
	}
	
	@ImplementBuiltin(name="operator!=", rettype="bool", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=ICMPNE)
	public static boolean opNei(int a, int b) {
		return a != b;
	}
	
	@ImplementBuiltin(name="operator<", rettype="bool", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=ICMPLT)
	public static boolean opLti(int a, int b) {
		return a < b;
	}
	
	@ImplementBuiltin(name="operator<=", rettype="bool", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=ICMPLE)
	public static boolean opLei(int a, int b) {
		return a <= b;
	}
	
	@ImplementBuiltin(name="operator>", rettype="bool", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=ICMPGT)
	public static boolean opGti(int a, int b) {
		return a > b;
	}
	
	@ImplementBuiltin(name="operator>=", rettype="bool", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=ICMPGE)
	public static boolean opGei(int a, int b) {
		return a >= b;
	}
	
	@ImplementBuiltin(name="operator~", rettype="int", paramtypes={"int"}, opcode=INOT)
	public static int opBitNoti(int a) {
		return ~a;
	}
	
	@ImplementBuiltin(name="operator-", rettype="int", paramtypes={"int"}, opcode=INEG)
	public static int opNegi(int a) {
		return -a;
	}
	
	@ImplementBuiltin(name="operator+", rettype="int", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=IADD)
	public static int opAddi(int a, int b) {
		return a + b;
	}
	
	@ImplementBuiltin(name="operator-", rettype="int", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=ISUB)
	public static int opSubi(int a, int b) {
		return a - b;
	}
	
	@ImplementBuiltin(name="operator*", rettype="int", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=IMUL)
	public static int opMuli(int a, int b) {
		return a * b;
	}
	
	@ImplementBuiltin(name="operator/", rettype="int", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=IDIV)
	public static int opDivi(int a, int b) {
		return a / b;
	}
	
	@ImplementBuiltin(name="operator%", rettype="int", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=IREM)
	public static int opModi(int a, int b) {
		return a % b;
	}
	
	@ImplementBuiltin(name="operator<<", rettype="int", paramtypes={"int", "int"}, opcode=ISL)
	public static int opSli(int a, int b) {
		return a << b;
	}
	
	@ImplementBuiltin(name="operator>>", rettype="int", paramtypes={"int", "int"}, opcode=ISR)
	public static int opSri(int a, int b) {
		return a >> b;
	}
	
	@ImplementBuiltin(name="operator&", rettype="int", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=IAND)
	public static int opBitAndi(int a, int b) {
		return a & b;
	}
	
	@ImplementBuiltin(name="operator|", rettype="int", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=IOR)
	public static int opBitOri(int a, int b) {
		return a | b;
	}
	
	@ImplementBuiltin(name="operator^", rettype="int", paramtypes={"int", "int"}, qualifiers={"__arith"}, opcode=IXOR)
	public static int opBitXori(int a, int b) {
		return a ^ b;
	}
	
	@ImplementBuiltin(name="operator==", rettype="bool", paramtypes={"uint", "uint"}, qualifiers={"__arith"}, opcode=UCMPEQ)
	public static boolean opEqu(int a, int b) {
		return a == b;
	}
	
	@ImplementBuiltin(name="operator!=", rettype="bool", paramtypes={"uint", "uint"}, qualifiers={"__arith"}, opcode=UCMPNE)
	public static boolean opNeu(int a, int b) {
		return a != b;
	}
	
	@ImplementBuiltin(name="operator<", rettype="bool", paramtypes={"uint", "uint"}, qualifiers={"__arith"})
	public static boolean opLtu(int a, int b) {
		return Integer.compareUnsigned(a, b) < 0;
	}
	
	@ImplementBuiltin(name="operator<=", rettype="bool", paramtypes={"uint", "uint"}, qualifiers={"__arith"})
	public static boolean opLeu(int a, int b) {
		return Integer.compareUnsigned(a, b) <= 0;
	}
	
	@ImplementBuiltin(name="operator>", rettype="bool", paramtypes={"uint", "uint"}, qualifiers={"__arith"})
	public static boolean opGtu(int a, int b) {
		return Integer.compareUnsigned(a, b) > 0;
	}
	
	@ImplementBuiltin(name="operator>=", rettype="bool", paramtypes={"uint", "uint"}, qualifiers={"__arith"})
	public static boolean opGeu(int a, int b) {
		return Integer.compareUnsigned(a, b) >= 0;
	}
	
	@ImplementBuiltin(name="operator~", rettype="uint", paramtypes={"uint"}, opcode=UNOT)
	public static int opBitNotu(int a) {
		return ~a;
	}
	
	@ImplementBuiltin(name="operator-", rettype="uint", paramtypes={"uint"}, opcode=UNEG)
	public static int opNegu(int a) {
		return -a;
	}
	
	@ImplementBuiltin(name="operator+", rettype="uint", paramtypes={"uint", "uint"}, qualifiers={"__arith"}, opcode=UADD)
	public static int opAddu(int a, int b) {
		return a + b;
	}
	
	@ImplementBuiltin(name="operator-", rettype="uint", paramtypes={"uint", "uint"}, qualifiers={"__arith"}, opcode=USUB)
	public static int opSubu(int a, int b) {
		return a - b;
	}
	
	@ImplementBuiltin(name="operator*", rettype="uint", paramtypes={"uint", "uint"}, qualifiers={"__arith"}, opcode=UMUL)
	public static int opMulu(int a, int b) {
		return a * b;
	}
	
	@ImplementBuiltin(name="operator/", rettype="uint", paramtypes={"uint", "uint"}, qualifiers={"__arith"}, opcode=UDIV)
	public static int opDivu(int a, int b) {
		return Integer.divideUnsigned(a, b);
	}
	
	@ImplementBuiltin(name="operator%", rettype="uint", paramtypes={"uint", "uint"}, qualifiers={"__arith"}, opcode=UREM)
	public static int opModu(int a, int b) {
		return Integer.remainderUnsigned(a, b);
	}
	
	@ImplementBuiltin(name="operator<<", rettype="uint", paramtypes={"uint", "uint"}, opcode=USL)
	public static int opSlu(int a, int b) {
		return a << b;
	}
	
	@ImplementBuiltin(name="operator>>", rettype="uint", paramtypes={"uint", "uint"}, opcode=USR)
	public static int opSru(int a, int b) {
		return a >>> b;
	}
	
	@ImplementBuiltin(name="operator&", rettype="uint", paramtypes={"uint", "uint"}, qualifiers={"__arith"}, opcode=UAND)
	public static int opBitAndu(int a, int b) {
		return a & b;
	}
	
	@ImplementBuiltin(name="operator|", rettype="uint", paramtypes={"uint", "uint"}, qualifiers={"__arith"}, opcode=UOR)
	public static int opBitOru(int a, int b) {
		return a | b;
	}
	
	@ImplementBuiltin(name="operator^", rettype="uint", paramtypes={"uint", "uint"}, qualifiers={"__arith"}, opcode=UXOR)
	public static int opBitXoru(int a, int b) {
		return a ^ b;
	}
	
	@ImplementBuiltin(name="operator==", rettype="bool", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FCMPEQ)
	public static boolean opEqf(float a, float b) {
		return a == b;
	}
	
	@ImplementBuiltin(name="operator!=", rettype="bool", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FCMPNE)
	public static boolean opNef(float a, float b) {
		return a != b;
	}
	
	@ImplementBuiltin(name="operator<", rettype="bool", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FCMPLT)
	public static boolean opLtf(float a, float b) {
		return a < b;
	}
	
	@ImplementBuiltin(name="operator<=", rettype="bool", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FCMPLE)
	public static boolean opLef(float a, float b) {
		return a <= b;
	}
	
	@ImplementBuiltin(name="operator>", rettype="bool", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FCMPGT)
	public static boolean opGtf(float a, float b) {
		return a > b;
	}
	
	@ImplementBuiltin(name="operator>=", rettype="bool", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FCMPGE)
	public static boolean opGef(float a, float b) {
		return a >= b;
	}
	
	@ImplementBuiltin(name="operator-", rettype="float", paramtypes={"float"}, opcode=FNEG)
	public static float opNegf(float a) {
		return -a;
	}
	
	@ImplementBuiltin(name="operator+", rettype="float", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FADD)
	public static float opAddf(float a, float b) {
		return a + b;
	}
	
	@ImplementBuiltin(name="operator-", rettype="float", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FSUB)
	public static float opSubf(float a, float b) {
		return a - b;
	}
	
	@ImplementBuiltin(name="operator*", rettype="float", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FMUL)
	public static float opMulf(float a, float b) {
		return a * b;
	}
	
	@ImplementBuiltin(name="operator/", rettype="float", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FDIV)
	public static float opDivf(float a, float b) {
		return a / b;
	}
	
	@ImplementBuiltin(name="operator%", rettype="float", paramtypes={"float", "float"}, qualifiers={"__arith"}, opcode=FREM)
	public static float opModf(float a, float b) {
		return a % b;
	}
	
	@ImplementBuiltin(name="min", rettype="int", paramtypes={"int", "int"}, opcode=IMIN)
	public static int mini(int a, int b) {
		return Math.min(a, b);
	}
	
	@ImplementBuiltin(name="max", rettype="int", paramtypes={"int", "int"}, opcode=IMAX)
	public static int maxi(int a, int b) {
		return Math.max(a, b);
	}
	
	@ImplementBuiltin(name="min", rettype="uint", paramtypes={"uint", "uint"}, opcode=UMIN)
	public static int minu(int a, int b) {
		return MethodBuilder.minUnsigned(a, b);
	}
	
	@ImplementBuiltin(name="max", rettype="uint", paramtypes={"uint", "uint"}, opcode=UMAX)
	public static int maxu(int a, int b) {
		return MethodBuilder.maxUnsigned(a, b);
	}
	
	@ImplementBuiltin(name="min", rettype="float", paramtypes={"float", "float"}, opcode=FMIN)
	public static float minf(float a, float b) {
		return Math.min(a, b);
	}
	
	@ImplementBuiltin(name="max", rettype="float", paramtypes={"float", "float"}, opcode=FMAX)
	public static float maxf(float a, float b) {
		return Math.max(a, b);
	}
	
	@ImplementBuiltin(name="abs", rettype="int", paramtypes={"int"}, opcode=IABS)
	public static int absi(int a) {
		return Math.abs(a);
	}
	
	@ImplementBuiltin(name="abs", rettype="uint", paramtypes={"uint"}, opcode=UABS)
	public static int absu(int a) {
		return a;
	}
	
	@ImplementBuiltin(name="abs", rettype="float", paramtypes={"float"}, opcode=FABS)
	public static float absf(float a) {
		return Math.abs(a);
	}
	
	
	
	
}
