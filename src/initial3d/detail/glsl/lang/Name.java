package initial3d.detail.glsl.lang;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.names.Qualifier;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenBuilder;

/**
 * the scope of a name refers to its _use_.
 * there can be many name instances referring to the same declaration.
 * names must be comparable without being declared (otherwise finding declarations is impossible).
 * scope must factor into name equality (consider 'int foo' at unrelated scopes).
 * therefore, even if names _used_ in different scopes refer to the same declaration, they are not equal.
 * 
 * names that are used to specify members (of a struct variable etc) should always claim
 * to be in the member scope of that struct, else we can't find their definitions.
 * 
 * @author ben
 *
 * @param <T>
 */
@SuppressWarnings("rawtypes")
public abstract class Name<T extends Name> implements Node {

	protected Token source;
	protected Scope scope;
	protected Map<String, Qualifier> qualifiers = new HashMap<>();
	
	public Name(Token source_, Scope scope_) {
		source = source_;
		scope = scope_;
	}
	
	protected abstract T copyImpl(Token source_, Scope scope_);
	
	public T copy(Token source_, Scope scope_) {
		// need to ensure the contents of the token don't change
		// as many names take their text() from the token directly
		T name = copyImpl(source_.withText(source().text()), scope_);
		name.qualifyAll(qualifiers());
		return name;
	}
	
	public T copy(String s) {
		TokenBuilder tb = new TokenBuilder(source);
		tb.text(s);
		return copy(tb.toToken(), scope);
	}
	
	public T copy() {
		return copy(source, scope);
	}
	
	public boolean declared() {
		return declaration() != null;
	}
	
	public boolean defined() {
		return definition() != null;
	}
	
	public void declare(InfoLog infolog) {
		scope.declare(this, infolog);
	}
	
	public void define(Node n, InfoLog infolog) {
		scope.define(this, n, infolog);
	}
	
	public Name declaration() {
		return scope.declaration(text());
	}
	
	public Node definition() {
		return scope.definition(this);
	}
	
	@Override
	public Token source() {
		return source;
	}
	
	/**
	 * the scope that this name instance appeared in, not the scope of the actual declaration.
	 */
	@Override
	public Scope scope() {
		return scope;
	}
	
	public Scope declScope() {
		return declaration().scope();
	}
	
	public String plainText() {
		return source == null ? null : source.text();
	}
	
	public String text() {
		return plainText();
	}
	
	public T qualify(String qs) {
		return qualify(new Qualifier(source.withText(qs), scope));
	}
	
	public T qualify(Qualifier q) {
		if (q == null) return (T) this;
		qualifiers.put(q.text(), q);
		return (T) this;
	}
	
	public T qualifyAll(Iterable<Qualifier> ql) {
		for (Qualifier q : ql) {
			qualify(q);
		}
		return (T) this;
	}
	
	public Qualifier dequalify(String qt) {
		return qualifiers.remove(qt);
	}
	
	public List<Qualifier> dequalifyAll() {
		List<Qualifier> qs = new ArrayList<>(qualifiers());
		qualifiers.clear();
		return qs;
	}
	
	public boolean qualified(String qt) {
		return qualifiers.containsKey(qt);
	}
	
	public Qualifier qualifier(String qt) {
		return qualifiers.get(qt);
	}
	
	public Collection<Qualifier> qualifiers() {
		return Collections.unmodifiableCollection(qualifiers.values());
	}
	
	public T withoutQualifiers() {
		T t = copy();
		t.qualifiers.clear();
		return t;
	}
	
	public T withQualifier(Qualifier q) {
		T t = copy();
		t.qualify(q);
		return t;
	}
	
	public T withoutQualifier(String qt) {
		T t = copy();
		t.dequalify(qt);
		return t;
	}
	
	/**
	 * only useful on declarations
	 * 
	 * @return
	 */
	public Qualifier interfaceQualifier() {
		// note that in and out are only interface qualifiers at global scope
		// and that only things at global scope can have interface qualifiers
		// TODO it is legal to repeat interface qualifiers on members of blocks...
		if (!scope.global()) return null;
		Qualifier q = qualifier("uniform");
		if (q != null) return q;
		q = qualifier("in");
		if (q != null) return q;
		q = qualifier("out");
		if (q != null) return q;
		return null;
	}
	
	/**
	 * only useful on declarations
	 * 
	 * @return
	 */
	public boolean builtin() {
		return scope == BuiltinScope.instance;
	}
	
	@Override
	public void acceptVisitor(NodeVisitor visitor) {
		visitor.visit(this);
	}
	
	public boolean declEquals(Name other) {
		return declaration().equals(other.declaration());
	}
	
	public boolean declQualEquals(Name other) {
		return declEquals(other) && qualifiers.equals(other.qualifiers);
	}
	
	/**
	 * Equality includes the scope the name appeared in, and is therefore
	 * mostly useful when applied to {@link #declaration()}.
	 */
	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
		if (this == other) return true;
		if (getClass() != other.getClass()) return false;
		return text().equals(((Name) other).text())
			&& scope.equals(((Name) other).scope())
			&& qualifiers.equals(((Name) other).qualifiers);
	}
	
	@Override
	public int hashCode() {
		return text().hashCode() ^ (scope.hashCode() * 31);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append('[');
		sb.append(text());
		sb.append(']');
		sb.append('[');
		for (Qualifier q : qualifiers.values()) {
			sb.append(q);
			sb.append(',');
		}
		if (!qualifiers.isEmpty()) sb.setLength(sb.length() - 1);
		sb.append(']');
		return sb.toString();
	}
}
