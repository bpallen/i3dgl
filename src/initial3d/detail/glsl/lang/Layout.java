package initial3d.detail.glsl.lang;

import java.util.HashMap;

import initial3d.detail.SimpleCloneable;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.names.LayoutQualifier;
import initial3d.detail.glsl.tokens.Token;

/**
 * <p>
 * Note that there are separate Layout instances for in/out/uniform (at scope, that layouts for in/out/uniform variables
 * inherit from). some members are shared between in/out etc.
 * </p>
 * 
 * <p>
 * Layout rules for <code>std140</code>:
 * <ol>
 * <li>If the member is a scalar consuming N basic machine units, the base alignment is N.</li>
 * <li>If the member is a two- or four-component vector with components consuming N basic machine units, the base
 * alignment is 2N or 4N, respectively.</li>
 * <li>If the member is a three-component vector with components consuming N basic machine units, the base alignment is
 * 4N.</li>
 * <li>If the member is an array of scalars or vectors, the base alignment and array stride are set to match the base
 * alignment of a single array element, according to rules (1), (2), and (3), and rounded up to the base alignment of a
 * vec4. The array may have padding at the end; the base offset of the member following the array is rounded up to the
 * next multiple of the base alignment.</li>
 * <li>If the member is a column-major matrix with C columns and R rows, the matrix is stored identically to an array of
 * C column vectors with R components each, according to rule (4).</li>
 * <li>If the member is an array of S column-major matrices with C columns and R rows, the matrix is stored identically
 * to a row of S � C column vectors with R components each, according to rule (4).</li>
 * <li>If the member is a row-major matrix with C columns and R rows, the matrix is stored identically to an array of R
 * row vectors with C components each, according to rule (4).</li>
 * <li>If the member is an array of S row-major matrices with C columns and R rows, the matrix is stored identically to
 * a row of S � R row vectors with C components each, according to rule (4).</li>
 * <li>If the member is a structure, the base alignment of the structure is N, where N is the largest base alignment
 * value of any of its members, and rounded up to the base alignment of a vec4. The individual members of this
 * substructure are then assigned offsets by applying this set of rules recursively, where the base offset of the first
 * member of the sub-structure is equal to the aligned offset of the structure. The structure may have padding at the
 * end; the base offset of the member following the sub-structure is rounded up to the next multiple of the base
 * alignment of the structure.</li>
 * <li>If the member is an array of S structures, the S elements of the array are laid out in order, according to rule
 * (9).</li>
 * </ol>
 * </p>
 * 
 * @author ben
 *
 */
public class Layout extends SimpleCloneable<Layout> {

	public static interface ParameterHandler {
		void execute(InfoLog infolog, Layout l, Token t, Instance x);
	}
	
	public static interface ParameterHandlerInt {
		void execute(InfoLog infolog, Layout l, Token t, int x);
	}
	
	private static HashMap<String, ParameterHandler> param_handlers = new HashMap<>();

	public static void addLayoutParameter(String s, ParameterHandler h) {
		param_handlers.put(s, h);
	}
	
	public static void addLayoutParameterInt(String s, ParameterHandlerInt h) {
		addLayoutParameter(s, (InfoLog infolog, Layout l, Token t, Instance x) -> {
			try {
				h.execute(infolog, l, t, (Integer) x.javaEquivalent());
			} catch (ClassCastException | NullPointerException e) {
				infolog.error("layout parameter '" + s + "' must have int/uint value", t);
			}
		});
	}
	
	static {
		addLayoutParameter("__special", (InfoLog infolog, Layout l, Token t, Instance x) -> { l.special = true; l.packing = Packing.NONE; });
		addLayoutParameterInt("location", (InfoLog infolog, Layout l, Token t, int x) -> l.location = x);
		addLayoutParameterInt("max_vertices", (InfoLog infolog, Layout l, Token t, int x) -> l.max_vertices = x);
		addLayoutParameterInt("index", (InfoLog infolog, Layout l, Token t, int x) -> l.index = x);
		addLayoutParameter("origin_upper_left", (InfoLog infolog, Layout l, Token t, Instance x) -> l.origin_upper_left = true);
		addLayoutParameter("pixel_center_integer", (InfoLog infolog, Layout l, Token t, Instance x) -> l.pixel_center_integer = true);
		addLayoutParameter("points", (InfoLog infolog, Layout l, Token t, Instance x) -> l.prim_mode = Layout.PrimitiveMode.POINTS);
		addLayoutParameter("lines", (InfoLog infolog, Layout l, Token t, Instance x) -> l.prim_mode = Layout.PrimitiveMode.LINES);
		addLayoutParameter("lines_adjacency", (InfoLog infolog, Layout l, Token t, Instance x) -> l.prim_mode = Layout.PrimitiveMode.LINES_ADJACENCY);
		addLayoutParameter("triangles", (InfoLog infolog, Layout l, Token t, Instance x) -> l.prim_mode = Layout.PrimitiveMode.TRIANGLES);
		addLayoutParameter("triangles_adjacency", (InfoLog infolog, Layout l, Token t, Instance x) -> l.prim_mode = Layout.PrimitiveMode.TRIANGLES_ADJACENCY);
		addLayoutParameter("line_strip", (InfoLog infolog, Layout l, Token t, Instance x) -> l.prim_mode = Layout.PrimitiveMode.LINE_STRIP);
		addLayoutParameter("triangle_strip", (InfoLog infolog, Layout l, Token t, Instance x) -> l.prim_mode = Layout.PrimitiveMode.TRIANGLE_STRIP);
		addLayoutParameter("shared", (InfoLog infolog, Layout l, Token t, Instance x) -> l.packing = Layout.Packing.SHARED);
		addLayoutParameter("packed", (InfoLog infolog, Layout l, Token t, Instance x) -> l.packing = Layout.Packing.PACKED);
		addLayoutParameter("std140", (InfoLog infolog, Layout l, Token t, Instance x) -> l.packing = Layout.Packing.STD140);
		addLayoutParameter("__attribute", (InfoLog infolog, Layout l, Token t, Instance x) -> l.packing = Layout.Packing.ATTRIBUTE);
		addLayoutParameter("__fragdata", (InfoLog infolog, Layout l, Token t, Instance x) -> l.packing = Layout.Packing.FRAGDATA);
		addLayoutParameter("row_major", (InfoLog infolog, Layout l, Token t, Instance x) -> l.matrix_order = Layout.MatrixOrder.ROW_MAJOR);
		addLayoutParameter("column_major", (InfoLog infolog, Layout l, Token t, Instance x) -> l.matrix_order = Layout.MatrixOrder.COLUMN_MAJOR);
	}
	
	public static enum PrimitiveMode {
		NONE, POINTS, LINES, LINES_ADJACENCY, TRIANGLES, TRIANGLES_ADJACENCY, LINE_STRIP, TRIANGLE_STRIP
	}
	
	public static enum Packing {
		NONE, SHARED, PACKED, STD140, ATTRIBUTE, FRAGDATA
	}
	
	public static enum MatrixOrder {
		NONE, ROW_MAJOR, COLUMN_MAJOR
	}
	
	public static enum Interpolation {
		NONE, FLAT, SMOOTH, NOPERSPECTIVE
	}
	
	// special (builtin) variables (e.g. gl_VertexID)
	public boolean special = false;
	
	// vert in, frag out
	public int location = -1;
	
	// geo out
	public int max_vertices = -1;
	
	// frag out
	public int index = -1;
	
	// frag input
	public boolean origin_upper_left = false;
	public boolean pixel_center_integer = false;
	
	// geo in/out
	public PrimitiveMode prim_mode = PrimitiveMode.NONE;
	
	// uniform blocks
	public Packing packing = Packing.NONE;
	public MatrixOrder matrix_order = MatrixOrder.NONE;
	
	// interpolation
	// this isn't specified by layout(...), but it might as well be
	public Interpolation interp = Interpolation.NONE;
	
	public void apply(LayoutQualifier q, InfoLog infolog) {
		for (LayoutQualifier.Param p : q.params()) {
			ParameterHandler h = param_handlers.get(p.source.text());
			if (h != null) {
				h.execute(infolog, this, p.source, p.value);
			} else {
				infolog.error("unknown layout parameter", p.source);
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("layout[");
		if (location >= 0) sb.append("location=" + location + ", ");
		if (max_vertices >= 0) sb.append("max_vertices=" + max_vertices + ", ");
		if (index >= 0) sb.append("index=" + index + ", ");
		if (origin_upper_left) sb.append("origin_upper_left, ");
		if (pixel_center_integer) sb.append("pixel_center_integer, ");
		if (prim_mode != PrimitiveMode.NONE) sb.append(prim_mode.toString() + ", ");
		if (packing != Packing.NONE) sb.append(packing.toString() + ", ");
		if (matrix_order != MatrixOrder.NONE) sb.append(matrix_order.toString() + ", ");
		if (sb.length() > 7) sb.setLength(sb.length() - 2);
		sb.append("]");
		return sb.toString();
	}
	
	/**
	 * @param type
	 * @return Absolute alignment for a primitive.
	 */
	public int primitiveAlign(BuiltinType type) {
		if (packing == Packing.ATTRIBUTE || packing == Packing.FRAGDATA) return 16;
		return type.allocSize(this);
	}
	
	/**
	 * @param type
	 * @param vsize
	 * @return Absolute alignment for a vector.
	 */
	public int vectorAlign(BuiltinType type, int vsize) {
		if (packing == Packing.ATTRIBUTE || packing == Packing.FRAGDATA) return 16;
		int ma = primitiveAlign(type);
		if (packing == Packing.STD140) {
			switch (vsize) {
			case 0:
				return 1;
			case 1:
				return ma;
			case 2:
				return ma * 2;
			default:
				return ma * 4;
			}
		} else {
			return ma;
		}
	}
	
	/**
	 * @param type
	 * @return Absolute alignment for a vector.
	 */
	public int vectorAlign(Vector type) {
		return vectorAlign(type.valType(), type.vecSize());
	}
	
	/**
	 * @param type
	 * @return Absolute alignment for a matrix.
	 */
	public int matrixAlign(Matrix type) {
		if (packing == Packing.STD140) {
			if (matrix_order == MatrixOrder.ROW_MAJOR) {
				return Math.max(arrayMinAlign(), vectorAlign(type.valType(), type.matCols()));
			} else {
				return Math.max(arrayMinAlign(), vectorAlign(type.valType(), type.matRows()));
			}
		} else {
			return vectorAlign(type.vecType());
		}
	}
	
	/**
	 * @return Minimum alignment for arrays, and their members.
	 */
	public int arrayMinAlign() {
		switch (packing) {
		case STD140:
		case ATTRIBUTE:
		case FRAGDATA:
			return 16;
		default:
			return 4;
		}
	}
	
	/**
	 * @return Minimum alignment for structs (but not their members).
	 */
	public int structMinAlign() {
		switch (packing) {
		case STD140:
		case ATTRIBUTE:
		case FRAGDATA:
			return 16;
		default:
			return 4;
		}
	}
	
}









