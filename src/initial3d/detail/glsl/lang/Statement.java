package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.expressions.VariableAccess;

public interface Statement extends Node {

	public void eval(VariableInstanceMap vars);
	
	public void translate(Translator trans);
	
	public void pushUsage();
	
	public void visitVariableAccesses(VariableAccess.Visitor v);
	
}
