package initial3d.detail.glsl.lang;

import java.lang.reflect.Method;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.names.FunctionName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.lang.statements.EmptyStatement;

public class BuiltinFunction extends Function {
	
	public static class MethodEvaluator implements FunctionEvaluator {

		private Method method;
		private TypeName rettype;
		
		public MethodEvaluator(Method method_, TypeName rettype_) {
			method = method_;
			rettype = rettype_;
		}
		
		@Override
		public Instance eval(VariableInstanceMap vars, Instance ...actual_params) {
			Instance ret = rettype.definition().instantiate(BuiltinScope.builtinToken("_"), rettype, vars.currentScope(), false);
			Object[] jparams = new Object[actual_params.length];
			for (int i = 0; i < jparams.length; i++) {
				jparams[i] = actual_params[i].javaEquivalent();
			}
			try {
				ret.javaEquivalent(method.invoke(null, jparams));
			} catch (Exception e) {
				vars.infoLog().internalError("error invoking java method " + method.getName() + ": " + e.getMessage());
			}
			return ret;
		}
		
	}
	
	public static class DefaultTranslator implements FunctionTranslator {

		private BuiltinFunction func;
		private MacroOp.Code opcode;
		
		public DefaultTranslator(BuiltinFunction func_, MacroOp.Code opcode_) {
			func = func_;
			opcode = opcode_;
		}
		
		@Override
		public Location translate(Translator trans, Location... actual_params) {
			// op args, return loc as first
			PrimitiveLocation[] opargs = new PrimitiveLocation[actual_params.length + 1];
			opargs[0] = (PrimitiveLocation) trans.temporary(func.name().returnType());
			for (int i = 1; i < opargs.length; i++) {
				opargs[i] = (PrimitiveLocation) actual_params[i - 1];
			}
			// call or normal op
			if (opcode == MacroOp.Code.CALL) {
				trans.call(func, opargs);
			} else {
				trans.op(opcode, opargs);
			}
			return opargs[0];
		}
		
	}
	
	private FunctionEvaluator feval;
	private FunctionTranslator ftrans;
	private Method equiv;
	
	public BuiltinFunction(FunctionName name_, FunctionEvaluator feval_, FunctionTranslator ftrans_) {
		super(InfoLog.SYSOUT, name_, new Scope(name_.scope()), null, new VariableName[0]);
		feval = feval_;
		ftrans = ftrans_;
		body(new EmptyStatement(name_.source(), bodyScope()));
		VariableName[] params = new VariableName[name_.parameterTypes().length];
		for (int i = 0; i < params.length; i++) {
			params[i] = new VariableName(name_.source().withText("arg" + i), bodyScope(), name_.parameterTypes()[i]);
		}
		parameters(params);
	}
	
	public BuiltinFunction(FunctionName name_, Method equiv_, MacroOp.Code opcode_) {
		this(name_, new MethodEvaluator(equiv_, name_.returnType()), null);
		ftrans = new DefaultTranslator(this, opcode_);
		equiv = equiv_;
	}

	@Override
	public Instance eval(VariableInstanceMap vars, Instance ...actual_params) {
		return feval.eval(vars, actual_params);
	}
	
	@Override
	public Location translate(Translator trans, Location ...actual_params) {
		return ftrans.translate(trans, actual_params);
	}
	
	@Override
	public Method javaEquivalent() {
		return equiv;
	}

}
