package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.BuildException;

public class NodeExpectationException extends BuildException {

	private static final long serialVersionUID = 1L;
	
	public NodeExpectationException(Exception cause) {
		super(cause);
	}

}
