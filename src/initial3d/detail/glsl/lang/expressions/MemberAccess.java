package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.StructLocation;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;

public class MemberAccess implements Expression {

	private TypeName type;
	private VariableName name;
	private Expression thisexpr;
	
	public MemberAccess(VariableName name_, Expression thisexpr_) {
		assert name_.declScope().equals(thisexpr_.type().definition().memberScope());
		name = name_;
		thisexpr = thisexpr_;
		type = name.type().copy();
		// i don't _think_ ref qualification should transfer
		//type.qualify(thisexpr.type().qualifier("__ref"));
		type.qualify(thisexpr.type().qualifier("const"));
		// TODO check member access type qualifiers
	}
	
	public VariableName name() {
		return name;
	}
	
	public Expression thisExpr() {
		return thisexpr;
	}
	
	@Override
	public Token source() {
		return name.source();
	}

	@Override
	public Scope scope() {
		return thisexpr.scope();
	}

	@Override
	public TypeName type() {
		return type;
	}
	
	@Override
	public boolean lValue() {
		return thisexpr.lValue();
	}

	@Override
	public boolean constexpr() {
		return thisexpr.constexpr();
	}

	@Override
	public boolean swizzled() {
		return thisexpr.swizzled();
	}
	
	@Override
	public String toString() {
		return "member[" + name.text() + ", " + thisexpr + "]";
	}

	@Override
	public Instance eval(VariableInstanceMap vars) {
		StructInstance p = (StructInstance) thisexpr.eval(vars);
		return p.member(name);
	}
	
	@Override
	public Location translate(Translator trans) {
		StructLocation l = (StructLocation) thisexpr.translate(trans);
		return l.member(name);
	}

	@Override
	public void pushRead() {
		thisexpr.pushRead();
	}

	@Override
	public void pushWrite() {
		thisexpr.pushWrite();
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		thisexpr.visitVariableAccesses(v);
	}
	
}
