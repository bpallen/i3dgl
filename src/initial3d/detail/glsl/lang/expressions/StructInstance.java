package initial3d.detail.glsl.lang.expressions;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.StructLocation;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;

public class StructInstance extends Instance {

	private HashMap<Variable, Instance> members = new HashMap<>();
	
	public StructInstance(Token source_, TypeName type_, Scope scope_, boolean lvalue_) {
		super(source_, type_, scope_, lvalue_);
		// instantiate member vars
		for (VariableName vname : type_.definition().memberScope().localDeclaredNames(VariableName.class)) {
			members.put(vname.definition(), vname.type().definition().instantiate(source_, vname.type(), scope_, lvalue_));
		}
	}

	@Override
	public Instance baseCopy(boolean lvalue) {
		return new StructInstance(source(), type(), scope(), lvalue);
	}

	public Instance member(VariableName vname) {
		Instance x = members.get(vname.definition());
		if (x == null) throw new NullPointerException("member " + vname + " of " + type() + " was null");
		return x;
	}
	
	public Instance member(VariableName vname, Instance x) {
		return members.put(vname.definition(), x);
	}
	
	@Override
	public boolean replaceSubInstance(Instance x0, Instance x1) {
		boolean r = false;
		for (Map.Entry<Variable, Instance> me : members.entrySet()) {
			if (me.getValue() == x0) {
				me.setValue(x1);
				r = true;
			} else {
				r |= me.getValue().replaceSubInstance(x0, x1);
			}
		}
		return r;
	}
	
	@Override
	public Location translate(Translator trans) {
		return new StructLocation(trans, this);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("struct[");
		sb.append(type().text());
		sb.append("]{");
		boolean first = true;
		for (Entry<Variable, Instance> e : members.entrySet()) {
			if (!first) sb.append("; ");
			sb.append(e.getKey().name().plainText());
			sb.append("=");
			Instance x = e.getValue();
			Object o = x.javaEquivalent();
			sb.append(o == null ? x : o);
			first = false;
		}
		sb.append("}");
		return sb.toString();
	}
}
