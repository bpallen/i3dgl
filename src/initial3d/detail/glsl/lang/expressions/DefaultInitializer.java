package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.ConstantPrimitiveLocation;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

// TODO remove this
@Deprecated
public class DefaultInitializer implements Expression {

	private Token source;
	private Scope scope;
	private TypeName type;
	
	public DefaultInitializer(Token source_, Scope scope_, TypeName type_) {
		source = source_;
		scope = scope_;
		type = type_;
	}
	
	@Override
	public Token source() {
		return source;
	}
	
	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public TypeName type() {
		return type;
	}

	@Override
	public String toString() {
		return "default[" + type.text() + "]";
	}

	@Override
	public Instance eval(VariableInstanceMap vars) {
		return null;
	}
	
	@Override
	public Location translate(Translator trans) {
		return null;
	}

	@Override
	public void pushRead() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pushWrite() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		// TODO Auto-generated method stub
		
	}
	
}
