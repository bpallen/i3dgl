package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.ConstantPrimitiveLocation;
import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Type;
import initial3d.detail.glsl.tokens.Token;

public class BoolInstance extends Instance {

	private boolean value;
	
	public BoolInstance(Token source_, Scope scope_, boolean lvalue_) {
		super(source_, BuiltinScope.instance.declaredType("bool", null), scope_, lvalue_);
	}

	public BoolInstance(Instance o, boolean x) {
		this(o.source(), o.scope(), o.lValue());
		value(x);
	}
	
	public boolean value() {
		return value;
	}
	
	public void value(boolean x) {
		value = x;
	}

	@Override
	public Object javaEquivalent() {
		return value;
	}

	@Override
	public void javaEquivalent(Object o) {
		value = (Boolean) o;
	}

	@Override
	public Instance baseCopy(boolean lvalue) {
		return new BoolInstance(source(), scope(), lvalue);
	}
	
	public static BoolInstance parseBool(Token t, Scope scope) {
		BoolInstance x = new BoolInstance(t, scope, false);
		x.value(t.isText("true"));
		return x;
	}
	
	@Override
	public Location translate(Translator trans) {
		return new ConstantPrimitiveLocation(this);
	}
	
	@Override
	public String toString() {
		return "bool(" + value + ")";
	}
}
