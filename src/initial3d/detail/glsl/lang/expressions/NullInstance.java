package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

/**
 * Exists for forcing attempted eval of non-constexpr function calls
 * 
 * @author ben
 *
 */
public class NullInstance extends Instance {

	public NullInstance(Token source_, TypeName type_, Scope scope_, boolean lvalue_) {
		super(source_, type_, scope_, lvalue_);
	}

	@Override
	public Instance baseCopy(boolean lvalue) {
		return new NullInstance(source(), type(), scope(), lvalue);
	}
	
	@Override
	public Location translate(Translator trans) {
		return null;
	}
}
