package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;

public class VariableAccess implements Expression {

	public static interface Visitor {
		void visit(VariableAccess a);
	}
	
	private Variable var;
	private VariableName name;
	private boolean read, write;
	
	public VariableAccess(Variable var_, VariableName name_) {
		var = var_;
		name = name_;
	}
	
	public Variable variable() {
		return var;
	}
	
	public VariableName name() {
		return name;
	}
	
	@Override
	public Token source() {
		return name.source();
	}
	
	@Override
	public Scope scope() {
		return name.scope();
	}

	@Override
	public TypeName type() {
		return name.type();
	}
	
	@Override
	public boolean lValue() {
		return true;
	}
	
	@Override
	public boolean constexpr() {
		return var.constexpr();
	}
	
	@Override
	public boolean swizzled() {
		return false;
	}
	
	@Override
	public String toString() {
		return "var[" + name.text() + "]";
	}

	@Override
	public Instance eval(VariableInstanceMap vars) {
		return vars.get(name);
	}
	
	@Override
	public Location translate(Translator trans) {
		return trans.variable(name);
	}
	
	public boolean read() {
		return read;
	}
	
	public boolean write() {
		return write;
	}
	
	@Override
	public void pushRead() {
		read = true;
	}

	@Override
	public void pushWrite() {
		write = true;
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		v.visit(this);
	}
	
}
