package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.ConstantPrimitiveLocation;
import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Type;
import initial3d.detail.glsl.tokens.Token;

public class FloatInstance extends Instance {

	private float value;
	
	public FloatInstance(Token source_, Scope scope_, boolean lvalue_) {
		super(source_, BuiltinScope.instance.declaredType("float", null), scope_, lvalue_);
	}
	
	public FloatInstance(Instance o, float x) {
		this(o.source(), o.scope(), o.lValue());
		value(x);
	}

	public float value() {
		return value;
	}
	
	public void value(float x) {
		value = x;
	}

	@Override
	public Object javaEquivalent() {
		return value;
	}

	@Override
	public void javaEquivalent(Object o) {
		value = (Float) o;
	}

	@Override
	public Instance baseCopy(boolean lvalue) {
		return new FloatInstance(source(), scope(), lvalue);
	}

	public static FloatInstance parseFloat(Token t, Scope scope) {
		FloatInstance x = new FloatInstance(t, scope, false);
		x.value(Float.parseFloat(t.text()));
		return x;
	}
	
	@Override
	public Location translate(Translator trans) {
		return new ConstantPrimitiveLocation(this);
	}
	
	@Override
	public String toString() {
		return "float(" + value + ")";
	}
}
