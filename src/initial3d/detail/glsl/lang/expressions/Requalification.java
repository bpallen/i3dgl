package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.ConstantPrimitiveLocation;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

public class Requalification implements Expression {

	// TODO is this class needed?
	// - probably
	
	private Expression expr;
	private TypeName type;
	
	public Requalification(Expression expr_, TypeName type_) {
		expr = expr_;
		// copy qualifiers from target type so we preserve any extra
		// info on the expression's type (like array sizes)
		type = (TypeName) expr_.type().withoutQualifiers().qualifyAll(type_.qualifiers());
	}
	
	@Override
	public Token source() {
		return expr.source();
	}

	@Override
	public Scope scope() {
		return expr.scope();
	}

	@Override
	public TypeName type() {
		return type;
	}
	
	@Override
	public boolean constexpr() {
		return expr.constexpr();
	}
	
	@Override
	public boolean swizzled() {
		return expr.swizzled();
	}
	
	private Expression expression() {
		return expr;
	}

	@Override
	public Instance eval(VariableInstanceMap vars) {
		return expr.eval(vars);
	}
	
	@Override
	public Location translate(Translator trans) {
		return expr.translate(trans);
	}
	
	@Override
	public String toString() {
		return expr.toString();
	}

	@Override
	public void pushRead() {
		expr.pushRead();
	}

	@Override
	public void pushWrite() {
		expr.pushWrite();
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		expr.visitVariableAccesses(v);
	}
}
