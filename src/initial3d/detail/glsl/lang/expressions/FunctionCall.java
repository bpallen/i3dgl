package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.EvalException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Function;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.lang.names.FunctionName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

public class FunctionCall implements Expression {

	private Token source;
	private Scope scope;
	protected FunctionName name;
	protected Expression[] params;
	
	public FunctionCall(InfoLog infolog, Token source_, Scope scope_, FunctionName name_, Expression ...params_) {
		source = source_;
		scope = scope_;
		name = name_;
		params = params_.clone();
		for (int i = 0; i < params.length; i++) {
			params[i] = params[i].maybeEval(false, infolog);
		}
	}

	@Override
	public TypeName type() {
		return name.returnType();
	}
	
	@Override
	public boolean constexpr() {
		// __forceconstexpr will force attempted eval even with non-constexpr params
		// but does _not_ make the expression constexpr if params are non-constexpr
		boolean b = name.qualified("__constexpr") || name.qualified("__forceconstexpr");
		// only constexpr if definition is available
		b &= name.defined();
		// only constexpr if all params are constexpr
		for (Expression e : params) {
			b &= e.constexpr();
		}
		return b;
	}

	@Override
	public boolean swizzled() {
		// if the function returns by value, the value gets copied so it
		// cannot possibly alias anything. therefore, only ref returns might be swizzled.
		return name.returnType().ref();
	}
	
	@Override
	public Token source() {
		return source;
	}
	
	@Override
	public Scope scope() {
		return scope;
	}
	
	public FunctionName name() {
		return name;
	}
	
	public Expression[] parameters() {
		return params;
	}
	
	@Override
	public boolean lValue() {
		return type().qualified("__ref");
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("call");
		sb.append('[');
		sb.append(name.text());
		sb.append(']');
		sb.append('(');
		for (Expression e : params) {
			sb.append(e);
			sb.append(", ");
		}
		if (params.length > 0) sb.setLength(sb.length() - 2);
		sb.append(')');
		return sb.toString();
	}

	@Override
	public Instance eval(VariableInstanceMap vars) {
		boolean force = name.qualified("__forceconstexpr");
		boolean forced = false;
		Function func = name.definition();
		if (func == null) {
			vars.infoLog().error("cannot evaluate call to undefined function", source());
			throw new EvalException("cannot evaluate call to undefined function");
		}
		Instance[] actual_params = new Instance[params.length];
		for (int i = 0; i < actual_params.length; i++) {
			vars.infoLog().pushContext(null, source(), force);
			try {
				actual_params[i] = params[i].eval(vars);
				if (actual_params[i] == null) {
					throw new NullPointerException("param " + i + " of call to " + name + " was null");
				}
				// deal with swizzle aliasing issues by copying when bound to const ref param
				if (params[i].swizzled() && func.parameters()[i].type().ref() && func.parameters()[i].type().qualified("const")) {
					actual_params[i] = actual_params[i].copy(vars);
				}
			} catch (EvalException e) {
				if (force) {
					// deal with forced constexpr with non-constexpr params
					// this is a bit of a hack, really
					forced = true;
					actual_params[i] = new NullInstance(params[i].source(), params[i].type(), scope(), false);
				} else {
					throw e;
				}
			} finally {
				vars.infoLog().popContext();
			}
		}
		try {
			return func.eval(vars, actual_params);
		} catch (EvalException e) {
			if (forced) vars.infoLog().info("function evaluation attempt was forced; parameter evaluation failure may have been suppressed", source());
			throw e;
		}
	}
	
	@Override
	public Location translate(Translator trans) {
		Function func = trans.functionDefinition(name);
		Location[] actual_params = new Location[params.length];
		for (int i = 0; i < actual_params.length; i++) {
			actual_params[i] = params[i].translate(trans);
			if (actual_params[i] == null) {
				throw new NullPointerException("param " + i + " of function " + name.text() + " was null");
			}
			// deal with swizzle aliasing issues by copying when bound to const ref param
			if (params[i].swizzled() && func.parameters()[i].type().ref() && func.parameters()[i].type().qualified("const")) {
				Location loc2 = trans.temporary((TypeName) params[i].type().withoutQualifiers());
				trans.copy(loc2, actual_params[i]);
				actual_params[i] = loc2;
			}
		}
		return func.translate(trans, actual_params);
	}

	@Override
	public void pushRead() {
		// just ignore the return, and push read/write to params
		Function func = name.definition();
		for (int i = 0; i < params.length; i++) {
			if (func.parameters()[i].type().ref()) {
				// TODO write-only params? ('out')
				// pass by ref, always read
				params[i].pushRead();
				if (!func.parameters()[i].type().qualified("const")) {
					// not const, push write
					params[i].pushWrite();
				}
			} else {
				// pass by value, push read
				params[i].pushRead();
			}
		}
	}

	@Override
	public void pushWrite() {
		pushRead();
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		for (int i = 0; i < params.length; i++) {
			params[i].visitVariableAccesses(v);
		}
	}
	
}
