package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.ConstantPrimitiveLocation;
import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Type;
import initial3d.detail.glsl.tokens.Token;

public class IntInstance extends Instance {

	private int value = 0;
	
	public IntInstance(Token source_, Scope scope_, boolean lvalue_) {
		super(source_, BuiltinScope.instance.declaredType("int", null), scope_, lvalue_);
	}
	
	public IntInstance(Instance o, int x) {
		this(o.source(), o.scope(), o.lValue());
		value(x);
	}
	
	public int value() {
		return value;
	}
	
	public void value(int x) {
		value = x;
	}

	@Override
	public Object javaEquivalent() {
		return value;
	}

	@Override
	public void javaEquivalent(Object o) {
		value = (Integer) o;
	}

	@Override
	public Instance baseCopy(boolean lvalue) {
		return new IntInstance(source(), scope(), lvalue);
	}

	public static IntInstance parseInt(Token t, Scope scope) {
		IntInstance x = new IntInstance(t, scope, false);
		x.value(Integer.parseInt(t.text()));
		return x;
	}
	
	@Override
	public Location translate(Translator trans) {
		return new ConstantPrimitiveLocation(this);
	}
	
	@Override
	public String toString() {
		return "int(" + value + ")";
	}
}
