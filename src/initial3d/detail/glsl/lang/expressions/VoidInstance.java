package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.VoidLocation;
import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.tokens.Token;

public class VoidInstance extends Instance {

	// yes, you can instantiate void. just for convenience.
	
	public VoidInstance(Token source_, Scope scope_, boolean lvalue_) {
		super(source_, BuiltinScope.instance.declaredType("void", null), scope_, lvalue_);
	}

	@Override
	public Instance baseCopy(boolean lvalue) {
		return new VoidInstance(source(), scope(), lvalue);
	}
	
	@Override
	public Location translate(Translator trans) {
		return new VoidLocation();
	}
	
	@Override
	public String toString() {
		return "void";
	}
}
