package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Type;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenException;

public abstract class Instance implements Expression {

	private Token source;
	private TypeName type;
	private Scope scope;
	private boolean lvalue;
	
	public Instance(Token source_, TypeName type_, Scope scope_, boolean lvalue_) {
		source = source_;
		type = type_;
		scope = scope_;
		lvalue = lvalue_;
	}
	
	@Override
	public Token source() {
		return source;
	}

	@Override
	public Scope scope() {
		return scope;
	}
	
	@Override
	public TypeName type() {
		return type;
	}

	@Override
	public boolean constexpr() {
		return true;
	}

	@Override
	public boolean lValue() {
		return lvalue;
	}
	
	@Override
	public Instance eval(VariableInstanceMap vars) {
		return this;
	}
	
	@Override
	public Instance maybeEval(boolean quiet, InfoLog infolog) {
		return this;
	}

	public Object javaEquivalent() {
		return null;
	}
	
	public void javaEquivalent(Object o) {
		throw new UnsupportedOperationException();
	}
	
	public abstract Instance baseCopy(boolean lvalue);
	
	public Instance copy(VariableInstanceMap vars) {
		// instance copy by calling operator=
		OverloadSet assign = type.definition().memberScope().declaredOverload("operator=", vars.infoLog());
		Instance r = baseCopy(true);
		FunctionCall fc = assign.call(vars.infoLog(), source(), scope, r, this);
		fc.eval(vars);
		return r;
	}
	
	public void assign(VariableInstanceMap vars, Instance x) {
		OverloadSet assign = type.definition().memberScope().declaredOverload("operator=", vars.infoLog());
		FunctionCall fc = assign.call(vars.infoLog(), source(), scope, this, x);
		fc.eval(vars);
	}
	
	public boolean replaceSubInstance(Instance x0, Instance x1) {
		return false;
	}
	
	public static Instance parse(Token t, Scope scope) {
		if (t.type() == Token.Type.INT_LITERAL) {
			if (t.text().endsWith("u")) return UIntInstance.parseUInt(t, scope);
			return IntInstance.parseInt(t, scope);
		} else if (t.type() == Token.Type.FLOAT_LITERAL) {
			return FloatInstance.parseFloat(t, scope);
		} else if (t.isText("true") || t.isText("false")) {
			return BoolInstance.parseBool(t, scope);
		} else {
			throw new TokenException(t, "invalid literal");
		}
	}
	
	@Override
	public void pushRead() {
		
	}
	
	@Override
	public void pushWrite() {
		
	}
	
	@Override
	public void visitVariableAccesses(VariableAccess.Visitor v) {
		
	}
	
}
