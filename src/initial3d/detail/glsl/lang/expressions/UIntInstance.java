package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.ConstantPrimitiveLocation;
import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Type;
import initial3d.detail.glsl.tokens.Token;

public class UIntInstance extends Instance {

	private int value;
	
	public UIntInstance(Token source_, Scope scope_, boolean lvalue_) {
		super(source_, BuiltinScope.instance.declaredType("uint", null), scope_, lvalue_);
	}
	
	public UIntInstance(Instance o, int x) {
		this(o.source(), o.scope(), o.lValue());
		value(x);
	}

	public int value() {
		return value;
	}
	
	public void value(int x) {
		value = x;
	}

	@Override
	public Object javaEquivalent() {
		return value;
	}

	@Override
	public void javaEquivalent(Object o) {
		value = (Integer) o;
	}

	@Override
	public Instance baseCopy(boolean lvalue) {
		return new UIntInstance(source(), scope(), lvalue);
	}

	public static UIntInstance parseUInt(Token t, Scope scope) {
		String s = t.text();
		if (s.endsWith("u")) s = t.text().substring(0, t.text().length() - 1);
		UIntInstance x = new UIntInstance(t, scope, false);
		x.value(Integer.parseInt(s));
		return x;
	}
	
	@Override
	public Location translate(Translator trans) {
		return new ConstantPrimitiveLocation(this);
	}
	
	@Override
	public String toString() {
		return "uint(" + (0xFFFFFFFFL & (long) value) + ")";
	}
	
}
