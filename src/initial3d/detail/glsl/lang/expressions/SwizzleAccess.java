package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Swizzle;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.lang.names.SwizzleName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

public class SwizzleAccess implements Expression {

	private Swizzle swiz;
	private SwizzleName name;
	private Expression thisexpr;
	
	public SwizzleAccess(Swizzle swiz_, SwizzleName name_, Expression thisexpr_) {
		swiz = swiz_;
		name = name_;
		thisexpr = thisexpr_;
	}
	
	public Swizzle variable() {
		return swiz;
	}
	
	public SwizzleName name() {
		return name;
	}
	
	public Expression thisExpr() {
		return thisexpr;
	}
	
	@Override
	public Token source() {
		return name.source();
	}
	
	@Override
	public Scope scope() {
		return thisexpr.scope();
	}

	@Override
	public TypeName type() {
		return name.type();
	}
	
	@Override
	public boolean lValue() {
		return thisexpr.lValue();
	}
	
	@Override
	public boolean constexpr() {
		return thisexpr.constexpr();
	}
	
	@Override
	public boolean swizzled() {
		return true;
	}
	
	@Override
	public String toString() {
		return "swiz[" + name.text() + ", " + thisexpr + "]";
	}

	@Override
	public Instance eval(VariableInstanceMap vars) {
		return swiz.eval(vars, thisexpr);
	}
	
	@Override
	public Location translate(Translator trans) {
		return swiz.translate(trans, thisexpr);
	}

	@Override
	public void pushRead() {
		thisexpr.pushRead();
	}

	@Override
	public void pushWrite() {
		thisexpr.pushWrite();
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		thisexpr.visitVariableAccesses(v);
	}
	
}
