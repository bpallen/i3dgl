package initial3d.detail.glsl.lang.expressions;

import java.util.ArrayList;
import java.util.List;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.ArrayLocation;
import initial3d.detail.glsl.lang.Array;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Type;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.tokens.Token;

public class ArrayInstance extends Instance {

	private List<Instance> members = new ArrayList<>();
	
	public ArrayInstance(Token source_, ArrayTypeName type_, Scope scope_, boolean lvalue_) {
		super(source_, type_, scope_, lvalue_);
		// instantiate members (assuming we know enough about the size)
		if (type_.size() < 1) throw new IllegalArgumentException("bad array size you faggot");
		for (int i = 0; i < type_.size(); i++) {
			members.add(type_.innerType().definition().instantiate(source_, type_.innerType(), scope_, lvalue_));
		}
	}

	@Override
	public ArrayTypeName type() {
		return (ArrayTypeName) super.type();
	}
	
	@Override
	public Instance baseCopy(boolean lvalue) {
		return new ArrayInstance(source(), (ArrayTypeName) type(), scope(), lvalue);
	}

	public Instance member(int i) {
		return members.get(i);
	}
	
	@Override
	public boolean replaceSubInstance(Instance x0, Instance x1) {
		boolean r = false;
		for (int i = 0; i < members.size(); i++) {
			if (members.get(i) == x0) {
				members.set(i, x1);
				r = true;
			} else {
				r |= members.get(i).replaceSubInstance(x0, x1);
			}
		}
		return r;
	}
	
	@Override
	public Location translate(Translator trans) {
		return new ArrayLocation(trans, this);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("array<");
		sb.append(type().innerType().text());
		sb.append(">{");
		boolean first = true;
		for (Instance x : members) {
			if (!first) sb.append(", ");
			Object o = x.javaEquivalent();
			sb.append(o == null ? x : o);
			first = false;
		}
		sb.append("}");
		return sb.toString();
	}
}
