package initial3d.detail.glsl.lang.expressions;

import initial3d.detail.glsl.intermediate.Label;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.intermediate.TranslateException;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.ConstantPrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.VariableAccess.Visitor;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

public class SelectionExpression implements Expression {

	private Token source;
	private Scope scope;
	
	private Expression cond;
	private Expression true_branch;
	private Expression false_branch;
	
	public SelectionExpression(Token source_, Scope scope_) {
		source = source_;
		scope = scope_;
	}
	
	public void cond(Expression x) {
		cond = x;
	}
	
	public void trueBranch(Expression x) {
		true_branch = x;
	}
	
	public void falseBranch(Expression x) {
		false_branch = x;
	}
	
	@Override
	public Token source() {
		return source;
	}

	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public TypeName type() {
		// TODO common type stuff for selection
		if (!true_branch.type().declEquals(false_branch.type())) throw new IllegalStateException("mismatched types for selection");
		return true_branch.type();
	}

	@Override
	public boolean constexpr() {
		return cond.constexpr() && true_branch.constexpr() && false_branch.constexpr();
	}
	
	@Override
	public boolean swizzled() {
		// this applies even if we don't return refs because we don't force a copy
		return true_branch.swizzled() || false_branch.swizzled();
	}
	
	@Override
	public boolean lValue() {
		return false;
	}
	
	@Override
	public String toString() {
		return "selection[" + cond + " ? " + true_branch + " : " + false_branch + "]";
	}
	
	@Override
	public Instance eval(VariableInstanceMap vars) {
		if (((BoolInstance) cond.eval(vars)).value()) {
			return true_branch.eval(vars);
		} else {
			return false_branch.eval(vars);
		}
	}
	
	@Override
	public Location translate(Translator trans) {
		if (cond.constexpr()) {
			if (((BoolInstance) cond.eval(false, trans.infoLog())).value()) {
				return true_branch.translate(trans);
			} else {
				return false_branch.translate(trans);
			}
		} else {
			if (type().ref()) {
				trans.infoLog().error("selection expression cannot have reference type", source);
				throw new TranslateException("selection expression cannot have reference type");
			}
			PrimitiveLocation cloc = (PrimitiveLocation) cond.translate(trans);
			Location rloc = trans.temporary(type());
			trans.pushStatement();
			try {
				Label false_lab = new Label();
				Label end_lab = new Label();
				trans.branch(MacroOp.Code.JEZ, false_lab, cloc);
				true_branch.translate(trans);
				trans.branch(MacroOp.Code.JMP, end_lab);
				trans.label(false_lab);
				false_branch.translate(trans);
				trans.label(end_lab);
			} finally {
				trans.popStatement();
			}
			return rloc;
		}
	}

	@Override
	public void pushRead() {
		cond.pushRead();
		true_branch.pushRead();
		false_branch.pushRead();
	}

	@Override
	public void pushWrite() {
		// can't write through selection
	}

	@Override
	public void visitVariableAccesses(Visitor v) {
		cond.visitVariableAccesses(v);
		true_branch.visitVariableAccesses(v);
		false_branch.visitVariableAccesses(v);
	}
	
}







