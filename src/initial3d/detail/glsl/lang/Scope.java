package initial3d.detail.glsl.lang;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.MemberAccess;
import initial3d.detail.glsl.lang.expressions.Requalification;
import initial3d.detail.glsl.lang.expressions.SwizzleAccess;
import initial3d.detail.glsl.lang.names.FunctionName;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.lang.names.SwizzleName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenBuilder;
import initial3d.detail.glsl.tokens.TokenException;

public class Scope {

	private static class TypeNamePair {
		
		public final TypeName t1, t2;
		
		public TypeNamePair(TypeName t1_, TypeName t2_) {
			t1 = t1_;
			t2 = t2_;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((t1 == null) ? 0 : t1.hashCode());
			result = prime * result + ((t2 == null) ? 0 : t2.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			TypeNamePair other = (TypeNamePair) obj;
			if (t1 == null) {
				if (other.t1 != null) return false;
			} else if (!t1.equals(other.t1)) return false;
			if (t2 == null) {
				if (other.t2 != null) return false;
			} else if (!t2.equals(other.t2)) return false;
			return true;
		}
		
	}
	
	private Scope parent;
	private Node owner = null;
	private HashMap<String, Name> declarations = new HashMap<>();
	private HashMap<String, Node> definitions = new HashMap<>();
	private ArrayList<Name> declaration_list = new ArrayList<>();
	private ArrayList<Node> definition_list = new ArrayList<>();
	private ArrayList<VariableName> linked_vars = new ArrayList<>();
	private HashMap<TypeNamePair, TypeName> common_types = new HashMap<>();
	private HashMap<String, Layout> layouts = new HashMap<>();
	private int next_id = 1;
	
	// TODO possibly use a specific 'global owner' type as owner?
	// set to true only explictly (after which setting owner is forbidden)
	// this is to prevent problems where name equality and hashcodes change when containing scope owners are changed
	private boolean allow_decl = false;
	
	public Scope(Scope parent_) {
		parent = parent_;
	}
	
	@Override
	public int hashCode() {
		if (!allow_decl) throw new IllegalStateException("scope hashcode is undefined before declarations are allowed");
		if (owner == null) return 0;
		return owner.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		// type check currently means builtin scope is not equal to normal global scope
		if (getClass() != o.getClass()) return false;
		if (!allow_decl) throw new IllegalStateException("scope equality is undefined before declarations are allowed");
		Scope os = (Scope) o;
		// all normal global scopes are equal
		if (global() && os.global()) return true;
		// if only one is global, never equal
		if (global() != os.global()) return false;
		// type member scope equality
		if (owner instanceof Type && os.owner instanceof Type) {
			return ((Type) owner).name().equals(((Type) os.owner).name());
		}
		// FIXME equality of other owned scopes
		return false;
	}
	
	public int genID() {
		return next_id++;
	}
	
	public Scope parent() {
		return parent;
	}
	
	public Node owner() {
		return owner;
	}
	
	public void owner(Node n) {
		if (owner == n) return;
		if (allow_decl) throw new IllegalStateException("can't set scope owner after declarations allowed");
		owner = n;
	}
	
	/**
	 * this effectively fixes the scope owner after checking that the parent allows declarations.
	 */
	public void allowDeclarations() {
		if (parent != null && !parent.allow_decl) throw new IllegalStateException("can't allow declarations when parent scope does not yet allow declarations");
		if (parent != null && !parent.global() && owner == null) throw new IllegalStateException("can't allow declarations in ownerless child of non-global scope");
		allow_decl = true;
	}
	
	public boolean global() {
		return owner == null && (parent == null || parent instanceof BuiltinScope);
	}
	
	/**
	 * Layout defaults by interface qualifier (in, out, uniform).
	 * 
	 * @param iface
	 * @return
	 */
	public Layout layout(String iface) {
		Layout l = layouts.get(iface);
		if (l == null) {
			if (parent == null) {
				l = new Layout();
			} else {
				l = parent.layout(iface).clone();
			}
			layouts.put(iface, l);
		}
		return l;
	}
	
	public boolean canDeclare(Name name) {
		return !declarations.containsKey(name.text());
	}
	
	public void declare(Name name, InfoLog infolog) {
		if (!allow_decl) throw new IllegalStateException("declarations not yet allowed in this scope");
		Name name0 = declarations.get(name.text());
		if (name0 != null) {
			if (name0.equals(name)) {
				return;
			} else {
				infolog.error("name already declared incompatibly: " + name, name.source());
				infolog.detail("see original declaration: " + name0, name0.source());
				throw new NameException("name already declared incompatibly", name, name0);
			}
		}
		if (name instanceof FunctionName) {
			// also declare the overload set if needed
			String oname = name.plainText();
			Name n = declarations.get(oname);
			if (n != null && !(n instanceof OverloadSet)) {
				infolog.error("name already declared incompatibly: " + name, n.source());
				infolog.detail("see original declaration: " + name0, name0.source());
				throw new NameException("name already declared incompatibly", n, name0);
			}
			if (n == null) {
				if (parent != null) {
					Name parname = parent.declaration(oname, true);
					if (parname instanceof OverloadSet) {
						// name is visible in parent scope as overloadset
						n = new OverloadSet(name.source(), this, (OverloadSet) parname);
					}
				}
				if (n == null) n = new OverloadSet(name.source(), this, null);
				declare(n, infolog);
			}
			OverloadSet oset = (OverloadSet) n;
			FunctionName fname = (FunctionName) name;
			// add function to overload set
			oset.add(fname);
			// special handling for declaring arithmetic common types
			if (oname.equals("__common_type")) {
				if (fname.parameterTypes().length != 2) throw new BuildException("bad args for __common_type");
				commonType(fname.parameterTypes()[0], fname.parameterTypes()[1], fname.returnType());
			}
		}
		if (name instanceof VariableName || name instanceof SwizzleName) {
			// also declare under plain name
			if (declarations.containsKey(name.plainText())) {
				Name name00 = declarations.get(name.plainText());
				infolog.error("name already declared incompatibly: " + name.text(), name.source());
				infolog.detail("see original declaration", name00.source());
				throw new NameException("name already declared incompatibly", name, name00);
			}
			declarations.put(name.plainText(), name);
		}
		declarations.put(name.text(), name);
		declaration_list.add(name);
	}
	
	public void define(Name name, Node def, InfoLog infolog) {
		Name name0 = declaration(name.text(), false);
		if (name0 == null) {
			infolog.error("name not declared: " + name.text(), name.source());
			throw new NameException("name not declared", name);
		}
		if (!name0.equals(name)) {
			infolog.error("name does not match declaration: " + name.text(), name.source());
			infolog.detail("see original declaration", name0.source());
			throw new NameException("name does not match declaration", name, name0);
		}
		if (definition(name) != null) {
			infolog.error("name is already defined: " + name.text(), name.source());
			infolog.detail("see original definition", definition(name).source());
			throw new NameException("name is already defined", name);
		}
		definitions.put(name.text(), def);
		definition_list.add(def);
	}
	
	public Name declaration(String declname, boolean recursive) {
		Name name = declarations.get(declname);
		if (recursive && name == null && parent != null) return parent.declaration(declname, true);
		return name;
	}
	
	public Name declaration(String declname) {
		return declaration(declname, true);
	}
	
	public Node access(Token t, InfoLog infolog) {
		Name name = declarations.get(t.text());
		if (name != null) {
			// declared in this scope
			if (name instanceof VariableName) {
				if (owner instanceof Type) {
					// member variables can't be accessed directly, require a linked instance as container
				} else {
					// function or global scope
					return useVariable(t, true, infolog).definition().access((VariableName) name.copy(t, this));
				}
			} else {
				return name.copy(t, this);
			}
		}
		// try resolve against linked vars ('this', unnamed interface blocks etc)
		for (VariableName v : linked_vars) {
			Type type = v.type().definition();
			Scope ms = type.memberScope();
			name = ms.declarations.get(t.text());
			if (name != null) {
				// have to be careful what scope the names are copied to (must be member scope)
				// else the name can end up being undefined
				if (name instanceof VariableName) {
					VariableName vname = (VariableName) name;
					return new MemberAccess(vname.copy(t, ms), v.definition().access(v));
				}
				if (name instanceof SwizzleName) {
					SwizzleName zname = (SwizzleName) name;
					return new SwizzleAccess(zname.definition(), zname.copy(t, ms), v.definition().access(v));
				}
				if (name instanceof OverloadSet) {
					OverloadSet oname = ((OverloadSet) name).copy(t, ms);
					oname.thisExpr(v.definition().access(v));
					return oname;
				}
				return name.copy(t, this);
			}
		}
		// try parent
		if (parent != null) return parent.access(t, infolog);
		return null;
	}
	
	public Name useName(Token t, boolean recursive) {
		Name name = declaration(t.text(), recursive);
		if (name != null) return name.copy(t, this);
		return null;
	}
	
	public Name useName(Token t) {
		return useName(t, true);
	}
	
	public <T extends Name> T useName(Token t, Class<T> cls, boolean recursive, InfoLog infolog) {
		Name name = useName(t, recursive);
		try {
			return cls.cast(name);
		} catch (ClassCastException e) {
			infolog.error("token does not name a " + cls.getSimpleName(), t);
			if (name != null) infolog.detail("see declaration", name.declaration().source());
			throw new NameException("token does not name a " + cls.getSimpleName(), name);
		}
	}
	
	public <T extends Name> T useName(Token t, Class<T> cls, InfoLog infolog) {
		return useName(t, cls, true, infolog);
	}
	
	public TypeName useType(Token t, boolean recursive, InfoLog infolog) {
		return useName(t, TypeName.class, recursive, infolog);
	}
	
	public VariableName useVariable(Token t, boolean recursive, InfoLog infolog) {
		return useName(t, VariableName.class, recursive, infolog);
	}
	
	public FunctionName useFunction(Token t, boolean recursive, InfoLog infolog) {
		return useName(t, FunctionName.class, recursive, infolog);
	}
	
	public OverloadSet useOverload(Token t, boolean recursive, InfoLog infolog) {
		return useName(t, OverloadSet.class, recursive, infolog);
	}
	
	public TypeName useType(Token t, InfoLog infolog) {
		return useName(t, TypeName.class, true, infolog);
	}
	
	public VariableName useVariable(Token t, InfoLog infolog) {
		return useName(t, VariableName.class, true, infolog);
	}
	
	public FunctionName useFunction(Token t, InfoLog infolog) {
		return useName(t, FunctionName.class, true, infolog);
	}
	
	public OverloadSet useOverload(Token t, InfoLog infolog) {
		return useName(t, OverloadSet.class, true, infolog);
	}
	
	public <T extends Name> T declaredName(String declname, Class<T> cls, boolean recursive, InfoLog infolog) {
		Name name = declaration(declname, recursive);
		try {
			return cls.cast(name);
		} catch (ClassCastException e) {
			infolog.error("'" + declname + "' is not a " + cls.getSimpleName());
			if (name != null) infolog.detail("see declaration", name.source());
			throw new NameException("'" + declname + "' is not a " + cls.getSimpleName(), name);
		}
	}
	
	public <T extends Name> T declaredName(String declname, Class<T> cls, InfoLog infolog) {
		return declaredName(declname, cls, true, infolog);
	}
	
	public TypeName declaredType(String declname, boolean recursive, InfoLog infolog) {
		return declaredName(declname, TypeName.class, recursive, infolog);
	}
	
	public VariableName declaredVariable(String declname, boolean recursive, InfoLog infolog) {
		return declaredName(declname, VariableName.class, recursive, infolog);
	}
	
	public SwizzleName declaredSwizzle(String declname, boolean recursive, InfoLog infolog) {
		return declaredName(declname, SwizzleName.class, recursive, infolog);
	}
	
	public FunctionName declaredFunction(String declname, boolean recursive, InfoLog infolog) {
		return declaredName(declname, FunctionName.class, recursive, infolog);
	}
	
	public OverloadSet declaredOverload(String declname, boolean recursive, InfoLog infolog) {
		return declaredName(declname, OverloadSet.class, recursive, infolog);
	}
	
	public TypeName declaredType(String declname, InfoLog infolog) {
		return declaredName(declname, TypeName.class, true, infolog);
	}
	
	public VariableName declaredVariable(String declname, InfoLog infolog) {
		return declaredName(declname, VariableName.class, true, infolog);
	}
	
	public SwizzleName declaredSwizzle(String declname, InfoLog infolog) {
		return declaredName(declname, SwizzleName.class, true, infolog);
	}
	
	public FunctionName declaredFunction(String declname, InfoLog infolog) {
		return declaredName(declname, FunctionName.class, true, infolog);
	}
	
	public OverloadSet declaredOverload(String declname, InfoLog infolog) {
		return declaredName(declname, OverloadSet.class, true, infolog);
	}
	
	public Node definition(Name name) {
		Name name0 = declarations.get(name.text());
		if (name0 == null && parent != null) return parent.definition(name);
		if (name0 == null) return null;
		if (!name0.getClass().equals(name.getClass())) return null;
		return definitions.get(name.text());
	}

	public <T extends Node> T definedNode(Name name, Class<T> cls, InfoLog infolog) {
		Node n = definition(name);
		try {
			return cls.cast(n);
		} catch (ClassCastException e) {
			infolog.error("name '${text}' is not defined as a " + cls.getSimpleName(), name.source());
			infolog.detail("see definition", n.source());
			throw new NameException("name is not defined as a " + cls.getSimpleName(), name);
		}
	}
	
	public Type definedType(TypeName name, InfoLog infolog) {
		return definedNode(name, Type.class, infolog);
	}
	
	public Variable definedVariable(VariableName name, InfoLog infolog) {
		return definedNode(name, Variable.class, infolog);
	}
	
	public Swizzle definedSwizzle(SwizzleName name, InfoLog infolog) {
		return definedNode(name, Swizzle.class, infolog);
	}
	
	public Function definedFunction(FunctionName name, InfoLog infolog) {
		return definedNode(name, Function.class, infolog);
	}
	
	public Set<String> localDeclarations() {
		return Collections.unmodifiableSet(declarations.keySet());
	}
	
	public List<Node> localDefinitions() {
		return Collections.unmodifiableList(definition_list);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Name> List<T> localDeclaredNames(Class<T> cls) {
		List<T> names = new ArrayList<>();
		for (Name name : declaration_list) {
			if (cls.isInstance(name)) {
				names.add((T) name);
			}
		}
		return names;
	}
	
	public void linkVariable(VariableName name, InfoLog infolog) {
		linked_vars.add(name);
		definedNode(name.type(), Type.class, infolog);
	}
	
	public void linkVariable(String declname, InfoLog infolog) {
		linkVariable(declaredVariable(declname, true, infolog), infolog);
	}
	
	private static boolean addsQualifier(TypeName type1, TypeName type0, String ...qs) {
		for (String q : qs) {
			if (type1.qualified(q) && !type0.qualified(q)) return true;
		}
		return false;
	}
	
	private static boolean removesQualifier(TypeName type1, TypeName type0, String q) {
		return !type1.qualified(q) && type0.qualified(q);
	}
	
	// TODO move to expression?
	public Expression convert(InfoLog infolog, TypeName type, Expression expr, boolean explicit, boolean quiet) {
		TypeName type0 = expr.type();
		// types exactly the same -> ok
		if (type.declQualEquals(type0)) return expr;
		infolog.pushContext("attempting conversion from " + expr.type() + " to " + type, expr.source(), quiet);
		try {
			// cant use non-const '__ref' unless everything else the same
			if (type.qualified("__ref") && !type.qualified("const") &&
				!type.withoutQualifier("__ref").declQualEquals(type0.withoutQualifier("__ref"))
			) {
				infolog.error("non-const '__ref' disallows other type changes", expr.source());
				return null;
			}
			// can't use non-const '__ref' unless lvalue
			if (type.qualified("__ref") && !type.qualified("const") && !expr.lValue()) {
				infolog.error("non-const '__ref' requires lvalue", expr.source());
				return null;
			}
			// FIXME convert out/inout param qualifiers to __ref
			// cant add 'out' or 'inout' unless lvalue and not const
			if (addsQualifier(type, type0, "out", "inout") && (!expr.lValue() || expr.type().qualified("const"))) {
				infolog.error("can't add 'out' or 'inout' qualifiers unless expression is non-const lvalue", expr.source());
				return null;
			}
			// cant add '__ref' unless const or lvalue
			if (addsQualifier(type, type0, "__ref") && !type.qualified("const") && !expr.lValue()) {
				infolog.error("can't add '__ref' qualifier unless expression is const or lvalue", expr.source());
				return null;
			}
			// check base type	
			if (type.declEquals(type0)) {
				return new Requalification(expr, type);
			} else {
				// value conversion
				try {
					if (expr instanceof FunctionCall) {
						//System.out.println(expr);
						if (((FunctionCall) expr).name().plainText().startsWith("__ctor__implicit_")) {
							// only allow one implicit conversion
							infolog.error("no " + (explicit ? "explicit" : "implicit") + " conversion", expr.source());
							return null;
						}
					}
					String f = (explicit ? "__ctor_" : "__ctor__implicit_") + type.withoutQualifiers().text();
					Token s = new TokenBuilder(expr.source()).text(f).toToken();
					OverloadSet o = (OverloadSet) access(s, infolog);
					FunctionCall c = o.call(infolog, expr.source(), expr.scope(), expr);
					// dont propagate explicitness
					return convert(infolog, type, c, false, quiet);
				} catch (Exception e) {
					infolog.error("no " + (explicit ? "explicit" : "implicit") + " conversion", expr.source());
					return null;
				}
			}
			
		} finally {
			infolog.popContext();
		}
	}
	
	public void commonType(TypeName a, TypeName b, TypeName res) {
		a = a.declaration();
		b = b.declaration();
		res = res.declaration();
		common_types.put(new TypeNamePair(a, b), res);
		common_types.put(new TypeNamePair(b, a), res);
	}
	
	/**
	 * It should be ok to set this even for non-arithmetic types, just useless.
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public TypeName commonType(TypeName a, TypeName b) {
		a = a.declaration();
		b = b.declaration();
		TypeName r = common_types.get(new TypeNamePair(a, b));
		if (r != null) return r;
		if (parent != null) return parent.commonType(a, b);
		return null;
	}
	
}
