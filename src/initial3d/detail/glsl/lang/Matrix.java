package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Allocator;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.TranslateException;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.lang.names.TypeName;

public class Matrix extends Struct {

	public Matrix(ScalarTypeName type_, Scope member_scope_, Statement body_) {
		super(type_, member_scope_, body_);
	}

	public Vector vecType() {
		Type type = ((ArrayTypeName) memberScope().declaredVariable("_data", false, InfoLog.NULL).type()).innerType().definition();
		if (!(type instanceof Vector)) throw new IllegalStateException("matrix vector type is not a vector");
		return (Vector) type;
	}
	
	public BuiltinType valType() {
		return vecType().valType();
	}
	
	public int matCols() {
		int cols = ((ArrayTypeName) memberScope().declaredVariable("_data", false, InfoLog.NULL).type()).size();
		if (cols < 0) throw new IllegalStateException("bad matrix array size");
		return cols;
	}
	
	public int matRows() {
		return vecType().vecSize();
	}
	
	@Override
	public Location allocate(Allocator alloc, TypeName actual_type, Layout layout, Location parent) {
		if (layout != null && layout.matrix_order == Layout.MatrixOrder.ROW_MAJOR) {
			// TODO source token?
			alloc.infoLog().error("row-major matrix allocation not currently supported");
			throw new TranslateException("row-major matrix allocation");
		}
		// FIXME matrix col/row-major
		return super.allocate(alloc, actual_type, layout, parent);
	}
	
	@Override
	public int allocAlign(Layout l) {
		return l == null ? valType().allocAlign(l) : l.matrixAlign(this);
	}
}
