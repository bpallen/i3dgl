package initial3d.detail.glsl.lang.source;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class BuiltinSource {
	
	public static Reader open(String fname) {
		InputStream s = BuiltinSource.class.getResourceAsStream(fname);
		if (s == null) return null;
		return new BufferedReader(new InputStreamReader(s));
	}
	
}
