// vertex shader environment header

layout(__attribute) in;

layout(__special) in int gl_VertexID;
layout(__special) in int gl_InstanceID;

layout(__special) out gl_PerVertex {
	vec4 gl_Position;
	// TODO pointsize, clipdist
};
