
#define __tokenpaste1(a, b) a##b
#define __tokenpaste(a, b) __tokenpaste1(a, b) 

//
// primitives
//
#pragma i3d_include <primitives>

//
// vector types
//

#define _VAL_TYPE bool
#define _VEC_TYPE_BASE bvec
#define _VEC_ARITH 0
#define _VEC_LOGIC 1
#define _VEC_BITWISE 0
#pragma i3d_include <vectors>

#define _VAL_TYPE int
#define _VEC_TYPE_BASE ivec
#define _VEC_ARITH 1
#define _VEC_LOGIC 0
#define _VEC_BITWISE 1
#pragma i3d_include <vectors>

#define _VAL_TYPE uint
#define _VEC_TYPE_BASE uvec
#define _VEC_ARITH 1
#define _VEC_LOGIC 0
#define _VEC_BITWISE 1
#pragma i3d_include <vectors>

#define _VAL_TYPE float
#define _VEC_TYPE_BASE vec
#define _VEC_ARITH 1
#define _VEC_LOGIC 0
#define _VEC_BITWISE 0
#pragma i3d_include <vectors>

vec2 __common_type(vec2 a, ivec2 b);
vec3 __common_type(vec3 a, ivec3 b);
vec4 __common_type(vec4 a, ivec4 b);
vec2 __common_type(vec2 a, uvec2 b);
vec3 __common_type(vec3 a, uvec3 b);
vec4 __common_type(vec4 a, uvec4 b);

#pragma i3d_include <vectorctors>

//
// matrix types
//

#define _VAL_TYPE float
#define _VEC_TYPE_BASE vec
#define _MAT_TYPE_BASE mat
#pragma i3d_include <matrices>

// cleanup
#undef _VEC_SIZE
#undef _MAT_COLS
#undef _MAT_ROWS
#undef _VAL_TYPE
#undef _VEC_TYPE
#undef _MAT_TYPE
#undef _VEC_ARITH
#undef _VEC_LOGIC
#undef _VEC_BITWISE

//
// specials
//

// results of texture functions
layout(__special) in vec4 __texdata;
layout(__special) in ivec4 __texidata;
layout(__special) in uvec4 __texudata;



