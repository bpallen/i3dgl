bvec2 __ctor_bvec2(bool x) {
	return _make_bvec2(bool(x), bool(x));
}

bvec2 __ctor_bvec2(int x) {
	return _make_bvec2(bool(x), bool(x));
}

bvec2 __ctor_bvec2(uint x) {
	return _make_bvec2(bool(x), bool(x));
}

bvec2 __ctor_bvec2(float x) {
	return _make_bvec2(bool(x), bool(x));
}

bvec2 __ctor_bvec2(bool x, bool y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(bool x, int y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(bool x, uint y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(bool x, float y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(int x, bool y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(int x, int y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(int x, uint y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(int x, float y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(uint x, bool y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(uint x, int y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(uint x, uint y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(uint x, float y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(float x, bool y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(float x, int y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(float x, uint y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(float x, float y) {
	return _make_bvec2(bool(x), bool(y));
}

bvec2 __ctor_bvec2(bvec2 xy) {
	return _make_bvec2(bool(xy.x), bool(xy.y));
}

bvec2 __ctor_bvec2(ivec2 xy) {
	return _make_bvec2(bool(xy.x), bool(xy.y));
}

bvec2 __ctor_bvec2(uvec2 xy) {
	return _make_bvec2(bool(xy.x), bool(xy.y));
}

bvec2 __ctor_bvec2(vec2 xy) {
	return _make_bvec2(bool(xy.x), bool(xy.y));
}

bvec2 __ctor_bvec2(bvec3 xyz) {
	return _make_bvec2(bool(xyz.x), bool(xyz.y));
}

bvec2 __ctor_bvec2(ivec3 xyz) {
	return _make_bvec2(bool(xyz.x), bool(xyz.y));
}

bvec2 __ctor_bvec2(uvec3 xyz) {
	return _make_bvec2(bool(xyz.x), bool(xyz.y));
}

bvec2 __ctor_bvec2(vec3 xyz) {
	return _make_bvec2(bool(xyz.x), bool(xyz.y));
}

bvec2 __ctor_bvec2(bvec4 xyzw) {
	return _make_bvec2(bool(xyzw.x), bool(xyzw.y));
}

bvec2 __ctor_bvec2(ivec4 xyzw) {
	return _make_bvec2(bool(xyzw.x), bool(xyzw.y));
}

bvec2 __ctor_bvec2(uvec4 xyzw) {
	return _make_bvec2(bool(xyzw.x), bool(xyzw.y));
}

bvec2 __ctor_bvec2(vec4 xyzw) {
	return _make_bvec2(bool(xyzw.x), bool(xyzw.y));
}

ivec2 __ctor_ivec2(bool x) {
	return _make_ivec2(int(x), int(x));
}

ivec2 __ctor_ivec2(int x) {
	return _make_ivec2(int(x), int(x));
}

ivec2 __ctor_ivec2(uint x) {
	return _make_ivec2(int(x), int(x));
}

ivec2 __ctor_ivec2(float x) {
	return _make_ivec2(int(x), int(x));
}

ivec2 __ctor_ivec2(bool x, bool y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(bool x, int y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(bool x, uint y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(bool x, float y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(int x, bool y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(int x, int y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(int x, uint y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(int x, float y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(uint x, bool y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(uint x, int y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(uint x, uint y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(uint x, float y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(float x, bool y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(float x, int y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(float x, uint y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(float x, float y) {
	return _make_ivec2(int(x), int(y));
}

ivec2 __ctor_ivec2(bvec2 xy) {
	return _make_ivec2(int(xy.x), int(xy.y));
}

ivec2 __ctor_ivec2(ivec2 xy) {
	return _make_ivec2(int(xy.x), int(xy.y));
}

ivec2 __ctor_ivec2(uvec2 xy) {
	return _make_ivec2(int(xy.x), int(xy.y));
}

ivec2 __ctor_ivec2(vec2 xy) {
	return _make_ivec2(int(xy.x), int(xy.y));
}

ivec2 __ctor_ivec2(bvec3 xyz) {
	return _make_ivec2(int(xyz.x), int(xyz.y));
}

ivec2 __ctor_ivec2(ivec3 xyz) {
	return _make_ivec2(int(xyz.x), int(xyz.y));
}

ivec2 __ctor_ivec2(uvec3 xyz) {
	return _make_ivec2(int(xyz.x), int(xyz.y));
}

ivec2 __ctor_ivec2(vec3 xyz) {
	return _make_ivec2(int(xyz.x), int(xyz.y));
}

ivec2 __ctor_ivec2(bvec4 xyzw) {
	return _make_ivec2(int(xyzw.x), int(xyzw.y));
}

ivec2 __ctor_ivec2(ivec4 xyzw) {
	return _make_ivec2(int(xyzw.x), int(xyzw.y));
}

ivec2 __ctor_ivec2(uvec4 xyzw) {
	return _make_ivec2(int(xyzw.x), int(xyzw.y));
}

ivec2 __ctor_ivec2(vec4 xyzw) {
	return _make_ivec2(int(xyzw.x), int(xyzw.y));
}

uvec2 __ctor_uvec2(bool x) {
	return _make_uvec2(uint(x), uint(x));
}

uvec2 __ctor_uvec2(int x) {
	return _make_uvec2(uint(x), uint(x));
}

uvec2 __ctor_uvec2(uint x) {
	return _make_uvec2(uint(x), uint(x));
}

uvec2 __ctor_uvec2(float x) {
	return _make_uvec2(uint(x), uint(x));
}

uvec2 __ctor_uvec2(bool x, bool y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(bool x, int y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(bool x, uint y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(bool x, float y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(int x, bool y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(int x, int y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(int x, uint y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(int x, float y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(uint x, bool y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(uint x, int y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(uint x, uint y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(uint x, float y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(float x, bool y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(float x, int y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(float x, uint y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(float x, float y) {
	return _make_uvec2(uint(x), uint(y));
}

uvec2 __ctor_uvec2(bvec2 xy) {
	return _make_uvec2(uint(xy.x), uint(xy.y));
}

uvec2 __ctor_uvec2(ivec2 xy) {
	return _make_uvec2(uint(xy.x), uint(xy.y));
}

uvec2 __ctor_uvec2(uvec2 xy) {
	return _make_uvec2(uint(xy.x), uint(xy.y));
}

uvec2 __ctor_uvec2(vec2 xy) {
	return _make_uvec2(uint(xy.x), uint(xy.y));
}

uvec2 __ctor_uvec2(bvec3 xyz) {
	return _make_uvec2(uint(xyz.x), uint(xyz.y));
}

uvec2 __ctor_uvec2(ivec3 xyz) {
	return _make_uvec2(uint(xyz.x), uint(xyz.y));
}

uvec2 __ctor_uvec2(uvec3 xyz) {
	return _make_uvec2(uint(xyz.x), uint(xyz.y));
}

uvec2 __ctor_uvec2(vec3 xyz) {
	return _make_uvec2(uint(xyz.x), uint(xyz.y));
}

uvec2 __ctor_uvec2(bvec4 xyzw) {
	return _make_uvec2(uint(xyzw.x), uint(xyzw.y));
}

uvec2 __ctor_uvec2(ivec4 xyzw) {
	return _make_uvec2(uint(xyzw.x), uint(xyzw.y));
}

uvec2 __ctor_uvec2(uvec4 xyzw) {
	return _make_uvec2(uint(xyzw.x), uint(xyzw.y));
}

uvec2 __ctor_uvec2(vec4 xyzw) {
	return _make_uvec2(uint(xyzw.x), uint(xyzw.y));
}

vec2 __ctor_vec2(bool x) {
	return _make_vec2(float(x), float(x));
}

vec2 __ctor_vec2(int x) {
	return _make_vec2(float(x), float(x));
}

vec2 __ctor_vec2(uint x) {
	return _make_vec2(float(x), float(x));
}

vec2 __ctor_vec2(float x) {
	return _make_vec2(float(x), float(x));
}

vec2 __ctor_vec2(bool x, bool y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(bool x, int y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(bool x, uint y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(bool x, float y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(int x, bool y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(int x, int y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(int x, uint y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(int x, float y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(uint x, bool y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(uint x, int y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(uint x, uint y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(uint x, float y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(float x, bool y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(float x, int y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(float x, uint y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(float x, float y) {
	return _make_vec2(float(x), float(y));
}

vec2 __ctor_vec2(bvec2 xy) {
	return _make_vec2(float(xy.x), float(xy.y));
}

vec2 __ctor_vec2(ivec2 xy) {
	return _make_vec2(float(xy.x), float(xy.y));
}

vec2 __ctor_vec2(uvec2 xy) {
	return _make_vec2(float(xy.x), float(xy.y));
}

vec2 __ctor_vec2(vec2 xy) {
	return _make_vec2(float(xy.x), float(xy.y));
}

vec2 __ctor_vec2(bvec3 xyz) {
	return _make_vec2(float(xyz.x), float(xyz.y));
}

vec2 __ctor_vec2(ivec3 xyz) {
	return _make_vec2(float(xyz.x), float(xyz.y));
}

vec2 __ctor_vec2(uvec3 xyz) {
	return _make_vec2(float(xyz.x), float(xyz.y));
}

vec2 __ctor_vec2(vec3 xyz) {
	return _make_vec2(float(xyz.x), float(xyz.y));
}

vec2 __ctor_vec2(bvec4 xyzw) {
	return _make_vec2(float(xyzw.x), float(xyzw.y));
}

vec2 __ctor_vec2(ivec4 xyzw) {
	return _make_vec2(float(xyzw.x), float(xyzw.y));
}

vec2 __ctor_vec2(uvec4 xyzw) {
	return _make_vec2(float(xyzw.x), float(xyzw.y));
}

vec2 __ctor_vec2(vec4 xyzw) {
	return _make_vec2(float(xyzw.x), float(xyzw.y));
}

bvec3 __ctor_bvec3(bool x) {
	return _make_bvec3(bool(x), bool(x), bool(x));
}

bvec3 __ctor_bvec3(int x) {
	return _make_bvec3(bool(x), bool(x), bool(x));
}

bvec3 __ctor_bvec3(uint x) {
	return _make_bvec3(bool(x), bool(x), bool(x));
}

bvec3 __ctor_bvec3(float x) {
	return _make_bvec3(bool(x), bool(x), bool(x));
}

bvec3 __ctor_bvec3(bool x, bool y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, bool y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, bool y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, bool y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, int y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, int y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, int y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, int y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, uint y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, uint y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, uint y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, uint y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, float y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, float y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, float y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, float y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, bool y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, bool y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, bool y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, bool y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, int y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, int y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, int y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, int y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, uint y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, uint y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, uint y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, uint y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, float y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, float y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, float y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(int x, float y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, bool y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, bool y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, bool y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, bool y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, int y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, int y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, int y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, int y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, uint y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, uint y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, uint y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, uint y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, float y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, float y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, float y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(uint x, float y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, bool y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, bool y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, bool y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, bool y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, int y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, int y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, int y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, int y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, uint y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, uint y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, uint y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, uint y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, float y, bool z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, float y, int z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, float y, uint z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(float x, float y, float z) {
	return _make_bvec3(bool(x), bool(y), bool(z));
}

bvec3 __ctor_bvec3(bool x, bvec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(bool x, ivec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(bool x, uvec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(bool x, vec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(int x, bvec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(int x, ivec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(int x, uvec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(int x, vec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(uint x, bvec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(uint x, ivec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(uint x, uvec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(uint x, vec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(float x, bvec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(float x, ivec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(float x, uvec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(float x, vec2 yz) {
	return _make_bvec3(bool(x), bool(yz.x), bool(yz.y));
}

bvec3 __ctor_bvec3(bvec2 xy, bool z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(bvec2 xy, int z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(bvec2 xy, uint z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(bvec2 xy, float z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(ivec2 xy, bool z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(ivec2 xy, int z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(ivec2 xy, uint z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(ivec2 xy, float z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(uvec2 xy, bool z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(uvec2 xy, int z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(uvec2 xy, uint z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(uvec2 xy, float z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(vec2 xy, bool z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(vec2 xy, int z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(vec2 xy, uint z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(vec2 xy, float z) {
	return _make_bvec3(bool(xy.x), bool(xy.y), bool(z));
}

bvec3 __ctor_bvec3(bvec3 xyz) {
	return _make_bvec3(bool(xyz.x), bool(xyz.y), bool(xyz.z));
}

bvec3 __ctor_bvec3(ivec3 xyz) {
	return _make_bvec3(bool(xyz.x), bool(xyz.y), bool(xyz.z));
}

bvec3 __ctor_bvec3(uvec3 xyz) {
	return _make_bvec3(bool(xyz.x), bool(xyz.y), bool(xyz.z));
}

bvec3 __ctor_bvec3(vec3 xyz) {
	return _make_bvec3(bool(xyz.x), bool(xyz.y), bool(xyz.z));
}

bvec3 __ctor_bvec3(bvec4 xyzw) {
	return _make_bvec3(bool(xyzw.x), bool(xyzw.y), bool(xyzw.z));
}

bvec3 __ctor_bvec3(ivec4 xyzw) {
	return _make_bvec3(bool(xyzw.x), bool(xyzw.y), bool(xyzw.z));
}

bvec3 __ctor_bvec3(uvec4 xyzw) {
	return _make_bvec3(bool(xyzw.x), bool(xyzw.y), bool(xyzw.z));
}

bvec3 __ctor_bvec3(vec4 xyzw) {
	return _make_bvec3(bool(xyzw.x), bool(xyzw.y), bool(xyzw.z));
}

ivec3 __ctor_ivec3(bool x) {
	return _make_ivec3(int(x), int(x), int(x));
}

ivec3 __ctor_ivec3(int x) {
	return _make_ivec3(int(x), int(x), int(x));
}

ivec3 __ctor_ivec3(uint x) {
	return _make_ivec3(int(x), int(x), int(x));
}

ivec3 __ctor_ivec3(float x) {
	return _make_ivec3(int(x), int(x), int(x));
}

ivec3 __ctor_ivec3(bool x, bool y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, bool y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, bool y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, bool y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, int y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, int y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, int y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, int y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, uint y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, uint y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, uint y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, uint y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, float y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, float y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, float y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, float y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, bool y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, bool y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, bool y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, bool y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, int y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, int y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, int y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, int y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, uint y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, uint y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, uint y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, uint y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, float y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, float y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, float y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(int x, float y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, bool y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, bool y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, bool y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, bool y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, int y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, int y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, int y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, int y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, uint y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, uint y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, uint y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, uint y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, float y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, float y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, float y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(uint x, float y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, bool y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, bool y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, bool y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, bool y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, int y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, int y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, int y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, int y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, uint y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, uint y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, uint y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, uint y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, float y, bool z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, float y, int z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, float y, uint z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(float x, float y, float z) {
	return _make_ivec3(int(x), int(y), int(z));
}

ivec3 __ctor_ivec3(bool x, bvec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(bool x, ivec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(bool x, uvec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(bool x, vec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(int x, bvec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(int x, ivec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(int x, uvec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(int x, vec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(uint x, bvec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(uint x, ivec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(uint x, uvec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(uint x, vec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(float x, bvec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(float x, ivec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(float x, uvec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(float x, vec2 yz) {
	return _make_ivec3(int(x), int(yz.x), int(yz.y));
}

ivec3 __ctor_ivec3(bvec2 xy, bool z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(bvec2 xy, int z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(bvec2 xy, uint z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(bvec2 xy, float z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(ivec2 xy, bool z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(ivec2 xy, int z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(ivec2 xy, uint z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(ivec2 xy, float z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(uvec2 xy, bool z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(uvec2 xy, int z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(uvec2 xy, uint z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(uvec2 xy, float z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(vec2 xy, bool z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(vec2 xy, int z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(vec2 xy, uint z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(vec2 xy, float z) {
	return _make_ivec3(int(xy.x), int(xy.y), int(z));
}

ivec3 __ctor_ivec3(bvec3 xyz) {
	return _make_ivec3(int(xyz.x), int(xyz.y), int(xyz.z));
}

ivec3 __ctor_ivec3(ivec3 xyz) {
	return _make_ivec3(int(xyz.x), int(xyz.y), int(xyz.z));
}

ivec3 __ctor_ivec3(uvec3 xyz) {
	return _make_ivec3(int(xyz.x), int(xyz.y), int(xyz.z));
}

ivec3 __ctor_ivec3(vec3 xyz) {
	return _make_ivec3(int(xyz.x), int(xyz.y), int(xyz.z));
}

ivec3 __ctor_ivec3(bvec4 xyzw) {
	return _make_ivec3(int(xyzw.x), int(xyzw.y), int(xyzw.z));
}

ivec3 __ctor_ivec3(ivec4 xyzw) {
	return _make_ivec3(int(xyzw.x), int(xyzw.y), int(xyzw.z));
}

ivec3 __ctor_ivec3(uvec4 xyzw) {
	return _make_ivec3(int(xyzw.x), int(xyzw.y), int(xyzw.z));
}

ivec3 __ctor_ivec3(vec4 xyzw) {
	return _make_ivec3(int(xyzw.x), int(xyzw.y), int(xyzw.z));
}

uvec3 __ctor_uvec3(bool x) {
	return _make_uvec3(uint(x), uint(x), uint(x));
}

uvec3 __ctor_uvec3(int x) {
	return _make_uvec3(uint(x), uint(x), uint(x));
}

uvec3 __ctor_uvec3(uint x) {
	return _make_uvec3(uint(x), uint(x), uint(x));
}

uvec3 __ctor_uvec3(float x) {
	return _make_uvec3(uint(x), uint(x), uint(x));
}

uvec3 __ctor_uvec3(bool x, bool y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, bool y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, bool y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, bool y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, int y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, int y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, int y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, int y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, uint y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, uint y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, uint y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, uint y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, float y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, float y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, float y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, float y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, bool y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, bool y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, bool y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, bool y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, int y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, int y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, int y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, int y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, uint y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, uint y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, uint y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, uint y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, float y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, float y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, float y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(int x, float y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, bool y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, bool y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, bool y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, bool y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, int y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, int y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, int y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, int y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, uint y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, uint y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, uint y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, uint y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, float y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, float y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, float y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(uint x, float y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, bool y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, bool y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, bool y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, bool y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, int y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, int y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, int y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, int y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, uint y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, uint y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, uint y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, uint y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, float y, bool z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, float y, int z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, float y, uint z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(float x, float y, float z) {
	return _make_uvec3(uint(x), uint(y), uint(z));
}

uvec3 __ctor_uvec3(bool x, bvec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(bool x, ivec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(bool x, uvec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(bool x, vec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(int x, bvec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(int x, ivec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(int x, uvec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(int x, vec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(uint x, bvec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(uint x, ivec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(uint x, uvec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(uint x, vec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(float x, bvec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(float x, ivec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(float x, uvec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(float x, vec2 yz) {
	return _make_uvec3(uint(x), uint(yz.x), uint(yz.y));
}

uvec3 __ctor_uvec3(bvec2 xy, bool z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(bvec2 xy, int z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(bvec2 xy, uint z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(bvec2 xy, float z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(ivec2 xy, bool z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(ivec2 xy, int z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(ivec2 xy, uint z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(ivec2 xy, float z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(uvec2 xy, bool z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(uvec2 xy, int z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(uvec2 xy, uint z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(uvec2 xy, float z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(vec2 xy, bool z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(vec2 xy, int z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(vec2 xy, uint z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(vec2 xy, float z) {
	return _make_uvec3(uint(xy.x), uint(xy.y), uint(z));
}

uvec3 __ctor_uvec3(bvec3 xyz) {
	return _make_uvec3(uint(xyz.x), uint(xyz.y), uint(xyz.z));
}

uvec3 __ctor_uvec3(ivec3 xyz) {
	return _make_uvec3(uint(xyz.x), uint(xyz.y), uint(xyz.z));
}

uvec3 __ctor_uvec3(uvec3 xyz) {
	return _make_uvec3(uint(xyz.x), uint(xyz.y), uint(xyz.z));
}

uvec3 __ctor_uvec3(vec3 xyz) {
	return _make_uvec3(uint(xyz.x), uint(xyz.y), uint(xyz.z));
}

uvec3 __ctor_uvec3(bvec4 xyzw) {
	return _make_uvec3(uint(xyzw.x), uint(xyzw.y), uint(xyzw.z));
}

uvec3 __ctor_uvec3(ivec4 xyzw) {
	return _make_uvec3(uint(xyzw.x), uint(xyzw.y), uint(xyzw.z));
}

uvec3 __ctor_uvec3(uvec4 xyzw) {
	return _make_uvec3(uint(xyzw.x), uint(xyzw.y), uint(xyzw.z));
}

uvec3 __ctor_uvec3(vec4 xyzw) {
	return _make_uvec3(uint(xyzw.x), uint(xyzw.y), uint(xyzw.z));
}

vec3 __ctor_vec3(bool x) {
	return _make_vec3(float(x), float(x), float(x));
}

vec3 __ctor_vec3(int x) {
	return _make_vec3(float(x), float(x), float(x));
}

vec3 __ctor_vec3(uint x) {
	return _make_vec3(float(x), float(x), float(x));
}

vec3 __ctor_vec3(float x) {
	return _make_vec3(float(x), float(x), float(x));
}

vec3 __ctor_vec3(bool x, bool y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, bool y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, bool y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, bool y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, int y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, int y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, int y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, int y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, uint y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, uint y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, uint y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, uint y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, float y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, float y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, float y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, float y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, bool y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, bool y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, bool y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, bool y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, int y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, int y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, int y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, int y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, uint y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, uint y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, uint y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, uint y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, float y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, float y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, float y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(int x, float y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, bool y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, bool y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, bool y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, bool y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, int y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, int y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, int y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, int y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, uint y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, uint y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, uint y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, uint y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, float y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, float y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, float y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(uint x, float y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, bool y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, bool y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, bool y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, bool y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, int y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, int y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, int y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, int y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, uint y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, uint y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, uint y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, uint y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, float y, bool z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, float y, int z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, float y, uint z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(float x, float y, float z) {
	return _make_vec3(float(x), float(y), float(z));
}

vec3 __ctor_vec3(bool x, bvec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(bool x, ivec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(bool x, uvec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(bool x, vec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(int x, bvec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(int x, ivec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(int x, uvec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(int x, vec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(uint x, bvec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(uint x, ivec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(uint x, uvec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(uint x, vec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(float x, bvec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(float x, ivec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(float x, uvec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(float x, vec2 yz) {
	return _make_vec3(float(x), float(yz.x), float(yz.y));
}

vec3 __ctor_vec3(bvec2 xy, bool z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(bvec2 xy, int z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(bvec2 xy, uint z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(bvec2 xy, float z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(ivec2 xy, bool z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(ivec2 xy, int z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(ivec2 xy, uint z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(ivec2 xy, float z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(uvec2 xy, bool z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(uvec2 xy, int z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(uvec2 xy, uint z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(uvec2 xy, float z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(vec2 xy, bool z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(vec2 xy, int z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(vec2 xy, uint z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(vec2 xy, float z) {
	return _make_vec3(float(xy.x), float(xy.y), float(z));
}

vec3 __ctor_vec3(bvec3 xyz) {
	return _make_vec3(float(xyz.x), float(xyz.y), float(xyz.z));
}

vec3 __ctor_vec3(ivec3 xyz) {
	return _make_vec3(float(xyz.x), float(xyz.y), float(xyz.z));
}

vec3 __ctor_vec3(uvec3 xyz) {
	return _make_vec3(float(xyz.x), float(xyz.y), float(xyz.z));
}

vec3 __ctor_vec3(vec3 xyz) {
	return _make_vec3(float(xyz.x), float(xyz.y), float(xyz.z));
}

vec3 __ctor_vec3(bvec4 xyzw) {
	return _make_vec3(float(xyzw.x), float(xyzw.y), float(xyzw.z));
}

vec3 __ctor_vec3(ivec4 xyzw) {
	return _make_vec3(float(xyzw.x), float(xyzw.y), float(xyzw.z));
}

vec3 __ctor_vec3(uvec4 xyzw) {
	return _make_vec3(float(xyzw.x), float(xyzw.y), float(xyzw.z));
}

vec3 __ctor_vec3(vec4 xyzw) {
	return _make_vec3(float(xyzw.x), float(xyzw.y), float(xyzw.z));
}

bvec4 __ctor_bvec4(bool x) {
	return _make_bvec4(bool(x), bool(x), bool(x), bool(x));
}

bvec4 __ctor_bvec4(int x) {
	return _make_bvec4(bool(x), bool(x), bool(x), bool(x));
}

bvec4 __ctor_bvec4(uint x) {
	return _make_bvec4(bool(x), bool(x), bool(x), bool(x));
}

bvec4 __ctor_bvec4(float x) {
	return _make_bvec4(bool(x), bool(x), bool(x), bool(x));
}

bvec4 __ctor_bvec4(bool x, bool y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, int y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, uint y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, float y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, bool y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, int y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, uint y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(int x, float y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, bool y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, int y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, uint y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uint x, float y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, bool y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, int y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, uint y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, bool z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, bool z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, bool z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, bool z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, int z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, int z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, int z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, int z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, uint z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, uint z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, uint z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, uint z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, float z, bool w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, float z, int w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, float z, uint w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(float x, float y, float z, float w) {
	return _make_bvec4(bool(x), bool(y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bool x, bool y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, bool y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, bool y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, bool y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, int y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, int y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, int y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, int y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, uint y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, uint y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, uint y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, uint y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, float y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, float y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, float y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, float y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, bool y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, bool y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, bool y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, bool y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, int y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, int y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, int y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, int y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, uint y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, uint y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, uint y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, uint y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, float y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, float y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, float y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(int x, float y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, bool y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, bool y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, bool y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, bool y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, int y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, int y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, int y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, int y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, uint y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, uint y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, uint y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, uint y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, float y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, float y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, float y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uint x, float y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, bool y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, bool y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, bool y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, bool y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, int y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, int y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, int y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, int y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, uint y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, uint y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, uint y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, uint y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, float y, bvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, float y, ivec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, float y, uvec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(float x, float y, vec2 zw) {
	return _make_bvec4(bool(x), bool(y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, bvec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, bvec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, bvec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, bvec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, ivec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, ivec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, ivec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, ivec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, uvec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, uvec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, uvec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, uvec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, vec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, vec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, vec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bool x, vec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, bvec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, bvec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, bvec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, bvec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, ivec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, ivec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, ivec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, ivec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, uvec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, uvec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, uvec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, uvec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, vec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, vec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, vec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(int x, vec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, bvec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, bvec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, bvec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, bvec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, ivec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, ivec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, ivec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, ivec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, uvec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, uvec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, uvec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, uvec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, vec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, vec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, vec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(uint x, vec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, bvec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, bvec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, bvec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, bvec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, ivec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, ivec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, ivec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, ivec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, uvec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, uvec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, uvec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, uvec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, vec2 yz, bool w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, vec2 yz, int w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, vec2 yz, uint w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(float x, vec2 yz, float w) {
	return _make_bvec4(bool(x), bool(yz.x), bool(yz.y), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, bool z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, bool z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, bool z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, bool z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, int z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, int z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, int z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, int z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, uint z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, uint z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, uint z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, uint z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, float z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, float z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, float z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, float z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, bool z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, bool z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, bool z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, bool z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, int z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, int z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, int z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, int z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, uint z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, uint z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, uint z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, uint z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, float z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, float z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, float z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(ivec2 xy, float z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, bool z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, bool z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, bool z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, bool z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, int z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, int z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, int z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, int z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, uint z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, uint z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, uint z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, uint z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, float z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, float z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, float z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(uvec2 xy, float z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, bool z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, bool z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, bool z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, bool z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, int z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, int z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, int z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, int z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, uint z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, uint z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, uint z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, uint z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, float z, bool w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, float z, int w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, float z, uint w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(vec2 xy, float z, float w) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(z), bool(w));
}

bvec4 __ctor_bvec4(bvec2 xy, bvec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bvec2 xy, ivec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bvec2 xy, uvec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bvec2 xy, vec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(ivec2 xy, bvec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(ivec2 xy, ivec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(ivec2 xy, uvec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(ivec2 xy, vec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uvec2 xy, bvec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uvec2 xy, ivec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uvec2 xy, uvec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(uvec2 xy, vec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(vec2 xy, bvec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(vec2 xy, ivec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(vec2 xy, uvec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(vec2 xy, vec2 zw) {
	return _make_bvec4(bool(xy.x), bool(xy.y), bool(zw.x), bool(zw.y));
}

bvec4 __ctor_bvec4(bool x, bvec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(bool x, ivec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(bool x, uvec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(bool x, vec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(int x, bvec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(int x, ivec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(int x, uvec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(int x, vec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(uint x, bvec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(uint x, ivec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(uint x, uvec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(uint x, vec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(float x, bvec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(float x, ivec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(float x, uvec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(float x, vec3 yzw) {
	return _make_bvec4(bool(x), bool(yzw.x), bool(yzw.y), bool(yzw.z));
}

bvec4 __ctor_bvec4(bvec3 xyz, bool w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(bvec3 xyz, int w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(bvec3 xyz, uint w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(bvec3 xyz, float w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(ivec3 xyz, bool w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(ivec3 xyz, int w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(ivec3 xyz, uint w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(ivec3 xyz, float w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(uvec3 xyz, bool w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(uvec3 xyz, int w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(uvec3 xyz, uint w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(uvec3 xyz, float w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(vec3 xyz, bool w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(vec3 xyz, int w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(vec3 xyz, uint w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(vec3 xyz, float w) {
	return _make_bvec4(bool(xyz.x), bool(xyz.y), bool(xyz.z), bool(w));
}

bvec4 __ctor_bvec4(bvec4 xyzw) {
	return _make_bvec4(bool(xyzw.x), bool(xyzw.y), bool(xyzw.z), bool(xyzw.w));
}

bvec4 __ctor_bvec4(ivec4 xyzw) {
	return _make_bvec4(bool(xyzw.x), bool(xyzw.y), bool(xyzw.z), bool(xyzw.w));
}

bvec4 __ctor_bvec4(uvec4 xyzw) {
	return _make_bvec4(bool(xyzw.x), bool(xyzw.y), bool(xyzw.z), bool(xyzw.w));
}

bvec4 __ctor_bvec4(vec4 xyzw) {
	return _make_bvec4(bool(xyzw.x), bool(xyzw.y), bool(xyzw.z), bool(xyzw.w));
}

ivec4 __ctor_ivec4(bool x) {
	return _make_ivec4(int(x), int(x), int(x), int(x));
}

ivec4 __ctor_ivec4(int x) {
	return _make_ivec4(int(x), int(x), int(x), int(x));
}

ivec4 __ctor_ivec4(uint x) {
	return _make_ivec4(int(x), int(x), int(x), int(x));
}

ivec4 __ctor_ivec4(float x) {
	return _make_ivec4(int(x), int(x), int(x), int(x));
}

ivec4 __ctor_ivec4(bool x, bool y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, int y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, uint y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, float y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, bool y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, int y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, uint y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(int x, float y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, bool y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, int y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, uint y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(uint x, float y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, bool y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, int y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, uint y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, bool z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, bool z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, bool z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, bool z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, int z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, int z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, int z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, int z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, uint z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, uint z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, uint z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, uint z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, float z, bool w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, float z, int w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, float z, uint w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(float x, float y, float z, float w) {
	return _make_ivec4(int(x), int(y), int(z), int(w));
}

ivec4 __ctor_ivec4(bool x, bool y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, bool y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, bool y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, bool y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, int y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, int y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, int y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, int y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, uint y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, uint y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, uint y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, uint y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, float y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, float y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, float y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, float y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, bool y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, bool y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, bool y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, bool y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, int y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, int y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, int y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, int y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, uint y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, uint y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, uint y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, uint y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, float y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, float y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, float y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(int x, float y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, bool y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, bool y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, bool y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, bool y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, int y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, int y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, int y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, int y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, uint y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, uint y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, uint y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, uint y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, float y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, float y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, float y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uint x, float y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, bool y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, bool y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, bool y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, bool y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, int y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, int y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, int y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, int y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, uint y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, uint y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, uint y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, uint y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, float y, bvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, float y, ivec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, float y, uvec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(float x, float y, vec2 zw) {
	return _make_ivec4(int(x), int(y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, bvec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, bvec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, bvec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, bvec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, ivec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, ivec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, ivec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, ivec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, uvec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, uvec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, uvec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, uvec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, vec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, vec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, vec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bool x, vec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, bvec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, bvec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, bvec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, bvec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, ivec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, ivec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, ivec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, ivec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, uvec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, uvec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, uvec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, uvec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, vec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, vec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, vec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(int x, vec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, bvec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, bvec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, bvec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, bvec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, ivec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, ivec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, ivec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, ivec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, uvec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, uvec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, uvec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, uvec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, vec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, vec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, vec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(uint x, vec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, bvec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, bvec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, bvec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, bvec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, ivec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, ivec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, ivec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, ivec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, uvec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, uvec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, uvec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, uvec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, vec2 yz, bool w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, vec2 yz, int w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, vec2 yz, uint w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(float x, vec2 yz, float w) {
	return _make_ivec4(int(x), int(yz.x), int(yz.y), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, bool z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, bool z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, bool z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, bool z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, int z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, int z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, int z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, int z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, uint z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, uint z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, uint z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, uint z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, float z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, float z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, float z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, float z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, bool z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, bool z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, bool z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, bool z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, int z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, int z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, int z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, int z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, uint z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, uint z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, uint z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, uint z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, float z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, float z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, float z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(ivec2 xy, float z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, bool z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, bool z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, bool z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, bool z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, int z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, int z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, int z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, int z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, uint z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, uint z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, uint z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, uint z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, float z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, float z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, float z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(uvec2 xy, float z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, bool z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, bool z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, bool z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, bool z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, int z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, int z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, int z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, int z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, uint z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, uint z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, uint z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, uint z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, float z, bool w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, float z, int w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, float z, uint w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(vec2 xy, float z, float w) {
	return _make_ivec4(int(xy.x), int(xy.y), int(z), int(w));
}

ivec4 __ctor_ivec4(bvec2 xy, bvec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bvec2 xy, ivec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bvec2 xy, uvec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bvec2 xy, vec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(ivec2 xy, bvec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(ivec2 xy, ivec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(ivec2 xy, uvec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(ivec2 xy, vec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uvec2 xy, bvec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uvec2 xy, ivec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uvec2 xy, uvec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(uvec2 xy, vec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(vec2 xy, bvec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(vec2 xy, ivec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(vec2 xy, uvec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(vec2 xy, vec2 zw) {
	return _make_ivec4(int(xy.x), int(xy.y), int(zw.x), int(zw.y));
}

ivec4 __ctor_ivec4(bool x, bvec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(bool x, ivec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(bool x, uvec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(bool x, vec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(int x, bvec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(int x, ivec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(int x, uvec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(int x, vec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(uint x, bvec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(uint x, ivec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(uint x, uvec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(uint x, vec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(float x, bvec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(float x, ivec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(float x, uvec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(float x, vec3 yzw) {
	return _make_ivec4(int(x), int(yzw.x), int(yzw.y), int(yzw.z));
}

ivec4 __ctor_ivec4(bvec3 xyz, bool w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(bvec3 xyz, int w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(bvec3 xyz, uint w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(bvec3 xyz, float w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(ivec3 xyz, bool w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(ivec3 xyz, int w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(ivec3 xyz, uint w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(ivec3 xyz, float w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(uvec3 xyz, bool w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(uvec3 xyz, int w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(uvec3 xyz, uint w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(uvec3 xyz, float w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(vec3 xyz, bool w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(vec3 xyz, int w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(vec3 xyz, uint w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(vec3 xyz, float w) {
	return _make_ivec4(int(xyz.x), int(xyz.y), int(xyz.z), int(w));
}

ivec4 __ctor_ivec4(bvec4 xyzw) {
	return _make_ivec4(int(xyzw.x), int(xyzw.y), int(xyzw.z), int(xyzw.w));
}

ivec4 __ctor_ivec4(ivec4 xyzw) {
	return _make_ivec4(int(xyzw.x), int(xyzw.y), int(xyzw.z), int(xyzw.w));
}

ivec4 __ctor_ivec4(uvec4 xyzw) {
	return _make_ivec4(int(xyzw.x), int(xyzw.y), int(xyzw.z), int(xyzw.w));
}

ivec4 __ctor_ivec4(vec4 xyzw) {
	return _make_ivec4(int(xyzw.x), int(xyzw.y), int(xyzw.z), int(xyzw.w));
}

uvec4 __ctor_uvec4(bool x) {
	return _make_uvec4(uint(x), uint(x), uint(x), uint(x));
}

uvec4 __ctor_uvec4(int x) {
	return _make_uvec4(uint(x), uint(x), uint(x), uint(x));
}

uvec4 __ctor_uvec4(uint x) {
	return _make_uvec4(uint(x), uint(x), uint(x), uint(x));
}

uvec4 __ctor_uvec4(float x) {
	return _make_uvec4(uint(x), uint(x), uint(x), uint(x));
}

uvec4 __ctor_uvec4(bool x, bool y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, int y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, uint y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, float y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, bool y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, int y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, uint y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(int x, float y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, bool y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, int y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, uint y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uint x, float y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, bool y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, int y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, uint y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, bool z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, bool z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, bool z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, bool z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, int z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, int z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, int z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, int z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, uint z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, uint z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, uint z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, uint z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, float z, bool w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, float z, int w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, float z, uint w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(float x, float y, float z, float w) {
	return _make_uvec4(uint(x), uint(y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bool x, bool y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, bool y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, bool y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, bool y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, int y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, int y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, int y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, int y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, uint y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, uint y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, uint y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, uint y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, float y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, float y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, float y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, float y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, bool y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, bool y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, bool y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, bool y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, int y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, int y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, int y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, int y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, uint y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, uint y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, uint y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, uint y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, float y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, float y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, float y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(int x, float y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, bool y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, bool y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, bool y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, bool y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, int y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, int y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, int y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, int y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, uint y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, uint y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, uint y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, uint y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, float y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, float y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, float y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uint x, float y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, bool y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, bool y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, bool y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, bool y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, int y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, int y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, int y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, int y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, uint y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, uint y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, uint y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, uint y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, float y, bvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, float y, ivec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, float y, uvec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(float x, float y, vec2 zw) {
	return _make_uvec4(uint(x), uint(y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, bvec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, bvec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, bvec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, bvec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, ivec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, ivec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, ivec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, ivec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, uvec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, uvec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, uvec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, uvec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, vec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, vec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, vec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bool x, vec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, bvec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, bvec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, bvec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, bvec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, ivec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, ivec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, ivec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, ivec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, uvec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, uvec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, uvec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, uvec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, vec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, vec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, vec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(int x, vec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, bvec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, bvec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, bvec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, bvec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, ivec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, ivec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, ivec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, ivec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, uvec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, uvec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, uvec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, uvec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, vec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, vec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, vec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(uint x, vec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, bvec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, bvec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, bvec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, bvec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, ivec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, ivec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, ivec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, ivec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, uvec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, uvec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, uvec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, uvec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, vec2 yz, bool w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, vec2 yz, int w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, vec2 yz, uint w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(float x, vec2 yz, float w) {
	return _make_uvec4(uint(x), uint(yz.x), uint(yz.y), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, bool z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, bool z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, bool z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, bool z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, int z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, int z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, int z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, int z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, uint z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, uint z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, uint z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, uint z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, float z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, float z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, float z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, float z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, bool z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, bool z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, bool z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, bool z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, int z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, int z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, int z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, int z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, uint z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, uint z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, uint z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, uint z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, float z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, float z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, float z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(ivec2 xy, float z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, bool z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, bool z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, bool z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, bool z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, int z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, int z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, int z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, int z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, uint z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, uint z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, uint z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, uint z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, float z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, float z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, float z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(uvec2 xy, float z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, bool z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, bool z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, bool z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, bool z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, int z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, int z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, int z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, int z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, uint z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, uint z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, uint z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, uint z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, float z, bool w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, float z, int w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, float z, uint w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(vec2 xy, float z, float w) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(z), uint(w));
}

uvec4 __ctor_uvec4(bvec2 xy, bvec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bvec2 xy, ivec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bvec2 xy, uvec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bvec2 xy, vec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(ivec2 xy, bvec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(ivec2 xy, ivec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(ivec2 xy, uvec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(ivec2 xy, vec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uvec2 xy, bvec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uvec2 xy, ivec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uvec2 xy, uvec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(uvec2 xy, vec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(vec2 xy, bvec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(vec2 xy, ivec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(vec2 xy, uvec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(vec2 xy, vec2 zw) {
	return _make_uvec4(uint(xy.x), uint(xy.y), uint(zw.x), uint(zw.y));
}

uvec4 __ctor_uvec4(bool x, bvec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(bool x, ivec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(bool x, uvec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(bool x, vec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(int x, bvec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(int x, ivec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(int x, uvec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(int x, vec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(uint x, bvec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(uint x, ivec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(uint x, uvec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(uint x, vec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(float x, bvec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(float x, ivec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(float x, uvec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(float x, vec3 yzw) {
	return _make_uvec4(uint(x), uint(yzw.x), uint(yzw.y), uint(yzw.z));
}

uvec4 __ctor_uvec4(bvec3 xyz, bool w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(bvec3 xyz, int w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(bvec3 xyz, uint w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(bvec3 xyz, float w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(ivec3 xyz, bool w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(ivec3 xyz, int w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(ivec3 xyz, uint w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(ivec3 xyz, float w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(uvec3 xyz, bool w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(uvec3 xyz, int w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(uvec3 xyz, uint w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(uvec3 xyz, float w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(vec3 xyz, bool w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(vec3 xyz, int w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(vec3 xyz, uint w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(vec3 xyz, float w) {
	return _make_uvec4(uint(xyz.x), uint(xyz.y), uint(xyz.z), uint(w));
}

uvec4 __ctor_uvec4(bvec4 xyzw) {
	return _make_uvec4(uint(xyzw.x), uint(xyzw.y), uint(xyzw.z), uint(xyzw.w));
}

uvec4 __ctor_uvec4(ivec4 xyzw) {
	return _make_uvec4(uint(xyzw.x), uint(xyzw.y), uint(xyzw.z), uint(xyzw.w));
}

uvec4 __ctor_uvec4(uvec4 xyzw) {
	return _make_uvec4(uint(xyzw.x), uint(xyzw.y), uint(xyzw.z), uint(xyzw.w));
}

uvec4 __ctor_uvec4(vec4 xyzw) {
	return _make_uvec4(uint(xyzw.x), uint(xyzw.y), uint(xyzw.z), uint(xyzw.w));
}

vec4 __ctor_vec4(bool x) {
	return _make_vec4(float(x), float(x), float(x), float(x));
}

vec4 __ctor_vec4(int x) {
	return _make_vec4(float(x), float(x), float(x), float(x));
}

vec4 __ctor_vec4(uint x) {
	return _make_vec4(float(x), float(x), float(x), float(x));
}

vec4 __ctor_vec4(float x) {
	return _make_vec4(float(x), float(x), float(x), float(x));
}

vec4 __ctor_vec4(bool x, bool y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, int y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, uint y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, float y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, bool y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, int y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, uint y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(int x, float y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, bool y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, int y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, uint y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(uint x, float y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, bool y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, int y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, uint y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, bool z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, bool z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, bool z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, bool z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, int z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, int z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, int z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, int z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, uint z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, uint z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, uint z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, uint z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, float z, bool w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, float z, int w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, float z, uint w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(float x, float y, float z, float w) {
	return _make_vec4(float(x), float(y), float(z), float(w));
}

vec4 __ctor_vec4(bool x, bool y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, bool y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, bool y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, bool y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, int y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, int y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, int y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, int y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, uint y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, uint y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, uint y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, uint y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, float y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, float y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, float y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, float y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, bool y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, bool y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, bool y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, bool y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, int y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, int y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, int y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, int y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, uint y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, uint y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, uint y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, uint y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, float y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, float y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, float y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(int x, float y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, bool y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, bool y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, bool y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, bool y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, int y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, int y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, int y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, int y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, uint y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, uint y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, uint y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, uint y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, float y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, float y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, float y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uint x, float y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, bool y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, bool y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, bool y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, bool y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, int y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, int y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, int y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, int y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, uint y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, uint y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, uint y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, uint y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, float y, bvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, float y, ivec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, float y, uvec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(float x, float y, vec2 zw) {
	return _make_vec4(float(x), float(y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, bvec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, bvec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, bvec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, bvec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, ivec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, ivec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, ivec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, ivec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, uvec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, uvec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, uvec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, uvec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, vec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, vec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, vec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bool x, vec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, bvec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, bvec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, bvec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, bvec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, ivec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, ivec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, ivec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, ivec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, uvec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, uvec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, uvec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, uvec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, vec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, vec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, vec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(int x, vec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, bvec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, bvec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, bvec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, bvec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, ivec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, ivec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, ivec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, ivec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, uvec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, uvec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, uvec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, uvec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, vec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, vec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, vec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(uint x, vec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, bvec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, bvec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, bvec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, bvec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, ivec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, ivec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, ivec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, ivec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, uvec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, uvec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, uvec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, uvec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, vec2 yz, bool w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, vec2 yz, int w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, vec2 yz, uint w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(float x, vec2 yz, float w) {
	return _make_vec4(float(x), float(yz.x), float(yz.y), float(w));
}

vec4 __ctor_vec4(bvec2 xy, bool z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, bool z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, bool z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, bool z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, int z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, int z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, int z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, int z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, uint z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, uint z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, uint z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, uint z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, float z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, float z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, float z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, float z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, bool z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, bool z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, bool z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, bool z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, int z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, int z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, int z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, int z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, uint z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, uint z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, uint z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, uint z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, float z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, float z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, float z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(ivec2 xy, float z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, bool z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, bool z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, bool z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, bool z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, int z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, int z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, int z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, int z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, uint z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, uint z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, uint z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, uint z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, float z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, float z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, float z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(uvec2 xy, float z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, bool z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, bool z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, bool z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, bool z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, int z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, int z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, int z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, int z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, uint z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, uint z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, uint z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, uint z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, float z, bool w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, float z, int w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, float z, uint w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(vec2 xy, float z, float w) {
	return _make_vec4(float(xy.x), float(xy.y), float(z), float(w));
}

vec4 __ctor_vec4(bvec2 xy, bvec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bvec2 xy, ivec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bvec2 xy, uvec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bvec2 xy, vec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(ivec2 xy, bvec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(ivec2 xy, ivec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(ivec2 xy, uvec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(ivec2 xy, vec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uvec2 xy, bvec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uvec2 xy, ivec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uvec2 xy, uvec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(uvec2 xy, vec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(vec2 xy, bvec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(vec2 xy, ivec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(vec2 xy, uvec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(vec2 xy, vec2 zw) {
	return _make_vec4(float(xy.x), float(xy.y), float(zw.x), float(zw.y));
}

vec4 __ctor_vec4(bool x, bvec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(bool x, ivec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(bool x, uvec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(bool x, vec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(int x, bvec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(int x, ivec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(int x, uvec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(int x, vec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(uint x, bvec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(uint x, ivec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(uint x, uvec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(uint x, vec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(float x, bvec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(float x, ivec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(float x, uvec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(float x, vec3 yzw) {
	return _make_vec4(float(x), float(yzw.x), float(yzw.y), float(yzw.z));
}

vec4 __ctor_vec4(bvec3 xyz, bool w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(bvec3 xyz, int w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(bvec3 xyz, uint w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(bvec3 xyz, float w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(ivec3 xyz, bool w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(ivec3 xyz, int w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(ivec3 xyz, uint w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(ivec3 xyz, float w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(uvec3 xyz, bool w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(uvec3 xyz, int w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(uvec3 xyz, uint w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(uvec3 xyz, float w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(vec3 xyz, bool w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(vec3 xyz, int w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(vec3 xyz, uint w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(vec3 xyz, float w) {
	return _make_vec4(float(xyz.x), float(xyz.y), float(xyz.z), float(w));
}

vec4 __ctor_vec4(bvec4 xyzw) {
	return _make_vec4(float(xyzw.x), float(xyzw.y), float(xyzw.z), float(xyzw.w));
}

vec4 __ctor_vec4(ivec4 xyzw) {
	return _make_vec4(float(xyzw.x), float(xyzw.y), float(xyzw.z), float(xyzw.w));
}

vec4 __ctor_vec4(uvec4 xyzw) {
	return _make_vec4(float(xyzw.x), float(xyzw.y), float(xyzw.z), float(xyzw.w));
}

vec4 __ctor_vec4(vec4 xyzw) {
	return _make_vec4(float(xyzw.x), float(xyzw.y), float(xyzw.z), float(xyzw.w));
}

