
// define _VAL_TYPE
// define _VEC_TYPE_BASE

#define _VEC_SIZE 2
#define _VEC_TYPE __tokenpaste(_VEC_TYPE_BASE, 2)
#pragma i3d_include <vector>

#define _VEC_SIZE 3
#define _VEC_TYPE __tokenpaste(_VEC_TYPE_BASE, 3)
#pragma i3d_include <vector>

#define _VEC_SIZE 4
#define _VEC_TYPE __tokenpaste(_VEC_TYPE_BASE, 4)
#pragma i3d_include <vector>

#pragma i3d_include <swizzles>
