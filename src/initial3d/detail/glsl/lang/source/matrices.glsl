
// define _VAL_TYPE
// define _VEC_TYPE_BASE
// define _MAT_TYPE_BASE

#define _MAT_COLS 2
#define _MAT_ROWS 2
#define _VEC_TYPE __tokenpaste(_VEC_TYPE_BASE, 2)
#define _MAT_TYPE __tokenpaste(_MAT_TYPE_BASE, 2)
#pragma i3d_include <matrix>

#define _MAT_COLS 3
#define _MAT_ROWS 3
#define _VEC_TYPE __tokenpaste(_VEC_TYPE_BASE, 3)
#define _MAT_TYPE __tokenpaste(_MAT_TYPE_BASE, 3)
#pragma i3d_include <matrix>

#define _MAT_COLS 4
#define _MAT_ROWS 4
#define _VEC_TYPE __tokenpaste(_VEC_TYPE_BASE, 4)
#define _MAT_TYPE __tokenpaste(_MAT_TYPE_BASE, 4)
#pragma i3d_include <matrix>

// TODO non-square matrices
