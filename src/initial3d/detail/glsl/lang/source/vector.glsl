
struct _VEC_TYPE __vector {
	
	_VAL_TYPE _data[_VEC_SIZE];
	
	__swizzle _VAL_TYPE x, r, s : _data[0];
	__swizzle _VAL_TYPE y, g, t : _data[1];
#if _VEC_SIZE > 2
	__swizzle _VAL_TYPE z, b, p : _data[2];
#endif
#if _VEC_SIZE > 3
	__swizzle _VAL_TYPE w, a, q : _data[3];
#endif
	
	const __ref _VAL_TYPE operator[](const __ref int i) const {
		return _data[i];
	}
	
	__ref _VAL_TYPE operator[](const __ref int i) {
		return _data[i];
	}
};

_VEC_TYPE __common_type(_VEC_TYPE a, _VEC_TYPE b);

// Xvec2 ctor helpers
#if _VEC_SIZE == 2

_VEC_TYPE __tokenpaste(_make_, _VEC_TYPE) (const __ref _VAL_TYPE x_, const __ref _VAL_TYPE y_) {
	_VEC_TYPE v;
	v.x = x_;
	v.y = y_;
	return v;
}

#endif

// Xvec3 ctor helpers
#if _VEC_SIZE == 3

_VEC_TYPE __tokenpaste(_make_, _VEC_TYPE) (const __ref _VAL_TYPE x_, const __ref _VAL_TYPE y_, const __ref _VAL_TYPE z_) {
	_VEC_TYPE v;
	v.x = x_;
	v.y = y_;
	v.z = z_;
	return v;
}

#endif

// Xvec4 ctor helpers
#if _VEC_SIZE == 4

_VEC_TYPE __tokenpaste(_make_, _VEC_TYPE) (const __ref _VAL_TYPE x_, const __ref _VAL_TYPE y_, const __ref _VAL_TYPE z_, const __ref _VAL_TYPE w_) {
	_VEC_TYPE v;
	v.x = x_;
	v.y = y_;
	v.z = z_;
	v.w = w_;
	return v;
}

#endif

#define IMPL_UNARY_ELEMENTWISE(func) \
	_VEC_TYPE func (_VEC_TYPE a) { \
		_VEC_TYPE v; \
		for __unroll (int i = 0; i < _VEC_SIZE; ++i) { \
			v[i] = func (a[i]); \
		} \
		return v; \
	}

#define IMPL_BINARY_ELEMENTWISE(func) \
	_VEC_TYPE func (_VEC_TYPE a, _VEC_TYPE b) { \
		_VEC_TYPE v; \
		for __unroll (int i = 0; i < _VEC_SIZE; ++i) { \
			v[i] = func (a[i], b[i]); \
		} \
		return v; \
	} \
	_VEC_TYPE func (_VEC_TYPE a, _VAL_TYPE b) { \
		_VEC_TYPE v; \
		for __unroll (int i = 0; i < _VEC_SIZE; ++i) { \
			v[i] = func (a[i], b); \
		} \
		return v; \
	} \
	_VEC_TYPE func (_VAL_TYPE a, _VEC_TYPE b) { \
		_VEC_TYPE v; \
		for __unroll (int i = 0; i < _VEC_SIZE; ++i) { \
			v[i] = func (a, b[i]); \
		} \
		return v; \
	}

#define IMPL_BINARY_ELEMENTWISE_ASSIGN(func) \
	__ref _VEC_TYPE func (__ref _VEC_TYPE a, _VEC_TYPE b) { \
		for __unroll (int i = 0; i < _VEC_SIZE; ++i) { \
			a[i] = func (a[i], b[i]); \
		} \
		return a; \
	} \
	__ref _VEC_TYPE func (__ref _VEC_TYPE a, _VAL_TYPE b) { \
		for __unroll (int i = 0; i < _VEC_SIZE; ++i) { \
			a[i] = func (a[i], b); \
		} \
		return a; \
	}

#if _VEC_ARITH

__constexpr __arith IMPL_BINARY_ELEMENTWISE(operator+)
__constexpr __arith IMPL_BINARY_ELEMENTWISE(operator-)
__constexpr __arith IMPL_BINARY_ELEMENTWISE(operator*)
__constexpr __arith IMPL_BINARY_ELEMENTWISE(operator/)
__constexpr __arith IMPL_BINARY_ELEMENTWISE(operator%)

__constexpr __arith IMPL_BINARY_ELEMENTWISE_ASSIGN(operator+=)
__constexpr __arith IMPL_BINARY_ELEMENTWISE_ASSIGN(operator-=)
__constexpr __arith IMPL_BINARY_ELEMENTWISE_ASSIGN(operator*=)
__constexpr __arith IMPL_BINARY_ELEMENTWISE_ASSIGN(operator/=)
__constexpr __arith IMPL_BINARY_ELEMENTWISE_ASSIGN(operator%=)

__constexpr IMPL_BINARY_ELEMENTWISE(min)
__constexpr IMPL_BINARY_ELEMENTWISE(max)
__constexpr IMPL_UNARY_ELEMENTWISE(abs)

__constexpr _VEC_TYPE clamp(_VEC_TYPE x, _VEC_TYPE l, _VEC_TYPE u) {
	_VEC_TYPE v;
	for __unroll (int i = 0; i < _VEC_SIZE; ++i) {
		v[i] = clamp(x[i], l[i], u[i]);
	}
	return v;
}

__constexpr _VEC_TYPE clamp(_VEC_TYPE x, _VAL_TYPE l, _VEC_TYPE u) {
	_VEC_TYPE v;
	for __unroll (int i = 0; i < _VEC_SIZE; ++i) {
		v[i] = clamp(x[i], l, u[i]);
	}
	return v;
}

__constexpr _VEC_TYPE clamp(_VEC_TYPE x, _VEC_TYPE l, _VAL_TYPE u) {
	_VEC_TYPE v;
	for __unroll (int i = 0; i < _VEC_SIZE; ++i) {
		v[i] = clamp(x[i], l[i], u);
	}
	return v;
}

__constexpr _VEC_TYPE clamp(_VEC_TYPE x, _VAL_TYPE l, _VAL_TYPE u) {
	_VEC_TYPE v;
	for __unroll (int i = 0; i < _VEC_SIZE; ++i) {
		v[i] = clamp(x[i], l, u);
	}
	return v;
}

__constexpr _VAL_TYPE dot(_VEC_TYPE a, _VEC_TYPE b) {
	_VAL_TYPE r = _VAL_TYPE(0);
	for __unroll (int i = 0; i < _VEC_SIZE; ++i) {
		r += a[i] * b[i];
	}
	return r;
}

#if _VEC_SIZE == 3
__constexpr _VEC_TYPE cross(_VEC_TYPE a, _VEC_TYPE b) {
	_VEC_TYPE v;
	v.x = a.y * b.z - a.z * b.y;
	v.y = a.z * b.x - a.x * b.z;
	v.z = a.x * b.y - a.y * b.x;
	return v;
}
#endif

#endif

// TODO logic and bitwise ops









