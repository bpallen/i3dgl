// fragment shader environment header

layout(__fragdata) out;

layout(__special) in vec4 gl_FragCoord;

layout(__special) out float gl_FragDepth;
