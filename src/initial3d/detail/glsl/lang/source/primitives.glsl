
//
// bool constructors
//

__constexpr bool __ctor_bool(bool x) { return x; }
__constexpr bool __ctor_bool(int x) { return x != 0; }
__constexpr bool __ctor_bool(uint x) { return x != 0u; }
__constexpr bool __ctor_bool(float x) { return x != 0.0; }

//
// int constructors
//

__constexpr int __ctor_int(bool x) { return x ? 1 : 0; }
__constexpr int __ctor_int(int x) { return x; }	
	
//
// uint constructors
//

__constexpr uint __ctor_uint(bool x) { return x ? 1u : 0u; }
__constexpr uint __ctor_uint(uint x) { return x; }

//
// float constructors
//

__constexpr float __ctor_float(bool x) { return x ? 1.0 : 0.0; }
__constexpr float __ctor_float(float x) { return x; }

__constexpr float __ctor__implicit_float(int a) {
	return __ctor_float(a);
}

__constexpr float __ctor__implicit_float(uint a) {
	return __ctor_float(a);
}

//
// bool operators
//

__constexpr __ref bool operator=(__ref bool a, const __ref bool b) {
	__builtin_assign(a, b);
	return a;
}

__constexpr __ref bool operator&=(__ref bool a, bool b) {
	return a = a && b;
}

__constexpr __ref bool operator|=(__ref bool a, bool b) {
	return a = a || b;
}

__constexpr __ref bool operator^=(__ref bool a, bool b) {
	return a = a ^^ b;
}

//
// int operators
//

__constexpr __ref int operator=(__ref int a, const __ref int b) {
	__builtin_assign(a, b);
	return a;
}

__constexpr __ref int operator++(__ref int a) {
	a = a + 1;
	return a;
}

__constexpr __ref int operator--(__ref int a) {
	a = a - 1;
	return a;
}

__constexpr __ref int operator+=(__ref int a, int b) {
	return a = a + b;
}

__constexpr __ref int operator-=(__ref int a, int b) {
	return a = a - b;
}

__constexpr __ref int operator*=(__ref int a, int b) {
	return a = a * b;
}

__constexpr __ref int operator/=(__ref int a, int b) {
	return a = a / b;
} 

__constexpr __ref int operator%=(__ref int a, int b) {
	return a = a % b;
}

__constexpr __ref int operator<<=(__ref int a, int b) {
	return a = a << b;
}

__constexpr __ref int operator>>=(__ref int a, int b) {
	return a = a >> b;
}

__constexpr __ref int operator&=(__ref int a, int b) {
	return a = a & b;
}

__constexpr __ref int operator|=(__ref int a, int b) {
	return a = a | b;
}

__constexpr __ref int operator^=(__ref int a, int b) {
	return a = a ^ b;
}

//
// uint operators
//

__constexpr __ref uint operator=(__ref uint a, const __ref uint b) {
	__builtin_assign(a, b);
	return a;
}

__constexpr __ref uint operator++(__ref uint a) {
	a = a + 1u;
	return a;
}

__constexpr __ref uint operator--(__ref uint a) {
	a = a - 1u;
	return a;
}

__constexpr __ref uint operator+=(__ref uint a, uint b) {
	return a = a + b;
}

__constexpr __ref uint operator-=(__ref uint a, uint b) {
	return a = a - b;
}

__constexpr __ref uint operator*=(__ref uint a, uint b) {
	return a = a * b;
}

__constexpr __ref uint operator/=(__ref uint a, uint b) {
	return a = a / b;
} 

__constexpr __ref uint operator%=(__ref uint a, uint b) {
	return a = a % b;
}

__constexpr __ref uint operator<<=(__ref uint a, uint b) {
	return a = a << b;
}

__constexpr __ref uint operator>>=(__ref uint a, uint b) {
	return a = a >> b;
}

__constexpr __ref uint operator&=(__ref uint a, uint b) {
	return a = a & b;
}

__constexpr __ref uint operator|=(__ref uint a, uint b) {
	return a = a | b;
}

__constexpr __ref uint operator^=(__ref uint a, uint b) {
	return a = a ^ b;
}

//
// float operators
//

__constexpr __ref float operator=(__ref float a, const __ref float b) {
	__builtin_assign(a, b);
	return a;
}

__constexpr __ref float operator++(__ref float a) {
	a = a + 1.0;
	return a;
}

__constexpr __ref float operator--(__ref float a) {
	a = a - 1.0;
	return a;
}

__constexpr __ref float operator+=(__ref float a, float b) {
	return a = a + b;
}

__constexpr __ref float operator-=(__ref float a, float b) {
	return a = a - b;
}

__constexpr __ref float operator*=(__ref float a, float b) {
	return a = a * b;
}

__constexpr __ref float operator/=(__ref float a, float b) {
	return a = a / b;
} 

__constexpr __ref float operator%=(__ref float a, float b) {
	return a = a % b;
}

//
// functions
//

__constexpr int clamp(int x, int l, int u) {
	return max(min(x, u), l);
}

__constexpr uint clamp(uint x, uint l, uint u) {
	return max(min(x, u), l);
}

__constexpr float clamp(float x, float l, float u) {
	return max(min(x, u), l);
}



