
struct _MAT_TYPE __matrix {
	
	_VEC_TYPE _data[_MAT_COLS];
	
	const __ref _VEC_TYPE operator[](const __ref int i) const {
		return _data[i];
	}
	
	__ref _VEC_TYPE operator[](const __ref int i) {
		return _data[i];
	}
	
};

// Xmat2[xN] ctors
#if _MAT_COLS == 2

_MAT_TYPE __tokenpaste(__ctor_, _MAT_TYPE) (_VEC_TYPE c0, _VEC_TYPE c1) {
	_MAT_TYPE m;
	m[0] = c0;
	m[1] = c1;
	return m;
}

#endif

// Xmat3[xN] ctors
#if _MAT_COLS == 3

_MAT_TYPE __tokenpaste(__ctor_, _MAT_TYPE) (_VEC_TYPE c0, _VEC_TYPE c1, _VEC_TYPE c2) {
	_MAT_TYPE m;
	m[0] = c0;
	m[1] = c1;
	m[2] = c2;
	return m;
}

#endif

// Xmat3[xN] ctors
#if _MAT_COLS == 4

_MAT_TYPE __tokenpaste(__ctor_, _MAT_TYPE) (_VEC_TYPE c0, _VEC_TYPE c1, _VEC_TYPE c2, _VEC_TYPE c3) {
	_MAT_TYPE m;
	m[0] = c0;
	m[1] = c1;
	m[2] = c2;
	m[3] = c3;
	return m;
}

#endif

__constexpr _VEC_TYPE operator*(const __ref _MAT_TYPE m, const __ref __tokenpaste(_VEC_TYPE_BASE, _MAT_COLS) v) {
	_VEC_TYPE r = _VEC_TYPE(0);
	for __unroll (int i = 0; i < _MAT_COLS; ++i) {
		r += m[i] * v[i];
	}
	return r;
}



