package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.intermediate.Allocator;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.locations.VoidLocation;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

public class BuiltinType extends Type {

	private Class<? extends Instance> instcls;
	private Class<?> equiv;
	
	public BuiltinType(TypeName name_, Class<? extends Instance> instcls_, Class<?> equiv_) {
		super(name_, new Scope(name_.scope()));
		instcls = instcls_;
		equiv = equiv_;
	}

	@Override
	public Instance instantiate(Token source, TypeName actual_type, Scope scope, boolean lvalue) {
		try {
			return instcls.getConstructor(Token.class, Scope.class, boolean.class).newInstance(source, scope, lvalue);
		} catch (Exception e) {
			throw new AssertionError(e);
		}
	}
	
	@Override
	public Location allocate(Allocator alloc, TypeName actual_type, Layout layout, Location parent) {
		if (name().plainText().equals("void")) return new VoidLocation();
		return alloc.allocPrimitive(this, layout, parent);
	}
	
	@Override
	public Class<?> javaEquivalent() {
		return equiv;
	}
	
	public int allocSize(Layout l) {
		if (name().plainText().equals("void")) return 0;
		// TODO proper primitive sizes?
		return 4;
	}
	
	@Override
	public int allocAlign(Layout l) {
		return l == null ? allocSize(l) : l.primitiveAlign(this);
	}
	
}
