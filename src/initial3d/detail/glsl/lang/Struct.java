package initial3d.detail.glsl.lang;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Allocator;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.locations.StructLocation;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.MemberAccess;
import initial3d.detail.glsl.lang.expressions.StructInstance;
import initial3d.detail.glsl.lang.names.BlockName;
import initial3d.detail.glsl.lang.names.FunctionName;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.lang.statements.CompoundStatement;
import initial3d.detail.glsl.lang.statements.ExpressionStatement;
import initial3d.detail.glsl.lang.statements.ReturnStatement;
import initial3d.detail.glsl.lang.statements.ScopedStatement;
import initial3d.detail.glsl.lang.statements.VariableDeclaration;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenBuilder;

public class Struct extends Type {

	private Statement body;
	private BlockName block;
	private ArrayList<Runnable> build_tasks = new ArrayList<>();
	
	/**
	 * takes ownership of the member scope and allows declarations in it.
	 * 
	 * @param type_
	 * @param member_scope_
	 * @param body_
	 */
	public Struct(ScalarTypeName type_, Scope member_scope_, Statement body_) {
		super(type_, member_scope_);
		body = new ScopedStatement(body_, member_scope_);
		member_scope_.owner(this);
		member_scope_.allowDeclarations();
	}
	
	@Override
	public BlockName block() {
		return block;
	}
	
	public void block(BlockName b) {
		block = b;
	}
	
	/**
	 * Invoke when the struct definition is complete to generate constructor and assignment,
	 * and execute other build tasks like deferred member function parsing.
	 */
	public void build(InfoLog infolog) {
		generateConstructor(infolog);
		generateAssignment(infolog);
		for (Runnable r : build_tasks) r.run();
	}
	
	public void addBuildTask(Runnable r) {
		build_tasks.add(Objects.requireNonNull(r));
	}
	
	public void generateConstructor(InfoLog infolog) {
		ScalarTypeName stype = (ScalarTypeName) name();
		infolog.pushContext("generating struct assignment", source());
		try {
			Token t0 = new TokenBuilder(source()).fileName("<generated>").toToken();
			// param types from struct fields
			List<VariableName> field_list = memberScope().localDeclaredNames(VariableName.class);
			TypeName[] ptypes = new TypeName[field_list.size()];
			for (int i = 0; i < ptypes.length; i++) {
				ptypes[i] = (TypeName) field_list.get(i).type().copy().qualify("__ref").qualify("const");
			}
			// function name (namespace scope, not member scope)
			FunctionName fname = new FunctionName(
				t0.withText("__ctor_" + stype.text()),
				scope(),
				stype,
				ptypes
			);
			fname.declare(infolog);
			Scope fscope = new Scope(scope());
			// parameters
			VariableName[] params = new VariableName[ptypes.length];
			for (int i = 0; i < params.length; i++) {
				params[i] = new VariableName(t0.withText(field_list.get(i).plainText() + "_"), fscope, ptypes[i]);
			}
			// setup
			CompoundStatement body = new CompoundStatement(t0.withText("{"), fscope);
			Function func = new Function(infolog, fname, fscope, body, params);
			// generate return val
			VariableName rv = new VariableName(t0.withText("ret"), fscope, stype);
			rv.declare(infolog);
			rv.define(new Variable(rv), infolog);
			// generate function body
			body.add(new VariableDeclaration(rv));
			for (int i = 0; i < params.length; i++) {
				VariableName vname = field_list.get(i);
				// member
				Expression ex0 = new MemberAccess(vname, rv.access());
				// param
				Expression ex1 = params[i].access();
				// assignment
				// TODO proper member-or-global call
				OverloadSet assignment = vname.type().definition().memberScope().declaredOverload("operator=", infolog);
				Expression ex_ass = assignment.call(infolog, t0.withText("("), fscope, ex0, ex1);
				body.add(new ExpressionStatement(ex_ass));
			}
			// return
			body.add(new ReturnStatement(infolog, t0.withText("return"), fscope, rv.access()));
			fname.define(func, infolog);
		} finally {
			infolog.popContext();
		}
	}
	
	public void generateAssignment(InfoLog infolog) {
		ScalarTypeName stype = (ScalarTypeName) name();
		infolog.pushContext("generating struct assignment", source());
		try {
			Token t0 = new TokenBuilder(source()).fileName("<generated>").toToken();
			FunctionName fname = new FunctionName(
				t0.withText("operator="),
				memberScope(),
				stype.copy().qualify("__ref"),
				stype.copy().qualify("__ref"),
				stype.copy().qualify("__ref").qualify("const")
			);
			fname.declare(infolog);
			Scope fscope = new Scope(memberScope());
			// parameters
			VariableName[] params = new VariableName[2];
			params[0] = new VariableName(t0.withText("arg0"), fscope, fname.parameterTypes()[0]);
			params[1] = new VariableName(t0.withText("arg1"), fscope, fname.parameterTypes()[1]);
			// generate function body
			CompoundStatement body = new CompoundStatement(t0.withText("{"), fscope);
			Function func = new Function(infolog, fname, fscope, body, params);
			for (VariableName vname : memberScope().localDeclaredNames(VariableName.class)) {
				// members
				Expression ex0 = new MemberAccess(vname, params[0].access());
				Expression ex1 = new MemberAccess(vname, params[1].access());
				// assignment
				// TODO proper member-or-global call
				OverloadSet assignment = vname.type().definition().memberScope().declaredOverload("operator=", infolog);
				Expression ex_ass = assignment.call(infolog, t0.withText("("), fscope, ex0, ex1);
				body.add(new ExpressionStatement(ex_ass));
			}
			// return
			body.add(new ReturnStatement(infolog, t0.withText("return"), fscope, params[0].access()));
			fname.define(func, infolog);
		} finally {
			infolog.popContext();
		}
	}

	@Override
	public String toString() {
		return "struct[" + name().text() + "] " + body;
	}

	@Override
	public Instance instantiate(Token source, TypeName actual_type,  Scope scope, boolean lvalue) {
		return new StructInstance(source, actual_type, scope, lvalue);
	}
	
	@Override
	public Location allocate(Allocator alloc, TypeName actual_type, Layout layout, Location parent) {
		return new StructLocation((ScalarTypeName) actual_type, alloc, layout, parent);
	}

	@Override
	public int allocAlign(Layout layout) {
		int a = layout == null ? 1 : layout.structMinAlign();
		for (VariableName vname : memberVariableNames()) {
			a = Math.max(a, vname.type().definition().allocAlign(layout));
		}
		return a;
	}
}





