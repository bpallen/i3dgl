package initial3d.detail.glsl.lang.names;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.tokens.Token;

public class ArrayTypeName extends TypeName<ArrayTypeName> {

	private ScalarTypeName inner;
	
	// size doesnt affect array typename equality, but can be attached for convenience
	private int size = -1;
	
	public ArrayTypeName(Token source_, ScalarTypeName inner_, Scope scope_) {
		super(source_, scope_);
		inner = inner_;
	}

	public ScalarTypeName innerType() {
		return inner;
	}
	
	public int size() {
		return size;
	}
	
	public ArrayTypeName size(int s) {
		size = s;
		return this;
	}

	@Override
	public String plainText() {
		return "array<" + inner.plainText() + "<";
	}
	
	@Override
	public String text() {
		StringBuilder sb = new StringBuilder("__array<");
		sb.append(inner.text());
		sb.append(">");
		for (Qualifier q : qualifiers()) {
			sb.append('&');
			sb.append(q.text());
		}
		return sb.toString();
	}
	
	@Override
	public ArrayTypeName copyImpl(Token source_, Scope scope_) {
		ArrayTypeName name = new ArrayTypeName(source_, inner.copy(source_, scope_), scope_);
		name.size(size);
		return name;
	}
	
	public ArrayTypeName withoutSize() {
		ArrayTypeName n = copy();
		n.size(-1);
		return n;
	}
	
	@Override
	public void declare(InfoLog infolog) {
		if (size < 0) {
			super.declare(infolog);
		} else {
			withoutSize().declare(infolog);
		}
	}
	
	@Override
	public String mangledText() {
		if (qualified("const")) return "C" + withoutQualifier("const").mangledText();
		if (qualified("__ref")) return "L" + withoutQualifier("__ref").mangledText();
		return "A" + inner.mangledText();
	}
}
