package initial3d.detail.glsl.lang.names;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.Function;
import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.tokens.Token;

public class FunctionName extends Name<FunctionName> {

	// TODO why are these protected?
	protected TypeName rettype;
	protected TypeName[] paramtypes;
	
	public FunctionName(Token source_, Scope scope_, TypeName rettype_, TypeName ...paramtypes_) {
		super(source_, scope_);
		rettype = rettype_;
		paramtypes = paramtypes_.clone();
	}

	@Override
	public String plainText() {
		return source.text();
	}
	
	@Override
	public String text() {
		StringBuilder sb = new StringBuilder("_F");
		sb.append(source.text());
		sb.append('.');
		sb.append(rettype.mangledText());
		sb.append('(');
		for (TypeName t : paramtypes) {
			sb.append(t.mangledText());
			if (t != paramtypes[paramtypes.length - 1]) sb.append(',');
		}
		sb.append(')');
		return sb.toString();
	}
	
	public TypeName returnType() {
		return rettype;
	}
	
	public TypeName[] parameterTypes() {
		return paramtypes;
	}

	@Override
	public FunctionName copyImpl(Token source_, Scope scope_) {
		FunctionName name = new FunctionName(source_, scope_, rettype, paramtypes);
		return name;
	}
	
	@Override
	public FunctionName declaration() {
		return scope.declaredFunction(text(), InfoLog.NULL);
	}
	
	@Override
	public Function definition() {
		return scope.definedFunction(this, InfoLog.NULL);
	}
}
