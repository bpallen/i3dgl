package initial3d.detail.glsl.lang.names;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.tokens.Token;

/**
 * TODO most of this is kinda terrible
 * 
 * @author ben
 *
 */
public class OverloadSet extends Name<OverloadSet> {

	protected OverloadSet parent = null;
	protected Set<FunctionName> overloads = new HashSet<>();
	protected Expression thisexpr = null;
	
	private static class CallCandidate implements Comparable<CallCandidate> {

		private InfoLog infolog;
		private FunctionName fname;
		private Expression[] params;
		private int conversions;
		
		public CallCandidate(InfoLog infolog_, FunctionName fname_, Scope scope_, List<Expression> params0_) {
			infolog = infolog_;
			fname = fname_;
			params = new Expression[fname.parameterTypes().length];
			checkConversions(scope_, params0_, true);
		}
		
		public void checkConversions(Scope scope, List<Expression> params0, boolean quiet) {
			if (params.length != params0.size()) {
				// incorrect number of actual parameters
				conversions = 1000000;
				return;
			}
			// are we an arithmetic operator?
			if (fname.qualified("__arith")) {
				// TODO maybe check that both actual params are arith types?
				infolog.pushContext("checking usual arithmetic conversions", fname.qualifier("__arith").source(), quiet);
				infolog.showParentContext(false);
				try {
					// what is the common type of the params?
					if (params.length != 2) throw new BuildException("bad params for __arith");
					TypeName common = scope.commonType(params0.get(0).type(), params0.get(1).type());
					// common type must exist
					if (common == null) {
						infolog.error("no actual common type", fname.qualifier("__arith").source());
						conversions = 1000000;
						return;
					}
					// common type must match both params
					if (!fname.parameterTypes()[0].declEquals(common)) {
						infolog.error("formal param 0 does not match actual common type " + common.plainText(), fname.parameterTypes()[0].source());
						conversions = 1000000;
						return;
					}
					if (!fname.parameterTypes()[1].declEquals(common)) {
						infolog.error("formal param 1 does not match actual common type " + common.plainText(), fname.parameterTypes()[1].source());
						conversions = 1000000;
						return;
					}
				} finally {
					infolog.popContext();
				}
			}
			// check param conversions
			for (int i = 0; i < params.length; i++) {
				infolog.pushContext("converting parameter " + i, params0.get(i).source(), quiet);
				infolog.showParentContext(false);
				try {
					params[i] = scope.convert(infolog, fname.parameterTypes()[i], params0.get(i), false, quiet);
				} finally {
					infolog.popContext();
				}
				if (params[i] == null) {
					// no conversion
					conversions += 100000;
				} else if (!params[i].type().declEquals(params0.get(i).type())) {
					// unqualified type changed
					conversions += 1000;
				} else if (!params[i].type().declQualEquals(params0.get(i).type())) {
					// qualifiers changed
					conversions += 10;
					if (params[i].type().qualified("const") && !params0.get(i).type().qualified("const")) {
						// added const
						conversions += 1;
					}
				} else {
					// ?
				}
			}
		}
		
		@Override
		public int compareTo(CallCandidate other) {
			return conversions - other.conversions;
		}
		
		public FunctionName functionName() {
			return fname;
		}
		
		public int conversions() {
			return conversions;
		}
		
		public boolean valid() {
			return conversions < 100000;
		}
		
		public FunctionCall call(Token source, Scope scope) {
			return new FunctionCall(infolog, source, scope, fname, params);
		}
	}
	
	public OverloadSet(Token source_, Scope scope_, OverloadSet parent_) {
		super(source_, scope_);
		parent = parent_;
	}
	
	public OverloadSet parent() {
		return parent;
	}

	public void thisExpr(Expression expr) {
		thisexpr = expr;
	}
	
	public Expression thisExpr() {
		return thisexpr;
	}
	
	public void add(FunctionName name) {
		overloads.add(name);
	}
	
	public FunctionCall call(InfoLog infolog, Token source, Scope scope, Expression ...params) {
		infolog.pushContext("resolving function call to " + text(), source);
		try {
			ArrayList<CallCandidate> candidates = new ArrayList<>();
			ArrayList<Expression> params1 = new ArrayList<>();
			if (thisexpr != null) params1.add(thisexpr);
			params1.addAll(Arrays.asList(params));
			StringBuilder pstr = new StringBuilder("actual parameter types: ");
			for (Expression e : params1) {
				pstr.append(e.type().text());
				pstr.append(", ");
			}
			if (!params1.isEmpty()) pstr.setLength(pstr.length() - 2);
			for (FunctionName fname : overloads) {
				candidates.add(new CallCandidate(infolog, fname, scope, params1));
			}
			Collections.sort(candidates);
			if (!candidates.isEmpty() && candidates.get(0).valid()) {
				// found valid overload
				if (candidates.size() > 1 && candidates.get(0).conversions() == candidates.get(1).conversions()) {
					// ambiguous
					infolog.error("overload resolution failed; ambiguous call", source);
					infolog.detail(pstr.toString());
					infolog.detail("candidates are (" + candidates.size() + "):");
					for (int i = 0; i < candidates.size() && candidates.get(i).conversions() == candidates.get(0).conversions(); i++) {
						FunctionName fname = candidates.get(i).functionName();
						int x = candidates.get(i).conversions();
						infolog.detail("candidate[" + x + "]" + fname.text() + ":", fname.source());
					}
				} else {
					return candidates.get(0).call(source, scope);
				}
			} else {
				// no valid overloads
				if (parent == null) {
					if (!candidates.isEmpty() && params1.size() != candidates.get(0).functionName().parameterTypes().length) {
						infolog.error("overload resolution failed; no overload found that takes " + params1.size() + " parameters", source);
					} else {
						infolog.error("overload resolution failed; no valid overloads", source);
					}
					infolog.detail(pstr.toString());
					if (candidates.isEmpty()) {
						infolog.detail("no candidates found");
						if (scope.declaredName(text(), Name.class, infolog) == null) infolog.detail("name '${text}' is not declared", source());
					} else {
						infolog.detail("candidates are (" + candidates.size() + "):");
					}
				} else {
					// try parent
					// TODO this should not really be done (name hiding)
					// but is used atm to call operator= from member scope and allow it to find non-members too
					FunctionCall c = parent.call(infolog, source, scope, params);
					if (c != null) return c;
				}
				// print candidates
				for (CallCandidate cc : candidates) {
					FunctionName fname = cc.functionName();
					infolog.detail("candidate[" + cc.conversions() + "]: " + fname.text(), fname.source());
					cc.checkConversions(scope, params1, false);
				}
			}
		} finally {
			infolog.popContext();
		}
		return null;
	}
	
	@Override
	public OverloadSet copyImpl(Token source_, Scope scope_) {
		OverloadSet name = new OverloadSet(source_, scope_, parent);
		for (FunctionName f : overloads) {
			name.add(f);
		}
		name.thisExpr(thisexpr);
		return name;
	}
	
	@Override
	public OverloadSet declaration() {
		return scope.declaredOverload(text(), InfoLog.NULL);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString());
		if (thisexpr != null) {
			sb.append('[');
			sb.append(thisexpr);
			sb.append(']');
		}
		return sb.toString();
	}
}
