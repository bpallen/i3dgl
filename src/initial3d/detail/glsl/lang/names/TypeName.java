package initial3d.detail.glsl.lang.names;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Type;
import initial3d.detail.glsl.tokens.Token;

@SuppressWarnings("rawtypes")
public abstract class TypeName<T extends TypeName> extends Name<T> {

	// TODO these silly generics seem not to work
	// ... unless used on an instance of a non-generic type?
	
	public TypeName(Token source_, Scope scope_) {
		super(source_, scope_);
	}

	@Override
	protected abstract T copyImpl(Token source_, Scope scope_);

	@Override
	public T copy() {
		// this override shouldnt be necessary, but it is
		return super.copy();
	}
	
	@Override
	public void declare(InfoLog infolog) {
		scope.declare(withoutQualifiers(), infolog);
	}

	@Override
	public void define(Node n, InfoLog infolog) {
		scope.define(withoutQualifiers(), n, infolog);
	}
	
	@Override
	public boolean declared() {
		if (qualifiers().isEmpty()) return super.declared();
		return withoutQualifiers().declared();
	}
	
	@Override
	public boolean defined() {
		if (qualifiers().isEmpty()) return super.defined();
		return withoutQualifiers().defined();
	}

	@Override
	public TypeName declaration() {
		return scope.declaredType(withoutQualifiers().text(), InfoLog.NULL);
	}
	
	@Override
	public Type definition() {
		return scope.definedType(withoutQualifiers(), InfoLog.NULL);
	}
	
	public String mangledText() {
		if (qualified("const")) return "C" + withoutQualifier("const").mangledText();
		if (qualified("__ref")) return "L" + withoutQualifier("__ref").mangledText();
		if ("void".equals(text())) return "v";
		if ("bool".equals(text())) return "b";
		if ("int".equals(text())) return "i";
		if ("uint".equals(text())) return "u";
		if ("float".equals(text())) return "f";
		if ("double".equals(text())) return "d";
		return "T" + text();
	}
	
	public boolean ref() {
		return qualified("__ref");
	}
}
