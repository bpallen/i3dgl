package initial3d.detail.glsl.lang.names;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.expressions.VariableAccess;
import initial3d.detail.glsl.tokens.Token;

public class VariableName extends Name<VariableName> {

	private TypeName type;
	
	public VariableName(Token source_, Scope scope_, TypeName type_) {
		super(source_, scope_);
		type = type_;
	}

	public TypeName type() {
		return type;
	}
	
	@Override
	public String text() {
		Qualifier qi = interfaceQualifier();
		String si = qi == null ? "" : "@" + qi.text().substring(0, 1).toUpperCase();
		// TODO other storage qualifiers?
		return "_V" + source().text() + si + "." + type.mangledText();
	}
	
	@Override
	public VariableName copyImpl(Token source_, Scope scope_) {
		VariableName name = new VariableName(source_, scope_, type);
		return name;
	}
	
	@Override
	public VariableName declaration() {
		return scope.declaredVariable(text(), InfoLog.NULL);
	}
	
	@Override
	public Variable definition() {
		return scope.definedVariable(this, InfoLog.NULL);
	}
	
	public boolean constexpr() {
		Variable v = definition();
		return v != null && v.constexpr();
	}
	
	public VariableAccess access() {
		return definition().access(this);
	}
}
