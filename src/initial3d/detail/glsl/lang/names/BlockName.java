package initial3d.detail.glsl.lang.names;

import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.tokens.Token;

public class BlockName extends Name<BlockName> {

	public BlockName(Token source_, Scope scope_) {
		super(source_, scope_);
	}

	@Override
	public String text() {
		return "_B" + source.text();
	}
	
	@Override
	public BlockName copyImpl(Token source_, Scope scope_) {
		BlockName name = new BlockName(source_, scope_);
		return name;
	}

	public TypeName type() {
		// TODO infolog?
		return scope.declaredType("__block_" + source.text(), null);
	}
	
}
