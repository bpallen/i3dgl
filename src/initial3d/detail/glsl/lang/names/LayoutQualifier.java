package initial3d.detail.glsl.lang.names;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.tokens.Token;

public class LayoutQualifier extends Qualifier {

	public static class Param {
		
		public final Token source;
		public final Instance value;
		
		public Param(Token source_, Instance value_) {
			source = source_;
			value = value_;
		}
		
		@Override
		public String toString() {
			return value == null ? "?" : value.toString();
		}
	}
	
	private HashMap<String, Param> params = new HashMap<>();
	
	public LayoutQualifier(Token source_, Scope scope_) {
		super(source_, scope_);
	}

	public Param get(String s) {
		return params.get(s);
	}
	
	public void put(String s, Token source, Instance value) {
		params.put(s, new Param(source, value));
	}
	
	public Collection<Param> params() {
		return Collections.unmodifiableCollection(params.values());
	}
	
	@Override
	public String text() {
		return "layout";
	}
	
	@Override
	public String toString() {
		return "layout" + params.toString();
	}
	
	@Override
	public boolean equals(Object other) {
		throw new UnsupportedOperationException("bad!");
	}
}
