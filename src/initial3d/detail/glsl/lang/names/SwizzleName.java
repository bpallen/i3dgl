package initial3d.detail.glsl.lang.names;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Swizzle;
import initial3d.detail.glsl.lang.expressions.SwizzleAccess;
import initial3d.detail.glsl.tokens.Token;

public class SwizzleName extends Name<SwizzleName> {
	
	private TypeName type;
	
	public SwizzleName(Token source_, Scope scope_, TypeName type_) {
		super(source_, scope_);
		type = type_;
	}

	public TypeName type() {
		return type;
	}
	
	@Override
	public String text() {
		return "_Z" + source().text() + "." + type.mangledText();
	}
	
	@Override
	public SwizzleName copyImpl(Token source_, Scope scope_) {
		SwizzleName name = new SwizzleName(source_, scope_, type);
		return name;
	}
	
	@Override
	public SwizzleName declaration() {
		return scope.declaredSwizzle(text(), InfoLog.NULL);
	}
	
	@Override
	public Swizzle definition() {
		return scope.definedSwizzle(this, InfoLog.NULL);
	}
	
	public SwizzleAccess access(Expression thisexpr) {
		return definition().access(this, thisexpr);
	}

}
