package initial3d.detail.glsl.lang.names;

import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.tokens.Token;

// scalar here just means non-array. probably not the best name.
public class ScalarTypeName extends TypeName<ScalarTypeName> {

	public ScalarTypeName(Token source_, Scope scope_) {
		super(source_, scope_);
	}
	
	@Override
	public String text() {
		StringBuilder sb = new StringBuilder(source.text());
		for (Qualifier q : qualifiers()) {
			sb.append('&');
			sb.append(q.text());
		}
		return sb.toString();
	}
	
	@Override
	public ScalarTypeName copyImpl(Token source_, Scope scope_) {
		ScalarTypeName name = new ScalarTypeName(source_, scope_);
		return name;
	}
	
}
