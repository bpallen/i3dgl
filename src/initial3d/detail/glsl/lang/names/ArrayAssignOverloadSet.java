package initial3d.detail.glsl.lang.names;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.Array;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Function;
import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.statements.CompoundStatement;
import initial3d.detail.glsl.lang.statements.ExpressionStatement;
import initial3d.detail.glsl.lang.statements.ReturnStatement;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenBuilder;

public class ArrayAssignOverloadSet extends OverloadSet {
	
	// general idea: acquire function when called (if sizes match).
	// overloads must be cleared before each call as otherwise the call would be ambiguous
	// (array size is not part of the type, so overload resolution cannot distinguish size)
	// generate functions under size-based names so they can stay declared and can be re-used.
	// this overload set uses the 'operator=' name, but is declared at member scope so should not conflict.
	
	private Array arr;
	
	public ArrayAssignOverloadSet(Array arr_) {
		super(arr_.name().source(), arr_.memberScope(), null);
		arr = arr_;
		Name parname = arr.scope().declaration("operator=");
		if (parname instanceof OverloadSet) {
			// name is visible in parent scope as overloadset
			parent = (OverloadSet) parname;
		}
	}

	protected void generate(int s, InfoLog infolog) {
		ArrayTypeName atype = (ArrayTypeName) arr.name();
		infolog.pushContext("generating array assignment", atype.source());
		try {
			Token t0 = new TokenBuilder(atype.source()).fileName("<generated>").toToken();
			Scope ascope = arr.memberScope();
			FunctionName fname = new FunctionName(
				t0.withText("__array_assign_" + s),
				ascope,
				atype.copy().qualify("__ref").size(s),
				atype.copy().qualify("__ref").size(s),
				atype.copy().qualify("__ref").qualify("const").size(s)
			);
			add(fname);
			if (fname.declared()) return;
			fname.declare(infolog);
			Scope fscope = new Scope(ascope);
			// parameters
			VariableName[] params = new VariableName[2];
			params[0] = new VariableName(t0.withText("arg0"), fscope, fname.parameterTypes()[0]);
			params[1] = new VariableName(t0.withText("arg1"), fscope, fname.parameterTypes()[1]);
			// generate function body
			CompoundStatement body = new CompoundStatement(t0.withText("{"), fscope);
			Function func = new Function(infolog, fname, fscope, body, params);
			fscope.owner(func);
			for (int i = 0; i < s; i++) {
				// array subscript operator
				OverloadSet subscript = ascope.declaredOverload("operator[]", infolog);
				Expression ex_sub0 = subscript.call(infolog, t0.withText("("), fscope, params[0].access(), Instance.parse(t0.withText("" + i), fscope));
				Expression ex_sub1 = subscript.call(infolog, t0.withText("("), fscope, params[1].access(), Instance.parse(t0.withText("" + i), fscope));
				// assignment operator
				// TODO proper member-or-global call
				OverloadSet assignment = atype.innerType().definition().memberScope().declaredOverload("operator=", infolog);
				Expression ex_ass = assignment.call(infolog, t0.withText("("), fscope, ex_sub0, ex_sub1);
				body.add(new ExpressionStatement(ex_ass));
			}
			// return
			body.add(new ReturnStatement(infolog, t0.withText("return"), fscope, params[0].access()));
			fname.define(func, infolog);
			//System.err.println(func);
			//System.err.println("overloads: " + overloads.size());
		} finally {
			infolog.popContext();
		}
	}
	
	@Override
	public OverloadSet copyImpl(Token source_, Scope scope_) {
		ArrayAssignOverloadSet name = new ArrayAssignOverloadSet(arr);
		name.source = source_;
		name.scope = scope_;
		name.thisExpr(thisexpr);
		return name;
	}

	@Override
	public String plainText() {
		return text();
	}

	@Override
	public String text() {
		return "operator=";
	}

	@Override
	public FunctionCall call(InfoLog infolog, Token source, Scope scope, Expression... params) {
		// clear overloads to avoid ambiguous calls
		overloads.clear();
		if (params.length == 2 && params[0].type() instanceof ArrayTypeName && params[1].type() instanceof ArrayTypeName) {
			ArrayTypeName at0 = (ArrayTypeName) params[0].type();
			ArrayTypeName at1 = (ArrayTypeName) params[1].type();
			// generate assignment for this size
			if (at0.size() == at1.size()) {
				generate(at0.size(), infolog);
			} else {
				infolog.warning(String.format("incompatible sizes (%d, %d) for array assignment", at0.size(), at1.size()), source);
			}
		} else {
			infolog.warning("incompatible types for array assignment", source);
		}
		FunctionCall fc = super.call(infolog, source, scope, params);
		return fc;
	}
}
