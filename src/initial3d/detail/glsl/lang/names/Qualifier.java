package initial3d.detail.glsl.lang.names;

import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.tokens.Token;

public class Qualifier implements Node {

	private Token source;
	private Scope scope;
	
	public Qualifier(Token source_, Scope scope_) {
		source = source_;
		scope = scope_;
	}
	
	public String text() {
		return source.text();
	}
	
	@Override
	public Token source() {
		return source;
	}
	
	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public String toString() {
		return text();
	}
	
	@Override
	public int hashCode() {
		return text().hashCode();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
		if (this == other) return true;
		if (getClass() != other.getClass()) return false;
		return text().equals(((Qualifier) other).text());
	}
	
}
