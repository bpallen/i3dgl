package initial3d.detail.glsl.lang.names;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.Array;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Function;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.statements.CompoundStatement;
import initial3d.detail.glsl.lang.statements.ExpressionStatement;
import initial3d.detail.glsl.lang.statements.ReturnStatement;
import initial3d.detail.glsl.lang.statements.VariableDeclaration;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenBuilder;

public class ArrayCtorOverloadSet extends OverloadSet {

	private Array arr;
	
	public ArrayCtorOverloadSet(Array arr_) {
		super(arr_.name().source(), arr_.scope(), null);
		arr = arr_;
	}
	
	protected void generate(int s, InfoLog infolog) {
		ArrayTypeName atype = ((ArrayTypeName) arr.name()).copy().size(s);
		infolog.pushContext("generating array ctor", atype.source());
		try {
			Token t0 = new TokenBuilder(atype.source()).fileName("<generated>").toToken();
			Scope ascope = arr.memberScope();
			TypeName[] ptypes = new TypeName[s];
			for (int i = 0; i < s; i++) {
				ptypes[i] = atype.innerType();
			}
			// attach size to return type
			FunctionName fname = new FunctionName(t0.withText("__ctor_" + atype.text()), atype.scope(), atype.copy().size(s), ptypes);
			fname.qualify(new Qualifier(t0.withText("__constexpr"), atype.scope()));
			// overload sets get copied around, and generate() is probably running on a copy
			add(fname);
			if (fname.declared()) return;
			fname.declare(infolog);
			Scope fscope = new Scope(atype.scope());
			// parameters
			VariableName[] params = new VariableName[s];
			for (int i = 0; i < s; i++) {
				params[i] = new VariableName(t0.withText("arg" + i), fscope, atype.innerType());
			}
			// setup
			CompoundStatement body = new CompoundStatement(t0.withText("{"), fscope);
			Function func = new Function(infolog, fname, fscope, body, params);
			// generate return val
			VariableName rv = new VariableName(t0.withText("ret"), fscope, atype);
			rv.declare(infolog);
			rv.define(new Variable(rv), infolog);
			// generate function body
			body.add(new VariableDeclaration(rv));
			for (int i = 0; i < s; i++) {
				// array subscript operator
				OverloadSet subscript = ascope.declaredOverload("operator[]", infolog);
				Expression ex_sub = subscript.call(infolog, t0.withText("("), fscope, rv.access(), Instance.parse(t0.withText("" + i), fscope));
				// assignment operator
				// TODO proper member-or-global call
				OverloadSet assignment = atype.innerType().definition().memberScope().declaredOverload("operator=", infolog);
				Expression ex_ass = assignment.call(infolog, t0.withText("("), fscope, ex_sub, params[i].access());
				body.add(new ExpressionStatement(ex_ass));
			}
			// return
			body.add(new ReturnStatement(infolog, t0.withText("return"), fscope, rv.access()));
			fname.define(func, infolog);
		} finally {
			infolog.popContext();
		}
	}

	@Override
	public ArrayCtorOverloadSet copyImpl(Token source_, Scope scope_) {
		ArrayCtorOverloadSet name = new ArrayCtorOverloadSet(arr);
		name.source = source_;
		name.scope = scope_;
		for (FunctionName f : overloads) {
			name.add(f);
		}
		name.thisExpr(thisexpr);
		return name;
	}
	
	@Override
	public String plainText() {
		return text();
	}

	@Override
	public String text() {
		return "__ctor_" + arr.name().text();
	}

	@Override
	public FunctionCall call(InfoLog infolog, Token source, Scope scope, Expression... params) {
		// lazily generate ctor with required number of params
		generate(params.length, infolog);
		return super.call(infolog, source, scope, params);
	}
	
}
