package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Allocator;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.TranslateException;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.ArrayLocation;
import initial3d.detail.glsl.intermediate.locations.ConstantPrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.VoidLocation;
import initial3d.detail.glsl.lang.expressions.ArrayInstance;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.IntInstance;
import initial3d.detail.glsl.lang.names.ArrayAssignOverloadSet;
import initial3d.detail.glsl.lang.names.ArrayCtorOverloadSet;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.FunctionName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenBuilder;

public class Array extends Type {
	
	public Array(ArrayTypeName name_, InfoLog infolog) {
		super(name_.withoutQualifiers().withoutSize(), new Scope(name_.scope()));
		memberScope().owner(this);
		memberScope().allowDeclarations();
		generateLength(infolog);
		generateSubscript(infolog);
		generateConstructor(infolog);
		generateAssignment(infolog);
	}

	protected void generateLength(InfoLog infolog) {
		ArrayTypeName atype = (ArrayTypeName) name();
		Token t0 = new TokenBuilder(atype.source()).fileName("<generated>").toToken();
		FunctionName fname0 = new FunctionName(
			t0.withText("length"),
			memberScope(),
			scope().declaredType("int", null), // TODO length as int or uint?
			atype.copy().qualify("__ref").qualify("const")
		);
		// __forceconstexpr will force attempted eval even with non-constexpr params
		// but does _not_ make the expression constexpr if params are non-constexpr
		fname0.qualify("__forceconstexpr");
		fname0.declare(infolog);
		fname0.define(new BuiltinFunction(
			fname0, 
			(VariableInstanceMap vars, Instance ...actual_params) -> {
				// TODO proper error
				int size = ((ArrayTypeName) actual_params[0].type()).size();
				if (size < 0) throw new EvalException("array size not constant");
				IntInstance x = new IntInstance(atype.source(), vars.currentScope(), false);
				x.value(size);
				return x;
			},
			(Translator trans, Location ...actual_params) -> {
				// TODO proper error
				int size = ((ArrayTypeName) actual_params[0].type()).size();
				if (size < 0) throw new TranslateException("array size not constant");
				IntInstance x = new IntInstance(atype.source(), atype.scope(), false);
				x.value(size);
				return new ConstantPrimitiveLocation(x);
			}
		), infolog);
	}
	
	private Instance evalSubscript(Instance... actual_params) {
		ArrayInstance xa = (ArrayInstance) actual_params[0];
		IntInstance xi = (IntInstance) actual_params[1];
		return xa.member(xi.value());
	}
	
	private Location translateSubscript(Translator trans, Location ...actual_params) {
		return actual_params[0].translateSubscript(trans, (PrimitiveLocation) actual_params[1]);
	}
	
	protected void generateSubscript(InfoLog infolog) {
		// TODO subscript overload set to implicitly size arrays?
		ArrayTypeName atype = (ArrayTypeName) name();
		Token t0 = new TokenBuilder(atype.source()).fileName("<generated>").toToken();
		// non-const
		FunctionName fname0 = new FunctionName(
			t0.withText("operator[]"),
			memberScope(),
			atype.innerType().copy().qualify("__ref"),
			atype.copy().qualify("__ref"),
			scope().declaredType("int", null)
		);
		fname0.qualify("__constexpr");
		fname0.declare(infolog);
		fname0.define(new BuiltinFunction(
			fname0,
			(VariableInstanceMap vars, Instance ...actual_params) -> {
				return evalSubscript(actual_params);
			},
			this::translateSubscript
		), infolog);
		// const
		FunctionName fname1 = new FunctionName(
			t0.withText("operator[]"),
			memberScope(),
			atype.innerType().copy().qualify("__ref").qualify("const"),
			atype.copy().qualify("__ref").qualify("const"),
			scope().declaredType("int", null)
		);
		fname1.qualify("__constexpr");
		fname1.declare(infolog);
		fname1.define(new BuiltinFunction(
			fname1,
			(VariableInstanceMap vars, Instance ...actual_params) -> {
				return evalSubscript(actual_params);
			},
			this::translateSubscript
		), infolog);
		// TODO subscript with uint
	}

	protected void generateConstructor(InfoLog infolog) {
		new ArrayCtorOverloadSet(this).declare(infolog);
	}
	
	protected void generateAssignment(InfoLog infolog) {
		new ArrayAssignOverloadSet(this).declare(infolog);
	}

	@Override
	public Instance instantiate(Token source, TypeName actual_type, Scope scope, boolean lvalue) {
		return new ArrayInstance(source, (ArrayTypeName) actual_type, scope, lvalue);
	}
	
	@Override
	public Location allocate(Allocator alloc, TypeName actual_type, Layout layout, Location parent) {
		return new ArrayLocation((ArrayTypeName) actual_type, alloc, layout, parent);
	}

	@Override
	public int allocAlign(Layout layout) {
		int a = layout == null ? 1 : layout.arrayMinAlign();
		return Math.max(a, ((ArrayTypeName) name()).innerType().definition().allocAlign(layout)); 
	}
}





