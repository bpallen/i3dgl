package initial3d.detail.glsl.lang;

public class Precedence {
	
	// internal machinery
	public static final int STATEMENT = 100;
	public static final int DECLARATION = 200;
	// TODO qualification is maybe higher precedence than call?
	public static final int QUALIFICATION = 300;
	
	// operators
	public static final int SEQUENCE = 1010;
	public static final int ASSIGNMENT = 1020;
	public static final int SELECTION = 1030;
	public static final int LOGICAL_OR = 1040;
	public static final int LOGICAL_XOR = 1050; // fucking redundant operator
	public static final int LOGICAL_AND = 1060;
	public static final int BITWISE_OR = 1070;
	public static final int BITWISE_XOR = 1080;
	public static final int BITWISE_AND = 1090;
	public static final int EQUALITY = 1100;
	public static final int RELATIONAL = 1110;
	public static final int BITSHIFT = 1120;
	public static final int ADDITIVE = 1130;
	public static final int MULTIPLICATIVE = 1140;
	public static final int PREFIX = 1150;
	public static final int CALL = 1160;
	
}
