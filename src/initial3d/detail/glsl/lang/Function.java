package initial3d.detail.glsl.lang;

import java.lang.reflect.Method;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Label;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.intermediate.TranslateException;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.names.FunctionName;
import initial3d.detail.glsl.lang.names.Qualifier;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.lang.statements.LoopBreak;
import initial3d.detail.glsl.lang.statements.LoopContinue;
import initial3d.detail.glsl.lang.statements.ReturnValue;
import initial3d.detail.glsl.lang.statements.ScopedStatement;
import initial3d.detail.glsl.tokens.Token;

public class Function implements Node {

	private FunctionName name;
	private Scope body_scope;
	private ScopedStatement body;
	private VariableName[] params;
	
	/**
	 * Takes ownership of the body scope, allows declarations in it and declares the params
	 * and defines them to new variables.
	 * 
	 * @param name_
	 * @param body_scope_
	 * @param body_
	 * @param params_
	 */
	public Function(InfoLog infolog, FunctionName name_, Scope body_scope_, Statement body_, VariableName ...params_) {
		name = name_;
		body_scope = body_scope_;
		body = new ScopedStatement(body_, body_scope_);
		params = params_.clone();
		body_scope.owner(this);
		body_scope.allowDeclarations();
		// declare formal params
		for (VariableName p : params) {
			p.declare(infolog);
			p.define(new Variable(p), infolog);
		}
		// declare magic return value
		// this is declared with the exact type as the function returns,
		// but will be bound like a reference to whatever the return value is
		VariableName r = new VariableName(name.source().withText("__return"), body_scope, name.returnType());
		r.declare(infolog);
		r.define(new Variable(r), infolog);
	}
	
	@Override
	public Token source() {
		return name.source();
	}
	
	@Override
	public Scope scope() {
		return name.scope();
	}
	
	public FunctionName name() {
		return name;
	}
	
	public Scope bodyScope() {
		return body_scope;
	}
	
	public ScopedStatement body() {
		return body;
	}
	
	public void body(Statement s) {
		body = new ScopedStatement(s, body_scope);
	}
	
	public VariableName[] parameters() {
		return params;
	}
	
	public void parameters(VariableName ...ps) {
		params = ps.clone();
	}
	
	public Method javaEquivalent() {
		return null;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("function[");
		sb.append(name.returnType().text());
		sb.append(' ');
		sb.append(name.plainText());
		sb.append('(');
		for (VariableName p : params) {
			String t = p.text();
			if (t != null) {
				sb.append(t);
			} else {
				sb.append(p.type().text());
			}
			sb.append(", ");
		}
		if (params.length > 0) sb.setLength(sb.length() - 2);
		sb.append(")] ");
		sb.append(body);
		return sb.toString();
	}

	public Instance eval(VariableInstanceMap vars, Instance ...actual_params) {
		vars.infoLog().pushContext("evaluating function " + name().text(), source());
		vars.pushScope(body_scope);
		try {
			// copy params into function scope
			// this is reponsible for copying params not passed by reference
			for (int i = 0; i < params.length; i++) {
				if (params[i].type().ref()) {
					vars.put(params[i], actual_params[i]);
				} else {
					vars.put(params[i], actual_params[i].copy(vars));
				}
			}
			// eval body
			Instance r = null;
			try {
				body.inner().eval(vars);
			} catch (ReturnValue rv) {
				r = rv.value();
			} catch (LoopBreak e) {
				// break outside a loop
				vars.infoLog().error("break used outside a loop", e.statement().source());
				throw new EvalException("break used outside a loop");
			} catch (LoopContinue e) {
				// continue outside a loop
				vars.infoLog().error("continue used outside a loop", e.statement().source());
				throw new EvalException("continue used outside a loop");
			}
			// return
			if (name.returnType().ref()) {
				return r;
			} else {
				return r.copy(vars);
			}
		} finally {
			vars.popScope(body_scope);
			vars.infoLog().popContext();
		}
	}
	
	public Location translate(Translator trans, Location ...actual_params) {
		// return value location (ref return is determined later)
		Location rloc = name.returnType().ref() ? null : trans.temporary(name.returnType());
		trans.infoLog().pushContext("translating function " + name().text(), source());
		trans.pushScope(body_scope);
		try {
			// bind return loc if not ref
			VariableName rvname = body_scope.declaredVariable("__return", trans.infoLog());
			if (!name.returnType().ref()) trans.bindReference(rvname, rloc);
			// set up return branch handler
			Label rlab = new Label();
			trans.branchHandler("return", (Translator trans2, MacroOp.Code code, String lname, PrimitiveLocation arg) -> {
				trans2.branch(code, rlab, arg);
			});
			trans.branchHandler("break", (Translator trans2, MacroOp.Code code, String lname, PrimitiveLocation arg) -> {
				trans2.infoLog().error("break used outside a loop");
			});
			trans.branchHandler("continue", (Translator trans2, MacroOp.Code code, String lname, PrimitiveLocation arg) -> {
				trans2.infoLog().error("continue used outside a loop");
			});
			// copy params into function scope
			// this is reponsible for copying params not passed by reference
			for (int i = 0; i < params.length; i++) {
				if (params[i].type().ref()) {
					trans.bindReference(params[i], actual_params[i]);
				} else {
					trans.copy(trans.variable(params[i]), actual_params[i]);
				}
			}
			// translate body
			body.inner().translate(trans);
			// return branch target
			trans.label(rlab);
			// update return loc (for ref return)
			rloc = trans.variable(rvname, true);
			if (rloc == null) {
				trans.infoLog().error("function returning reference did not return", source());
				throw new TranslateException("function returning reference did not return");
			}
			return rloc;
		} finally {
			trans.popScope(body_scope);
			trans.infoLog().popContext();
		}
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (getClass() != o.getClass()) return false;
		Function of = (Function) o;
		return name.equals(of.name);
	}
}
