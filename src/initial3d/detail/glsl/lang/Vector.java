package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Allocator;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.lang.names.TypeName;

public class Vector extends Struct {

	public Vector(ScalarTypeName type_, Scope member_scope_, Statement body_) {
		super(type_, member_scope_, body_);
	}

	public BuiltinType valType() {
		Type type = ((ArrayTypeName) memberScope().declaredVariable("_data", false, InfoLog.NULL).type()).innerType().definition();
		if (!(type instanceof BuiltinType)) throw new IllegalStateException("vector value type is not a builtin");
		return (BuiltinType) type;
	}
	
	public int vecSize() {
		int size = ((ArrayTypeName) memberScope().declaredVariable("_data", false, InfoLog.NULL).type()).size();
		if (size < 0) throw new IllegalStateException("bad vector array size");
		return size;
	}
	
	@Override
	public Location allocate(Allocator alloc, TypeName actual_type, Layout layout, Location parent) {
		// set proper vector align, then alloc members using new layout without specified block layout
		// have to copy the layout because we need to not suppress other layout params (like interpolation)
		// this ensures that vector members are properly packed
		Layout l2 = null;
		if (layout != null) {
			alloc.align(layout.vectorAlign(this));
			l2 = layout.clone();
			l2.packing = Layout.Packing.NONE;
		}
		return super.allocate(alloc, actual_type, l2, parent);
	}
	
	@Override
	public int allocAlign(Layout l) {
		return l == null ? valType().allocAlign(l) : l.vectorAlign(this);
	}
}
