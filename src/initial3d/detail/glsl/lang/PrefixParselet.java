package initial3d.detail.glsl.lang;

import java.io.IOException;

import initial3d.detail.glsl.tokens.Token;

public interface PrefixParselet {
	public Node parse(Parser p, Token t) throws IOException;
	public Class<? extends Node>[] nodeClasses();
}