package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.VariableAccess;
import initial3d.detail.glsl.lang.names.TypeName;

public interface Expression extends Node {

	public TypeName type();
	
	/**
	 * Is this expression constant?
	 * <p>
	 * Note that this isn't useful for controlling evaluation:
	 * for a function call to be constexpr, the parameters must also be.
	 * However, within the evaluation of that call, other calls will likely
	 * not have constexpr parameters, and therefore those calls cannot
	 * be evaluated if we check for constexpr before evaluation.
	 * It is better to just try and {@link #eval(VariableInstanceMap)} and see if it succeeds.
	 * </p>
	 */
	default boolean constexpr() {
		return false;
	}
	
	/**
	 * Might the result of this expression alias an l-value in a swizzled manner?
	 * If so, it should be copied before being bound to a const ref function param.
	 * 
	 * @return
	 */
	default boolean swizzled() {
		return false;
	}
	
	default boolean lValue() {
		return false;
	}
	
	default boolean rValue() {
		return !lValue();
	}
	
	Instance eval(VariableInstanceMap vars);
	
	Location translate(Translator trans);
	
	void pushRead();
	
	void pushWrite();
	
	void visitVariableAccesses(VariableAccess.Visitor v);
	
	@Override
	default void acceptVisitor(NodeVisitor visitor) {
		visitor.visit(this);
	}
	
	default Instance eval(boolean quiet, InfoLog infolog) {
		infolog.pushContext("evaluating root expression", source(), quiet);
		try {
			VariableInstanceMap vars = new VariableInstanceMap(infolog, scope());
			return eval(vars);
		} finally {
			infolog.popContext();
		}
	}
	
	default Expression maybeEval(boolean quiet, InfoLog infolog) {
		if (constexpr()) {
			try {
				return eval(quiet, infolog);
			} catch (EvalException e) {
				infolog.error("failed to evaluate purportedly-constant expression", source());
			}
		}
		return this;
	}
	
}
