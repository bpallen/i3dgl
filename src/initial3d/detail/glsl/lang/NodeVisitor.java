package initial3d.detail.glsl.lang;

public interface NodeVisitor {

	default void visit(Node node) { }
	
	default void visit(Name name) { }
	
	default void visit(Expression expr) { }
	
}
