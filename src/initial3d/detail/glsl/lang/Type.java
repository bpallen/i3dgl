package initial3d.detail.glsl.lang;

import java.util.List;

import initial3d.detail.glsl.intermediate.Allocator;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.names.BlockName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;

public abstract class Type implements Node {

	private TypeName name;
	private Scope member_scope;
	
	public Type(TypeName name_, Scope member_scope_) {
		name = name_;
		member_scope = member_scope_;
	}
	
	public TypeName name() {
		return name;
	}
	
	public Scope memberScope() {
		return member_scope;
	}
	
	public List<VariableName> memberVariableNames() {
		return member_scope.localDeclaredNames(VariableName.class);
	}
	
	public BlockName block() {
		return null;
	}
	
	@Override
	public Token source() {
		return name.source();
	}
	
	@Override
	public Scope scope() {
		return name.scope();
	}
	
	public abstract Instance instantiate(Token source, TypeName actual_type, Scope scope, boolean lvalue);
	
	/**
	 * This should only be invoked by allocators.
	 * 
	 * @param alloc
	 * @param actual_type
	 * @param layout
	 * @param parent
	 * @return
	 */
	public abstract Location allocate(Allocator alloc, TypeName actual_type, Layout layout, Location parent);
	
	public abstract int allocAlign(Layout layout);
	
	public Class<?> javaEquivalent() {
		return null;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (getClass() != o.getClass()) return false;
		Type ot = (Type) o;
		return name.equals(ot.name);
	}
}
