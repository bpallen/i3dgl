package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.BuildException;

public class NameException extends BuildException {

	private static final long serialVersionUID = 1L;
	
	private Name[] names;
	
	public NameException(String msg_, Name ...names_) {
		super(msg_);
		names = names_.clone();
	}
	
	public Name[] names() {
		return names;
	}
}
