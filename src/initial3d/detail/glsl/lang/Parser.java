package initial3d.detail.glsl.lang;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.PragmaHandler;
import initial3d.detail.glsl.SysInfoLog;
import initial3d.detail.glsl.lang.expressions.ArrayInstance;
import initial3d.detail.glsl.lang.expressions.BoolInstance;
import initial3d.detail.glsl.lang.expressions.DefaultInitializer;
import initial3d.detail.glsl.lang.expressions.FloatInstance;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.IntInstance;
import initial3d.detail.glsl.lang.expressions.MemberAccess;
import initial3d.detail.glsl.lang.expressions.StructInstance;
import initial3d.detail.glsl.lang.expressions.SwizzleAccess;
import initial3d.detail.glsl.lang.expressions.UIntInstance;
import initial3d.detail.glsl.lang.expressions.VariableAccess;
import initial3d.detail.glsl.lang.expressions.VoidInstance;
import initial3d.detail.glsl.lang.names.ArrayAssignOverloadSet;
import initial3d.detail.glsl.lang.names.ArrayCtorOverloadSet;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.BlockName;
import initial3d.detail.glsl.lang.names.FunctionName;
import initial3d.detail.glsl.lang.names.LayoutQualifier;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.lang.names.Qualifier;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.lang.names.SwizzleName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.lang.parselets.ArraySubscriptParselet;
import initial3d.detail.glsl.lang.parselets.ArrayTypeNameParselet;
import initial3d.detail.glsl.lang.parselets.BinaryOperatorParselet;
import initial3d.detail.glsl.lang.parselets.BlockDeclarationParselet;
import initial3d.detail.glsl.lang.parselets.BlockParselet;
import initial3d.detail.glsl.lang.parselets.BreakParselet;
import initial3d.detail.glsl.lang.parselets.ContinueParselet;
import initial3d.detail.glsl.lang.parselets.DiscardParselet;
import initial3d.detail.glsl.lang.parselets.DoParselet;
import initial3d.detail.glsl.lang.parselets.EmptyStatementParselet;
import initial3d.detail.glsl.lang.parselets.ExpressionStatementParselet;
import initial3d.detail.glsl.lang.parselets.ForParselet;
import initial3d.detail.glsl.lang.parselets.FunctionCallParselet;
import initial3d.detail.glsl.lang.parselets.IfParselet;
import initial3d.detail.glsl.lang.parselets.LayoutParselet;
import initial3d.detail.glsl.lang.parselets.MemberSelectionParselet;
import initial3d.detail.glsl.lang.parselets.ParenthesesParselet;
import initial3d.detail.glsl.lang.parselets.PragmaParselet;
import initial3d.detail.glsl.lang.parselets.PrefixOperatorParselet;
import initial3d.detail.glsl.lang.parselets.QualifierParselet;
import initial3d.detail.glsl.lang.parselets.ReturnParselet;
import initial3d.detail.glsl.lang.parselets.SelectionParselet;
import initial3d.detail.glsl.lang.parselets.StructDeclarationParselet;
import initial3d.detail.glsl.lang.parselets.StructParselet;
import initial3d.detail.glsl.lang.parselets.SwizzleDeclarationParselet;
import initial3d.detail.glsl.lang.parselets.WhileParselet;
import initial3d.detail.glsl.lang.source.BuiltinSource;
import initial3d.detail.glsl.lang.statements.BreakStatement;
import initial3d.detail.glsl.lang.statements.CompoundStatement;
import initial3d.detail.glsl.lang.statements.ContinueStatement;
import initial3d.detail.glsl.lang.statements.Declaration;
import initial3d.detail.glsl.lang.statements.DiscardStatement;
import initial3d.detail.glsl.lang.statements.EmptyStatement;
import initial3d.detail.glsl.lang.statements.ExpressionStatement;
import initial3d.detail.glsl.lang.statements.FunctionDeclaration;
import initial3d.detail.glsl.lang.statements.IfStatement;
import initial3d.detail.glsl.lang.statements.LoopStatement;
import initial3d.detail.glsl.lang.statements.ReturnStatement;
import initial3d.detail.glsl.lang.statements.ScopedStatement;
import initial3d.detail.glsl.lang.statements.StructDeclaration;
import initial3d.detail.glsl.lang.statements.SwizzleDeclaration;
import initial3d.detail.glsl.lang.statements.VariableDeclaration;
import initial3d.detail.glsl.preprocessor.Preprocessor;
import initial3d.detail.glsl.tokens.CatTokenInput;
import initial3d.detail.glsl.tokens.IteratorTokenInput;
import initial3d.detail.glsl.tokens.OperatorIdentifierTokenInput;
import initial3d.detail.glsl.tokens.StripEmptyTokenInput;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenInput;
import initial3d.detail.glsl.tokens.Tokenizer;

public class Parser {
	
	private static List<Class<? extends Node>> node_classes = new ArrayList<>();
	
	public static void registerNodeClass(Class<? extends Node> cls) {
		node_classes.add(cls);
	}
	
	static {
		registerNodeClass(Node.class);
		registerNodeClass(Expression.class);
		registerNodeClass(Statement.class);
		registerNodeClass(Declaration.class);
		registerNodeClass(Name.class);
		registerNodeClass(TypeName.class);
		registerNodeClass(ScalarTypeName.class);
		registerNodeClass(ArrayTypeName.class);
		registerNodeClass(VariableName.class);
		registerNodeClass(SwizzleName.class);
		registerNodeClass(FunctionName.class);
		registerNodeClass(OverloadSet.class);
		registerNodeClass(ArrayCtorOverloadSet.class);
		registerNodeClass(ArrayAssignOverloadSet.class);
		registerNodeClass(BlockName.class);
		registerNodeClass(FunctionCall.class);
		registerNodeClass(VariableDeclaration.class);
		registerNodeClass(SwizzleDeclaration.class);
		registerNodeClass(Qualifier.class);
		registerNodeClass(LayoutQualifier.class);
		registerNodeClass(DefaultInitializer.class);
		registerNodeClass(ExpressionStatement.class);
		registerNodeClass(CompoundStatement.class);
		registerNodeClass(ScopedStatement.class);
		registerNodeClass(FunctionDeclaration.class);
		registerNodeClass(Function.class);
		registerNodeClass(Type.class);
		registerNodeClass(Struct.class);
		registerNodeClass(Array.class);
		registerNodeClass(Swizzle.class);
		registerNodeClass(StructDeclaration.class);
		registerNodeClass(ReturnStatement.class);
		registerNodeClass(Variable.class);
		registerNodeClass(VariableAccess.class);
		registerNodeClass(Swizzle.class);
		registerNodeClass(MemberAccess.class);
		registerNodeClass(SwizzleAccess.class);
		registerNodeClass(Instance.class);
		registerNodeClass(VoidInstance.class);
		registerNodeClass(IntInstance.class);
		registerNodeClass(UIntInstance.class);
		registerNodeClass(FloatInstance.class);
		registerNodeClass(BoolInstance.class);
		registerNodeClass(ArrayInstance.class);
		registerNodeClass(StructInstance.class);
		registerNodeClass(IfStatement.class);
		registerNodeClass(LoopStatement.class);
		registerNodeClass(BreakStatement.class);
		registerNodeClass(ContinueStatement.class);
		registerNodeClass(DiscardStatement.class);
		
		// TODO update this for new node classes
	}
	
	static {
		try {
			// initialize builtins
			Reader r = BuiltinSource.open("builtin.glsl");
			TokenInput tin = new Tokenizer(new SysInfoLog(), "<builtin>", r);
			Preprocessor pp = new Preprocessor(tin.infoLog());
			pp.addInput(tin);
			Parser p = new Parser(pp, BuiltinScope.instance);
			System.err.println("I3DGL: parsing builtins...");
			long time0 = System.currentTimeMillis();
			p.parseAll();
			System.err.printf("I3DGL: parsing builtins done (%.2f seconds)\n", (System.currentTimeMillis() - time0) / 1000.0);
		} catch (IOException e) {
			throw new AssertionError(e);
		}
	}
	
	@SafeVarargs
	public static Class<? extends Node>[] makeNodeClasses(Class<? extends Node> ...clss) {
		return clss;
	}
	
	private static class InfixRule {
		public Map<String, InfixParselet> infix_tokens = new HashMap<>();
	}
	
	private CatTokenInput input;
	private HashMap<String, PrefixParselet> prefix_tokens = new HashMap<>();
	private HashMap<Class<?>, InfixRule> infix_rules = new HashMap<>();
	private HashMap<String, PragmaHandler> pragmas = new HashMap<>();
	private HashMap<String, PragmaHandler> pragmas_i3d = new HashMap<>();
	private Scope global_scope;
	private Scope current_scope;
	private Stack<Diagnostics> diagnostics = new Stack<>();
	
	Parser(TokenInput in_, Scope global_scope_) {
		diagnostics.push(new Diagnostics());
		input = new CatTokenInput(new OperatorIdentifierTokenInput(new StripEmptyTokenInput(Objects.requireNonNull(in_))));
		global_scope = global_scope_;
		current_scope = global_scope;
		current_scope.allowDeclarations();
		prefix("__pragma", new PragmaParselet());
		prefix("struct", new StructParselet());
		prefix("return", new ReturnParselet());
		prefix("layout", new LayoutParselet());
		qualifier("const");
		qualifier("in");
		qualifier("out");
		qualifier("inout");
		qualifier("uniform");
		qualifier("flat");
		qualifier("smooth");
		qualifier("noperspective");
		qualifier("__ref");
		qualifier("__constexpr");
		qualifier("__arith");
		prefix("__swizzle", new SwizzleDeclarationParselet());
		prefix("if", new IfParselet());
		prefix("while", new WhileParselet());
		prefix("do", new DoParselet());
		prefix("for", new ForParselet());
		prefix("break", new BreakParselet());
		prefix("continue", new ContinueParselet());
		prefix("discard", new DiscardParselet());
		prefix("(", new ParenthesesParselet());
		prefix(";", new EmptyStatementParselet());
		infix(Expression.class, ";", new ExpressionStatementParselet());
		infix(Expression.class, "[", new ArraySubscriptParselet());
		infix(Expression.class, ".", new MemberSelectionParselet());
		infix(OverloadSet.class, "(", new FunctionCallParselet());
		infix(ScalarTypeName.class, ";", new StructDeclarationParselet());
		infix(ScalarTypeName.class, "[", new ArrayTypeNameParselet());
		infix(BlockName.class, "{", new BlockParselet());
		infix(BlockName.class, ";", new BlockDeclarationParselet());
		// TODO sequence operator
		infix(Expression.class, "?", new SelectionParselet());
		prefixOperator("-");
		prefixOperator("++");
		prefixOperator("--");
		prefixOperator("!");
		prefixOperator("~");
		binaryOperator("=", Precedence.ASSIGNMENT, true);
		binaryOperator("+=", Precedence.ASSIGNMENT, true);
		binaryOperator("-=", Precedence.ASSIGNMENT, true);
		binaryOperator("*=", Precedence.ASSIGNMENT, true);
		binaryOperator("/=", Precedence.ASSIGNMENT, true);
		binaryOperator("%=", Precedence.ASSIGNMENT, true);
		binaryOperator("<<=", Precedence.ASSIGNMENT, true);
		binaryOperator(">>=", Precedence.ASSIGNMENT, true);
		binaryOperator("&=", Precedence.ASSIGNMENT, true);
		binaryOperator("^=", Precedence.ASSIGNMENT, true);
		binaryOperator("|=", Precedence.ASSIGNMENT, true);
		binaryOperator("||", Precedence.LOGICAL_OR);
		binaryOperator("^^", Precedence.LOGICAL_XOR);
		binaryOperator("&&", Precedence.LOGICAL_AND);
		binaryOperator("|", Precedence.BITWISE_OR);
		binaryOperator("^", Precedence.BITWISE_XOR);
		binaryOperator("&", Precedence.BITWISE_AND);
		binaryOperator("==", Precedence.EQUALITY);
		binaryOperator("!=", Precedence.EQUALITY);
		binaryOperator("<", Precedence.RELATIONAL);
		binaryOperator(">", Precedence.RELATIONAL);
		binaryOperator("<=", Precedence.RELATIONAL);
		binaryOperator(">=", Precedence.RELATIONAL);
		binaryOperator("<<", Precedence.BITSHIFT);
		binaryOperator(">>", Precedence.BITSHIFT);
		binaryOperator("+", Precedence.ADDITIVE);
		binaryOperator("-", Precedence.ADDITIVE);
		binaryOperator("*", Precedence.MULTIPLICATIVE);
		binaryOperator("/", Precedence.MULTIPLICATIVE);
		binaryOperator("%", Precedence.MULTIPLICATIVE);
		// TODO prefix/suffix increment/decrement
		
		pragma("i3d", (TokenInput in) -> {
			in.pushContext("parsing i3d pragma", in.peekToken());
			try {
				in.expectNextToken("i3d");
				if (in.hasNextToken()) {
					Token tp = in.peekToken();
					PragmaHandler ph = pragmas_i3d.get(tp.text());
					if (ph != null) {
						return ph.pragma(in);
					} else {
						in.infoLog().warning("unknown i3d pragma", tp);
					}
				}
			} finally {
				in.popContext();
			}
			return in.asEmpty();
		});
		
		pragmaI3D("diagnostic", (TokenInput in) -> {
			in.pushContext("parsing i3d diagnostic pragma", in.peekToken());
			try {
				in.infoLog().showParentContext(false);
				in.expectNextToken("diagnostic");
				Token t0 = in.expectNextToken("enable", "disable", "push", "pop");
				if (t0.isText("push")) {
					pushDiagnostics();
				} else if (t0.isText("pop")) {
					popDiagnostics();
				} else if (t0.isText("enable") || t0.isText("disable")) {
					// TODO set state for multiple diagnostics
					Token td = in.nextToken();
					try {
						Method m = Diagnostics.class.getMethod("diag_" + td.text(), Parser.class, Token.class, boolean.class);
						m.invoke(diagnostics(), Parser.this, td, t0.isText("enable"));
					} catch (NoSuchMethodException e) {
						in.infoLog().warning("unknown diagnostic", td);
					} catch (Exception e) {
						in.infoLog().warning("unable to set diagnostic: " + e.getMessage(), td);
						e.printStackTrace();
					}
				}
			} finally {
				in.popContext();
			}
			return in.asEmpty();
		});
		
	}
	
	public Parser(TokenInput in_) {
		this(in_, new Scope(BuiltinScope.instance));
	}
	
	public InfoLog infoLog() {
		return input.infoLog();
	}
	
	public TokenInput input() {
		return input;
	}
	
	public List<Token> captureTokens(String open, String close) throws IOException {
		return input.capture(open, close);
	}
	
	public void replayTokens(Iterator<Token> tokens) {
		input.prepend(new IteratorTokenInput(input.infoLog(), Objects.requireNonNull(tokens)));
	}
	
	public Scope globalScope() {
		return global_scope;
	}
	
	public Scope currentScope() {
		return current_scope;
	}
	
	/**
	 * Push a new scope; owner is inherited from current scope.
	 * 
	 * @return
	 */
	public Scope pushScope() {
		current_scope = new Scope(current_scope);
		current_scope.owner(current_scope.parent().owner());
		return current_scope;
	}
	
	public Scope pushScope(Scope s) {
		if (s.parent() != current_scope) throw new IllegalArgumentException();
		current_scope = s;
		return current_scope;
	}
	
	public Scope popScope() {
		if (current_scope == global_scope) throw new IllegalStateException();
		Scope s = current_scope;
		current_scope = current_scope.parent();
		return s;
	}
	
	public void prefix(String tok, PrefixParselet prefix) {
		prefix_tokens.put(tok, prefix);
	}
	
	public <T extends Node> void infix(Class<T> cls, String tok, InfixParselet infix) {
		for (Class<? extends Node> c : node_classes) {
			// for all subclasses of the input to this parselet
			if (cls.isAssignableFrom(c)) {
				InfixRule rule = infix_rules.get(c);
				if (rule == null) {
					rule = new InfixRule();
					infix_rules.put(c, rule);
				}
				// map that posssible concrete input class with the specified token to this parselet
				rule.infix_tokens.put(tok, infix);
			}
		}
	}
	
	public void qualifier(String q) {
		prefix(q, new QualifierParselet(Precedence.QUALIFICATION));
	}
	
	public void prefixOperator(String op) {
		prefix(op, new PrefixOperatorParselet(op));
	}
	
	public void binaryOperator(String op, int prec) {
		infix(Expression.class, op, new BinaryOperatorParselet(op, prec));
	}
	
	public void binaryOperator(String op, int prec, boolean right) {
		infix(Expression.class, op, new BinaryOperatorParselet(op, prec, right));
	}
	
	public void pragma(String name, PragmaHandler ph) {
		pragmas.put(name, ph);
	}
	
	public void pragmaI3D(String name, PragmaHandler ph) {
		pragmas_i3d.put(name, ph);
	}
	
	public PragmaHandler pragma(String name) {
		return pragmas.get(name);
	}
	
	public Diagnostics diagnostics() {
		return diagnostics.lastElement();
	}
	
	public Diagnostics pushDiagnostics() {
		diagnostics.push(diagnostics.lastElement().clone());
		return diagnostics.lastElement();
	}
	
	public Diagnostics popDiagnostics() {
		if (diagnostics.size() == 1) throw new IllegalStateException("cant pop last diagnostics state"); 
		return diagnostics.pop();
	}
	
	public Statement parseControlBody() throws IOException {
		if (input.hasNextToken("{")) {
			CompoundStatement s = new CompoundStatement(input.peekToken(), current_scope.parent());
			input.expectNextToken("{");
			while (!input.hasNextToken("}")) {
				s.add(parse(Statement.class, 0));
			}
			input.expectNextToken("}");
			// simplify if only one statement in block
			if (s.statements().size() == 1) return s.statements().get(0);
			return s;
		} else {
			return parse(Statement.class, 0);
		}
	}
	
	public <T extends Node> T parse(Class<T> cls, int prec) throws IOException {
		Token t0 = input.peekToken();
		Node n = null;
		try {
			n = parse(prec);
			return cls.cast(n);
		} catch (BuildException | ClassCastException e) {
			// if we already failed a node class expectation, don't bother to report more failures
			if (e instanceof NodeExpectationException) throw e;
			input.pushContext("parsing", t0);
			StringBuilder sb = new StringBuilder("Expected ");
			sb.append(cls.getSimpleName());
			if (n != null) {
				sb.append(", not ");
				sb.append(n.getClass().getSimpleName());
			}
			sb.append(". ");
			InfixRule rule = n == null ? null : infix_rules.get(n.getClass());
			if (rule != null) {
				boolean first = true;
				for (Map.Entry<String, InfixParselet> me : rule.infix_tokens.entrySet()) {
					for (Class<? extends Node> cc : me.getValue().nodeClasses()) {
						if (cls.isAssignableFrom(cc)) {
							if (first) sb.append("Possibly missing: ");
							if (!first) sb.append(", ");
							sb.append('\'');
							sb.append(me.getKey());
							sb.append('\'');
							first = false;
						}
					}
				}
			}
			input.infoLog().error(sb.toString(), input.peekToken());
			input.popContext();
			throw new NodeExpectationException(e);
		}
	}
	
	private Node handleVariableDeclaration(TypeName l, Token t) throws IOException {
		// TODO initialization as separate statements
		// TODO what was ^ referring to exactly?
		// variable declaration/definition
		CompoundStatement decl = new CompoundStatement(t, current_scope);
		do {
			TypeName type = l;
			// layout qualifiers appear on the type
			LayoutQualifier ql = (LayoutQualifier) type.dequalify("layout");
			if (input.hasNextToken("[")) {
				// array variable declarations
				// the parselet moves qualifiers to the array type
				ArrayTypeNameParselet pl = new ArrayTypeNameParselet();
				type = ((TypeName) pl.parse(this, l, input.nextToken())).copy();
			}
			VariableName vname = new VariableName(t, current_scope, type);
			// move interpolation qualifiers from type to variable name
			vname.qualify(type.dequalify("flat"));
			vname.qualify(type.dequalify("smooth"));
			vname.qualify(type.dequalify("noperspective"));
			// move interface qualifiers from type to variable name
			Qualifier qiface = null;
			if (vname.scope().global()) {
				vname.qualify(type.dequalify("in"));
				vname.qualify(type.dequalify("out"));
				vname.qualify(type.dequalify("uniform"));
				qiface = vname.interfaceQualifier();
			}
			// use block interface qualifier when block member
			if (vname.scope().owner() instanceof Struct) {
				BlockName b = ((Struct) vname.scope().owner()).block();
				if (b != null) qiface = b.interfaceQualifier();
			}
			// parse initializer expression
			Expression expr = null;
			Token tass = null;
			if (input.hasNextToken("=")) {
				input.pushContext("parsing variable initializer", input.peekToken());
				try {
					tass = input.nextToken();
					expr = parse(Expression.class, Precedence.SEQUENCE);
					expr = current_scope.convert(infoLog(), type, expr, false, false);
					// FIXME use maybeEval
				} finally {
					input.popContext();
				}
			}
			// try declare/define
			try {
				vname.declare(infoLog());
				Variable v = null;
				if (type instanceof ArrayTypeName) {
					int s0 = ((ArrayTypeName) type).size();
					int s1 = expr == null ? -1 : ((ArrayTypeName) expr.type()).size();
					if (s0 > -1 && s1 > -1 && s0 != s1) {
						infoLog().error("explicitly sized array must be initialized by expression of same size", t);
					}
					((ArrayTypeName) type).size(Math.max(s0, s1));
				}
				// TODO there was something i was gonna do here, what was it?
				v = new Variable(vname);
				v.initExpr(expr, infoLog());
				if (vname.type().qualified("__ref") && (expr == null || false)) {
					// TODO check ref-resolvable if ref
					
				} else if (vname.type().qualified("const") && (expr == null || !v.constexpr())) {
					// glsl requires const variables to be constexpr
					// TODO except interface variables?
					infoLog().error("constant initialized by non-constant expression", t);
				}
				if (vname.scope().global()) {
					// global scope -> static var
					vname.define(v, infoLog());
				} else if (vname.scope().owner() instanceof Function) {
					// function scope -> local var
					vname.define(v, infoLog());
				} else if (vname.scope().owner() instanceof Type) {
					// type scope -> member var
					if (expr != null) infoLog().error("member initializers are not allowed", expr.source());
					// although this is defined so it can hold layout info etc, it is never accessed directly
					// (until we implement static member vars...)
					vname.define(v, infoLog());
				} else {
					infoLog().internalError("bad scope for variable definition", vname.source());
					throw new BuildException("bad scope for variable definition");
				}
				// if interface-qualified, create layout
				if (qiface != null) {
					// clone scope layout
					Layout lay = current_scope.layout(qiface.text()).clone();
					// then apply any changes for this declaration
					if (ql != null) lay.apply(ql, infoLog());
					// and apply interpolation qualifiers to the layout
					if (vname.qualified("flat")) lay.interp = Layout.Interpolation.FLAT;
					if (vname.qualified("smooth")) lay.interp = Layout.Interpolation.SMOOTH;
					if (vname.qualified("noperspective")) lay.interp = Layout.Interpolation.NOPERSPECTIVE;
					v.layout(lay);
				} else if (ql != null) {
					input.infoLog().warning("layout qualifier ignored", ql.source());
				}
				// done, add to (potential) list
				decl.add(new VariableDeclaration(vname));
			} catch (NameException e) {
				// TODO cause?
			}
			// next variable in list
			if (input.expectNextToken(",", ";").isText(",")) {
				t = input.expectNextToken(Token.Type.IDENTIFIER);
			} else {
				t = Token.EOF;
			}
		} while (t.type() == Token.Type.IDENTIFIER);
		// trim declaration statement if possible
		if (decl.size() == 0) {
			return new EmptyStatement(t, current_scope);
		} else if (decl.size() == 1) {
			return decl.get(0);
		} else {
			return decl;
		}
	}
	
	private Node handleFunctionDeclaration(TypeName l, Token t) throws IOException {
		input.expectNextToken("(");
		input.consumeNextToken("void");
		// is this a member function?
		Struct owner_type = null;
		try {
			owner_type = (Struct) current_scope.owner();
		} catch (ClassCastException | NullPointerException e) {
			// owner not a struct
		}
		// new scope to declare parameters, regardless of whether we define anything or not
		pushScope();
		try {
			// parse declaration
			List<VariableName> params = new ArrayList<>();
			List<TypeName> param_types = new ArrayList<>();
			// if member function, add 'this' as first param
			if (owner_type != null) {
				TypeName thistype = ((Type) current_scope.owner()).name().copy();
				thistype.qualify(new Qualifier(t.withText("__ref"), current_scope));
				VariableName thisname = new VariableName(t.withText("this"), current_scope, thistype);
				param_types.add(thistype);
				params.add(thisname);
				// TODO if param scope separated, move to body scope
				current_scope.linkVariable(thisname, infoLog());
			}
			// parse real formal params
			while (!input.hasNextToken(")")) {
				TypeName ptype = parse(TypeName.class, Precedence.DECLARATION);
				Token vtok = input.expectNextToken(Token.Type.IDENTIFIER);
				if (input.hasNextToken("[")) {
					// array variable declarations
					// the parselet moves qualifiers to the array type
					ArrayTypeNameParselet pl = new ArrayTypeNameParselet();
					ptype = ((TypeName) pl.parse(this, ptype, input.nextToken())).copy();
				}
				if (ptype instanceof ArrayTypeName) {
					// note that the first parse() could produce an array type too
					if (((ArrayTypeName) ptype).size() >= 0) {
						input.infoLog().error("function parameter of array type cannot specify array size", ptype.source());
					}
				}
				VariableName pname = new VariableName(vtok, current_scope, ptype);
				param_types.add(ptype);
				params.add(pname);
				// TODO separate param scope from body scope for ease of inlining?
				input.consumeNextToken(",");
			}
			input.expectNextToken(")");
			// qualifiers for member functions
			if (owner_type != null) {
				Token tq = input.consumeNextToken("const");
				if (tq != null) {
					param_types.get(0).qualify(new Qualifier(tq, current_scope));
				}
			}
			FunctionName fname = new FunctionName(t, current_scope.parent(), l, (TypeName[]) param_types.toArray(new TypeName[param_types.size()]));
			// steal function qualifiers from return type
			fname.qualify(l.dequalify("__constexpr"));
			fname.qualify(l.dequalify("__arith"));
			// grab function body, if there is one
			List<Token> bodytokens = null;
			if (input.hasNextToken("{")) {
				bodytokens = captureTokens("{", "}");
			} else {
				input.expectNextToken(";", "{");
			}
			// (re)declare
			FunctionDeclaration decl = null;
			try {
				fname.declare(infoLog());
				decl = new FunctionDeclaration(fname, null);
			} catch (NameException e) {
				// probably declared incompatibly
				return new EmptyStatement(t, current_scope.parent());
			}
			// handle definition if we have a body
			if (bodytokens != null) {
				CompoundStatement body = new CompoundStatement(bodytokens.get(0), current_scope);
				// function takes scope ownership and declares/defines the params
				Function func = new Function(infoLog(), fname, current_scope, body, (VariableName[]) params.toArray(new VariableName[params.size()]));
				// define function
				try {
					fname.define(func, infoLog());
				} catch (NameException e) {
					// probably already defined
					return new EmptyStatement(t, current_scope.parent());
				}
				decl = new FunctionDeclaration(fname, func);
				if (owner_type == null) {
					// parse now if free function
					input.pushContext("parsing function definition", t);
					try {
						replayTokens(bodytokens.iterator());
						input.expectNextToken("{");
						while (!input.hasNextToken("}")) {
							body.add(parse(Statement.class, 0));
						}
						input.expectNextToken("}");
					} finally {
						input.popContext();
					}
				} else {
					// defer parsing if member function
					final List<Token> bodytokens2 = bodytokens;
					owner_type.addBuildTask(() -> {
						input.pushContext("parsing deferred member function definition", t);
						// get back to function scope
						pushScope(func.bodyScope());
						try {
							replayTokens(bodytokens2.iterator());
							try {
								input.expectNextToken("{");
								while (!input.hasNextToken("}")) {
									body.add(parse(Statement.class, 0));
								}
								input.expectNextToken("}");
							} catch (IOException e) {
								// have to catch or use an interface that throws.
								// because we're parsing replayed input, no io error should happen.
								throw new AssertionError(e);
							}
						} finally {
							popScope();
							input.popContext();
						}
					});
				}
			}
			return decl;
		} finally {
			popScope();
		}
	}
	
	private Node handleInfixIdentifier(Node l, Token t) throws IOException {
		if (l instanceof TypeName) {
			if (input.hasNextToken("[", "=", ";", ",")) {
				return handleVariableDeclaration((TypeName) l, t);
			} else if (input.hasNextToken("(")) {
				return handleFunctionDeclaration((TypeName) l, t);
			} else {
				input.expectNextToken("[", "=", ";", "(");
			}
		}
		if (l instanceof BlockName) {
			return new BlockDeclarationParselet().parse(this, l, t);
		}
		throw input.expectationFailure(t);
	}
	
	private Node handlePrefixIdentifier(Token t) throws IOException {
		if (input.hasNextToken("{")) return new BlockName(t, current_scope);
		// attempt to resolve function call anyway for possible universal call or ADL,
		// neither of which are actually implemented currently.
		// this does give nicer errors for undeclared functions though.
		if (input.hasNextToken("(")) return new OverloadSet(t, current_scope, null);
		throw input.expectationFailure(t);
	}
	
	private boolean declarable(Token t) {
		return t.type() == Token.Type.IDENTIFIER;
	}
	
	private int precedence(Node l, Token t) {
		InfixRule rule = infix_rules.get(l.getClass());
		if (rule != null) {
			InfixParselet infix = rule.infix_tokens.get(t.text());
			if (infix != null) return infix.precedence();
		}
		// special handling for declarations
		if ((l instanceof TypeName || l instanceof BlockName) && declarable(t)) return Precedence.DECLARATION;
		return 0;
	}
	
	public Node parseInfix(Node l, int prec) throws IOException {
		Token t;
		while (precedence(l, t = input.peekToken()) > prec) {
			input.nextToken();
			if (declarable(t)) {
				// special handling of infix identifiers (declarations)
				l = handleInfixIdentifier(l, t);
				continue;
			}
			InfixRule rule = infix_rules.get(l.getClass());
			InfixParselet infix = rule.infix_tokens.get(t.text());
			l = infix.parse(this, l, t);
			// typename -> ctor
			if (l instanceof TypeName && input.hasNextToken("(")) {
				Token s = l.source().withText("__ctor_" + ((TypeName) l).text());
				l = current_scope.access(s, infoLog());
				if (l == null) {
					input.infoLog().internalError("typename to constructor failed", s);
					throw new BuildException("typename to constructor failed");
				}
			}
		}
		// if a parsed expression is constant, evaluate immediately
		if (l instanceof Expression) {
			Expression x = (Expression) l;
			if (x.constexpr()) {
				try {
					l = x.eval(false, infoLog());
				} catch (BuildException e) {
					input.infoLog().error("Failed to evaluate purportedly-constant expression", x.source());
				}
			}
		}
		return l;
	}
	
	public Node parse(int prec) throws IOException {
		// get a token
		Token t = input.unexpectNextToken(Token.Type.UNKNOWN);
		if (t.type() == Token.Type.EOF) return null;
		Node l = null;
		// basic behaviour from type
		switch (t.type()) {
		case INT_LITERAL:
		case FLOAT_LITERAL:
			l = Instance.parse(t, current_scope);
			break;
		case KEYWORD:
		case IDENTIFIER:
			try {
				l = current_scope.access(t, infoLog());
				if (t.isText("true") || t.isText("false")) l = Instance.parse(t, current_scope);
				if (l != null) break;
			} catch (Exception e) {
				input.infoLog().internalError(e.getMessage(), t);
				throw e;
			}
		default:
			// operator/keyword prefix behaviour
			PrefixParselet prefix = prefix_tokens.get(t.text());
			if (prefix == null) {
				if (t.type() == Token.Type.IDENTIFIER) {
					l = handlePrefixIdentifier(t);
				} else {
					try {
						input.expectationFailure(t);
					} finally {
						// TODO should this go here? we have to know that we're actually aborting
						// because we're consuming input for a diagnostic message
						StringBuilder sb = new StringBuilder("Remaining tokens on input line: ");
						input.readLine((Token tx) -> {
							sb.append(tx.text());
							sb.append(" ");
						});
						input.infoLog().detail(sb.toString(), t);
					}
				}
			} else {
				l = prefix.parse(this, t);
			}
		}
		if (l == null) input.expectationFailure(t);
		// typename -> ctor
		if (l instanceof TypeName && input.hasNextToken("(")) {
			Token s = l.source().withText("__ctor_" + ((TypeName) l).text());
			l = current_scope.access(s, infoLog());
			if (l == null) {
				input.infoLog().internalError("typename to constructor failed", s);
				throw new BuildException("typename to constructor failed");
			}
		}
		// infix
		l = parseInfix(l, prec);
		// parsedump?
		if (diagnostics().parsedump_scope == current_scope && prec == 0 && l != null) {
			String s = l.toString();
			input.infoLog().detail(String.format(s.contains("\n") ? "(%s)\n" : "(%s) ", l.source().fileLineCol()) + s);
		}
		return l;
	}
	
	public void parseAll() throws IOException {
		while (!input.hasNextToken(Token.Type.EOF)) {
			parse(0);
		}
	}
	
}
