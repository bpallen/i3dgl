package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.VariableAccess;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;

public class Variable implements Node {
	
	private VariableName name;
	private Expression initexpr;
	private Instance initval;
	private Layout layout;
	
	public Variable(VariableName name_) {
		name = name_;
	}
	
	public VariableName name() {
		return name;
	}
	
	@Override
	public Scope scope() {
		return name.scope();
	}
	
	@Override
	public Token source() {
		return name.source();
	}

	public TypeName type() {
		return name.type();
	}
	
	public Expression initExpr() {
		return initexpr;
	}
	
	public void initExpr(Expression x, InfoLog infolog) {
		initexpr = x;
		if (x != null) {
			if (type().qualified("__ref")) {
				// TODO ref-eval to check ref is resolvable at compile time
			} else if (type().qualified("const")) {
				try {
					initval = initexpr.eval(false, infolog);
				} catch (EvalException e) {
					initval = null;
				}
			}
		}
	}
	
	public Instance initVal() {
		return initval;
	}
	
	public boolean constexpr() {
		return initval != null;
	}
	
	public Layout layout() {
		return layout;
	}
	
	public void layout(Layout l) {
		layout = l;
	}
	
	public VariableAccess access(VariableName n) {
		if (!name.declEquals(n)) throw new NameException("bad name for variable access", n);
		return new VariableAccess(this, n);
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (getClass() != o.getClass()) return false;
		Variable ov = (Variable) o;
		return name.equals(ov.name);
	}
	
}
