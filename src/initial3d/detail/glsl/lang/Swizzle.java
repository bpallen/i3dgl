package initial3d.detail.glsl.lang;

import java.util.HashMap;
import java.util.Map;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.TranslateException;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.StructLocation;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.StructInstance;
import initial3d.detail.glsl.lang.expressions.SwizzleAccess;
import initial3d.detail.glsl.lang.names.SwizzleName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;

public class Swizzle implements Node {

	private SwizzleName name;
	private Scope source_scope;
	private Scope target_scope;
	
	// either a map of target expressions to source expressions or just a single expression
	// TODO this doesn't really need to be a map
	private HashMap<Expression, Expression> target_exprs = new HashMap<>();
	private Expression target_expr = null;
	
	/**
	 * Takes ownership of the source and target scopes and allows declarations.
	 * 
	 * @param name_
	 * @param source_scope_
	 * @param target_scope_
	 */
	public Swizzle(SwizzleName name_, Scope source_scope_, Scope target_scope_) {
		name = name_;
		source_scope = source_scope_;
		target_scope = target_scope_;
		source_scope.owner(this);
		target_scope.owner(this);
		source_scope.allowDeclarations();
		target_scope.allowDeclarations();
	}
	
	public void targetExpr(Expression ex) {
		target_exprs.clear();
		target_expr = ex;
	}
	
	public void targetFieldExpr(Expression tex, Expression sex) {
		target_expr = null;
		target_exprs.put(tex, sex);
	}
	
	public SwizzleName name() {
		return name;
	}
	
	@Override
	public Token source() {
		return name.source();
	}

	@Override
	public Scope scope() {
		return name.scope();
	}
	
	public TypeName type() {
		return name.type();
	}

	@Override
	public String toString() {
		return "fixme lol";
	}
	
	public SwizzleAccess access(SwizzleName n, Expression thisexpr) {
		if (!name().declEquals(n)) throw new NameException("bad name for swizzle access", n);
		return new SwizzleAccess(this, n, thisexpr);
	}
	
	public Instance eval(VariableInstanceMap vars, Expression thisexpr) {
		if (target_expr != null) {
			vars.pushScope(source_scope);
			try {
				vars.put(source_scope.declaredVariable("this", false, vars.infoLog()), thisexpr.eval(vars));
				return target_expr.eval(vars);
			} finally {
				vars.popScope(source_scope);
			}
		} else {
			StructInstance target = new StructInstance(source(), type(), thisexpr.scope(), thisexpr.lValue());
			Instance source = thisexpr.eval(vars);
			// replace original owned member instances by whatever the expressions eval to
			for (Map.Entry<Expression, Expression> me : target_exprs.entrySet()) {
				// eval source expression
				Instance sx = null;
				vars.pushScope(source_scope);
				try {
					vars.put(source_scope.declaredVariable("this", false, vars.infoLog()), source);
					sx = me.getValue().eval(vars);
				} finally {
					vars.popScope(source_scope);
				}
				// eval target expression
				Instance tx = null;
				vars.pushScope(target_scope);
				try {
					vars.put(target_scope.declaredVariable("this", false, vars.infoLog()), target);
					tx = me.getKey().eval(vars);
				} finally {
					vars.popScope(target_scope);
				}
				// replace subinstance in target
				if (!target.replaceSubInstance(tx, sx)) {
					throw new EvalException("swizzle subinstance replaced failed");
				}
			}
			return target;
		}
	}
	
	public Location translate(Translator trans, Expression thisexpr) {
		if (target_expr != null) {
			trans.pushScope(source_scope);
			try {
				trans.bindReference(source_scope.declaredVariable("this", false, trans.infoLog()), thisexpr.translate(trans));
				return target_expr.translate(trans);
			} finally {
				trans.popScope(source_scope);
			}
		} else {
			StructLocation target = (StructLocation) trans.temporary(type());
			Location source = thisexpr.translate(trans);
			// replace original owned locations by whatever the expressions eval to
			for (Map.Entry<Expression, Expression> me : target_exprs.entrySet()) {
				// translate source expression
				Location sl = null;
				trans.pushScope(source_scope);
				try {
					trans.bindReference(source_scope.declaredVariable("this", false, trans.infoLog()), source);
					sl = me.getValue().translate(trans);
				} finally {
					trans.popScope(source_scope);
				}
				// translate target expression
				Location tl = null;
				trans.pushScope(target_scope);
				try {
					trans.bindReference(target_scope.declaredVariable("this", false, trans.infoLog()), target);
					tl = me.getKey().translate(trans);
				} finally {
					trans.popScope(target_scope);
				}
				// replace sublocation in target
				if (!target.replaceSubLocation(tl, sl)) {
					throw new TranslateException("swizzle sublocation replace failed");
				}
			}
			return target;
		}
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (getClass() != o.getClass()) return false;
		Swizzle oz = (Swizzle) o;
		return name.equals(oz.name);
	}
}
