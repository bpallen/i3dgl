package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.intermediate.Location;
import initial3d.detail.glsl.intermediate.Translator;

public interface FunctionTranslator {
	public Location translate(Translator trans, Location ...actual_params);
}
