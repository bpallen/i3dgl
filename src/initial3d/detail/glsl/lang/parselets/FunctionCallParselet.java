package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.InfixParselet;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.expressions.DefaultInitializer;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

public class FunctionCallParselet implements InfixParselet {

	@Override
	public Node parse(Parser p, Node l, Token t) throws IOException {
		assert t.isText("(");
		assert l instanceof OverloadSet;
		OverloadSet o = (OverloadSet) l;
		List<Expression> params = new ArrayList<>();
		List<TypeName> ptypes = new ArrayList<>();
		while (!p.input().hasNextToken(")")) {
			Expression e = p.parse(Expression.class, Precedence.SEQUENCE);
			params.add(e);
			ptypes.add(e.type());
			p.input().consumeNextToken(",");
		}
		p.input().expectNextToken(")");
		FunctionCall c = o.call(p.infoLog(), t, p.currentScope(), (Expression[]) params.toArray(new Expression[params.size()]));
		if (c != null) return c;
		// return new DefaultInitializer(t, p.currentScope(), p.currentScope().declaredType("int"));
		throw new BuildException("overload resolution failed");
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(FunctionCall.class);
	}

	@Override
	public int precedence() {
		return Precedence.CALL;
	}
	
}