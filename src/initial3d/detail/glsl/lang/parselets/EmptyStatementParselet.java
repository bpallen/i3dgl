package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.statements.EmptyStatement;
import initial3d.detail.glsl.tokens.Token;

public class EmptyStatementParselet implements PrefixParselet {

	@Override
	public Node parse(Parser p, Token t) throws IOException {
		return new EmptyStatement(t, p.currentScope());
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(EmptyStatement.class);
	}

}
