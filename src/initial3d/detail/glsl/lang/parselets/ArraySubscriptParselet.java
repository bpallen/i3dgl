package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.InfixParselet;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.tokens.Token;

public class ArraySubscriptParselet implements InfixParselet {

	@Override
	public Node parse(Parser p, Node l, Token t) throws IOException {
		Expression re = p.parse(Expression.class, Precedence.STATEMENT);
		Expression le = (Expression) l;
		p.input().expectNextToken("]");
		Token t1 = t.withText("operator[]");
		OverloadSet o = le.type().definition().memberScope().useOverload(t1, false, p.infoLog());
		if (o == null) {
			p.input().infoLog().error("no available overloads for operator[]", t);
			throw new BuildException("no overloads");
		}
		FunctionCall c = o.call(p.infoLog(), t, p.currentScope(), le, re);
		if (c != null) return c;
		throw new BuildException("overload resolution failed");
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(FunctionCall.class);
	}

	@Override
	public int precedence() {
		return Precedence.CALL;
	}

}
