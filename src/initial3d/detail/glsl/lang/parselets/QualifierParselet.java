package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.names.Qualifier;
import initial3d.detail.glsl.tokens.Token;

public class QualifierParselet implements PrefixParselet {

	private int prec;
	
	public QualifierParselet(int prec_) {
		prec = prec_;
	}
	
	@Override
	public Node parse(Parser p, Token t) throws IOException {
		if (p.input().hasNextToken(";")) return new Qualifier(t, p.currentScope());
		Name n = p.parse(Name.class, prec - 1);
		n = n.copy();
		n.qualify(new Qualifier(t, p.currentScope()));
		return n;
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(Name.class, Qualifier.class);
	}
	
}
