package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.tokens.Token;

public class PrefixOperatorParselet implements PrefixParselet {

	private String op;
	
	public PrefixOperatorParselet(String op_) {
		op = op_;
	}
	
	@Override
	public Node parse(Parser p, Token t) throws IOException {
		Expression re = p.parse(Expression.class, Precedence.PREFIX - 1);
		Token t1 = t.withText("operator" + op);
		// first try member lookup (no recurse)
		OverloadSet o = re.type().definition().memberScope().useOverload(t1, false, p.infoLog());
		// else try normal lookup
		if (o == null) o = p.currentScope().useOverload(t1, p.infoLog());
		if (o == null) {
			p.input().infoLog().error("no available overloads for operator" + op, t);
			throw new BuildException("no overloads");
		}
		FunctionCall c = o.call(p.infoLog(), t, p.currentScope(), re);
		if (c != null) return c;
		throw new BuildException("overload resolution failed");
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(FunctionCall.class);
	}

}
