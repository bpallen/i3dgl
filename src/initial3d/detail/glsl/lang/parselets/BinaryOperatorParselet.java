package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.InfixParselet;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.expressions.DefaultInitializer;
import initial3d.detail.glsl.lang.expressions.FunctionCall;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.tokens.Token;

public class BinaryOperatorParselet implements InfixParselet {

	private String op;
	private int prec;
	private boolean right;
	
	public BinaryOperatorParselet(String op_, int prec_, boolean right_) {
		op = op_;
		prec = prec_;
		right = right_;
	}
	
	public BinaryOperatorParselet(String op_, int prec_) {
		this(op_, prec_, false);
	}
	
	@Override
	public Node parse(Parser p, Node l, Token t) throws IOException {
		Expression re = p.parse(Expression.class, right ? prec - 1 : prec);
		Expression le = (Expression) l;
		Token t1 = t.withText("operator" + op);
		// first try lhs member lookup (no recurse)
		OverloadSet o = le.type().definition().memberScope().useOverload(t1, false, p.infoLog());
		// else try normal lookup
		if (o == null) o = p.currentScope().useOverload(t1, p.infoLog());
		if (o == null) {
			p.input().infoLog().error("no available overloads for operator" + op, t);
			throw new BuildException("no overloads");
		}
		FunctionCall c = o.call(p.infoLog(), t, p.currentScope(), le, re);
		if (c != null) return c;
		throw new BuildException("overload resolution failed");
	}

	@Override
	public int precedence() {
		return prec;
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(FunctionCall.class);
	}
	
}