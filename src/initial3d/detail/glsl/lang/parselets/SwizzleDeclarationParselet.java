package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;
import java.util.ArrayList;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.lang.Struct;
import initial3d.detail.glsl.lang.Swizzle;
import initial3d.detail.glsl.lang.Type;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.names.SwizzleName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.lang.statements.SwizzleDeclaration;
import initial3d.detail.glsl.tokens.Token;

public class SwizzleDeclarationParselet implements PrefixParselet {

	@Override
	public Node parse(Parser p, Token t) throws IOException {
		assert t.isText("__swizzle");
		p.input().infoLog().pushContext("parsing swizzle declaration", t);
		// scopes for expression resolution
		p.pushScope();
		Scope sourcescope = p.currentScope();
		p.popScope();
		p.pushScope();
		Scope targetscope = p.currentScope();
		p.popScope();
		try {
			// type of swizzle
			TypeName ztype = p.parse(TypeName.class, Precedence.DECLARATION);
			// this struct type
			TypeName stype = null;
			try {
				// in-class declaration
				stype = ((Type) p.currentScope().owner()).name();
			} catch (NullPointerException | ClassCastException e) {
				// out-of-class declaration
				stype = p.parse(TypeName.class, Precedence.DECLARATION);
				p.input().expectNextToken("::");
			}
			// swizzle names (multiple names are allowed, but the first is treated as primary for the definition)
			ArrayList<SwizzleName> znames = new ArrayList<>();
			do {
				znames.add(new SwizzleName(p.input().expectNextToken(Token.Type.IDENTIFIER), stype.definition().memberScope(), ztype));
			} while (p.input().consumeNextToken(",") != null);
			// swizzle definition
			Swizzle swiz = new Swizzle(znames.get(0), sourcescope, targetscope);
			for (SwizzleName zname : znames) {
				zname.declare(p.infoLog());
				zname.define(swiz, p.infoLog());
			}
			// 'this' variables for expression resolution
			VariableName sourcename = new VariableName(t.withText("this"), sourcescope, stype);
			sourcename.declare(p.infoLog());
			sourcename.define(new Variable(sourcename), p.infoLog());
			sourcescope.linkVariable(sourcename, p.infoLog());
			VariableName targetname = new VariableName(t.withText("this"), targetscope, ztype);
			targetname.declare(p.infoLog());
			targetname.define(new Variable(targetname), p.infoLog());
			targetscope.linkVariable(targetname, p.infoLog());
			if (p.input().hasNextToken(":")) {
				// swizzle is result of single expression
				p.input().expectNextToken(":");
				p.pushScope(sourcescope);
				Expression sex = null;
				try {
					sex = p.parse(Expression.class, Precedence.STATEMENT);
				} finally {
					p.popScope();
				}
				p.input().expectNextToken(";");
				swiz.targetExpr(sex);
				return new SwizzleDeclaration(znames.get(0), swiz);
			} else if (p.input().hasNextToken("{")) {
				// swizzle specifies members of result type
				p.input().expectNextToken("{");
				while (!p.input().hasNextToken("}")) {
					p.pushScope(targetscope);
					Expression tex = null;
					try {
						tex = p.parse(Expression.class, Precedence.STATEMENT);
					} finally {
						p.popScope();
					}
					p.input().expectNextToken(":");
					p.pushScope(sourcescope);
					Expression sex = null;
					try {
						sex = p.parse(Expression.class, Precedence.STATEMENT);
					} finally {
						p.popScope();
					}
					p.input().expectNextToken(";");
					swiz.targetFieldExpr(tex, sex);
				}
				p.input().expectNextToken("}");
				return new SwizzleDeclaration(znames.get(0), swiz);
			} else {
				p.input().expectNextToken(":", "{");
			}
			return null;
		} finally {
			p.input().infoLog().popContext();
		}
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(SwizzleDeclaration.class);
	}

}
