package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.InfixParselet;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.statements.ExpressionStatement;
import initial3d.detail.glsl.tokens.Token;

public class ExpressionStatementParselet implements InfixParselet {

	@Override
	public Node parse(Parser p, Node l, Token t) throws IOException {
		return new ExpressionStatement((Expression) l);
	}

	@Override
	public int precedence() {
		return Precedence.STATEMENT;
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(ExpressionStatement.class);
	}
	
}