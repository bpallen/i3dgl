package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.expressions.BoolInstance;
import initial3d.detail.glsl.lang.statements.LoopStatement;
import initial3d.detail.glsl.tokens.Token;

public class ForParselet implements PrefixParselet {

	@Override
	public Node parse(Parser p, Token t) throws IOException {
		assert t.isText("for");
		p.input().pushContext("parsing for statement", t);
		try {
			boolean should_unroll = p.input().consumeNextToken("__unroll") != null;
			LoopStatement ls = new LoopStatement(t, p.currentScope());
			ls.shouldUnroll(should_unroll);
			p.pushScope();
			p.currentScope().allowDeclarations();
			// init scope
			try {
				p.input().expectNextToken("(");
				// parse should eat the ;
				ls.init(p.parse(Statement.class, 0));
				p.pushScope();
				p.currentScope().allowDeclarations();
				// loop scope
				try {
					if (p.input().hasNextToken(";")) {
						// no condition -> replace with bool literal true
						BoolInstance x = new BoolInstance(t, p.currentScope(), false);
						x.value(true);
						ls.cond(x, false);
					} else {
						Expression cond = p.parse(Expression.class, Precedence.STATEMENT);
						cond = p.currentScope().convert(p.infoLog(), BuiltinScope.instance.declaredType("bool", null), cond, true, false);
						ls.cond(cond, false);
					}
					p.input().expectNextToken(";");
					if (p.input().hasNextToken(")")) {
						p.input().expectNextToken(")");
					} else {
						ls.incr(p.parse(Expression.class, Precedence.STATEMENT));
						p.input().expectNextToken(")");
					}
					Statement body = p.parseControlBody();
					ls.body(body, p.currentScope());
					ls.build(p.infoLog());
					return ls;
				} finally {
					p.popScope();
				}
			} finally {
				p.popScope();
			}
		} finally {
			p.input().popContext();
		}
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(LoopStatement.class);
	}

}
