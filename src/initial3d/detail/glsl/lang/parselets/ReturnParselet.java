package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.lang.statements.ReturnStatement;
import initial3d.detail.glsl.tokens.Token;

public class ReturnParselet implements PrefixParselet {

	@Override
	public Node parse(Parser p, Token t) throws IOException {
		if (p.input().hasNextToken(";")) {
			// FIXME eat the ;
			return new ReturnStatement(p.infoLog(), t, p.currentScope(), null);
		}
		VariableName rv = p.currentScope().useVariable(t.withText("__return"), p.infoLog());
		Expression x = p.parse(Expression.class, Precedence.STATEMENT);
		if (rv == null) {
			p.input().infoLog().error("unable to determine return type (is this actually a function?)", t);
		} else {
			x = p.currentScope().convert(p.infoLog(), rv.type(), x, false, false);
		}
		ReturnStatement s = new ReturnStatement(p.infoLog(), t, p.currentScope(), x);
		p.input().expectNextToken(";");
		return s;
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(ReturnStatement.class);
	}
	
}