package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.InfixParselet;
import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.Type;
import initial3d.detail.glsl.lang.expressions.MemberAccess;
import initial3d.detail.glsl.lang.expressions.SwizzleAccess;
import initial3d.detail.glsl.lang.names.OverloadSet;
import initial3d.detail.glsl.lang.names.SwizzleName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.tokens.Token;

public class MemberSelectionParselet implements InfixParselet {

	@Override
	public Node parse(Parser p, Node l, Token t) throws IOException {
		assert t.isText(".");
		assert l instanceof Expression;
		Type type = ((Expression) l).type().definition();
		Token t1 = p.input().expectNextToken(Token.Type.IDENTIFIER);
		Name name = type.memberScope().useName(t1, false);
		if (name instanceof VariableName) {
			// member variable
			return new MemberAccess((VariableName) name, (Expression) l);
		} else if (name instanceof SwizzleName) {
			// swizzle
			SwizzleName zname = (SwizzleName) name;
			return new SwizzleAccess(zname.definition(), zname, (Expression) l);
		} else if (name instanceof OverloadSet) {
			// member function (call)
			OverloadSet o = (OverloadSet) name.copy();
			o.thisExpr((Expression) l);
			return o;
		} else {
			p.input().infoLog().error("type " + type.name().plainText() + " has no member '${text}'", t1);
			p.input().infoLog().detail("see type definition", type.name().source());
			throw new BuildException("bad member name");
		}
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(MemberAccess.class, OverloadSet.class);
	}

	@Override
	public int precedence() {
		return Precedence.CALL;
	}
	
}