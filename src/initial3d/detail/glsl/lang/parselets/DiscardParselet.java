package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.statements.DiscardStatement;
import initial3d.detail.glsl.tokens.Token;

public class DiscardParselet implements PrefixParselet {

	@Override
	public Node parse(Parser p, Token t) throws IOException {
		assert t.isText("discard");
		p.input().expectNextToken(";");
		return new DiscardStatement(t, p.currentScope());
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(DiscardStatement.class);
	}

}
