package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.Matrix;
import initial3d.detail.glsl.lang.NameException;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.Struct;
import initial3d.detail.glsl.lang.Vector;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.lang.statements.CompoundStatement;
import initial3d.detail.glsl.tokens.Token;

public class StructParselet implements PrefixParselet {

	@Override
	public Node parse(Parser p, Token t) throws IOException {
		assert t.isText("struct");
		// have to allow keywords for vector types etc
		ScalarTypeName type = new ScalarTypeName(p.input().expectNextToken(Token.Type.IDENTIFIER, Token.Type.KEYWORD), p.currentScope());
		// vector/matrix? (slightly hacky)
		Token tvec = p.input().consumeNextToken("__vector");
		Token tmat = p.input().consumeNextToken("__matrix");
		if (p.input().hasNextToken(";")) {
			type.declare(p.infoLog());
			return type;
		}
		CompoundStatement s = new CompoundStatement(p.input().expectNextToken("{"), p.currentScope());
		p.pushScope();
		p.input().pushContext(String.format("parsing %s definition", tmat != null ? "matrix" : tvec != null ? "vector" : "struct"), t);
		try {
			Struct struct = tmat != null ? new Matrix(type, p.currentScope(), s) :
				tvec != null ? new Vector(type, p.currentScope(), s) :
					new Struct(type, p.currentScope(), s);
			p.currentScope().owner(struct);
			p.currentScope().allowDeclarations();
			type.declare(p.infoLog());
			type.define(struct, p.infoLog());
			while (!p.input().hasNextToken("}")) {
				// TODO restriction to declarations
				s.add(p.parse(Statement.class, 0));
			}
			p.input().expectNextToken("}");
			struct.build(p.infoLog());
		} catch (NameException e) {
			// ?
		} finally {
			p.input().popContext();
			p.popScope();
		}
		return type;
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(ScalarTypeName.class);
	}
	
}