package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.InfixParselet;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.Struct;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.lang.statements.StructDeclaration;
import initial3d.detail.glsl.tokens.Token;

public class StructDeclarationParselet implements InfixParselet {

	@Override
	public Node parse(Parser p, Node l, Token t) throws IOException {
		assert t.isText(";");
		if (((ScalarTypeName) l).defined()) {
			return new StructDeclaration((ScalarTypeName) l, (Struct) ((ScalarTypeName) l).definition());
		} else {
			return new StructDeclaration((ScalarTypeName) l, null);
		}
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(StructDeclaration.class);
	}

	@Override
	public int precedence() {
		return Precedence.STATEMENT;
	}
	
}