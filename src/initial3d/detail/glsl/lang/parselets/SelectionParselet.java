package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.InfixParselet;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.expressions.SelectionExpression;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

public class SelectionParselet implements InfixParselet {

	@Override
	public Node parse(Parser p, Node l, Token t) throws IOException {
		assert t.isText("?");
		Expression cond = (Expression) l;
		cond = p.currentScope().convert(p.infoLog(), BuiltinScope.instance.declaredType("bool", null), cond, true, false);
		Expression tx = p.parse(Expression.class, Precedence.SELECTION - 1);
		p.input().expectNextToken(":");
		Expression fx = p.parse(Expression.class, Precedence.SELECTION - 1);
		// TODO common type?
		if (!tx.type().declEquals(fx.type())) {
			p.input().infoLog().error("mismatched types for selection expression; base types must match exactly", t);
			throw new BuildException("mismatched types for selection");
		}
		TypeName type = (TypeName) tx.type().withoutQualifiers();
		// TODO ref types?
		// glsl doesn't allow ?: to be l-valued, so it doesn't matter that much
		tx = p.currentScope().convert(p.infoLog(), type, tx, false, false);
		fx = p.currentScope().convert(p.infoLog(), type, fx, false, false);
		SelectionExpression sx = new SelectionExpression(t, p.currentScope());
		sx.cond(cond);
		sx.trueBranch(tx);
		sx.falseBranch(fx);
		return sx;
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(SelectionExpression.class);
	}

	@Override
	public int precedence() {
		return Precedence.SELECTION;
	}

}
