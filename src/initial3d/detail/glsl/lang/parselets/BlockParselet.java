package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.InfixParselet;
import initial3d.detail.glsl.lang.NameException;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.Struct;
import initial3d.detail.glsl.lang.names.BlockName;
import initial3d.detail.glsl.lang.names.LayoutQualifier;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.lang.statements.CompoundStatement;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenBuilder;

public class BlockParselet implements InfixParselet {

	@Override
	public Node parse(Parser p, Node l, Token t) throws IOException {
		assert t.isText("{");
		assert l instanceof BlockName;
		BlockName block = (BlockName) l;
		if (!p.currentScope().global()) {
			p.input().infoLog().error("blocks must be declared at global scope", block.source());
		}
		if (block.interfaceQualifier() == null) {
			p.infoLog().error("block must have interface qualifier in/out/uniform", block.source());
			// TODO better block qualifier checking?
		}
		ScalarTypeName type = new ScalarTypeName(new TokenBuilder(block.source()).prependText("__block_").toToken(), p.currentScope());
		CompoundStatement s = new CompoundStatement(t, p.currentScope());
		// push block member scope
		p.pushScope();
		p.input().pushContext("parsing block definition", t);
		try {
			// apply block layout changes; will be picked up by declarations inside the block
			LayoutQualifier ql = (LayoutQualifier) block.dequalify("layout");
			if (ql != null) p.currentScope().layout(block.interfaceQualifier().text()).apply(ql, p.infoLog());
			Struct struct = new Struct(type, p.currentScope(), s);
			struct.block(block);
			p.currentScope().owner(struct);
			p.currentScope().allowDeclarations();
			type.declare(p.infoLog());
			type.define(struct, p.infoLog());
			block.declare(p.infoLog());
			while (!p.input().hasNextToken("}")) {
				// TODO restriction to declarations ('Declaration' interface?)
				s.add(p.parse(Statement.class, 0));
			}
			p.input().expectNextToken("}");
			struct.build(p.infoLog());
		} catch (NameException e) {
			// TODO cause?
		} finally {
			p.input().popContext();
			p.popScope();
		}
		return block;
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(BlockName.class);
	}

	@Override
	public int precedence() {
		return Precedence.DECLARATION;
	}
	
}