package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.tokens.Token;

public class ParenthesesParselet implements PrefixParselet {

	@Override
	public Node parse(Parser p, Token t) throws IOException {
		assert t.isText("(");
		Expression e = p.parse(Expression.class, 0);
		p.input().expectNextToken(")");
		return e;
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(Expression.class);
	}
	
}