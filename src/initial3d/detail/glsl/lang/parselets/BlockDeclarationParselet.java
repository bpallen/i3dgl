package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.InfixParselet;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.Variable;
import initial3d.detail.glsl.lang.names.BlockName;
import initial3d.detail.glsl.lang.names.Qualifier;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.lang.names.VariableName;
import initial3d.detail.glsl.lang.statements.VariableDeclaration;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenBuilder;

public class BlockDeclarationParselet implements InfixParselet {

	@Override
	public Node parse(Parser p, Node l, Token t) throws IOException {
		assert l instanceof BlockName;
		BlockName block = (BlockName) l;
		VariableName vname = null;
		TypeName type = block.type().copy();
		if (t.type() == Token.Type.IDENTIFIER) {
			// specified instance name
			// TODO block arrays
			vname = new VariableName(t, p.currentScope(), type);
			p.input().expectNextToken(";");
		} else {
			if (!t.isText(";")) p.input().expectationFailure(t);
			// generate instance name and link scope to instance
			Token s = block.source().withText("__blockinst_" + block.source().text() + p.currentScope().genID());
			vname = new VariableName(s, p.currentScope(), type);
			p.currentScope().linkVariable(vname, p.infoLog());
		}
		vname.qualify(block.interfaceQualifier());
		vname.declare(p.infoLog());
		Variable v = new Variable(vname);
		// set layout (any layout qualifier on block has already been applied to member scope) 
		v.layout(type.definition().memberScope().layout(vname.interfaceQualifier().text()).clone());
		vname.define(v, p.infoLog());
		return new VariableDeclaration(vname);
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(VariableDeclaration.class);
	}

	@Override
	public int precedence() {
		return Precedence.STATEMENT;
	}
	
}