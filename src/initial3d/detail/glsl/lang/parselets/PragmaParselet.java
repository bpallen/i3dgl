package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;
import java.util.List;

import initial3d.detail.glsl.PragmaHandler;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.statements.EmptyStatement;
import initial3d.detail.glsl.tokens.IteratorTokenInput;
import initial3d.detail.glsl.tokens.Token;

public class PragmaParselet implements PrefixParselet {

	@Override
	public Node parse(Parser p, Token t) throws IOException {
		assert t.isText("__pragma");
		p.input().pushContext("parsing __pragma()", t);
		try {
			List<Token> ts = p.captureTokens("(", ")");
			if (ts.size() > 2) {
				Token tp = ts.get(1);
				PragmaHandler ph = p.pragma(tp.text());
				if (ph != null) {
					// strip parens, forward to handler
					ts.remove(0);
					ts.remove(ts.size() - 1);
					ph.pragma(new IteratorTokenInput(p.input().infoLog(), ts));
				} else {
					p.input().infoLog().warning("unkown pragma", tp);
				}
			}
		} finally {
			p.input().popContext();
		}
		return new EmptyStatement(t, p.currentScope());
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		// TODO update if we make a pragma that produces something
		return Parser.makeNodeClasses(EmptyStatement.class);
	}

}
