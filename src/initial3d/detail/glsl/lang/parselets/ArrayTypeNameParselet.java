package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.Array;
import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.EvalException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.InfixParselet;
import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.VariableInstanceMap;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.IntInstance;
import initial3d.detail.glsl.lang.expressions.UIntInstance;
import initial3d.detail.glsl.lang.names.ArrayTypeName;
import initial3d.detail.glsl.lang.names.ScalarTypeName;
import initial3d.detail.glsl.lang.names.TypeName;
import initial3d.detail.glsl.tokens.Token;

public class ArrayTypeNameParselet implements InfixParselet {

	@Override
	public Node parse(Parser p, Node l, Token t) throws IOException {
		assert t.isText("[");
		if (!(l instanceof TypeName)) throw new IllegalArgumentException("lhs must be TypeName");
		if (!((Name) l).declared()) throw new IllegalArgumentException("inner type must be declared");
		int size = -1;
		if (!p.input().hasNextToken("]")) {
			// eval array size
			Expression expr = p.parse(Expression.class, Precedence.STATEMENT);
			try {
				Instance x = expr.eval(false, p.infoLog());
				if (x instanceof IntInstance) {
					size = ((IntInstance) x).value();
				} else if (x instanceof UIntInstance) {
					size = ((UIntInstance) x).value();
				} else {
					p.input().infoLog().error("array size expression must have type int or uint", t);
				}
			} catch (EvalException e) {
				p.input().infoLog().error("array size expression must be constant", t);
			}
		}
		p.input().expectNextToken("]");
		if (l instanceof ArrayTypeName) {
			p.input().infoLog().error("arrays can only have 1 dimension", t);
			return l;
		}
		if (l instanceof ScalarTypeName && ((Name) l).declEquals(BuiltinScope.instance.declaredType("void", null))) {
			p.input().infoLog().error("array of void is not allowed", t);
			return l;
		}
		ScalarTypeName inner = ((ScalarTypeName) l).copy();
		ArrayTypeName type = new ArrayTypeName(t, inner, inner.declScope());
		// TODO what about arrays of qualified types?
		type.qualifyAll(inner.dequalifyAll());
		type.size(size);
		if (!type.declared()) {
			// declare and define array type
			type.declare(p.infoLog());
			type.define(new Array(type, p.infoLog()), p.infoLog());
		}
		return type;
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(ArrayTypeName.class);
	}

	@Override
	public int precedence() {
		return Precedence.CALL;
	}
	
}