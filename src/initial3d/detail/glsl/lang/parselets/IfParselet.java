package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.statements.DiscardStatement;
import initial3d.detail.glsl.lang.statements.IfStatement;
import initial3d.detail.glsl.tokens.Token;

public class IfParselet implements PrefixParselet {
	
	// TODO variable declaration in condition?
	
	@Override
	public Node parse(Parser p, Token t) throws IOException {
		assert t.isText("if");
		IfStatement s = new IfStatement(t, p.currentScope());
		boolean has_else = false;
		do {
			p.pushScope();
			p.currentScope().allowDeclarations();
			// branch scope
			// TODO if we did allow declarations in conditions, they need to be accessible from all branches
			try {
				p.input().expectNextToken("(");
				Expression cond = p.parse(Expression.class, Precedence.STATEMENT);
				cond = p.currentScope().convert(p.infoLog(), BuiltinScope.instance.declaredType("bool", null), cond, true, false);
				p.input().expectNextToken(")");
				Statement body = p.parseControlBody();
				s.addIfBranch(cond, body, p.currentScope());
				has_else = p.input().consumeNextToken("else") != null;
			} finally {
				p.popScope();
			}
		} while (has_else && p.input().consumeNextToken("if") != null);
		if (has_else) {
			p.pushScope();
			p.currentScope().allowDeclarations();
			// branch scope
			try {
				Statement body = p.parseControlBody();
				s.elseBranch(body, p.currentScope());
			} finally {
				p.popScope();
			}
		}
		return s.maybeReplace(p.infoLog());
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		// TODO does this need to list discard etc?
		return Parser.makeNodeClasses(IfStatement.class, DiscardStatement.class);
	}

}
