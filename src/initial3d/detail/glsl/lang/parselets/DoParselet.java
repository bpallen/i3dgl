package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;

import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.Statement;
import initial3d.detail.glsl.lang.statements.LoopStatement;
import initial3d.detail.glsl.tokens.Token;

public class DoParselet implements PrefixParselet {

	@Override
	public Node parse(Parser p, Token t) throws IOException {
		assert t.isText("do");
		p.input().pushContext("parsing do statement", t);
		try {
			LoopStatement ls = new LoopStatement(t, p.currentScope());
			p.pushScope();
			p.currentScope().allowDeclarations();
			// loop scope
			try {
				Statement body = p.parseControlBody();
				p.input().expectNextToken("while");
				p.input().expectNextToken("(");
				Expression cond = p.parse(Expression.class, Precedence.STATEMENT);
				cond = p.currentScope().convert(p.infoLog(), BuiltinScope.instance.declaredType("bool", null), cond, true, false);
				p.input().expectNextToken(")");
				p.input().expectNextToken(";");
				ls.cond(cond, true);
				ls.body(body, p.currentScope());
				ls.build(p.infoLog());
				return ls;
			} finally {
				p.popScope();
			}
		} finally {
			p.input().popContext();
		}
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(LoopStatement.class);
	}

}
