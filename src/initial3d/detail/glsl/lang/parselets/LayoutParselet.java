package initial3d.detail.glsl.lang.parselets;

import java.io.IOException;
import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.lang.Expression;
import initial3d.detail.glsl.lang.Name;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.NodeExpectationException;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Precedence;
import initial3d.detail.glsl.lang.PrefixParselet;
import initial3d.detail.glsl.lang.expressions.Instance;
import initial3d.detail.glsl.lang.expressions.VoidInstance;
import initial3d.detail.glsl.lang.names.LayoutQualifier;
import initial3d.detail.glsl.lang.names.Qualifier;
import initial3d.detail.glsl.lang.statements.LayoutStatement;
import initial3d.detail.glsl.tokens.Token;

public class LayoutParselet implements PrefixParselet {

	@Override
	public Node parse(Parser p, Token t) throws IOException {
		assert t.isText("layout");
		p.input().pushContext("parsing layout qualifier", t);
		try {
			LayoutQualifier q = new LayoutQualifier(t, p.currentScope());
			p.input().expectNextToken("(");
			while (!p.input().hasNextToken(")")) {
				// these are supposed to just be identifiers, but row_major is apparently a keyword
				Token t0 = p.input().expectNextToken(Token.Type.IDENTIFIER, Token.Type.KEYWORD);
				Instance x = null;
				if (p.input().consumeNextToken("=") != null) {
					Expression x0 = p.parse(Expression.class, Precedence.STATEMENT);
					try {
						x = x0.eval(false, p.infoLog());
					} catch (BuildException e) {
						p.input().infoLog().error("failed to evaluate layout parameter expression", x0.source());
					}
				}
				p.input().consumeNextToken(",");
				// default to magic void instance if unspecified to prevent needless null problems
				if (x == null) x = new VoidInstance(t0, p.currentScope(), false);
				q.put(t0.text(), t0, x);
			}
			p.input().expectNextToken(")");
			Node rhs = p.parse(Precedence.QUALIFICATION - 1);
			if (rhs instanceof Qualifier) {
				// layout statement
				p.input().expectNextToken(";");
				Token t1 = rhs.source();
				if (t1.isText("in") || t1.isText("out") || t1.isText("uniform")) {
					// apply layout changes now
					p.currentScope().layout(t1.text()).apply(q, p.infoLog());
					return new LayoutStatement(q, (Qualifier) rhs);
				} else {
					p.input().infoLog().error("layout statement requires interface qualifier in/out/uniform", t1);
					throw new BuildException("bad layout interface");
				}
			} else if (rhs instanceof Name) {
				// block or variable declaration (probably) => rhs could be BlockName or TypeName
				// layout changes applied during declaration parsing (not here)
				Name name = ((Name) rhs).copy();
				name.qualify(q);
				return name;
			} else {
				// TODO parser multi-class expect?
				p.input().infoLog().error("expected Name or Qualifier", rhs.source());
				throw new BuildException("expected Name or Qualifier");
			}
		} finally {
			p.input().popContext();
		}
	}

	@Override
	public Class<? extends Node>[] nodeClasses() {
		return Parser.makeNodeClasses(Name.class, LayoutStatement.class);
	}

}
