package initial3d.detail.glsl.lang;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import initial3d.detail.glsl.intermediate.MacroOp;

@Retention(RetentionPolicy.RUNTIME)
public @interface ImplementBuiltin {
	String name();
	String rettype();
	String[] paramtypes();
	String[] qualifiers() default {};
	MacroOp.Code opcode() default MacroOp.Code.CALL;
}
