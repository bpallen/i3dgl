package initial3d.detail.glsl.lang;

import initial3d.detail.glsl.lang.names.VariableName;

@Deprecated
public class ArrayVariable extends Variable {

	// TODO is this class needed?
	
	private int size = -1;
	
	public ArrayVariable(VariableName name_, int size_) {
		super(name_);
		size = size_;
	}

	public int size() {
		return size;
	}
	
	public void size(int s) {
		size = s;
	}
	
	public void ensureSize(int s) {
		size = Math.max(size, s);
	}
}
