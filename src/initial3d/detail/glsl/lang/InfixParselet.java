package initial3d.detail.glsl.lang;

import java.io.IOException;

import initial3d.detail.glsl.tokens.Token;

public interface InfixParselet {
	public Node parse(Parser p, Node l, Token t) throws IOException;
	public Class<? extends Node>[] nodeClasses();
	public int precedence();
}