package initial3d.detail.glsl.asmjava;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import initial3d.GLEnum;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.bytecode.Flag;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Environment;
import initial3d.detail.glsl.intermediate.ProgramBuilder;

public class JavaProgramBuilder extends ProgramBuilder {

	// reserved space at start of uniform storage
	public static final int UNIFORM_BASE_OFFSET = 128;
	
	private ClassBuilder cb;
	
	public JavaProgramBuilder(InfoLog infolog_) {
		super(infolog_);
		cb = new ClassBuilder();
		cb.declareFlag(Flag.PUBLIC);
		cb.declareNameUnique("initial3d.detail.gen.ShaderProgramAccessor");
		cb.declareSuperclass(Object.class);
		assembleDefaultCtor();
	}

	@Override
	protected Environment createEnv(GLEnum stype) {
		if (stype == GLEnum.GL_VERTEX_SHADER) return new JavaVertexEnvironment(infoLog(), cb);
		if (stype == GLEnum.GL_FRAGMENT_SHADER) return new JavaFragmentEnvironment(infoLog(), cb);
		throw new IllegalArgumentException("bad shader type");
	}
	
	@Override
	public void build() {
		super.build();
		cb.build();
		
		// testing
		try {
			FileOutputStream fs = new FileOutputStream("c:/users/ben/desktop/shadertest.class");
			fs.write(cb.builtClassBytes());
			fs.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void assembleDefaultCtor() {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("<init>");
		mb.declareType(false, void.class);
		mb.aload(0);
		try {
			mb.invokeSpecial(Object.class, "<init>", void.class);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		mb.op(Op.RETURN);
	}
	
	public Class<?> programClass() {
		return cb.builtClass();
	}
	
}
