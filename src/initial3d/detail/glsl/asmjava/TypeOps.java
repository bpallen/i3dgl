package initial3d.detail.glsl.asmjava;

import static initial3d.GLEnum.GL_FLOAT;
import static initial3d.GLEnum.GL_INT;

import java.util.HashMap;

import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.gl.DataFormat;
import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.expressions.Instance;

public interface TypeOps {

	public static final HashMap<String, TypeOps> typeops = new HashMap<>();
	public static final BoolOps boolops = new BoolOps();
	public static final IntOps intops = new IntOps();
	public static final FloatOps floatops = new FloatOps();
	
	public static TypeOps typeops(String type) {
		TypeOps t = typeops.get(type);
		if (t != null) return t;
		throw new BuildException("java backend does not support type " + type);
	}
	
	public static TypeOps typeops(BuiltinType type) {
		return typeops(type.name().plainText());
	}
	
	public void load(MethodBuilder mb, int x);
	
	public void store(MethodBuilder mb, int x);
	
	public void unsafeLoad(MethodBuilder mb, boolean sanitize);
	
	public void unsafeStore(MethodBuilder mb);
	
	public void constant(MethodBuilder mb, Instance x);
	
	public static class BoolOps implements TypeOps {

		static {
			typeops.put("bool", new BoolOps());
		}
		
		@Override
		public void load(MethodBuilder mb, int x) {
			mb.iload(x);
		}

		@Override
		public void store(MethodBuilder mb, int x) {
			mb.istore(x);
		}

		@Override
		public void constant(MethodBuilder mb, Instance x) {
			mb.constant(((Boolean) x.javaEquivalent()) ? 1 : 0);
		}

		@Override
		public void unsafeLoad(MethodBuilder mb, boolean sanitize) {
			// TODO determine correct load size properly
			try {
				DataFormat.buildGetRaw(mb, 4);
				// ensure any value != 0 becomes 1 if needed
				if (sanitize) mb.ine();
			} catch (Exception e) {
				throw new AssertionError(e);
			}
		}

		@Override
		public void unsafeStore(MethodBuilder mb) {
			// TODO determine correct store size properly
			try {
				DataFormat.buildPutRaw(mb, 4);
			} catch (Exception e) {
				throw new AssertionError(e);
			}
		}
		
	}
	
	public static class IntOps implements TypeOps {

		static {
			typeops.put("int", new IntOps());
			typeops.put("uint", new IntOps());
		}
		
		@Override
		public void load(MethodBuilder mb, int x) {
			mb.iload(x);
		}

		@Override
		public void store(MethodBuilder mb, int x) {
			mb.istore(x);
		}

		@Override
		public void constant(MethodBuilder mb, Instance x) {
			mb.constant((Integer) x.javaEquivalent());
		}

		@Override
		public void unsafeLoad(MethodBuilder mb, boolean sanitize) {
			try {
				DataFormat.buildGetRaw(mb, 4);
			} catch (Exception e) {
				throw new AssertionError(e);
			}
		}

		@Override
		public void unsafeStore(MethodBuilder mb) {
			try {
				DataFormat.buildPutRaw(mb, 4);
			} catch (Exception e) {
				throw new AssertionError(e);
			}
		}
		
	}
	
	public static class FloatOps implements TypeOps {

		static {
			typeops.put("float", new FloatOps());
		}
		
		@Override
		public void load(MethodBuilder mb, int x) {
			mb.fload(x);
		}

		@Override
		public void store(MethodBuilder mb, int x) {
			mb.fstore(x);
		}

		@Override
		public void constant(MethodBuilder mb, Instance x) {
			mb.constant((Float) x.javaEquivalent());
		}

		@Override
		public void unsafeLoad(MethodBuilder mb, boolean sanitize) {
			try {
				DataFormat.buildGetFloat(mb, GL_FLOAT, false);
			} catch (Exception e) {
				throw new AssertionError(e);
			}
		}

		@Override
		public void unsafeStore(MethodBuilder mb) {
			try {
				DataFormat.buildPutFloat(mb, GL_FLOAT, false);
			} catch (Exception e) {
				throw new AssertionError(e);
			}			
		}
		
	}
	
}
