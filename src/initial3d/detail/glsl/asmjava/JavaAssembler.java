package initial3d.detail.glsl.asmjava;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;

import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;
import initial3d.detail.bytecode.Constant.MethodRef;
import initial3d.detail.glsl.BuildException;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.Assembler;
import initial3d.detail.glsl.intermediate.Environment;
import initial3d.detail.glsl.intermediate.LocationAssembler;
import initial3d.detail.glsl.intermediate.MacroOp;
import initial3d.detail.glsl.intermediate.ShaderInterface;
import initial3d.detail.glsl.intermediate.locations.ArrayLocation;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.lang.BuiltinType;
import initial3d.detail.glsl.lang.expressions.Instance;

public abstract class JavaAssembler implements Assembler {

	protected interface OpHandler {
		public void assemble(MethodBuilder mb, MacroOp mop);
	}
	
	protected class LocalLocationAssembler implements LocationAssembler {

		public final BuiltinType type;
		public final int regindex;
		
		public LocalLocationAssembler(BuiltinType type_, int regindex_) {
			type = type_;
			regindex = regindex_;
		}
		
		@Override
		public void arrayOffset(ArrayLocation a, LocationAssembler l) {
			throw new IllegalStateException("java local variable location cannot be offset");
		}

		@Override
		public void ifaceOffset(LocationAssembler l) {
			throw new IllegalStateException("java local variable location cannot be offset");
		}

		@Override
		public void load(int group) {
			TypeOps.typeops(type).load(current_mb, jvar_base + jvarLocalOffset(current_mb, regindex, group));
		}

		@Override
		public void store(int group) {
			TypeOps.typeops(type).store(current_mb, jvar_base + jvarLocalOffset(current_mb, regindex, group));
		}
		
	}
	
	protected static enum UnsafeStorage {
		CONSTANT, LOCAL, UNIFORM, INPUT, OUTPUT;
	}
	
	protected static class UnsafeOffset {
		public final int stride;
		public final LocationAssembler index;
		
		public UnsafeOffset(int stride_, LocationAssembler index_) {
			stride = stride_;
			index = index_;
		}
	}
	
	protected class UnsafeLocationAssembler implements LocationAssembler {

		public final BuiltinType type;
		public final UnsafeStorage storage;
		public final ShaderInterface iface;
		public final int iface_index;
		public final int address;
		public final EnumSet<PrimitiveLocation.Flags> flags;
		public LocationAssembler iface_offset;
		public ArrayList<UnsafeOffset> offsets = new ArrayList<>();

		public UnsafeLocationAssembler(BuiltinType type_, UnsafeStorage storage_, int address_, EnumSet<PrimitiveLocation.Flags> flags_) {
			type = type_;
			storage = storage_;
			iface = null;
			iface_index = 0;
			address = address_;
			flags = flags_;
		}

		public UnsafeLocationAssembler(
			BuiltinType type_, UnsafeStorage storage_, ShaderInterface iface_, int iface_index_, int address_,
			EnumSet<PrimitiveLocation.Flags> flags_
		) {
			type = type_;
			storage = storage_;
			iface = iface_;
			iface_index = iface_index_;
			address = address_;
			flags = flags_;
		}

		@Override
		public void arrayOffset(ArrayLocation a, LocationAssembler l) {
			offsets.add(new UnsafeOffset(a.stride(), l));
		}

		@Override
		public void ifaceOffset(LocationAssembler l) {
			if (iface_offset != null) throw new IllegalStateException("interface offset already applied");
			iface_offset = l;
		}

		public void loadOffset(int group) {
			// offset of initial primitive
			current_mb.constant(address + storageBaseOffset(storage, iface_index));
			// apply any array offsets
			for (UnsafeOffset off : offsets) {
				off.index.load(group);
				current_mb.constant(off.stride);
				current_mb.op(Op.IMUL);
				current_mb.op(Op.IADD);
			}
		}
		
		@Override
		public void load(int group) {
			loadOffset(group);
			assembleUnsafeLoad(current_mb, storage, iface_index, iface_offset, type, flags, group);
		}

		@Override
		public void store(int group) {
			// need to move value above address; using local var is easier
			current_mb.pushScope();
			try {
				int jv = current_mb.scope().alloc(type.javaEquivalent(), "storeval");
				TypeOps.typeops(type).store(current_mb, jv);
				loadOffset(group);
				assembleUnsafeStore(current_mb, storage, iface_index, iface_offset, type, flags, group);
			} finally {
				current_mb.popScope();
			}
		}
		
	}
	
	protected class ConstantLocationAssembler implements LocationAssembler {

		public final BuiltinType type;
		public final Instance value;
		
		public ConstantLocationAssembler(BuiltinType type_, Instance value_) {
			type = type_;
			value = value_;
		}
		
		@Override
		public void arrayOffset(ArrayLocation a, LocationAssembler l) {
			throw new IllegalStateException("java constant location cannot be offset");
		}

		@Override
		public void ifaceOffset(LocationAssembler l) {
			throw new IllegalStateException("java constant location cannot be offset");
		}

		@Override
		public void load(int group) {
			// groups are ignored for constants
			TypeOps.typeops(type).constant(current_mb, value);
		}

		@Override
		public void store(int group) {
			throw new IllegalStateException("java constant location cannot be written to");
		}
		
	}
	
	protected class SpecialLocationAssembler implements LocationAssembler {

		public final BuiltinType type;
		public final String linkname;
		public final int primindex;
		
		public SpecialLocationAssembler(BuiltinType type_, String linkname_, int primindex_) {
			type = type_;
			linkname = linkname_;
			primindex = primindex_;
		}
		
		@Override
		public void arrayOffset(ArrayLocation a, LocationAssembler l) {
			throw new IllegalStateException("java special location cannot be offset");
		}

		@Override
		public void ifaceOffset(LocationAssembler l) {
			throw new IllegalStateException("java special location cannot be offset");
		}

		private String jvar(int group) {
			String[] jvars = specials.get(linkname);
			if (jvars == null) throw new BuildException(String.format("special variable %s not found in shader %s", linkname, env.shaderType()));
			return jvars[jvarSpecialIndex(linkname, primindex, group)];
		}
		
		private int unsafeOffset(int group) {
			int[] offsets = special_offsets.get(linkname);
			if (offsets == null) return -1;
			return offsets[jvarSpecialIndex(linkname, primindex, group)];
		}
		
		@Override
		public void load(int group) {
			int offset = unsafeOffset(group);
			if (offset < 0) {
				TypeOps.typeops(type).load(current_mb, current_mb.scope().get(jvar(group)));
			} else {
				// treat var as pointer
				current_mb.lload(jvar(group));
				current_mb.constant((long) offset);
				current_mb.op(Op.LADD);
				TypeOps.typeops(type).unsafeLoad(current_mb, false);
			}
		}

		@Override
		public void store(int group) {
			int offset = unsafeOffset(group);
			if (offset < 0) {
				TypeOps.typeops(type).store(current_mb, current_mb.scope().get(jvar(group)));
			} else {
				current_mb.pushScope();
				try {
					int jv = current_mb.scope().alloc(type.javaEquivalent(), "storeval");
					TypeOps.typeops(type).store(current_mb, jv);
					// treat var as pointer
					current_mb.lload(jvar(group));
					current_mb.constant((long) offset);
					current_mb.op(Op.LADD);
					TypeOps.typeops(type).load(current_mb, jv);
					TypeOps.typeops(type).unsafeStore(current_mb);
				} finally {
					current_mb.popScope();
				}
			}
		}
		
	}
	
	private final Environment env;
	
	// execution groups
	private final int groupcount;
	
	// special locations
	private HashMap<String, String[]> specials = new HashMap<>();
	private HashMap<String, int[]> special_offsets = new HashMap<>();
	
	// ops
	private HashMap<MacroOp.Code, OpHandler> opimpl = new HashMap<>();
	
	// method assembly state
	private MethodBuilder current_mb;
	private int jvar_base;
	
	public JavaAssembler(Environment env_, int groupcount_) {
		env = env_;
		groupcount = groupcount_;
		if (groupcount < 1) throw new IllegalArgumentException("bad group count");
		// ops
		addOp(MacroOp.Code.NOP, (MethodBuilder mb, MacroOp mop) -> {
			// does nothing
		});
		addOp(MacroOp.Code.JMP, (MethodBuilder mb, MacroOp mop) -> {
			mb.branch(Op.GOTO, branchTarget(mop));
		});
		addOp(MacroOp.Code.JEZ, (MethodBuilder mb, MacroOp mop) -> {
			mb.branch(Op.IFEQ, branchTarget(mop));
		});
		addOp(MacroOp.Code.JNZ, (MethodBuilder mb, MacroOp mop) -> {
			mb.branch(Op.IFNE, branchTarget(mop));
		});
		addOp(MacroOp.Code.CALL, (MethodBuilder mb, MacroOp mop) -> {
			MacroOp.Call c = (MacroOp.Call) mop;
			Method m = c.func.javaEquivalent();
			try {
				// TODO why is this not in method builder?
				mb.invokeStatic(m.getDeclaringClass(), m.getName(), m.getReturnType(), m.getParameterTypes());
			} catch (Exception e) {
				throw new AssertionError(e);
			}
		});
		addOp(MacroOp.Code.MOV, (MethodBuilder mb, MacroOp mop) -> {
			// src arg on stack
			// dst arg will be popped off stack
			// => nothing required here
		});
		// bool
		addOp(MacroOp.Code.BCMPEQ, (MethodBuilder mb, MacroOp mop) -> mb.ieq());
		addOp(MacroOp.Code.BCMPNE, (MethodBuilder mb, MacroOp mop) -> mb.ine());
		addOp(MacroOp.Code.BNOT, (MethodBuilder mb, MacroOp mop) -> { mb.constant(-1); mb.op(Op.IXOR); });
		// TODO true == 1 safety?
		addOpDirect(MacroOp.Code.BAND, Op.IAND);
		addOpDirect(MacroOp.Code.BOR, Op.IOR);
		addOpDirect(MacroOp.Code.BXOR, Op.IXOR);
		// int
		addOpDirect(MacroOp.Code.IADD, Op.IADD);
		addOpDirect(MacroOp.Code.ISUB, Op.ISUB);
		addOpDirect(MacroOp.Code.IMUL, Op.IMUL);
		addOpDirect(MacroOp.Code.IDIV, Op.IDIV);
		addOpDirect(MacroOp.Code.IREM, Op.IREM);
		addOpDirect(MacroOp.Code.INEG, Op.INEG);
		addOp(MacroOp.Code.INOT, (MethodBuilder mb, MacroOp mop) -> { mb.constant(-1); mb.op(Op.IXOR); });
		addOpDirect(MacroOp.Code.ISL, Op.ISHL);
		addOpDirect(MacroOp.Code.ISR, Op.ISHR);
		addOpDirect(MacroOp.Code.IAND, Op.IAND);
		addOpDirect(MacroOp.Code.IOR, Op.IOR);
		addOpDirect(MacroOp.Code.IXOR, Op.IXOR);
		addOp(MacroOp.Code.ICMPEQ, (MethodBuilder mb, MacroOp mop) -> mb.ieq());
		addOp(MacroOp.Code.ICMPNE, (MethodBuilder mb, MacroOp mop) -> mb.ine());
		addOp(MacroOp.Code.ICMPLT, (MethodBuilder mb, MacroOp mop) -> mb.ilt());
		addOp(MacroOp.Code.ICMPLE, (MethodBuilder mb, MacroOp mop) -> mb.ile());
		addOp(MacroOp.Code.ICMPGT, (MethodBuilder mb, MacroOp mop) -> mb.igt());
		addOp(MacroOp.Code.ICMPGE, (MethodBuilder mb, MacroOp mop) -> mb.ige());
		addOp(MacroOp.Code.IMIN, (MethodBuilder mb, MacroOp mop) -> mb.imin());
		addOp(MacroOp.Code.IMAX, (MethodBuilder mb, MacroOp mop) -> mb.imax());
		addOp(MacroOp.Code.IABS, (MethodBuilder mb, MacroOp mop) -> mb.imath("abs", 1));
		// uint
		addOpDirect(MacroOp.Code.UADD, Op.IADD);
		addOpDirect(MacroOp.Code.USUB, Op.ISUB);
		addOpDirect(MacroOp.Code.UMUL, Op.IMUL);
		addOp(MacroOp.Code.UDIV, (MethodBuilder mb, MacroOp mop) -> mb.udiv());
		addOp(MacroOp.Code.UREM, (MethodBuilder mb, MacroOp mop) -> mb.urem());
		addOpDirect(MacroOp.Code.UNEG, Op.INEG);
		addOp(MacroOp.Code.UNOT, (MethodBuilder mb, MacroOp mop) -> { mb.constant(-1); mb.op(Op.IXOR); });
		addOpDirect(MacroOp.Code.USL, Op.ISHL);
		addOpDirect(MacroOp.Code.USR, Op.IUSHR);
		addOpDirect(MacroOp.Code.UAND, Op.IAND);
		addOpDirect(MacroOp.Code.UOR, Op.IOR);
		addOpDirect(MacroOp.Code.UXOR, Op.IXOR);
		addOp(MacroOp.Code.UCMPEQ, (MethodBuilder mb, MacroOp mop) -> mb.ieq());
		addOp(MacroOp.Code.UCMPNE, (MethodBuilder mb, MacroOp mop) -> mb.ine());
		// FIXME unsigned compare (set opcode in builtinfunctions when done)
		//addOp(MacroOp.Code.UCMPLT, (MethodBuilder mb, MacroOp mop) -> mb.ilt());
		//addOp(MacroOp.Code.UCMPLE, (MethodBuilder mb, MacroOp mop) -> mb.ile());
		//addOp(MacroOp.Code.UCMPGT, (MethodBuilder mb, MacroOp mop) -> mb.igt());
		//addOp(MacroOp.Code.UCMPGE, (MethodBuilder mb, MacroOp mop) -> mb.ige());
		addOp(MacroOp.Code.UMIN, (MethodBuilder mb, MacroOp mop) -> mb.imin());
		addOp(MacroOp.Code.UMAX, (MethodBuilder mb, MacroOp mop) -> mb.imax());
		addOp(MacroOp.Code.UABS, (MethodBuilder mb, MacroOp mop) -> {/*like mov*/});
		// float
		addOpDirect(MacroOp.Code.FADD, Op.FADD);
		addOpDirect(MacroOp.Code.FSUB, Op.FSUB);
		addOpDirect(MacroOp.Code.FMUL, Op.FMUL);
		addOpDirect(MacroOp.Code.FDIV, Op.FDIV);
		addOpDirect(MacroOp.Code.FREM, Op.FREM);
		addOpDirect(MacroOp.Code.FNEG, Op.FNEG);
		addOp(MacroOp.Code.FCMPEQ, (MethodBuilder mb, MacroOp mop) -> mb.feq());
		addOp(MacroOp.Code.FCMPNE, (MethodBuilder mb, MacroOp mop) -> mb.fne());
		addOp(MacroOp.Code.FCMPLT, (MethodBuilder mb, MacroOp mop) -> mb.flt());
		addOp(MacroOp.Code.FCMPLE, (MethodBuilder mb, MacroOp mop) -> mb.fle());
		addOp(MacroOp.Code.FCMPGT, (MethodBuilder mb, MacroOp mop) -> mb.fgt());
		addOp(MacroOp.Code.FCMPGE, (MethodBuilder mb, MacroOp mop) -> mb.fge());
		addOp(MacroOp.Code.FMIN, (MethodBuilder mb, MacroOp mop) -> mb.fmin());
		addOp(MacroOp.Code.FMAX, (MethodBuilder mb, MacroOp mop) -> mb.fmax());
		addOp(MacroOp.Code.FABS, (MethodBuilder mb, MacroOp mop) -> mb.fmath("abs", 1));
		// conversion
		addOpDirect(MacroOp.Code.I2F, Op.I2F);
		addOpDirect(MacroOp.Code.F2I, Op.F2I);
		addOp(MacroOp.Code.U2F, (MethodBuilder mb, MacroOp mop) -> mb.u2f());
		addOp(MacroOp.Code.F2U, (MethodBuilder mb, MacroOp mop) -> mb.f2u());
		
	}
	
	private String branchTarget(MacroOp mop) {
		return "__" + ((MacroOp.Branch) mop).target.toString();
	}
	
	public InfoLog infoLog() {
		return env.infoLog();
	}
	
	@Override
	public Environment environment() {
		return env;
	}
	
	@Override
	public LocationAssembler localLoc(BuiltinType type, int regindex, boolean random_access) {
		if (random_access) {
			// FIXME need proper fixed-size primitives for addressing to work (theyre all 4 atm)
			return new UnsafeLocationAssembler(type, UnsafeStorage.LOCAL, regindex * 4, EnumSet.noneOf(PrimitiveLocation.Flags.class));
		} else {
			return new LocalLocationAssembler(type, regindex);
		}
	}

	@Override
	public LocationAssembler ifaceLoc(BuiltinType type, ShaderInterface iface, int ifaceindex, int address, EnumSet<PrimitiveLocation.Flags> flags) {
		if (address < 0) throw new IllegalArgumentException("bad address");
		if (env.uniforms().iface() == iface) {
			return new UnsafeLocationAssembler(type, UnsafeStorage.UNIFORM, iface, ifaceindex, address, flags);
		} else if (env.inputs().iface() == iface) {
			return new UnsafeLocationAssembler(type, UnsafeStorage.INPUT, iface, ifaceindex, address, flags);
		} else if (env.outputs().iface() == iface) {
			return new UnsafeLocationAssembler(type, UnsafeStorage.OUTPUT, iface, ifaceindex, address, flags);
		} else {
			throw new IllegalArgumentException("bad interface");
		}
	}

	@Override
	public LocationAssembler specialLoc(BuiltinType type, String linkname, int primindex) {
		return new SpecialLocationAssembler(type, linkname, primindex);
	}

	@Override
	public LocationAssembler constantLoc(BuiltinType type, Instance value) {
		return new ConstantLocationAssembler(type, value);
	}

	/**
	 * subclasses: use this to create the special locations for texture reads.
	 * provided offset should probably be in reserved space at start of local storage.
	 * 
	 * @param localoffset offset in local storage for <code>16*groupcount</code> bytes of space
	 */
	protected void implementTextureSpecials(int localoffset) {
		String[] ptempn = new String[4 * groupcount];
		int[] offsets = new int[4 * groupcount];
		for (int i = 0; i < ptempn.length; i++) {
			ptempn[i] = "ptemp";
			// texture result vars have all components for one group packed together
			int group = i % groupcount;
			int prim = i / groupcount;
			offsets[i] = localoffset + 16 * group + 4 * prim;
		}
		// these are all aliases to the same storage
		addSpecialVar("__texdata", ptempn);
		addSpecialVarUnsafeOffsets("__texdata", offsets);
		addSpecialVar("__texidata", ptempn);
		addSpecialVarUnsafeOffsets("__texidata", offsets);
		addSpecialVar("__texudata", ptempn);
		addSpecialVarUnsafeOffsets("__texudata", offsets);
	}
	
	/**
	 * subclasses: use this to create special locations referencing arbitrary java local vars.
	 * 
	 * @param linkname
	 * @param jvars
	 */
	protected void addSpecialVar(String linkname, String ...jvars) {
		specials.put(linkname, jvars);
	}
	
	protected void addSpecialVarUnsafeOffsets(String linkname, int... offsets) {
		special_offsets.put(linkname, offsets);
	}
	
	protected void addOp(MacroOp.Code code, OpHandler h) {
		opimpl.put(code, h);
	}
	
	protected void addOpDirect(MacroOp.Code code, byte jop) {
		opimpl.put(code, (MethodBuilder mb, MacroOp mop) -> {
			mb.op(jop);
		});
	}
	
	protected void assembleOp(MethodBuilder mb, MacroOp mop) {
		OpHandler h = opimpl.get(mop.code);
		if (h == null) throw new BuildException(String.format("unimplemented op %s in shader %s", mop.code, env.shaderType()));
		if (mop.label != null) {
			mb.op(Op.NOP).label("__" + mop.label.toString());
		}
		h.assemble(mb, mop);
	}
	
	/**
	 * subclasses: use this to assemble method body.
	 * 
	 * @param mb
	 * @param jvarcount number of java local vars to reserve
	 * @param ops
	 */
	protected void assembleMethod(MethodBuilder mb, int jvarcount, List<MacroOp> ops) {
		if (current_mb != null) throw new IllegalStateException("method assembly already in progress");
		mb.pushScope();
		try {
			current_mb = mb;
			// TODO 64-bit types would need 2 local vars each if implemented
			jvar_base = current_mb.scope().alloc(jvarcount);
			for (MacroOp op : ops) {
				// group count for this op
				int gc = groupcount;
				boolean dst_once = true;
				for (int i = 0; i < op.code.dest_args; i++) {
					dst_once &= op.args[i].flags().contains(PrimitiveLocation.Flags.GROUP_ONCE);
				}
				boolean src_once = true;
				for (int i = op.code.dest_args; i < op.args.length; i++) {
					src_once &= op.args[i].flags().contains(PrimitiveLocation.Flags.GROUP_ONCE);
				}
				// only exec once if arg locations permit it...
				if (dst_once && op.code.dest_args > 0) gc = 1;
				if (dst_once && src_once) gc = 1;
				// or if the op can't exec in a grouped manner (jumps)
				if (MacroOp.UNGROUPABLE.contains(op.code)) gc = 1;
				for (int g = 0; g < gc; g++) {
					// load all read args onto stack
					int i0 = op.code.dest_mode == MacroOp.DestMode.WRITE ? op.code.dest_args : 0;
					for (int i = i0; i < op.args.length; i++) {
						LocationAssembler l = op.args[i].assemble(this);
						// if reading grouped from ungrouped, always use group 0
						boolean once = op.args[i].flags().contains(PrimitiveLocation.Flags.GROUP_ONCE);
						if (l != null) l.load(once ? 0 : g);
					}
					// execute op
					assembleOp(mb, op);
					// pop all write args from stack
					for (int i = op.code.dest_args; i-- > 0; ) {
						LocationAssembler l = op.args[i].assemble(this);
						if (l != null) l.store(g);
					}
				}
			}
		} finally {
			current_mb = null;
			jvar_base = 0;
			mb.popScope();
		}
	}

	/**
	 * subclasses: override to implement group behaviour for locals.
	 * 
	 * @return offset of local variable from start of range reserved for locals.
	 * 
	 * @param mb
	 * @param reg
	 * @param group
	 * @return
	 */
	protected int jvarLocalOffset(MethodBuilder mb, int reg, int group) {
		return reg;
	}
	
	/**
	 * subclasses: override to implement group behaviour for specials.
	 * 
	 * @return index of local var name in array given to {@link #addSpecialVar(String, String...)}.
	 */
	protected int jvarSpecialIndex(String linkname, int primindex, int group) {
		return primindex;
	}
	
	/**
	 * subclasses: specify how much space is reserved at the base of storage areas.
	 * e.g. 16 for gl_Position vertex output. will be applied as a constant offset
	 * for all storage locations.
	 * 
	 * @param stor
	 * @param ifaceindex
	 * @return
	 */
	protected abstract int storageBaseOffset(UnsafeStorage stor, int ifaceindex);
	
	/**
	 * subclasses: implement this to load things from memory.
	 * the stack is already loaded with the offset of the primitive in question within the storage.
	 * 
	 * stack: [int] -> []
	 * 
	 */
	protected abstract void assembleUnsafeLoad(
		MethodBuilder mb, UnsafeStorage stor, int ifaceindex, LocationAssembler iface_offset,
		BuiltinType type, EnumSet<PrimitiveLocation.Flags> flags, int group
	);
	
	/**
	 * subclasses: implement this to store things to memory.
	 * the stack is already loaded with the offset of the primitive in question within the storage.
	 * the value to be stored is in local variable 'storeval'.
	 * 
	 * stack: [int] -> []
	 * 
	 */
	protected abstract void assembleUnsafeStore(
		MethodBuilder mb, UnsafeStorage stor, int ifaceindex, LocationAssembler iface_offset,
		BuiltinType type, EnumSet<PrimitiveLocation.Flags> flags, int group
	);
	
}
