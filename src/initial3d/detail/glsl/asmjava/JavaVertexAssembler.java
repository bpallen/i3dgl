package initial3d.detail.glsl.asmjava;

import java.util.EnumSet;

import initial3d.GLEnum;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.bytecode.Flag;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;
import initial3d.detail.glsl.intermediate.Environment;
import initial3d.detail.glsl.intermediate.LocationAssembler;
import initial3d.detail.glsl.intermediate.ProgramBuilder;
import initial3d.detail.glsl.intermediate.ShaderInterfaceView;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation.Flags;
import initial3d.detail.glsl.lang.BuiltinType;

public class JavaVertexAssembler extends JavaAssembler {
	
	private ClassBuilder cb;
	
	public JavaVertexAssembler(Environment env_, ClassBuilder cb_) {
		super(env_, 1);
		cb = cb_;
		implementTextureSpecials(0);
		addSpecialVar("gl_VertexID", "vertid");
		addSpecialVar("gl_InstanceID", "instid");
		addSpecialVar("gl_PerVertex", "pos_x", "pos_y", "pos_z", "pos_w");
	}

	private void assembleShadeVertex(Translator trans) {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("shadeVertex_static");
		// void (long pvert_out, long ptemp, long puniforms, long pattrib_in)
		mb.declareType(true, void.class, long.class, long.class, long.class, long.class);
		mb.declareParamNames("pvert_out", "ptemp", "puniforms", "pattrib_in");
		// TODO proper max stack?
		mb.maxStack(8);
		// special vars
		mb.scope().alloc(int.class, "vertid");
		mb.scope().alloc(int.class, "instid");
		mb.scope().alloc(float.class, "pos_x");
		mb.scope().alloc(float.class, "pos_y");
		mb.scope().alloc(float.class, "pos_z");
		mb.scope().alloc(float.class, "pos_w");
		// init in case never written to by shader
		mb.constant(0.f);
		mb.fstore("pos_x");
		mb.constant(0.f);
		mb.fstore("pos_y");
		mb.constant(0.f);
		mb.fstore("pos_z");
		mb.constant(0.f);
		mb.fstore("pos_w");
		// special inputs
		mb.lload("pattrib_in");
		TypeOps.intops.unsafeLoad(mb, false);
		mb.istore("vertid");
		mb.lload("pattrib_in");
		mb.constant(4L);
		mb.op(Op.LADD);
		TypeOps.intops.unsafeLoad(mb, false);
		mb.istore("instid");
		// method body
		assembleMethod(mb, trans.localCount(), trans.ops());
		// special outputs
		for (int i = 0; i < 4; i++) {
			mb.lload("pvert_out");
			mb.constant(4L * i);
			mb.op(Op.LADD);
			mb.fload(mb.scope().get("pos_x") + i);
			TypeOps.floatops.unsafeStore(mb);
		}
		mb.op(Op.RETURN);
	}
	
	private void assembleVertexOutSize(ShaderInterfaceView outputs) {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("vertexOutSize_static");
		mb.declareType(true, int.class);
		// 16 bytes for gl_Position
		mb.constant(16 + outputs.iface().elementAllocSize(0));
		mb.op(Op.IRETURN);
	}
	
	private void assembleGeometryOutSize(ShaderInterfaceView outputs) {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("geometryOutSize_static");
		mb.declareType(true, int.class);
		// 16 bytes for gl_Position
		mb.constant(16 + outputs.iface().elementAllocSize(0));
		mb.op(Op.IRETURN);
	}
	
	@Override
	public void assemble(ProgramBuilder builder, Translator trans) {
		assembleShadeVertex(trans);
		assembleVertexOutSize(environment().outputs());
		if (builder.environment(GLEnum.GL_GEOMETRY_SHADER) == null) {
			// TODO where should this actually go?
			assembleGeometryOutSize(environment().outputs());
		}
	}
	
	@Override
	protected int storageBaseOffset(UnsafeStorage stor, int ifaceindex) {
		switch (stor) {
		case INPUT:
			// gl_VertexID, gl_InstanceID
			return 16;
		case OUTPUT:
			// gl_Position
			return 16;
		case UNIFORM:
			if (ifaceindex == 0) return JavaProgramBuilder.UNIFORM_BASE_OFFSET;
			return 0;
		case LOCAL:
			// texdata
			return 16;
		case CONSTANT:
		default:
			return 0;
		}
	}

	@Override
	protected void assembleUnsafeLoad(
		MethodBuilder mb, UnsafeStorage stor, int ifaceindex, LocationAssembler iface_offset,
		BuiltinType type, EnumSet<Flags> flags, int group
	) {
		// group is ignored (always 0)
		switch (stor) {
		case CONSTANT:
			throw new UnsupportedOperationException("constant storage not implemented yet");
		case LOCAL:
			mb.op(Op.I2L);
			mb.lload("ptemp");
			mb.op(Op.LADD);
			TypeOps.typeops(type).unsafeLoad(mb, false);
			break;
		case UNIFORM:
			mb.op(Op.I2L);
			if (ifaceindex == 0) {
				mb.lload("puniforms");
			} else {
				// FIXME uniform buffer access
				throw new UnsupportedOperationException("uniform buffers not implemented yet");
			}
			mb.op(Op.LADD);
			TypeOps.typeops(type).unsafeLoad(mb, true);
			break;
		case INPUT:
			mb.op(Op.I2L);
			mb.lload("pattrib_in");
			mb.op(Op.LADD);
			TypeOps.typeops(type).unsafeLoad(mb, true);
			break;
		case OUTPUT:
			mb.op(Op.I2L);
			mb.lload("pvert_out");
			mb.op(Op.LADD);
			TypeOps.typeops(type).unsafeLoad(mb, false);
			break;
		default:
			throw new AssertionError("storage case not handled");
		}
	}

	@Override
	protected void assembleUnsafeStore(
		MethodBuilder mb, UnsafeStorage stor, int ifaceindex, LocationAssembler iface_offset,
		BuiltinType type, EnumSet<Flags> flags, int group
	) {
		// group is ignored (always 0)
		switch (stor) {
		case CONSTANT:
			throw new UnsupportedOperationException("constant storage not implemented yet");
		case LOCAL:
			mb.op(Op.I2L);
			mb.lload("ptemp");
			mb.op(Op.LADD);
			TypeOps.typeops(type).load(mb, mb.scope().get("storeval"));
			TypeOps.typeops(type).unsafeStore(mb);
			break;
		case UNIFORM:
			throw new UnsupportedOperationException("cant write to uniforms");
		case INPUT:
			throw new UnsupportedOperationException("cant write to vertex inputs");
		case OUTPUT:
			mb.op(Op.I2L);
			mb.lload("pvert_out");
			mb.op(Op.LADD);
			TypeOps.typeops(type).load(mb, mb.scope().get("storeval"));
			TypeOps.typeops(type).unsafeStore(mb);
			break;
		default:
			throw new AssertionError("storage case not handled");
		}
	}

}
