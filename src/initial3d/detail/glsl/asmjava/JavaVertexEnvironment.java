package initial3d.detail.glsl.asmjava;

import initial3d.GLEnum;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.AbstractEnvironment;
import initial3d.detail.glsl.intermediate.Assembler;

public class JavaVertexEnvironment extends AbstractEnvironment {

	private JavaVertexAssembler asm;
	private ClassBuilder cb;
	
	public JavaVertexEnvironment(InfoLog infolog_, ClassBuilder cb_) {
		super(GLEnum.GL_VERTEX_SHADER, infolog_);
		cb = cb_;
		asm = new JavaVertexAssembler(this, cb_);
	}

	@Override
	public Assembler assembler() {
		return asm;
	}

}
