package initial3d.detail.glsl.asmjava;

import java.util.EnumSet;

import initial3d.GLEnum;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.bytecode.Flag;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;
import initial3d.detail.glsl.intermediate.Environment;
import initial3d.detail.glsl.intermediate.LocationAssembler;
import initial3d.detail.glsl.intermediate.ProgramBuilder;
import initial3d.detail.glsl.intermediate.ShaderInterfaceView;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.locations.InterfacePrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation;
import initial3d.detail.glsl.intermediate.locations.PrimitiveLocation.Flags;
import initial3d.detail.glsl.lang.BuiltinType;

public class JavaFragmentAssembler extends JavaAssembler {

	// TODO flat inputs should be group_once
	
	private ClassBuilder cb;
	
	public JavaFragmentAssembler(Environment env_, ClassBuilder cb_) {
		super(env_, 4);
		cb = cb_;
		implementTextureSpecials(0);
		addSpecialVar(
			"gl_FragCoord",
			"x0", "x1", "x0", "x1", "y0", "y0", "y1", "y1",
			"z00", "z10", "z01", "z11", "iw00", "iw10", "iw01", "iw11"
		);
		// TODO frag special vars
	}

	private void assembleShadeFragment(Translator trans) {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("shadeFragmentQuad_static");
		// int (long pfrag_out, long ptemp, long puniforms, long pvary, long pvary_ddx, long pvary_ddy,
		// int x, int y,
		// float z, float dz_dx, float dz_dy,
		// float iw, float diw_dx, float diw_dy)
		mb.declareType(
			true, int.class, long.class, long.class, long.class, long.class, long.class, long.class,
			int.class, int.class, float.class, float.class, float.class, float.class, float.class, float.class
		);
		mb.declareParamNames(
			"pfrag_out", "ptemp", "puniforms", "pvary", "pvary_ddx", "pvary_ddy",
			"x0i", "y0i", "z00", "dzdx", "dzdy", "iw00", "diwdx", "diwdy"
		);
		// TODO proper max stack?
		mb.maxStack(8);
		// special vars
		mb.scope().alloc(int.class, "acceptflags");
		mb.scope().alloc(float.class, "x0");
		mb.scope().alloc(float.class, "y0");
		mb.scope().alloc(float.class, "x1");
		mb.scope().alloc(float.class, "y1");
		mb.scope().alloc(float.class, "z10");
		mb.scope().alloc(float.class, "z01");
		mb.scope().alloc(float.class, "z11");
		mb.scope().alloc(float.class, "iw10");
		mb.scope().alloc(float.class, "iw01");
		mb.scope().alloc(float.class, "iw11");
		mb.scope().alloc(float.class, "w00");
		mb.scope().alloc(float.class, "w10");
		mb.scope().alloc(float.class, "w01");
		mb.scope().alloc(float.class, "w11");
		// special inputs
		mb.constant(0xF);
		mb.istore("acceptflags");
		// x,y
		// TODO pixel center integer
		mb.iload("x0i");
		mb.op(Op.I2F);
		mb.constant(0.5f);
		mb.op(Op.FADD);
		mb.fstore("x0");
		mb.iload("x0i");
		mb.op(Op.I2F);
		mb.constant(1.5f);
		mb.op(Op.FADD);
		mb.fstore("x1");
		mb.iload("y0i");
		mb.op(Op.I2F);
		mb.constant(0.5f);
		mb.op(Op.FADD);
		mb.fstore("y0");
		mb.iload("y0i");
		mb.op(Op.I2F);
		mb.constant(1.5f);
		mb.op(Op.FADD);
		mb.fstore("y1");
		// z
		mb.fload("z00");
		mb.fload("dzdx");
		mb.op(Op.FADD);
		mb.fstore("z10");
		mb.fload("z00");
		mb.fload("dzdy");
		mb.op(Op.FADD);
		mb.fstore("z01");
		mb.fload("z00");
		mb.fload("dzdx");
		mb.op(Op.FADD);
		mb.fload("dzdy");
		mb.op(Op.FADD);
		mb.fstore("z11");
		// iw
		mb.fload("iw00");
		mb.fload("diwdx");
		mb.op(Op.FADD);
		mb.fstore("iw10");
		mb.fload("iw00");
		mb.fload("diwdy");
		mb.op(Op.FADD);
		mb.fstore("iw01");
		mb.fload("iw00");
		mb.fload("diwdx");
		mb.op(Op.FADD);
		mb.fload("diwdy");
		mb.op(Op.FADD);
		mb.fstore("iw11");
		// w
		mb.constant(1.f);
		mb.fload("iw00");
		mb.op(Op.FDIV);
		mb.fstore("w00");
		mb.constant(1.f);
		mb.fload("iw10");
		mb.op(Op.FDIV);
		mb.fstore("w10");
		mb.constant(1.f);
		mb.fload("iw01");
		mb.op(Op.FDIV);
		mb.fstore("w01");
		mb.constant(1.f);
		mb.fload("iw11");
		mb.op(Op.FDIV);
		mb.fstore("w11");
		// method body
		assembleMethod(mb, trans.localCount() * 4, trans.ops());
		// TODO frag special outputs?
		// TODO gl_FragDepth
		mb.iload("acceptflags");
		mb.op(Op.IRETURN);
	}
	
	private void assembleVaryingInterpolate2(ShaderInterfaceView inputs) {
		// TODO interpolate2 may not be the responsibility of the fragment assembler
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("varyingInterpolate2_static");
		// void varyingInterpolate2(long pvary_out, long pvary0_in, long pvary1_in, float k0, float k1, float w0, float w1, float w)
		mb.declareType(true, void.class, long.class, long.class, long.class, float.class, float.class, float.class, float.class, float.class);
		mb.declareParamNames("pvary_out", "pvary0", "pvary1", "k0", "k1", "w0", "w1", "w");
		mb.scope().alloc(float.class, "kw0");
		mb.scope().alloc(float.class, "kw1");
		mb.fload("k0");
		mb.fload("w0");
		mb.op(Op.FMUL);
		mb.fstore("kw0");
		mb.fload("k1");
		mb.fload("w1");
		mb.op(Op.FMUL);
		mb.fstore("kw1");
		for (InterfacePrimitiveLocation l : inputs.iface().elementPrimitives(0)) {
			mb.lload("pvary_out");
			mb.constant((long) l.address());
			mb.op(Op.LADD);
			if (l.flags().contains(PrimitiveLocation.Flags.FLAT)) {
				// flat, grab one value
				mb.lload("pvary0");
				mb.constant((long) l.address());
				mb.op(Op.LADD);
				TypeOps.typeops(l.primitiveType()).unsafeLoad(mb, false);
			} else {
				// interpolate if not flat
				if (!l.type().plainText().equals("float")) throw new UnsupportedOperationException("cant interpolate " + l.type());
				if (l.flags().contains(PrimitiveLocation.Flags.NOPERSPECTIVE)) {
					// clip space, noperspective interp needs mul by w
					mb.lload("pvary0");
					mb.constant((long) l.address());
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, false);
					mb.fload("kw0");
					mb.op(Op.FMUL);
					mb.lload("pvary1");
					mb.constant((long) l.address());
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, false);
					mb.fload("kw1");
					mb.op(Op.FMUL);
					mb.op(Op.FADD);
					mb.fload("w");
					mb.op(Op.FDIV);
				} else {
					// clip space, perspective interp is linear
					mb.lload("pvary0");
					mb.constant((long) l.address());
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, false);
					mb.fload("k0");
					mb.op(Op.FMUL);
					mb.lload("pvary1");
					mb.constant((long) l.address());
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, false);
					mb.fload("k1");
					mb.op(Op.FMUL);
					mb.op(Op.FADD);
				}
			}
			TypeOps.typeops(l.primitiveType()).unsafeStore(mb);
		}
		mb.op(Op.RETURN);
	}
	
	private void assembleVaryingInterpolate3(ShaderInterfaceView inputs) {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("varyingInterpolate3_static");
		// void varyingInterpolate3(long pvary_out, long pvary0_in, long pvary1_in, long pvary2_in,
		// float k0, float k1, float k2, float iw0, float iw1, float iw2)
		mb.declareType(
			true, void.class, long.class, long.class, long.class, long.class,
			float.class, float.class, float.class, float.class, float.class, float.class
		);
		mb.declareParamNames("pvary_out", "pvary0", "pvary1", "pvary2", "k0", "k1", "k2", "iw0", "iw1", "iw2");
		mb.scope().alloc(float.class, "kiw0");
		mb.scope().alloc(float.class, "kiw1");
		mb.scope().alloc(float.class, "kiw2");
		mb.fload("k0");
		mb.fload("iw0");
		mb.op(Op.FMUL);
		mb.fstore("kiw0");
		mb.fload("k1");
		mb.fload("iw1");
		mb.op(Op.FMUL);
		mb.fstore("kiw1");
		mb.fload("k2");
		mb.fload("iw2");
		mb.op(Op.FMUL);
		mb.fstore("kiw2");
		for (InterfacePrimitiveLocation l : inputs.iface().elementPrimitives(0)) {
			mb.lload("pvary_out");
			mb.constant((long) l.address());
			mb.op(Op.LADD);
			if (l.flags().contains(PrimitiveLocation.Flags.FLAT)) {
				// flat, grab one value
				mb.lload("pvary0");
				mb.constant((long) l.address());
				mb.op(Op.LADD);
				TypeOps.typeops(l.primitiveType()).unsafeLoad(mb, false);
			} else {
				// interpolate if not flat
				if (!l.type().plainText().equals("float")) throw new UnsupportedOperationException("cant interpolate " + l.type());
				if (l.flags().contains(PrimitiveLocation.Flags.NOPERSPECTIVE)) {
					// screen space, noperspective interp is linear
					mb.lload("pvary0");
					mb.constant((long) l.address());
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, false);
					mb.fload("k0");
					mb.op(Op.FMUL);
					mb.lload("pvary1");
					mb.constant((long) l.address());
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, false);
					mb.fload("k1");
					mb.op(Op.FMUL);
					mb.op(Op.FADD);
					mb.lload("pvary2");
					mb.constant((long) l.address());
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, false);
					mb.fload("k2");
					mb.op(Op.FMUL);
					mb.op(Op.FADD);
				} else {
					// screen space, perspective interp needs div by w
					// interpolation is left divided by w (mul by w happens when reading in the frag shader)
					mb.lload("pvary0");
					mb.constant((long) l.address());
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, false);
					mb.fload("kiw0");
					mb.op(Op.FMUL);
					mb.lload("pvary1");
					mb.constant((long) l.address());
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, false);
					mb.fload("kiw1");
					mb.op(Op.FMUL);
					mb.op(Op.FADD);
					mb.lload("pvary2");
					mb.constant((long) l.address());
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, false);
					mb.fload("kiw2");
					mb.op(Op.FMUL);
					mb.op(Op.FADD);
				}
			}
			TypeOps.typeops(l.primitiveType()).unsafeStore(mb);
		}
		mb.op(Op.RETURN);
	}
	
	private void assembleVaryingSub(ShaderInterfaceView inputs) {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("varyingSub_static");
		// void varyingSub(long pvary_out, long pvary0_in, long pvary1_in)
		mb.declareType(true, void.class, long.class, long.class, long.class);
		mb.declareParamNames("pvary_out", "pvary0", "pvary1");
		for (InterfacePrimitiveLocation l : inputs.iface().elementPrimitives(0)) {
			if (!l.flags().contains(PrimitiveLocation.Flags.FLAT)) {
				// calc derivatives if not flat
				if (!l.type().plainText().equals("float")) throw new UnsupportedOperationException("cant interpolate " + l.type());
				mb.lload("pvary_out");
				mb.constant((long) l.address());
				mb.op(Op.LADD);
				mb.lload("pvary0");
				mb.constant((long) l.address());
				mb.op(Op.LADD);
				TypeOps.floatops.unsafeLoad(mb, false);
				mb.lload("pvary1");
				mb.constant((long) l.address());
				mb.op(Op.LADD);
				TypeOps.floatops.unsafeLoad(mb, false);
				mb.op(Op.FSUB);
				TypeOps.floatops.unsafeStore(mb);
			}
		}
		mb.op(Op.RETURN);
	}
	
	private void assembleVaryingAdd(ShaderInterfaceView inputs) {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("varyingAdd_static");
		// void varyingAdd(long pvary_out, long pvary_in, long pderiv_in)
		mb.declareType(true, void.class, long.class, long.class, long.class);
		mb.declareParamNames("pvary_out", "pvary", "pderiv");
		for (InterfacePrimitiveLocation l : inputs.iface().elementPrimitives(0)) {
			if (!l.flags().contains(PrimitiveLocation.Flags.FLAT)) {
				// add derivatives if not flat
				if (!l.type().plainText().equals("float")) throw new UnsupportedOperationException("cant interpolate " + l.type());
				mb.lload("pvary_out");
				mb.constant((long) l.address());
				mb.op(Op.LADD);
				mb.lload("pvary");
				mb.constant((long) l.address());
				mb.op(Op.LADD);
				TypeOps.floatops.unsafeLoad(mb, false);
				mb.lload("pderiv");
				mb.constant((long) l.address());
				mb.op(Op.LADD);
				TypeOps.floatops.unsafeLoad(mb, false);
				mb.op(Op.FADD);
				TypeOps.floatops.unsafeStore(mb);
			}
		}
		mb.op(Op.RETURN);
	}
	
	private void assembleVaryingMul(ShaderInterfaceView inputs) {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("varyingMul_static");
		//void varyingMul(long pderiv_out, long pderiv_in, float k)
		mb.declareType(true, void.class, long.class, long.class, float.class);
		mb.declareParamNames("pderiv_out", "pderiv", "k");
		for (InterfacePrimitiveLocation l : inputs.iface().elementPrimitives(0)) {
			if (!l.flags().contains(PrimitiveLocation.Flags.FLAT)) {
				// add derivatives if not flat
				if (!l.type().plainText().equals("float")) throw new UnsupportedOperationException("cant interpolate " + l.type());
				mb.lload("pderiv_out");
				mb.constant((long) l.address());
				mb.op(Op.LADD);
				mb.lload("pderiv");
				mb.constant((long) l.address());
				mb.op(Op.LADD);
				TypeOps.floatops.unsafeLoad(mb, false);
				mb.fload("k");
				mb.op(Op.FMUL);
				TypeOps.floatops.unsafeStore(mb);
			}
		}
		mb.op(Op.RETURN);
	}
	
	private void assembleVaryingCopy(ShaderInterfaceView inputs) {
		MethodBuilder mb = cb.declareMethod();
		mb.declareFlag(Flag.PUBLIC);
		mb.declareName("varyingCopy_static");
		// void varyingCopy(long pvary_out, long pvary_in)
		mb.declareType(true, void.class, long.class, long.class);
		mb.declareParamNames("pvary_out", "pvary");
		for (InterfacePrimitiveLocation l : inputs.iface().elementPrimitives(0)) {
			mb.lload("pvary_out");
			mb.constant((long) l.address());
			mb.op(Op.LADD);
			mb.lload("pvary");
			mb.constant((long) l.address());
			mb.op(Op.LADD);
			TypeOps.typeops(l.primitiveType()).unsafeLoad(mb, false);
			TypeOps.typeops(l.primitiveType()).unsafeStore(mb);
		}
		mb.op(Op.RETURN);
	}
	
	@Override
	public void assemble(ProgramBuilder builder, Translator trans) {
		ShaderInterfaceView inputs = environment().inputs();
		assembleShadeFragment(trans);
		assembleVaryingInterpolate2(inputs);
		assembleVaryingInterpolate3(inputs);
		assembleVaryingSub(inputs);
		assembleVaryingAdd(inputs);
		assembleVaryingMul(inputs);
		assembleVaryingCopy(inputs);
	}

	@Override
	protected int jvarLocalOffset(MethodBuilder mb, int reg, int group) {
		return reg * 4 + group;
	}

	@Override
	protected int jvarSpecialIndex(String linkname, int primindex, int group) {
		return primindex * 4 + group;
	}

	@Override
	protected int storageBaseOffset(UnsafeStorage stor, int ifaceindex) {
		switch (stor) {
		case INPUT:
			// TODO frag input reserve?
			return 0;
		case OUTPUT:
			// TODO frag output reserve?
			return 0;
		case UNIFORM:
			if (ifaceindex == 0) return JavaProgramBuilder.UNIFORM_BASE_OFFSET;
			return 0;
		case LOCAL:
			// texdata
			return 64;
		case CONSTANT:
		default:
			return 0;
		}
	}

	@Override
	protected void assembleUnsafeLoad(
		MethodBuilder mb, UnsafeStorage stor, int ifaceindex, LocationAssembler iface_offset,
		BuiltinType type, EnumSet<Flags> flags, int group
	) {
		switch (stor) {
		case CONSTANT:
			throw new UnsupportedOperationException("constant storage not implemented yet");
		case LOCAL:
			mb.constant(4);
			mb.op(Op.IMUL);
			// TODO prim size
			mb.constant(group * 4);
			mb.op(Op.IADD);
			mb.op(Op.I2L);
			mb.lload("ptemp");
			mb.op(Op.LADD);
			TypeOps.typeops(type).unsafeLoad(mb, false);
			break;
		case UNIFORM:
			// group ignored for uniforms
			mb.op(Op.I2L);
			if (ifaceindex == 0) {
				mb.lload("puniforms");
			} else {
				// FIXME uniform buffer access
				throw new UnsupportedOperationException("uniform buffers not implemented yet");
			}
			mb.op(Op.LADD);
			TypeOps.typeops(type).unsafeLoad(mb, true);
			break;
		case INPUT:
			mb.pushScope();
			mb.scope().alloc(int.class, "offset");
			mb.istore("offset");
			mb.iload("offset");
			// base value
			mb.op(Op.I2L);
			mb.lload("pvary");
			mb.op(Op.LADD);
			TypeOps.typeops(type).unsafeLoad(mb, true);
			// interpolate if not flat
			if (!flags.contains(PrimitiveLocation.Flags.FLAT)) {
				if (!type.name().plainText().equals("float")) throw new UnsupportedOperationException("cant interpolate " + type.name());
				if ((group & 1) != 0) {
					// apply x deriv
					mb.iload("offset");
					mb.op(Op.I2L);
					mb.lload("pvary_ddx");
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, true);
					mb.op(Op.FADD);
				}
				if ((group & 2) != 0) {
					// apply y deriv
					mb.iload("offset");
					mb.op(Op.I2L);
					mb.lload("pvary_ddy");
					mb.op(Op.LADD);
					TypeOps.floatops.unsafeLoad(mb, true);
					mb.op(Op.FADD);
				}
				// multiply by w if perspective interpolation
				if (!flags.contains(PrimitiveLocation.Flags.NOPERSPECTIVE)) {
					switch (group) {
					case 0:
						mb.fload("w00");
						break;
					case 1:
						mb.fload("w10");
						break;
					case 2:
						mb.fload("w01");
						break;
					case 3:
						mb.fload("w11");
						break;
					default:
						throw new IllegalArgumentException("bad group");
					}
					mb.op(Op.FMUL);
				}
			}
			mb.popScope();
			break;
		case OUTPUT:
			mb.constant(4);
			mb.op(Op.IMUL);
			// TODO prim size
			mb.constant(group * 4);
			mb.op(Op.IADD);
			mb.op(Op.I2L);
			mb.lload("pfrag_out");
			mb.op(Op.LADD);
			TypeOps.typeops(type).unsafeLoad(mb, false);
			break;
		default:
			throw new AssertionError("storage case not handled");
		}
	}

	@Override
	protected void assembleUnsafeStore(
		MethodBuilder mb, UnsafeStorage stor, int ifaceindex, LocationAssembler iface_offset,
		BuiltinType type, EnumSet<Flags> flags, int group
	) {
		switch (stor) {
		case CONSTANT:
			throw new UnsupportedOperationException("constant storage not implemented yet");
		case LOCAL:
			mb.constant(4);
			mb.op(Op.IMUL);
			// TODO prim size
			mb.constant(group * 4);
			mb.op(Op.IADD);
			mb.op(Op.I2L);
			mb.lload("ptemp");
			mb.op(Op.LADD);
			TypeOps.typeops(type).load(mb, mb.scope().get("storeval"));
			TypeOps.typeops(type).unsafeStore(mb);
			break;
		case UNIFORM:
			throw new UnsupportedOperationException("cant write to uniforms");
		case INPUT:
			throw new UnsupportedOperationException("cant write to frag inputs");
		case OUTPUT:
			mb.constant(4);
			mb.op(Op.IMUL);
			// TODO prim size
			mb.constant(group * 4);
			mb.op(Op.IADD);
			mb.op(Op.I2L);
			mb.lload("pfrag_out");
			mb.op(Op.LADD);
			TypeOps.typeops(type).load(mb, mb.scope().get("storeval"));
			TypeOps.typeops(type).unsafeStore(mb);
			break;
		default:
			throw new AssertionError("storage case not handled");
		}
	}

}
