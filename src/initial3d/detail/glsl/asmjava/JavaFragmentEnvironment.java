package initial3d.detail.glsl.asmjava;

import initial3d.GLEnum;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.intermediate.AbstractEnvironment;
import initial3d.detail.glsl.intermediate.Assembler;

public class JavaFragmentEnvironment extends AbstractEnvironment {

	private ClassBuilder cb;
	private JavaFragmentAssembler asm;
	
	public JavaFragmentEnvironment(InfoLog infolog_, ClassBuilder cb_) {
		super(GLEnum.GL_FRAGMENT_SHADER, infolog_);
		cb = cb_;
		asm = new JavaFragmentAssembler(this, cb_);
	}

	@Override
	public Assembler assembler() {
		return asm;
	}

}
