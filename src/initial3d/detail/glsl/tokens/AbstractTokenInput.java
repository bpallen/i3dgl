package initial3d.detail.glsl.tokens;

import java.io.IOException;
import java.util.Objects;

import initial3d.detail.glsl.InfoLog;

public abstract class AbstractTokenInput implements TokenInput {

	private Token peekback = null;
	private InfoLog log;
	
	public AbstractTokenInput(InfoLog log_) {
		log = Objects.requireNonNull(log_);
	}
	
	@Override
	public InfoLog infoLog() {
		return log;
	}
	
	@Override
	public Token nextToken() throws IOException {
		if (peekback == null) {
			return nextTokenImpl();
		} else {
			Token r = peekback;
			peekback = null;
			return r;
		}
	}
	
	@Override
	public Token peekToken() throws IOException {
		if (peekback == null) peekback = nextTokenImpl();
		return peekback;
	}

	protected abstract Token nextTokenImpl() throws IOException;
	
}
