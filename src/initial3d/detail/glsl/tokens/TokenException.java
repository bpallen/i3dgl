package initial3d.detail.glsl.tokens;

import initial3d.detail.glsl.BuildException;

public class TokenException extends BuildException {

	private static final long serialVersionUID = 1L;
	
	private Token t;
	
	public TokenException() { }
	
	public TokenException(Token t_) {
		t = t_;
	}
	
	public TokenException(Token t_, String msg_) {
		super(msg_);
		t = t_;
	}
	
	public Token token() {
		return t;
	}
}
