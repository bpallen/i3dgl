package initial3d.detail.glsl.tokens;

public class TokenExpectationException extends TokenException {

	private static final long serialVersionUID = 1L;
	
	private static String buildMessage(Token t, Token.Type ...types) {
		StringBuilder sb = new StringBuilder();
		if (types.length > 0) {
			sb.append("Expected ");
			for (Token.Type type : types) {
				sb.append("'");
				sb.append(type.exemplar());
				sb.append("' or ");
			}
			sb.setLength(sb.length() - 4);
			sb.append(", not ");
			sb.append(t.type().friendlyName());
			sb.append(" '");
			sb.append(t.text().replace("\n", "\\n"));
			sb.append("'");
		} else {
			sb.append("Unexpected ");
			sb.append(t.type().friendlyName());
			sb.append(" '");
			sb.append(t.text().replace("\n", "\\n"));
			sb.append("'");
		}
		return sb.toString();
	}
	
	private static String buildMessage(Token t, String ...ss) {
		StringBuilder sb = new StringBuilder();
		if (ss.length > 0) {
			sb.append("Expected ");
			for (String s : ss) {
				sb.append("'");
				sb.append(s);
				sb.append("' or ");
			}
			sb.setLength(sb.length() - 4);
			sb.append(", not ");
			sb.append(t.type().friendlyName());
			sb.append(" '");
			sb.append(t.text().replace("\n", "\\n"));
			sb.append("'");
		} else {
			sb.append("Unexpected ");
			sb.append(t.type().friendlyName());
			sb.append(" '");
			sb.append(t.text().replace("\n", "\\n"));
			sb.append("'");
		}
		return sb.toString();
	}
	
	public TokenExpectationException() { }
	
	public TokenExpectationException(Token t) {
		super(t, buildMessage(t, new Token.Type[] {}));
	}
	
	public TokenExpectationException(Token t, Token.Type ...types) {
		super(t, buildMessage(t, types));
	}
	
	public TokenExpectationException(Token t, String ...ss) {
		super(t, buildMessage(t, ss));
	}
	
}
