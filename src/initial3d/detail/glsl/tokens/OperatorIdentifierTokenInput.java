package initial3d.detail.glsl.tokens;

import java.io.IOException;

public class OperatorIdentifierTokenInput extends AbstractTokenInput {

	private TokenInput in;
	
	public OperatorIdentifierTokenInput(TokenInput in_) {
		super(in_.infoLog());
		in = in_;
	}

	@Override
	protected Token nextTokenImpl() throws IOException {
		Token t = in.nextToken();
		if (t.isText("operator") && in.hasNextToken(Token.Type.OPERATOR)) {
			TokenBuilder tb = new TokenBuilder(t);
			// add operator chars to token, set type to identifier
			Token t2 = in.nextToken();
			tb.appendText(t2.text());
			if (t2.isText("(")) tb.appendText(in.expectNextToken(")").text());
			if (t2.isText("[")) tb.appendText(in.expectNextToken("]").text());
			tb.type(Token.Type.IDENTIFIER);
			t = tb.toToken();
		}
		return t;
	}

}
