package initial3d.detail.glsl.tokens;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Objects;

import initial3d.detail.glsl.InfoLog;

public class Tokenizer extends AbstractTokenInput {

	private BufferedReader in;
	private String linetext = null;
	private Reader linein = null;
	private String filename;
	
	// TODO stop using a special value for 'notpeeked'
	private int peekbackc = 0;
	
	// current line and 'column' (actually character)
	private int line = 1, col = 1;
	
	// start-of-content flags
	private boolean filecontent_start = true, linecontent_start = true;
	
	public Tokenizer(InfoLog log_, String filename_, Reader in_) {
		super(log_);
		filename = filename_;
		in = new BufferedReader(Objects.requireNonNull(in_));
	}

	private int peekChar() throws IOException {
		if (peekbackc == 0) {
			// grab a new line if none current
			if (linein == null) {
				linetext = in.readLine();
				if (linetext != null) {
					linein = new StringReader(linetext);
				} else {
					// actual EOF
					peekbackc = -1;
					linetext = "";
					return -1;
				}
			}
			// grab next char
			int c = linein.read();
			// check for end of line
			if (c < 0) {
				// leave linetext so it can be reported
				linein = null;
				peekbackc = '\n';
			} else {
				peekbackc = c;
			}
		}
		return peekbackc;
	}
	
	private int nextChar() throws IOException {
		int r = peekChar();
		peekbackc = 0;
		// increment line/col numbers
		col++;
		if (r == '\n') {
			line++;
			col = 1;
		}
		return r;
	}
	
	private boolean matchesReserved(String s) {
		if (s.isEmpty()) return true;
		if ("{".startsWith(s)) return true;
		if ("}".startsWith(s)) return true;
		if (";".startsWith(s)) return true;
		for (String r : Token.KEYWORDS) {
			if (r.startsWith(s)) return true;
		}
		for (String r : Token.OPERATORS) {
			if (r.startsWith(s)) return true;
		}
		return false;
	}
	
	private boolean isReserved(String s) {
		if ("{".equals(s)) return true;
		if ("}".equals(s)) return true;
		if (";".equals(s)) return true;
		if (Token.KEYWORDS.contains(s)) return true;
		if (Token.OPERATORS.contains(s)) return true;
		return false;
	}
	
	private boolean matchesNumericLiteral(String s) {
		if (s.isEmpty()) return true;
		if (s.charAt(0) < '0' || s.charAt(0) > '9') return false;
		for (int i = 1; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c != '.' && (c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) return false;
		}
		return true;
	}
	
	private boolean isNumericLiteral(String s) {
		return !s.isEmpty() && matchesNumericLiteral(s);
	}
	
	private boolean matchesLiteral(String s) {
		return matchesNumericLiteral(s);
	}
	
	private boolean isLiteral(String s) {
		return isNumericLiteral(s);
	}
	
	private boolean matchesIdentifier(String s) {
		if (s.isEmpty()) return true;
		if (s.charAt(0) != '_' && (s.charAt(0) < 'a' || s.charAt(0) > 'z') && (s.charAt(0) < 'A' || s.charAt(0) > 'Z')) return false;
		for (int i = 1; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c != '_' && (c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) return false;
		}
		return true;
	}
	
	private boolean isIdentifier(String s) {
		return !s.isEmpty() && matchesIdentifier(s);
	}
	
	private boolean matchesEOLComment(String s) {
		// comments should not remove newlines
		// they will contain '\r' for windows-style files
		if (s.isEmpty()) return true;
		if (s.charAt(0) != '/') return false;
		if (s.length() == 1) return true;
		if (s.charAt(1) != '/') return false;
		for (int i = 2; i < s.length(); i++) {
			if (s.charAt(i) == '\n') return false;
		}
		return true;
	}
	
	private boolean isEOLComment(String s) {
		if (s.length() < 2) return false;
		return matchesEOLComment(s);
	}
	
	private boolean matchesBlockComment(String s) {
		// comments should not remove newlines
		// we remove newlines from inside block comments (only)
		if (s.isEmpty()) return true;
		if (s.charAt(0) != '/') return false;
		if (s.length() == 1) return true;
		if (s.charAt(1) != '*') return false;
		for (int i = 3; i < s.length() - 1; i++) {
			if (s.charAt(i) == '/' && s.charAt(i - 1) == '*') return false;
		}
		return true;
	}
	
	private boolean isBlockComment(String s) {
		if (s.length() < 4) return false;
		return matchesBlockComment(s) && s.charAt(s.length() - 1) == '/' && s.charAt(s.length() - 2) == '*';
	}
	
	private boolean matchesComment(String s) {
		return matchesEOLComment(s) || matchesBlockComment(s);
	}
	
	private boolean isComment(String s) {
		return isEOLComment(s) || isBlockComment(s);
	}
	
	private void skipUselessWhitespace() throws IOException {
		int c = peekChar();
		while (c == ' ' || c == '\t' || c == '\r') {
			nextChar();
			c = peekChar();
		}
	}
	
	@Override
	protected Token nextTokenImpl() throws IOException {
		
		TokenBuilder tok = new TokenBuilder();
		
		skipUselessWhitespace();
		
		// read while it looks like a comment
		while (matchesComment(tok.text() + (char) peekChar())) {
			tok.appendText((char) nextChar());
		}
		
		// discard if it was a comment
		if (isComment(tok.text())) {
			tok.text("");
			skipUselessWhitespace();
		}
		
		// grab line/col number and line text after skipping comments
		tok.fileName(filename);
		tok.line(line);
		tok.col(col);
		tok.lineText(linetext);
		
		// end of file?
		if (peekChar() == -1) {
			nextChar();
			tok.type(Token.Type.EOF);
			return tok.toToken();
		}
		
		// newline?
		if (peekChar() == '\n') {
			tok.appendText((char) nextChar());
			tok.type(Token.Type.NEWLINE);
			linecontent_start = true;
			return tok.toToken();
		}
		
		// start of actual content?
		if (filecontent_start) {
			tok.fileContentStart(true);
			filecontent_start = false;
		}
		
		// start of line?
		if (linecontent_start) {
			tok.lineContentStart(true);
			linecontent_start = false;
		}
		
		boolean added_chars = false;
		
		// try reserved symbols
		if (tok.type() == Token.Type.UNKNOWN && isReserved(tok.text())) tok.type(Token.classify(tok.text()));
		while (matchesReserved(tok.text() + (char) peekChar())) {
			tok.appendText((char) nextChar());
			added_chars = true;
		}
		if (isReserved(tok.text()) && added_chars) tok.type(Token.classify(tok.text()));
		added_chars = false;
		
		// try literals
		if (tok.type() == Token.Type.UNKNOWN && isLiteral(tok.text())) tok.type(Token.classify(tok.text()));
		while (matchesLiteral(tok.text() + (char) peekChar())) {
			tok.appendText((char) nextChar());
			added_chars = true;
		}
		if (isLiteral(tok.text()) && added_chars) tok.type(Token.classify(tok.text()));
		added_chars = false;
		
		// try identifier
		if (tok.type() == Token.Type.UNKNOWN && isIdentifier(tok.text())) tok.type(Token.classify(tok.text()));
		while (matchesIdentifier(tok.text() + (char) peekChar())) {
			tok.appendText((char) nextChar());
			added_chars = true;
		}
		if (isIdentifier(tok.text()) && added_chars) tok.type(Token.classify(tok.text()));
		added_chars = false;
		
		if (tok.text().isEmpty()) {
			// didn't match anything, read one char to keep moving
			tok.appendText((char) nextChar());
			// deal with suppressed newlines (does GLSL even have these?)
			if (tok.text().equals("\\")) {
				skipUselessWhitespace();
				if (peekChar() == '\n') {
					// skip this token + newline, try again
					nextChar();
					return nextTokenImpl();
				}
			}
		}
		
		return tok.toToken();
	}
}




















