package initial3d.detail.glsl.tokens;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import initial3d.detail.glsl.InfoLog;

public interface TokenInput {
	
	public InfoLog infoLog();
	
	public Token peekToken() throws IOException;

	public Token nextToken() throws IOException;
	
	default Token unexpectPeekToken(Token.Type ...types) throws IOException {
		Token t = peekToken();
		for (Token.Type type : types) {
			if (t.type() == type) throw expectationFailure(t);
		}
		return t;
	}
	
	default Token unexpectPeekToken(String ...ss) throws IOException {
		Token t = peekToken();
		for (String s : ss) {
			if (t.isText(s)) throw expectationFailure(t);
		}
		return t;
	}
	
	default Token unexpectNextToken(Token.Type ...types) throws IOException {
		unexpectPeekToken(types);
		return nextToken();
	}
	
	default Token unexpectNextToken(String ...ss) throws IOException {
		unexpectPeekToken(ss);
		return nextToken();
	}
	
	default Token unexpectPeekEmptyToken() throws IOException {
		Token t = peekToken();
		if (t.isEmpty()) throw expectationFailure(t);
		return t;
	}
	
	default Token unexpectNextEmptyToken() throws IOException {
		unexpectPeekEmptyToken();
		return nextToken();
	}
	
	default Token expectPeekToken(Token.Type ...types) throws IOException {
		Token t = peekToken();
		for (Token.Type type : types) {
			if (t.type() == type) return t;
		}
		throw expectationFailure(t, types);
	}
	
	default Token expectPeekToken(String ...ss) throws IOException {
		Token t = peekToken();
		for (String s : ss) {
			if (t.isText(s)) return t;
		}
		throw expectationFailure(t, ss);
	}
	
	default Token expectNextToken(Token.Type ...types) throws IOException {
		expectPeekToken(types);
		return nextToken();
	}
	
	default Token expectNextToken(String ...ss) throws IOException {
		expectPeekToken(ss);
		return nextToken();
	}
	
	default boolean hasNextToken() throws IOException {
		return peekToken().type() != Token.Type.EOF;
	}
	
	default boolean hasNextToken(Token.Type ...types) throws IOException {
		Token t = peekToken();
		for (Token.Type type : types) {
			if (t.type() == type) return true;
		}
		return false;
	}
	
	default boolean hasNextToken(String ...ss) throws IOException {
		Token t = peekToken();
		for (String s : ss) {
			if (t.isText(s)) return true;
		}
		return false;
	}
	
	default Token consumeNextToken(Token.Type ...types) throws IOException {
		if (hasNextToken(types)) return nextToken();
		return null;
	}
	
	default Token consumeNextToken(String ...ss) throws IOException {
		if (hasNextToken(ss)) return nextToken();
		return null;
	}
	
	default void readLine(TokenOutput out) throws IOException {
		for (Token t = peekToken(); t.type() != Token.Type.NEWLINE && t.type() != Token.Type.EOF; t = peekToken()) {
			nextToken();
			if (out != null) {
				out.writeToken(t);
			}
			// look for line-content-start after first token
			if (peekToken().lineContentStart()) break;
		}
	}
	
	default TokenInput readLineAsStream() throws IOException {
		ArrayList<Token> ts = new ArrayList<>();
		readLine((Token t) -> { ts.add(t); });
		return new IteratorTokenInput(infoLog(), ts);
	}
	
	default void skipLine() throws IOException {
		readLine(null);
		expectNextToken(Token.Type.NEWLINE);
	}
	
	default EmptyTokenInput asEmpty() {
		return new EmptyTokenInput(infoLog());
	}
	
	default List<Token> capture(String open, String close) throws IOException {
		infoLog().pushContext(String.format("capturing %s ... %s token group", open, close), peekToken());
		try {
			ArrayList<Token> tokens = new ArrayList<>();
			tokens.add(expectNextToken(open));
			int count = 1;
			while (hasNextToken() && count > 0) {
				Token t = nextToken();
				tokens.add(t);
				if (t.isText(open)) {
					count++;
				} else if (t.isText(close)) {
					count--;
				}
			}
			if (count > 0) {
				expectationFailure(peekToken());
			}
			return tokens;
		} finally {
			infoLog().popContext();
		}
	}
	
	default TokenExpectationException expectationFailure(Token t) {
		TokenExpectationException e = new TokenExpectationException(t);
		infoLog().error(e.getMessage(), t);
		throw e;
	}
	
	default TokenExpectationException expectationFailure(Token t, Token.Type ...types) {
		TokenExpectationException e = new TokenExpectationException(t, types);
		infoLog().error(e.getMessage(), t);
		throw e;
	}
	
	default TokenExpectationException expectationFailure(Token t, String ...ss) {
		TokenExpectationException e = new TokenExpectationException(t, ss);
		infoLog().error(e.getMessage(), t);
		throw e;
	}
	
	default void pushContext(String msg, Token t, boolean quiet) {
		infoLog().pushContext(msg, t, quiet);
	}
	
	default void pushContext(String msg, Token t) {
		infoLog().pushContext(msg, t);
	}
	
	default void pushContext(String msg) {
		infoLog().pushContext(msg);
	}
	
	default void popContext() {
		infoLog().popContext();
	}
}
