package initial3d.detail.glsl.tokens;

public class TokenBuilder {
	
	private int line, col;
	private Token.Type type;
	private StringBuilder text;
	private String filename = null;
	private String linetext = null;
	private Token original = null;
	private boolean linecontent_start = false, filecontent_start = false;
	
	public TokenBuilder() {
		line = 1;
		col = 1;
		type = Token.Type.UNKNOWN;
		text = new StringBuilder();
	}
	
	public TokenBuilder(Token t) {
		line = t.line();
		col = t.col();
		type = t.type();
		text = new StringBuilder(t.text());
		filename = t.fileName();
		linetext = t.lineText();
		original = t.original();
		linecontent_start = t.lineContentStart();
		filecontent_start = t.fileContentStart();
	}
	
	public int line() {
		return line;
	}
	
	public TokenBuilder line(int x) {
		line = x;
		return this;
	}
	
	public int col() {
		return col;
	}
	
	public TokenBuilder col(int x) {
		col = x;
		return this;
	}
	
	public Token.Type type() {
		return type;
	}
	
	public TokenBuilder type(Token.Type t) {
		type = t;
		return this;
	}
	
	public String text() {
		return text.toString();
	}
	
	public TokenBuilder text(String s) {
		text.setLength(0);
		text.append(s);
		return this;
	}
	
	public TokenBuilder appendText(String s) {
		text.append(s);
		return this;
	}
	
	public TokenBuilder appendText(char c) {
		text.append(c);
		return this;
	}
	
	public TokenBuilder prependText(String s) {
		String old = text();
		text = new StringBuilder(s);
		text.append(old);
		return this;
	}
	
	public String fileName() {
		return filename;
	}
	
	public TokenBuilder fileName(String s) {
		filename = s;
		return this;
	}
	
	public String lineText() {
		return linetext;
	}
	
	public TokenBuilder lineText(String s) {
		linetext = s;
		return this;
	}
	
	public Token original() {
		return original;
	}
	
	public TokenBuilder original(Token t) {
		original = t;
		return this;
	}
	
	public boolean lineContentStart() {
		return linecontent_start;
	}
	
	public TokenBuilder lineContentStart(boolean b) {
		linecontent_start = b;
		return this;
	}
	
	public boolean fileContentStart() {
		return filecontent_start;
	}
	
	public TokenBuilder fileContentStart(boolean b) {
		filecontent_start = b;
		return this;
	}
	
	public TokenBuilder classify() {
		type = Token.classify(text());
		return this;
	}
	
	public Token toToken() {
		return new Token(line, col, type, text.toString(), filename, linetext, original, linecontent_start, filecontent_start);
	}
	
}