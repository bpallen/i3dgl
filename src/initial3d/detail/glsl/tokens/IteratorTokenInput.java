package initial3d.detail.glsl.tokens;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

import initial3d.detail.glsl.InfoLog;

public class IteratorTokenInput extends AbstractTokenInput {

	private Iterator<Token> it;
	
	public IteratorTokenInput(InfoLog log_, Iterable<Token> c) {
		super(log_);
		it = Objects.requireNonNull(c).iterator();
	}
	
	public IteratorTokenInput(InfoLog log_, Iterator<Token> it_) {
		super(log_);
		it = Objects.requireNonNull(it_);
	}
	
	public IteratorTokenInput(InfoLog log_, Token ...ts) {
		super(log_);
		it = Arrays.asList(ts).iterator();
	}

	@Override
	protected Token nextTokenImpl() throws IOException {
		if (!it.hasNext()) return Token.EOF;
		return it.next();
	}

}
