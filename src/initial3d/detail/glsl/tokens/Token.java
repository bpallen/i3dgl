package initial3d.detail.glsl.tokens;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Token {
	
	/** Default EOF token. */
	public static final Token EOF;
	
	static {
		TokenBuilder tb = new TokenBuilder();
		tb.type(Token.Type.EOF);
		EOF = tb.toToken();
	}
	
	// keyword tokens
	public static final Set<String> KEYWORDS = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {
		"attribute",
		"const",
		"uniform",
		"varying",
		"layout",
		"centroid",
		"flat",
		"smooth",
		"noperspective",
		"break",
		"continue",
		"do",
		"for",
		"while",
		"switch",
		"case",
		"default",
		"if",
		"else",
		"in",
		"out",
		"inout",
		"float",
		"int",
		"void",
		"bool",
		"true",
		"false",
		"invariant",
		"discard",
		"return",
		"mat2",
		"mat3",
		"mat4",
		"mat2x2",
		"mat2x3",
		"mat2x4",
		"mat3x2",
		"mat3x3",
		"mat3x4",
		"mat4x2",
		"mat4x3",
		"mat4x4",
		"vec2",
		"vec3",
		"vec4",
		"ivec2",
		"ivec3",
		"ivec4",
		"bvec2",
		"bvec3",
		"bvec4",
		"uint",
		"uvec2",
		"uvec3",
		"uvec4",
		"lowp",
		"mediump",
		"highp",
		"precision",
		"sampler1D",
		"sampler2D",
		"sampler3D",
		"samplerCube",
		"sampler1DShadow",
		"sampler2DShadow",
		"samplerCubeShadow",
		"sampler1DArray",
		"sampler2DArray",
		"sampler1DArrayShadow",
		"sampler2DArrayShadow",
		"isampler1D",
		"isampler2D",
		"isampler3D",
		"isamplerCube",
		"isampler1DArray",
		"isampler2DArray",
		"usampler1D",
		"usampler2D",
		"usampler3D",
		"usamplerCube",
		"usampler1DArray",
		"usampler2DArray",
		"sampler2DRect",
		"sampler2DRectShadow",
		"isampler2DRect",
		"usampler2DRect",
		"samplerBuffer",
		"isamplerBuffer",
		"usamplerBuffer",
		"sampler2DMS",
		"isampler2DMS",
		"usampler2DMS",
		"sampler2DMSArray",
		"isampler2DMSArray",
		"usampler2DMSArray",
		"struct",
		// future use
		"common",
		"partition",
		"active",
		"asm",
		"class",
		"union",
		"enum",
		"typedef",
		"template",
		"this",
		"packed",
		"goto",
		"inline",
		"noinline",
		"volatile",
		"public",
		"static",
		"extern",
		"external",
		"interface",
		"long",
		"short",
		"double",
		"half",
		"fixed",
		"unsigned",
		"superp",
		"input",
		"output",
		"hvec2",
		"hvec3",
		"hvec4",
		"dvec2",
		"dvec3",
		"dvec4",
		"fvec2",
		"fvec3",
		"fvec4",
		"sampler3DRect",
		"filter",
		"image1D",
		"image2D",
		"image3D",
		"imageCube",
		"iimage1D",
		"iimage2D",
		"iimage3D",
		"iimageCube",
		"uimage1D",
		"uimage2D",
		"uimage3D",
		"uimageCube",
		"image1DArray",
		"image2DArray",
		"iimage1DArray",
		"iimage2DArray",
		"uimage1DArray",
		"uimage2DArray",
		"image1DShadow",
		"image2DShadow",
		"image1DArrayShadow",
		"image2DArrayShadow",
		"imageBuffer",
		"iimageBuffer",
		"uimageBuffer",
		"sizeof",
		"cast",
		"namespace",
		"using",
		"row_major",
		// implementation
		"__pragma",
		"operator",
		"__ref",
		"__constexpr",
		"__swizzle",
		"__arith"
	})));
		
	// operator tokens
	public static final Set<String> OPERATORS = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {
		"(",
		")",
		"[",
		"]",
		".",
		"++",
		"--",
		"+",
		"-",
		"~",
		"!",
		"*",
		"/",
		"%",
		"<<",
		">>",
		"<",
		">",
		"<=",
		">=",
		"==",
		"!=",
		"&",
		"^",
		"|",
		"&&",
		"^^",
		"||",
		"?",
		":",
		"=",
		"+=",
		"-=",
		"*=",
		"/=",
		"%=",
		"<<=",
		">>=",
		"&=",
		"^=",
		"|=",
		",",
		"#",
		"##",
		"...",
		"::"
	})));
	
	public static enum Type {
		UNKNOWN("unknown-token", "<?>"),
		EOF("end-file", "<eof>"),
		NEWLINE("newline", "\\n"),
		KEYWORD("keyword", "<keyword>"),
		IDENTIFIER("identifier", "<identifier>"),
		INT_LITERAL("integer-literal", "<int>"),
		FLOAT_LITERAL("floating-literal", "<float>"),
		OPERATOR("operator", "<operator>"),
		SEMICOLON("semicolon", ";"),
		OPEN_BRACE("open-brace", "{"),
		CLOSE_BRACE("close-brace", "}");
		
		private String name;
		private String exemplar;
		
		private Type(String name_, String exemplar_) {
			name = name_;
			exemplar = exemplar_;
		}
		
		public String friendlyName() {
			return name;
		}
		
		public String exemplar() {
			return exemplar;
		}
	}

	private int line, col;
	private Type type;
	private String text;
	private String filename;
	private String linetext;
	private Token original;
	private boolean linecontent_start, filecontent_start;
	
	public Token(int line_, int col_, Type type_, String text_, String filename_, String linetext_, Token original_, boolean linestart_, boolean filestart_) {
		line = line_;
		col = col_;
		type = Objects.requireNonNull(type_);
		text = Objects.requireNonNull(text_);
		filename = filename_;
		linetext = linetext_;
		original = original_;
		linecontent_start = linestart_;
		filecontent_start = filestart_;
	}
	
	public int line() {
		return line;
	}

	public int col() {
		return col;
	}

	public Type type() {
		return type;
	}

	public String text() {
		return text;
	}
	
	public String fileName() {
		return filename;
	}
	
	public String lineText() {
		return linetext;
	}
	
	/**
	 * @return The token this token was substituted for.
	 */
	public Token original() {
		return original;
	}
	
	/**
	 * @return true if this is the first non-whitespace token on the current line
	 */
	public boolean lineContentStart() {
		return linecontent_start;
	}
	
	/**
	 * @return true if this is the first non-whitespace, non-comment token in the current file
	 */
	public boolean fileContentStart() {
		return filecontent_start;
	}
	
	public boolean isText(String s) {
		return text.equals(s);
	}
	
	public boolean isText(Token t) {
		return isText(t.text);
	}
	
	public boolean isEmpty() {
		return type == Type.EOF || type == Type.NEWLINE;
	}
	
	public Token withText(String s) {
		return new TokenBuilder(this).text(s).classify().toToken();
	}
	
	@Override
	public String toString() {
		return String.format("Token:%s[%s:%d:%d,'%s']", type, filename, line, col, text.replace("\n", "\\n"));
	}
	
	public String fileLineCol() {
		return String.format("%s:%d:%d", filename, line, col);
	}
	
	public static Type classify(String s) {
		if (s == null) return Type.UNKNOWN;
		if ("\n".equals(s)) return Type.NEWLINE;
		if (";".equals(s)) return Type.SEMICOLON;
		if ("{".equals(s)) return Type.OPEN_BRACE;
		if ("}".equals(s)) return Type.CLOSE_BRACE;
		if (KEYWORDS.contains(s)) return Type.KEYWORD;
		if (OPERATORS.contains(s)) return Type.OPERATOR;
		// these regexes are mainly to differentiate between int/float/identifier, not to exclude invalid literals
		if (s.matches("[a-zA-Z_][a-zA-Z0-9_]*|operator..?")) return Type.IDENTIFIER;
		if (s.matches("(?:0[xX])?[0-9A-Fa-f]+[uU]?")) return Type.INT_LITERAL;
		if (s.matches("[0-9\\.]+(?:[eE][+-]?[0-9]+)?[fF]?")) return Type.FLOAT_LITERAL;
		return Type.UNKNOWN;
	}
	
}
