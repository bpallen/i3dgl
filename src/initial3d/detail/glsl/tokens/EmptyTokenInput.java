package initial3d.detail.glsl.tokens;

import java.io.IOException;

import initial3d.detail.glsl.InfoLog;

public class EmptyTokenInput extends AbstractTokenInput {
	
	public EmptyTokenInput(InfoLog log_) {
		super(log_);
	}

	@Override
	protected Token nextTokenImpl() throws IOException {
		return Token.EOF;
	}
	
}
