package initial3d.detail.glsl.tokens;

import java.io.IOException;

public class StripEmptyTokenInput extends AbstractTokenInput {

	private TokenInput in;
	
	public StripEmptyTokenInput(TokenInput in_) {
		super(in_.infoLog());
		in = in_;
	}

	@Override
	protected Token nextTokenImpl() throws IOException {
		Token t = in.nextToken();
		while (t.type() != Token.Type.EOF && t.isEmpty()) t = in.nextToken();
		return t;
	}

}
