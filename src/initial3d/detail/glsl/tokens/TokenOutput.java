package initial3d.detail.glsl.tokens;

public interface TokenOutput {

	public void writeToken(Token t);
	
}
