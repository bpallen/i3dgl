package initial3d.detail.glsl.tokens;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import initial3d.detail.glsl.InfoLog;

public class CatTokenInput implements TokenInput {

	private LinkedList<TokenInput> inputs = new LinkedList<>();
	private RefillHandler refiller = null;
	private InfoLog log;
	
	public CatTokenInput(InfoLog log_, TokenInput ...ins) {
		log = Objects.requireNonNull(log_);
		for (TokenInput in : ins) {
			inputs.addLast(in);
		}
	}
	
	public CatTokenInput(TokenInput in0, TokenInput ...ins) {
		log = Objects.requireNonNull(in0).infoLog();
		inputs.addLast(in0);
		for (TokenInput in : ins) {
			inputs.addLast(in);
		}
	}
	
	@Override
	public InfoLog infoLog() {
		return log;
	}
	
	public void refillHandler(RefillHandler f) {
		refiller = f;
	}
	
	public List<TokenInput> inputs() {
		return Collections.unmodifiableList(inputs);
	}
	
	public TokenInput firstInput() {
		return inputs.peekFirst();
	}
	
	public TokenInput lastInput() {
		return inputs.peekLast();
	}
	
	public void prepend(TokenInput in) {
		if (in != null) inputs.addFirst(in);
	}
	
	public void append(TokenInput in) {
		if (in != null) inputs.addLast(in);
	}

	@Override
	public Token nextToken() throws IOException {
		peekToken();
		if (inputs.isEmpty()) return Token.EOF;
		return inputs.peekFirst().nextToken();
	}

	@Override
	public Token peekToken() throws IOException {
		while (true) {
			TokenInput in = inputs.peekFirst();
			// look for first non-eof input
			while (in != null && in.peekToken().type() == Token.Type.EOF) {
				inputs.pollFirst();
				in = inputs.peekFirst();
			}
			// had a non-eof input, peek token and return
			if (in != null) return in.peekToken();
			// did not have a non-eof input, get new input from refiller
			if (in == null && refiller != null) refiller.refill(this);
			if (inputs.isEmpty()) {
				// no refiller or refiller did not supply an input
				return Token.EOF;
			} else {
				// got input from refiller
				// need to do eof testing again
			}
		}
	}
	
	public static interface RefillHandler {
		
		public void refill(CatTokenInput cts) throws IOException;
		
	}
	
}
