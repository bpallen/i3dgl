package initial3d.detail.glsl;

import java.io.StringWriter;
import java.io.Writer;

public class StringInfoLog extends AbstractInfoLog {

	private StringWriter writer = new StringWriter();
	
	public StringInfoLog() {
		output(writer);
	}
	
	public String str() {
		return writer.toString();
	}

}
