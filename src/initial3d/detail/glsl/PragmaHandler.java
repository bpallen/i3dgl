package initial3d.detail.glsl;

import java.io.IOException;

import initial3d.detail.glsl.tokens.TokenInput;

public interface PragmaHandler {

	public TokenInput pragma(TokenInput in) throws IOException;
	
}
