package initial3d.detail.glsl;

import java.io.OutputStreamWriter;

public class SysInfoLog extends AbstractInfoLog {
	
	public SysInfoLog() {
		output(new OutputStreamWriter(System.out));
	}
	
}
