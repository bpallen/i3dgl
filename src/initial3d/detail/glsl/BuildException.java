package initial3d.detail.glsl;

public class BuildException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public BuildException() { }
	
	public BuildException(String msg) {
		super(msg);
	}

	public BuildException(Exception cause) {
		super(cause);
	}
}
