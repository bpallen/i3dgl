package initial3d.detail.glsl;

import initial3d.detail.glsl.tokens.Token;

import java.io.IOException;
import java.io.Writer;

public interface InfoLog {
	
	public static final InfoLog NULL = new AbstractInfoLog(new Writer() {

		@Override
		public void close() throws IOException {
			
		}

		@Override
		public void flush() throws IOException {
			
		}

		@Override
		public void write(char[] arg0, int arg1, int arg2) throws IOException {
			
		}
		
	}){};
	
	public static final InfoLog SYSOUT = new SysInfoLog();
	
	public boolean quiet();
	
	public void pushContext(String msg, Token t, boolean quiet);
	
	public void pushContext(String msg, Token t);
	
	public void popContext();

	public void showParentContext(boolean b);
	
	public void info(String msg, Token t);
	
	public void warning(String msg, Token t);
	
	public void error(String msg, Token t);
	
	public void detail(String msg, Token t);
	
	public void internalError(String msg, Token t);
	
	default void pushContext(String msg) {
		pushContext(msg, null);
	}
	
	default void info(String msg) {
		info(msg, null);
	}
	
	default void warning(String msg) {
		warning(msg, null);
	}
	
	default void error(String msg) {
		error(msg, null);
	}
	
	default void detail(String msg) {
		detail(msg, null);
	}
	
	default void internalError(String msg) {
		internalError(msg, null);
	}
	
	public int errorCount();
	
	public int warningCount();
	
}
