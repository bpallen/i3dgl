package initial3d.detail.glsl;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Stack;

import initial3d.detail.glsl.tokens.Token;

public class AbstractInfoLog implements InfoLog {
	
	protected static final String INDENT = "   ";
	
	protected class Context {
		public String msg;
		public Token t;
		public boolean quiet;
		public boolean show_parent = true;
		
		public Context(String msg_, Token t_, boolean quiet_) {
			msg = msg_;
			t = t_;
			quiet = quiet_;
		}
	}
	
	private PrintWriter out;
	private Stack<Context> contexts = new Stack<>();
	private int errors = 0;
	private int warnings = 0;
	
	public AbstractInfoLog() {
		
	}
	
	public AbstractInfoLog(Writer out_) {
		out = new PrintWriter(out_);
	}
	
	public PrintWriter output() {
		return out;
	}
	
	public void output(Writer out_) {
		out = new PrintWriter(out_);
	}
	
	@Override
	public boolean quiet() {
		for (Context c : contexts) {
			if (c.quiet) return true;
		}
		return false;
	}
	
	protected void printContext(String indent) {
		if (contexts.isEmpty()) return;
		for (int i = contexts.size(); i --> 0; ) {
			Context c = contexts.get(i);
			if (!c.show_parent) break;
			if (c.msg == null) continue;
			out.printf("%sWhile %s:\n", indent, c.msg);
			if (c.t != null) {
				printTokenDetail(indent + INDENT, c.t);
			}
		}
	}
	
	protected void printTokenDetail(String indent, Token t) {
		if (t == null) return;
		out.printf("%s%s:%d:%d : %s '%s'\n", indent, t.fileName(), t.line(), t.col(), t.type().friendlyName(), t.text().replace("\n", "\\n"));
		// print source line with column indicator
		if (t.lineText() != null) {
			out.println(indent + t.lineText().replace('\t', ' '));
			StringBuilder sb = new StringBuilder(indent);
			for (int i = 0; i < t.col() - 1; i++) {
				sb.append(' ');
			}
			sb.append('^');
			out.println(sb.toString());
		}
		// print original token details
		if (t.original() != null) {
			out.println(indent + "Substituted for:");
			printTokenDetail(indent, t.original());
		}
	}
	
	protected void printMessage(String title, String indent, String msg, Token t) {
		if (t == null) {
			out.printf("?:?:? %s : %s\n", title, msg);
		} else {
			msg = msg.replace("${text}", t.text());
			msg = msg.replace("${token}", t.toString());
			out.printf("%s:%d:%d : %s : %s\n", t.fileName(), t.line(), t.col(), title, msg);
		}
		printTokenDetail(indent, t);
		printContext(indent);
		out.flush();
	}
	
	@Override
	public void info(String msg, Token t) {
		if (quiet()) return;
		printMessage("Info", INDENT, msg, t);
	}

	@Override
	public void warning(String msg, Token t) {
		if (quiet()) return;
		warnings++;
		printMessage("Warning", INDENT, msg, t);
	}

	@Override
	public void error(String msg, Token t) {
		if (quiet()) return;
		errors++;
		printMessage("Error", INDENT, msg, t);
	}
	
	@Override
	public void detail(String msg, Token t) {
		if (quiet()) return;
		if (t != null) {
			msg = msg.replace("${text}", t.text());
			msg = msg.replace("${token}", t.toString());
		}
		out.println(" - " + msg);
		printTokenDetail(INDENT + INDENT, t);
		out.flush();
	}
	
	@Override
	public void internalError(String msg, Token t) {
		errors++;
		printMessage("INTERNAL ERROR", INDENT, msg, t);
	}
	
	@Override
	public void pushContext(String msg, Token t, boolean quiet) {
		contexts.push(new Context(msg, t, quiet));
	}
	
	@Override
	public void pushContext(String msg, Token t) {
		contexts.push(new Context(msg, t, false));
	}
	
	@Override
	public void popContext() {
		if (!contexts.isEmpty()) contexts.pop();
	}
	
	@Override
	public void showParentContext(boolean b) {
		if (!contexts.isEmpty()) contexts.lastElement().show_parent = b;
	}

	@Override
	public int errorCount() {
		return errors;
	}

	@Override
	public int warningCount() {
		return warnings;
	}
	
}
