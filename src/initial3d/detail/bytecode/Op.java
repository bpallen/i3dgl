package initial3d.detail.bytecode;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

public class Op {

	/**
	 * <p>nop (0)</p>
	 * <p>perform no operation</p>
	 * <p>Stack: <code>[No change]</code></p>
	 */
	public static final byte NOP = (byte) 0x00;

	/**
	 * <p>aconst_null (1)</p>
	 * <p>push a null reference onto the stack</p>
	 * <p>Stack: <code>-> null</code></p>
	 */
	public static final byte ACONST_NULL = (byte) 0x01;

	/**
	 * <p>iconst_m1 (2)</p>
	 * <p>load the int value 1 onto the stack</p>
	 * <p>Stack: <code>-> -1</code></p>
	 */
	public static final byte ICONST_M1 = (byte) 0x02;

	/**
	 * <p>iconst_0 (3)</p>
	 * <p>load the int value 0 onto the stack</p>
	 * <p>Stack: <code>-> 0</code></p>
	 */
	public static final byte ICONST_0 = (byte) 0x03;

	/**
	 * <p>iconst_1 (4)</p>
	 * <p>load the int value 1 onto the stack</p>
	 * <p>Stack: <code>-> 1</code></p>
	 */
	public static final byte ICONST_1 = (byte) 0x04;

	/**
	 * <p>iconst_2 (5)</p>
	 * <p>load the int value 2 onto the stack</p>
	 * <p>Stack: <code>-> 2</code></p>
	 */
	public static final byte ICONST_2 = (byte) 0x05;

	/**
	 * <p>iconst_3 (6)</p>
	 * <p>load the int value 3 onto the stack</p>
	 * <p>Stack: <code>-> 3</code></p>
	 */
	public static final byte ICONST_3 = (byte) 0x06;

	/**
	 * <p>iconst_4 (7)</p>
	 * <p>load the int value 4 onto the stack</p>
	 * <p>Stack: <code>-> 4</code></p>
	 */
	public static final byte ICONST_4 = (byte) 0x07;

	/**
	 * <p>iconst_5 (8)</p>
	 * <p>load the int value 5 onto the stack</p>
	 * <p>Stack: <code>-> 5</code></p>
	 */
	public static final byte ICONST_5 = (byte) 0x08;

	/**
	 * <p>lconst_0 (9)</p>
	 * <p>push the long 0 onto the stack</p>
	 * <p>Stack: <code>-> 0L</code></p>
	 */
	public static final byte LCONST_0 = (byte) 0x09;

	/**
	 * <p>lconst_1 (10)</p>
	 * <p>push the long 1 onto the stack</p>
	 * <p>Stack: <code>-> 1L</code></p>
	 */
	public static final byte LCONST_1 = (byte) 0x0A;

	/**
	 * <p>fconst_0 (11)</p>
	 * <p>push 0.0f on the stack</p>
	 * <p>Stack: <code>-> 0.0f</code></p>
	 */
	public static final byte FCONST_0 = (byte) 0x0B;

	/**
	 * <p>fconst_1 (12)</p>
	 * <p>push 1.0f on the stack</p>
	 * <p>Stack: <code>-> 1.0f</code></p>
	 */
	public static final byte FCONST_1 = (byte) 0x0C;

	/**
	 * <p>fconst_2 (13)</p>
	 * <p>push 2.0f on the stack</p>
	 * <p>Stack: <code>-> 2.0f</code></p>
	 */
	public static final byte FCONST_2 = (byte) 0x0D;

	/**
	 * <p>dconst_0 (14)</p>
	 * <p>push the constant 0.0 onto the stack</p>
	 * <p>Stack: <code>-> 0.0</code></p>
	 */
	public static final byte DCONST_0 = (byte) 0x0E;

	/**
	 * <p>dconst_1 (15)</p>
	 * <p>push the constant 1.0 onto the stack</p>
	 * <p>Stack: <code>-> 1.0</code></p>
	 */
	public static final byte DCONST_1 = (byte) 0x0F;

	/**
	 * <p>bipush (16)</p>
	 * <p>push a byte onto the stack as an integer value</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte BIPUSH = (byte) 0x10;

	/**
	 * <p>sipush (17)</p>
	 * <p>push a short onto the stack</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte SIPUSH = (byte) 0x11;

	/**
	 * <p>ldc (18)</p>
	 * <p>push a constant #index from a constant pool (String, int or float) onto the stack</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte LDC = (byte) 0x12;

	/**
	 * <p>ldc_w (19)</p>
	 * <p>push a constant #index from a constant pool (String, int or float) onto the stack (wide index is constructed as indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte LDC_W = (byte) 0x13;

	/**
	 * <p>ldc2_w (20)</p>
	 * <p>push a constant #index from a constant pool (double or long) onto the stack (wide index is constructed as indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte LDC2_W = (byte) 0x14;

	/**
	 * <p>iload (21)</p>
	 * <p>load an int value from a local variable #index</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte ILOAD = (byte) 0x15;

	/**
	 * <p>lload (22)</p>
	 * <p>load a long value from a local variable #index</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte LLOAD = (byte) 0x16;

	/**
	 * <p>fload (23)</p>
	 * <p>load a float value from a local variable #index</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte FLOAD = (byte) 0x17;

	/**
	 * <p>dload (24)</p>
	 * <p>load a double value from a local variable #index</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte DLOAD = (byte) 0x18;

	/**
	 * <p>aload (25)</p>
	 * <p>load a reference onto the stack from a local variable #index</p>
	 * <p>Stack: <code>-> objectref</code></p>
	 */
	public static final byte ALOAD = (byte) 0x19;

	/**
	 * <p>iload_0 (26)</p>
	 * <p>load an int value from local variable 0</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte ILOAD_0 = (byte) 0x1A;

	/**
	 * <p>iload_1 (27)</p>
	 * <p>load an int value from local variable 1</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte ILOAD_1 = (byte) 0x1B;

	/**
	 * <p>iload_2 (28)</p>
	 * <p>load an int value from local variable 2</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte ILOAD_2 = (byte) 0x1C;

	/**
	 * <p>iload_3 (29)</p>
	 * <p>load an int value from local variable 3</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte ILOAD_3 = (byte) 0x1D;

	/**
	 * <p>lload_0 (30)</p>
	 * <p>load a long value from a local variable 0</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte LLOAD_0 = (byte) 0x1E;

	/**
	 * <p>lload_1 (31)</p>
	 * <p>load a long value from a local variable 1</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte LLOAD_1 = (byte) 0x1F;

	/**
	 * <p>lload_2 (32)</p>
	 * <p>load a long value from a local variable 2</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte LLOAD_2 = (byte) 0x20;

	/**
	 * <p>lload_3 (33)</p>
	 * <p>load a long value from a local variable 3</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte LLOAD_3 = (byte) 0x21;

	/**
	 * <p>fload_0 (34)</p>
	 * <p>load a float value from local variable 0</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte FLOAD_0 = (byte) 0x22;

	/**
	 * <p>fload_1 (35)</p>
	 * <p>load a float value from local variable 1</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte FLOAD_1 = (byte) 0x23;

	/**
	 * <p>fload_2 (36)</p>
	 * <p>load a float value from local variable 2</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte FLOAD_2 = (byte) 0x24;

	/**
	 * <p>fload_3 (37)</p>
	 * <p>load a float value from local variable 3</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte FLOAD_3 = (byte) 0x25;

	/**
	 * <p>dload_0 (38)</p>
	 * <p>load a double from local variable 0</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte DLOAD_0 = (byte) 0x26;

	/**
	 * <p>dload_1 (39)</p>
	 * <p>load a double from local variable 1</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte DLOAD_1 = (byte) 0x27;

	/**
	 * <p>dload_2 (40)</p>
	 * <p>load a double from local variable 2</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte DLOAD_2 = (byte) 0x28;

	/**
	 * <p>dload_3 (41)</p>
	 * <p>load a double from local variable 3</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte DLOAD_3 = (byte) 0x29;

	/**
	 * <p>aload_0 (42)</p>
	 * <p>load a reference onto the stack from local variable 0</p>
	 * <p>Stack: <code>-> objectref</code></p>
	 */
	public static final byte ALOAD_0 = (byte) 0x2A;

	/**
	 * <p>aload_1 (43)</p>
	 * <p>load a reference onto the stack from local variable 1</p>
	 * <p>Stack: <code>-> objectref</code></p>
	 */
	public static final byte ALOAD_1 = (byte) 0x2B;

	/**
	 * <p>aload_2 (44)</p>
	 * <p>load a reference onto the stack from local variable 2</p>
	 * <p>Stack: <code>-> objectref</code></p>
	 */
	public static final byte ALOAD_2 = (byte) 0x2C;

	/**
	 * <p>aload_3 (45)</p>
	 * <p>load a reference onto the stack from local variable 3</p>
	 * <p>Stack: <code>-> objectref</code></p>
	 */
	public static final byte ALOAD_3 = (byte) 0x2D;

	/**
	 * <p>iaload (46)</p>
	 * <p>load an int from an array</p>
	 * <p>Stack: <code>arrayref, index -> value</code></p>
	 */
	public static final byte IALOAD = (byte) 0x2E;

	/**
	 * <p>laload (47)</p>
	 * <p>load a long from an array</p>
	 * <p>Stack: <code>arrayref, index -> value</code></p>
	 */
	public static final byte LALOAD = (byte) 0x2F;

	/**
	 * <p>faload (48)</p>
	 * <p>load a float from an array</p>
	 * <p>Stack: <code>arrayref, index -> value</code></p>
	 */
	public static final byte FALOAD = (byte) 0x30;

	/**
	 * <p>daload (49)</p>
	 * <p>load a double from an array</p>
	 * <p>Stack: <code>arrayref, index -> value</code></p>
	 */
	public static final byte DALOAD = (byte) 0x31;

	/**
	 * <p>aaload (50)</p>
	 * <p>load onto the stack a reference from an array</p>
	 * <p>Stack: <code>arrayref, index -> value</code></p>
	 */
	public static final byte AALOAD = (byte) 0x32;

	/**
	 * <p>baload (51)</p>
	 * <p>load a byte or Boolean value from an array</p>
	 * <p>Stack: <code>arrayref, index -> value</code></p>
	 */
	public static final byte BALOAD = (byte) 0x33;

	/**
	 * <p>caload (52)</p>
	 * <p>load a char from an array</p>
	 * <p>Stack: <code>arrayref, index -> value</code></p>
	 */
	public static final byte CALOAD = (byte) 0x34;

	/**
	 * <p>saload (53)</p>
	 * <p>load short from array</p>
	 * <p>Stack: <code>arrayref, index -> value</code></p>
	 */
	public static final byte SALOAD = (byte) 0x35;

	/**
	 * <p>istore (54)</p>
	 * <p>store int value into variable #index</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte ISTORE = (byte) 0x36;

	/**
	 * <p>lstore (55)</p>
	 * <p>store a long value in a local variable #index</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte LSTORE = (byte) 0x37;

	/**
	 * <p>fstore (56)</p>
	 * <p>store a float value into a local variable #index</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte FSTORE = (byte) 0x38;

	/**
	 * <p>dstore (57)</p>
	 * <p>store a double value into a local variable #index</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte DSTORE = (byte) 0x39;

	/**
	 * <p>astore (58)</p>
	 * <p>store a reference into a local variable #index</p>
	 * <p>Stack: <code>objectref -></code></p>
	 */
	public static final byte ASTORE = (byte) 0x3A;

	/**
	 * <p>istore_0 (59)</p>
	 * <p>store int value into variable 0</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte ISTORE_0 = (byte) 0x3B;

	/**
	 * <p>istore_1 (60)</p>
	 * <p>store int value into variable 1</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte ISTORE_1 = (byte) 0x3C;

	/**
	 * <p>istore_2 (61)</p>
	 * <p>store int value into variable 2</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte ISTORE_2 = (byte) 0x3D;

	/**
	 * <p>istore_3 (62)</p>
	 * <p>store int value into variable 3</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte ISTORE_3 = (byte) 0x3E;

	/**
	 * <p>lstore_0 (63)</p>
	 * <p>store a long value in a local variable 0</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte LSTORE_0 = (byte) 0x3F;

	/**
	 * <p>lstore_1 (64)</p>
	 * <p>store a long value in a local variable 1</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte LSTORE_1 = (byte) 0x40;

	/**
	 * <p>lstore_2 (65)</p>
	 * <p>store a long value in a local variable 2</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte LSTORE_2 = (byte) 0x41;

	/**
	 * <p>lstore_3 (66)</p>
	 * <p>store a long value in a local variable 3</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte LSTORE_3 = (byte) 0x42;

	/**
	 * <p>fstore_0 (67)</p>
	 * <p>store a float value into local variable 0</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte FSTORE_0 = (byte) 0x43;

	/**
	 * <p>fstore_1 (68)</p>
	 * <p>store a float value into local variable 1</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte FSTORE_1 = (byte) 0x44;

	/**
	 * <p>fstore_2 (69)</p>
	 * <p>store a float value into local variable 2</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte FSTORE_2 = (byte) 0x45;

	/**
	 * <p>fstore_3 (70)</p>
	 * <p>store a float value into local variable 3</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte FSTORE_3 = (byte) 0x46;

	/**
	 * <p>dstore_0 (71)</p>
	 * <p>store a double into local variable 0</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte DSTORE_0 = (byte) 0x47;

	/**
	 * <p>dstore_1 (72)</p>
	 * <p>store a double into local variable 1</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte DSTORE_1 = (byte) 0x48;

	/**
	 * <p>dstore_2 (73)</p>
	 * <p>store a double into local variable 2</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte DSTORE_2 = (byte) 0x49;

	/**
	 * <p>dstore_3 (74)</p>
	 * <p>store a double into local variable 3</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte DSTORE_3 = (byte) 0x4A;

	/**
	 * <p>astore_0 (75)</p>
	 * <p>store a reference into local variable 0</p>
	 * <p>Stack: <code>objectref -></code></p>
	 */
	public static final byte ASTORE_0 = (byte) 0x4B;

	/**
	 * <p>astore_1 (76)</p>
	 * <p>store a reference into local variable 1</p>
	 * <p>Stack: <code>objectref -></code></p>
	 */
	public static final byte ASTORE_1 = (byte) 0x4C;

	/**
	 * <p>astore_2 (77)</p>
	 * <p>store a reference into local variable 2</p>
	 * <p>Stack: <code>objectref -></code></p>
	 */
	public static final byte ASTORE_2 = (byte) 0x4D;

	/**
	 * <p>astore_3 (78)</p>
	 * <p>store a reference into local variable 3</p>
	 * <p>Stack: <code>objectref -></code></p>
	 */
	public static final byte ASTORE_3 = (byte) 0x4E;

	/**
	 * <p>iastore (79)</p>
	 * <p>store an int into an array</p>
	 * <p>Stack: <code>arrayref, index, value -></code></p>
	 */
	public static final byte IASTORE = (byte) 0x4F;

	/**
	 * <p>lastore (80)</p>
	 * <p>store a long to an array</p>
	 * <p>Stack: <code>arrayref, index, value -></code></p>
	 */
	public static final byte LASTORE = (byte) 0x50;

	/**
	 * <p>fastore (81)</p>
	 * <p>store a float in an array</p>
	 * <p>Stack: <code>arrayref, index, value -></code></p>
	 */
	public static final byte FASTORE = (byte) 0x51;

	/**
	 * <p>dastore (82)</p>
	 * <p>store a double into an array</p>
	 * <p>Stack: <code>arrayref, index, value -></code></p>
	 */
	public static final byte DASTORE = (byte) 0x52;

	/**
	 * <p>aastore (83)</p>
	 * <p>store into a reference in an array</p>
	 * <p>Stack: <code>arrayref, index, value -></code></p>
	 */
	public static final byte AASTORE = (byte) 0x53;

	/**
	 * <p>bastore (84)</p>
	 * <p>store a byte or Boolean value into an array</p>
	 * <p>Stack: <code>arrayref, index, value -></code></p>
	 */
	public static final byte BASTORE = (byte) 0x54;

	/**
	 * <p>castore (85)</p>
	 * <p>store a char into an array</p>
	 * <p>Stack: <code>arrayref, index, value -></code></p>
	 */
	public static final byte CASTORE = (byte) 0x55;

	/**
	 * <p>sastore (86)</p>
	 * <p>store short to array</p>
	 * <p>Stack: <code>arrayref, index, value -></code></p>
	 */
	public static final byte SASTORE = (byte) 0x56;

	/**
	 * <p>pop (87)</p>
	 * <p>discard the top value on the stack</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte POP = (byte) 0x57;

	/**
	 * <p>pop2 (88)</p>
	 * <p>discard the top two values on the stack (or one value, if it is a double or long)</p>
	 * <p>Stack: <code>{value2, value1} -></code></p>
	 */
	public static final byte POP2 = (byte) 0x58;

	/**
	 * <p>dup (89)</p>
	 * <p>duplicate the value on top of the stack</p>
	 * <p>Stack: <code>value -> value, value</code></p>
	 */
	public static final byte DUP = (byte) 0x59;

	/**
	 * <p>dup_x1 (90)</p>
	 * <p>insert a copy of the top value into the stack two values from the top. value1 and value2 must not be of the type double or long.</p>
	 * <p>Stack: <code>value2, value1 -> value1, value2, value1</code></p>
	 */
	public static final byte DUP_X1 = (byte) 0x5A;

	/**
	 * <p>dup_x2 (91)</p>
	 * <p>insert a copy of the top value into the stack two (if value2 is double or long it takes up the entry of value3, too) or three values (if value2 is neither double nor long) from the top</p>
	 * <p>Stack: <code>value3, value2, value1 -> value1, value3, value2, value1</code></p>
	 */
	public static final byte DUP_X2 = (byte) 0x5B;

	/**
	 * <p>dup2 (92)</p>
	 * <p>duplicate top two stack words (two values, if value1 is not double nor long; a single value, if value1 is double or long)</p>
	 * <p>Stack: <code>{value2, value1} -> {value2, value1}, {value2, value1}</code></p>
	 */
	public static final byte DUP2 = (byte) 0x5C;

	/**
	 * <p>dup2_x1 (93)</p>
	 * <p>duplicate two words and insert beneath third word (see explanation above)</p>
	 * <p>Stack: <code>value3, {value2, value1} -> {value2, value1}, value3, {value2, value1}</code></p>
	 */
	public static final byte DUP2_X1 = (byte) 0x5D;

	/**
	 * <p>dup2_x2 (94)</p>
	 * <p>duplicate two words and insert beneath fourth word</p>
	 * <p>Stack: <code>{value4, value3}, {value2, value1} -> {value2, value1}, {value4, value3}, {value2, value1}</code></p>
	 */
	public static final byte DUP2_X2 = (byte) 0x5E;

	/**
	 * <p>swap (95)</p>
	 * <p>swaps two top words on the stack (note that value1 and value2 must not be double or long)</p>
	 * <p>Stack: <code>value2, value1 -> value1, value2</code></p>
	 */
	public static final byte SWAP = (byte) 0x5F;

	/**
	 * <p>iadd (96)</p>
	 * <p>add two ints</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte IADD = (byte) 0x60;

	/**
	 * <p>ladd (97)</p>
	 * <p>add two longs</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LADD = (byte) 0x61;

	/**
	 * <p>fadd (98)</p>
	 * <p>add two floats</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte FADD = (byte) 0x62;

	/**
	 * <p>dadd (99)</p>
	 * <p>add two doubles</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte DADD = (byte) 0x63;

	/**
	 * <p>isub (100)</p>
	 * <p>int subtract</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte ISUB = (byte) 0x64;

	/**
	 * <p>lsub (101)</p>
	 * <p>subtract two longs</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LSUB = (byte) 0x65;

	/**
	 * <p>fsub (102)</p>
	 * <p>subtract two floats</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte FSUB = (byte) 0x66;

	/**
	 * <p>dsub (103)</p>
	 * <p>subtract a double from another</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte DSUB = (byte) 0x67;

	/**
	 * <p>imul (104)</p>
	 * <p>multiply two integers</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte IMUL = (byte) 0x68;

	/**
	 * <p>lmul (105)</p>
	 * <p>multiply two longs</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LMUL = (byte) 0x69;

	/**
	 * <p>fmul (106)</p>
	 * <p>multiply two floats</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte FMUL = (byte) 0x6A;

	/**
	 * <p>dmul (107)</p>
	 * <p>multiply two doubles</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte DMUL = (byte) 0x6B;

	/**
	 * <p>idiv (108)</p>
	 * <p>divide two integers</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte IDIV = (byte) 0x6C;

	/**
	 * <p>ldiv (109)</p>
	 * <p>divide two longs</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LDIV = (byte) 0x6D;

	/**
	 * <p>fdiv (110)</p>
	 * <p>divide two floats</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte FDIV = (byte) 0x6E;

	/**
	 * <p>ddiv (111)</p>
	 * <p>divide two doubles</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte DDIV = (byte) 0x6F;

	/**
	 * <p>irem (112)</p>
	 * <p>logical int remainder</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte IREM = (byte) 0x70;

	/**
	 * <p>lrem (113)</p>
	 * <p>remainder of division of two longs</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LREM = (byte) 0x71;

	/**
	 * <p>frem (114)</p>
	 * <p>get the remainder from a division between two floats</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte FREM = (byte) 0x72;

	/**
	 * <p>drem (115)</p>
	 * <p>get the remainder from a division between two doubles</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte DREM = (byte) 0x73;

	/**
	 * <p>ineg (116)</p>
	 * <p>negate int</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte INEG = (byte) 0x74;

	/**
	 * <p>lneg (117)</p>
	 * <p>negate a long</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte LNEG = (byte) 0x75;

	/**
	 * <p>fneg (118)</p>
	 * <p>negate a float</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte FNEG = (byte) 0x76;

	/**
	 * <p>dneg (119)</p>
	 * <p>negate a double</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte DNEG = (byte) 0x77;

	/**
	 * <p>ishl (120)</p>
	 * <p>int shift left</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte ISHL = (byte) 0x78;

	/**
	 * <p>lshl (121)</p>
	 * <p>bitwise shift left of a long value1 by int value2 positions</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LSHL = (byte) 0x79;

	/**
	 * <p>ishr (122)</p>
	 * <p>int arithmetic shift right</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte ISHR = (byte) 0x7A;

	/**
	 * <p>lshr (123)</p>
	 * <p>bitwise shift right of a long value1 by int value2 positions</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LSHR = (byte) 0x7B;

	/**
	 * <p>iushr (124)</p>
	 * <p>int logical shift right</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte IUSHR = (byte) 0x7C;

	/**
	 * <p>lushr (125)</p>
	 * <p>bitwise shift right of a long value1 by int value2 positions, unsigned</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LUSHR = (byte) 0x7D;

	/**
	 * <p>iand (126)</p>
	 * <p>perform a bitwise and on two integers</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte IAND = (byte) 0x7E;

	/**
	 * <p>land (127)</p>
	 * <p>bitwise and of two longs</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LAND = (byte) 0x7F;

	/**
	 * <p>ior (128)</p>
	 * <p>bitwise int or</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte IOR = (byte) 0x80;

	/**
	 * <p>lor (129)</p>
	 * <p>bitwise or of two longs</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LOR = (byte) 0x81;

	/**
	 * <p>ixor (130)</p>
	 * <p>int xor</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte IXOR = (byte) 0x82;

	/**
	 * <p>lxor (131)</p>
	 * <p>bitwise exclusive or of two longs</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LXOR = (byte) 0x83;

	/**
	 * <p>iinc (132)</p>
	 * <p>increment local variable #index by signed byte const</p>
	 * <p>Stack: <code>[No change]</code></p>
	 */
	public static final byte IINC = (byte) 0x84;

	/**
	 * <p>i2l (133)</p>
	 * <p>convert an int into a long</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte I2L = (byte) 0x85;

	/**
	 * <p>i2f (134)</p>
	 * <p>convert an int into a float</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte I2F = (byte) 0x86;

	/**
	 * <p>i2d (135)</p>
	 * <p>convert an int into a double</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte I2D = (byte) 0x87;

	/**
	 * <p>l2i (136)</p>
	 * <p>convert a long to a int</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte L2I = (byte) 0x88;

	/**
	 * <p>l2f (137)</p>
	 * <p>convert a long to a float</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte L2F = (byte) 0x89;

	/**
	 * <p>l2d (138)</p>
	 * <p>convert a long to a double</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte L2D = (byte) 0x8A;

	/**
	 * <p>f2i (139)</p>
	 * <p>convert a float to an int</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte F2I = (byte) 0x8B;

	/**
	 * <p>f2l (140)</p>
	 * <p>convert a float to a long</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte F2L = (byte) 0x8C;

	/**
	 * <p>f2d (141)</p>
	 * <p>convert a float to a double</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte F2D = (byte) 0x8D;

	/**
	 * <p>d2i (142)</p>
	 * <p>convert a double to an int</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte D2I = (byte) 0x8E;

	/**
	 * <p>d2l (143)</p>
	 * <p>convert a double to a long</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte D2L = (byte) 0x8F;

	/**
	 * <p>d2f (144)</p>
	 * <p>convert a double to a float</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte D2F = (byte) 0x90;

	/**
	 * <p>i2b (145)</p>
	 * <p>convert an int into a byte</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte I2B = (byte) 0x91;

	/**
	 * <p>i2c (146)</p>
	 * <p>convert an int into a character</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte I2C = (byte) 0x92;

	/**
	 * <p>i2s (147)</p>
	 * <p>convert an int into a short</p>
	 * <p>Stack: <code>value -> result</code></p>
	 */
	public static final byte I2S = (byte) 0x93;

	/**
	 * <p>lcmp (148)</p>
	 * <p>compare two longs values</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte LCMP = (byte) 0x94;

	/**
	 * <p>fcmpl (149)</p>
	 * <p>compare two floats</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte FCMPL = (byte) 0x95;

	/**
	 * <p>fcmpg (150)</p>
	 * <p>compare two floats</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte FCMPG = (byte) 0x96;

	/**
	 * <p>dcmpl (151)</p>
	 * <p>compare two doubles</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte DCMPL = (byte) 0x97;

	/**
	 * <p>dcmpg (152)</p>
	 * <p>compare two doubles</p>
	 * <p>Stack: <code>value1, value2 -> result</code></p>
	 */
	public static final byte DCMPG = (byte) 0x98;

	/**
	 * <p>ifeq (153)</p>
	 * <p>if value is 0, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte IFEQ = (byte) 0x99;

	/**
	 * <p>ifne (154)</p>
	 * <p>if value is not 0, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte IFNE = (byte) 0x9A;

	/**
	 * <p>iflt (155)</p>
	 * <p>if value is less than 0, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte IFLT = (byte) 0x9B;

	/**
	 * <p>ifge (156)</p>
	 * <p>if value is greater than or equal to 0, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte IFGE = (byte) 0x9C;

	/**
	 * <p>ifgt (157)</p>
	 * <p>if value is greater than 0, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte IFGT = (byte) 0x9D;

	/**
	 * <p>ifle (158)</p>
	 * <p>if value is less than or equal to 0, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte IFLE = (byte) 0x9E;

	/**
	 * <p>if_icmpeq (159)</p>
	 * <p>if ints are equal, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value1, value2 -></code></p>
	 */
	public static final byte IF_ICMPEQ = (byte) 0x9F;

	/**
	 * <p>if_icmpne (160)</p>
	 * <p>if ints are not equal, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value1, value2 -></code></p>
	 */
	public static final byte IF_ICMPNE = (byte) 0xA0;

	/**
	 * <p>if_icmplt (161)</p>
	 * <p>if value1 is less than value2, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value1, value2 -></code></p>
	 */
	public static final byte IF_ICMPLT = (byte) 0xA1;

	/**
	 * <p>if_icmpge (162)</p>
	 * <p>if value1 is greater than or equal to value2, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value1, value2 -></code></p>
	 */
	public static final byte IF_ICMPGE = (byte) 0xA2;

	/**
	 * <p>if_icmpgt (163)</p>
	 * <p>if value1 is greater than value2, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value1, value2 -></code></p>
	 */
	public static final byte IF_ICMPGT = (byte) 0xA3;

	/**
	 * <p>if_icmple (164)</p>
	 * <p>if value1 is less than or equal to value2, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value1, value2 -></code></p>
	 */
	public static final byte IF_ICMPLE = (byte) 0xA4;

	/**
	 * <p>if_acmpeq (165)</p>
	 * <p>if references are equal, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value1, value2 -></code></p>
	 */
	public static final byte IF_ACMPEQ = (byte) 0xA5;

	/**
	 * <p>if_acmpne (166)</p>
	 * <p>if references are not equal, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value1, value2 -></code></p>
	 */
	public static final byte IF_ACMPNE = (byte) 0xA6;

	/**
	 * <p>goto (167)</p>
	 * <p>goes to another instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>[no change]</code></p>
	 */
	public static final byte GOTO = (byte) 0xA7;

	/**
	 * <p>jsr (168)</p>
	 * <p>jump to subroutine at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2) and place the return address on the stack</p>
	 * <p>Stack: <code>-> address</code></p>
	 */
	public static final byte JSR = (byte) 0xA8;

	/**
	 * <p>ret (169)</p>
	 * <p>continue execution from address taken from a local variable #index (the asymmetry with jsr is intentional)</p>
	 * <p>Stack: <code>[No change]</code></p>
	 */
	public static final byte RET = (byte) 0xA9;

	/**
	 * <p>tableswitch (170)</p>
	 * <p>continue execution from an address in the table at offset index</p>
	 * <p>Stack: <code>index -></code></p>
	 */
	public static final byte TABLESWITCH = (byte) 0xAA;

	/**
	 * <p>lookupswitch (171)</p>
	 * <p>a target address is looked up from a table using a key and execution continues from the instruction at that address</p>
	 * <p>Stack: <code>key -></code></p>
	 */
	public static final byte LOOKUPSWITCH = (byte) 0xAB;

	/**
	 * <p>ireturn (172)</p>
	 * <p>return an integer from a method</p>
	 * <p>Stack: <code>value -> [empty]</code></p>
	 */
	public static final byte IRETURN = (byte) 0xAC;

	/**
	 * <p>lreturn (173)</p>
	 * <p>return a long value</p>
	 * <p>Stack: <code>value -> [empty]</code></p>
	 */
	public static final byte LRETURN = (byte) 0xAD;

	/**
	 * <p>freturn (174)</p>
	 * <p>return a float</p>
	 * <p>Stack: <code>value -> [empty]</code></p>
	 */
	public static final byte FRETURN = (byte) 0xAE;

	/**
	 * <p>dreturn (175)</p>
	 * <p>return a double from a method</p>
	 * <p>Stack: <code>value -> [empty]</code></p>
	 */
	public static final byte DRETURN = (byte) 0xAF;

	/**
	 * <p>areturn (176)</p>
	 * <p>return a reference from a method</p>
	 * <p>Stack: <code>objectref -> [empty]</code></p>
	 */
	public static final byte ARETURN = (byte) 0xB0;

	/**
	 * <p>return (177)</p>
	 * <p>return void from method</p>
	 * <p>Stack: <code>-> [empty]</code></p>
	 */
	public static final byte RETURN = (byte) 0xB1;

	/**
	 * <p>getstatic (178)</p>
	 * <p>get a static field value of a class, where the field is identified by field reference in the constant pool index (index1 << 8 + index2)</p>
	 * <p>Stack: <code>-> value</code></p>
	 */
	public static final byte GETSTATIC = (byte) 0xB2;

	/**
	 * <p>putstatic (179)</p>
	 * <p>set static field to value in a class, where the field is identified by a field reference index in constant pool (indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte PUTSTATIC = (byte) 0xB3;

	/**
	 * <p>getfield (180)</p>
	 * <p>get a field value of an object objectref, where the field is identified by field reference in the constant pool index (index1 << 8 + index2)</p>
	 * <p>Stack: <code>objectref -> value</code></p>
	 */
	public static final byte GETFIELD = (byte) 0xB4;

	/**
	 * <p>putfield (181)</p>
	 * <p>set field to value in an object objectref, where the field is identified by a field reference index in constant pool (indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>objectref, value -></code></p>
	 */
	public static final byte PUTFIELD = (byte) 0xB5;

	/**
	 * <p>invokevirtual (182)</p>
	 * <p>invoke virtual method on object objectref and puts the result on the stack (might be void); the method is identified by method reference index in constant pool (indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>objectref, [arg1, arg2, ...] -> result</code></p>
	 */
	public static final byte INVOKEVIRTUAL = (byte) 0xB6;

	/**
	 * <p>invokespecial (183)</p>
	 * <p>invoke instance method on object objectref and puts the result on the stack (might be void); the method is identified by method reference index in constant pool (indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>objectref, [arg1, arg2, ...] -> result</code></p>
	 */
	public static final byte INVOKESPECIAL = (byte) 0xB7;

	/**
	 * <p>invokestatic (184)</p>
	 * <p>invoke a static method and puts the result on the stack (might be void); the method is identified by method reference index in constant pool (indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>[arg1, arg2, ...] -> result</code></p>
	 */
	public static final byte INVOKESTATIC = (byte) 0xB8;

	/**
	 * <p>invokeinterface (185)</p>
	 * <p>invokes an interface method on object objectref and puts the result on the stack (might be void); the interface method is identified by method reference index in constant pool (indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>objectref, [arg1, arg2, ...] -> result</code></p>
	 */
	public static final byte INVOKEINTERFACE = (byte) 0xB9;

	/**
	 * <p>invokedynamic (186)</p>
	 * <p>invokes a dynamic method and puts the result on the stack (might be void); the method is identified by method reference index in constant pool (indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>[arg1, [arg2 ...]] -> result</code></p>
	 */
	public static final byte INVOKEDYNAMIC = (byte) 0xBA;

	/**
	 * <p>new (187)</p>
	 * <p>create new object of type identified by class reference in constant pool index (indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>-> objectref</code></p>
	 */
	public static final byte NEW = (byte) 0xBB;

	/**
	 * <p>newarray (188)</p>
	 * <p>create new array with count elements of primitive type identified by atype</p>
	 * <p>Stack: <code>count -> arrayref</code></p>
	 */
	public static final byte NEWARRAY = (byte) 0xBC;

	/**
	 * <p>anewarray (189)</p>
	 * <p>create a new array of references of length count and component type identified by the class reference index (indexbyte1 << 8 + indexbyte2) in the constant pool</p>
	 * <p>Stack: <code>count -> arrayref</code></p>
	 */
	public static final byte ANEWARRAY = (byte) 0xBD;

	/**
	 * <p>arraylength (190)</p>
	 * <p>get the length of an array</p>
	 * <p>Stack: <code>arrayref -> length</code></p>
	 */
	public static final byte ARRAYLENGTH = (byte) 0xBE;

	/**
	 * <p>athrow (191)</p>
	 * <p>throws an error or exception (notice that the rest of the stack is cleared, leaving only a reference to the Throwable)</p>
	 * <p>Stack: <code>objectref -> [empty], objectref</code></p>
	 */
	public static final byte ATHROW = (byte) 0xBF;

	/**
	 * <p>checkcast (192)</p>
	 * <p>checks whether an objectref is of a certain type, the class reference of which is in the constant pool at index (indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>objectref -> objectref</code></p>
	 */
	public static final byte CHECKCAST = (byte) 0xC0;

	/**
	 * <p>instanceof (193)</p>
	 * <p>determines if an object objectref is of a given type, identified by class reference index in constant pool (indexbyte1 << 8 + indexbyte2)</p>
	 * <p>Stack: <code>objectref -> result</code></p>
	 */
	public static final byte INSTANCEOF = (byte) 0xC1;

	/**
	 * <p>monitorenter (194)</p>
	 * <p>enter monitor for object ("grab the lock"  start of synchronized() section)</p>
	 * <p>Stack: <code>objectref -></code></p>
	 */
	public static final byte MONITORENTER = (byte) 0xC2;

	/**
	 * <p>monitorexit (195)</p>
	 * <p>exit monitor for object ("release the lock"  end of synchronized() section)</p>
	 * <p>Stack: <code>objectref -></code></p>
	 */
	public static final byte MONITOREXIT = (byte) 0xC3;

	/**
	 * <p>wide (196)</p>
	 * <p>execute opcode, where opcode is either iload, fload, aload, lload, dload, istore, fstore, astore, lstore, dstore, or ret, but assume the index is 16 bit; or execute iinc, where the index is 16 bits and the constant to increment by is a signed 16 bit short</p>
	 * <p>Stack: <code>[same as for corresponding instructions]</code></p>
	 */
	public static final byte WIDE = (byte) 0xC4;

	/**
	 * <p>multianewarray (197)</p>
	 * <p>create a new array of dimensions dimensions with elements of type identified by class reference in constant pool index (indexbyte1 << 8 + indexbyte2); the sizes of each dimension is identified by count1, [count2, etc.]</p>
	 * <p>Stack: <code>count1, [count2,...] -> arrayref</code></p>
	 */
	public static final byte MULTIANEWARRAY = (byte) 0xC5;

	/**
	 * <p>ifnull (198)</p>
	 * <p>if value is null, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte IFNULL = (byte) 0xC6;

	/**
	 * <p>ifnonnull (199)</p>
	 * <p>if value is not null, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)</p>
	 * <p>Stack: <code>value -></code></p>
	 */
	public static final byte IFNONNULL = (byte) 0xC7;

	/**
	 * <p>goto_w (200)</p>
	 * <p>goes to another instruction at branchoffset (signed int constructed from unsigned bytes branchbyte1 << 24 + branchbyte2 << 16 + branchbyte3 << 8 + branchbyte4)</p>
	 * <p>Stack: <code>[no change]</code></p>
	 */
	public static final byte GOTO_W = (byte) 0xC8;

	/**
	 * <p>jsr_w (201)</p>
	 * <p>jump to subroutine at branchoffset (signed int constructed from unsigned bytes branchbyte1 << 24 + branchbyte2 << 16 + branchbyte3 << 8 + branchbyte4) and place the return address on the stack</p>
	 * <p>Stack: <code>-> address</code></p>
	 */
	public static final byte JSR_W = (byte) 0xC9;

	/**
	 * <p>breakpoint (202)</p>
	 * <p>reserved for breakpoints in Java debuggers; should not appear in any class file</p>
	 * <p>Stack: <code></code></p>
	 */
	public static final byte BREAKPOINT = (byte) 0xCA;

	/**
	 * <p>impdep1 (254)</p>
	 * <p>reserved for implementation-dependent operations within debuggers; should not appear in any class file</p>
	 * <p>Stack: <code></code></p>
	 */
	public static final byte IMPDEP1 = (byte) 0xFE;

	/**
	 * <p>impdep2 (255)</p>
	 * <p>reserved for implementation-dependent operations within debuggers; should not appear in any class file</p>
	 * <p>Stack: <code></code></p>
	 */
	public static final byte IMPDEP2 = (byte) 0xFF;

	private final Scope scope;
	protected final byte opcode;
	private Constant constant = null;
	private byte[] immediate = null;

	public Op(Scope scope_, byte opcode_) {
		scope = scope_;
		opcode = opcode_;
	}

	public Op(Scope scope_, byte opcode_, Constant constant_) {
		scope = scope_;
		opcode = opcode_;
		constant = Objects.requireNonNull(constant_);
	}

	public Op(Scope scope_, byte opcode_, byte[] immediate_) {
		scope = scope_;
		opcode = opcode_;
		immediate = Objects.requireNonNull(immediate_);
	}

	public Op(Scope scope_, byte opcode_, Constant constant_, byte[] immediate_) {
		scope = scope_;
		opcode = opcode_;
		constant = Objects.requireNonNull(constant_);
		immediate = Objects.requireNonNull(immediate_);
	}

	public Scope scope() {
		return scope;
	}
	
	public void label(String l) {
		scope.label(this, l);
	}
	
	public int size(ClassBuilder cb, MethodBuilder mb) {
		return 1 + (constant == null ? 0 : 2) + (immediate == null ? 0 : immediate.length);
	}

	public void build(ClassBuilder cb, MethodBuilder mb, int address, DataOutput out) throws IOException {
		int ci = cb.addConstant(constant);
		out.writeByte(opcode);
		if (ci > 0) {
			out.writeShort(ci);
		}
		if (immediate != null) {
			out.write(immediate);
		}
	}

	public static class LoadConstant extends Op {

		private Constant constant;

		public LoadConstant(Scope scope_, Constant constant_) {
			// opcode determined dynamically
			super(scope_, NOP);
			constant = Objects.requireNonNull(constant_);
		}

		private byte decideOp(ClassBuilder cb) {
			int ci = cb.addConstant(constant);
			if (constant instanceof Constant.Long) return LDC2_W;
			if (constant instanceof Constant.Double) return LDC2_W;
			if (ci < 256) {
				if (constant instanceof Constant.StringRef) return LDC;
				if (constant instanceof Constant.Integer) return LDC;
				if (constant instanceof Constant.Float) return LDC;
			} else {
				if (constant instanceof Constant.StringRef) return LDC_W;
				if (constant instanceof Constant.Integer) return LDC_W;
				if (constant instanceof Constant.Float) return LDC_W;
			}
			throw new RuntimeException("bad Constant type for LDC*");
		}

		@Override
		public int size(ClassBuilder cb, MethodBuilder mb) {
			switch (decideOp(cb)) {
			case LDC:
				return 2;
			case LDC_W:
			case LDC2_W:
				return 3;
			default:
				throw new RuntimeException("decided bad LDC* op");
			}
		}

		@Override
		public void build(ClassBuilder cb, MethodBuilder mb, int address, DataOutput out) throws IOException {
			int ci = cb.addConstant(constant);
			byte op = decideOp(cb);
			out.writeByte(op);
			if (op == LDC) {
				out.writeByte(ci);
			} else {
				out.writeShort(ci);
			}
		}

	}

	public static class Branch extends Op {

		private String dest = null;

		public Branch(Scope scope_, byte opcode_, String dest_) {
			super(scope_, opcode_);
			dest = Objects.requireNonNull(dest_);
		}

		@Override
		public int size(ClassBuilder cb, MethodBuilder mb) {
			return 3;
		}

		@Override
		public void build(ClassBuilder cb, MethodBuilder mb, int address, DataOutput out) throws IOException {
			// resolve dest label based on scope at build time
			int offset = mb.address(cb, scope().label(dest)) - address;
			out.writeByte(opcode);
			out.writeShort(offset);
		}

	}

}
