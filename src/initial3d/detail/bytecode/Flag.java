package initial3d.detail.bytecode;

// this is the combined set of field, method and class access flags
public enum Flag {
	
	PUBLIC(0x1),
	PRIVATE(0x2),
	PROTECTED(0x4),
	STATIC(0x8),
	FINAL(0x10),
	SYNCHRONIZED(0x20),
	SUPER(0x20),
	VOLATILE(0x40),
	BRIDGE(0x40),
	TRANSIENT(0x80),
	VARARGS(0x80),
	NATIVE(0x100),
	INTERFACE(0x200),
	ABSTRACT(0x400),
	STRICT(0x800),
	SYNTHETIC(0x1000),
	ANNOTATION(0x2000),
	ENUM(0x4000);
	
	int flag;
	
	private Flag(int flag_) {
		flag = flag_;
	}
	
	public int flag() {
		return flag;
	}
	
}
