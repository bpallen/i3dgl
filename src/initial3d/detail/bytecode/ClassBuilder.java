package initial3d.detail.bytecode;

import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import initial3d.detail.StringUtil;
import initial3d.detail.Util;

public class ClassBuilder {
	
	// version 50? (jre6) is last version before StackMapTable is required
	private int version_major = 48;
	private int version_minor = 0;
	
	private Constant.ClassRef superclass = null;
	private Constant.ClassRef thisclass = null;
	private int accessflags = 0;
	private List<Constant.ClassRef> interfaces = new ArrayList<Constant.ClassRef>();
	private List<Constant> constants = new ArrayList<Constant>();
	private List<FieldBuilder> fields = new ArrayList<FieldBuilder>();
	private List<MethodBuilder> methods = new ArrayList<MethodBuilder>();
	private Map<String, String> substitutions = new HashMap<String, String>();
	
	// built class
	private byte[] built_clsbytes;
	private Class<?> built_cls;
	
	public ClassBuilder() {
		// there isn't a zero index 
		constants.add(null);
		declareSuperclass(Object.class);
		thisclass = new Constant.ClassRef("${thisclass}");
		addConstant(thisclass);
	}
	
	public int addConstant(Constant c) {
		if (c == null) return 0;
		int i = constants.indexOf(c);
		if (i >= 0) return i;
		i = constants.size();
		constants.add(c);
		for (int j = 1; j < c.slots(); j++) {
			// long and double take 2 'slots'
			constants.add(null);
		}
		return i;
	}
	
	public int addConstantUTF(String s) {
		return addConstant(new Constant.UTF(s));
	}
	
	public void addUTFSubstitution(String key, String val) {
		substitutions.put(key, val);
	}
	
	public String getUTFSubstitution(String key) {
		return substitutions.get(key);
	}

	public void declareName(String name) {
		addUTFSubstitution("thisclass", name.replace('.', '/'));
	}
	
	public void declareNameUnique(String name) {
		declareName(name + "_" + StringUtil.randomID(16));
	}
	
	public void declareFlag(Flag a) {
		accessflags |= a.flag();
	}
	
	public void declareSuperclass(Constant.ClassRef cls) {
		superclass = Objects.requireNonNull(cls);
		addConstant(superclass);
	}
	
	public void declareSuperclass(Class<?> cls) {
		declareSuperclass(new Constant.ClassRef(cls));
	}
	
	public void declareSuperclass(String clsname) {
		declareSuperclass(new Constant.ClassRef(clsname));
	}
	
	public void declareInterface(Constant.ClassRef cls) {
		Objects.requireNonNull(cls);
		if (interfaces.contains(cls)) return;
		interfaces.add(cls);
		addConstant(cls);
	}
	
	public void declareInterface(Class<?> cls) {
		Objects.requireNonNull(cls);
		if (!cls.isInterface()) throw new RuntimeException(cls.getCanonicalName() + " is not an interface");
		declareInterface(new Constant.ClassRef(cls));
	}
	
	public void declareInterface(String clsname) {
		declareInterface(new Constant.ClassRef(clsname));
	}
	
	public FieldBuilder declareField(FieldBuilder fb) {
		fields.add(Objects.requireNonNull(fb));
		return fb;
	}
	
	public FieldBuilder declareField() {
		return declareField(new FieldBuilder());
	}
	
	public MethodBuilder declareMethod(MethodBuilder mb) {
		methods.add(Objects.requireNonNull(mb));
		return mb;
	}
	
	public MethodBuilder declareMethod() {
		return declareMethod(new MethodBuilder());
	}
	
	public MethodBuilder method(String name, Class<?> rettype, Class<?> ...argtypes) {
		Constant.NameAndType nt = Constant.NameAndType.forMethod(name, rettype, argtypes);
		for (MethodBuilder mb : methods) {
			if (mb.nameAndType().equals(nt)) return mb;
		}
		return null;
	}
	
	public Constant.ClassRef ref() {
		return thisclass;
	}
	
	public Class<?> build() {
		Objects.requireNonNull(thisclass);
		
		built_cls = null;
		built_clsbytes = null;
		
		byte[] clsbytes = null;
		
		try {
			// build body of class before constant table; serializing fields/methods/etc can add constants
			ByteArrayOutputStream bodydata = new ByteArrayOutputStream();
			DataOutput bodyout = new DataOutputStream(bodydata);
			bodyout.writeShort(accessflags);
			bodyout.writeShort(addConstant(thisclass));
			bodyout.writeShort(addConstant(superclass));
			
			// interfaces
			bodyout.writeShort(interfaces.size());
			for (Constant.ClassRef ic : interfaces) {
				bodyout.writeShort(addConstant(ic));
			}
			
			// fields
			bodyout.writeShort(fields.size());
			for (FieldBuilder fb : fields) {
				fb.build(this, bodyout);
			}
			
			// methods
			bodyout.writeShort(methods.size());
			for (MethodBuilder mb : methods) {
				mb.build(this, bodyout);
			}
			
			// attributes (none)
			bodyout.writeShort(0);
			
			// build constant table before class; serializing constants can add more constants
			ByteArrayOutputStream constdata = new ByteArrayOutputStream();
			DataOutput constout = new DataOutputStream(constdata);
			// 1 is first index
			for (int i = 1; i < constants.size(); i++) {
				// we _cant_ use for-each here
				Constant c = constants.get(i);
				//System.out.println("constant " + i + " : " + (c == null ? "" : c.getClass()));
				//System.out.println(c);
				if (c != null) {
					c.build(this, constout);
				} else {
					// the slot must be valid but is considered unusable
					// ie - the index must be allocated but should be skipped when writing
					//constants.get(i - 1).build(this, constout);
				}
			}
			
			// build full class
			ByteArrayOutputStream clsdata = new ByteArrayOutputStream();
			DataOutput clsout = new DataOutputStream(clsdata);
			clsout.writeInt(0xCAFEBABE);
			clsout.writeShort(version_minor);
			clsout.writeShort(version_major);
			
			// all constants accounted for now
			clsout.writeShort(constants.size());
			clsout.write(constdata.toByteArray());
			
			// add body to class
			clsout.write(bodydata.toByteArray());
			
			// ... done
			clsbytes = clsdata.toByteArray();
			
		} catch (IOException e) {
			// shouldnt happen; checked exceptions suck
			throw new AssertionError(e);
		}
		
		String clsname = getUTFSubstitution("thisclass").replace('/', '.');
		Class<?> cls = loader.installClass(clsname, clsbytes);
		try {
			loader.loadClass(clsname);
		} catch (ClassNotFoundException e) {
			throw new AssertionError(e);
		}
		built_clsbytes = clsbytes;
		built_cls = cls;
		return cls;
		
	}
	
	public Class<?> builtClass() {
		return built_cls;
	}
	
	public byte[] builtClassBytes() {
		return built_clsbytes;
	}
	
	private class DynamicClassLoader extends ClassLoader {
		
		public synchronized Class<?> installClass(String name, byte[] clsbytes) {
			Objects.requireNonNull(name);
			// FIXME this is an ugly hack for dynamically generated classes that reference each other
			try {
				ClassLoader loader0 = getClass().getClassLoader();
				Method m = ClassLoader.class.getDeclaredMethod("defineClass", String.class, byte[].class, int.class, int.class);
				m.setAccessible(true);
				Class<?> cls = (Class<?>) m.invoke(loader0, name, clsbytes, 0, clsbytes.length);
				return cls;
			} catch (Exception e) {
				throw new AssertionError(e);
			}
			
			//return defineClass(name, clsbytes, 0, clsbytes.length);
			//return UnsafeUtil.unsafe.defineClass(null, clsbytes, 0, clsbytes.length, null, null);
		}
		
		@Override
		public synchronized Class<?> findClass(String name) {
			
			return null;
		}
		
	}
	
	private DynamicClassLoader loader = new DynamicClassLoader();
	
}
