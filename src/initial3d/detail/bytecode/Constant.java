package initial3d.detail.bytecode;

import java.io.DataOutput;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Constant {

	public abstract void build(ClassBuilder cb, DataOutput out) throws IOException;
	
	public int slots() {
		return 1;
	}
	
	@Override
	public abstract int hashCode();
	
	@Override
	public abstract boolean equals(Object other);
	
	public static class UTF extends Constant {

		private String val;
		
		public UTF(String val_) {
			val = Objects.requireNonNull(val_);
		}
		
		@Override
		public void build(ClassBuilder cb, DataOutput out) throws IOException {
			out.writeByte(1);
			
			// check substitutions, eg "foo${mykey}bar" -> "fooMYVALUEbar"
			Pattern pat = Pattern.compile("\\$\\{(\\w*)\\}");
			StringBuffer sb = new StringBuffer(val);
			boolean done = true;
			
			// loop to allow recursive substitution
			do {
				// match against result of last iteration
				Matcher m = pat.matcher(sb.toString());
				sb.setLength(0);
				done = true;
				// loop through substitution fields
				while (m.find()) {
					String key = m.group(1);
					String rep = cb.getUTFSubstitution(key);
					m.appendReplacement(sb, Matcher.quoteReplacement(rep == null ? "" : rep));
					// replacement happened, retry
					done = false;
				}
				m.appendTail(sb);
			} while (!done);
			
			// replace escaped '$', '{', '}'
			String val2 = sb.toString().replace("$$", "$").replace("{{", "{").replace("}}", "}");
			
			// TODO is this string representation actually compatible with the class format?
			out.writeUTF(val2);
			
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((val == null) ? 0 : val.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			UTF other = (UTF) obj;
			if (val == null) {
				if (other.val != null) return false;
			} else if (!val.equals(other.val)) return false;
			return true;
		}
		
		@Override
		public String toString() {
			return val;
		}
		
	}
	
	public static class Integer extends Constant {

		private int val;
		
		public Integer(int val_) {
			val = val_;
		}
		
		@Override
		public void build(ClassBuilder cb, DataOutput out) throws IOException {
			out.writeByte(3);
			out.writeInt(val);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + val;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			Integer other = (Integer) obj;
			if (val != other.val) return false;
			return true;
		}
		
	}
	
	public static class Float extends Constant {

		private float val;
		
		public Float(float val_) {
			val = val_;
		}
		
		@Override
		public void build(ClassBuilder cb, DataOutput out) throws IOException {
			out.writeByte(4);
			out.writeFloat(val);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + java.lang.Float.floatToIntBits(val);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			Float other = (Float) obj;
			if (java.lang.Float.floatToIntBits(val) != java.lang.Float.floatToIntBits(other.val)) return false;
			return true;
		}
		
	}
	
	public static class Long extends Constant {

		private long val;
		
		public Long(long val_) {
			val = val_;
		}
		
		@Override
		public int slots() {
			return 2;
		}
		
		@Override
		public void build(ClassBuilder cb, DataOutput out) throws IOException {
			out.writeByte(5);
			out.writeLong(val);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (val ^ (val >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			Long other = (Long) obj;
			if (val != other.val) return false;
			return true;
		}
		
	}
	
	public static class Double extends Constant {

		private double val;
		
		public Double(double val_) {
			val = val_;
		}
		
		@Override
		public int slots() {
			return 2;
		}
		
		@Override
		public void build(ClassBuilder cb, DataOutput out) throws IOException {
			out.writeByte(6);
			out.writeDouble(val);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = java.lang.Double.doubleToLongBits(val);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			Double other = (Double) obj;
			if (java.lang.Double.doubleToLongBits(val) != java.lang.Double.doubleToLongBits(other.val)) return false;
			return true;
		}
		
	}
	
	public static class ClassRef extends Constant {

		private UTF name;
		
		public ClassRef(String name_) {
			name = new UTF(name_.replace('.', '/'));
		}
		
		public ClassRef(Class<?> c) {
			name = new UTF(c.getName().replace('.', '/'));
		}
		
		@Override
		public void build(ClassBuilder cb, DataOutput out) throws IOException {
			int i = cb.addConstant(name);
			out.writeByte(7);
			out.writeShort(i);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			ClassRef other = (ClassRef) obj;
			if (name == null) {
				if (other.name != null) return false;
			} else if (!name.equals(other.name)) return false;
			return true;
		}
		
	}
	
	public static class StringRef extends Constant {

		private UTF val;
		
		public StringRef(String val_) {
			val = new UTF(val_);
		}
		
		@Override
		public void build(ClassBuilder cb, DataOutput out) throws IOException {
			int i = cb.addConstant(val);
			out.writeByte(8);
			out.writeShort(i);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((val == null) ? 0 : val.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			StringRef other = (StringRef) obj;
			if (val == null) {
				if (other.val != null) return false;
			} else if (!val.equals(other.val)) return false;
			return true;
		}
		
	}
	
	public static abstract class AbstractMemberRef extends Constant {
		
		protected ClassRef cls;
		protected NameAndType member;
		
		public AbstractMemberRef(ClassRef cls_, NameAndType member_) {
			cls = Objects.requireNonNull(cls_);
			member = Objects.requireNonNull(member_);
		}
		
		protected abstract int tag();
		
		@Override
		public void build(ClassBuilder cb, DataOutput out) throws IOException {
			int ci = cb.addConstant(cls);
			int mi = cb.addConstant(member);
			out.writeByte(tag());
			out.writeShort(ci);
			out.writeShort(mi);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((cls == null) ? 0 : cls.hashCode());
			result = prime * result + ((member == null) ? 0 : member.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			AbstractMemberRef other = (AbstractMemberRef) obj;
			if (cls == null) {
				if (other.cls != null) return false;
			} else if (!cls.equals(other.cls)) return false;
			if (member == null) {
				if (other.member != null) return false;
			} else if (!member.equals(other.member)) return false;
			return true;
		}
		
	}
	
	public static class FieldRef extends AbstractMemberRef {
		
		public FieldRef(ClassRef cls_, NameAndType field_) {
			super(cls_, field_);
		}

		@Override
		protected int tag() {
			return 9;
		}
		
		public static FieldRef reflect(Class<?> cls, String fname) throws NoSuchFieldException {
			Field f = cls.getDeclaredField(fname);
			return new FieldRef(new ClassRef(cls), NameAndType.forField(fname, f.getType()));
		}
	}
	
	public static class MethodRef extends AbstractMemberRef {
		
		public MethodRef(ClassRef cls_, NameAndType method_) {
			super(cls_, method_);
		}

		@Override
		protected int tag() {
			return 10;
		}
		
		public static MethodRef reflect(Class<?> cls, String mname, Class<?> rettype, Class<?> ...argtypes) throws NoSuchMethodException {
			Objects.requireNonNull(mname);
			if ("<init>".equals(mname)) {
				cls.getDeclaredConstructor(argtypes);
				if (!void.class.equals(rettype)) throw new NoSuchMethodException("Constructor can only have void return type.");
				return new MethodRef(new ClassRef(cls), NameAndType.forMethod("<init>", void.class, argtypes));
			} else {
				Method m = cls.getDeclaredMethod(mname, argtypes);
				if (!m.getReturnType().equals(rettype)) throw new NoSuchMethodException("Method does not have specified return type.");
				return new MethodRef(new ClassRef(cls), NameAndType.forMethod(mname, m.getReturnType(), argtypes));
			}
		}
		
	}
	
	public static class InterfaceMethodRef extends AbstractMemberRef {

		public InterfaceMethodRef(ClassRef cls_, NameAndType method_) {
			super(cls_, method_);
		}

		@Override
		protected int tag() {
			return 11;
		}
		
	}
	
	public static class NameAndType extends Constant {

		private UTF name;
		private UTF desc;
		
		public NameAndType(UTF name_, UTF desc_) {
			name = Objects.requireNonNull(name_);
			desc = Objects.requireNonNull(desc_);
		}
		
		public NameAndType(String name_, String desc_) {
			name = new UTF(name_);
			desc = new UTF(desc_);
		}
		
		public UTF name() {
			return name;
		}
		
		public UTF desc() {
			return desc;
		}
		
		public static String classTypeDescriptor(Class<?> cls) {
			if (cls == null) return "";
			if (void.class.equals(cls)) return "V";
			if (boolean.class.equals(cls)) return "Z";
			if (byte.class.equals(cls)) return "B";
			if (char.class.equals(cls)) return "C";
			if (short.class.equals(cls)) return "S";
			if (int.class.equals(cls)) return "I";
			if (long.class.equals(cls)) return "J";
			if (float.class.equals(cls)) return "F";
			if (double.class.equals(cls)) return "D";
			if (cls.isArray()) return cls.getName().replace('.', '/');
			return ("L" + cls.getName() + ";").replace('.', '/');
		}
		
		public static String methodTypeDescriptor(Class<?> rettype, Class<?> ...argtypes) {
			StringBuilder sb = new StringBuilder();
			sb.append("(");
			for (Class<?> c : argtypes) {
				sb.append(classTypeDescriptor(c));
			}
			sb.append(")");
			sb.append(classTypeDescriptor(rettype));
			return sb.toString();
		}
		
		public static NameAndType forField(String name, Class<?> cls) {
			return new NameAndType(name, classTypeDescriptor(cls));
		}
		
		public static NameAndType forMethod(String name, Class<?> rettype, Class<?> ...argtypes) {
			return new NameAndType(name, methodTypeDescriptor(rettype, argtypes));
		}
		
		@Override
		public void build(ClassBuilder cb, DataOutput out) throws IOException {
			int ni = cb.addConstant(name);
			int ti = cb.addConstant(desc);
			out.writeByte(12);
			out.writeShort(ni);
			out.writeShort(ti);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((desc == null) ? 0 : desc.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			NameAndType other = (NameAndType) obj;
			if (name == null) {
				if (other.name != null) return false;
			} else if (!name.equals(other.name)) return false;
			if (desc == null) {
				if (other.desc != null) return false;
			} else if (!desc.equals(other.desc)) return false;
			return true;
		}
		
		@Override
		public String toString() {
			return name.toString() + "::" + desc.toString();
		}
	}
	
	// TODO method handle constant
	
	// TODO method type constant
	
	// TODO invoke dynamic constant
	
	public static StringRef forString(String val) {
		return new StringRef(val);
	}
	
	public static Integer forInteger(int val) {
		return new Integer(val);
	}
	
	public static Float forFloat(float val) {
		return new Float(val);
	}
	
	public static Long forLong(long val) {
		return new Long(val);
	}
	
	public static Double forDouble(double val) {
		return new Double(val);
	}
	
	public static ClassRef forClass(Class<?> cls) {
		return new ClassRef(cls);
	}
	
	public static ClassRef forClass(String cname) {
		return new ClassRef(cname);
	}
	
	public static FieldRef forField(ClassRef cls, String fname, String fdesc) {
		return new FieldRef(cls, new NameAndType(fname, fdesc));
	}
	
	public static FieldRef forField(String cname, String fname, String fdesc) {
		return new FieldRef(forClass(cname), new NameAndType(fname, fdesc));
	}
	
	public static FieldRef forField(ClassRef cls, String fname, Class<?> ftype) {
		return new FieldRef(cls, NameAndType.forField(fname, ftype));
	}
	
	public static FieldRef forField(String cname, String fname, Class<?> ftype) {
		return new FieldRef(forClass(cname), NameAndType.forField(fname, ftype));
	}
	
	public static MethodRef forMethod(ClassRef cls, String mname, String mdesc) {
		return new MethodRef(cls, new NameAndType(mname, mdesc));
	}
	
	public static MethodRef forMethod(String cname, String mname, String mdesc) {
		return new MethodRef(forClass(cname), new NameAndType(mname, mdesc));
	}
	
	public static MethodRef forMethod(ClassRef cls, String mname, Class<?> rettype, Class<?> ...argtypes) {
		return new MethodRef(cls, NameAndType.forMethod(mname, rettype, argtypes));
	}
	
	public static MethodRef forMethod(String cname, String mname, Class<?> rettype, Class<?> ...argtypes) {
		return new MethodRef(forClass(cname), NameAndType.forMethod(mname, rettype, argtypes));
	}
}
