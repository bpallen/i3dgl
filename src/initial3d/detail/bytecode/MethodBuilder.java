package initial3d.detail.bytecode;

import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import com.sun.org.apache.bcel.internal.generic.ILOAD;

import initial3d.detail.bytecode.Constant.NameAndType;

/**
 * 
 * @author ben
 *
 */
public class MethodBuilder {

	private Constant.UTF name = null;
	private Constant.UTF desc = null;
	private int accessflags = 0;
	private int max_stack = 8;
	private int max_locals = 8;
	private List<Op> ops = new ArrayList<Op>();
	private List<TryCatchBlock> trycatches = new ArrayList<TryCatchBlock>();
	private Scope current_scope = new Scope(this, null, 0);

	private static class TryCatchBlock {
		// start of try, inclusive
		public final Op start;
		// end of try, exclusive
		public final Op end;
		// start of catch
		public final Op handler;
		// type of catch
		public final Constant.ClassRef catch_type;

		public TryCatchBlock(
			Op start_,
			Op end_,
			Op handler_,
			Constant.ClassRef catch_type_) {
			start = Objects.requireNonNull(start_);
			end = Objects.requireNonNull(end_);
			handler = Objects.requireNonNull(handler_);
			catch_type = Objects.requireNonNull(catch_type_);
		}
	}

	public MethodBuilder() {

	}

	public void declareName(String mname) {
		name = new Constant.UTF(mname);
	}

	public void declareType(String mdesc) {
		desc = new Constant.UTF(mdesc);
	}

	public void declareFlag(Flag a) {
		accessflags |= a.flag();
	}
	
	public void declareType(boolean static_, Class<?> rettype, Class<?>... argtypes) {
		if (current_scope.parent() != null) throw new RuntimeException("method type must be declared at root scope");
		if (current_scope.nextIndex() != 0) throw new RuntimeException("method type must be declared before any variables");
		if (static_) declareFlag(Flag.STATIC);
		declareType(NameAndType.methodTypeDescriptor(rettype, argtypes));
		maxLocals(argtypes.length);
		if (!static_) current_scope.alloc(Object.class, "this"); 
		for (int i = 0; i < argtypes.length; i++) {
			current_scope.alloc(argtypes[i], "arg" + i);
		}
	}
	
	public void declareParamNames(String ...names) {
		for (int i = 0; i < names.length; i++) {
			current_scope.rename("arg" + i, names[i]);
		}
	}
	
	public Constant.NameAndType nameAndType() {
		return new Constant.NameAndType(name, desc);
	}
	
	public Scope scope() {
		return current_scope;
	}
	
	public Scope pushScope() {
		current_scope = new Scope(this, current_scope, current_scope.nextIndex());
		return current_scope;
	}
	
	public Scope popScope() {
		if (current_scope.parent() == null) throw new RuntimeException("can't pop last scope");
		current_scope = current_scope.parent();
		return current_scope;
	}

	public MethodBuilder op(Op o) {
		if (Objects.requireNonNull(o).scope() != current_scope) throw new RuntimeException("op not in current scope");
		ops.add(o);
		return this;
	}

	public MethodBuilder op(byte opcode) {
		ops.add(new Op(current_scope, opcode));
		return this;
	}

	public MethodBuilder op(byte opcode, byte... imm) {
		ops.add(new Op(current_scope, opcode, imm));
		return this;
	}

	public MethodBuilder op(byte opcode, int imm) {
		ops.add(new Op(current_scope, opcode, new byte[] { (byte) imm }));
		return this;
	}

	public MethodBuilder opShort(byte opcode, int imm) {
		byte[] data = new byte[2];
		data[0] = (byte) ((imm >> 8) & 0xFF);
		data[1] = (byte) (imm & 0xFF);
		ops.add(new Op(current_scope, opcode, data));
		return this;
	}

	public MethodBuilder opInt(byte opcode, int imm) {
		byte[] data = new byte[4];
		data[0] = (byte) ((imm >> 24) & 0xFF);
		data[1] = (byte) ((imm >> 16) & 0xFF);
		data[2] = (byte) ((imm >> 8) & 0xFF);
		data[3] = (byte) (imm & 0xFF);
		ops.add(new Op(current_scope, opcode, data));
		return this;
	}

	public MethodBuilder op(byte opcode, Constant c) {
		ops.add(new Op(current_scope, opcode, c));
		return this;
	}

	public MethodBuilder op(byte opcode, Constant c, byte... imm) {
		ops.add(new Op(current_scope, opcode, c, imm));
		return this;
	}

	public MethodBuilder op(byte opcode, Constant c, int imm) {
		ops.add(new Op(current_scope, opcode, c, new byte[] { (byte) imm }));
		return this;
	}

	public MethodBuilder opShort(byte opcode, Constant c, short imm) {
		byte[] data = new byte[2];
		data[0] = (byte) (imm >> 8);
		data[1] = (byte) (imm & 0xFF);
		ops.add(new Op(current_scope, opcode, c, data));
		return this;
	}
	
	public MethodBuilder invokeCompare(Class<?> cls, String cmp) {
		try {
			invokeStatic(MethodBuilder.class, "compare" + cmp, boolean.class, cls, cls);
			return this;
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
	}
	
	public MethodBuilder feq() {
		invokeCompare(float.class, "EQ");
		return this;
	}
	
	public MethodBuilder fne() {
		invokeCompare(float.class, "NE");
		return this;
	}
	
	public MethodBuilder flt() {
		invokeCompare(float.class, "LT");
		return this;
	}
	
	public MethodBuilder fle() {
		invokeCompare(float.class, "LE");
		return this;
	}
	
	public MethodBuilder fge() {
		invokeCompare(float.class, "GE");
		return this;
	}
	
	public MethodBuilder fgt() {
		invokeCompare(float.class, "GT");
		return this;
	}
	
	public MethodBuilder ieq() {
		invokeCompare(int.class, "EQ");
		return this;
	}
	
	public MethodBuilder ine() {
		invokeCompare(int.class, "NE");
		return this;
	}
	
	public MethodBuilder ilt() {
		invokeCompare(int.class, "LT");
		return this;
	}
	
	public MethodBuilder ile() {
		invokeCompare(int.class, "LE");
		return this;
	}
	
	public MethodBuilder ige() {
		invokeCompare(int.class, "GE");
		return this;
	}
	
	public MethodBuilder igt() {
		invokeCompare(int.class, "GT");
		return this;
	}
	
	public MethodBuilder math(Class<?> cls, String mname, int nargs) {
		Class<?>[] argtypes = new Class<?>[nargs];
		for (int i = 0; i < nargs; i++) argtypes[i] = cls;
		try {
			invokeStatic(Math.class, mname, cls, argtypes);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		return this;
	}
	
	public MethodBuilder imath(String mname, int nargs) {
		return math(int.class, mname, nargs);
	}
	
	public MethodBuilder fmath(String mname, int nargs) {
		return math(float.class, mname, nargs);
	}
	
	public MethodBuilder dmath(String mname, int nargs) {
		return math(double.class, mname, nargs);
	}
	
	public MethodBuilder imin() {
		return imath("min", 2);
	}
	
	public MethodBuilder imax() {
		return imath("max", 2);
	}
	
	public MethodBuilder umin() {
		try {
			invokeStatic(MethodBuilder.class, "minUnsigned", int.class, int.class, int.class);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		return this;
	}
	
	public MethodBuilder umax() {
		try {
			invokeStatic(MethodBuilder.class, "maxUnsigned", int.class, int.class, int.class);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		return this;
	}
	
	public MethodBuilder fmin() {
		return fmath("min", 2);
	}
	
	public MethodBuilder fmax() {
		return fmath("max", 2);
	}
	
	public MethodBuilder udiv() {
		try {
			invokeStatic(Integer.class, "divideUnsigned", int.class, int.class, int.class);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		return this;
	}
	
	public MethodBuilder urem() {
		try {
			invokeStatic(Integer.class, "remainderUnsigned", int.class, int.class, int.class);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		return this;
	}
	
	public MethodBuilder u2f() {
		try {
			invokeStatic(MethodBuilder.class, "unsignedToFloat", float.class, int.class);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		return this;
	}
	
	public MethodBuilder f2u() {
		try {
			invokeStatic(MethodBuilder.class, "floatToUnsigned", int.class, float.class);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		return this;
	}
	
	public MethodBuilder aload(int x) {
		switch (x) {
		case 0:
			op(Op.ALOAD_0);
			break;
		case 1:
			op(Op.ALOAD_1);
			break;
		case 2:
			op(Op.ALOAD_2);
			break;
		case 3:
			op(Op.ALOAD_3);
			break;
		default:
			op(Op.ALOAD, x);
		}
		return this;
	}
	
	public MethodBuilder aload(String name) {
		return aload(current_scope.get(name));
	}
	
	public MethodBuilder astore(int x) {
		switch (x) {
		case 0:
			op(Op.ASTORE_0);
			break;
		case 1:
			op(Op.ASTORE_1);
			break;
		case 2:
			op(Op.ASTORE_2);
			break;
		case 3:
			op(Op.ASTORE_3);
			break;
		default:
			op(Op.ASTORE, x);
		}
		return this;
	}
	
	public MethodBuilder astore(String name) {
		return astore(current_scope.get(name));
	}

	public MethodBuilder iload(int x) {
		switch (x) {
		case 0:
			op(Op.ILOAD_0);
			break;
		case 1:
			op(Op.ILOAD_1);
			break;
		case 2:
			op(Op.ILOAD_2);
			break;
		case 3:
			op(Op.ILOAD_3);
			break;
		default:
			op(Op.ILOAD, x);
		}
		return this;
	}

	public MethodBuilder iload(String name) {
		return iload(current_scope.get(name));
	}
	
	public MethodBuilder istore(int x) {
		switch (x) {
		case 0:
			op(Op.ISTORE_0);
			break;
		case 1:
			op(Op.ISTORE_1);
			break;
		case 2:
			op(Op.ISTORE_2);
			break;
		case 3:
			op(Op.ISTORE_3);
			break;
		default:
			op(Op.ISTORE, x);
		}
		return this;
	}
	
	public MethodBuilder istore(String name) {
		return istore(current_scope.get(name));
	}
	
	public MethodBuilder lload(int x) {
		switch (x) {
		case 0:
			op(Op.LLOAD_0);
			break;
		case 1:
			op(Op.LLOAD_1);
			break;
		case 2:
			op(Op.LLOAD_2);
			break;
		case 3:
			op(Op.LLOAD_3);
			break;
		default:
			op(Op.LLOAD, x);
		}
		return this;
	}
	
	public MethodBuilder lload(String name) {
		return lload(current_scope.get(name));
	}
	
	public MethodBuilder lstore(int x) {
		switch (x) {
		case 0:
			op(Op.LSTORE_0);
			break;
		case 1:
			op(Op.LSTORE_1);
			break;
		case 2:
			op(Op.LSTORE_2);
			break;
		case 3:
			op(Op.LSTORE_3);
			break;
		default:
			op(Op.LSTORE, x);
		}
		return this;
	}
	
	public MethodBuilder lstore(String name) {
		return lstore(current_scope.get(name));
	}
	
	public MethodBuilder fload(int x) {
		switch (x) {
		case 0:
			op(Op.FLOAD_0);
			break;
		case 1:
			op(Op.FLOAD_1);
			break;
		case 2:
			op(Op.FLOAD_2);
			break;
		case 3:
			op(Op.FLOAD_3);
			break;
		default:
			op(Op.FLOAD, x);
		}
		return this;
	}
	
	public MethodBuilder fload(String name) {
		return fload(current_scope.get(name));
	}
	
	public MethodBuilder fstore(int x) {
		switch (x) {
		case 0:
			op(Op.FSTORE_0);
			break;
		case 1:
			op(Op.FSTORE_1);
			break;
		case 2:
			op(Op.FSTORE_2);
			break;
		case 3:
			op(Op.FSTORE_3);
			break;
		default:
			op(Op.FSTORE, x);
		}
		return this;
	}
	
	public MethodBuilder fstore(String name) {
		return fstore(current_scope.get(name));
	}
	
	public MethodBuilder dload(int x) {
		switch (x) {
		case 0:
			op(Op.DLOAD_0);
			break;
		case 1:
			op(Op.DLOAD_1);
			break;
		case 2:
			op(Op.DLOAD_2);
			break;
		case 3:
			op(Op.DLOAD_3);
			break;
		default:
			op(Op.DLOAD, x);
		}
		return this;
	}
	
	public MethodBuilder dload(String name) {
		return dload(current_scope.get(name));
	}
	
	public MethodBuilder dstore(int x) {
		switch (x) {
		case 0:
			op(Op.DSTORE_0);
			break;
		case 1:
			op(Op.DSTORE_1);
			break;
		case 2:
			op(Op.DSTORE_2);
			break;
		case 3:
			op(Op.DSTORE_3);
			break;
		default:
			op(Op.DSTORE, x);
		}
		return this;
	}
	
	public MethodBuilder dstore(String name) {
		return dstore(current_scope.get(name));
	}
	
	public MethodBuilder load(Class<?> cls, String name) {
		if (cls == boolean.class) return iload(name);
		if (cls == byte.class) return iload(name);
		if (cls == short.class) return iload(name);
		if (cls == char.class) return iload(name);
		if (cls == int.class) return iload(name);
		if (cls == long.class) return lload(name);
		if (cls == float.class) return fload(name);
		if (cls == double.class) return dload(name);
		return aload(name);
	}
	
	public MethodBuilder store(Class<?> cls, String name) {
		if (cls == boolean.class) return istore(name);
		if (cls == byte.class) return istore(name);
		if (cls == short.class) return istore(name);
		if (cls == char.class) return istore(name);
		if (cls == int.class) return istore(name);
		if (cls == long.class) return lstore(name);
		if (cls == float.class) return fstore(name);
		if (cls == double.class) return dstore(name);
		return astore(name);
	}
	
	public MethodBuilder ret(Class<?> cls) {
		if (cls == void.class) return op(Op.RETURN);
		if (cls == boolean.class) return op(Op.IRETURN);
		if (cls == byte.class) return op(Op.IRETURN);
		if (cls == short.class) return op(Op.IRETURN);
		if (cls == char.class) return op(Op.IRETURN);
		if (cls == int.class) return op(Op.IRETURN);
		if (cls == long.class) return op(Op.LRETURN);
		if (cls == float.class) return op(Op.FRETURN);
		if (cls == double.class) return op(Op.DRETURN);
		return op(Op.ARETURN);
	}
	
	public MethodBuilder getField(Class<?> cls, String fname) throws NoSuchFieldException {
		ops.add(new Op(current_scope, Op.GETFIELD, Constant.FieldRef.reflect(cls, fname)));
		return this;
	}

	public MethodBuilder putField(Class<?> cls, String fname) throws NoSuchFieldException {
		ops.add(new Op(current_scope, Op.PUTFIELD, Constant.FieldRef.reflect(cls, fname)));
		return this;
	}

	public MethodBuilder getStatic(Class<?> cls, String fname) throws NoSuchFieldException {
		ops.add(new Op(current_scope, Op.GETSTATIC, Constant.FieldRef.reflect(cls, fname)));
		return this;
	}

	public MethodBuilder putStatic(Class<?> cls, String fname) throws NoSuchFieldException {
		ops.add(new Op(current_scope, Op.PUTSTATIC, Constant.FieldRef.reflect(cls, fname)));
		return this;
	}

	public MethodBuilder invokeStatic(Constant.MethodRef m) {
		ops.add(new Op(current_scope, Op.INVOKESTATIC, m));
		return this;
	}

	public MethodBuilder invokeStatic(String mname, Class<?> rettype, Class<?>... argtypes) {
		invokeStatic(Constant.forMethod("${thisclass}", mname, rettype, argtypes));
		return this;
	}

	public MethodBuilder invokeStatic(Constant.ClassRef cls, String mname, Class<?> rettype, Class<?>... argtypes) {
		invokeStatic(Constant.forMethod(cls, mname, rettype, argtypes));
		return this;
	}

	public MethodBuilder invokeStatic(String cname, String mname, Class<?> rettype, Class<?>... argtypes) {
		invokeStatic(Constant.forMethod(cname, mname, rettype, argtypes));
		return this;
	}

	public MethodBuilder invokeStatic(Class<?> cls, String mname, Class<?> rettype, Class<?>... argtypes) throws NoSuchMethodException {
		invokeStatic(Constant.MethodRef.reflect(cls, mname, rettype, argtypes));
		return this;
	}

	public MethodBuilder invokeSpecial(Constant.MethodRef m) {
		ops.add(new Op(current_scope, Op.INVOKESPECIAL, m));
		return this;
	}

	public MethodBuilder invokeSpecial(String mname, Class<?> rettype, Class<?>... argtypes) {
		invokeSpecial(Constant.forMethod("${thisclass}", mname, rettype, argtypes));
		return this;
	}

	public MethodBuilder invokeSpecial(Constant.ClassRef cls, String mname, Class<?> rettype, Class<?>... argtypes) {
		invokeSpecial(Constant.forMethod(cls, mname, rettype, argtypes));
		return this;
	}

	public MethodBuilder invokeSpecial(String cname, String mname, Class<?> rettype, Class<?>... argtypes) {
		invokeSpecial(Constant.forMethod(cname, mname, rettype, argtypes));
		return this;
	}

	public MethodBuilder invokeSpecial(Class<?> cls, String mname, Class<?> rettype, Class<?>... argtypes) throws NoSuchMethodException {
		invokeSpecial(Constant.MethodRef.reflect(cls, mname, rettype, argtypes));
		return this;
	}

	public MethodBuilder invokeVirtual(Constant.MethodRef m) {
		ops.add(new Op(current_scope, Op.INVOKEVIRTUAL, m));
		return this;
	}

	public MethodBuilder invokeVirtual(String mname, Class<?> rettype, Class<?>... argtypes) {
		invokeVirtual(Constant.forMethod("${thisclass}", mname, rettype, argtypes));
		return this;
	}

	public MethodBuilder invokeVirtual(Constant.ClassRef cls, String mname, Class<?> rettype, Class<?>... argtypes) {
		invokeVirtual(Constant.forMethod(cls, mname, rettype, argtypes));
		return this;
	}
	
	public MethodBuilder invokeVirtual(String cname, String mname, Class<?> rettype, Class<?>... argtypes) {
		invokeVirtual(Constant.forMethod(cname, mname, rettype, argtypes));
		return this;
	}
	
	public MethodBuilder invokeVirtual(Class<?> cls, String mname, Class<?> rettype, Class<?>... argtypes) throws NoSuchMethodException {
		invokeVirtual(Constant.MethodRef.reflect(cls, mname, rettype, argtypes));
		return this;
	}

	public MethodBuilder invokeInterface(Class<?> cls, String mname, Class<?> rettype, Class<?>... argtypes) throws NoSuchMethodException {
		int count = 1;
		for (Class<?> c : argtypes) {
			if (long.class.equals(c)) {
				count += 2;
			} else if (double.class.equals(c)) {
				count += 2;
			} else {
				count += 1;
			}
		}
		ops.add(new Op(
			current_scope,
			Op.INVOKEINTERFACE,
			Constant.MethodRef.reflect(cls, mname, rettype, argtypes),
			new byte[] { (byte) count, 0 }));
		return this;
	}

	public MethodBuilder label(String label) {
		ops.get(ops.size() - 1).label(label);
		return this;
	}

	public MethodBuilder branch(byte opcode, String dest) {
		ops.add(new Op.Branch(current_scope, opcode, dest));
		return this;
	}

	public MethodBuilder constant(Constant c) {
		ops.add(new Op.LoadConstant(current_scope, c));
		return this;
	}
	
	public MethodBuilder constant(String c) {
		return constant(Constant.forString(c));
	}
	
	public MethodBuilder constant(int c) {
		return constant(Constant.forInteger(c));
	}
	
	public MethodBuilder constant(long c) {
		return constant(Constant.forLong(c));
	}
	
	public MethodBuilder constant(float c) {
		return constant(Constant.forFloat(c));
	}

	public MethodBuilder printlni() {
		try {
			getStatic(System.class, "out");
		} catch (NoSuchFieldException e) {
			throw new AssertionError(e);
		}
		op(Op.SWAP);
		try {
			invokeVirtual(System.out.getClass(), "println", void.class, int.class);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		return this;
	}
	
	public MethodBuilder printlnf() {
		try {
			getStatic(System.class, "out");
		} catch (NoSuchFieldException e) {
			throw new AssertionError(e);
		}
		op(Op.SWAP);
		try {
			invokeVirtual(System.out.getClass(), "println", void.class, float.class);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		return this;
	}
	
	public MethodBuilder printlna() {
		try {
			getStatic(System.class, "out");
		} catch (NoSuchFieldException e) {
			throw new AssertionError(e);
		}
		op(Op.SWAP);
		try {
			invokeVirtual(System.out.getClass(), "println", void.class, Object.class);
		} catch (NoSuchMethodException e) {
			throw new AssertionError(e);
		}
		return this;
	}
	
	public MethodBuilder tryCatch(String start_label, String end_label, String handler_label, Constant.ClassRef catch_type) {
		// labels must already be resolvable in current scope
		trycatches.add(new TryCatchBlock(
			current_scope.label(start_label),
			current_scope.label(end_label),
			current_scope.label(handler_label),
			catch_type
		));
		return this;
	}

	public MethodBuilder tryCatch(String start_label, String end_label, String handler_label, Class<?> catch_type) {
		return tryCatch(start_label, end_label, handler_label, new Constant.ClassRef(catch_type));
	}

	public MethodBuilder maxStack(int x) {
		max_stack = Math.max(max_stack, x);
		return this;
	}

	public MethodBuilder maxLocals(int x) {
		max_locals = Math.max(max_locals, x);
		return this;
	}

	public int address(ClassBuilder cb, Op op) {
		Objects.requireNonNull(op);
		int bi = 0;
		for (Op o : ops) {
			if (op == o) return bi;
			bi += o.size(cb, this);
		}
		throw new NoSuchElementException();
	}

	public int codeSize(ClassBuilder cb) {
		int sz = 0;
		for (Op o : ops) {
			sz += o.size(cb, this);
		}
		return sz;
	}

	public Constant.MethodRef ref(ClassBuilder cb) {
		return new Constant.MethodRef(cb.ref(), new Constant.NameAndType(name, desc));
	}

	public void build(ClassBuilder cb, DataOutput out) throws IOException {
		Objects.requireNonNull(name);
		Objects.requireNonNull(desc);
		out.writeShort(accessflags);
		out.writeShort(cb.addConstant(name));
		out.writeShort(cb.addConstant(desc));

		// Code attribute only
		if ((accessflags & (Flag.ABSTRACT.flag() | Flag.NATIVE.flag())) == 0) {
			// neither abstract or native is set, write method body
			out.writeShort(1);
			out.writeShort(cb.addConstantUTF("Code"));

			// in order for verification to succeed at version >= 51 (jre7)
			// we need to produce a StackMapTable ...

			// Code attribute length
			int attr_length = 8;
			attr_length += codeSize(cb);
			attr_length += 2;
			attr_length += 8 * trycatches.size();
			attr_length += 2;
			out.writeInt(attr_length);

			out.writeShort(max_stack);
			out.writeShort(max_locals);

			// opcodes
			out.writeInt(codeSize(cb));
			int address = 0;
			for (Op o : ops) {
				o.build(cb, this, address, out);
				address += o.size(cb, this);
			}

			// exception table
			out.writeShort(trycatches.size());
			for (TryCatchBlock tc : trycatches) {
				out.writeShort(address(cb, tc.start));
				out.writeShort(address(cb, tc.end));
				out.writeShort(address(cb, tc.handler));
				out.writeShort(cb.addConstant(tc.catch_type));
			}

			// no extra attributes
			out.writeShort(0);

		} else {
			// no attributes
			out.writeShort(0);
		}

	}

	public static boolean compareEQ(float a, float b) {
		return a == b;
	}
	
	public static boolean compareNE(float a, float b) {
		return a != b;
	}
	
	public static boolean compareLT(float a, float b) {
		return a < b;
	}
	
	public static boolean compareLE(float a, float b) {
		return a <= b;
	}
	
	public static boolean compareGE(float a, float b) {
		return a >= b;
	}
	
	public static boolean compareGT(float a, float b) {
		return a > b;
	}
	
	public static boolean compareEQ(int a, int b) {
		return a == b;
	}
	
	public static boolean compareNE(int a, int b) {
		return a != b;
	}
	
	public static boolean compareLT(int a, int b) {
		return a < b;
	}
	
	public static boolean compareLE(int a, int b) {
		return a <= b;
	}
	
	public static boolean compareGE(int a, int b) {
		return a >= b;
	}
	
	public static boolean compareGT(int a, int b) {
		return a > b;
	}
	
	public static int minUnsigned(int a, int b) {
		return (int) Math.min(Integer.toUnsignedLong(a), Integer.toUnsignedLong(b));
	}
	
	public static int maxUnsigned(int a, int b) {
		return (int) Math.max(Integer.toUnsignedLong(a), Integer.toUnsignedLong(b));
	}
	
	public static float unsignedToFloat(int a) {
		float f = (float) (a & Integer.MAX_VALUE);
		if (f < 0) f += 0x1.0p31f;
		return f;
	}
	
	public static float floatToUnsigned(float a) {
		if (a > Integer.MAX_VALUE) {
			return ((int) (a * 0.5f)) << 1;
		} else {
			return (int) a;
		}
	}
	
}















