package initial3d.detail.bytecode;

import java.util.HashMap;

public class Scope {

	private static class Variable {
		public final int index;
		public final Class<?> type;
		public String name;
		
		public Variable(int index_, Class<?> type_, String name_) {
			index = index_;
			type = type_;
			name = name_;
		}
	}
	
	private final MethodBuilder method;
	private final Scope parent;
	private int next_index;
	private HashMap<String, Variable> variables = new HashMap<>();
	private HashMap<String, Op> labels = new HashMap<>();
	
	public Scope(MethodBuilder method_, Scope parent_, int next_) {
		method = method_;
		parent = parent_;
		next_index = next_;
	}
	
	public MethodBuilder method() {
		return method;
	}
	
	public Scope parent() {
		return parent;
	}
	
	public int nextIndex() {
		return next_index;
	}
	
	/**
	 * Allocate some number of variable indices in this scope.
	 * 
	 * @param n
	 * @return
	 */
	public int alloc(int n) {
		int x = next_index;
		next_index += n;
		method.maxLocals(next_index);
		return x;
	}
	
	/**
	 * Allocate a variable in this scope.
	 * 
	 * @param type
	 * @param name
	 * @return
	 */
	public int alloc(Class<?> type, String name) {
		int x = -1;
		if (type == long.class || type == double.class) {
			x = alloc(2);
		} else {
			x = alloc(1);
		}
		Variable v = new Variable(x, type, name);
		variables.put(name, v);
		return x;
	}
	
	/**
	 * Get index of named variable.
	 * Searches in parent scopes if not found in this scope.
	 * 
	 * @param name
	 * @throws IllegalArgumentException if no variable found
	 * @return
	 */
	public int get(String name) {
		Variable v = variables.get(name);
		if (v == null) {
			if (parent == null) throw new IllegalArgumentException(String.format("variable '%s' not found" , name));
			return parent.get(name);
		}
		return v.index;
	}
	
	/**
	 * Rename a variable in this scope. Does not search parent scopes.
	 * 
	 * @param old_name
	 * @param new_name
	 */
	public void rename(String old_name, String new_name) {
		Variable v = variables.remove(old_name);
		v.name = new_name;
		variables.put(new_name, v);
	}
	
	/**
	 * Attach a label to an op in this scope.
	 * 
	 * @param op
	 * @param l
	 */
	public void label(Op op, String l) {
		if (op.scope() != this) throw new RuntimeException("op not in this scope");
		labels.put(l, op);
	}
	
	/**
	 * Get an op in this scope by label.
	 * Searches in parent scopes if not found in this scope.
	 * 
	 * @param l
	 * @return
	 */
	public Op label(String l) {
		Op op = labels.get(l);
		if (op == null) {
			if (parent == null) return null;
			return parent.label(l);
		}
		return op;
	}
}
