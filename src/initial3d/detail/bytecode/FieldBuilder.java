package initial3d.detail.bytecode;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

import initial3d.detail.bytecode.Constant.NameAndType;

public class FieldBuilder {

	private Constant.UTF name = null;
	private Constant.UTF desc = null;
	private int accessflags = 0;
	private Constant constant = null;
	
	public FieldBuilder() {
		
	}
	
	public void declareName(String fname) {
		name = new Constant.UTF(fname);
	}
	
	public void declareType(String fdesc) {
		desc = new Constant.UTF(fdesc);
	}
	
	public void declareType(Class<?> fcls) {
		declareType(NameAndType.classTypeDescriptor(fcls));
	}
	
	public void declareAccess(Flag a) {
		accessflags |= a.flag();
	}
	
	public void constantValue(Constant c) {
		constant = c;
	}
	
	public Constant.FieldRef ref(ClassBuilder cb) {
		return new Constant.FieldRef(cb.ref(), new Constant.NameAndType(name, desc));
	}
	
	public void build(ClassBuilder cb, DataOutput out) throws IOException {
		Objects.requireNonNull(name);
		Objects.requireNonNull(desc);
		out.writeShort(accessflags);
		out.writeShort(cb.addConstant(name));
		out.writeShort(cb.addConstant(desc));
		// ConstantValue attribute only
		if (constant != null) {
			out.writeShort(1);
			out.writeShort(cb.addConstantUTF("ConstantValue"));
			out.writeInt(2);
			out.writeShort(cb.addConstant(constant));
		} else {
			// no attributes
			out.writeShort(0);
		}
	}
	
}
