package initial3d.detail;

public interface AccessArbitrator {

	public void arbitrate();
	
	public AccessPromise requestAccess(Access a, boolean arbitrate_now);
	
	public void checkAcquire(AccessPromise p);
	
	public void onAcquire(AccessPromise p);
	
	public void onRelease(AccessPromise p);
	
	public void onSatisfied(AccessPromise p);
	
}
