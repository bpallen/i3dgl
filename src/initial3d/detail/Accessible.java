package initial3d.detail;

public interface Accessible {

	public void arbitrate();
	
	public AccessFuture requestAccess(Access a, boolean arbitrate_now);
	
	default AccessFuture requestAccess(Access a) {
		return requestAccess(a, true);
	}

	public AccessFuture requestAccessDeferred(Access a);
	
}
