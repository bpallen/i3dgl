package initial3d.detail;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.zip.CRC32;

import initial3d.Mat2f;
import initial3d.Mat3f;
import initial3d.Mat4f;
import initial3d.Vec2f;
import initial3d.Vec2i;
import initial3d.Vec3f;
import initial3d.Vec3i;
import initial3d.Vec4f;
import initial3d.Vec4i;
import sun.misc.Unsafe;

@SuppressWarnings("restriction")
public class Util {

	public static final Unsafe unsafe;

	static {
		try {
			Field field = Unsafe.class.getDeclaredField("theUnsafe");
			field.setAccessible(true);
			unsafe = (Unsafe) field.get(null);
		} catch (Throwable t) {
			throw new AssertionError(t);
		}
	}

	private static final int compressed_oop_shift;
	
	static {
		// oop shift is either 0 or 3
		// if we get any oop with LSB set, shift is in use
		int shift = 0;
		for (int i = 0; i < 128; i++) {
			long oop = oopOf(new Object());
			if ((oop & 1L) == 1L) {
				System.out.println("Compressed Oops detected!");
				shift = 3;
				break;
			}
		}
		compressed_oop_shift = shift;
	}
	
	private Util() {
		throw new AssertionError("You're doing it wrong.");
	}

	/**
	 * Allocate a reference-counted block of memory. Reference count is incremented with {@link #sharedRefInc(long)}
	 * and decremented with {@link #sharedRefDec(long)}. Reference count starts at 1. When the reference count reaches 0,
	 * the block is freed. The returned pointer is 64-byte aligned.
	 * 
	 * @param size
	 * @return
	 */
	public static long sharedAlloc(long size) {
		final long align = 64;
		final long mask = ~((long) align - 1);
		long pbase = unsafe.allocateMemory(size + align + 12);
		long palign = (pbase + 12 + align - 1) & mask;
		// store the base pointer
		unsafe.putLongVolatile(null, palign - 8, pbase);
		// and init the refcount
		unsafe.putIntVolatile(null, palign - 12, 1);
		return palign;
	}
	
	/**
	 * Increment reference count for shared allocation. Does nothing if p == 0.
	 * 
	 * @param p
	 */
	public static void sharedRefInc(long p) {
		if (p == 0) return;
		int c = 0;
		do {
			c = unsafe.getInt(p - 12);
		} while (!unsafe.compareAndSwapInt(null, p - 12, c, c + 1));
	}
	
	/**
	 * Decrement reference count for shared allocation. If reference count reaches 0, the block is freed.
	 * Does nothing if p == 0.
	 * 
	 * @param p
	 */
	public static void sharedRefDec(long p) {
		if (p == 0) return;
		int c = 0;
		do {
			c = unsafe.getInt(p - 12);
		} while (!unsafe.compareAndSwapInt(null, p - 12, c, c - 1));
		if (c == 0) {
			unsafe.freeMemory(unsafe.getLong(p - 8));
		}
	}
	
	/**
	 * Map a region of native memory as a <code>java.nio.ByteBuffer</code>. The returned buffer does not own the
	 * referenced memory.
	 * 
	 * @param address
	 * @param size
	 * @return
	 */
	public static ByteBuffer mapBuffer(long address, int size) {
		try {
			// invoke the ctor used by JNI to map native memory
			Constructor<?> ctor = Class.forName("java.nio.DirectByteBuffer").getDeclaredConstructor(long.class, int.class);
			ctor.setAccessible(true);
			ByteBuffer b = (ByteBuffer) ctor.newInstance(address, size);
			b.order(ByteOrder.nativeOrder());
			return b;
		} catch (Exception e) {
			throw new AssertionError(e);
		}
	}
	
	/**
	 * Retrieve OOP (Ordinary Object Pointer) of an Object. May be compressed.
	 * 
	 * @param o
	 * @return OOP
	 */
	public static long oopOf(Object o) {
		Object[] a = new Object[] { o };
		return ((1l << 32) - 1l) & (long) unsafe.getInt(a, 16L);
	}
	
	/**
	 * Retrieve address of an Object by decompressing its OOP. Assumes zero-based OOPs.
	 * 
	 * @param o
	 * @return address
	 */
	public static long addressOf(Object o) {
		return oopOf(o) << compressed_oop_shift;
	}
	
	public static void copy4(long pout, long pin) {
		int x = unsafe.getInt(pin);
		unsafe.putInt(pout, x);
	}
	
	public static void copy8(long pout, long pin) {
		long x = unsafe.getLong(pin);
		unsafe.putLong(pout, x);
	}
	
	public static void copy16(long pout, long pin) {
		copy8(pout, pin);
		copy8(pout + 8, pin + 8);
	}
	
	public static void copy24(long pout, long pin) {
		copy8(pout, pin);
		copy8(pout + 8, pin + 8);
		copy8(pout + 16, pin + 16);
	}
	
	public static void copy32(long pout, long pin) {
		copy8(pout, pin);
		copy8(pout + 8, pin + 8);
		copy8(pout + 16, pin + 16);
		copy8(pout + 24, pin + 24);
	}
	
	public static void copy64(long pout, long pin) {
		// TODO at what point does memcpy become faster?
//		copy8(pout, pin);
//		copy8(pout + 8, pin + 8);
//		copy8(pout + 16, pin + 16);
//		copy8(pout + 24, pin + 24);
//		copy8(pout + 32, pin + 32);
//		copy8(pout + 40, pin + 40);
//		copy8(pout + 48, pin + 48);
//		copy8(pout + 56, pin + 56);
		unsafe.copyMemory(pin, pout, 64);
	}
	
	public static void copy(long pout, long pin, int size) {
		unsafe.copyMemory(pin, pout, size);
	}
	
	public static void putf(long pout, float x) {
		unsafe.putFloat(pout, x);
	}
	
	public static void putf(long pout, float x, float y) {
		unsafe.putFloat(pout, x);
		unsafe.putFloat(pout + 4, y);
	}
	
	public static void putf(long pout, float x, float y, float z) {
		unsafe.putFloat(pout, x);
		unsafe.putFloat(pout + 4, y);
		unsafe.putFloat(pout + 8, z);
	}
	
	public static void putf(long pout, float x, float y, float z, float w) {
		unsafe.putFloat(pout, x);
		unsafe.putFloat(pout + 4, y);
		unsafe.putFloat(pout + 8, z);
		unsafe.putFloat(pout + 12, w);
	}
	
	public static float getf(long p) {
		return unsafe.getFloat(p);
	}
	
	public static void puti(long pout, int x) {
		unsafe.putInt(pout, x);
	}
	
	public static void puti(long pout, int x, int y) {
		unsafe.putInt(pout, x);
		unsafe.putInt(pout + 4, y);
	}
	
	public static void puti(long pout, int x, int y, int z) {
		unsafe.putInt(pout, x);
		unsafe.putInt(pout + 4, y);
		unsafe.putInt(pout + 8, z);
	}
	
	public static void puti(long pout, int x, int y, int z, int w) {
		unsafe.putInt(pout, x);
		unsafe.putInt(pout + 4, y);
		unsafe.putInt(pout + 8, z);
		unsafe.putInt(pout + 12, z);
	}
	
	public static int geti(long p) {
		return unsafe.getInt(p);
	}
	
	public static void putl(long p, long x) {
		unsafe.putLong(p, x);
	}
	
	public static long getl(long p) {
		return unsafe.getLong(p);
	}
	
	public static void puts(long p, short x) {
		unsafe.putShort(p, x);
	}
	
	public static short gets(long p) {
		return unsafe.getShort(p);
	}
	
	public static void putb(long p, byte x) {
		unsafe.putByte(p, x);
	}
	
	public static byte getb(long p) {
		return unsafe.getByte(p);
	}
	
	public static void putf(long pout, Vec2f x) {
		putf(pout, x.x, x.y);
	}
	
	public static void putf(long pout, Vec3f x) {
		putf(pout, x.x, x.y, x.z);
	}
	
	public static void putf(long pout, Vec4f x) {
		putf(pout, x.x, x.y, x.z, x.w);
	}
	
	public static void puti(long pout, Vec2i x) {
		puti(pout, x.x, x.y);
	}
	
	public static void puti(long pout, Vec3i x) {
		puti(pout, x.x, x.y, x.z);
	}
	
	public static void puti(long pout, Vec4i x) {
		puti(pout, x.x, x.y, x.z, x.w);
	}
	
	/**
	 * Column-major, columns padded to 16 bytes.
	 * 
	 * @param pout
	 * @param x
	 */
	public static void putf(long pout, Mat2f x) {
		for (int c = 0; c < 2; c++) {
			for (int r = 0; r < 2; r++) {
				putf(pout, x.get(r, c));
				pout += 4;
			}
			// pad
			pout += 8;
		}
	}
	
	/**
	 * Column-major, columns padded to 16 bytes.
	 * @param pout
	 * @param x
	 */
	public static void putf(long pout, Mat3f x) {
		for (int c = 0; c < 3; c++) {
			for (int r = 0; r < 3; r++) {
				putf(pout, x.get(r, c));
				pout += 4;
			}
			// pad
			pout += 4;
		}
	}
	
	/**
	 * Column-major, columns padded to 16 bytes (but not really as they're already 16 bytes).
	 * 
	 * @param pout
	 * @param x
	 */
	public static void putf(long pout, Mat4f x) {
		for (int c = 0; c < 3; c++) {
			for (int r = 0; r < 3; r++) {
				putf(pout, x.get(r, c));
				pout += 4;
			}
			// no padding required
		}
	}

	public static void copyVec1(long pout, long pin0) {
		float f0 = unsafe.getFloat(pin0);
		unsafe.putFloat(pout, f0);
	}
	
	public static void copyVec2(long pout, long pin0) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
	}
	
	public static void copyVec3(long pout, long pin0) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
	}
	
	public static void copyVec4(long pout, long pin0) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		float f3 = unsafe.getFloat(pin0 + 12);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
		unsafe.putFloat(pout + 12, f3);
	}
	
	public static void addVec1Scalar(long pout, long pin, float k) {
		float f0 = k + unsafe.getFloat(pin);
		unsafe.putFloat(pout, f0);
	}
	
	public static void addVec2Scalar(long pout, long pin, float k) {
		float f0 = k + unsafe.getFloat(pin);
		float f1 = k + unsafe.getFloat(pin + 4);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
	}
	
	public static void addVec3Scalar(long pout, long pin, float k) {
		float f0 = k + unsafe.getFloat(pin);
		float f1 = k + unsafe.getFloat(pin + 4);
		float f2 = k + unsafe.getFloat(pin + 8);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
	}
	
	public static void addVec4Scalar(long pout, long pin, float k) {
		float f0 = k + unsafe.getFloat(pin);
		float f1 = k + unsafe.getFloat(pin + 4);
		float f2 = k + unsafe.getFloat(pin + 8);
		float f3 = k + unsafe.getFloat(pin + 12);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
		unsafe.putFloat(pout + 12, f3);
	}
	
	public static void addVec1(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		f0 += unsafe.getFloat(pin1);
		unsafe.putFloat(pout, f0);
	}
	
	public static void addVec2(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		f0 += unsafe.getFloat(pin1);
		f1 += unsafe.getFloat(pin1 + 4);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
	}
	
	public static void addVec3(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		f0 += unsafe.getFloat(pin1);
		f1 += unsafe.getFloat(pin1 + 4);
		f2 += unsafe.getFloat(pin1 + 8);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
	}
	
	public static void addVec4(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		float f3 = unsafe.getFloat(pin0 + 12);
		f0 += unsafe.getFloat(pin1);
		f1 += unsafe.getFloat(pin1 + 4);
		f2 += unsafe.getFloat(pin1 + 8);
		f3 += unsafe.getFloat(pin1 + 12);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
		unsafe.putFloat(pout + 12, f3);
	}
	
	public static void subVec1(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		f0 -= unsafe.getFloat(pin1);
		unsafe.putFloat(pout, f0);
	}
	
	public static void subVec2(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		f0 -= unsafe.getFloat(pin1);
		f1 -= unsafe.getFloat(pin1 + 4);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
	}
	
	public static void subVec3(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		f0 -= unsafe.getFloat(pin1);
		f1 -= unsafe.getFloat(pin1 + 4);
		f2 -= unsafe.getFloat(pin1 + 8);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
	}
	
	public static void subVec4(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		float f3 = unsafe.getFloat(pin0 + 12);
		f0 -= unsafe.getFloat(pin1);
		f1 -= unsafe.getFloat(pin1 + 4);
		f2 -= unsafe.getFloat(pin1 + 8);
		f3 -= unsafe.getFloat(pin1 + 12);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
		unsafe.putFloat(pout + 12, f3);
	}
	
	public static void mulVec1Scalar(long pout, long pin, float k) {
		float f0 = k * unsafe.getFloat(pin);
		unsafe.putFloat(pout, f0);
	}
	
	public static void mulVec2Scalar(long pout, long pin, float k) {
		float f0 = k * unsafe.getFloat(pin);
		float f1 = k * unsafe.getFloat(pin + 4);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
	}
	
	public static void mulVec3Scalar(long pout, long pin, float k) {
		float f0 = k * unsafe.getFloat(pin);
		float f1 = k * unsafe.getFloat(pin + 4);
		float f2 = k * unsafe.getFloat(pin + 8);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
	}
	
	public static void mulVec4Scalar(long pout, long pin, float k) {
		float f0 = k * unsafe.getFloat(pin);
		float f1 = k * unsafe.getFloat(pin + 4);
		float f2 = k * unsafe.getFloat(pin + 8);
		float f3 = k * unsafe.getFloat(pin + 12);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
		unsafe.putFloat(pout + 12, f3);
	}

	public static void mulVec1(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		f0 *= unsafe.getFloat(pin1);
		unsafe.putFloat(pout, f0);
	}
	
	public static void mulVec2(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		f0 *= unsafe.getFloat(pin1);
		f1 *= unsafe.getFloat(pin1 + 4);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
	}
	
	public static void mulVec3(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		f0 *= unsafe.getFloat(pin1);
		f1 *= unsafe.getFloat(pin1 + 4);
		f2 *= unsafe.getFloat(pin1 + 8);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
	}
	
	public static void mulVec4(long pout, long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		float f3 = unsafe.getFloat(pin0 + 12);
		f0 *= unsafe.getFloat(pin1);
		f1 *= unsafe.getFloat(pin1 + 4);
		f2 *= unsafe.getFloat(pin1 + 8);
		f3 *= unsafe.getFloat(pin1 + 12);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
		unsafe.putFloat(pout + 12, f3);
	}

	public static void fmaVec1(long pout, long pin0, long pin1, long pin_add) {
		float f0 = unsafe.getFloat(pin0);
		f0 *= unsafe.getFloat(pin1);
		f0 += unsafe.getFloat(pin_add);
		unsafe.putFloat(pout, f0);
	}
	
	public static void fmaVec2(long pout, long pin0, long pin1, long pin_add) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		f0 *= unsafe.getFloat(pin1);
		f1 *= unsafe.getFloat(pin1 + 4);
		f0 += unsafe.getFloat(pin_add);
		f1 += unsafe.getFloat(pin_add + 4);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
	}
	
	public static void fmaVec3(long pout, long pin0, long pin1, long pin_add) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		f0 *= unsafe.getFloat(pin1);
		f1 *= unsafe.getFloat(pin1 + 4);
		f2 *= unsafe.getFloat(pin1 + 8);
		f0 += unsafe.getFloat(pin_add);
		f1 += unsafe.getFloat(pin_add + 4);
		f2 += unsafe.getFloat(pin_add + 8);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
	}
	
	public static void fmaVec4(long pout, long pin0, long pin1, long pin_add) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		float f3 = unsafe.getFloat(pin0 + 12);
		f0 *= unsafe.getFloat(pin1);
		f1 *= unsafe.getFloat(pin1 + 4);
		f2 *= unsafe.getFloat(pin1 + 8);
		f3 *= unsafe.getFloat(pin1 + 12);
		f0 += unsafe.getFloat(pin_add);
		f1 += unsafe.getFloat(pin_add + 4);
		f2 += unsafe.getFloat(pin_add + 8);
		f3 += unsafe.getFloat(pin_add + 12);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
		unsafe.putFloat(pout + 12, f3);
	}

	public static void fmaVec1Scalar(long pout, long pin0, float k, long pin_add) {
		float f0 = k * unsafe.getFloat(pin0);
		f0 += unsafe.getFloat(pin_add);
		unsafe.putFloat(pout, f0);
	}
	
	public static void fmaVec2Scalar(long pout, long pin0, float k, long pin_add) {
		float f0 = k * unsafe.getFloat(pin0);
		float f1 = k * unsafe.getFloat(pin0 + 4);
		f0 += unsafe.getFloat(pin_add);
		f1 += unsafe.getFloat(pin_add + 4);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
	}
	
	public static void fmaVec3Scalar(long pout, long pin0, float k, long pin_add) {
		float f0 = k * unsafe.getFloat(pin0);
		float f1 = k * unsafe.getFloat(pin0 + 4);
		float f2 = k * unsafe.getFloat(pin0 + 8);
		f0 += unsafe.getFloat(pin_add);
		f1 += unsafe.getFloat(pin_add + 4);
		f2 += unsafe.getFloat(pin_add + 8);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
	}
	
	public static void fmaVec4Scalar(long pout, long pin0, float k, long pin_add) {
		float f0 = k * unsafe.getFloat(pin0);
		float f1 = k * unsafe.getFloat(pin0 + 4);
		float f2 = k * unsafe.getFloat(pin0 + 8);
		float f3 = k * unsafe.getFloat(pin0 + 12);
		f0 += unsafe.getFloat(pin_add);
		f1 += unsafe.getFloat(pin_add + 4);
		f2 += unsafe.getFloat(pin_add + 8);
		f3 += unsafe.getFloat(pin_add + 12);
		unsafe.putFloat(pout, f0);
		unsafe.putFloat(pout + 4, f1);
		unsafe.putFloat(pout + 8, f2);
		unsafe.putFloat(pout + 12, f3);
	}
	
	public static float dotVec1Vec1(long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		f0 *= unsafe.getFloat(pin1);
		return f0;
	}
	
	public static float dotVec2Vec2(long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		f0 *= unsafe.getFloat(pin1);
		f1 *= unsafe.getFloat(pin1 + 4);
		return f0 + f1;
	}
	
	public static float dotVec3Vec3(long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		f0 *= unsafe.getFloat(pin1);
		f1 *= unsafe.getFloat(pin1 + 4);
		f2 *= unsafe.getFloat(pin1 + 8);
		return f0 + f1 + f2;
	}
	
	public static float dotVec4Vec4(long pin0, long pin1) {
		float f0 = unsafe.getFloat(pin0);
		float f1 = unsafe.getFloat(pin0 + 4);
		float f2 = unsafe.getFloat(pin0 + 8);
		float f3 = unsafe.getFloat(pin0 + 12);
		f0 *= unsafe.getFloat(pin1);
		f1 *= unsafe.getFloat(pin1 + 4);
		f2 *= unsafe.getFloat(pin1 + 8);
		f3 *= unsafe.getFloat(pin1 + 12);
		return f0 + f1 + f2 + f3;
	}
	
	public static void mulMat4Vec4(long pout, long pmin, long pvin) {
		float m00 = unsafe.getFloat(pmin);
		float m01 = unsafe.getFloat(pmin += 4);
		float m02 = unsafe.getFloat(pmin += 4);
		float m03 = unsafe.getFloat(pmin += 4);
		float m10 = unsafe.getFloat(pmin += 4);
		float m11 = unsafe.getFloat(pmin += 4);
		float m12 = unsafe.getFloat(pmin += 4);
		float m13 = unsafe.getFloat(pmin += 4);
		float m20 = unsafe.getFloat(pmin += 4);
		float m21 = unsafe.getFloat(pmin += 4);
		float m22 = unsafe.getFloat(pmin += 4);
		float m23 = unsafe.getFloat(pmin += 4);
		float m30 = unsafe.getFloat(pmin += 4);
		float m31 = unsafe.getFloat(pmin += 4);
		float m32 = unsafe.getFloat(pmin += 4);
		float m33 = unsafe.getFloat(pmin += 4);
		float v0 = unsafe.getFloat(pvin);
		float v1 = unsafe.getFloat(pvin += 4);
		float v2 = unsafe.getFloat(pvin += 4);
		float v3 = unsafe.getFloat(pvin += 4);
		float o0 = v0 * m00 + v1 * m10 + v2 * m20 + v3 * m30;
		float o1 = v0 * m01 + v1 * m11 + v2 * m21 + v3 * m31;
		float o2 = v0 * m02 + v1 * m12 + v2 * m22 + v3 * m32;
		float o3 = v0 * m03 + v1 * m13 + v2 * m23 + v3 * m33;
		unsafe.putFloat(pout, o0);
		unsafe.putFloat(pout + 4, o1);
		unsafe.putFloat(pout + 8, o2);
		unsafe.putFloat(pout + 12, o3);
	}
	
	public static void interp3Vec1(long pout, long pin0, long pin1, long pin2, float k0, float k1, float k2) {
		mulVec1Scalar(pout, pin0, k0);
		fmaVec1Scalar(pout, pin1, k1, pout);
		fmaVec1Scalar(pout, pin2, k2, pout);
	}
	
	public static void interp3Vec2(long pout, long pin0, long pin1, long pin2, float k0, float k1, float k2) {
		mulVec2Scalar(pout, pin0, k0);
		fmaVec2Scalar(pout, pin1, k1, pout);
		fmaVec2Scalar(pout, pin2, k2, pout);
	}
	
	public static void interp3Vec3(long pout, long pin0, long pin1, long pin2, float k0, float k1, float k2) {
		mulVec3Scalar(pout, pin0, k0);
		fmaVec3Scalar(pout, pin1, k1, pout);
		fmaVec3Scalar(pout, pin2, k2, pout);
	}
	
	public static void interp3Vec4(long pout, long pin0, long pin1, long pin2, float k0, float k1, float k2) {
		mulVec4Scalar(pout, pin0, k0);
		fmaVec4Scalar(pout, pin1, k1, pout);
		fmaVec4Scalar(pout, pin2, k2, pout);
	}
	
	public static int crc32(long p, int sz) {
		CRC32 crc = new CRC32();
		ByteBuffer b = mapBuffer(p, sz);
		crc.update(b);
		return (int) crc.getValue();
	}
	
	public static int gcd(int a, int b) {
		if (b == 0) return a;
		return gcd(b, a % b);
	}
	
}
