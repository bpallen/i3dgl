package initial3d;

import initial3d.detail.StringUtil;

public class Vec2d {

	public static final Vec2d zero = new Vec2d(0, 0);
	
	public static final Vec2d i = new Vec2d(1, 0);
	public static final Vec2d j = new Vec2d(0, 1);

	public final double x;
	public final double y;

	public Vec2d() {
		this(0, 0);
	}

	public Vec2d(double x_, double y_) {
		x = x_;
		y = y_;
	}

	public double get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec2d with(int i, double val) {
		switch (i) {
		case 0:
			return new Vec2d(val, y);
		case 1:
			return new Vec2d(x, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec2d withX(double x_) {
		return new Vec2d(x_, y);
	}
	
	public Vec2d withY(double y_) {
		return new Vec2d(x, y_);
	}

	public Vec2d neg() {
		return new Vec2d(-x, -y);
	}
	
	public Vec2d add(Vec2d rhs) {
		return new Vec2d(x + rhs.x, y + rhs.y);
	}

	public Vec2d add(double rhs) {
		return new Vec2d(x + rhs, y + rhs);
	}

	public Vec2d sub(Vec2d rhs) {
		return new Vec2d(x - rhs.x, y - rhs.y);
	}

	public Vec2d sub(double rhs) {
		return new Vec2d(x - rhs, y - rhs);
	}

	public Vec2d mul(Vec2d rhs) {
		return new Vec2d(x * rhs.x, y * rhs.y);
	}

	public Vec2d mul(double rhs) {
		return new Vec2d(x * rhs, y * rhs);
	}

	public Vec2d div(Vec2d rhs) {
		return new Vec2d(x / rhs.x, y / rhs.y);
	}

	public Vec2d div(double rhs) {
		return new Vec2d(x / rhs, y / rhs);
	}

	public Vec2b lt(Vec2d rhs) {
		return new Vec2b(x < rhs.x, y < rhs.y);
	}
	
	public Vec2b le(Vec2d rhs) {
		return new Vec2b(x <= rhs.x, y <= rhs.y);
	}
	
	public Vec2b eq(Vec2d rhs) {
		return new Vec2b(x == rhs.x, y == rhs.y);
	}
	
	public Vec2b ne(Vec2d rhs) {
		return new Vec2b(x != rhs.x, y != rhs.y);
	}
	
	public Vec2b ge(Vec2d rhs) {
		return new Vec2b(x >= rhs.x, y >= rhs.y);
	}
	
	public Vec2b gt(Vec2d rhs) {
		return new Vec2b(x > rhs.x, y > rhs.y);
	}
	
	@Override
	public String toString() {
		final int prec = 5;
		return String.format(
			"(%s, %s)",
			StringUtil.formatDouble(x, prec),
			StringUtil.formatDouble(y, prec)
		);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec2d other = (Vec2d) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) return false;
		return true;
	}
	
}
