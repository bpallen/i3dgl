package initial3d;

/**
 * Initial3D functions. <code>import static initial3d.Functions.*;</code> to use.
 */
public class Functions {

	private Functions() {
		throw new AssertionError("Not constructible");
	}

	/*
	 * Manually authored functions
	 */

	public static boolean asBoolean(boolean x) {
		return x;
	}

	public static boolean asBoolean(double x) {
		return x != 0.0;
	}

	public static boolean asBoolean(float x) {
		return x != 0.f;
	}

	public static boolean asBoolean(int x) {
		return x != 0;
	}

	public static double asDouble(boolean x) {
		return x ? 1.0 : 0.0;
	}

	public static double asDouble(double x) {
		return x;
	}

	public static double asDouble(float x) {
		return x;
	}

	public static double asDouble(int x) {
		return x;
	}

	public static float asFloat(boolean x) {
		return x ? 1.f : 0.f;
	}

	public static float asFloat(double x) {
		return (float) x;
	}

	public static float asFloat(float x) {
		return x;
	}

	public static float asFloat(int x) {
		return x;
	}

	public static int asInt(boolean x) {
		return x ? 1 : 0;
	}

	public static int asInt(double x) {
		return (int) x;
	}

	public static int asInt(float x) {
		return (int) x;
	}

	public static int asInt(int x) {
		return x;
	}

	public static Vec2d min(Vec2d x, double y) {
		return new Vec2d(Math.min(x.x, y), Math.min(x.y, y));
	}

	public static Vec2f min(Vec2f x, float y) {
		return new Vec2f(Math.min(x.x, y), Math.min(x.y, y));
	}

	public static Vec2i min(Vec2i x, int y) {
		return new Vec2i(Math.min(x.x, y), Math.min(x.y, y));
	}

	public static Vec3d min(Vec3d x, double y) {
		return new Vec3d(Math.min(x.x, y), Math.min(x.y, y), Math.min(x.z, y));
	}

	public static Vec3f min(Vec3f x, float y) {
		return new Vec3f(Math.min(x.x, y), Math.min(x.y, y), Math.min(x.z, y));
	}

	public static Vec3i min(Vec3i x, int y) {
		return new Vec3i(Math.min(x.x, y), Math.min(x.y, y), Math.min(x.z, y));
	}

	public static Vec4d min(Vec4d x, double y) {
		return new Vec4d(Math.min(x.x, y), Math.min(x.y, y), Math.min(x.z, y), Math.min(x.w, y));
	}

	public static Vec4f min(Vec4f x, float y) {
		return new Vec4f(Math.min(x.x, y), Math.min(x.y, y), Math.min(x.z, y), Math.min(x.w, y));
	}

	public static Vec4i min(Vec4i x, int y) {
		return new Vec4i(Math.min(x.x, y), Math.min(x.y, y), Math.min(x.z, y), Math.min(x.w, y));
	}

	public static Vec2d max(Vec2d x, double y) {
		return new Vec2d(Math.max(x.x, y), Math.max(x.y, y));
	}

	public static Vec2f max(Vec2f x, float y) {
		return new Vec2f(Math.max(x.x, y), Math.max(x.y, y));
	}

	public static Vec2i max(Vec2i x, int y) {
		return new Vec2i(Math.max(x.x, y), Math.max(x.y, y));
	}

	public static Vec3d max(Vec3d x, double y) {
		return new Vec3d(Math.max(x.x, y), Math.max(x.y, y), Math.max(x.z, y));
	}

	public static Vec3f max(Vec3f x, float y) {
		return new Vec3f(Math.max(x.x, y), Math.max(x.y, y), Math.max(x.z, y));
	}

	public static Vec3i max(Vec3i x, int y) {
		return new Vec3i(Math.max(x.x, y), Math.max(x.y, y), Math.max(x.z, y));
	}

	public static Vec4d max(Vec4d x, double y) {
		return new Vec4d(Math.max(x.x, y), Math.max(x.y, y), Math.max(x.z, y), Math.max(x.w, y));
	}

	public static Vec4f max(Vec4f x, float y) {
		return new Vec4f(Math.max(x.x, y), Math.max(x.y, y), Math.max(x.z, y), Math.max(x.w, y));
	}

	public static Vec4i max(Vec4i x, int y) {
		return new Vec4i(Math.max(x.x, y), Math.max(x.y, y), Math.max(x.z, y), Math.max(x.w, y));
	}

	public static Vec2d clamp(Vec2d x, double a, double b) {
		return new Vec2d(Math.min(Math.max(x.x, a), b), Math.min(Math.max(x.y, a), b));
	}

	public static Vec2f clamp(Vec2f x, float a, float b) {
		return new Vec2f(Math.min(Math.max(x.x, a), b), Math.min(Math.max(x.y, a), b));
	}

	public static Vec2i clamp(Vec2i x, int a, int b) {
		return new Vec2i(Math.min(Math.max(x.x, a), b), Math.min(Math.max(x.y, a), b));
	}

	public static Vec3d clamp(Vec3d x, double a, double b) {
		return new Vec3d(Math.min(Math.max(x.x, a), b), Math.min(Math.max(x.y, a), b), Math.min(Math.max(x.z, a), b));
	}

	public static Vec3f clamp(Vec3f x, float a, float b) {
		return new Vec3f(Math.min(Math.max(x.x, a), b), Math.min(Math.max(x.y, a), b), Math.min(Math.max(x.z, a), b));
	}

	public static Vec3i clamp(Vec3i x, int a, int b) {
		return new Vec3i(Math.min(Math.max(x.x, a), b), Math.min(Math.max(x.y, a), b), Math.min(Math.max(x.z, a), b));
	}

	public static Vec4d clamp(Vec4d x, double a, double b) {
		return new Vec4d(Math.min(Math.max(x.x, a), b), Math.min(Math.max(x.y, a), b), Math.min(Math.max(x.z, a), b), Math.min(Math.max(x.w, a), b));
	}

	public static Vec4f clamp(Vec4f x, float a, float b) {
		return new Vec4f(Math.min(Math.max(x.x, a), b), Math.min(Math.max(x.y, a), b), Math.min(Math.max(x.z, a), b), Math.min(Math.max(x.w, a), b));
	}

	public static Vec4i clamp(Vec4i x, int a, int b) {
		return new Vec4i(Math.min(Math.max(x.x, a), b), Math.min(Math.max(x.y, a), b), Math.min(Math.max(x.z, a), b), Math.min(Math.max(x.w, a), b));
	}

	public static double dot(Vec2d x, Vec2d y) {
		return x.x * y.x + x.y * y.y;
	}

	public static float dot(Vec2f x, Vec2f y) {
		return x.x * y.x + x.y * y.y;
	}

	public static int dot(Vec2i x, Vec2i y) {
		return x.x * y.x + x.y * y.y;
	}

	public static double dot(Vec3d x, Vec3d y) {
		return x.x * y.x + x.y * y.y + x.z * y.z;
	}

	public static float dot(Vec3f x, Vec3f y) {
		return x.x * y.x + x.y * y.y + x.z * y.z;
	}

	public static int dot(Vec3i x, Vec3i y) {
		return x.x * y.x + x.y * y.y + x.z * y.z;
	}

	public static double dot(Vec4d x, Vec4d y) {
		return x.x * y.x + x.y * y.y + x.z * y.z + x.w * y.w;
	}

	public static float dot(Vec4f x, Vec4f y) {
		return x.x * y.x + x.y * y.y + x.z * y.z + x.w * y.w;
	}

	public static int dot(Vec4i x, Vec4i y) {
		return x.x * y.x + x.y * y.y + x.z * y.z + x.w * y.w;
	}

	public static Vec3d cross(Vec3d x, Vec3d y) {
		return vec3d(x.y * y.z - x.z * y.y, x.z * y.x - x.x * y.z, x.x * y.y - x.y * y.x);
	}

	public static Vec3f cross(Vec3f x, Vec3f y) {
		return vec3f(x.y * y.z - x.z * y.y, x.z * y.x - x.x * y.z, x.x * y.y - x.y * y.x);
	}

	public static Vec3i cross(Vec3i x, Vec3i y) {
		return vec3i(x.y * y.z - x.z * y.y, x.z * y.x - x.x * y.z, x.x * y.y - x.y * y.x);
	}

	public static double norm(Vec2d x) {
		return sqrt(x.x * x.x + x.y * x.y);
	}

	public static float norm(Vec2f x) {
		return sqrt(x.x * x.x + x.y * x.y);
	}

	public static double norm(Vec3d x) {
		return sqrt(x.x * x.x + x.y * x.y + x.z * x.z);
	}

	public static float norm(Vec3f x) {
		return sqrt(x.x * x.x + x.y * x.y + x.z * x.z);
	}

	public static double norm(Vec4d x) {
		return sqrt(x.x * x.x + x.y * x.y + x.z * x.z + x.w * x.w);
	}

	public static float norm(Vec4f x) {
		return sqrt(x.x * x.x + x.y * x.y + x.z * x.z + x.w * x.w);
	}

	public static Vec2d normalize(Vec2d x) {
		return x.mul(1.0 / norm(x));
	}

	public static Vec2f normalize(Vec2f x) {
		return x.mul(1.f / norm(x));
	}

	public static Vec3d normalize(Vec3d x) {
		return x.mul(1.0 / norm(x));
	}

	public static Vec3f normalize(Vec3f x) {
		return x.mul(1.f / norm(x));
	}

	public static Vec4d normalize(Vec4d x) {
		return x.mul(1.0 / norm(x));
	}

	public static Vec4f normalize(Vec4f x) {
		return x.mul(1.f / norm(x));
	}

	public static Vec2d homogenize(Vec2d x) {
		return vec2d(x.x / x.y, 1.0);
	}

	public static Vec2f homogenize(Vec2f x) {
		return vec2f(x.x / x.y, 1.f);
	}

	public static Vec3d homogenize(Vec3d x) {
		return x.mul(1.0 / x.z);
	}

	public static Vec3f homogenize(Vec3f x) {
		return x.mul(1.f / x.z);
	}

	public static Vec4d homogenize(Vec4d x) {
		return x.mul(1.0 / x.w);
	}

	public static Vec4f homogenize(Vec4f x) {
		return x.mul(1.f / x.w);
	}

	public static Vec2d mix(Vec2d x, Vec2d y, double t) {
		return vec2d(x.x * (1 - t) + y.x * t, x.y * (1 - t) + y.y * t);
	}

	public static Vec2f mix(Vec2f x, Vec2f y, float t) {
		return vec2f(x.x * (1 - t) + y.x * t, x.y * (1 - t) + y.y * t);
	}

	public static Vec3d mix(Vec3d x, Vec3d y, double t) {
		return vec3d(x.x * (1 - t) + y.x * t, x.y * (1 - t) + y.y * t, x.z * (1 - t) + y.z * t);
	}

	public static Vec3f mix(Vec3f x, Vec3f y, float t) {
		return vec3f(x.x * (1 - t) + y.x * t, x.y * (1 - t) + y.y * t, x.z * (1 - t) + y.z * t);
	}

	public static Vec4d mix(Vec4d x, Vec4d y, double t) {
		return vec4d(x.x * (1 - t) + y.x * t, x.y * (1 - t) + y.y * t, x.z * (1 - t) + y.z * t, x.w * (1 - t) + y.w * t);
	}

	public static Vec4f mix(Vec4f x, Vec4f y, float t) {
		return vec4f(x.x * (1 - t) + y.x * t, x.y * (1 - t) + y.y * t, x.z * (1 - t) + y.z * t, x.w * (1 - t) + y.w * t);
	}

	public static boolean any(boolean x) {
		return x;
	}

	public static boolean any(Vec2b x) {
		return x.x || x.y;
	}

	public static boolean any(Vec3b x) {
		return x.x || x.y || x.z;
	}

	public static boolean any(Vec4b x) {
		return x.x || x.y || x.z || x.w;
	}

	public static boolean all(boolean x) {
		return x;
	}

	public static boolean all(Vec2b x) {
		return x.x && x.y;
	}

	public static boolean all(Vec3b x) {
		return x.x && x.y && x.z;
	}

	public static boolean all(Vec4b x) {
		return x.x && x.y && x.z && x.w;
	}

	public static double rand1d() {
		return Math.random();
	}

	public static Vec2d rand2d() {
		return vec2d(Math.random(), Math.random());
	}

	public static Vec3d rand3d() {
		return vec3d(Math.random(), Math.random(), Math.random());
	}

	public static Vec4d rand4d() {
		return vec4d(Math.random(), Math.random(), Math.random(), Math.random());
	}

	public static float rand1f() {
		return asFloat(Math.random());
	}

	public static Vec2f rand2f() {
		return vec2f(Math.random(), Math.random());
	}

	public static Vec3f rand3f() {
		return vec3f(Math.random(), Math.random(), Math.random());
	}

	public static Vec4f rand4f() {
		return vec4f(Math.random(), Math.random(), Math.random(), Math.random());
	}

	public static Mat2d mat2d(double x) {
		return new Mat2d(x);
	}

	public static Mat2d mat2d(Vec2d c0, Vec2d c1) {
		return new Mat2d(c0.x, c0.y, c1.x, c1.y);
	}

	public static Mat2d mat2d(Mat2d x) {
		return new Mat2d(asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(0, 1)), asDouble(x.get(1, 1)));
	}

	public static Mat2d mat2d(Mat2f x) {
		return new Mat2d(asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(0, 1)), asDouble(x.get(1, 1)));
	}

	public static Mat2d mat2d(Mat3d x) {
		return new Mat2d(asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(0, 1)), asDouble(x.get(1, 1)));
	}

	public static Mat2d mat2d(Mat3f x) {
		return new Mat2d(asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(0, 1)), asDouble(x.get(1, 1)));
	}

	public static Mat2d mat2d(Mat4d x) {
		return new Mat2d(asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(0, 1)), asDouble(x.get(1, 1)));
	}

	public static Mat2d mat2d(Mat4f x) {
		return new Mat2d(asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(0, 1)), asDouble(x.get(1, 1)));
	}

	public static Mat2d mat2d(double e00, double e01, double e10, double e11) {
		return new Mat2d(e00, e01, e10, e11);
	}

	public static Mat3d mat3d(double x) {
		return new Mat3d(x);
	}

	public static Mat3d mat3d(Vec3d c0, Vec3d c1, Vec3d c2) {
		return new Mat3d(c0.x, c0.y, c0.z, c1.x, c1.y, c1.z, c2.x, c2.y, c2.z);
	}

	public static Mat3d mat3d(Mat2d x) {
		return new Mat3d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), 0,
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), 0,
			0, 0, 1
		);
	}

	public static Mat3d mat3d(Mat2f x) {
		return new Mat3d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), 0,
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), 0,
			0, 0, 1
		);
	}

	public static Mat3d mat3d(Mat3d x) {
		return new Mat3d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(2, 0)),
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), asDouble(x.get(2, 1)),
			asDouble(x.get(0, 2)), asDouble(x.get(1, 2)), asDouble(x.get(2, 2))
		);
	}

	public static Mat3d mat3d(Mat3f x) {
		return new Mat3d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(2, 0)),
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), asDouble(x.get(2, 1)),
			asDouble(x.get(0, 2)), asDouble(x.get(1, 2)), asDouble(x.get(2, 2))
		);
	}

	public static Mat3d mat3d(Mat4d x) {
		return new Mat3d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(2, 0)),
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), asDouble(x.get(2, 1)),
			asDouble(x.get(0, 2)), asDouble(x.get(1, 2)), asDouble(x.get(2, 2))
		);
	}

	public static Mat3d mat3d(Mat4f x) {
		return new Mat3d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(2, 0)),
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), asDouble(x.get(2, 1)),
			asDouble(x.get(0, 2)), asDouble(x.get(1, 2)), asDouble(x.get(2, 2))
		);
	}

	public static Mat3d mat3d(
		double e00, double e01, double e02,
		double e10, double e11, double e12,
		double e20, double e21, double e22
	) {
		return new Mat3d(e00, e01, e02, e10, e11, e12, e20, e21, e22);
	}

	public static Mat4d mat4d(double x) {
		return new Mat4d(x);
	}

	public static Mat4d mat4d(Vec4d c0, Vec4d c1, Vec4d c2, Vec4d c3) {
		return new Mat4d(c0.x, c0.y, c0.z, c0.w, c1.x, c1.y, c1.z, c1.w, c2.x, c2.y, c2.z, c2.w, c3.x, c3.y, c3.z, c3.w);
	}

	public static Mat4d mat4d(Mat2d x) {
		return new Mat4d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), 0, 0,
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		);
	}

	public static Mat4d mat4d(Mat2f x) {
		return new Mat4d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), 0, 0,
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		);
	}

	public static Mat4d mat4d(Mat3d x) {
		return new Mat4d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(2, 0)), 0,
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), asDouble(x.get(2, 1)), 0,
			asDouble(x.get(0, 2)), asDouble(x.get(1, 2)), asDouble(x.get(2, 2)), 0,
			0, 0, 0, 1
		);
	}

	public static Mat4d mat4d(Mat3f x) {
		return new Mat4d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(2, 0)), 0,
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), asDouble(x.get(2, 1)), 0,
			asDouble(x.get(0, 2)), asDouble(x.get(1, 2)), asDouble(x.get(2, 2)), 0,
			0, 0, 0, 1
		);
	}

	public static Mat4d mat4d(Mat4d x) {
		return new Mat4d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(2, 0)), asDouble(x.get(3, 0)),
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), asDouble(x.get(2, 1)), asDouble(x.get(3, 1)),
			asDouble(x.get(0, 2)), asDouble(x.get(1, 2)), asDouble(x.get(2, 2)), asDouble(x.get(3, 2)),
			asDouble(x.get(0, 3)), asDouble(x.get(1, 3)), asDouble(x.get(2, 3)), asDouble(x.get(3, 3))
		);
	}

	public static Mat4d mat4d(Mat4f x) {
		return new Mat4d(
			asDouble(x.get(0, 0)), asDouble(x.get(1, 0)), asDouble(x.get(2, 0)), asDouble(x.get(3, 0)),
			asDouble(x.get(0, 1)), asDouble(x.get(1, 1)), asDouble(x.get(2, 1)), asDouble(x.get(3, 1)),
			asDouble(x.get(0, 2)), asDouble(x.get(1, 2)), asDouble(x.get(2, 2)), asDouble(x.get(3, 2)),
			asDouble(x.get(0, 3)), asDouble(x.get(1, 3)), asDouble(x.get(2, 3)), asDouble(x.get(3, 3))
		);
	}

	public static Mat4d mat4d(
		double e00, double e01, double e02, double e03,
		double e10, double e11, double e12, double e13,
		double e20, double e21, double e22, double e23,
		double e30, double e31, double e32, double e33
	) {
		return new Mat4d(e00, e01, e02, e03, e10, e11, e12, e13, e20, e21, e22, e23, e30, e31, e32, e33);
	}

	public static Mat2f mat2f(float x) {
		return new Mat2f(x);
	}

	public static Mat2f mat2f(Vec2f c0, Vec2f c1) {
		return new Mat2f(c0.x, c0.y, c1.x, c1.y);
	}

	public static Mat2f mat2f(Mat2d x) {
		return new Mat2f(asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(0, 1)), asFloat(x.get(1, 1)));
	}

	public static Mat2f mat2f(Mat2f x) {
		return new Mat2f(asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(0, 1)), asFloat(x.get(1, 1)));
	}

	public static Mat2f mat2f(Mat3d x) {
		return new Mat2f(asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(0, 1)), asFloat(x.get(1, 1)));
	}

	public static Mat2f mat2f(Mat3f x) {
		return new Mat2f(asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(0, 1)), asFloat(x.get(1, 1)));
	}

	public static Mat2f mat2f(Mat4d x) {
		return new Mat2f(asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(0, 1)), asFloat(x.get(1, 1)));
	}

	public static Mat2f mat2f(Mat4f x) {
		return new Mat2f(asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(0, 1)), asFloat(x.get(1, 1)));
	}

	public static Mat2f mat2f(float e00, float e01, float e10, float e11) {
		return new Mat2f(e00, e01, e10, e11);
	}

	public static Mat3f mat3f(float x) {
		return new Mat3f(x);
	}

	public static Mat3f mat3f(Vec3f c0, Vec3f c1, Vec3f c2) {
		return new Mat3f(c0.x, c0.y, c0.z, c1.x, c1.y, c1.z, c2.x, c2.y, c2.z);
	}

	public static Mat3f mat3f(Mat2d x) {
		return new Mat3f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), 0,
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), 0,
			0, 0, 1
		);
	}

	public static Mat3f mat3f(Mat2f x) {
		return new Mat3f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), 0,
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), 0,
			0, 0, 1
		);
	}

	public static Mat3f mat3f(Mat3d x) {
		return new Mat3f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(2, 0)),
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), asFloat(x.get(2, 1)),
			asFloat(x.get(0, 2)), asFloat(x.get(1, 2)), asFloat(x.get(2, 2))
		);
	}

	public static Mat3f mat3f(Mat3f x) {
		return new Mat3f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(2, 0)),
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), asFloat(x.get(2, 1)),
			asFloat(x.get(0, 2)), asFloat(x.get(1, 2)), asFloat(x.get(2, 2))
		);
	}

	public static Mat3f mat3f(Mat4d x) {
		return new Mat3f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(2, 0)),
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), asFloat(x.get(2, 1)),
			asFloat(x.get(0, 2)), asFloat(x.get(1, 2)), asFloat(x.get(2, 2))
		);
	}

	public static Mat3f mat3f(Mat4f x) {
		return new Mat3f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(2, 0)),
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), asFloat(x.get(2, 1)),
			asFloat(x.get(0, 2)), asFloat(x.get(1, 2)), asFloat(x.get(2, 2))
		);
	}

	public static Mat3f mat3f(
		float e00, float e01, float e02,
		float e10, float e11, float e12,
		float e20, float e21, float e22
	) {
		return new Mat3f(e00, e01, e02, e10, e11, e12, e20, e21, e22);
	}

	public static Mat4f mat4f(float x) {
		return new Mat4f(x);
	}

	public static Mat4f mat4f(Vec4f c0, Vec4f c1, Vec4f c2, Vec4f c3) {
		return new Mat4f(c0.x, c0.y, c0.z, c0.w, c1.x, c1.y, c1.z, c1.w, c2.x, c2.y, c2.z, c2.w, c3.x, c3.y, c3.z, c3.w);
	}

	public static Mat4f mat4f(Mat2d x) {
		return new Mat4f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), 0, 0,
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		);
	}

	public static Mat4f mat4f(Mat2f x) {
		return new Mat4f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), 0, 0,
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		);
	}

	public static Mat4f mat4f(Mat3d x) {
		return new Mat4f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(2, 0)), 0,
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), asFloat(x.get(2, 1)), 0,
			asFloat(x.get(0, 2)), asFloat(x.get(1, 2)), asFloat(x.get(2, 2)), 0,
			0, 0, 0, 1
		);
	}

	public static Mat4f mat4f(Mat3f x) {
		return new Mat4f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(2, 0)), 0,
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), asFloat(x.get(2, 1)), 0,
			asFloat(x.get(0, 2)), asFloat(x.get(1, 2)), asFloat(x.get(2, 2)), 0,
			0, 0, 0, 1
		);
	}

	public static Mat4f mat4f(Mat4d x) {
		return new Mat4f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(2, 0)), asFloat(x.get(3, 0)),
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), asFloat(x.get(2, 1)), asFloat(x.get(3, 1)),
			asFloat(x.get(0, 2)), asFloat(x.get(1, 2)), asFloat(x.get(2, 2)), asFloat(x.get(3, 2)),
			asFloat(x.get(0, 3)), asFloat(x.get(1, 3)), asFloat(x.get(2, 3)), asFloat(x.get(3, 3))
		);
	}

	public static Mat4f mat4f(Mat4f x) {
		return new Mat4f(
			asFloat(x.get(0, 0)), asFloat(x.get(1, 0)), asFloat(x.get(2, 0)), asFloat(x.get(3, 0)),
			asFloat(x.get(0, 1)), asFloat(x.get(1, 1)), asFloat(x.get(2, 1)), asFloat(x.get(3, 1)),
			asFloat(x.get(0, 2)), asFloat(x.get(1, 2)), asFloat(x.get(2, 2)), asFloat(x.get(3, 2)),
			asFloat(x.get(0, 3)), asFloat(x.get(1, 3)), asFloat(x.get(2, 3)), asFloat(x.get(3, 3))
		);
	}

	public static Mat4f mat4f(
		float e00, float e01, float e02, float e03,
		float e10, float e11, float e12, float e13,
		float e20, float e21, float e22, float e23,
		float e30, float e31, float e32, float e33
	) {
		return new Mat4f(e00, e01, e02, e03, e10, e11, e12, e13, e20, e21, e22, e23, e30, e31, e32, e33);
	}

	private static double determinant2(double e00, double e01, double e10, double e11) {
		return e00 * e11 - e01 * e10;
	}

	private static double determinant3(
		double e00, double e01, double e02,
		double e10, double e11, double e12,
		double e20, double e21, double e22
	) {
		return e00 * e11 * e22 + e10 * e21 * e02 + e20 * e01 * e12
			- e20 * e11 * e02 - e10 * e01 * e22 - e00 * e21 * e12;
	}

	private static double determinant4(
		double e00, double e01, double e02, double e03,
		double e10, double e11, double e12, double e13,
		double e20, double e21, double e22, double e23,
		double e30, double e31, double e32, double e33
	) {
		// expand about first column
		return e00 * determinant3(e11, e12, e13, e21, e22, e23, e31, e32, e33)
			- e10 * determinant3(e01, e02, e03, e21, e22, e23, e31, e32, e33)
			+ e20 * determinant3(e01, e02, e03, e11, e12, e13, e31, e32, e33)
			- e30 * determinant3(e01, e02, e03, e11, e12, e13, e21, e22, e23);
	}

	private static float determinant2(float e00, float e01, float e10, float e11) {
		return e00 * e11 - e01 * e10;
	}

	private static float determinant3(
		float e00, float e01, float e02,
		float e10, float e11, float e12,
		float e20, float e21, float e22
	) {
		return e00 * e11 * e22 + e10 * e21 * e02 + e20 * e01 * e12
			- e20 * e11 * e02 - e10 * e01 * e22 - e00 * e21 * e12;
	}

	private static float determinant4(
		float e00, float e01, float e02, float e03,
		float e10, float e11, float e12, float e13,
		float e20, float e21, float e22, float e23,
		float e30, float e31, float e32, float e33
	) {
		// expand about first column
		return e00 * determinant3(e11, e12, e13, e21, e22, e23, e31, e32, e33)
			- e10 * determinant3(e01, e02, e03, e21, e22, e23, e31, e32, e33)
			+ e20 * determinant3(e01, e02, e03, e11, e12, e13, e31, e32, e33)
			- e30 * determinant3(e01, e02, e03, e11, e12, e13, e21, e22, e23);
	}

	public static double determinant(Mat2d x) {
		return determinant2(x.get(0, 0), x.get(1, 0), x.get(0, 1), x.get(1, 1));
	}

	public static double determinant(Mat3d x) {
		return determinant3(
			x.get(0, 0), x.get(1, 0), x.get(2, 0),
			x.get(0, 1), x.get(1, 1), x.get(2, 1),
			x.get(0, 2), x.get(1, 2), x.get(2, 2)
		);
	}

	public static double determinant(Mat4d x) {
		return determinant4(
			x.get(0, 0), x.get(1, 0), x.get(2, 0), x.get(3, 0),
			x.get(0, 1), x.get(1, 1), x.get(2, 1), x.get(3, 1),
			x.get(0, 2), x.get(1, 2), x.get(2, 2), x.get(3, 2),
			x.get(0, 3), x.get(1, 3), x.get(2, 3), x.get(3, 3)
		);
	}

	public static float determinant(Mat2f x) {
		return determinant2(x.get(0, 0), x.get(1, 0), x.get(0, 1), x.get(1, 1));
	}

	public static float determinant(Mat3f x) {
		return determinant3(
			x.get(0, 0), x.get(1, 0), x.get(2, 0),
			x.get(0, 1), x.get(1, 1), x.get(2, 1),
			x.get(0, 2), x.get(1, 2), x.get(2, 2)
		);
	}

	public static float determinant(Mat4f x) {
		return determinant4(
			x.get(0, 0), x.get(1, 0), x.get(2, 0), x.get(3, 0),
			x.get(0, 1), x.get(1, 1), x.get(2, 1), x.get(3, 1),
			x.get(0, 2), x.get(1, 2), x.get(2, 2), x.get(3, 2),
			x.get(0, 3), x.get(1, 3), x.get(2, 3), x.get(3, 3)
		);
	}

	public static Mat2d transpose(Mat2d x) {
		return mat2d(x.get(0, 0), x.get(0, 1), x.get(1, 0), x.get(1, 1));
	}

	public static Mat3d transpose(Mat3d x) {
		return mat3d(
			x.get(0, 0), x.get(0, 1), x.get(0, 2),
			x.get(1, 0), x.get(1, 1), x.get(1, 2),
			x.get(2, 0), x.get(2, 1), x.get(2, 2)
		);
	}

	public static Mat4d transpose(Mat4d x) {
		return mat4d(
			x.get(0, 0), x.get(0, 1), x.get(0, 2), x.get(0, 3),
			x.get(1, 0), x.get(1, 1), x.get(1, 2), x.get(1, 3),
			x.get(2, 0), x.get(2, 1), x.get(2, 2), x.get(2, 3),
			x.get(3, 0), x.get(3, 1), x.get(3, 2), x.get(3, 3)
		);
	}

	public static Mat2f transpose(Mat2f x) {
		return mat2f(x.get(0, 0), x.get(0, 1), x.get(1, 0), x.get(1, 1));
	}

	public static Mat3f transpose(Mat3f x) {
		return mat3f(
			x.get(0, 0), x.get(0, 1), x.get(0, 2),
			x.get(1, 0), x.get(1, 1), x.get(1, 2),
			x.get(2, 0), x.get(2, 1), x.get(2, 2)
		);
	}

	public static Mat4f transpose(Mat4f x) {
		return mat4f(
			x.get(0, 0), x.get(0, 1), x.get(0, 2), x.get(0, 3),
			x.get(1, 0), x.get(1, 1), x.get(1, 2), x.get(1, 3),
			x.get(2, 0), x.get(2, 1), x.get(2, 2), x.get(2, 3),
			x.get(3, 0), x.get(3, 1), x.get(3, 2), x.get(3, 3)
		);
	}

	public static Mat2d inverse(Mat2d x) {
		double invdet = 1.0 / determinant(x);
		assert !isinf(invdet) && !isnan(invdet) && invdet != 0.0 : "Not invertible.";
		return mat2d(x.get(1, 1) * invdet, -x.get(1, 0) * invdet, -x.get(0, 1) * invdet, x.get(0, 0) * invdet);
	}

	public static Mat3d inverse(Mat3d x) {
		// first column of cofactors, can uses for determinant
		double c00 = determinant2(x.get(1, 1), x.get(2, 1), x.get(1, 2), x.get(2, 2));
		double c01 = -determinant2(x.get(0, 1), x.get(2, 1), x.get(0, 2), x.get(2, 2));
		double c02 = determinant2(x.get(0, 1), x.get(1, 1), x.get(0, 2), x.get(1, 2));
		// get determinant by expanding about first column
		double invdet = 1.0 / (x.get(0, 0) * c00 + x.get(1, 0) * c01 + x.get(2, 0) * c02);
		assert !isinf(invdet) && !isnan(invdet) && invdet != 0.0 : "Not invertible.";
		// transpose of cofactor matrix * (1 / det)
		double e00 = c00 * invdet;
		double e10 = c01 * invdet;
		double e20 = c02 * invdet;
		double e01 = -determinant2(x.get(1, 0), x.get(2, 0), x.get(1, 2), x.get(2, 2)) * invdet;
		double e11 = determinant2(x.get(0, 0), x.get(2, 0), x.get(0, 2), x.get(2, 2)) * invdet;
		double e21 = -determinant2(x.get(0, 0), x.get(1, 0), x.get(0, 2), x.get(1, 2)) * invdet;
		double e02 = determinant2(x.get(1, 0), x.get(2, 0), x.get(1, 1), x.get(2, 1)) * invdet;
		double e12 = -determinant2(x.get(0, 0), x.get(2, 0), x.get(0, 1), x.get(2, 1)) * invdet;
		double e22 = determinant2(x.get(0, 0), x.get(1, 0), x.get(0, 1), x.get(1, 1)) * invdet;
		return mat3d(e00, e01, e02, e10, e11, e12, e20, e21, e22);
	}

	public static Mat4d inverse(Mat4d x) {
		// first column of cofactors, can use for determinant
		double c00 = determinant3(x.get(1, 1), x.get(2, 1), x.get(3, 1), x.get(1, 2), x.get(2, 2), x.get(3, 2), x.get(1, 3), x.get(2, 3), x.get(3, 3));
		double c01 = -determinant3(x.get(0, 1), x.get(2, 1), x.get(3, 1), x.get(0, 2), x.get(2, 2), x.get(3, 2), x.get(0, 3), x.get(2, 3), x.get(3, 3));
		double c02 = determinant3(x.get(0, 1), x.get(1, 1), x.get(3, 1), x.get(0, 2), x.get(1, 2), x.get(3, 2), x.get(0, 3), x.get(1, 3), x.get(3, 3));
		double c03 = -determinant3(x.get(0, 1), x.get(1, 1), x.get(2, 1), x.get(0, 2), x.get(1, 2), x.get(2, 2), x.get(0, 3), x.get(1, 3), x.get(2, 3));
		// get determinant by expanding about first column
		double invdet = 1.0 / (x.get(0, 0) * c00 + x.get(1, 0) * c01 + x.get(2, 0) * c02 + x.get(3, 0) * c03);
		assert !isinf(invdet) && !isnan(invdet) && invdet != 0.0 : "Not invertible.";
		// transpose of cofactor matrix * (1 / det)
		double e00 = c00 * invdet;
		double e10 = c01 * invdet;
		double e20 = c02 * invdet;
		double e30 = c03 * invdet;
		double e01 = -determinant3(x.get(1, 0), x.get(2, 0), x.get(3, 0), x.get(1, 2), x.get(2, 2), x.get(3, 2), x.get(1, 3), x.get(2, 3), x.get(3, 3)) * invdet;
		double e11 = determinant3(x.get(0, 0), x.get(2, 0), x.get(3, 0), x.get(0, 2), x.get(2, 2), x.get(3, 2), x.get(0, 3), x.get(2, 3), x.get(3, 3)) * invdet;
		double e21 = -determinant3(x.get(0, 0), x.get(1, 0), x.get(3, 0), x.get(0, 2), x.get(1, 2), x.get(3, 2), x.get(0, 3), x.get(1, 3), x.get(3, 3)) * invdet;
		double e31 = determinant3(x.get(0, 0), x.get(1, 0), x.get(2, 0), x.get(0, 2), x.get(1, 2), x.get(2, 2), x.get(0, 3), x.get(1, 3), x.get(2, 3)) * invdet;
		double e02 = determinant3(x.get(1, 0), x.get(2, 0), x.get(3, 0), x.get(1, 1), x.get(2, 1), x.get(3, 1), x.get(1, 3), x.get(2, 3), x.get(3, 3)) * invdet;
		double e12 = -determinant3(x.get(0, 0), x.get(2, 0), x.get(3, 0), x.get(0, 1), x.get(2, 1), x.get(3, 1), x.get(0, 3), x.get(2, 3), x.get(3, 3)) * invdet;
		double e22 = determinant3(x.get(0, 0), x.get(1, 0), x.get(3, 0), x.get(0, 1), x.get(1, 1), x.get(3, 1), x.get(0, 3), x.get(1, 3), x.get(3, 3)) * invdet;
		double e32 = -determinant3(x.get(0, 0), x.get(1, 0), x.get(2, 0), x.get(0, 1), x.get(1, 1), x.get(2, 1), x.get(0, 3), x.get(1, 3), x.get(2, 3)) * invdet;
		double e03 = -determinant3(x.get(1, 0), x.get(2, 0), x.get(3, 0), x.get(1, 1), x.get(2, 1), x.get(3, 1), x.get(1, 2), x.get(2, 2), x.get(3, 2)) * invdet;
		double e13 = determinant3(x.get(0, 0), x.get(2, 0), x.get(3, 0), x.get(0, 1), x.get(2, 1), x.get(3, 1), x.get(0, 2), x.get(2, 2), x.get(3, 2)) * invdet;
		double e23 = -determinant3(x.get(0, 0), x.get(1, 0), x.get(3, 0), x.get(0, 1), x.get(1, 1), x.get(3, 1), x.get(0, 2), x.get(1, 2), x.get(3, 2)) * invdet;
		double e33 = determinant3(x.get(0, 0), x.get(1, 0), x.get(2, 0), x.get(0, 1), x.get(1, 1), x.get(2, 1), x.get(0, 2), x.get(1, 2), x.get(2, 2)) * invdet;
		return mat4d(e00, e01, e02, e03, e10, e11, e12, e13, e20, e21, e22, e23, e30, e31, e32, e33);
	}

	public static Mat2f inverse(Mat2f x) {
		float invdet = 1.f / determinant(x);
		assert !isinf(invdet) && !isnan(invdet) && invdet != 0.f;
		return mat2f(x.get(1, 1) * invdet, -x.get(1, 0) * invdet, -x.get(0, 1) * invdet, x.get(0, 0) * invdet);
	}

	public static Mat3f inverse(Mat3f x) {
		// first column of cofactors, can uses for determinant
		float c00 = determinant2(x.get(1, 1), x.get(2, 1), x.get(1, 2), x.get(2, 2));
		float c01 = -determinant2(x.get(0, 1), x.get(2, 1), x.get(0, 2), x.get(2, 2));
		float c02 = determinant2(x.get(0, 1), x.get(1, 1), x.get(0, 2), x.get(1, 2));
		// get determinant by expanding about first column
		float invdet = 1.f / (x.get(0, 0) * c00 + x.get(1, 0) * c01 + x.get(2, 0) * c02);
		assert !isinf(invdet) && !isnan(invdet) && invdet != 0.0 : "Not invertible.";
		// transpose of cofactor matrix * (1 / det)
		float e00 = c00 * invdet;
		float e10 = c01 * invdet;
		float e20 = c02 * invdet;
		float e01 = -determinant2(x.get(1, 0), x.get(2, 0), x.get(1, 2), x.get(2, 2)) * invdet;
		float e11 = determinant2(x.get(0, 0), x.get(2, 0), x.get(0, 2), x.get(2, 2)) * invdet;
		float e21 = -determinant2(x.get(0, 0), x.get(1, 0), x.get(0, 2), x.get(1, 2)) * invdet;
		float e02 = determinant2(x.get(1, 0), x.get(2, 0), x.get(1, 1), x.get(2, 1)) * invdet;
		float e12 = -determinant2(x.get(0, 0), x.get(2, 0), x.get(0, 1), x.get(2, 1)) * invdet;
		float e22 = determinant2(x.get(0, 0), x.get(1, 0), x.get(0, 1), x.get(1, 1)) * invdet;
		return mat3f(e00, e01, e02, e10, e11, e12, e20, e21, e22);
	}

	public static Mat4f inverse(Mat4f x) {
		// first column of cofactors, can use for determinant
		float c00 = determinant3(x.get(1, 1), x.get(2, 1), x.get(3, 1), x.get(1, 2), x.get(2, 2), x.get(3, 2), x.get(1, 3), x.get(2, 3), x.get(3, 3));
		float c01 = -determinant3(x.get(0, 1), x.get(2, 1), x.get(3, 1), x.get(0, 2), x.get(2, 2), x.get(3, 2), x.get(0, 3), x.get(2, 3), x.get(3, 3));
		float c02 = determinant3(x.get(0, 1), x.get(1, 1), x.get(3, 1), x.get(0, 2), x.get(1, 2), x.get(3, 2), x.get(0, 3), x.get(1, 3), x.get(3, 3));
		float c03 = -determinant3(x.get(0, 1), x.get(1, 1), x.get(2, 1), x.get(0, 2), x.get(1, 2), x.get(2, 2), x.get(0, 3), x.get(1, 3), x.get(2, 3));
		// get determinant by expanding about first column
		float invdet = 1.f / (x.get(0, 0) * c00 + x.get(1, 0) * c01 + x.get(2, 0) * c02 + x.get(3, 0) * c03);
		assert !isinf(invdet) && !isnan(invdet) && invdet != 0.0 : "Not invertible.";
		// transpose of cofactor matrix * (1 / det)
		float e00 = c00 * invdet;
		float e10 = c01 * invdet;
		float e20 = c02 * invdet;
		float e30 = c03 * invdet;
		float e01 = -determinant3(x.get(1, 0), x.get(2, 0), x.get(3, 0), x.get(1, 2), x.get(2, 2), x.get(3, 2), x.get(1, 3), x.get(2, 3), x.get(3, 3)) * invdet;
		float e11 = determinant3(x.get(0, 0), x.get(2, 0), x.get(3, 0), x.get(0, 2), x.get(2, 2), x.get(3, 2), x.get(0, 3), x.get(2, 3), x.get(3, 3)) * invdet;
		float e21 = -determinant3(x.get(0, 0), x.get(1, 0), x.get(3, 0), x.get(0, 2), x.get(1, 2), x.get(3, 2), x.get(0, 3), x.get(1, 3), x.get(3, 3)) * invdet;
		float e31 = determinant3(x.get(0, 0), x.get(1, 0), x.get(2, 0), x.get(0, 2), x.get(1, 2), x.get(2, 2), x.get(0, 3), x.get(1, 3), x.get(2, 3)) * invdet;
		float e02 = determinant3(x.get(1, 0), x.get(2, 0), x.get(3, 0), x.get(1, 1), x.get(2, 1), x.get(3, 1), x.get(1, 3), x.get(2, 3), x.get(3, 3)) * invdet;
		float e12 = -determinant3(x.get(0, 0), x.get(2, 0), x.get(3, 0), x.get(0, 1), x.get(2, 1), x.get(3, 1), x.get(0, 3), x.get(2, 3), x.get(3, 3)) * invdet;
		float e22 = determinant3(x.get(0, 0), x.get(1, 0), x.get(3, 0), x.get(0, 1), x.get(1, 1), x.get(3, 1), x.get(0, 3), x.get(1, 3), x.get(3, 3)) * invdet;
		float e32 = -determinant3(x.get(0, 0), x.get(1, 0), x.get(2, 0), x.get(0, 1), x.get(1, 1), x.get(2, 1), x.get(0, 3), x.get(1, 3), x.get(2, 3)) * invdet;
		float e03 = -determinant3(x.get(1, 0), x.get(2, 0), x.get(3, 0), x.get(1, 1), x.get(2, 1), x.get(3, 1), x.get(1, 2), x.get(2, 2), x.get(3, 2)) * invdet;
		float e13 = determinant3(x.get(0, 0), x.get(2, 0), x.get(3, 0), x.get(0, 1), x.get(2, 1), x.get(3, 1), x.get(0, 2), x.get(2, 2), x.get(3, 2)) * invdet;
		float e23 = -determinant3(x.get(0, 0), x.get(1, 0), x.get(3, 0), x.get(0, 1), x.get(1, 1), x.get(3, 1), x.get(0, 2), x.get(1, 2), x.get(3, 2)) * invdet;
		float e33 = determinant3(x.get(0, 0), x.get(1, 0), x.get(2, 0), x.get(0, 1), x.get(1, 1), x.get(2, 1), x.get(0, 2), x.get(1, 2), x.get(2, 2)) * invdet;
		return mat4f(e00, e01, e02, e03, e10, e11, e12, e13, e20, e21, e22, e23, e30, e31, e32, e33);
	}

    // fovy in radians, aspect is w/h
    public static Mat4d perspectiveProjectiond(double fovy, double aspect, double zNear, double zFar) {
        double f = 1.0 / (fovy / 2.0);
        return mat4d(
            f / aspect, 0,  0,                                   0,
            0,          f,  0,                                   0,
            0,          0,  (zFar + zNear) / (zNear - zFar),     -1,
            0,          0,  (2 * zFar * zNear) / (zNear - zFar), 0);
    }

    public static Mat4d orthographicProjectiond(double left, double right, double bottom, double top, double nearVal, double farVal) {
        return mat4d(
            2 / (right - left),               0,                                0,                                        0,
            0,                                2 / (top - bottom),               0,                                        0,
            0,                                0,                                -2 / (farVal - nearVal),                  0,
            -(right + left) / (right - left), -(top + bottom) / (top - bottom), -(farVal + nearVal) / (farVal - nearVal), 1);
    }

    public static Mat4d shear3d(int t_dim, int s_dim, double f) {
        Mat4d m = mat4d(1);
        return m.with(s_dim, t_dim, f);
    }

    public static Mat4d translate3d(double d) {
        return mat4d(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            d, d, d, 1);
    }

    public static Mat4d translate3d(double dx, double dy, double dz) {
        return mat4d(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            dx, dy, dz, 1);
    }


    public static Mat4d translate3d(Vec3d d) {
        return mat4d(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            d.x, d.y, d.z, 1);
    }

    public static Mat4d scale3d(double f) {
        return mat4d(
            asDouble(f), 0,  0,  0,
            0,  asDouble(f), 0,  0,
            0,  0,  asDouble(f), 0,
            0,  0,  0,  1);
    }

    public static Mat4d scale3d(double fx, double fy, double fz) {
        return mat4d(
            asDouble(fx), 0,  0,  0,
            0,  asDouble(fy), 0,  0,
            0,  0,  asDouble(fz), 0,
            0,  0,  0,  1);
    }

    public static Mat4d scale3d(Vec3d f) {
        return mat4d(
            asDouble(f.x), 0,  0,  0,
            0,  asDouble(f.y), 0,  0,
            0,  0,  asDouble(f.z), 0,
            0,  0,  0,  1);
    }

    public static Mat4d rotateX3d(double angle) {
        double a = asDouble(angle);
        return mat4d(
    		1, 0, 0, 0,
            0, Math.cos(a), -Math.sin(a), 0,
            0, Math.sin(a), Math.cos(a), 0,
            0, 0, 0, 1);
    }

    public static Mat4d rotateY3d(double angle) {
        double a = asDouble(angle);
        return mat4d(
            Math.cos(a), 0, Math.sin(a), 0,
            0, 1, 0, 0,
            -Math.sin(a), 0, Math.cos(a), 0,
            0, 0, 0, 1);
    }

    public static Mat4d rotateZ3d(double angle) {
        double a = asDouble(angle);
        return mat4d(
            Math.cos(a), -Math.sin(a), 0, 0,
            Math.sin(a), Math.cos(a), 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);
    }

    public static Mat4d rotate3d(Quatd q) {
        double x = q.x;
        double y = q.y;
        double z = q.z;
        double w = q.w;
        return mat4d(
            (w * w + x * x - y * y - z * z), (2 * x * y + 2 * w * z),         (2 * x * z - 2 * w * y),         0,
            (2 * x * y - 2 * w * z),         (w * w - x * x + y * y - z * z), (2 * y * z + 2 * w * x),         0,
            (2 * x * z + 2 * w * y),         (2 * y * z - 2 * w * x),         (w * w - x * x - y * y + z * z), 0,
            0,                               0,                               0,                               (w * w + x * x + y * y + z * z));
    }


    // fovy in radians, aspect is w/h
    public static Mat4f perspectiveProjectionf(float fovy, float aspect, float zNear, float zFar) {
        float f = 1.0f / (fovy / 2.0f);
        return mat4f(
            f / aspect, 0,  0,                                   0,
            0,          f,  0,                                   0,
            0,          0,  (zFar + zNear) / (zNear - zFar),     -1,
            0,          0,  (2 * zFar * zNear) / (zNear - zFar), 0);
    }

    public static Mat4f orthographicProjectionf(float left, float right, float bottom, float top, float nearVal, float farVal) {
        return mat4f(
            2 / (right - left),               0,                                0,                                        0,
            0,                                2 / (top - bottom),               0,                                        0,
            0,                                0,                                -2 / (farVal - nearVal),                  0,
            -(right + left) / (right - left), -(top + bottom) / (top - bottom), -(farVal + nearVal) / (farVal - nearVal), 1);
    }

    public static Mat4f shear3f(int t_dim, int s_dim, float f) {
        Mat4f m = mat4f(1);
        return m.with(s_dim, t_dim, f);
    }

    public static Mat4f translate3f(float d) {
        return mat4f(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            d, d, d, 1);
    }

    public static Mat4f translate3f(float dx, float dy, float dz) {
        return mat4f(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            dx, dy, dz, 1);
    }


    public static Mat4f translate3f(Vec3f d) {
        return mat4f(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            d.x, d.y, d.z, 1);
    }

    public static Mat4f scale3f(float f) {
        return mat4f(
            asFloat(f), 0,  0,  0,
            0,  asFloat(f), 0,  0,
            0,  0,  asFloat(f), 0,
            0,  0,  0,  1);
    }

    public static Mat4f scale3f(float fx, float fy, float fz) {
        return mat4f(
            asFloat(fx), 0,  0,  0,
            0,  asFloat(fy), 0,  0,
            0,  0,  asFloat(fz), 0,
            0,  0,  0,  1);
    }

    public static Mat4f scale3f(Vec3f f) {
        return mat4f(
            asFloat(f.x), 0,  0,  0,
            0,  asFloat(f.y), 0,  0,
            0,  0,  asFloat(f.z), 0,
            0,  0,  0,  1);
    }

    public static Mat4f rotateX3f(float angle) {
        float a = asFloat(angle);
        return mat4f(
    		1, 0, 0, 0,
            0, (float) Math.cos(a), (float) -Math.sin(a), 0,
            0, (float) Math.sin(a), (float) Math.cos(a), 0,
            0, 0, 0, 1);
    }

    public static Mat4f rotateY3f(float angle) {
        float a = asFloat(angle);
        return mat4f(
        	(float) Math.cos(a), 0, (float) Math.sin(a), 0,
            0, 1, 0, 0,
            (float) -Math.sin(a), 0, (float) Math.cos(a), 0,
            0, 0, 0, 1);
    }

    public static Mat4f rotateZ3f(float angle) {
        float a = asFloat(angle);
        return mat4f(
            (float) Math.cos(a), (float)-Math.sin(a), 0, 0,
            (float) Math.sin(a), (float) Math.cos(a), 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);
    }

    public static Mat4f rotate3f(Quatf q) {
        float x = q.x;
        float y = q.y;
        float z = q.z;
        float w = q.w;
        return mat4f(
            (w * w + x * x - y * y - z * z), (2 * x * y + 2 * w * z),         (2 * x * z - 2 * w * y),         0,
            (2 * x * y - 2 * w * z),         (w * w - x * x + y * y - z * z), (2 * y * z + 2 * w * x),         0,
            (2 * x * z + 2 * w * y),         (2 * y * z - 2 * w * x),         (w * w - x * x - y * y + z * z), 0,
            0,                               0,                               0,                               (w * w + x * x + y * y + z * z));
    }

	public static Quatd quatd(boolean w, boolean x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, boolean x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, boolean x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, boolean x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, boolean x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, double x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, double x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, double x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, double x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, float x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, float x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, float x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, float x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, boolean y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, boolean y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, boolean y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, boolean y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, double y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, double y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, double y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, double y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, float y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, float y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, float y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, float y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, int y, boolean z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, int y, double z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, int y, float z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, int x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(double w, int x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(float w, int x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(int w, int x, int y, int z) {
		return new Quatd(asDouble(w), asDouble(x), asDouble(y), asDouble(z));
	}

	public static Quatd quatd(boolean w, Vec3b xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(double w, Vec3b xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(float w, Vec3b xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(int w, Vec3b xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(boolean w, Vec3d xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(double w, Vec3d xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(float w, Vec3d xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(int w, Vec3d xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(boolean w, Vec3f xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(double w, Vec3f xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(float w, Vec3f xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(int w, Vec3f xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(boolean w, Vec3i xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(double w, Vec3i xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(float w, Vec3i xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(int w, Vec3i xyz) {
		return new Quatd(asDouble(w), asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Quatd quatd(Vec4b xyzw) {
		return new Quatd(asDouble(xyzw.w), asDouble(xyzw.x), asDouble(xyzw.y), asDouble(xyzw.z));
	}

	public static Quatd quatd(Vec4d xyzw) {
		return new Quatd(asDouble(xyzw.w), asDouble(xyzw.x), asDouble(xyzw.y), asDouble(xyzw.z));
	}

	public static Quatd quatd(Vec4f xyzw) {
		return new Quatd(asDouble(xyzw.w), asDouble(xyzw.x), asDouble(xyzw.y), asDouble(xyzw.z));
	}

	public static Quatd quatd(Vec4i xyzw) {
		return new Quatd(asDouble(xyzw.w), asDouble(xyzw.x), asDouble(xyzw.y), asDouble(xyzw.z));
	}

	public static Quatd quatd(Quatd xyzw) {
		return new Quatd(asDouble(xyzw.w), asDouble(xyzw.x), asDouble(xyzw.y), asDouble(xyzw.z));
	}

	public static Quatd quatd(Quatf xyzw) {
		return new Quatd(asDouble(xyzw.w), asDouble(xyzw.x), asDouble(xyzw.y), asDouble(xyzw.z));
	}

	public static Quatd axisangled(Vec3b xyz, boolean w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3b xyz, double w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3b xyz, float w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3b xyz, int w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3d xyz, boolean w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3d xyz, double w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3d xyz, float w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3d xyz, int w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3f xyz, boolean w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3f xyz, double w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3f xyz, float w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3f xyz, int w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3i xyz, boolean w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3i xyz, double w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3i xyz, float w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd axisangled(Vec3i xyz, int w) {
		return new Quatd(vec3d(xyz), asDouble(w));
	}

	public static Quatd quatd(Vec3b rot) {
		return new Quatd(vec3d(rot));
	}

	public static Quatd quatd(Vec3d rot) {
		return new Quatd(vec3d(rot));
	}

	public static Quatd quatd(Vec3f rot) {
		return new Quatd(vec3d(rot));
	}

	public static Quatd quatd(Vec3i rot) {
		return new Quatd(vec3d(rot));
	}

	public static Quatf quatf(boolean w, boolean x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, boolean x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, boolean x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, boolean x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, boolean x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, double x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, double x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, double x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, double x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, float x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, float x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, float x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, float x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, boolean y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, boolean y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, boolean y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, boolean y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, double y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, double y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, double y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, double y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, float y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, float y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, float y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, float y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, int y, boolean z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, int y, double z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, int y, float z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, int x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(double w, int x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(float w, int x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(int w, int x, int y, int z) {
        return new Quatf(asFloat(w), asFloat(x), asFloat(y), asFloat(z));
    }

    public static Quatf quatf(boolean w, Vec3b xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(double w, Vec3b xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(float w, Vec3b xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(int w, Vec3b xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(boolean w, Vec3d xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(double w, Vec3d xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(float w, Vec3d xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(int w, Vec3d xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(boolean w, Vec3f xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(double w, Vec3f xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(float w, Vec3f xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(int w, Vec3f xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(boolean w, Vec3i xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(double w, Vec3i xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(float w, Vec3i xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(int w, Vec3i xyz) {
        return new Quatf(asFloat(w), asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
    }

    public static Quatf quatf(Vec4b xyzw) {
        return new Quatf(asFloat(xyzw.w), asFloat(xyzw.x), asFloat(xyzw.y), asFloat(xyzw.z));
    }

    public static Quatf quatf(Vec4d xyzw) {
        return new Quatf(asFloat(xyzw.w), asFloat(xyzw.x), asFloat(xyzw.y), asFloat(xyzw.z));
    }

    public static Quatf quatf(Vec4f xyzw) {
        return new Quatf(asFloat(xyzw.w), asFloat(xyzw.x), asFloat(xyzw.y), asFloat(xyzw.z));
    }

    public static Quatf quatf(Vec4i xyzw) {
        return new Quatf(asFloat(xyzw.w), asFloat(xyzw.x), asFloat(xyzw.y), asFloat(xyzw.z));
    }

    public static Quatf axisanglef(Vec3b xyz, boolean w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3b xyz, double w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3b xyz, float w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3b xyz, int w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3d xyz, boolean w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3d xyz, double w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3d xyz, float w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3d xyz, int w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3f xyz, boolean w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3f xyz, double w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3f xyz, float w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3f xyz, int w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3i xyz, boolean w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3i xyz, double w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3i xyz, float w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf axisanglef(Vec3i xyz, int w) {
        return new Quatf(vec3f(xyz), asFloat(w));
    }

    public static Quatf quatf(Vec3b rot) {
        return new Quatf(vec3f(rot));
    }

    public static Quatf quatf(Vec3d rot) {
        return new Quatf(vec3f(rot));
    }

    public static Quatf quatf(Vec3f rot) {
        return new Quatf(vec3f(rot));
    }

    public static Quatf quatf(Vec3i rot) {
        return new Quatf(vec3f(rot));
    }

    public static double norm(Quatd q) {
        return Math.sqrt(q.w * q.w + q.x * q.x + q.y * q.y + q.z * q.z);
    }

    public static float norm(Quatf q) {
        return (float) Math.sqrt(q.w * q.w + q.x * q.x + q.y * q.y + q.z * q.z);
    }

    public static Quatd normalize(Quatd q) {
        return q.mul(1d / norm(q));
    }

    public static Quatf normalize(Quatf q) {
        return q.mul(1f / norm(q));
    }

    public static Quatd conjugate(Quatd q) {
        return new Quatd(q.w, -q.x, -q.y, -q.z);
    }

    public static Quatf conjugate(Quatf q) {
        return new Quatf(q.w, -q.x, -q.y, -q.z);
    }

    public static Quatd inverse(Quatd q) {
        double inorm2 = 1 / (q.w * q.w + q.x * q.x + q.y * q.y + q.z * q.z);
        return new Quatd(inorm2 * q.w, -inorm2 * q.x, -inorm2 * q.y, -inorm2 * q.z);
    }

    public static Quatf inverse(Quatf q) {
        float inorm2 = 1 / (q.w * q.w + q.x * q.x + q.y * q.y + q.z * q.z);
        return new Quatf(inorm2 * q.w, -inorm2 * q.x, -inorm2 * q.y, -inorm2 * q.z);
    }

    public static Quatd pow(Quatd q, double alpha) {
        double qm = norm(q);
        double theta = Math.acos(q.w / qm);
        double ivm = 1 / Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z);
        if (Double.isInfinite(ivm)) {
            return Quatd.one.mul(Math.pow(qm, alpha));
        } else {
            double ivmalphatheta = ivm * alpha * theta;
            Quatd p = new Quatd(0, q.x * ivmalphatheta, q.y * ivmalphatheta, q.z * ivmalphatheta);
            return exp(p).mul(Math.pow(qm, alpha));
        }
    }

    public static Quatd pow(Quatd q, float alpha) {
        double qm = norm(q);
        double theta = Math.acos(q.w / qm);
        double ivm = 1 / Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z);
        if (Double.isInfinite(ivm)) {
            return Quatd.one.mul(Math.pow(qm, alpha));
        } else {
            double ivmalphatheta = ivm * alpha * theta;
            Quatd p = new Quatd(0, q.x * ivmalphatheta, q.y * ivmalphatheta, q.z * ivmalphatheta);
            return exp(p).mul(Math.pow(qm, alpha));
        }
    }

    public static Quatf pow(Quatf q, double alpha) {
        float qm = norm(q);
        float theta = (float) Math.acos(q.w / qm);
        float ivm = (float) (1 / Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z));
        if (Double.isInfinite(ivm)) {
            return Quatf.one.mul((float) Math.pow(qm, alpha));
        } else {
            float ivmalphatheta = (float) (ivm * alpha * theta);
            Quatf p = new Quatf(0, q.x * ivmalphatheta, q.y * ivmalphatheta, q.z * ivmalphatheta);
            return exp(p).mul((float) Math.pow(qm, alpha));
        }
    }

    public static Quatf pow(Quatf q, float alpha) {
        float qm = norm(q);
        float theta = (float) Math.acos(q.w / qm);
        float ivm = (float) (1 / Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z));
        if (Double.isInfinite(ivm)) {
            return Quatf.one.mul((float) Math.pow(qm, alpha));
        } else {
            float ivmalphatheta = ivm * alpha * theta;
            Quatf p = new Quatf(0, q.x * ivmalphatheta, q.y * ivmalphatheta, q.z * ivmalphatheta);
            return exp(p).mul((float) Math.pow(qm, alpha));
        }
    }

    public static Quatd exp(Quatd q) {
        double expw = Math.exp(q.w);
        double vm = Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z);
        double ivm = 1 / vm;
        if (Double.isInfinite(ivm)) {
            return new Quatd(expw * Math.cos(vm), 0, 0, 0);
        } else {
            double vf = expw * Math.sin(vm) * ivm;
            return new Quatd(expw * Math.cos(vm), q.x * vf, q.y * vf, q.z * vf);
        }
    }

    public static Quatf exp(Quatf q) {
        float expw = (float) Math.exp(q.w);
        float vm = (float) Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z);
        float ivm = 1 / vm;
        if (Double.isInfinite(ivm)) {
            return new Quatf((float) (expw * Math.cos(vm)), 0, 0, 0);
        } else {
            float vf = (float) (expw * Math.sin(vm) * ivm);
            return new Quatf((float) (expw * Math.cos(vm)), q.x * vf, q.y * vf, q.z * vf);
        }
    }

    public static Quatd log(Quatd q) {
        double qm = norm(q);
        double ivm = 1 / Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z);
        if (Double.isInfinite(ivm)) {
            return new Quatd(Math.log(qm), 0, 0, 0);
        } else {
            double vf = Math.acos(q.w / qm) * ivm;
            return new Quatd(Math.log(qm), q.x * vf, q.y * vf, q.z * vf);
        }
    }

    public static Quatf log(Quatf q) {
        float qm = norm(q);
        float ivm = (float) (1 / Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z));
        if (Double.isInfinite(ivm)) {
            return new Quatf((float) Math.log(qm), 0, 0, 0);
        } else {
            float vf = (float) (Math.acos(q.w / qm) * ivm);
            return new Quatf((float) Math.log(qm), q.x * vf, q.y * vf, q.z * vf);
        }
    }

    public static double distance(Quatd lhs, Quatd rhs) {
        double dw = lhs.w - rhs.w;
        double dx = lhs.x - rhs.x;
        double dy = lhs.y - rhs.y;
        double dz = lhs.z - rhs.z;
        return Math.sqrt(dw * dw + dx * dx + dy * dy + dz * dz);
    }

    public static double distance(Quatd lhs, Quatf rhs) {
        double dw = lhs.w - rhs.w;
        double dx = lhs.x - rhs.x;
        double dy = lhs.y - rhs.y;
        double dz = lhs.z - rhs.z;
        return Math.sqrt(dw * dw + dx * dx + dy * dy + dz * dz);
    }

    public static double distance(Quatf lhs, Quatd rhs) {
        double dw = lhs.w - rhs.w;
        double dx = lhs.x - rhs.x;
        double dy = lhs.y - rhs.y;
        double dz = lhs.z - rhs.z;
        return Math.sqrt(dw * dw + dx * dx + dy * dy + dz * dz);
    }

    public static float distance(Quatf lhs, Quatf rhs) {
        float dw = lhs.w - rhs.w;
        float dx = lhs.x - rhs.x;
        float dy = lhs.y - rhs.y;
        float dz = lhs.z - rhs.z;
        return (float) Math.sqrt(dw * dw + dx * dx + dy * dy + dz * dz);
    }

    public static Quatd mix(Quatd q0, Quatd q1, double t) {
        double u = 1 - t;
        return normalize(new Quatd(q0.w * u + q1.w * t, q0.x * u + q1.x * t, q0.y * u + q1.y * t, q0.z * u + q1.z * t));
    }

    public static Quatd mix(Quatd q0, Quatd q1, float t) {
        double u = 1 - t;
        return normalize(new Quatd(q0.w * u + q1.w * t, q0.x * u + q1.x * t, q0.y * u + q1.y * t, q0.z * u + q1.z * t));
    }

    public static Quatf mix(Quatf q0, Quatf q1, double t) {
        float u = (float) (1 - t);
        return normalize(new Quatf(q0.w * u + q1.w * (float)t, q0.x * u + q1.x * (float)t, q0.y * u + q1.y * (float)t, q0.z * u + q1.z * (float)t));
    }

    public static Quatf mix(Quatf q0, Quatf q1, float t) {
        float u = 1 - t;
        return normalize(new Quatf(q0.w * u + q1.w * t, q0.x * u + q1.x * t, q0.y * u + q1.y * t, q0.z * u + q1.z * t));
    }

    public static double angle(Quatd q) {
        return 2 * Math.acos(q.w / norm(q));
    }

    public static float angle(Quatf q) {
        return (float) (2 * Math.acos(q.w / norm(q)));
    }

    public static Vec3d axis(Quatd q) {
        double ivm = 1 / Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z);
        return vec3d(ivm * q.x, ivm * q.y, ivm * q.z);
    }

    public static Vec3f axis(Quatf q) {
        double ivm = 1 / Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z);
        return vec3f(ivm * q.x, ivm * q.y, ivm * q.z);
    }
    
    public static Vec3d eular(Quatd q) {
    	return vec3d(
    			Math.atan2(2*(q.w*q.x + q.y*q.z), 1-2*(q.x*q.x + q.y*q.y)),
    			Math.asin(2*(q.w*q.y - q.z*q.x)),
    			Math.atan2(2*(q.w*q.z + q.x*q.y), 1-2*(q.y*q.y + q.z*q.z))
    	);
    }
    
    public static Vec3f eular(Quatf q) {
    	return vec3f(
    			Math.atan2(2*(q.w*q.x + q.y*q.z), 1-2*(q.x*q.x + q.y*q.y)),
    			Math.asin(2*(q.w*q.y - q.z*q.x)),
    			Math.atan2(2*(q.w*q.z + q.x*q.y), 1-2*(q.y*q.y + q.z*q.z))
    	);
    }


	/*
	 * Generated vector 'constructors'
	 */

	public static Vec2b vec2b(boolean x) {
		return new Vec2b(asBoolean(x), asBoolean(x));
	}

	public static Vec2b vec2b(double x) {
		return new Vec2b(asBoolean(x), asBoolean(x));
	}

	public static Vec2b vec2b(float x) {
		return new Vec2b(asBoolean(x), asBoolean(x));
	}

	public static Vec2b vec2b(int x) {
		return new Vec2b(asBoolean(x), asBoolean(x));
	}

	public static Vec2b vec2b(boolean x, boolean y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(boolean x, double y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(boolean x, float y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(boolean x, int y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(double x, boolean y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(double x, double y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(double x, float y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(double x, int y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(float x, boolean y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(float x, double y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(float x, float y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(float x, int y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(int x, boolean y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(int x, double y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(int x, float y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(int x, int y) {
		return new Vec2b(asBoolean(x), asBoolean(y));
	}

	public static Vec2b vec2b(Vec2b xy) {
		return new Vec2b(asBoolean(xy.x), asBoolean(xy.y));
	}

	public static Vec2b vec2b(Vec2d xy) {
		return new Vec2b(asBoolean(xy.x), asBoolean(xy.y));
	}

	public static Vec2b vec2b(Vec2f xy) {
		return new Vec2b(asBoolean(xy.x), asBoolean(xy.y));
	}

	public static Vec2b vec2b(Vec2i xy) {
		return new Vec2b(asBoolean(xy.x), asBoolean(xy.y));
	}

	public static Vec2d vec2d(boolean x) {
		return new Vec2d(asDouble(x), asDouble(x));
	}

	public static Vec2d vec2d(double x) {
		return new Vec2d(asDouble(x), asDouble(x));
	}

	public static Vec2d vec2d(float x) {
		return new Vec2d(asDouble(x), asDouble(x));
	}

	public static Vec2d vec2d(int x) {
		return new Vec2d(asDouble(x), asDouble(x));
	}

	public static Vec2d vec2d(boolean x, boolean y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(boolean x, double y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(boolean x, float y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(boolean x, int y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(double x, boolean y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(double x, double y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(double x, float y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(double x, int y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(float x, boolean y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(float x, double y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(float x, float y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(float x, int y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(int x, boolean y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(int x, double y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(int x, float y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(int x, int y) {
		return new Vec2d(asDouble(x), asDouble(y));
	}

	public static Vec2d vec2d(Vec2b xy) {
		return new Vec2d(asDouble(xy.x), asDouble(xy.y));
	}

	public static Vec2d vec2d(Vec2d xy) {
		return new Vec2d(asDouble(xy.x), asDouble(xy.y));
	}

	public static Vec2d vec2d(Vec2f xy) {
		return new Vec2d(asDouble(xy.x), asDouble(xy.y));
	}

	public static Vec2d vec2d(Vec2i xy) {
		return new Vec2d(asDouble(xy.x), asDouble(xy.y));
	}

	public static Vec2f vec2f(boolean x) {
		return new Vec2f(asFloat(x), asFloat(x));
	}

	public static Vec2f vec2f(double x) {
		return new Vec2f(asFloat(x), asFloat(x));
	}

	public static Vec2f vec2f(float x) {
		return new Vec2f(asFloat(x), asFloat(x));
	}

	public static Vec2f vec2f(int x) {
		return new Vec2f(asFloat(x), asFloat(x));
	}

	public static Vec2f vec2f(boolean x, boolean y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(boolean x, double y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(boolean x, float y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(boolean x, int y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(double x, boolean y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(double x, double y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(double x, float y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(double x, int y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(float x, boolean y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(float x, double y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(float x, float y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(float x, int y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(int x, boolean y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(int x, double y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(int x, float y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(int x, int y) {
		return new Vec2f(asFloat(x), asFloat(y));
	}

	public static Vec2f vec2f(Vec2b xy) {
		return new Vec2f(asFloat(xy.x), asFloat(xy.y));
	}

	public static Vec2f vec2f(Vec2d xy) {
		return new Vec2f(asFloat(xy.x), asFloat(xy.y));
	}

	public static Vec2f vec2f(Vec2f xy) {
		return new Vec2f(asFloat(xy.x), asFloat(xy.y));
	}

	public static Vec2f vec2f(Vec2i xy) {
		return new Vec2f(asFloat(xy.x), asFloat(xy.y));
	}

	public static Vec2i vec2i(boolean x) {
		return new Vec2i(asInt(x), asInt(x));
	}

	public static Vec2i vec2i(double x) {
		return new Vec2i(asInt(x), asInt(x));
	}

	public static Vec2i vec2i(float x) {
		return new Vec2i(asInt(x), asInt(x));
	}

	public static Vec2i vec2i(int x) {
		return new Vec2i(asInt(x), asInt(x));
	}

	public static Vec2i vec2i(boolean x, boolean y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(boolean x, double y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(boolean x, float y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(boolean x, int y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(double x, boolean y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(double x, double y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(double x, float y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(double x, int y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(float x, boolean y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(float x, double y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(float x, float y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(float x, int y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(int x, boolean y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(int x, double y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(int x, float y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(int x, int y) {
		return new Vec2i(asInt(x), asInt(y));
	}

	public static Vec2i vec2i(Vec2b xy) {
		return new Vec2i(asInt(xy.x), asInt(xy.y));
	}

	public static Vec2i vec2i(Vec2d xy) {
		return new Vec2i(asInt(xy.x), asInt(xy.y));
	}

	public static Vec2i vec2i(Vec2f xy) {
		return new Vec2i(asInt(xy.x), asInt(xy.y));
	}

	public static Vec2i vec2i(Vec2i xy) {
		return new Vec2i(asInt(xy.x), asInt(xy.y));
	}

	public static Vec3b vec3b(boolean x) {
		return new Vec3b(asBoolean(x), asBoolean(x), asBoolean(x));
	}

	public static Vec3b vec3b(double x) {
		return new Vec3b(asBoolean(x), asBoolean(x), asBoolean(x));
	}

	public static Vec3b vec3b(float x) {
		return new Vec3b(asBoolean(x), asBoolean(x), asBoolean(x));
	}

	public static Vec3b vec3b(int x) {
		return new Vec3b(asBoolean(x), asBoolean(x), asBoolean(x));
	}

	public static Vec3b vec3b(boolean x, boolean y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, boolean y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, boolean y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, boolean y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, double y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, double y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, double y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, double y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, float y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, float y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, float y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, float y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, int y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, int y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, int y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, int y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, boolean y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, boolean y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, boolean y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, boolean y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, double y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, double y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, double y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, double y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, float y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, float y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, float y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, float y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, int y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, int y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, int y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(double x, int y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, boolean y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, boolean y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, boolean y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, boolean y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, double y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, double y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, double y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, double y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, float y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, float y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, float y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, float y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, int y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, int y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, int y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(float x, int y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, boolean y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, boolean y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, boolean y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, boolean y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, double y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, double y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, double y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, double y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, float y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, float y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, float y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, float y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, int y, boolean z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, int y, double z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, int y, float z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(int x, int y, int z) {
		return new Vec3b(asBoolean(x), asBoolean(y), asBoolean(z));
	}

	public static Vec3b vec3b(boolean x, Vec2b yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(boolean x, Vec2d yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(boolean x, Vec2f yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(boolean x, Vec2i yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(double x, Vec2b yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(double x, Vec2d yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(double x, Vec2f yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(double x, Vec2i yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(float x, Vec2b yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(float x, Vec2d yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(float x, Vec2f yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(float x, Vec2i yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(int x, Vec2b yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(int x, Vec2d yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(int x, Vec2f yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(int x, Vec2i yz) {
		return new Vec3b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y));
	}

	public static Vec3b vec3b(Vec2b xy, boolean z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2b xy, double z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2b xy, float z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2b xy, int z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2d xy, boolean z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2d xy, double z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2d xy, float z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2d xy, int z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2f xy, boolean z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2f xy, double z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2f xy, float z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2f xy, int z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2i xy, boolean z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2i xy, double z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2i xy, float z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec2i xy, int z) {
		return new Vec3b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z));
	}

	public static Vec3b vec3b(Vec3b xyz) {
		return new Vec3b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z));
	}

	public static Vec3b vec3b(Vec3d xyz) {
		return new Vec3b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z));
	}

	public static Vec3b vec3b(Vec3f xyz) {
		return new Vec3b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z));
	}

	public static Vec3b vec3b(Vec3i xyz) {
		return new Vec3b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z));
	}

	public static Vec3d vec3d(boolean x) {
		return new Vec3d(asDouble(x), asDouble(x), asDouble(x));
	}

	public static Vec3d vec3d(double x) {
		return new Vec3d(asDouble(x), asDouble(x), asDouble(x));
	}

	public static Vec3d vec3d(float x) {
		return new Vec3d(asDouble(x), asDouble(x), asDouble(x));
	}

	public static Vec3d vec3d(int x) {
		return new Vec3d(asDouble(x), asDouble(x), asDouble(x));
	}

	public static Vec3d vec3d(boolean x, boolean y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, boolean y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, boolean y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, boolean y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, double y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, double y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, double y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, double y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, float y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, float y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, float y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, float y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, int y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, int y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, int y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, int y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, boolean y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, boolean y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, boolean y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, boolean y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, double y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, double y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, double y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, double y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, float y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, float y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, float y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, float y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, int y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, int y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, int y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(double x, int y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, boolean y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, boolean y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, boolean y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, boolean y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, double y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, double y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, double y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, double y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, float y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, float y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, float y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, float y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, int y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, int y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, int y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(float x, int y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, boolean y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, boolean y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, boolean y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, boolean y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, double y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, double y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, double y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, double y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, float y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, float y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, float y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, float y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, int y, boolean z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, int y, double z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, int y, float z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(int x, int y, int z) {
		return new Vec3d(asDouble(x), asDouble(y), asDouble(z));
	}

	public static Vec3d vec3d(boolean x, Vec2b yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(boolean x, Vec2d yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(boolean x, Vec2f yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(boolean x, Vec2i yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(double x, Vec2b yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(double x, Vec2d yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(double x, Vec2f yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(double x, Vec2i yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(float x, Vec2b yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(float x, Vec2d yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(float x, Vec2f yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(float x, Vec2i yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(int x, Vec2b yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(int x, Vec2d yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(int x, Vec2f yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(int x, Vec2i yz) {
		return new Vec3d(asDouble(x), asDouble(yz.x), asDouble(yz.y));
	}

	public static Vec3d vec3d(Vec2b xy, boolean z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2b xy, double z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2b xy, float z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2b xy, int z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2d xy, boolean z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2d xy, double z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2d xy, float z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2d xy, int z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2f xy, boolean z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2f xy, double z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2f xy, float z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2f xy, int z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2i xy, boolean z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2i xy, double z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2i xy, float z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec2i xy, int z) {
		return new Vec3d(asDouble(xy.x), asDouble(xy.y), asDouble(z));
	}

	public static Vec3d vec3d(Vec3b xyz) {
		return new Vec3d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Vec3d vec3d(Vec3d xyz) {
		return new Vec3d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Vec3d vec3d(Vec3f xyz) {
		return new Vec3d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Vec3d vec3d(Vec3i xyz) {
		return new Vec3d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z));
	}

	public static Vec3f vec3f(boolean x) {
		return new Vec3f(asFloat(x), asFloat(x), asFloat(x));
	}

	public static Vec3f vec3f(double x) {
		return new Vec3f(asFloat(x), asFloat(x), asFloat(x));
	}

	public static Vec3f vec3f(float x) {
		return new Vec3f(asFloat(x), asFloat(x), asFloat(x));
	}

	public static Vec3f vec3f(int x) {
		return new Vec3f(asFloat(x), asFloat(x), asFloat(x));
	}

	public static Vec3f vec3f(boolean x, boolean y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, boolean y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, boolean y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, boolean y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, double y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, double y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, double y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, double y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, float y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, float y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, float y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, float y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, int y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, int y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, int y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, int y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, boolean y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, boolean y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, boolean y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, boolean y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, double y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, double y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, double y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, double y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, float y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, float y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, float y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, float y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, int y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, int y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, int y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(double x, int y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, boolean y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, boolean y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, boolean y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, boolean y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, double y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, double y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, double y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, double y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, float y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, float y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, float y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, float y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, int y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, int y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, int y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(float x, int y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, boolean y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, boolean y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, boolean y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, boolean y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, double y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, double y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, double y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, double y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, float y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, float y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, float y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, float y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, int y, boolean z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, int y, double z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, int y, float z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(int x, int y, int z) {
		return new Vec3f(asFloat(x), asFloat(y), asFloat(z));
	}

	public static Vec3f vec3f(boolean x, Vec2b yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(boolean x, Vec2d yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(boolean x, Vec2f yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(boolean x, Vec2i yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(double x, Vec2b yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(double x, Vec2d yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(double x, Vec2f yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(double x, Vec2i yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(float x, Vec2b yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(float x, Vec2d yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(float x, Vec2f yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(float x, Vec2i yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(int x, Vec2b yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(int x, Vec2d yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(int x, Vec2f yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(int x, Vec2i yz) {
		return new Vec3f(asFloat(x), asFloat(yz.x), asFloat(yz.y));
	}

	public static Vec3f vec3f(Vec2b xy, boolean z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2b xy, double z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2b xy, float z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2b xy, int z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2d xy, boolean z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2d xy, double z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2d xy, float z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2d xy, int z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2f xy, boolean z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2f xy, double z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2f xy, float z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2f xy, int z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2i xy, boolean z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2i xy, double z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2i xy, float z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec2i xy, int z) {
		return new Vec3f(asFloat(xy.x), asFloat(xy.y), asFloat(z));
	}

	public static Vec3f vec3f(Vec3b xyz) {
		return new Vec3f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
	}

	public static Vec3f vec3f(Vec3d xyz) {
		return new Vec3f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
	}

	public static Vec3f vec3f(Vec3f xyz) {
		return new Vec3f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
	}

	public static Vec3f vec3f(Vec3i xyz) {
		return new Vec3f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z));
	}

	public static Vec3i vec3i(boolean x) {
		return new Vec3i(asInt(x), asInt(x), asInt(x));
	}

	public static Vec3i vec3i(double x) {
		return new Vec3i(asInt(x), asInt(x), asInt(x));
	}

	public static Vec3i vec3i(float x) {
		return new Vec3i(asInt(x), asInt(x), asInt(x));
	}

	public static Vec3i vec3i(int x) {
		return new Vec3i(asInt(x), asInt(x), asInt(x));
	}

	public static Vec3i vec3i(boolean x, boolean y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, boolean y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, boolean y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, boolean y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, double y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, double y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, double y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, double y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, float y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, float y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, float y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, float y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, int y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, int y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, int y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, int y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, boolean y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, boolean y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, boolean y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, boolean y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, double y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, double y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, double y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, double y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, float y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, float y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, float y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, float y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, int y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, int y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, int y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(double x, int y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, boolean y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, boolean y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, boolean y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, boolean y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, double y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, double y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, double y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, double y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, float y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, float y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, float y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, float y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, int y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, int y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, int y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(float x, int y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, boolean y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, boolean y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, boolean y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, boolean y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, double y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, double y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, double y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, double y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, float y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, float y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, float y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, float y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, int y, boolean z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, int y, double z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, int y, float z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(int x, int y, int z) {
		return new Vec3i(asInt(x), asInt(y), asInt(z));
	}

	public static Vec3i vec3i(boolean x, Vec2b yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(boolean x, Vec2d yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(boolean x, Vec2f yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(boolean x, Vec2i yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(double x, Vec2b yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(double x, Vec2d yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(double x, Vec2f yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(double x, Vec2i yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(float x, Vec2b yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(float x, Vec2d yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(float x, Vec2f yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(float x, Vec2i yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(int x, Vec2b yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(int x, Vec2d yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(int x, Vec2f yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(int x, Vec2i yz) {
		return new Vec3i(asInt(x), asInt(yz.x), asInt(yz.y));
	}

	public static Vec3i vec3i(Vec2b xy, boolean z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2b xy, double z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2b xy, float z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2b xy, int z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2d xy, boolean z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2d xy, double z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2d xy, float z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2d xy, int z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2f xy, boolean z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2f xy, double z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2f xy, float z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2f xy, int z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2i xy, boolean z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2i xy, double z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2i xy, float z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec2i xy, int z) {
		return new Vec3i(asInt(xy.x), asInt(xy.y), asInt(z));
	}

	public static Vec3i vec3i(Vec3b xyz) {
		return new Vec3i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z));
	}

	public static Vec3i vec3i(Vec3d xyz) {
		return new Vec3i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z));
	}

	public static Vec3i vec3i(Vec3f xyz) {
		return new Vec3i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z));
	}

	public static Vec3i vec3i(Vec3i xyz) {
		return new Vec3i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z));
	}

	public static Vec4b vec4b(boolean x) {
		return new Vec4b(asBoolean(x), asBoolean(x), asBoolean(x), asBoolean(x));
	}

	public static Vec4b vec4b(double x) {
		return new Vec4b(asBoolean(x), asBoolean(x), asBoolean(x), asBoolean(x));
	}

	public static Vec4b vec4b(float x) {
		return new Vec4b(asBoolean(x), asBoolean(x), asBoolean(x), asBoolean(x));
	}

	public static Vec4b vec4b(int x) {
		return new Vec4b(asBoolean(x), asBoolean(x), asBoolean(x), asBoolean(x));
	}

	public static Vec4b vec4b(boolean x, boolean y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, double y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, float y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, int y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, boolean y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, double y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, float y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(double x, int y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, boolean y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, double y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, float y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(float x, int y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, boolean y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, double y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, float y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, boolean z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, boolean z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, boolean z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, boolean z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, double z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, double z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, double z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, double z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, float z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, float z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, float z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, float z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, int z, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, int z, double w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, int z, float w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(int x, int y, int z, int w) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, boolean y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, boolean y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, boolean y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, boolean y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, double y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, double y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, double y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, double y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, float y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, float y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, float y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, float y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, int y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, int y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, int y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, int y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, boolean y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, boolean y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, boolean y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, boolean y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, double y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, double y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, double y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, double y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, float y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, float y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, float y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, float y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, int y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, int y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, int y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(double x, int y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, boolean y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, boolean y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, boolean y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, boolean y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, double y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, double y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, double y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, double y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, float y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, float y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, float y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, float y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, int y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, int y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, int y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(float x, int y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, boolean y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, boolean y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, boolean y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, boolean y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, double y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, double y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, double y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, double y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, float y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, float y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, float y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, float y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, int y, Vec2b zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, int y, Vec2d zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, int y, Vec2f zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(int x, int y, Vec2i zw) {
		return new Vec4b(asBoolean(x), asBoolean(y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, Vec2b yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2b yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2b yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2b yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2d yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2d yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2d yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2d yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2f yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2f yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2f yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2f yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2i yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2i yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2i yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(boolean x, Vec2i yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2b yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2b yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2b yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2b yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2d yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2d yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2d yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2d yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2f yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2f yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2f yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2f yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2i yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2i yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2i yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(double x, Vec2i yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2b yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2b yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2b yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2b yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2d yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2d yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2d yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2d yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2f yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2f yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2f yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2f yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2i yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2i yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2i yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(float x, Vec2i yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2b yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2b yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2b yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2b yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2d yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2d yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2d yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2d yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2f yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2f yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2f yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2f yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2i yz, boolean w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2i yz, double w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2i yz, float w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(int x, Vec2i yz, int w) {
		return new Vec4b(asBoolean(x), asBoolean(yz.x), asBoolean(yz.y), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, boolean z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, boolean z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, boolean z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, boolean z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, double z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, double z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, double z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, double z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, float z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, float z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, float z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, float z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, int z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, int z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, int z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, int z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, boolean z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, boolean z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, boolean z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, boolean z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, double z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, double z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, double z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, double z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, float z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, float z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, float z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, float z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, int z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, int z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, int z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2d xy, int z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, boolean z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, boolean z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, boolean z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, boolean z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, double z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, double z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, double z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, double z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, float z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, float z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, float z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, float z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, int z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, int z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, int z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2f xy, int z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, boolean z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, boolean z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, boolean z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, boolean z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, double z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, double z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, double z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, double z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, float z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, float z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, float z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, float z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, int z, boolean w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, int z, double w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, int z, float w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2i xy, int z, int w) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec2b xy, Vec2b zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2b xy, Vec2d zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2b xy, Vec2f zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2b xy, Vec2i zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2d xy, Vec2b zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2d xy, Vec2d zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2d xy, Vec2f zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2d xy, Vec2i zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2f xy, Vec2b zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2f xy, Vec2d zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2f xy, Vec2f zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2f xy, Vec2i zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2i xy, Vec2b zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2i xy, Vec2d zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2i xy, Vec2f zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(Vec2i xy, Vec2i zw) {
		return new Vec4b(asBoolean(xy.x), asBoolean(xy.y), asBoolean(zw.x), asBoolean(zw.y));
	}

	public static Vec4b vec4b(boolean x, Vec3b yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(boolean x, Vec3d yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(boolean x, Vec3f yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(boolean x, Vec3i yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(double x, Vec3b yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(double x, Vec3d yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(double x, Vec3f yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(double x, Vec3i yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(float x, Vec3b yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(float x, Vec3d yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(float x, Vec3f yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(float x, Vec3i yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(int x, Vec3b yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(int x, Vec3d yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(int x, Vec3f yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(int x, Vec3i yzw) {
		return new Vec4b(asBoolean(x), asBoolean(yzw.x), asBoolean(yzw.y), asBoolean(yzw.z));
	}

	public static Vec4b vec4b(Vec3b xyz, boolean w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3b xyz, double w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3b xyz, float w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3b xyz, int w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3d xyz, boolean w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3d xyz, double w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3d xyz, float w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3d xyz, int w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3f xyz, boolean w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3f xyz, double w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3f xyz, float w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3f xyz, int w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3i xyz, boolean w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3i xyz, double w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3i xyz, float w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec3i xyz, int w) {
		return new Vec4b(asBoolean(xyz.x), asBoolean(xyz.y), asBoolean(xyz.z), asBoolean(w));
	}

	public static Vec4b vec4b(Vec4b xyzw) {
		return new Vec4b(asBoolean(xyzw.x), asBoolean(xyzw.y), asBoolean(xyzw.z), asBoolean(xyzw.w));
	}

	public static Vec4b vec4b(Vec4d xyzw) {
		return new Vec4b(asBoolean(xyzw.x), asBoolean(xyzw.y), asBoolean(xyzw.z), asBoolean(xyzw.w));
	}

	public static Vec4b vec4b(Vec4f xyzw) {
		return new Vec4b(asBoolean(xyzw.x), asBoolean(xyzw.y), asBoolean(xyzw.z), asBoolean(xyzw.w));
	}

	public static Vec4b vec4b(Vec4i xyzw) {
		return new Vec4b(asBoolean(xyzw.x), asBoolean(xyzw.y), asBoolean(xyzw.z), asBoolean(xyzw.w));
	}

	public static Vec4d vec4d(boolean x) {
		return new Vec4d(asDouble(x), asDouble(x), asDouble(x), asDouble(x));
	}

	public static Vec4d vec4d(double x) {
		return new Vec4d(asDouble(x), asDouble(x), asDouble(x), asDouble(x));
	}

	public static Vec4d vec4d(float x) {
		return new Vec4d(asDouble(x), asDouble(x), asDouble(x), asDouble(x));
	}

	public static Vec4d vec4d(int x) {
		return new Vec4d(asDouble(x), asDouble(x), asDouble(x), asDouble(x));
	}

	public static Vec4d vec4d(boolean x, boolean y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, double y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, float y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, int y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, boolean y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, double y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, float y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(double x, int y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, boolean y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, double y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, float y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(float x, int y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, boolean y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, double y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, float y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, boolean z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, boolean z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, boolean z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, boolean z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, double z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, double z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, double z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, double z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, float z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, float z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, float z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, float z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, int z, boolean w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, int z, double w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, int z, float w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(int x, int y, int z, int w) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, boolean y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, boolean y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, boolean y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, boolean y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, double y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, double y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, double y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, double y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, float y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, float y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, float y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, float y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, int y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, int y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, int y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, int y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, boolean y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, boolean y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, boolean y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, boolean y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, double y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, double y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, double y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, double y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, float y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, float y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, float y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, float y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, int y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, int y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, int y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(double x, int y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, boolean y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, boolean y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, boolean y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, boolean y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, double y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, double y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, double y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, double y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, float y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, float y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, float y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, float y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, int y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, int y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, int y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(float x, int y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, boolean y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, boolean y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, boolean y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, boolean y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, double y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, double y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, double y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, double y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, float y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, float y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, float y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, float y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, int y, Vec2b zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, int y, Vec2d zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, int y, Vec2f zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(int x, int y, Vec2i zw) {
		return new Vec4d(asDouble(x), asDouble(y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, Vec2b yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2b yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2b yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2b yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2d yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2d yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2d yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2d yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2f yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2f yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2f yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2f yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2i yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2i yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2i yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(boolean x, Vec2i yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2b yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2b yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2b yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2b yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2d yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2d yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2d yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2d yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2f yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2f yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2f yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2f yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2i yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2i yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2i yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(double x, Vec2i yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2b yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2b yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2b yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2b yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2d yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2d yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2d yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2d yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2f yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2f yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2f yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2f yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2i yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2i yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2i yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(float x, Vec2i yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2b yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2b yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2b yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2b yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2d yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2d yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2d yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2d yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2f yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2f yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2f yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2f yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2i yz, boolean w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2i yz, double w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2i yz, float w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(int x, Vec2i yz, int w) {
		return new Vec4d(asDouble(x), asDouble(yz.x), asDouble(yz.y), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, boolean z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, boolean z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, boolean z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, boolean z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, double z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, double z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, double z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, double z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, float z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, float z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, float z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, float z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, int z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, int z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, int z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, int z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, boolean z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, boolean z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, boolean z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, boolean z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, double z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, double z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, double z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, double z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, float z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, float z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, float z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, float z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, int z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, int z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, int z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2d xy, int z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, boolean z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, boolean z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, boolean z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, boolean z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, double z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, double z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, double z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, double z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, float z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, float z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, float z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, float z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, int z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, int z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, int z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2f xy, int z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, boolean z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, boolean z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, boolean z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, boolean z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, double z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, double z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, double z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, double z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, float z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, float z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, float z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, float z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, int z, boolean w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, int z, double w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, int z, float w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2i xy, int z, int w) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(z), asDouble(w));
	}

	public static Vec4d vec4d(Vec2b xy, Vec2b zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2b xy, Vec2d zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2b xy, Vec2f zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2b xy, Vec2i zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2d xy, Vec2b zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2d xy, Vec2d zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2d xy, Vec2f zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2d xy, Vec2i zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2f xy, Vec2b zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2f xy, Vec2d zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2f xy, Vec2f zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2f xy, Vec2i zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2i xy, Vec2b zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2i xy, Vec2d zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2i xy, Vec2f zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(Vec2i xy, Vec2i zw) {
		return new Vec4d(asDouble(xy.x), asDouble(xy.y), asDouble(zw.x), asDouble(zw.y));
	}

	public static Vec4d vec4d(boolean x, Vec3b yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(boolean x, Vec3d yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(boolean x, Vec3f yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(boolean x, Vec3i yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(double x, Vec3b yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(double x, Vec3d yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(double x, Vec3f yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(double x, Vec3i yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(float x, Vec3b yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(float x, Vec3d yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(float x, Vec3f yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(float x, Vec3i yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(int x, Vec3b yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(int x, Vec3d yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(int x, Vec3f yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(int x, Vec3i yzw) {
		return new Vec4d(asDouble(x), asDouble(yzw.x), asDouble(yzw.y), asDouble(yzw.z));
	}

	public static Vec4d vec4d(Vec3b xyz, boolean w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3b xyz, double w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3b xyz, float w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3b xyz, int w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3d xyz, boolean w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3d xyz, double w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3d xyz, float w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3d xyz, int w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3f xyz, boolean w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3f xyz, double w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3f xyz, float w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3f xyz, int w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3i xyz, boolean w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3i xyz, double w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3i xyz, float w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec3i xyz, int w) {
		return new Vec4d(asDouble(xyz.x), asDouble(xyz.y), asDouble(xyz.z), asDouble(w));
	}

	public static Vec4d vec4d(Vec4b xyzw) {
		return new Vec4d(asDouble(xyzw.x), asDouble(xyzw.y), asDouble(xyzw.z), asDouble(xyzw.w));
	}

	public static Vec4d vec4d(Vec4d xyzw) {
		return new Vec4d(asDouble(xyzw.x), asDouble(xyzw.y), asDouble(xyzw.z), asDouble(xyzw.w));
	}

	public static Vec4d vec4d(Vec4f xyzw) {
		return new Vec4d(asDouble(xyzw.x), asDouble(xyzw.y), asDouble(xyzw.z), asDouble(xyzw.w));
	}

	public static Vec4d vec4d(Vec4i xyzw) {
		return new Vec4d(asDouble(xyzw.x), asDouble(xyzw.y), asDouble(xyzw.z), asDouble(xyzw.w));
	}

	public static Vec4f vec4f(boolean x) {
		return new Vec4f(asFloat(x), asFloat(x), asFloat(x), asFloat(x));
	}

	public static Vec4f vec4f(double x) {
		return new Vec4f(asFloat(x), asFloat(x), asFloat(x), asFloat(x));
	}

	public static Vec4f vec4f(float x) {
		return new Vec4f(asFloat(x), asFloat(x), asFloat(x), asFloat(x));
	}

	public static Vec4f vec4f(int x) {
		return new Vec4f(asFloat(x), asFloat(x), asFloat(x), asFloat(x));
	}

	public static Vec4f vec4f(boolean x, boolean y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, double y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, float y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, int y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, boolean y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, double y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, float y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(double x, int y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, boolean y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, double y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, float y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(float x, int y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, boolean y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, double y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, float y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, boolean z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, boolean z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, boolean z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, boolean z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, double z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, double z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, double z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, double z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, float z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, float z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, float z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, float z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, int z, boolean w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, int z, double w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, int z, float w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(int x, int y, int z, int w) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, boolean y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, boolean y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, boolean y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, boolean y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, double y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, double y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, double y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, double y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, float y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, float y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, float y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, float y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, int y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, int y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, int y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, int y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, boolean y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, boolean y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, boolean y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, boolean y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, double y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, double y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, double y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, double y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, float y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, float y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, float y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, float y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, int y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, int y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, int y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(double x, int y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, boolean y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, boolean y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, boolean y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, boolean y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, double y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, double y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, double y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, double y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, float y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, float y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, float y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, float y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, int y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, int y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, int y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(float x, int y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, boolean y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, boolean y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, boolean y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, boolean y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, double y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, double y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, double y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, double y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, float y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, float y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, float y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, float y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, int y, Vec2b zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, int y, Vec2d zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, int y, Vec2f zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(int x, int y, Vec2i zw) {
		return new Vec4f(asFloat(x), asFloat(y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, Vec2b yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2b yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2b yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2b yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2d yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2d yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2d yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2d yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2f yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2f yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2f yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2f yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2i yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2i yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2i yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(boolean x, Vec2i yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2b yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2b yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2b yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2b yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2d yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2d yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2d yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2d yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2f yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2f yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2f yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2f yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2i yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2i yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2i yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(double x, Vec2i yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2b yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2b yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2b yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2b yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2d yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2d yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2d yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2d yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2f yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2f yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2f yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2f yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2i yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2i yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2i yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(float x, Vec2i yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2b yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2b yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2b yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2b yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2d yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2d yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2d yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2d yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2f yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2f yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2f yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2f yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2i yz, boolean w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2i yz, double w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2i yz, float w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(int x, Vec2i yz, int w) {
		return new Vec4f(asFloat(x), asFloat(yz.x), asFloat(yz.y), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, boolean z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, boolean z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, boolean z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, boolean z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, double z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, double z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, double z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, double z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, float z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, float z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, float z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, float z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, int z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, int z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, int z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, int z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, boolean z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, boolean z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, boolean z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, boolean z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, double z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, double z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, double z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, double z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, float z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, float z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, float z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, float z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, int z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, int z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, int z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2d xy, int z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, boolean z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, boolean z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, boolean z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, boolean z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, double z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, double z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, double z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, double z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, float z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, float z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, float z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, float z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, int z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, int z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, int z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2f xy, int z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, boolean z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, boolean z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, boolean z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, boolean z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, double z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, double z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, double z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, double z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, float z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, float z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, float z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, float z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, int z, boolean w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, int z, double w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, int z, float w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2i xy, int z, int w) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(z), asFloat(w));
	}

	public static Vec4f vec4f(Vec2b xy, Vec2b zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2b xy, Vec2d zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2b xy, Vec2f zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2b xy, Vec2i zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2d xy, Vec2b zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2d xy, Vec2d zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2d xy, Vec2f zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2d xy, Vec2i zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2f xy, Vec2b zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2f xy, Vec2d zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2f xy, Vec2f zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2f xy, Vec2i zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2i xy, Vec2b zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2i xy, Vec2d zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2i xy, Vec2f zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(Vec2i xy, Vec2i zw) {
		return new Vec4f(asFloat(xy.x), asFloat(xy.y), asFloat(zw.x), asFloat(zw.y));
	}

	public static Vec4f vec4f(boolean x, Vec3b yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(boolean x, Vec3d yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(boolean x, Vec3f yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(boolean x, Vec3i yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(double x, Vec3b yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(double x, Vec3d yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(double x, Vec3f yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(double x, Vec3i yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(float x, Vec3b yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(float x, Vec3d yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(float x, Vec3f yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(float x, Vec3i yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(int x, Vec3b yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(int x, Vec3d yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(int x, Vec3f yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(int x, Vec3i yzw) {
		return new Vec4f(asFloat(x), asFloat(yzw.x), asFloat(yzw.y), asFloat(yzw.z));
	}

	public static Vec4f vec4f(Vec3b xyz, boolean w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3b xyz, double w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3b xyz, float w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3b xyz, int w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3d xyz, boolean w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3d xyz, double w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3d xyz, float w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3d xyz, int w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3f xyz, boolean w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3f xyz, double w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3f xyz, float w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3f xyz, int w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3i xyz, boolean w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3i xyz, double w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3i xyz, float w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec3i xyz, int w) {
		return new Vec4f(asFloat(xyz.x), asFloat(xyz.y), asFloat(xyz.z), asFloat(w));
	}

	public static Vec4f vec4f(Vec4b xyzw) {
		return new Vec4f(asFloat(xyzw.x), asFloat(xyzw.y), asFloat(xyzw.z), asFloat(xyzw.w));
	}

	public static Vec4f vec4f(Vec4d xyzw) {
		return new Vec4f(asFloat(xyzw.x), asFloat(xyzw.y), asFloat(xyzw.z), asFloat(xyzw.w));
	}

	public static Vec4f vec4f(Vec4f xyzw) {
		return new Vec4f(asFloat(xyzw.x), asFloat(xyzw.y), asFloat(xyzw.z), asFloat(xyzw.w));
	}

	public static Vec4f vec4f(Vec4i xyzw) {
		return new Vec4f(asFloat(xyzw.x), asFloat(xyzw.y), asFloat(xyzw.z), asFloat(xyzw.w));
	}

	public static Vec4i vec4i(boolean x) {
		return new Vec4i(asInt(x), asInt(x), asInt(x), asInt(x));
	}

	public static Vec4i vec4i(double x) {
		return new Vec4i(asInt(x), asInt(x), asInt(x), asInt(x));
	}

	public static Vec4i vec4i(float x) {
		return new Vec4i(asInt(x), asInt(x), asInt(x), asInt(x));
	}

	public static Vec4i vec4i(int x) {
		return new Vec4i(asInt(x), asInt(x), asInt(x), asInt(x));
	}

	public static Vec4i vec4i(boolean x, boolean y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, double y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, float y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, int y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, boolean y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, double y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, float y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(double x, int y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, boolean y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, double y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, float y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(float x, int y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, boolean y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, double y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, float y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, boolean z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, boolean z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, boolean z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, boolean z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, double z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, double z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, double z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, double z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, float z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, float z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, float z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, float z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, int z, boolean w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, int z, double w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, int z, float w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(int x, int y, int z, int w) {
		return new Vec4i(asInt(x), asInt(y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(boolean x, boolean y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, boolean y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, boolean y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, boolean y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, double y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, double y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, double y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, double y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, float y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, float y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, float y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, float y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, int y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, int y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, int y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, int y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, boolean y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, boolean y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, boolean y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, boolean y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, double y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, double y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, double y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, double y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, float y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, float y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, float y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, float y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, int y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, int y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, int y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(double x, int y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, boolean y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, boolean y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, boolean y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, boolean y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, double y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, double y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, double y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, double y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, float y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, float y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, float y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, float y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, int y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, int y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, int y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(float x, int y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, boolean y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, boolean y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, boolean y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, boolean y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, double y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, double y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, double y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, double y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, float y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, float y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, float y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, float y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, int y, Vec2b zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, int y, Vec2d zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, int y, Vec2f zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(int x, int y, Vec2i zw) {
		return new Vec4i(asInt(x), asInt(y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, Vec2b yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2b yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2b yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2b yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2d yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2d yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2d yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2d yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2f yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2f yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2f yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2f yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2i yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2i yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2i yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(boolean x, Vec2i yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2b yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2b yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2b yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2b yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2d yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2d yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2d yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2d yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2f yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2f yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2f yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2f yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2i yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2i yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2i yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(double x, Vec2i yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2b yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2b yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2b yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2b yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2d yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2d yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2d yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2d yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2f yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2f yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2f yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2f yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2i yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2i yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2i yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(float x, Vec2i yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2b yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2b yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2b yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2b yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2d yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2d yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2d yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2d yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2f yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2f yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2f yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2f yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2i yz, boolean w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2i yz, double w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2i yz, float w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(int x, Vec2i yz, int w) {
		return new Vec4i(asInt(x), asInt(yz.x), asInt(yz.y), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, boolean z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, boolean z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, boolean z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, boolean z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, double z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, double z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, double z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, double z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, float z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, float z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, float z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, float z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, int z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, int z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, int z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, int z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, boolean z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, boolean z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, boolean z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, boolean z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, double z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, double z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, double z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, double z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, float z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, float z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, float z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, float z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, int z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, int z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, int z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2d xy, int z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, boolean z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, boolean z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, boolean z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, boolean z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, double z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, double z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, double z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, double z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, float z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, float z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, float z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, float z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, int z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, int z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, int z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2f xy, int z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, boolean z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, boolean z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, boolean z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, boolean z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, double z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, double z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, double z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, double z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, float z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, float z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, float z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, float z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, int z, boolean w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, int z, double w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, int z, float w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2i xy, int z, int w) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(z), asInt(w));
	}

	public static Vec4i vec4i(Vec2b xy, Vec2b zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2b xy, Vec2d zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2b xy, Vec2f zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2b xy, Vec2i zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2d xy, Vec2b zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2d xy, Vec2d zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2d xy, Vec2f zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2d xy, Vec2i zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2f xy, Vec2b zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2f xy, Vec2d zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2f xy, Vec2f zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2f xy, Vec2i zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2i xy, Vec2b zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2i xy, Vec2d zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2i xy, Vec2f zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(Vec2i xy, Vec2i zw) {
		return new Vec4i(asInt(xy.x), asInt(xy.y), asInt(zw.x), asInt(zw.y));
	}

	public static Vec4i vec4i(boolean x, Vec3b yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(boolean x, Vec3d yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(boolean x, Vec3f yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(boolean x, Vec3i yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(double x, Vec3b yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(double x, Vec3d yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(double x, Vec3f yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(double x, Vec3i yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(float x, Vec3b yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(float x, Vec3d yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(float x, Vec3f yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(float x, Vec3i yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(int x, Vec3b yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(int x, Vec3d yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(int x, Vec3f yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(int x, Vec3i yzw) {
		return new Vec4i(asInt(x), asInt(yzw.x), asInt(yzw.y), asInt(yzw.z));
	}

	public static Vec4i vec4i(Vec3b xyz, boolean w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3b xyz, double w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3b xyz, float w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3b xyz, int w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3d xyz, boolean w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3d xyz, double w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3d xyz, float w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3d xyz, int w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3f xyz, boolean w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3f xyz, double w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3f xyz, float w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3f xyz, int w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3i xyz, boolean w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3i xyz, double w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3i xyz, float w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec3i xyz, int w) {
		return new Vec4i(asInt(xyz.x), asInt(xyz.y), asInt(xyz.z), asInt(w));
	}

	public static Vec4i vec4i(Vec4b xyzw) {
		return new Vec4i(asInt(xyzw.x), asInt(xyzw.y), asInt(xyzw.z), asInt(xyzw.w));
	}

	public static Vec4i vec4i(Vec4d xyzw) {
		return new Vec4i(asInt(xyzw.x), asInt(xyzw.y), asInt(xyzw.z), asInt(xyzw.w));
	}

	public static Vec4i vec4i(Vec4f xyzw) {
		return new Vec4i(asInt(xyzw.x), asInt(xyzw.y), asInt(xyzw.z), asInt(xyzw.w));
	}

	public static Vec4i vec4i(Vec4i xyzw) {
		return new Vec4i(asInt(xyzw.x), asInt(xyzw.y), asInt(xyzw.z), asInt(xyzw.w));
	}

	/*
	 * Generated element-wise vector functions
	 */

	public static double deg2rad(double x) {
		return asDouble(Math.toRadians(x));
	}

	public static Vec2d deg2rad(Vec2d x) {
		return vec2d(Math.toRadians(x.x), Math.toRadians(x.y));
	}

	public static Vec3d deg2rad(Vec3d x) {
		return vec3d(Math.toRadians(x.x), Math.toRadians(x.y), Math.toRadians(x.z));
	}

	public static Vec4d deg2rad(Vec4d x) {
		return vec4d(Math.toRadians(x.x), Math.toRadians(x.y), Math.toRadians(x.z), Math.toRadians(x.w));
	}

	public static float deg2rad(float x) {
		return asFloat(Math.toRadians(x));
	}

	public static Vec2f deg2rad(Vec2f x) {
		return vec2f(Math.toRadians(x.x), Math.toRadians(x.y));
	}

	public static Vec3f deg2rad(Vec3f x) {
		return vec3f(Math.toRadians(x.x), Math.toRadians(x.y), Math.toRadians(x.z));
	}

	public static Vec4f deg2rad(Vec4f x) {
		return vec4f(Math.toRadians(x.x), Math.toRadians(x.y), Math.toRadians(x.z), Math.toRadians(x.w));
	}

	public static double rad2deg(double x) {
		return asDouble(Math.toDegrees(x));
	}

	public static Vec2d rad2deg(Vec2d x) {
		return vec2d(Math.toDegrees(x.x), Math.toDegrees(x.y));
	}

	public static Vec3d rad2deg(Vec3d x) {
		return vec3d(Math.toDegrees(x.x), Math.toDegrees(x.y), Math.toDegrees(x.z));
	}

	public static Vec4d rad2deg(Vec4d x) {
		return vec4d(Math.toDegrees(x.x), Math.toDegrees(x.y), Math.toDegrees(x.z), Math.toDegrees(x.w));
	}

	public static float rad2deg(float x) {
		return asFloat(Math.toDegrees(x));
	}

	public static Vec2f rad2deg(Vec2f x) {
		return vec2f(Math.toDegrees(x.x), Math.toDegrees(x.y));
	}

	public static Vec3f rad2deg(Vec3f x) {
		return vec3f(Math.toDegrees(x.x), Math.toDegrees(x.y), Math.toDegrees(x.z));
	}

	public static Vec4f rad2deg(Vec4f x) {
		return vec4f(Math.toDegrees(x.x), Math.toDegrees(x.y), Math.toDegrees(x.z), Math.toDegrees(x.w));
	}

	public static double sin(double x) {
		return asDouble(Math.sin(x));
	}

	public static Vec2d sin(Vec2d x) {
		return vec2d(Math.sin(x.x), Math.sin(x.y));
	}

	public static Vec3d sin(Vec3d x) {
		return vec3d(Math.sin(x.x), Math.sin(x.y), Math.sin(x.z));
	}

	public static Vec4d sin(Vec4d x) {
		return vec4d(Math.sin(x.x), Math.sin(x.y), Math.sin(x.z), Math.sin(x.w));
	}

	public static float sin(float x) {
		return asFloat(Math.sin(x));
	}

	public static Vec2f sin(Vec2f x) {
		return vec2f(Math.sin(x.x), Math.sin(x.y));
	}

	public static Vec3f sin(Vec3f x) {
		return vec3f(Math.sin(x.x), Math.sin(x.y), Math.sin(x.z));
	}

	public static Vec4f sin(Vec4f x) {
		return vec4f(Math.sin(x.x), Math.sin(x.y), Math.sin(x.z), Math.sin(x.w));
	}

	public static double cos(double x) {
		return asDouble(Math.cos(x));
	}

	public static Vec2d cos(Vec2d x) {
		return vec2d(Math.cos(x.x), Math.cos(x.y));
	}

	public static Vec3d cos(Vec3d x) {
		return vec3d(Math.cos(x.x), Math.cos(x.y), Math.cos(x.z));
	}

	public static Vec4d cos(Vec4d x) {
		return vec4d(Math.cos(x.x), Math.cos(x.y), Math.cos(x.z), Math.cos(x.w));
	}

	public static float cos(float x) {
		return asFloat(Math.cos(x));
	}

	public static Vec2f cos(Vec2f x) {
		return vec2f(Math.cos(x.x), Math.cos(x.y));
	}

	public static Vec3f cos(Vec3f x) {
		return vec3f(Math.cos(x.x), Math.cos(x.y), Math.cos(x.z));
	}

	public static Vec4f cos(Vec4f x) {
		return vec4f(Math.cos(x.x), Math.cos(x.y), Math.cos(x.z), Math.cos(x.w));
	}

	public static double tan(double x) {
		return asDouble(Math.tan(x));
	}

	public static Vec2d tan(Vec2d x) {
		return vec2d(Math.tan(x.x), Math.tan(x.y));
	}

	public static Vec3d tan(Vec3d x) {
		return vec3d(Math.tan(x.x), Math.tan(x.y), Math.tan(x.z));
	}

	public static Vec4d tan(Vec4d x) {
		return vec4d(Math.tan(x.x), Math.tan(x.y), Math.tan(x.z), Math.tan(x.w));
	}

	public static float tan(float x) {
		return asFloat(Math.tan(x));
	}

	public static Vec2f tan(Vec2f x) {
		return vec2f(Math.tan(x.x), Math.tan(x.y));
	}

	public static Vec3f tan(Vec3f x) {
		return vec3f(Math.tan(x.x), Math.tan(x.y), Math.tan(x.z));
	}

	public static Vec4f tan(Vec4f x) {
		return vec4f(Math.tan(x.x), Math.tan(x.y), Math.tan(x.z), Math.tan(x.w));
	}

	public static double asin(double x) {
		return asDouble(Math.asin(x));
	}

	public static Vec2d asin(Vec2d x) {
		return vec2d(Math.asin(x.x), Math.asin(x.y));
	}

	public static Vec3d asin(Vec3d x) {
		return vec3d(Math.asin(x.x), Math.asin(x.y), Math.asin(x.z));
	}

	public static Vec4d asin(Vec4d x) {
		return vec4d(Math.asin(x.x), Math.asin(x.y), Math.asin(x.z), Math.asin(x.w));
	}

	public static float asin(float x) {
		return asFloat(Math.asin(x));
	}

	public static Vec2f asin(Vec2f x) {
		return vec2f(Math.asin(x.x), Math.asin(x.y));
	}

	public static Vec3f asin(Vec3f x) {
		return vec3f(Math.asin(x.x), Math.asin(x.y), Math.asin(x.z));
	}

	public static Vec4f asin(Vec4f x) {
		return vec4f(Math.asin(x.x), Math.asin(x.y), Math.asin(x.z), Math.asin(x.w));
	}

	public static double acos(double x) {
		return asDouble(Math.acos(x));
	}

	public static Vec2d acos(Vec2d x) {
		return vec2d(Math.acos(x.x), Math.acos(x.y));
	}

	public static Vec3d acos(Vec3d x) {
		return vec3d(Math.acos(x.x), Math.acos(x.y), Math.acos(x.z));
	}

	public static Vec4d acos(Vec4d x) {
		return vec4d(Math.acos(x.x), Math.acos(x.y), Math.acos(x.z), Math.acos(x.w));
	}

	public static float acos(float x) {
		return asFloat(Math.acos(x));
	}

	public static Vec2f acos(Vec2f x) {
		return vec2f(Math.acos(x.x), Math.acos(x.y));
	}

	public static Vec3f acos(Vec3f x) {
		return vec3f(Math.acos(x.x), Math.acos(x.y), Math.acos(x.z));
	}

	public static Vec4f acos(Vec4f x) {
		return vec4f(Math.acos(x.x), Math.acos(x.y), Math.acos(x.z), Math.acos(x.w));
	}

	public static double atan(double x) {
		return asDouble(Math.atan(x));
	}

	public static Vec2d atan(Vec2d x) {
		return vec2d(Math.atan(x.x), Math.atan(x.y));
	}

	public static Vec3d atan(Vec3d x) {
		return vec3d(Math.atan(x.x), Math.atan(x.y), Math.atan(x.z));
	}

	public static Vec4d atan(Vec4d x) {
		return vec4d(Math.atan(x.x), Math.atan(x.y), Math.atan(x.z), Math.atan(x.w));
	}

	public static float atan(float x) {
		return asFloat(Math.atan(x));
	}

	public static Vec2f atan(Vec2f x) {
		return vec2f(Math.atan(x.x), Math.atan(x.y));
	}

	public static Vec3f atan(Vec3f x) {
		return vec3f(Math.atan(x.x), Math.atan(x.y), Math.atan(x.z));
	}

	public static Vec4f atan(Vec4f x) {
		return vec4f(Math.atan(x.x), Math.atan(x.y), Math.atan(x.z), Math.atan(x.w));
	}

	public static double atan(double y, double x) {
		return asDouble(Math.atan2(y, x));
	}

	public static Vec2d atan(Vec2d y, Vec2d x) {
		return vec2d(Math.atan2(y.x, x.x), Math.atan2(y.y, x.y));
	}

	public static Vec3d atan(Vec3d y, Vec3d x) {
		return vec3d(Math.atan2(y.x, x.x), Math.atan2(y.y, x.y), Math.atan2(y.z, x.z));
	}

	public static Vec4d atan(Vec4d y, Vec4d x) {
		return vec4d(Math.atan2(y.x, x.x), Math.atan2(y.y, x.y), Math.atan2(y.z, x.z), Math.atan2(y.w, x.w));
	}

	public static float atan(float y, float x) {
		return asFloat(Math.atan2(y, x));
	}

	public static Vec2f atan(Vec2f y, Vec2f x) {
		return vec2f(Math.atan2(y.x, x.x), Math.atan2(y.y, x.y));
	}

	public static Vec3f atan(Vec3f y, Vec3f x) {
		return vec3f(Math.atan2(y.x, x.x), Math.atan2(y.y, x.y), Math.atan2(y.z, x.z));
	}

	public static Vec4f atan(Vec4f y, Vec4f x) {
		return vec4f(Math.atan2(y.x, x.x), Math.atan2(y.y, x.y), Math.atan2(y.z, x.z), Math.atan2(y.w, x.w));
	}

	public static double pow(double x, double e) {
		return asDouble(Math.pow(x, e));
	}

	public static Vec2d pow(Vec2d x, Vec2d e) {
		return vec2d(Math.pow(x.x, e.x), Math.pow(x.y, e.y));
	}

	public static Vec3d pow(Vec3d x, Vec3d e) {
		return vec3d(Math.pow(x.x, e.x), Math.pow(x.y, e.y), Math.pow(x.z, e.z));
	}

	public static Vec4d pow(Vec4d x, Vec4d e) {
		return vec4d(Math.pow(x.x, e.x), Math.pow(x.y, e.y), Math.pow(x.z, e.z), Math.pow(x.w, e.w));
	}

	public static float pow(float x, float e) {
		return asFloat(Math.pow(x, e));
	}

	public static Vec2f pow(Vec2f x, Vec2f e) {
		return vec2f(Math.pow(x.x, e.x), Math.pow(x.y, e.y));
	}

	public static Vec3f pow(Vec3f x, Vec3f e) {
		return vec3f(Math.pow(x.x, e.x), Math.pow(x.y, e.y), Math.pow(x.z, e.z));
	}

	public static Vec4f pow(Vec4f x, Vec4f e) {
		return vec4f(Math.pow(x.x, e.x), Math.pow(x.y, e.y), Math.pow(x.z, e.z), Math.pow(x.w, e.w));
	}

	public static int pow(int x, int e) {
		return asInt(Math.pow(x, e));
	}

	public static Vec2i pow(Vec2i x, Vec2i e) {
		return vec2i(Math.pow(x.x, e.x), Math.pow(x.y, e.y));
	}

	public static Vec3i pow(Vec3i x, Vec3i e) {
		return vec3i(Math.pow(x.x, e.x), Math.pow(x.y, e.y), Math.pow(x.z, e.z));
	}

	public static Vec4i pow(Vec4i x, Vec4i e) {
		return vec4i(Math.pow(x.x, e.x), Math.pow(x.y, e.y), Math.pow(x.z, e.z), Math.pow(x.w, e.w));
	}

	public static double exp(double x) {
		return asDouble(Math.exp(x));
	}

	public static Vec2d exp(Vec2d x) {
		return vec2d(Math.exp(x.x), Math.exp(x.y));
	}

	public static Vec3d exp(Vec3d x) {
		return vec3d(Math.exp(x.x), Math.exp(x.y), Math.exp(x.z));
	}

	public static Vec4d exp(Vec4d x) {
		return vec4d(Math.exp(x.x), Math.exp(x.y), Math.exp(x.z), Math.exp(x.w));
	}

	public static float exp(float x) {
		return asFloat(Math.exp(x));
	}

	public static Vec2f exp(Vec2f x) {
		return vec2f(Math.exp(x.x), Math.exp(x.y));
	}

	public static Vec3f exp(Vec3f x) {
		return vec3f(Math.exp(x.x), Math.exp(x.y), Math.exp(x.z));
	}

	public static Vec4f exp(Vec4f x) {
		return vec4f(Math.exp(x.x), Math.exp(x.y), Math.exp(x.z), Math.exp(x.w));
	}

	public static double log(double x) {
		return asDouble(Math.log(x));
	}

	public static Vec2d log(Vec2d x) {
		return vec2d(Math.log(x.x), Math.log(x.y));
	}

	public static Vec3d log(Vec3d x) {
		return vec3d(Math.log(x.x), Math.log(x.y), Math.log(x.z));
	}

	public static Vec4d log(Vec4d x) {
		return vec4d(Math.log(x.x), Math.log(x.y), Math.log(x.z), Math.log(x.w));
	}

	public static float log(float x) {
		return asFloat(Math.log(x));
	}

	public static Vec2f log(Vec2f x) {
		return vec2f(Math.log(x.x), Math.log(x.y));
	}

	public static Vec3f log(Vec3f x) {
		return vec3f(Math.log(x.x), Math.log(x.y), Math.log(x.z));
	}

	public static Vec4f log(Vec4f x) {
		return vec4f(Math.log(x.x), Math.log(x.y), Math.log(x.z), Math.log(x.w));
	}

	public static double exp2(double x) {
		return asDouble(Math.pow(2.0, x));
	}

	public static Vec2d exp2(Vec2d x) {
		return vec2d(Math.pow(2.0, x.x), Math.pow(2.0, x.y));
	}

	public static Vec3d exp2(Vec3d x) {
		return vec3d(Math.pow(2.0, x.x), Math.pow(2.0, x.y), Math.pow(2.0, x.z));
	}

	public static Vec4d exp2(Vec4d x) {
		return vec4d(Math.pow(2.0, x.x), Math.pow(2.0, x.y), Math.pow(2.0, x.z), Math.pow(2.0, x.w));
	}

	public static float exp2(float x) {
		return asFloat(Math.pow(2.0, x));
	}

	public static Vec2f exp2(Vec2f x) {
		return vec2f(Math.pow(2.0, x.x), Math.pow(2.0, x.y));
	}

	public static Vec3f exp2(Vec3f x) {
		return vec3f(Math.pow(2.0, x.x), Math.pow(2.0, x.y), Math.pow(2.0, x.z));
	}

	public static Vec4f exp2(Vec4f x) {
		return vec4f(Math.pow(2.0, x.x), Math.pow(2.0, x.y), Math.pow(2.0, x.z), Math.pow(2.0, x.w));
	}

	public static double log2(double x) {
		return asDouble(Math.log(x) * 1.4426950408889634);
	}

	public static Vec2d log2(Vec2d x) {
		return vec2d(Math.log(x.x) * 1.4426950408889634, Math.log(x.y) * 1.4426950408889634);
	}

	public static Vec3d log2(Vec3d x) {
		return vec3d(Math.log(x.x) * 1.4426950408889634, Math.log(x.y) * 1.4426950408889634, Math.log(x.z) * 1.4426950408889634);
	}

	public static Vec4d log2(Vec4d x) {
		return vec4d(Math.log(x.x) * 1.4426950408889634, Math.log(x.y) * 1.4426950408889634, Math.log(x.z) * 1.4426950408889634, Math.log(x.w) * 1.4426950408889634);
	}

	public static float log2(float x) {
		return asFloat(Math.log(x) * 1.4426950408889634);
	}

	public static Vec2f log2(Vec2f x) {
		return vec2f(Math.log(x.x) * 1.4426950408889634, Math.log(x.y) * 1.4426950408889634);
	}

	public static Vec3f log2(Vec3f x) {
		return vec3f(Math.log(x.x) * 1.4426950408889634, Math.log(x.y) * 1.4426950408889634, Math.log(x.z) * 1.4426950408889634);
	}

	public static Vec4f log2(Vec4f x) {
		return vec4f(Math.log(x.x) * 1.4426950408889634, Math.log(x.y) * 1.4426950408889634, Math.log(x.z) * 1.4426950408889634, Math.log(x.w) * 1.4426950408889634);
	}

	public static double sqrt(double x) {
		return asDouble(Math.sqrt(x));
	}

	public static Vec2d sqrt(Vec2d x) {
		return vec2d(Math.sqrt(x.x), Math.sqrt(x.y));
	}

	public static Vec3d sqrt(Vec3d x) {
		return vec3d(Math.sqrt(x.x), Math.sqrt(x.y), Math.sqrt(x.z));
	}

	public static Vec4d sqrt(Vec4d x) {
		return vec4d(Math.sqrt(x.x), Math.sqrt(x.y), Math.sqrt(x.z), Math.sqrt(x.w));
	}

	public static float sqrt(float x) {
		return asFloat(Math.sqrt(x));
	}

	public static Vec2f sqrt(Vec2f x) {
		return vec2f(Math.sqrt(x.x), Math.sqrt(x.y));
	}

	public static Vec3f sqrt(Vec3f x) {
		return vec3f(Math.sqrt(x.x), Math.sqrt(x.y), Math.sqrt(x.z));
	}

	public static Vec4f sqrt(Vec4f x) {
		return vec4f(Math.sqrt(x.x), Math.sqrt(x.y), Math.sqrt(x.z), Math.sqrt(x.w));
	}

	public static double cbrt(double x) {
		return asDouble(Math.cbrt(x));
	}

	public static Vec2d cbrt(Vec2d x) {
		return vec2d(Math.cbrt(x.x), Math.cbrt(x.y));
	}

	public static Vec3d cbrt(Vec3d x) {
		return vec3d(Math.cbrt(x.x), Math.cbrt(x.y), Math.cbrt(x.z));
	}

	public static Vec4d cbrt(Vec4d x) {
		return vec4d(Math.cbrt(x.x), Math.cbrt(x.y), Math.cbrt(x.z), Math.cbrt(x.w));
	}

	public static float cbrt(float x) {
		return asFloat(Math.cbrt(x));
	}

	public static Vec2f cbrt(Vec2f x) {
		return vec2f(Math.cbrt(x.x), Math.cbrt(x.y));
	}

	public static Vec3f cbrt(Vec3f x) {
		return vec3f(Math.cbrt(x.x), Math.cbrt(x.y), Math.cbrt(x.z));
	}

	public static Vec4f cbrt(Vec4f x) {
		return vec4f(Math.cbrt(x.x), Math.cbrt(x.y), Math.cbrt(x.z), Math.cbrt(x.w));
	}

	public static double abs(double x) {
		return asDouble(Math.abs(x));
	}

	public static Vec2d abs(Vec2d x) {
		return vec2d(Math.abs(x.x), Math.abs(x.y));
	}

	public static Vec3d abs(Vec3d x) {
		return vec3d(Math.abs(x.x), Math.abs(x.y), Math.abs(x.z));
	}

	public static Vec4d abs(Vec4d x) {
		return vec4d(Math.abs(x.x), Math.abs(x.y), Math.abs(x.z), Math.abs(x.w));
	}

	public static float abs(float x) {
		return asFloat(Math.abs(x));
	}

	public static Vec2f abs(Vec2f x) {
		return vec2f(Math.abs(x.x), Math.abs(x.y));
	}

	public static Vec3f abs(Vec3f x) {
		return vec3f(Math.abs(x.x), Math.abs(x.y), Math.abs(x.z));
	}

	public static Vec4f abs(Vec4f x) {
		return vec4f(Math.abs(x.x), Math.abs(x.y), Math.abs(x.z), Math.abs(x.w));
	}

	public static int abs(int x) {
		return asInt(Math.abs(x));
	}

	public static Vec2i abs(Vec2i x) {
		return vec2i(Math.abs(x.x), Math.abs(x.y));
	}

	public static Vec3i abs(Vec3i x) {
		return vec3i(Math.abs(x.x), Math.abs(x.y), Math.abs(x.z));
	}

	public static Vec4i abs(Vec4i x) {
		return vec4i(Math.abs(x.x), Math.abs(x.y), Math.abs(x.z), Math.abs(x.w));
	}

	public static double sign(double x) {
		return asDouble(Math.signum(x));
	}

	public static Vec2d sign(Vec2d x) {
		return vec2d(Math.signum(x.x), Math.signum(x.y));
	}

	public static Vec3d sign(Vec3d x) {
		return vec3d(Math.signum(x.x), Math.signum(x.y), Math.signum(x.z));
	}

	public static Vec4d sign(Vec4d x) {
		return vec4d(Math.signum(x.x), Math.signum(x.y), Math.signum(x.z), Math.signum(x.w));
	}

	public static float sign(float x) {
		return asFloat(Math.signum(x));
	}

	public static Vec2f sign(Vec2f x) {
		return vec2f(Math.signum(x.x), Math.signum(x.y));
	}

	public static Vec3f sign(Vec3f x) {
		return vec3f(Math.signum(x.x), Math.signum(x.y), Math.signum(x.z));
	}

	public static Vec4f sign(Vec4f x) {
		return vec4f(Math.signum(x.x), Math.signum(x.y), Math.signum(x.z), Math.signum(x.w));
	}

	public static int sign(int x) {
		return asInt(asInt(x > 0) - asInt(x < 0));
	}

	public static Vec2i sign(Vec2i x) {
		return vec2i(asInt(x.x > 0) - asInt(x.x < 0), asInt(x.y > 0) - asInt(x.y < 0));
	}

	public static Vec3i sign(Vec3i x) {
		return vec3i(asInt(x.x > 0) - asInt(x.x < 0), asInt(x.y > 0) - asInt(x.y < 0), asInt(x.z > 0) - asInt(x.z < 0));
	}

	public static Vec4i sign(Vec4i x) {
		return vec4i(asInt(x.x > 0) - asInt(x.x < 0), asInt(x.y > 0) - asInt(x.y < 0), asInt(x.z > 0) - asInt(x.z < 0), asInt(x.w > 0) - asInt(x.w < 0));
	}

	public static double floor(double x) {
		return asDouble(Math.floor(x));
	}

	public static Vec2d floor(Vec2d x) {
		return vec2d(Math.floor(x.x), Math.floor(x.y));
	}

	public static Vec3d floor(Vec3d x) {
		return vec3d(Math.floor(x.x), Math.floor(x.y), Math.floor(x.z));
	}

	public static Vec4d floor(Vec4d x) {
		return vec4d(Math.floor(x.x), Math.floor(x.y), Math.floor(x.z), Math.floor(x.w));
	}

	public static float floor(float x) {
		return asFloat(Math.floor(x));
	}

	public static Vec2f floor(Vec2f x) {
		return vec2f(Math.floor(x.x), Math.floor(x.y));
	}

	public static Vec3f floor(Vec3f x) {
		return vec3f(Math.floor(x.x), Math.floor(x.y), Math.floor(x.z));
	}

	public static Vec4f floor(Vec4f x) {
		return vec4f(Math.floor(x.x), Math.floor(x.y), Math.floor(x.z), Math.floor(x.w));
	}

	public static double ceil(double x) {
		return asDouble(Math.ceil(x));
	}

	public static Vec2d ceil(Vec2d x) {
		return vec2d(Math.ceil(x.x), Math.ceil(x.y));
	}

	public static Vec3d ceil(Vec3d x) {
		return vec3d(Math.ceil(x.x), Math.ceil(x.y), Math.ceil(x.z));
	}

	public static Vec4d ceil(Vec4d x) {
		return vec4d(Math.ceil(x.x), Math.ceil(x.y), Math.ceil(x.z), Math.ceil(x.w));
	}

	public static float ceil(float x) {
		return asFloat(Math.ceil(x));
	}

	public static Vec2f ceil(Vec2f x) {
		return vec2f(Math.ceil(x.x), Math.ceil(x.y));
	}

	public static Vec3f ceil(Vec3f x) {
		return vec3f(Math.ceil(x.x), Math.ceil(x.y), Math.ceil(x.z));
	}

	public static Vec4f ceil(Vec4f x) {
		return vec4f(Math.ceil(x.x), Math.ceil(x.y), Math.ceil(x.z), Math.ceil(x.w));
	}

	public static double fract(double x) {
		return asDouble(x - floor(x));
	}

	public static Vec2d fract(Vec2d x) {
		return vec2d(x.x - floor(x.x), x.y - floor(x.y));
	}

	public static Vec3d fract(Vec3d x) {
		return vec3d(x.x - floor(x.x), x.y - floor(x.y), x.z - floor(x.z));
	}

	public static Vec4d fract(Vec4d x) {
		return vec4d(x.x - floor(x.x), x.y - floor(x.y), x.z - floor(x.z), x.w - floor(x.w));
	}

	public static float fract(float x) {
		return asFloat(x - floor(x));
	}

	public static Vec2f fract(Vec2f x) {
		return vec2f(x.x - floor(x.x), x.y - floor(x.y));
	}

	public static Vec3f fract(Vec3f x) {
		return vec3f(x.x - floor(x.x), x.y - floor(x.y), x.z - floor(x.z));
	}

	public static Vec4f fract(Vec4f x) {
		return vec4f(x.x - floor(x.x), x.y - floor(x.y), x.z - floor(x.z), x.w - floor(x.w));
	}

	public static double mod(double x, double d) {
		return asDouble(x - d * floor(x / d));
	}

	public static Vec2d mod(Vec2d x, Vec2d d) {
		return vec2d(x.x - d.x * floor(x.x / d.x), x.y - d.y * floor(x.y / d.y));
	}

	public static Vec3d mod(Vec3d x, Vec3d d) {
		return vec3d(x.x - d.x * floor(x.x / d.x), x.y - d.y * floor(x.y / d.y), x.z - d.z * floor(x.z / d.z));
	}

	public static Vec4d mod(Vec4d x, Vec4d d) {
		return vec4d(x.x - d.x * floor(x.x / d.x), x.y - d.y * floor(x.y / d.y), x.z - d.z * floor(x.z / d.z), x.w - d.w * floor(x.w / d.w));
	}

	public static float mod(float x, float d) {
		return asFloat(x - d * floor(x / d));
	}

	public static Vec2f mod(Vec2f x, Vec2f d) {
		return vec2f(x.x - d.x * floor(x.x / d.x), x.y - d.y * floor(x.y / d.y));
	}

	public static Vec3f mod(Vec3f x, Vec3f d) {
		return vec3f(x.x - d.x * floor(x.x / d.x), x.y - d.y * floor(x.y / d.y), x.z - d.z * floor(x.z / d.z));
	}

	public static Vec4f mod(Vec4f x, Vec4f d) {
		return vec4f(x.x - d.x * floor(x.x / d.x), x.y - d.y * floor(x.y / d.y), x.z - d.z * floor(x.z / d.z), x.w - d.w * floor(x.w / d.w));
	}

	public static int mod(int x, int d) {
		return asInt(Math.floorMod(x, d));
	}

	public static Vec2i mod(Vec2i x, Vec2i d) {
		return vec2i(Math.floorMod(x.x, d.x), Math.floorMod(x.y, d.y));
	}

	public static Vec3i mod(Vec3i x, Vec3i d) {
		return vec3i(Math.floorMod(x.x, d.x), Math.floorMod(x.y, d.y), Math.floorMod(x.z, d.z));
	}

	public static Vec4i mod(Vec4i x, Vec4i d) {
		return vec4i(Math.floorMod(x.x, d.x), Math.floorMod(x.y, d.y), Math.floorMod(x.z, d.z), Math.floorMod(x.w, d.w));
	}

	public static double min(double x, double y) {
		return asDouble(Math.min(x, y));
	}

	public static Vec2d min(Vec2d x, Vec2d y) {
		return vec2d(Math.min(x.x, y.x), Math.min(x.y, y.y));
	}

	public static Vec3d min(Vec3d x, Vec3d y) {
		return vec3d(Math.min(x.x, y.x), Math.min(x.y, y.y), Math.min(x.z, y.z));
	}

	public static Vec4d min(Vec4d x, Vec4d y) {
		return vec4d(Math.min(x.x, y.x), Math.min(x.y, y.y), Math.min(x.z, y.z), Math.min(x.w, y.w));
	}

	public static float min(float x, float y) {
		return asFloat(Math.min(x, y));
	}

	public static Vec2f min(Vec2f x, Vec2f y) {
		return vec2f(Math.min(x.x, y.x), Math.min(x.y, y.y));
	}

	public static Vec3f min(Vec3f x, Vec3f y) {
		return vec3f(Math.min(x.x, y.x), Math.min(x.y, y.y), Math.min(x.z, y.z));
	}

	public static Vec4f min(Vec4f x, Vec4f y) {
		return vec4f(Math.min(x.x, y.x), Math.min(x.y, y.y), Math.min(x.z, y.z), Math.min(x.w, y.w));
	}

	public static int min(int x, int y) {
		return asInt(Math.min(x, y));
	}

	public static Vec2i min(Vec2i x, Vec2i y) {
		return vec2i(Math.min(x.x, y.x), Math.min(x.y, y.y));
	}

	public static Vec3i min(Vec3i x, Vec3i y) {
		return vec3i(Math.min(x.x, y.x), Math.min(x.y, y.y), Math.min(x.z, y.z));
	}

	public static Vec4i min(Vec4i x, Vec4i y) {
		return vec4i(Math.min(x.x, y.x), Math.min(x.y, y.y), Math.min(x.z, y.z), Math.min(x.w, y.w));
	}

	public static double max(double x, double y) {
		return asDouble(Math.max(x, y));
	}

	public static Vec2d max(Vec2d x, Vec2d y) {
		return vec2d(Math.max(x.x, y.x), Math.max(x.y, y.y));
	}

	public static Vec3d max(Vec3d x, Vec3d y) {
		return vec3d(Math.max(x.x, y.x), Math.max(x.y, y.y), Math.max(x.z, y.z));
	}

	public static Vec4d max(Vec4d x, Vec4d y) {
		return vec4d(Math.max(x.x, y.x), Math.max(x.y, y.y), Math.max(x.z, y.z), Math.max(x.w, y.w));
	}

	public static float max(float x, float y) {
		return asFloat(Math.max(x, y));
	}

	public static Vec2f max(Vec2f x, Vec2f y) {
		return vec2f(Math.max(x.x, y.x), Math.max(x.y, y.y));
	}

	public static Vec3f max(Vec3f x, Vec3f y) {
		return vec3f(Math.max(x.x, y.x), Math.max(x.y, y.y), Math.max(x.z, y.z));
	}

	public static Vec4f max(Vec4f x, Vec4f y) {
		return vec4f(Math.max(x.x, y.x), Math.max(x.y, y.y), Math.max(x.z, y.z), Math.max(x.w, y.w));
	}

	public static int max(int x, int y) {
		return asInt(Math.max(x, y));
	}

	public static Vec2i max(Vec2i x, Vec2i y) {
		return vec2i(Math.max(x.x, y.x), Math.max(x.y, y.y));
	}

	public static Vec3i max(Vec3i x, Vec3i y) {
		return vec3i(Math.max(x.x, y.x), Math.max(x.y, y.y), Math.max(x.z, y.z));
	}

	public static Vec4i max(Vec4i x, Vec4i y) {
		return vec4i(Math.max(x.x, y.x), Math.max(x.y, y.y), Math.max(x.z, y.z), Math.max(x.w, y.w));
	}

	public static double clamp(double x, double a, double b) {
		return asDouble(min(max(x, a), b));
	}

	public static Vec2d clamp(Vec2d x, Vec2d a, Vec2d b) {
		return vec2d(min(max(x.x, a.x), b.x), min(max(x.y, a.y), b.y));
	}

	public static Vec3d clamp(Vec3d x, Vec3d a, Vec3d b) {
		return vec3d(min(max(x.x, a.x), b.x), min(max(x.y, a.y), b.y), min(max(x.z, a.z), b.z));
	}

	public static Vec4d clamp(Vec4d x, Vec4d a, Vec4d b) {
		return vec4d(min(max(x.x, a.x), b.x), min(max(x.y, a.y), b.y), min(max(x.z, a.z), b.z), min(max(x.w, a.w), b.w));
	}

	public static float clamp(float x, float a, float b) {
		return asFloat(min(max(x, a), b));
	}

	public static Vec2f clamp(Vec2f x, Vec2f a, Vec2f b) {
		return vec2f(min(max(x.x, a.x), b.x), min(max(x.y, a.y), b.y));
	}

	public static Vec3f clamp(Vec3f x, Vec3f a, Vec3f b) {
		return vec3f(min(max(x.x, a.x), b.x), min(max(x.y, a.y), b.y), min(max(x.z, a.z), b.z));
	}

	public static Vec4f clamp(Vec4f x, Vec4f a, Vec4f b) {
		return vec4f(min(max(x.x, a.x), b.x), min(max(x.y, a.y), b.y), min(max(x.z, a.z), b.z), min(max(x.w, a.w), b.w));
	}

	public static int clamp(int x, int a, int b) {
		return asInt(min(max(x, a), b));
	}

	public static Vec2i clamp(Vec2i x, Vec2i a, Vec2i b) {
		return vec2i(min(max(x.x, a.x), b.x), min(max(x.y, a.y), b.y));
	}

	public static Vec3i clamp(Vec3i x, Vec3i a, Vec3i b) {
		return vec3i(min(max(x.x, a.x), b.x), min(max(x.y, a.y), b.y), min(max(x.z, a.z), b.z));
	}

	public static Vec4i clamp(Vec4i x, Vec4i a, Vec4i b) {
		return vec4i(min(max(x.x, a.x), b.x), min(max(x.y, a.y), b.y), min(max(x.z, a.z), b.z), min(max(x.w, a.w), b.w));
	}

	public static double mix(double x, double y, double t) {
		return asDouble(x * (1 - t) + y * t);
	}

	public static Vec2d mix(Vec2d x, Vec2d y, Vec2d t) {
		return vec2d(x.x * (1 - t.x) + y.x * t.x, x.y * (1 - t.y) + y.y * t.y);
	}

	public static Vec3d mix(Vec3d x, Vec3d y, Vec3d t) {
		return vec3d(x.x * (1 - t.x) + y.x * t.x, x.y * (1 - t.y) + y.y * t.y, x.z * (1 - t.z) + y.z * t.z);
	}

	public static Vec4d mix(Vec4d x, Vec4d y, Vec4d t) {
		return vec4d(x.x * (1 - t.x) + y.x * t.x, x.y * (1 - t.y) + y.y * t.y, x.z * (1 - t.z) + y.z * t.z, x.w * (1 - t.w) + y.w * t.w);
	}

	public static float mix(float x, float y, float t) {
		return asFloat(x * (1 - t) + y * t);
	}

	public static Vec2f mix(Vec2f x, Vec2f y, Vec2f t) {
		return vec2f(x.x * (1 - t.x) + y.x * t.x, x.y * (1 - t.y) + y.y * t.y);
	}

	public static Vec3f mix(Vec3f x, Vec3f y, Vec3f t) {
		return vec3f(x.x * (1 - t.x) + y.x * t.x, x.y * (1 - t.y) + y.y * t.y, x.z * (1 - t.z) + y.z * t.z);
	}

	public static Vec4f mix(Vec4f x, Vec4f y, Vec4f t) {
		return vec4f(x.x * (1 - t.x) + y.x * t.x, x.y * (1 - t.y) + y.y * t.y, x.z * (1 - t.z) + y.z * t.z, x.w * (1 - t.w) + y.w * t.w);
	}

	public static double mix(double x, double y, boolean t) {
		return asDouble(t ? y : x);
	}

	public static Vec2d mix(Vec2d x, Vec2d y, Vec2b t) {
		return vec2d(t.x ? y.x : x.x, t.y ? y.y : x.y);
	}

	public static Vec3d mix(Vec3d x, Vec3d y, Vec3b t) {
		return vec3d(t.x ? y.x : x.x, t.y ? y.y : x.y, t.z ? y.z : x.z);
	}

	public static Vec4d mix(Vec4d x, Vec4d y, Vec4b t) {
		return vec4d(t.x ? y.x : x.x, t.y ? y.y : x.y, t.z ? y.z : x.z, t.w ? y.w : x.w);
	}

	public static float mix(float x, float y, boolean t) {
		return asFloat(t ? y : x);
	}

	public static Vec2f mix(Vec2f x, Vec2f y, Vec2b t) {
		return vec2f(t.x ? y.x : x.x, t.y ? y.y : x.y);
	}

	public static Vec3f mix(Vec3f x, Vec3f y, Vec3b t) {
		return vec3f(t.x ? y.x : x.x, t.y ? y.y : x.y, t.z ? y.z : x.z);
	}

	public static Vec4f mix(Vec4f x, Vec4f y, Vec4b t) {
		return vec4f(t.x ? y.x : x.x, t.y ? y.y : x.y, t.z ? y.z : x.z, t.w ? y.w : x.w);
	}

	public static int mix(int x, int y, boolean t) {
		return asInt(t ? y : x);
	}

	public static Vec2i mix(Vec2i x, Vec2i y, Vec2b t) {
		return vec2i(t.x ? y.x : x.x, t.y ? y.y : x.y);
	}

	public static Vec3i mix(Vec3i x, Vec3i y, Vec3b t) {
		return vec3i(t.x ? y.x : x.x, t.y ? y.y : x.y, t.z ? y.z : x.z);
	}

	public static Vec4i mix(Vec4i x, Vec4i y, Vec4b t) {
		return vec4i(t.x ? y.x : x.x, t.y ? y.y : x.y, t.z ? y.z : x.z, t.w ? y.w : x.w);
	}

	public static boolean mix(boolean x, boolean y, boolean t) {
		return asBoolean(t ? y : x);
	}

	public static Vec2b mix(Vec2b x, Vec2b y, Vec2b t) {
		return vec2b(t.x ? y.x : x.x, t.y ? y.y : x.y);
	}

	public static Vec3b mix(Vec3b x, Vec3b y, Vec3b t) {
		return vec3b(t.x ? y.x : x.x, t.y ? y.y : x.y, t.z ? y.z : x.z);
	}

	public static Vec4b mix(Vec4b x, Vec4b y, Vec4b t) {
		return vec4b(t.x ? y.x : x.x, t.y ? y.y : x.y, t.z ? y.z : x.z, t.w ? y.w : x.w);
	}

	public static boolean isinf(double x) {
		return asBoolean(Double.isInfinite(x));
	}

	public static Vec2b isinf(Vec2d x) {
		return vec2b(Double.isInfinite(x.x), Double.isInfinite(x.y));
	}

	public static Vec3b isinf(Vec3d x) {
		return vec3b(Double.isInfinite(x.x), Double.isInfinite(x.y), Double.isInfinite(x.z));
	}

	public static Vec4b isinf(Vec4d x) {
		return vec4b(Double.isInfinite(x.x), Double.isInfinite(x.y), Double.isInfinite(x.z), Double.isInfinite(x.w));
	}

	public static boolean isinf(float x) {
		return asBoolean(Float.isInfinite(x));
	}

	public static Vec2b isinf(Vec2f x) {
		return vec2b(Float.isInfinite(x.x), Float.isInfinite(x.y));
	}

	public static Vec3b isinf(Vec3f x) {
		return vec3b(Float.isInfinite(x.x), Float.isInfinite(x.y), Float.isInfinite(x.z));
	}

	public static Vec4b isinf(Vec4f x) {
		return vec4b(Float.isInfinite(x.x), Float.isInfinite(x.y), Float.isInfinite(x.z), Float.isInfinite(x.w));
	}

	public static boolean isnan(double x) {
		return asBoolean(Double.isNaN(x));
	}

	public static Vec2b isnan(Vec2d x) {
		return vec2b(Double.isNaN(x.x), Double.isNaN(x.y));
	}

	public static Vec3b isnan(Vec3d x) {
		return vec3b(Double.isNaN(x.x), Double.isNaN(x.y), Double.isNaN(x.z));
	}

	public static Vec4b isnan(Vec4d x) {
		return vec4b(Double.isNaN(x.x), Double.isNaN(x.y), Double.isNaN(x.z), Double.isNaN(x.w));
	}

	public static boolean isnan(float x) {
		return asBoolean(Float.isNaN(x));
	}

	public static Vec2b isnan(Vec2f x) {
		return vec2b(Float.isNaN(x.x), Float.isNaN(x.y));
	}

	public static Vec3b isnan(Vec3f x) {
		return vec3b(Float.isNaN(x.x), Float.isNaN(x.y), Float.isNaN(x.z));
	}

	public static Vec4b isnan(Vec4f x) {
		return vec4b(Float.isNaN(x.x), Float.isNaN(x.y), Float.isNaN(x.z), Float.isNaN(x.w));
	}


}
