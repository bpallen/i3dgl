package initial3d;

import java.util.Arrays;

import initial3d.detail.ArrayMath;
import initial3d.detail.StringUtil;

public class Mat4f {

	public static final Mat4f zero = new Mat4f();
	public static final Mat4f eye = new Mat4f(1);
	
	// column-major
	private final float[] e;

	private static int index(int r, int c) {
		return 4 * c + r;
	}
	
	private static int arraySize() {
		return 16;
	}

	public Mat4f() {
		e = new float[arraySize()];
	}
	
	public Mat4f(float x) {
		e = new float[] { x, 0, 0, 0, 0, x, 0, 0, 0, 0, x, 0, 0, 0, 0, x };
	}
	
	public Mat4f(
		float e00_, float e01_, float e02_, float e03_,
		float e10_, float e11_, float e12_, float e13_,
		float e20_, float e21_, float e22_, float e23_,
		float e30_, float e31_, float e32_, float e33_
	) {
		e = new float[] { e00_, e01_, e02_, e03_, e10_, e11_, e12_, e13_, e20_, e21_, e22_, e23_, e30_, e31_, e32_, e33_ };
	}

	public float get(int r, int c) {
		return e[index(r, c)];
	}

	public Mat4f with(int r, int c, float val) {
		Mat4f m = new Mat4f();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(r, c)] = val;
		return m;
	}

	public Vec4f col(int c) {
		return new Vec4f(get(0, c), get(1, c), get(2, c), get(3, c));
	}

	public Vec4f row(int r) {
		return new Vec4f(get(r, 0), get(r, 1), get(r, 2), get(r, 3));
	}

	public Mat4f withCol(int c, Vec4f v) {
		Mat4f m = new Mat4f();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(0, c)] = v.x;
		m.e[index(1, c)] = v.y;
		m.e[index(2, c)] = v.z;
		m.e[index(3, c)] = v.w;
		return m;
	}

	public Mat4f withRow(int r, Vec4f v) {
		Mat4f m = new Mat4f();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(r, 0)] = v.x;
		m.e[index(r, 1)] = v.y;
		m.e[index(r, 2)] = v.z;
		m.e[index(r, 3)] = v.w;
		return m;
	}

	public Mat4f add(Mat4f rhs) {
		Mat4f m = new Mat4f();
		ArrayMath.add(arraySize(), m.e, 0, e, 0, rhs.e, 0);
		return m;
	}

	public Mat4f add(float rhs) {
		Mat4f m = new Mat4f();
		ArrayMath.add(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat4f sub(Mat4f rhs) {
		Mat4f m = new Mat4f();
		ArrayMath.sub(arraySize(), m.e, 0, e, 0, rhs.e, 0);
		return m;
	}

	public Mat4f sub(float rhs) {
		Mat4f m = new Mat4f();
		ArrayMath.sub(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat4f mul(float rhs) {
		Mat4f m = new Mat4f();
		ArrayMath.mul(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat4f div(float rhs) {
		Mat4f m = new Mat4f();
		ArrayMath.div(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat4f mul(Mat4f rhs) {
		Mat4f m = new Mat4f();
		float r;
		float t0;
		float t1;
		float t2;
		float t3;
		r = rhs.get(0, 0);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		t3 = this.get(3, 0) * r;
		r = rhs.get(1, 0);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		t3 += this.get(3, 1) * r;
		r = rhs.get(2, 0);
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		t3 += this.get(3, 2) * r;
		r = rhs.get(3, 0);
		t0 += this.get(0, 3) * r;
		t1 += this.get(1, 3) * r;
		t2 += this.get(2, 3) * r;
		t3 += this.get(3, 3) * r;
		m.e[index(0, 0)] = t0;
		m.e[index(1, 0)] = t1;
		m.e[index(2, 0)] = t2;
		m.e[index(3, 0)] = t3;
		r = rhs.get(0, 1);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		t3 = this.get(3, 0) * r;
		r = rhs.get(1, 1);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		t3 += this.get(3, 1) * r;
		r = rhs.get(2, 1);
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		t3 += this.get(3, 2) * r;
		r = rhs.get(3, 1);
		t0 += this.get(0, 3) * r;
		t1 += this.get(1, 3) * r;
		t2 += this.get(2, 3) * r;
		t3 += this.get(3, 3) * r;
		m.e[index(0, 1)] = t0;
		m.e[index(1, 1)] = t1;
		m.e[index(2, 1)] = t2;
		m.e[index(3, 1)] = t3;
		r = rhs.get(0, 2);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		t3 = this.get(3, 0) * r;
		r = rhs.get(1, 2);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		t3 += this.get(3, 1) * r;
		r = rhs.get(2, 2);
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		t3 += this.get(3, 2) * r;
		r = rhs.get(3, 2);
		t0 += this.get(0, 3) * r;
		t1 += this.get(1, 3) * r;
		t2 += this.get(2, 3) * r;
		t3 += this.get(3, 3) * r;
		m.e[index(0, 2)] = t0;
		m.e[index(1, 2)] = t1;
		m.e[index(2, 2)] = t2;
		m.e[index(3, 2)] = t3;
		r = rhs.get(0, 3);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		t3 = this.get(3, 0) * r;
		r = rhs.get(1, 3);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		t3 += this.get(3, 1) * r;
		r = rhs.get(2, 3);
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		t3 += this.get(3, 2) * r;
		r = rhs.get(3, 3);
		t0 += this.get(0, 3) * r;
		t1 += this.get(1, 3) * r;
		t2 += this.get(2, 3) * r;
		t3 += this.get(3, 3) * r;
		m.e[index(0, 3)] = t0;
		m.e[index(1, 3)] = t1;
		m.e[index(2, 3)] = t2;
		m.e[index(3, 3)] = t3;
		return m;
	}

	public Vec4f mul(Vec4f rhs) {
		float r;
		float t0;
		float t1;
		float t2;
		float t3;
		r = rhs.x;
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		t3 = this.get(3, 0) * r;
		r = rhs.y;
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		t3 += this.get(3, 1) * r;
		r = rhs.z;
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		t3 += this.get(3, 2) * r;
		r = rhs.w;
		t0 += this.get(0, 3) * r;
		t1 += this.get(1, 3) * r;
		t2 += this.get(2, 3) * r;
		t3 += this.get(3, 3) * r;
		return new Vec4f(t0, t1, t2, t3);
	}

	@Override
	public String toString() {
		return StringUtil.matrixToString(e, 4);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(e);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Mat4f other = (Mat4f) obj;
		if (!Arrays.equals(e, other.e)) return false;
		return true;
	}
	
}
