package initial3d.test.terrain.voronoi;

class Edge {
	public double a = 0, b = 0, c = 0;
	Site[] ep; // End points?
	Site[] reg; // Sites this edge bisects?
	int edgenbr;

	Edge() {
		ep = new Site[2];
		reg = new Site[2];
	}
}
