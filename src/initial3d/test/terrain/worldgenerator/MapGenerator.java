/* Map generation project
 * <http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/>
 * Copyright 2010 Amit J Patel <amitp@cs.stanford.edu>
 * 
 * licensed under the MIT Open Source license
 * <http://www.opensource.org/licenses/mit-license.php>
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to 
 * the following conditions:
 *   
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software. 
 *   
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

package initial3d.test.terrain.worldgenerator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;

import ecs100.UI;
import initial3d.Vec2d;
import initial3d.Vec2i;
import initial3d.Vec3d;
import initial3d.test.terrain.voronoi.*;
import initial3d.test.terrain.Perlin;

import static initial3d.Functions.*;
import static initial3d.test.terrain.worldgenerator.Biome.*;

public class MapGenerator {

	public static final int NUM_POINTS = 2000;
	// 0 to 1, fraction of water corners for water polygon
	public static final double LAKE_THRESHOLD = 0.3; 
	public static final int NUM_LLOYD_ITERATIONS = 2;

	// Passed in by the caller:
	public final int width;
	public final int height;
	public final Vec2d dimension;
	public final long seed;

	// Island shape is controlled by the islandRandom seed and the
	// type of island, passed in when we set the island shape. The
	// islandShape function uses both of them to determine whether any
	// point should be water or land. TODO change description
	public final IslandStrategy islandShape;
	public final Random mapRandom;

	// These store the graph data
	public List<Polygon> polygons = new ArrayList<Polygon>();
	public List<Vertex> vertices = new ArrayList<Vertex>();
	public List<Edge> edges = new ArrayList<Edge>();
	

	public MapGenerator(long seed, int width, int height) {
		mapRandom = new Random(seed);
		this.width = width;
		this.height = height;
		this.dimension = vec2d(width, height);
		this.seed = seed;

//		this.islandShape = new PerlinIsland(seed);
		this.islandShape = new PerlinSanctuary(seed);
	}
	
	public List<Triangle> getTriangles(){
		List<Triangle> tri = new ArrayList<Triangle>();
		for(Polygon c : polygons){
			Vec3d col = vec3d(c.biome.color.getRed()/255.0, c.biome.color.getGreen()/255.0, c.biome.color.getBlue()/255.0);
			for(Edge e : c.edges){
				tri.add(new Triangle(
						new Vec2d[]{c.point, e.v0.point, e.v1.point}, 
						new double[]{c.elevation, e.v0.elevation, e.v1.elevation},
						col
				));
			}
		}
		return tri;
	}

	public void run() {
		// Generate Random points and Relax points
		List<Vec2d> points = generateRandomPoints(NUM_POINTS);
		points = relaxPoints(points);

		// Building Graph
		buildGraph(points);
//		improveCorners();

		// Assign heights and land types
		islandShape.assignCornerElevations(vertices);
		assignOceanCoastAndLand();
		
		// Redistribute elevations to non-land corners
		// And assign elevations to polygons
		redistributeElevations(landCorners(vertices));
		//change coast and ocean to zero
		for (Vertex q : vertices) {
			if (q.ocean || q.coast) {
				q.elevation = 0.0;
			}
		}
		assignPolygonElevations();

		// Work out rivers
		calculateDownslopes();
		calculateWatersheds();
		createRivers();

		// Assign Moisture and Biome
		assignVertexMoisture();
		redistributeMoisture(landCorners(vertices));
		assignPolygonMoisture();
		for (Polygon p : polygons) {
			p.biome = getBiome(p);
		}
	}

	// Generate a List of edges that correspond to Voronoi edges
	// for the list of given points
	private List<GraphEdge> createVoronoi(List<Vec2d> points) {
		double[] xValues = new double[points.size()];
		double[] yValues = new double[points.size()];

		for (int i = 0; i < points.size(); i++) {
			xValues[i] = points.get(i).x;
			yValues[i] = points.get(i).y;
		}

		Voronoi v = new Voronoi(0.01);
		return v.generateVoronoi(xValues, yValues, 0, width, 0, height);
	}

	
	// Generate N random points inside map (buffer of 10)
	private List<Vec2d> generateRandomPoints(int num) {
		List<Vec2d> points = new ArrayList<Vec2d>();

		for (int i = 0; i < num; i++) {
			points.add(vec2d(nextInRange(10, this.width - 10), nextInRange(10, this.height - 10)));
		}
		return points;
	}

	// Pseudo random get next random in range method
	private double nextInRange(int low, int high) {
		return (mapRandom.nextDouble() * (high - low)) + low;
	}

	// Use Llyod relaxation for spacing the points
	private List<Vec2d> relaxPoints(List<Vec2d> points) {
		
		for (int i = 0; i < NUM_LLOYD_ITERATIONS; i++) {
			List<GraphEdge> gEdges = createVoronoi(points);
			
			List<Vec2d> newPoints = new ArrayList<>(points.size());
			for (int r = 0; r < points.size(); r++) {
				
				Set<Vec2d> region = new HashSet<Vec2d>();
				// populate the set of vertices surrounding the polygon
				for (GraphEdge e : gEdges) {
					if (e.site1 == r || e.site2 == r) {
						region.add(vec2d((int) e.x1, (int) e.y1));
						region.add(vec2d((int) e.x2, (int) e.y2));
					}
				}
				
				Vec2d p = Vec2d.zero;
				for (Vec2d q : region) {
					p = p.add(q);
				}
				p = p.div(region.size());
				newPoints.add(p);
			}
			points = newPoints;
		}
		return points;
	}


	private void buildGraph(List<Vec2d> points) {

		// Build Center objects for each of the points
		for (Vec2d point : points) {
			Polygon p = new Polygon(polygons.size(), point);
			polygons.add(p);
		}

		// The Voronoi library generates lines from point to point.
		// To centralize edges on one "corner" we get a map of corners
		HashMap<Vec2i, Vertex> cornerMap = new HashMap<Vec2i, Vertex>();
		

		for (GraphEdge vorEdge : createVoronoi(points)) {
			Vec2d p1 = vec2d(vorEdge.x1, vorEdge.y1);
			Vec2d p2 = vec2d(vorEdge.x2, vorEdge.y2);
			
			Vec2i ip1 = vec2i(p1);
			Vec2i ip2 = vec2i(p2);
			
			
			// don't want polygons on the outside to be connected
			// via a voronoi edge of length 0
			if (all(ip1.eq(ip2))) {
				continue;
			}

			// Make an Edge corresponding to the voronoi graph edge.
			Edge edge = new Edge(edges.size(), (p1.add(p2)).div(2));
			edges.add(edge);
			
			// Edges point to corners. Edges point to centers.
			edge.v0 = getVertex(cornerMap, p1);
			edge.v1 = getVertex(cornerMap, p2);

			// m.siteX refers to the index of the point passed.
			// get centers by site1 and site2 for given edge
			edge.p0 = polygons.get(vorEdge.site1);
			edge.p1 = polygons.get(vorEdge.site2);

			// Add to the edges of the polygon
			edge.p0.edges.add(edge);
			edge.p1.edges.add(edge);
			// Add to edges of the vertex
			edge.v0.edges.add(edge);
			edge.v1.edges.add(edge);

			
			// Connect centers
			edge.p0.neighbors.add(edge.p1);
			edge.p1.neighbors.add(edge.p0);

			// Connect corners
			edge.v0.neighbours.add(edge.v1);
			edge.v1.neighbours.add(edge.v0);

			// Centers to corners
			edge.p0.vertices.add(edge.v0);
			edge.p0.vertices.add(edge.v1);
			edge.p1.vertices.add(edge.v0);
			edge.p1.vertices.add(edge.v1);

			// Corners to centers
			edge.v0.polygons.add(edge.p0);
			edge.v0.polygons.add(edge.p1);
			edge.v1.polygons.add(edge.p0);
			edge.v1.polygons.add(edge.p1);
			
		}
	}


	// Takes a mapping of Vec2d to Verticies
	// TODO fix relying on floats being the same for matching
	private Vertex getVertex(Map<Vec2i, Vertex> cornerMap, Vec2d point) {
		Vec2i intVec = vec2i(point);
		// For a given point, get a corresponding corner if one exists
		Vertex q = cornerMap.get(intVec);
		if (q != null) return q;
		
		// Otherwise create and store corner in map
		q = new Vertex(vertices.size(), point);
		q.index = vertices.size();

		// add this corner to global corners
		// and index it by its point
		vertices.add(q);
		q.border = any(point.le(Vec2d.zero)) || any(point.ge(this.dimension));
		cornerMap.put(intVec, q);
		
		return q;
	}

	
	// Another smoothing step for relaxing the edges
	private void improveCorners() {
		// The corners are moved to the average of the polygon centers around them.
		// Short edges become longer. Long edges tend to become shorter. The
		// polygons tend to be more uniform after this step.
		// Although Lloyd relaxation improves the uniformity of polygon
		// sizes, it doesn't help with the edge lengths. Short edges can
		// be bad for some games, and lead to weird artifacts on
		// rivers. We can easily lengthen short edges by moving the
		// vertices, but **we lose the Voronoi property**.

		Vec2d[] newCorners = new Vec2d[vertices.size()];

		// First we compute the average of the polygons next to each vertices.
		for (Vertex q : vertices) {
			if (q.border) {
				newCorners[q.index] = q.point;
			} else {
				Vec2d point = Vec2d.zero;
				for (Polygon r : q.polygons) {
					point = point.add(r.point);
				}
				point = point.div(q.polygons.size());
				newCorners[q.index] = point;
			}
		}

		// Move the vertices to the new locations.
		for (int i = 0; i < vertices.size(); i++) {
			vertices.get(i).point = newCorners[i];
		}

		// The edge midpoints were computed for the old vertices and need
		// to be recomputed.
		for (Edge edge : edges) {
			double midX = (edge.v0.point.x + edge.v1.point.x) / 2;
			double midY = (edge.v0.point.y + edge.v1.point.y) / 2;
			edge.midpoint = vec2d(midX, midY);
		}
	}
	

	// Return list of vertices that are not flagged as ocean or coast
	private List<Vertex> landCorners(List<Vertex> corners) {
		List<Vertex> locations = new ArrayList<Vertex>(corners.size());
		for (Vertex q : corners) {
			if (!q.ocean && !q.coast) {
				locations.add(q);
			}
		}
		return locations;
	}
	

	/** 
	 * Change the overall distribution of elevations so that lower
	 * elevations are more common than higher
	 * elevations. Specifically, we want elevation X to have frequency
	 * (1-X). To do this we will sort the corners, then set each
	 * corner to its desired elevation. 
	 */
	private void redistributeElevations(List<Vertex> locations) {
		//TODO scale
//		for(Corner c : corners){
//			c.elevation = 10-c.elevation;
//		}
		
		// SCALE_FACTOR increases the mountain area. At 1.0 the maximum
		// elevation barely shows up on the map, so we set it to 1.1.
		double SCALE_FACTOR = 1.1;

		// locations.sortOn('elevation', Array.NUMERIC);//comparator TODO
		Collections.sort(locations, new Comparator<Vertex>() {
			@Override
			public int compare(Vertex o1, Vertex o2) {
				return Double.compare(o1.elevation, o2.elevation);
				// TODO right comparable variable?
			}
		});

		for (int i = 0; i < locations.size(); i++) {
			// Let y(x) be the total area that we want at elevation <= x.
			// We want the higher elevations to occur less than lower
			// ones, and set the area to be y(x) = 1 - (1-x)^2.
			double y = (double) i / (locations.size() - 1);

			// Now we have to solve for x, given the known y.
			// * y = 1 - (1-x)^2
			// * y = 1 - (1 - 2x + x^2)
			// * y = 2x - x^2
			// * x^2 - 2x + y = 0
			// From this we can use the quadratic equation to get:
			double x = Math.sqrt(SCALE_FACTOR)
					- Math.sqrt(SCALE_FACTOR * (1 - y));
			if (x > 1.0)
				x = 1.0; // TODO: does this break downslopes?
			locations.get(i).elevation = x;
		}

		// finally we make the lakes flat and record them
		List<HashSet<Polygon>> lakes = new ArrayList<HashSet<Polygon>>();

		// TODO???
		for (Polygon c : polygons) {
			if (!c.ocean && c.water) { //c is a lake
				boolean isLake = false;// flag that center is already in a lake

				for (HashSet<Polygon> lake : lakes) {
					if (isLake = lake.contains(c)) {
						break;
					}
				}

				//if it isn't already a lake
				if (!isLake) {
					lakes.add(createLake(c));
				}
			}
		}
	}

	/** Helper method to return the lake set of a given polygon */
	private HashSet<Polygon> createLake(Polygon c) {
		// else create a new lake with it, and all its neighbors
		HashSet<Polygon> newLake = new HashSet<Polygon>();
		double lowest = Double.POSITIVE_INFINITY;

		// set up the list of neighbors to be traversed
		List<Polygon> neighbours = new ArrayList<Polygon>();
		neighbours.add(c);

		// traverse all neighbors that are also part of the lake
		while (!neighbours.isEmpty()) {
			Polygon next = neighbours.remove(0);
			newLake.add(next);
			
			for(Vertex k : next.vertices){
				lowest = (k.elevation < lowest) ? k.elevation : lowest;
			}

			for (Polygon neigh : next.neighbors) {
				// if it's neighbour is a lake but is not recorded
				if (!neigh.ocean && neigh.water && 
						!newLake.contains(neigh) && !neighbours.contains(neigh)) {
					neighbours.add(neigh);
				}
			}
		}
		for(Polygon l : newLake){
			for(Vertex r : l.vertices){
				r.elevation = lowest;
			}
		}
		return newLake;
	}

	// Redistribute moisture over entire land mass
	private void redistributeMoisture(List<Vertex> locations) {
		List<Vertex> sortedLocations = new ArrayList<Vertex>(locations);
		Collections.sort(sortedLocations, new Comparator<Vertex>() {
			@Override
			public int compare(Vertex o1, Vertex o2) {
				return (int) ((o1.moisture - o2.moisture) * 1000);
			}
		});

		for (int i = 0; i < sortedLocations.size(); i++) {
			sortedLocations.get(i).moisture = ((double)i) / (sortedLocations.size() - 1);
		}
	}

	// Determine land types
	private void assignOceanCoastAndLand() {
		// Compute polygon attributes 'ocean' and 'water' based on the
		// corner attributes. Count the water corners per
		// polygon. Oceans are all polygons connected to the edge of the
		// map. In the first pass, mark the edges of the map as ocean;
		// in the second pass, mark any water-containing polygon
		// connected an ocean as ocean.
		Queue<Polygon> queue = new ArrayDeque<Polygon>();

		for (Polygon p : polygons) {
			int numWater = 0;
			for (Vertex q : p.vertices) {
				if (q.border) {
					p.border = true;
					p.ocean = true;
					q.water = true;
					queue.add(p);
				}
				if (q.water) {
					numWater += 1;
				}
			}
			p.water = (p.ocean || numWater >= p.vertices.size() * LAKE_THRESHOLD);
		}
		while (!queue.isEmpty()) {
			Polygon p = queue.poll();
			for (Polygon r : p.neighbors) {
				if (r.water && !r.ocean) {
					r.ocean = true;
					queue.add(r);
				}
			}
		}

		// Set the polygon attribute 'coast' based on its neighbors. If
		// it has at least one ocean and at least one land neighbor,
		// then this is a coastal polygon.
		for (Polygon p : polygons) {
			int numOcean = 0;
			int numLand = 0;
			for (Polygon r : p.neighbors) {
				numOcean += (r.ocean) ? 1 : 0;
				numLand += (!r.water) ? 1 : 0;
			}
			p.coast = (numOcean > 0) && (numLand > 0);
		}

		// Set the corner attributes based on the computed polygon
		// attributes. If all polygons connected to this corner are
		// ocean, then it's ocean; if all are land, then it's land;
		// otherwise it's coast.
		for (Vertex q : vertices) {
			int numOcean = 0;
			int numLand = 0;
			for (Polygon p : q.polygons) {
				numOcean += (p.ocean) ? 1 : 0;
				numLand += (!p.water) ? 1 : 0;
			}
			q.ocean = (numOcean == q.polygons.size());
			q.coast = (numOcean > 0) && (numLand > 0);
			q.water = q.border || ((numLand != q.polygons.size()) && !q.coast);
		}
	}
	

	// Polygon elevations are the average of the elevations of their corners.
	private void assignPolygonElevations() {
		for (Polygon p : polygons) {
			double sumElevation = 0.0;
			for (Vertex q : p.vertices) {
				sumElevation += q.elevation;
			}
			p.elevation = sumElevation / p.vertices.size();
		}
	}
	

	// Calculate downslope pointers. At every point, we point to the
	// point downstream from it, or to itself. This is used for
	// generating rivers and watersheds.
	private void calculateDownslopes() {
		for (Vertex q : vertices) {
			Vertex r = q; // default case
			for (Vertex s : q.neighbours) {
				if (s.elevation <= r.elevation) {
					r = s;
				}
			}
			q.downslope = r;
		}
	}
	

	// Calculate per polygon watersheds
	private void calculateWatersheds() {

		// Initially the watershed pointer points downslope one step.
		for (Vertex q : vertices) {
			q.watershed = q;
			if (!q.ocean && !q.coast) {
				q.watershed = q.downslope;
			}
		}
		
		// Follow the downslope pointers to the coast. Limit to 100
		// iterations although most of the time with NUM_POINTS=2000 it
		// only takes 20 iterations because most points are not far from
		// a coast. TODO: can run faster by looking at
		// p.watershed.watershed instead of p.downslope.watershed.
		for (int i = 0; i < 100; i++) {
			boolean changed = false;
			for (Vertex q : vertices) {
				if (!q.ocean && !q.coast && !q.watershed.coast) {
					Vertex r = q.downslope.watershed;
					if (!r.ocean)
						q.watershed = r;
					changed = true;
				}
			}
			if (!changed)
				break;
		}
		
		// How big is each watershed?
		for (Vertex q : vertices) {
			Vertex r = q.watershed;
			r.watershed_size = 1 + Math.max(r.watershed_size, 0);
		}
	}

	
	// Create rivers, starting from a random point in the map
	private void createRivers() {
		for (int i = 0; i < NUM_POINTS / 4; i++) {

			Vertex q = vertices.get((int) nextInRange(0, vertices.size() - 1));
			if (q.ocean || q.elevation < 0.3 || q.elevation > 0.9)
				continue;

			// Bias rivers to go west: if (q.downslope.x > q.x) continue;
			while (!q.coast && !q.water) {
				if (q == q.downslope) {
					break;
				}
				Edge edge = lookupEdgeFromCorner(q, q.downslope);
				edge.river = edge.river + 1;
				q.river = Math.max(q.river, 0) + 1;
				q.downslope.river = Math.max(q.downslope.river, 0) + 1;
				// TODO: fix double count
				q = q.downslope;
			}
		}
	}
	

	// Calculate moisture for Vertices from rivers and lakes (freshwater)
	private void assignVertexMoisture() {
		Queue<Vertex> queue = new ArrayDeque<Vertex>();
		// Fresh water
		for (Vertex q : vertices) {
			if ((q.water || q.river > 0) && !q.ocean) {
				q.moisture = q.river > 0 ? Math.min(3.0, (0.2 * q.river)) : 1.0;
				queue.add(q);
			} else {
				q.moisture = 0.0;
			}
		}
		while (!queue.isEmpty()) {
			Vertex q = queue.poll();

			for (Vertex r : q.neighbours) {
				double newMoisture = q.moisture * 0.9;
				if (newMoisture > r.moisture) {
					r.moisture = newMoisture;
					queue.add(r);
				}
			}
		}
		
		// Salt water
		for (Vertex q : vertices) {
			if (q.ocean || q.coast) {
				q.moisture = 1.0;
			}
		}
	}

	
	// Polygon moisture is the average of the moisture at corners
	private void assignPolygonMoisture() {
		for (Polygon p : polygons) {
			double sumMoisture = 0.0;
			for (Vertex q : p.vertices) {
				// ceiling value for all corner water values
				if (q.moisture > 1.0) {
					q.moisture = 1.0;
				}
				sumMoisture += q.moisture;
			}
			p.moisture = sumMoisture / p.vertices.size();
		}
	}
	

	/** 
	 * Assign a biome type to each polygon. If it has
	 * ocean/coast/water, then that's the biome; otherwise it depends
	 * on low/high elevation and low/medium/high moisture. This is
	 * roughly based on the Whittaker diagram but adapted to fit the
	 * needs of the island map generator.
	 */
	static public Biome getBiome(Polygon p) {
		if (p.ocean) {
			return OCEAN;
		} else if (p.water) {
			if (p.elevation < 0.1)
				return MARSH;
			if (p.elevation > 0.8)
				return ICE;
			return LAKE;
		} else if (p.coast) {// TODO change this to only lower down is beach
			return BEACH;

			// High elevation
		} else if (p.elevation > 0.8) {
			if (p.moisture > 0.50)
				return SNOW;
			else if (p.moisture > 0.33)
				return TUNDRA;
			else if (p.moisture > 0.16)
				return BARE;
			else
				return SCORCHED;

			// Relatively high
		} else if (p.elevation > 0.6) {
			if (p.moisture > 0.66)
				return TAIGA;
			else if (p.moisture > 0.33)
				return SHRUBLAND;
			else
				return TEMPERATE_DESERT;

			// Medium
		} else if (p.elevation > 0.3) {
			if (p.moisture > 0.83)
				return TEMPERATE_RAIN_FOREST;
			else if (p.moisture > 0.50)
				return TEMPERATE_DECIDUOUS_FOREST;
			else if (p.moisture > 0.16)
				return GRASSLAND;
			else
				return TEMPERATE_DESERT;

			// Quite low
		} else {
			if (p.moisture > 0.66)
				return TROPICAL_RAIN_FOREST;
			else if (p.moisture > 0.33)
				return TROPICAL_SEASONAL_FOREST;
			else if (p.moisture > 0.16)
				return GRASSLAND;
			else
				return SUBTROPICAL_DESERT;
		}
	}

	
	// Look up a Voronoi Edge object given two adjacent Voronoi
	// polygons, or two adjacent Voronoi corners
	private Edge lookupEdgeFromCenter(Polygon p, Polygon r) {
		for (Edge edge : p.edges) {
			if (edge.p0 == r || edge.p1 == r)
				return edge;
		}
		return null;
	}

	private Edge lookupEdgeFromCorner(Vertex q, Vertex s) {
		for (Edge edge : q.edges) {
			if (edge.v0 == s || edge.v1 == s)
				return edge;
		}
		return null;
	}

	
	/**
	 * Determines the shape and distribution of elevations of the corners of the
	 * map to create the base shape and structure.
	 */
	private abstract class IslandStrategy {

		/**
		 * Determine whether a given point should be on the island or in the
		 * water.
		 */
		protected abstract boolean inside(Vec2d p);

		/**
		 * Determine elevations and water at Voronoi corners. By construction,
		 * we have no local minima. This is important for the downslope vectors
		 * later, which are used in the river construction algorithm. Also by
		 * construction, inlets/bays push low elevation areas inland, which
		 * means many rivers end up flowing out through them. Also by
		 * construction, lakes often end up on river paths because they don't
		 * raise the elevation as much as other terrain does.
		 */
		public abstract void assignCornerElevations(List<Vertex> points);
	}

	
	/**
	 * An implementation of Island Shape that uses normal distribution to create
	 * a round island in the middle of the map altered by Perlin noise to create
	 * inlets, bays and lakes.
	 */
	public class PerlinIsland extends IslandStrategy {
		private Perlin perlin;

		public PerlinIsland(long seed) {
			perlin = new Perlin(seed);
		}

		@Override
		protected boolean inside(Vec2d p) {
			final double SD_ISLAND = 3.5;
			final double SD_MAP_EDGE = 2.7;

			double x = SD_ISLAND * ((2d * p.x / width) - 1);
			double y = SD_ISLAND * ((2d * p.y / height) - 1);

			double fg = normalDistribution(SD_ISLAND);

			// z = (1/sqrt(2PI)) e^(-(x^2 + y^2) / 2)
			double noiseBarrier = normalDistribution(Math.hypot(x, y))
					- (normalDistribution(Math.hypot(x, y) * 1.4) - fg) * 2
					+ fg;

			double z = normalDistribution(Math.hypot(x, y));

			double noise = perlin.getNoise(p.x / width, p.y / height, 0, 8);

			return z - noise * noiseBarrier * 10 > normalDistribution(SD_MAP_EDGE);

			// n*(1-z)
		}

		/* Helper function to determine z value at given point */
		private double normalDistribution(double sd) {
			return (1 / Math.sqrt(2 * Math.PI)) * Math.pow(Math.E, -(sd * sd) / 2);
		}

		@Override
		public void assignCornerElevations(List<Vertex> points) {

			// build a priority queue with least elevated at the head
			Queue<Vertex> queue = new PriorityQueue<Vertex>(NUM_POINTS / 40,
					new Comparator<Vertex>() {
						@Override
						public int compare(Vertex o1, Vertex o2) {
							return (int) ((o1.elevation - o2.elevation) * 1000);
						}
					});

			for (Vertex q : points) {
				q.water = !inside(q.point);
			}

			for (Vertex q : points) {
				// Set the edges of the map are elevation 0 and add to queue
				if (q.border) {
					q.elevation = -1d;
					queue.add(q);
				} else {
					q.elevation = Double.POSITIVE_INFINITY;
				}
			}

			Perlin noise = new Perlin(seed);

			// Traverse the graph and assign elevations to each point. As we
			// move away from the map border, increase the elevations. This
			// guarantees that rivers always have a way down to the coast by
			// going downhill (no local minima).
			while (!queue.isEmpty()) {
				Vertex q = queue.poll();

				for (Vertex s : q.neighbours) {
					// Every step up is epsilon over water or 1 over land. The
					// number doesn't matter because we'll rescale the
					// elevations later.
					double newElevation = 0.01 + q.elevation;
					if (!q.water && !s.water) {
						double perlin = noise.getNoise(s.point.x / width,
								s.point.y / height, 0, 1);
						newElevation += ((perlin > 0) ? perlin * 100 : 0.5);
					}

					// If this point changed, we'll add it to the queue so
					// that we can process its neighbors too.
					if (newElevation < s.elevation) {
						s.elevation = newElevation;
						queue.add(s);
					}
				}
			}
		}

	}
	
	
	
	/**
	 * An implementation of Island Shape that uses normal distribution to create
	 * a round island in the middle of the map altered by Perlin noise to create
	 * inlets, bays and lakes.
	 */
	public class PerlinSanctuary extends IslandStrategy {
		private Perlin perlin;

		private static final double SD_ISLAND = 3.5;
		private static final double SD_MAP_EDGE = 2.7;
		
		public PerlinSanctuary(long seed) {
			perlin = new Perlin(seed);
		}

		@Override
		protected boolean inside(Vec2d p) {

			double x = SD_ISLAND * ((2d * p.x / width) - 1);
			double y = SD_ISLAND * ((2d * p.y / height) - 1);

			double fg = normalDistribution(SD_ISLAND);

			// z = (1/sqrt(2PI)) e^(-(x^2 + y^2) / 2)
			double noiseBarrier = normalDistribution(Math.hypot(x, y))
					- (normalDistribution(Math.hypot(x, y) * 1.4) - fg) * 2
					+ fg;

			double z = normalDistribution(Math.hypot(x, y));

			double noise = perlin.getNoise(p.x / width, p.y / height, 0, 8);

			return z - noise * noiseBarrier * 10 > normalDistribution(SD_MAP_EDGE);

			// n*(1-z)
		}

		/* Helper function to determine z value at given point */
		private double normalDistribution(double sd) {
			return (1 / Math.sqrt(2 * Math.PI))
					* Math.pow(Math.E, -(sd * sd) / 2);
		}

		@Override
		public void assignCornerElevations(List<Vertex> points) {

			//assign land and water
			for (Vertex q : points) {
				q.water = !inside(q.point);
			}

			Perlin noise = new Perlin(seed);


			for (Vertex s : vertices) {
					
				double x = SD_ISLAND * ((2d * s.point.x / width) - 1);
				double y = SD_ISLAND * ((2d * s.point.y / height) - 1);

				double fg = normalDistribution(SD_ISLAND);

//				double noiseBarrier = normalDistribution(Math.hypot(x, y))
//						- (normalDistribution(Math.hypot(x, y) * 1.4) - fg) * 2;
				
				double noiseBarrier = normalDistribution(Math.hypot(x, y)/3)*2
						- (normalDistribution(Math.hypot(x, y)));
				
				double perlin = noise.getNoise(s.point.x / width, s.point.y / height, 0, 8);
				double val = noiseBarrier + perlin*noiseBarrier;
				
				s.elevation = ((val > 0) ? val: 0.001);
			}
			
			// build list of border polygons so we can set
			// the elevation of the area outside the island 
			List<Vertex> ocean = new ArrayList<Vertex>();
			
			for (Vertex q : points) {
				// Set the edges of the map are elevation 0 and add to queue
				if (q.border) {
					q.elevation = -1d;
					ocean.add(q);
				}
			}
			
			while(!ocean.isEmpty()){
				Vertex c = ocean.remove(0);
				for(Vertex k : c.neighbours){
					if(k.water && k.elevation<0){
						ocean.add(k);
						k.elevation = c.elevation + 0.00001;
					}
				}
			}
		}

	}


	public static void main(String[] args) {
		MapGenerator mp = new MapGenerator(42, 800, 800);
		mp.run();
		
		for (Triangle t : mp.getTriangles()) {
			double xp[] = new double[3];
			double yp[] = new double[3];
			double height = 0;
			for (int i=0; i<3; ++i) {
				xp[i] = t.ver[i].x;
				yp[i] = t.ver[i].z;
				height+= t.ver[i].y;
			}
			height /=3;
			int rbg = (int)(height * 255);
			UI.setColor(new Color(rbg, rbg, rbg));
//			UI.setColor(Color.black);
			UI.fillPolygon(xp, yp, 3);
		}
		
//		for (Edge e : mp.edges) {
//			UI.setLineWidth(2);
//			UI.setColor(Color.red);
//			UI.drawLine(e.v0.point.x, e.v0.point.y, e.v1.point.x, e.v1.point.y);
//		}
		
		for (Vertex v : mp.vertices) {
			if (v.edges.size() <= 2) {

				UI.setColor(Color.red);
				UI.fillRect(v.point.x-1, v.point.y-1, 3, 3);
			}
		}

	}
}
