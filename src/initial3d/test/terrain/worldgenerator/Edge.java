package initial3d.test.terrain.worldgenerator;

import initial3d.Vec2d;

public class Edge {
	public final int index;
	public Vec2d midpoint;  // halfway between v0,v1 //TODO make final?
	
	public Polygon p0, p1;  // Delaunay edge
	public Vertex v0, v1;  // Voronoi edge
	public int river = 0;  // volume of water, or 0
	
	public Edge(int idx, Vec2d mp) {
		index = idx;
		midpoint = mp;
	}
}
