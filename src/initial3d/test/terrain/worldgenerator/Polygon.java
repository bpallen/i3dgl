package initial3d.test.terrain.worldgenerator;
import java.util.ArrayList;
import java.util.List;

import initial3d.Vec2d;

public class Polygon {
	
    public final int index;
    public final Vec2d point;  // location
    
    public boolean water;  // lake or ocean
    public boolean ocean;  // ocean
    public boolean coast;  // land polygon touching an ocean
    public boolean border;  // at the edge of the map
    public Biome biome;  // biome type (see article)
    public double elevation;  // 0.0-1.0
    public double moisture;  // 0.0-1.0

   
    public List<Polygon> neighbors = new ArrayList<>();
    public List<Edge> edges = new ArrayList<>();
    public List<Vertex> vertices = new ArrayList<>();
    
    public Polygon(int idx, Vec2d p) {
    	index = idx;
    	point = p;
    }
}
