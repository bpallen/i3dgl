package initial3d.test.terrain.worldgenerator;
import java.util.ArrayList;
import java.util.List;

import initial3d.Vec2d;

public class Vertex {
    public int index;
  
    public Vec2d point;  // location
    public boolean ocean;  // ocean
    public boolean water;  // lake or ocean
    public boolean coast;  // touches ocean and land polygons
    public boolean border;  // at the edge of the map
    public double elevation;  // 0.0-1.0
    public double moisture;  // 0.0-1.0

    public List<Polygon> polygons = new ArrayList<>();
    public List<Edge> edges = new ArrayList<>();
    public List<Vertex> neighbours = new ArrayList<>();
  
    public int river;  // 0 if no river, or volume of water in river
    public Vertex downslope;  // pointer to adjacent corner most down hill
    public Vertex watershed;  // pointer to coastal corner, or null
    public int watershed_size;
    
    public Vertex(int idx, Vec2d p) {
    	index = idx;
    	point = p;
    }
}
