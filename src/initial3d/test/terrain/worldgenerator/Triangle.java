package initial3d.test.terrain.worldgenerator;

import initial3d.Vec2d;
import initial3d.Vec3d;

import static initial3d.Functions.*;

public class Triangle {
	
	public final Vec3d[] ver = new Vec3d[3];
	public final Vec3d color;
	public final Vec3d normal;
	
	public Triangle(Vec2d[] points, double[] heights, Vec3d col){
		if(points.length!=3 || heights.length!=3){
			throw new IllegalArgumentException();
		}
		
		color = col;
		
		for(int i = 0; i<3; i++){
			ver[i] = vec3d(points[i].x, heights[i], points[i].y);

		}
		
		Vec3d ab = ver[1].sub(ver[0]);
		Vec3d ac = ver[2].sub(ver[0]);
		Vec3d n = normalize(cross(ab, ac));
		
		if (n.y < 0) {
			Vec3d temp = ver[0];
			ver[0] = ver[1];
			ver[1] = temp; 
			n = vec3d(n.x, -n.y, n.z);
		}
		
		normal = n;
	}
	
	/**
	 * A method that returns true if the given point in inside or on the 
	 * triangle using some halfspace function gibberish.
	 */
	public boolean contains(Vec2d p){
		return ((ver[0].x - ver[1].x) * (p.y - ver[0].z) - (ver[0].z - ver[1].z) * (p.x - ver[0].x) >= 0 &&
	            (ver[1].x - ver[2].x) * (p.y - ver[1].z) - (ver[1].z - ver[2].z) * (p.x - ver[1].x) >= 0 &&
	            (ver[2].x - ver[0].x) * (p.y - ver[2].z) - (ver[2].z - ver[0].z) * (p.x - ver[2].x) >= 0);
	}
	
	/**
	 * A method that returns true if the given positions are in inside or on the 
	 * triangle using some halfspace function gibberish.
	 */
	public boolean contains(double x, double z){
		return ((ver[0].x - ver[1].x) * (z - ver[0].z) - (ver[0].z - ver[1].z) * (x - ver[0].x) >= 0 &&
	            (ver[1].x - ver[2].x) * (z - ver[1].z) - (ver[1].z - ver[2].z) * (x - ver[1].x) >= 0 &&
	            (ver[2].x - ver[0].x) * (z - ver[2].z) - (ver[2].z - ver[0].z) * (x - ver[2].x) >= 0);
	}

	/**
	 * Returns the interpolated value for the given point (assuming it's
	 * inside the triangle) using barycentric gibberish.
	 */
	public double height(Vec2d p){
		double dT = (ver[1].z - ver[2].z) * (ver[0].x - ver[2].x) + (ver[2].x - ver[1].x) * (ver[0].z - ver[2].z);
		
		double a1 = ((ver[1].z - ver[2].z) * (p.x - ver[2].x) + (ver[2].x - ver[1].x) * (p.y - ver[2].z)) / dT;
		double a2 = ((ver[2].z - ver[0].z) * (p.x - ver[2].x) + (ver[0].x - ver[2].x) * (p.y - ver[2].z)) / dT;
		double a3 = 1-a1-a2;
		
		return a1 * ver[0].y + a2 * ver[1].y + a3 * ver[2].y ;
	}
	
	/**
	 * Returns the interpolated value for the given positions (assuming it's
	 * inside the triangle) using barycentric gibberish.
	 */
	public double height(double x, double z){
		double dT = (ver[1].z - ver[2].z) * (ver[0].x - ver[2].x) + (ver[2].x - ver[1].x) * (ver[0].z - ver[2].z);
		
		double a1 = ((ver[1].z - ver[2].z) * (x - ver[2].x) + (ver[2].x - ver[1].x) * (z - ver[2].z)) / dT;
		double a2 = ((ver[2].z - ver[0].z) * (x - ver[2].x) + (ver[0].x - ver[2].x) * (z - ver[2].z)) / dT;
		double a3 = 1-a1-a2;
		
		return a1 * ver[0].y + a2 * ver[1].y + a3 * ver[2].y ;
	}
	
	public double getMinX(){
		return Math.min(Math.min(ver[0].x, ver[1].x), ver[2].x);
	}
	
	public double getMinZ(){
		return Math.min(Math.min(ver[0].z, ver[1].z), ver[2].z);
	}
	
	public double getMaxX(){
		return Math.max(Math.max(ver[0].x, ver[1].x), ver[2].x);
	}

	public double getMaxZ(){
		return Math.max(Math.max(ver[0].z, ver[1].z), ver[2].z);
	}
	
	public String toString(){
		String s = "Triangle :: ";
		for(Vec3d v : ver){
			s += v.toString();
		}
		return s;
	}
	
}
















