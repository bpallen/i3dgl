package initial3d.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.CharBuffer;
import java.nio.IntBuffer;

import initial3d.GL;
import initial3d.GLEnum;
import initial3d.GLEnumValues;

import static initial3d.GLEnum.*;

public class ShaderLoader {

	public static void printShaderInfoLog(GL gl, int shader) {
		IntBuffer loglen = IntBuffer.allocate(1);
		gl.glGetShader(shader, GL_INFO_LOG_LENGTH, loglen);
		if (loglen.get(0) > 1) {
			CharBuffer logtext = CharBuffer.allocate(loglen.get(0));
			gl.glGetShaderInfoLog(shader, logtext);
			logtext.rewind();
			System.out.println("SHADER:\n" + logtext.toString());
		}
	}
	
	public static void printProgramInfoLog(GL gl, int program) {
		IntBuffer loglen = IntBuffer.allocate(1);
		gl.glGetProgram(program, GL_INFO_LOG_LENGTH, loglen);
		if (loglen.get(0) > 1) {
			CharBuffer logtext = CharBuffer.allocate(loglen.get(0));
			gl.glGetProgramInfoLog(program, logtext);
			logtext.rewind();
			System.out.println("PROGRAM:\n" + logtext.toString());
		}
	}
	
	public static int compileShader(GL gl, GLEnum stype, String text) {
		int shader = gl.glCreateShader(stype);
		gl.glShaderSource(shader, text);
		gl.glCompileShader(shader);
		IntBuffer status = IntBuffer.allocate(1);
		gl.glGetShader(shader, GL_COMPILE_STATUS, status);
		// always print for warnings
		printShaderInfoLog(gl, shader);
		if (status.get(0) == 0) throw new ShaderException("shader compile failed");
		return shader;
	}
	
	public static void linkShaderProgram(GL gl, int program) {
		gl.glLinkProgram(program);
		IntBuffer status = IntBuffer.allocate(1);
		gl.glGetProgram(program, GL_LINK_STATUS, status);
		// always print for warnings
		printProgramInfoLog(gl, program);
		if (status.get(0) == 0) throw new ShaderException("program link failed");
	}
	
	public static String shaderStageMacro(GLEnum stype) {
		switch (stype.valueInt()) {
		case GLEnumValues.GL_VERTEX_SHADER_value:
			return "_VERTEX_";
		case GLEnumValues.GL_GEOMETRY_SHADER_value:
			return "_GEOMETRY_";
		case GLEnumValues.GL_FRAGMENT_SHADER_value:
			return "_FRAGMENT_";
		default:
			throw new IllegalArgumentException("bad shader type");
		}
	}
	
	public static int makeShaderProgram(GL gl, String profile, GLEnum[] stypes, String header, String source) {
		int program = gl.glCreateProgram();
		for (GLEnum stype : stypes) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			pw.println("#version " + profile);
			pw.println("#define " + shaderStageMacro(stype));
			pw.println(header);
			pw.println("#line 1");
			pw.println(source);
			pw.flush();
			int shader = compileShader(gl, stype, sw.toString());
			gl.glAttachShader(program, shader);
		}
		linkShaderProgram(gl, program);
		System.out.println("shader program compiled and linked successfully");
		return program;
	}
	
	public static int makeShaderProgramFromFiles(GL gl, String profile, GLEnum[] stypes, String header, String[] filenames) throws IOException {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		for (int i = 0; i < filenames.length; i++) {
			pw.println("#line 1 " + i);
			try (BufferedReader r = new BufferedReader(new FileReader(filenames[i]))) {
				r.lines().forEach((String line) -> pw.println(line));
			}
		}
		pw.flush();
		String text = sw.toString();
		if (text.isEmpty()) throw new ShaderException("read empty file");
		return makeShaderProgram(gl, profile, stypes, header, text);
	}
	
}
