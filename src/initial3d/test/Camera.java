package initial3d.test;

import initial3d.Mat4d;
import initial3d.Quatd;
import initial3d.Vec3d;

import static initial3d.Functions.*;

public interface Camera {

	public void update();
	
	public Quatd rotation();
	
	public Vec3d position();
	
	default Mat4d viewTransform() {
		return rotate3d(inverse(rotation())).mul(translate3d(position().neg()));
	}
	
}
