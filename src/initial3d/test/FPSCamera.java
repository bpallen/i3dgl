package initial3d.test;

import initial3d.Quatd;
import initial3d.Vec2d;
import initial3d.Vec2i;
import initial3d.Vec3d;

import static initial3d.Functions.*;

import java.awt.event.KeyEvent;

public class FPSCamera implements Camera {

	private Vec3d pos;
	private Quatd rot_base;
	private double rot_v = 0;
	private double speed = 2;
	private long time_last;
	
	public FPSCamera(Vec3d pos_, double rot_h_, double rot_v_) {
		pos = pos_;
		rot_base = axisangled(Vec3d.j, rot_h_);
		rot_v = rot_v_;
	}
	
	@Override
	public void update() {
		
		// time since last update
		long time_now = System.nanoTime();
		double dt = (time_now - time_last) / 1000000000.0;
		time_last = time_now;
		
		// pixels per 2*pi
		final double rot_speed = 600;
		
		Vec3d up = Vec3d.j; // up is just world up
		Vec3d forward = normalize(reject(rot_base.mul(Vec3d.k.neg()), up));
		Vec3d side = normalize(cross(forward, up));
		
		if (Input.isMouseCaptured()) {
			Vec2i xy = Input.mouseTravel();
			double rot_h = -xy.x / rot_speed;
			rot_base = axisangled(up, rot_h).mul(rot_base);
			rot_v += -xy.y / rot_speed;
			rot_v = clamp(rot_v, -0.499 * Math.PI, 0.499 * Math.PI);
		}
		
		Vec3d move = Vec3d.zero;
		
		if (Input.keyDown(Input.K_W)) move = move.add(forward);
		if (Input.keyDown(Input.K_S)) move = move.sub(forward);
		if (Input.keyDown(Input.K_A)) move = move.sub(side);
		if (Input.keyDown(Input.K_D)) move = move.add(side);
		if (Input.keyDown(Input.K_SHIFT)) move = move.sub(up);
		if (Input.keyDown(Input.K_SPACE)) move = move.add(up);
		
		if (Input.keyPressed(Input.K_OPEN_BRACKET)) speed *= 0.5;
		if (Input.keyPressed(Input.K_CLOSE_BRACKET)) speed *= 2;
		
		if (Input.keyPressed(KeyEvent.VK_BACK_QUOTE)) {
			Input.setMouseCapture(!Input.isMouseCaptured());
		}
		
		if (norm(move) > 0.1) {
			Vec3d dpos = normalize(move).mul(speed).mul(dt);
			pos = pos.add(dpos);
		}
	}
	
	@Override
	public Quatd rotation() {
		return rot_base.mul(axisangled(Vec3d.i, rot_v));
	}

	@Override
	public Vec3d position() {
		return pos;
	}

	private static Vec3d project(Vec3d v, Vec3d axis) {
		return axis.mul(dot(v, axis) / dot(v, v));
	}
	
	private static Vec3d reject(Vec3d v, Vec3d axis) {
		return v.sub(project(v, axis));
	}
}












