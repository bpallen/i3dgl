package initial3d.test;

import initial3d.GL;
import initial3d.GLBitfield;
import initial3d.GLEnum;
import initial3d.GLUtil;
import initial3d.Mat4d;
import initial3d.Mat4f;
import initial3d.Quatd;
import initial3d.Vec2i;
import initial3d.Vec3d;
import initial3d.detail.Access;
import initial3d.detail.AccessFuture;
import initial3d.detail.AccessQueue;
import initial3d.detail.StringUtil;
import initial3d.detail.Util;
import initial3d.detail.bytecode.Flag;
import initial3d.detail.bytecode.ClassBuilder;
import initial3d.detail.bytecode.Constant;
import initial3d.detail.bytecode.MethodBuilder;
import initial3d.detail.bytecode.Op;
import initial3d.detail.gl.DataStore;
import initial3d.detail.gl.OldIdentityShaderProgram;
import initial3d.detail.gl.Rasterizer;
import initial3d.detail.glsl.InfoLog;
import initial3d.detail.glsl.SysInfoLog;
import initial3d.detail.glsl.asmjava.JavaProgramBuilder;
import initial3d.detail.glsl.intermediate.Environment;
import initial3d.detail.glsl.intermediate.Translator;
import initial3d.detail.glsl.intermediate.AbstractEnvironment;
import initial3d.detail.glsl.lang.BuiltinScope;
import initial3d.detail.glsl.lang.Node;
import initial3d.detail.glsl.lang.Parser;
import initial3d.detail.glsl.lang.Scope;
import initial3d.detail.glsl.preprocessor.Preprocessor;
import initial3d.detail.glsl.tokens.Token;
import initial3d.detail.glsl.tokens.TokenInput;
import initial3d.detail.glsl.tokens.Tokenizer;
import initial3d.test.terrain.worldgenerator.MapGenerator;
import initial3d.test.terrain.worldgenerator.Triangle;
import sun.misc.Unsafe;

import static initial3d.Functions.*;
import static initial3d.GLEnum.*;
import static initial3d.GLEnumValues.*;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JComponent;

import ecs100.UI;

public class Main {

	private static final Unsafe unsafe = Util.unsafe;
	
	public static Class<?> makeAClass() throws Exception {
		ClassBuilder cb = new ClassBuilder();
		
		cb.declareNameUnique("HelloWorld");
		cb.declareFlag(Flag.PUBLIC);
		cb.declareInterface(Runnable.class);
		
		cb.addConstant(Constant.forLong(9001L));
		
		// test substitutions
		cb.addConstantUTF("$${{thisclass}}");
		cb.addConstantUTF("THISCLASS:${${T}${C}}");
		cb.addUTFSubstitution("T", "this");
		cb.addUTFSubstitution("C", "class");
		
		// define ctor
		MethodBuilder mb_init = cb.declareMethod();
		mb_init.declareName("<init>");
		mb_init.declareType(false, void.class);
		mb_init.declareFlag(Flag.PUBLIC);
		mb_init.aload(0);
		mb_init.invokeSpecial(Object.class, "<init>", void.class);
		mb_init.op(Op.RETURN);

		// define run()
		MethodBuilder mb_run = cb.declareMethod();
		mb_run.declareName("run");
		mb_run.declareType(false, void.class);
		mb_run.declareFlag(Flag.PUBLIC);
		mb_run.constant(Constant.forInteger((int) Math.random() * 100));
		mb_run.istore(1);
		mb_run.getStatic(System.class, "out").label("loop");
		mb_run.constant("Hello World!");
		mb_run.invokeVirtual(System.out.getClass(), "println", void.class, String.class);
		mb_run.iload(1);
		mb_run.constant(1);
		mb_run.op(Op.ISUB);
		mb_run.op(Op.DUP);
		mb_run.istore(1);
		mb_run.branch(Op.IFNE, "loop");
		mb_run.op(Op.RETURN);

		// go!
		Class<?> cls = cb.build();
		return cls;
	}
	
	public static void testClassBuilder() throws Exception {
		
		for (int i = 0; i < 10000; i++) {
			Class<?> cls = makeAClass();
			cls.newInstance();
		}
		
		long time0 = System.nanoTime();
		
		for (int i = 0; i < 10000; i++) {
			Class<?> cls = makeAClass();
			cls.newInstance();
		}
		
		long time1 = System.nanoTime();
		
		System.out.printf("class build time: %.3fms\n", (time1 - time0) / 1e6 / 10000.0);
		
		//r.run();
	}

	private static void testPreprocessor() throws Exception {
		InfoLog log = new SysInfoLog();
		Tokenizer intok = new Tokenizer(log, "pptest.glsl", new FileReader("pptest.glsl"));
		Preprocessor preproc = new Preprocessor(log);
		preproc.addInput(intok);
		
		preproc.addInput(new Tokenizer(log, "", new StringReader("int lineX = __LINE__, fileX = __FILE__;\n")));
		
		StringWriter out = new StringWriter();
		PrintWriter pout = new PrintWriter(out);
		
		int indent = 0;
		for (Token t = preproc.nextToken(); t.type() != Token.Type.EOF; t = preproc.nextToken()) {
			if (t.type() != Token.Type.NEWLINE) {
				pout.print(t.text() + " ");
			} else {
				if (!preproc.hasNextToken(Token.Type.NEWLINE)) {
					pout.println();
					if (preproc.hasNextToken(Token.Type.CLOSE_BRACE)) indent--;
					for (int i = 0; i < indent; i++) {
						pout.print("\t");
					}
				}
			}
			if (t.type() == Token.Type.OPEN_BRACE) indent++;
		}
		
		pout.flush();
		
		System.out.println(out.toString());
	}
	
	private static Scope compileShader(String fname) throws IOException {
		InfoLog infolog = new SysInfoLog();
		TokenInput in = new Tokenizer(infolog, fname, new FileReader(fname));
		Preprocessor pp = new Preprocessor(infolog);
		pp.addInput(in);
		Parser p = new Parser(pp);
		p.parseAll();
		return p.globalScope();
	}
	
	private static void testCompiler() throws Exception {
		InfoLog log = new SysInfoLog();
		
		Scope vert = compileShader("testvert.glsl");
		Scope frag = compileShader("testfrag.glsl");
		
		JavaProgramBuilder pb = new JavaProgramBuilder(log);
		pb.addShader(GL_VERTEX_SHADER, vert);
		pb.addShader(GL_FRAGMENT_SHADER, frag);
		pb.build();
		
		pb.programClass().newInstance();
		
	}
	
	private static ByteBuffer cube = GLUtil.buffer(
		-1.0f,-1.0f,-1.0f, // triangle 1 : begin
	    -1.0f,-1.0f, 1.0f,
	    -1.0f, 1.0f, 1.0f, // triangle 1 : end
	    1.0f, 1.0f,-1.0f, // triangle 2 : begin
	    -1.0f,-1.0f,-1.0f,
	    -1.0f, 1.0f,-1.0f, // triangle 2 : end
	    1.0f,-1.0f, 1.0f,
	    -1.0f,-1.0f,-1.0f,
	    1.0f,-1.0f,-1.0f,
	    1.0f, 1.0f,-1.0f,
	    1.0f,-1.0f,-1.0f,
	    -1.0f,-1.0f,-1.0f,
	    -1.0f,-1.0f,-1.0f,
	    -1.0f, 1.0f, 1.0f,
	    -1.0f, 1.0f,-1.0f,
	    1.0f,-1.0f, 1.0f,
	    -1.0f,-1.0f, 1.0f,
	    -1.0f,-1.0f,-1.0f,
	    -1.0f, 1.0f, 1.0f,
	    -1.0f,-1.0f, 1.0f,
	    1.0f,-1.0f, 1.0f,
	    1.0f, 1.0f, 1.0f,
	    1.0f,-1.0f,-1.0f,
	    1.0f, 1.0f,-1.0f,
	    1.0f,-1.0f,-1.0f,
	    1.0f, 1.0f, 1.0f,
	    1.0f,-1.0f, 1.0f,
	    1.0f, 1.0f, 1.0f,
	    1.0f, 1.0f,-1.0f,
	    -1.0f, 1.0f,-1.0f,
	    1.0f, 1.0f, 1.0f,
	    -1.0f, 1.0f,-1.0f,
	    -1.0f, 1.0f, 1.0f,
	    1.0f, 1.0f, 1.0f,
	    -1.0f, 1.0f, 1.0f,
	    1.0f,-1.0f, 1.0f
	);
	
	public static void programClass(GL gl, int prog, Class<?> cls) {
		ByteBuffer b = GLUtil.buffer(cls.getCanonicalName());
		gl.glProgramBinary(prog, GLUtil.GL_PROGRAM_CLASSNAME_I3D_value, b, b.remaining());
	}
	
	public static Mesh terrainMesh() {
		// Create some AWESOME terrain
		MapGenerator mg = new MapGenerator(9001, 400, 400);
		mg.run();
		MeshBuilder terrainMeshBuilder = new MeshBuilder();
		int count = 0;
		
		List<Triangle> tris = mg.getTriangles();
		Collections.sort(tris, new Comparator<Triangle>() {
			@Override
			public int compare(Triangle arg0, Triangle arg1) {
				
				Vec3d p0 = arg0.ver[0];
				Vec3d p1 = arg1.ver[0];
				
				if (Math.abs(p0.y - p1.y) > 2) {
					return Double.compare(p0.y, p1.y);
				} else {
					return Double.compare(p0.x, p1.x);
				}
				
			}
		});
		
		for (Triangle t : tris) {
			terrainMeshBuilder.setColor(vec3f(t.color));
			terrainMeshBuilder.setNormal(vec3f(t.normal));
			terrainMeshBuilder.addVertex(vec3f(t.ver[0]).mul(vec3f(10, 100, 10)));
			terrainMeshBuilder.addVertex(vec3f(t.ver[1]).mul(vec3f(10, 100, 10)));
			terrainMeshBuilder.addVertex(vec3f(t.ver[2]).mul(vec3f(10, 100, 10)));
			++count;
		}
		System.out.println("terrain tri count " + count);
		return terrainMeshBuilder.mesh();
	}
	
	private static void testGL() throws Exception {

		//ByteBuffer verts = GLUtil.buffer(-0.5f, -0.5f, 0.5f, -0.5f, 0f, 1f, 1f, 0f, 1f, 0.5f);
		//ByteBuffer verts = GLUtil.buffer(0f, 0f, -2f, 0f, 1.2f, 0f, -1.2f, 0f, 0f, 0f, -1.2f, 0f, 1.2f, 0f, 0f);
		
		UI.initialise();
		UI.setWindowSize(1280, 720);
		UI.setImmediateRepaint(false);
		
		// wait what, this is public?
		JComponent ui_canvas = UI.theUI.canvas;
		
		Input.initialize(ui_canvas);
		
		final float pos[] = new float[] {0, 0};
		
		UI.setKeyListener((String key) -> {
			if ("Left".equals(key)) pos[0] -= 0.1f;
			if ("Right".equals(key)) pos[0] += 0.1f;
			if ("Down".equals(key)) pos[1] -= 0.1f;
			if ("Up".equals(key)) pos[1] += 0.1f;
		});
		
		GLUtil.initWorkers(1);
		
		final Vec2i fbsize = vec2i(807, 607);
		GL gl = GLUtil.makeI3DGL(fbsize.x, fbsize.y, GL_DEPTH_COMPONENT32F);
		
		int progtest = ShaderLoader.makeShaderProgramFromFiles(
			gl, "330 core", GLUtil.array(GL_VERTEX_SHADER, GL_FRAGMENT_SHADER), "", GLUtil.array("testprog.glsl")
		);
		
		int progterrain = ShaderLoader.makeShaderProgramFromFiles(
			gl, "330 core", GLUtil.array(GL_VERTEX_SHADER, GL_FRAGMENT_SHADER), "", GLUtil.array("terrainprog.glsl")
		);
		
		int vao = gl.glGenVertexArray();
		gl.glBindVertexArray(vao);
		int vbo = gl.glGenBuffer();
		gl.glBindBuffer(GL_ARRAY_BUFFER, vbo);
		gl.glBufferData(GL_ARRAY_BUFFER, cube.remaining(), cube, GL_STATIC_DRAW);
		gl.glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
		gl.glEnableVertexAttribArray(0);
		
		Mesh mesh = WaveFrontLoader.Load("models/bunny.obj");
		mesh.buildVAO(gl);
		
		Mesh terrain = terrainMesh();
		terrain.buildVAO(gl);
		
		long time_last_fps = System.currentTimeMillis();
		int fps = 0;
		
		Camera cam = new FPSCamera(vec3d(0, 100, 0), 0, 0);
		
		boolean done = false;
		boolean lines = false;
		
		while (!done) {
			
			long now = System.currentTimeMillis();
			if (now - time_last_fps > 1000) {
				time_last_fps = now;
				UI.println(fps + " fps; " + (1000.f / fps) + "ms");
				fps = 0;
				
				//long tmem = Runtime.getRuntime().totalMemory();
				//long fmem = Runtime.getRuntime().freeMemory();
				//UI.printf("vm heap: %d/%d (%f%%)\n", (tmem - fmem), tmem, 100 * (tmem - fmem) / (double) tmem);
				
			}
			fps++;
			
			Input.poll();
			done = Input.keyPressed(Input.K_ESCAPE);
			if (Input.keyPressed(Input.K_BACK_SLASH)) {
				lines = !lines;
			}
			
			cam.update();
			
			gl.glViewport(3, 3, fbsize.x - 6, fbsize.y - 6);
			gl.glClearColor(0.4f, 0.6f, 0.95f, 1);
			gl.glClearDepth(1.f);
			gl.glClear(new GLBitfield(GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT));
			
			Mat4d proj = perspectiveProjectiond(1, fbsize.x / (double) fbsize.y, 10, 100000);
			Mat4d view = cam.viewTransform();
			
			gl.glCullFace(GL_BACK);
			gl.glEnable(GL_CULL_FACE);
			gl.glPolygonMode(GL_FRONT_AND_BACK, lines ? GL_LINE : GL_FILL);
//			gl.glPolygonMode(GL_FRONT, GL_FILL);
//			gl.glPolygonMode(GL_BACK, GL_LINE);
			gl.glLineWidth(2);
			
			gl.glEnable(GL_DEPTH_TEST);
			gl.glDepthFunc(GL_LESS);
			
			gl.glUseProgram(progterrain);
			gl.glUniformMatrix4f(gl.glGetUniformLocation(progtest, "u_modelview"), false,
				mat4f(view.mul(translate3d(-2000, 0, -2000))));
			gl.glUniformMatrix4f(gl.glGetUniformLocation(progtest, "u_projection"), false, mat4f(proj));
			gl.glBindVertexArray(terrain.vao());
			gl.glDrawArrays(GL_TRIANGLES, 0, terrain.vertexCount);
			
			gl.glEnable(GL_BLEND);
			gl.glBlendColor(0, 0, 0, 0.5f);
			gl.glBlendEquation(GL_FUNC_ADD);
			gl.glBlendFunc(GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);
			
			gl.glUseProgram(progtest);
			gl.glUniformMatrix4f(gl.glGetUniformLocation(progtest, "u_modelview"), false,
				mat4f(view.mul(translate3d(0, 100, -5)).mul(rotateX3d(0)).mul(scale3d(100))));
			gl.glUniformMatrix4f(gl.glGetUniformLocation(progtest, "u_projection"), false, mat4f(proj));
			gl.glBindVertexArray(mesh.vao());
			gl.glDrawArrays(GL_TRIANGLES, 0, mesh.vertexCount);
//			gl.glBindVertexArray(vao);
//			gl.glDrawArrays(GL_TRIANGLES, 0, 36);
			
			gl.glDisable(GL_BLEND);
			
			gl.glDisable(GL_DEPTH_TEST);
			
			// UI.getGraphics() draws to a temp image.
			// much faster to go straight to the canvas.
			Graphics2D g = (Graphics2D) ui_canvas.getGraphics();
			
			GLUtil.blitFramebuffer(gl, g, 0, 0, fbsize.x, fbsize.y);
			
		}
		
		GLUtil.shutdown(gl);
		UI.quit();
		
	}
	
	public static void main(String[] args) throws Exception {
		//testCompiler();
		testGL();
	}

}












