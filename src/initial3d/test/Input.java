package initial3d.test;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

import initial3d.Vec2i;


import static initial3d.Functions.*;
import static java.awt.event.KeyEvent.*;
import static java.awt.event.MouseEvent.*;

import java.awt.AWTException;

@SuppressWarnings("serial")
public final class Input {

	// TODO Left and Right
	public static final int K_SHIFT = VK_SHIFT;
	public static final int K_CONTROL = VK_CONTROL;
	public static final int K_ALT = VK_ALT;
	public static final int K_WINDOWS = VK_WINDOWS;
	public static final int K_CONTEXT_MENU = VK_CONTEXT_MENU;

	// Simple
	public static final int K_ENTER = VK_ENTER;
	public static final int K_BACK_SPACE = VK_BACK_SPACE;
	public static final int K_SPACE = VK_SPACE;
	public static final int K_ESCAPE = VK_ESCAPE;
	public static final int K_TAB = VK_TAB;
	public static final int K_CAPS_LOCK = VK_CAPS_LOCK;

	// Navagation
	public static final int K_INSERT = VK_INSERT;
	public static final int K_DELETE = VK_DELETE;
	public static final int K_HOME = VK_HOME;
	public static final int K_END = VK_END;
	public static final int K_PAGE_UP = VK_PAGE_UP;
	public static final int K_PAGE_DOWN = VK_PAGE_DOWN;

	// Directions
	public static final int K_LEFT = VK_LEFT;
	public static final int K_UP = VK_UP;
	public static final int K_RIGHT = VK_RIGHT;
	public static final int K_DOWN = VK_DOWN;

	// Non alphanumeric
	public static final int K_MINUS = VK_MINUS;
	public static final int K_UNDERSCORE = VK_UNDERSCORE;
	public static final int K_PLUS = VK_PLUS;
	public static final int K_EQUALS = VK_EQUALS;
	public static final int K_QUOTE = VK_QUOTE;
	public static final int K_BACK_QUOTE = VK_BACK_QUOTE;
	public static final int K_DOUBLE_QUOTE = VK_QUOTEDBL;
	public static final int K_LESS = VK_LESS;
	public static final int K_GREATER = VK_GREATER;
	public static final int K_COMMA = VK_COMMA;
	public static final int K_PERIOD = VK_PERIOD;
	public static final int K_SLASH = VK_SLASH;
	public static final int K_BACK_SLASH = VK_BACK_SLASH;

	// Numbers
	public static final int K_0 = VK_0;
	public static final int K_1 = VK_1;
	public static final int K_2 = VK_2;
	public static final int K_3 = VK_3;
	public static final int K_4 = VK_4;
	public static final int K_5 = VK_5;
	public static final int K_6 = VK_6;
	public static final int K_7 = VK_7;
	public static final int K_8 = VK_8;
	public static final int K_9 = VK_9;

	// Colons
	public static final int K_COLON = VK_COLON;
	public static final int K_SEMICOLON = VK_SEMICOLON;

	// Alphabet
	public static final int K_A = VK_A;
	public static final int K_B = VK_B;
	public static final int K_C = VK_C;
	public static final int K_D = VK_D;
	public static final int K_E = VK_E;
	public static final int K_F = VK_F;
	public static final int K_G = VK_G;
	public static final int K_H = VK_H;
	public static final int K_I = VK_I;
	public static final int K_J = VK_J;
	public static final int K_K = VK_K;
	public static final int K_L = VK_L;
	public static final int K_M = VK_M;
	public static final int K_N = VK_N;
	public static final int K_O = VK_O;
	public static final int K_P = VK_P;
	public static final int K_Q = VK_Q;
	public static final int K_R = VK_R;
	public static final int K_S = VK_S;
	public static final int K_T = VK_T;
	public static final int K_U = VK_U;
	public static final int K_V = VK_V;
	public static final int K_W = VK_W;
	public static final int K_X = VK_X;
	public static final int K_Y = VK_Y;
	public static final int K_Z = VK_Z;


	// Numpad numbers
	public static final int K_NUMPAD_0 = VK_NUMPAD0;
	public static final int K_NUMPAD_1 = VK_NUMPAD1;
	public static final int K_NUMPAD_2 = VK_NUMPAD2;
	public static final int K_NUMPAD_3 = VK_NUMPAD3;
	public static final int K_NUMPAD_4 = VK_NUMPAD4;
	public static final int K_NUMPAD_5 = VK_NUMPAD5;
	public static final int K_NUMPAD_6 = VK_NUMPAD6;
	public static final int K_NUMPAD_7 = VK_NUMPAD7;
	public static final int K_NUMPAD_8 = VK_NUMPAD8;
	public static final int K_NUMPAD_9 = VK_NUMPAD9;

	// Numpad values
	public static final int K_NUMPAD_MULTIPLY = VK_MULTIPLY;
	public static final int K_NUMPAD_ADD = VK_ADD;
	public static final int K_NUMPAD_SEPARATER = VK_SEPARATER;
	public static final int K_NUMPAD_SEPARATOR = VK_SEPARATOR;
	public static final int K_NUMPAD_SUBTRACT = VK_SUBTRACT;
	public static final int K_NUMPAD_DECIMAL = VK_DECIMAL;
	public static final int K_NUMPAD_DIVIDE = VK_DIVIDE;

	// Keypad directions
	public static final int K_KEYPAD_UP = VK_KP_UP;
	public static final int K_KEYPAD_DOWN = VK_KP_DOWN;
	public static final int K_KEYPAD_LEFT = VK_KP_LEFT;
	public static final int K_KEYPAD_RIGHT = VK_KP_RIGHT;

	// Function keys
	public static final int K_F1 = VK_F1;
	public static final int K_F2 = VK_F2;
	public static final int K_F3 = VK_F3;
	public static final int K_F4 = VK_F4;
	public static final int K_F5 = VK_F5;
	public static final int K_F6 = VK_F6;
	public static final int K_F7 = VK_F7;
	public static final int K_F8 = VK_F8;
	public static final int K_F9 = VK_F9;
	public static final int K_F10 = VK_F10;
	public static final int K_F11 = VK_F11;
	public static final int K_F12 = VK_F12;

	// Locks
	public static final int K_NUM_LOCK = VK_NUM_LOCK;
	public static final int K_SCROLL_LOCK = VK_SCROLL_LOCK;
	public static final int K_PRINTSCREEN = VK_PRINTSCREEN;

	// Brackets
	public static final int K_OPEN_BRACKET = VK_OPEN_BRACKET;
	public static final int K_LEFT_BRACKET = VK_OPEN_BRACKET;
	public static final int K_CLOSE_BRACKET = VK_CLOSE_BRACKET;
	public static final int K_RIGHT_BRACKET = VK_CLOSE_BRACKET;
	public static final int K_OPEN_BRACE = VK_BRACELEFT;
	public static final int K_LEFT_BRACE = VK_BRACELEFT;
	public static final int K_CLOSE_BRACE = VK_BRACERIGHT;
	public static final int K_RIGHT_BRACE = VK_BRACERIGHT;
	public static final int K_OPEN_PARENTHESIS = VK_LEFT_PARENTHESIS;
	public static final int K_LEFT_PARENTHESIS = VK_LEFT_PARENTHESIS;
	public static final int K_CLOSE_PARENTHESIS = VK_RIGHT_PARENTHESIS;
	public static final int K_RIGHT_PARENTHESIS = VK_RIGHT_PARENTHESIS;


	// Some alternate keys
	public static final int K_EXCLAMATION_MARK = VK_EXCLAMATION_MARK; // 1
	public static final int K_AT = VK_AT; // 2
	public static final int K_NUMBER_SIGN = VK_NUMBER_SIGN; // 3
	public static final int K_DOLLAR = VK_DOLLAR; // 4
	public static final int K_EURO = VK_EURO_SIGN; // 4
	public static final int K_CIRCUMFLEX = VK_CIRCUMFLEX; // 6
	public static final int K_AMPERSAND = VK_AMPERSAND; // 7
	public static final int K_ASTERISK = VK_ASTERISK; // 8


	// Mouse buttons
	public static final int MOUSE_LEFT = BUTTON1;
	public static final int MOUSE_MIDDLE = BUTTON2;
	public static final int MOUSE_RIGHT = BUTTON3;




	// Key Listener
	private static Object lock_keyData = new Object();
	private static Set<Integer> back_keyDown = new HashSet<Integer>();
	private static Set<Integer> back_keyPressed = new HashSet<Integer>();
	private static Set<Integer> back_keyReleased = new HashSet<Integer>();

	private static Set<Integer> front_keyDown = new HashSet<Integer>();
	private static Set<Integer> front_keyPressed = new HashSet<Integer>();
	private static Set<Integer> front_keyReleased = new HashSet<Integer>();

	// TODO is there a better way to do this?
	private static String front_keyTyped;
	static {
		try {
			front_keyTyped = new String(new String("blank").getBytes("ASCII"), "UTF-8").substring(0, 0);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	private static StringBuilder back_keyTyped = new StringBuilder();


	// Mouse Listener
	private static Object lock_mouseData = new Object();
	private static Set<Integer> back_mouseDown = new HashSet<Integer>();
	private static Set<Integer> back_mousePressed = new HashSet<Integer>();
	private static Set<Integer> back_mouseReleased = new HashSet<Integer>();

	private static Set<Integer> front_mouseDown = new HashSet<Integer>();
	private static Set<Integer> front_mousePressed = new HashSet<Integer>();
	private static Set<Integer> front_mouseReleased = new HashSet<Integer>();

	private static Vec2i back_mousePos = Vec2i.zero;
	private static Vec2i front_mousePos = Vec2i.zero;

	private static double back_mouseScroll = 0;
	private static double front_mouseScroll = 0;

	private static Vec2i back_mouseTravel = Vec2i.zero;
	private static Vec2i front_mouseTravel = Vec2i.zero;

	// Cursor manipulation
	private static Robot robot;
	private static Cursor defaultCursor = Cursor.getDefaultCursor();
	private static Cursor blankCursor;

	private static boolean onFocusMouseCapture = false;
	private static boolean isMouseCaptured = false;



	//
	private static InputListener ipl = null;
	private static Component canvas = null;


	// Initialize method to be called before anything works
	public static void initialize(Component c) {
		ipl = new InputListener();
		canvas = c;

		// add the listener to the canvas
		canvas.addKeyListener(ipl);
		canvas.addMouseListener(ipl);
		canvas.addMouseMotionListener(ipl);
		canvas.addMouseWheelListener(ipl);

		// globally stop arrow scrolling input on ScrollPanes
		InputMap actionMap = (InputMap) UIManager.getDefaults().get("ScrollPane.ancestorInputMap");
		actionMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {}
		});
		actionMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {}
		});
		actionMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {}
		});
		actionMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {}
		});

		// Create a blank cursor for the window
		BufferedImage cursorimg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorimg, new Point(0, 0), "blankCursor");

		// If the window is unfocused, uncapture mouse etc.
		canvas.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				setMouseCapture(onFocusMouseCapture);
			}

			@Override
			public void focusLost(FocusEvent e) {
				onFocusMouseCapture = isMouseCaptured;
				setMouseCapture(false);
			}
		});

		// Create the robot for mouse capture
		try {
			robot = new Robot();
		} catch (AWTException e) {
			throw new AssertionError(e);
		}


	}

	public static void poll() {
		synchronized(lock_keyData) {
			front_keyDown = new HashSet<Integer>(back_keyDown);

			front_keyTyped = back_keyTyped.toString();
			back_keyTyped.setLength(0); // clear buffer


			front_keyPressed = back_keyPressed;
			back_keyPressed = new HashSet<Integer>();

			front_keyReleased = back_keyReleased;
			back_keyReleased = new HashSet<Integer>();

			front_mousePressed = back_mousePressed;
			back_mousePressed = new HashSet<Integer>();

			front_mouseReleased = back_mouseReleased;
			back_mouseReleased = new HashSet<Integer>();
		}

		synchronized(lock_mouseData) {
			front_mousePos = back_mousePos;

			front_mouseDown = new HashSet<Integer>(back_mouseDown);

			front_mouseScroll = back_mouseScroll;
			back_mouseScroll = 0;

			front_mouseTravel = back_mouseTravel;
			back_mouseTravel = Vec2i.zero;
		}

	}

	public static boolean keyDown(int keycode) {
		return front_keyDown.contains(keycode);
	}

	public static boolean keyPressed(int keycode) {
		return front_keyPressed.contains(keycode);
	}

	public static boolean keyReleased(int keycode) {
		return front_keyReleased.contains(keycode);
	}

	public static String keysTyped() {
		return front_keyTyped;
	}

	public static Vec2i mousePosition() {
		return front_mousePos;
	}

	public static boolean mouseDown(int mousecode) {
		return front_mouseDown.contains(mousecode);
	}

	public static boolean mousePressed(int mousecode) {
		return front_mousePressed.contains(mousecode);
	}

	public static boolean mouseReleased(int mousecode) {
		return front_mouseReleased.contains(mousecode);
	}

	public static double mouseScroll() {
		return front_mouseScroll;
	}

	// Cumulative travel from the center of the screen when captured
	public static Vec2i mouseTravel() {
		return front_mouseTravel;
	}

	public static void setMouseCapture(boolean state) {
		synchronized(lock_mouseData) {
			isMouseCaptured = state;
			if (isMouseCaptured) {
				int cx = canvas.getWidth()/2;
				int cy = canvas.getHeight()/2;
				Point canvasLoc = canvas.getLocationOnScreen();

				// Put mouse in the middle of the canvas
				robot.mouseMove(canvasLoc.x+cx, canvasLoc.y+cy);
				canvas.setCursor(blankCursor);
			} else {
				canvas.setCursor(defaultCursor);
			}
		}
	}

	public static boolean isMouseCaptured() {
		return isMouseCaptured;
	}



	private static class InputListener implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {
		@Override
		public void keyPressed(KeyEvent arg0) {
			synchronized(lock_keyData) {
				back_keyDown.add(arg0.getKeyCode());
				back_keyPressed.add(arg0.getKeyCode());
			}
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			synchronized(lock_keyData) {
				back_keyDown.remove(arg0.getKeyCode());
				back_keyReleased.add(arg0.getKeyCode());
			}
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			synchronized (lock_keyData) {
				back_keyTyped.append(arg0.getKeyChar());
			}
		}


		@Override public void mouseClicked(MouseEvent arg0) {}
		@Override public void mouseEntered(MouseEvent arg0) {}
		@Override public void mouseExited(MouseEvent arg0) {}

		@Override
		public void mousePressed(MouseEvent arg0) {
			synchronized(lock_keyData) {
				back_mouseDown.add(arg0.getButton());
				back_mousePressed.add(arg0.getButton());
			}
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			synchronized(lock_mouseData) {
				back_mouseDown.remove(arg0.getButton());
				back_mouseReleased.add(arg0.getButton());
			}
		}

		private void mouseMoveDispatch(MouseEvent e) {
			synchronized(lock_mouseData) {
				Point canvasLoc = canvas.getLocationOnScreen();
				back_mousePos = vec2i(e.getXOnScreen()-canvasLoc.getX(), e.getYOnScreen()-canvasLoc.getY());

				if (isMouseCaptured) {
					Vec2i center = vec2i(canvas.getWidth()/2, canvas.getHeight()/2);
					back_mouseTravel = back_mouseTravel.add(back_mousePos.sub(center));

					if (any(back_mouseTravel.ne(Vec2i.zero))) {
						robot.mouseMove(canvasLoc.x + center.x, canvasLoc.y + center.y);
					}
				}
			}
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			mouseMoveDispatch(e);
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			mouseMoveDispatch(e);
		}

		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			synchronized (lock_mouseData) {
				back_mouseScroll += e.getPreciseWheelRotation();
			}
		}
	}
}
