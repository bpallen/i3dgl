package initial3d.test;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import initial3d.GL;
import initial3d.GLEnum;

import static initial3d.GLEnum.*;

public class Mesh {

	private final ByteBuffer vertices;
	private final ByteBuffer textures;
	private final ByteBuffer normals;
	private final ByteBuffer colors;

	public final int vertexCount;
	public final int faceCount;

	//TODO bounding box etc
	
	private int vao;
	private int vbo_pos;
	private int vbo_norm;
	private int vbo_col;
	private int vbo_uv;

	public Mesh(ByteBuffer bbp, ByteBuffer bbt, ByteBuffer bbn, ByteBuffer bbc) {
		// Assert the buffers are exactly the right size (factor of 3 floats)
		assert(bbp.capacity()%12 == 0);
		assert(bbt.capacity()%8 == 0);
		assert(bbn.capacity()%12 == 0);
		assert(bbc.capacity()%12 == 0);

		this.vertexCount = bbp.capacity()/12;
		int tCount = bbt.capacity()/8;
		int nCount = bbn.capacity()/12;
		int cCount = bbc.capacity()/12;

		// Assert buffers are same size and have (factor of 3 vectors)
		assert(vertexCount == tCount && vertexCount == nCount && vertexCount == cCount);
		assert(vertexCount % 3 == 0);
		this.faceCount = this.vertexCount/3;

		this.vertices = bbp;
		this.textures = bbt;
		this.normals = bbn;
		this.colors = bbc;
		
	}
	
	public void buildVAO(GL gl) {
		if (vao != 0) gl.glDeleteVertexArray(vao);
		if (vbo_pos != 0) gl.glDeleteBuffer(vbo_pos);
		
		vao = gl.glGenVertexArray();
		gl.glBindVertexArray(vao);
		
		// TODO one buffer, interleaved
		
		vbo_pos = gl.glGenBuffer();
		gl.glBindBuffer(GL_ARRAY_BUFFER, vbo_pos);
		gl.glBufferData(GL_ARRAY_BUFFER, vertices.remaining(), vertices, GL_STATIC_DRAW);
		gl.glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
		gl.glEnableVertexAttribArray(0);
		
		vbo_norm = gl.glGenBuffer();
		gl.glBindBuffer(GL_ARRAY_BUFFER, vbo_norm);
		gl.glBufferData(GL_ARRAY_BUFFER, normals.remaining(), normals, GL_STATIC_DRAW);
		gl.glVertexAttribPointer(1, 3, GL_FLOAT, false, 0, 0);
		gl.glEnableVertexAttribArray(1);
		
		vbo_col = gl.glGenBuffer();
		gl.glBindBuffer(GL_ARRAY_BUFFER, vbo_col);
		gl.glBufferData(GL_ARRAY_BUFFER, colors.remaining(), colors, GL_STATIC_DRAW);
		gl.glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);
		gl.glEnableVertexAttribArray(2);
		
		vbo_uv = gl.glGenBuffer();
		gl.glBindBuffer(GL_ARRAY_BUFFER, vbo_uv);
		gl.glBufferData(GL_ARRAY_BUFFER, textures.remaining(), textures, GL_STATIC_DRAW);
		gl.glVertexAttribPointer(3, 2, GL_FLOAT, false, 0, 0);
		gl.glEnableVertexAttribArray(3);
		
		gl.glBindBuffer(GL_ARRAY_BUFFER, 0);
		gl.glBindVertexArray(0);
		
	}
	
	public int vao() {
		return vao;
	}

	public FloatBuffer vertexBuffer() {
		return this.vertices.asFloatBuffer().asReadOnlyBuffer();
	}

	public FloatBuffer textureBuffer() {
		return this.textures.asFloatBuffer().asReadOnlyBuffer();
	}

	public FloatBuffer normalBuffer() {
		return this.normals.asFloatBuffer().asReadOnlyBuffer();
	}
	
	public FloatBuffer colorBuffer() {
		return this.colors.asFloatBuffer().asReadOnlyBuffer();
	}
}
