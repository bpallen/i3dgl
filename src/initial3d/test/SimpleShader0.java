package initial3d.test;

import initial3d.detail.Util;
import initial3d.detail.gl.ShaderProgramAccessor;

public class SimpleShader0 {
	
	public static int vertexOutSize_static() {
		return 32;
	}
	
	public static int geometryOutSize_static() {
		return 32;
	}
	
	public static void shadeVertex_static(long pvert_out, long ptemp, long puniforms, long pattrib_in) {
		Util.mulMat4Vec4(pvert_out, puniforms, pattrib_in + 16);
		switch (Util.geti(pattrib_in) % 3) {
		case 0:
			Util.putf(pvert_out + 16, 1, 0, 0);
			break;
		case 1:
			Util.putf(pvert_out + 16, 0, 1, 0);
			break;
		case 2:
			Util.putf(pvert_out + 16, 0, 0, 1);
			break;
		default:
			break;
		}
	}

	public static void varyingInterpolate2_static(long pvary_out, long pvary0_in, long pvary1_in, float k0, float k1, float w0, float w1, float w) {
		varyingInterpolate3_static(pvary_out, pvary0_in, pvary1_in, pvary1_in, k0, k1, 0.f, 1.f, 1.f, 1.f);
	}
	
	public static void varyingInterpolate3_static(long pvary_out, long pvary0_in, long pvary1_in, long pvary2_in, float k0, float k1, float k2, float iw0, float iw1, float iw2) {
		Util.interp3Vec3(pvary_out, pvary0_in, pvary1_in, pvary2_in, k0 * iw0, k1 * iw1, k2 * iw2);
	}
	
	public static void varyingSub_static(long pderiv_out, long pvary0_in, long pvary1_in) {
		Util.subVec3(pderiv_out, pvary0_in, pvary1_in);
	}
	
	public static void varyingAdd_static(long pvary_out, long pvary_in, long pderiv_in) {
		Util.addVec3(pvary_out, pvary_in, pderiv_in);
	}
	
	public static void varyingMul_static(long pderiv_out, long pderiv_in, float k) {
		Util.mulVec3Scalar(pderiv_out, pderiv_in, k);
	}
	
	public static void varyingFma_static(long pvary_out, long pderiv_in, float k, long pvary_in) {
		Util.fmaVec3Scalar(pvary_out, pderiv_in, k, pvary_in);
	}
	
	public static void varyingCopy_static(long pvary_out, long pvary_in) {
		Util.copyVec3(pvary_out, pvary_in);
	}
	
	public static int shadeFragmentQuad_static(
		long pfrag_out, long ptemp, long puniforms, long pvary, long pvary_ddx, long pvary_ddy,
		int x, int y,
		float z, float dz_dx, float dz_dy,
		float iw, float diw_dx, float diw_dy
	) {
		float w00 = 1.f / iw;
		float w10 = 1.f / (iw + diw_dx);
		float w01 = 1.f / (iw + diw_dy);
		float w11 = 1.f / (iw + diw_dx + diw_dy);
		// r
		float r00 = w00 * Util.getf(pvary);
		float r10 = w10 * (Util.getf(pvary) + Util.getf(pvary_ddx));
		float r01 = w01 * (Util.getf(pvary) + Util.getf(pvary_ddy));
		float r11 = w11 * (Util.getf(pvary) + Util.getf(pvary_ddx) + Util.getf(pvary_ddy));
		// g
		float g00 = w00 * Util.getf(pvary + 4);
		float g10 = w10 * (Util.getf(pvary + 4) + Util.getf(pvary_ddx + 4));
		float g01 = w01 * (Util.getf(pvary + 4) + Util.getf(pvary_ddy + 4));
		float g11 = w11 * (Util.getf(pvary + 4) + Util.getf(pvary_ddx + 4) + Util.getf(pvary_ddy + 4));
		// b
		float b00 = w00 * Util.getf(pvary + 8);
		float b10 = w10 * (Util.getf(pvary + 8) + Util.getf(pvary_ddx + 8));
		float b01 = w01 * (Util.getf(pvary + 8) + Util.getf(pvary_ddy + 8));
		float b11 = w11 * (Util.getf(pvary + 8) + Util.getf(pvary_ddx + 8) + Util.getf(pvary_ddy + 8));
		
		Util.putf(pfrag_out +  0, r00, r10, r01, r11);
		Util.putf(pfrag_out + 16, g00, g10, g01, g11);
		Util.putf(pfrag_out + 32, b00, b10, b01, b11);
		
		return 0xF;
	}
	
}
