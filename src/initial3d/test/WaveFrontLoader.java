package initial3d.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.plaf.synth.SynthSeparatorUI;

import initial3d.*;

import static initial3d.Functions.*;

public class WaveFrontLoader {


	public static Mesh Load(String filename) {
		try {
			System.out.println("loading " + filename + "...");
			return WaveFrontLoader.Load(new FileInputStream(filename));
		} catch (FileNotFoundException e) {
		}

		return null;
	}

	public static Mesh Load(InputStream in) {

		LineNumberReader input = new LineNumberReader(new InputStreamReader(in));
		String line = null;

		try {
			List<Vec3f> vertices = new ArrayList<Vec3f>();
			List<Vec2f> textures = new ArrayList<Vec2f>();
			List<Vec3f> normals = new ArrayList<Vec3f>();

			List<Polygon> polygons = new ArrayList<Polygon>();

			vertices.add(vec3f(0));
			textures.add(vec2f(0));
			normals.add(vec3f(0, 0, 1));


			for(line = input.readLine(); line != null; line = input.readLine()) {
				if(line.length() > 0) {
					StringTokenizer tok = new StringTokenizer(line);

					if(tok.hasMoreElements()) tok.nextToken();
					else continue;

					if(line.startsWith("v ")) {
						float x = Float.parseFloat(tok.nextToken()), y = Float.parseFloat(tok.nextToken()), z = Float.parseFloat(tok.nextToken());
						vertices.add(vec3f(x, y, z));
					}
					else if(line.startsWith("vt ")) {
						float u = Float.parseFloat(tok.nextToken()), v = Float.parseFloat(tok.nextToken());
						textures.add(vec2f(u, v));
					}
					else if(line.startsWith("vn ")) {
						float i = Float.parseFloat(tok.nextToken()), j = Float.parseFloat(tok.nextToken()), k = Float.parseFloat(tok.nextToken());
						normals.add(normalize(vec3f(i, j, k)));
					}
					else if(line.startsWith("f ")) {

						int i = 0;
						Polygon p = new Polygon();

						while(tok.hasMoreTokens()) {
							String token = tok.nextToken();
							String first = token, second = null, third = null;
							if(token.indexOf('/') > -1) {
								first = token.substring(0,  token.indexOf('/'));
								second = token.substring(token.indexOf('/')+1, token.length());
								if(second.indexOf('/') > -1) {
									third = second.substring(second.indexOf('/')+1, second.length());
									second = second.substring(0, second.indexOf('/'));
								}
							}

							int vi = Integer.parseInt(first), ti = 0, ni = 0;
							p.pos[i] = vi;

							if(second != null && second.length() > 0) {
								ti = Integer.parseInt(second);
								p.uv[i] = ti;
							}

							if(third != null && third.length() > 0) {
								ni = Integer.parseInt(third);
								p.norm[i] = ni;
							}

							++i;
							
							p.count++;
						}

						polygons.add(p);
					}
				}
			}

			if (normals.size() == 1) {
				//TODO calculate normals
			}

			int tris = 0;
			MeshBuilder builder = new MeshBuilder();
			for (Polygon p : polygons) {
				for (int i=1; i<p.count-1; ++i) {
					if (textures.size() > 1) builder.setUV(textures.get(p.uv[0]));
					if (normals.size() > 1) builder.setNormal(normals.get(p.norm[0]));
					builder.addVertex(vertices.get(p.pos[0]));
					if (textures.size() > 1) builder.setUV(textures.get(p.uv[i]));
					if (normals.size() > 1) builder.setNormal(normals.get(p.norm[i]));
					builder.addVertex(vertices.get(p.pos[i]));
					if (textures.size() > 1) builder.setUV(textures.get(p.uv[i+1]));
					if (normals.size() > 1) builder.setNormal(normals.get(p.norm[i+1]));
					builder.addVertex(vertices.get(p.pos[i+1]));
					tris++;
				}
			}
			
			System.out.println("loaded " + tris + " triangles");
			
			return builder.mesh();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static class Polygon {
		public int count = 0;
		public int[] pos = new int[30];
		public int[] uv = new int[30];
		public int[] norm = new int[30];
	}
}
