package initial3d.test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import initial3d.*;

import static initial3d.Functions.*;

public class MeshBuilder {

	private List<Float> vertices = new ArrayList<Float>();
	private List<Float> normals = new ArrayList<Float>();
	private List<Float> textures = new ArrayList<Float>();
	private List<Float> colors = new ArrayList<Float>();

	private Vec3f normal = Vec3f.k;
	private Vec2f uv = Vec2f.zero;
	private Vec3f color = vec3f(1);

	public MeshBuilder() {}

	public Mesh mesh() {
		ByteBuffer bbp = ByteBuffer.allocate(vertices.size()*4);
		ByteBuffer bbn = ByteBuffer.allocate(normals.size()*4);
		ByteBuffer bbt = ByteBuffer.allocate(textures.size()*4);
		ByteBuffer bbc = ByteBuffer.allocate(colors.size()*4);

		bbp.order(ByteOrder.nativeOrder());
		bbn.order(ByteOrder.nativeOrder());
		bbt.order(ByteOrder.nativeOrder());
		bbc.order(ByteOrder.nativeOrder());

		FloatBuffer p = bbp.asFloatBuffer();
		FloatBuffer n = bbn.asFloatBuffer();
		FloatBuffer t = bbt.asFloatBuffer();
		FloatBuffer c = bbc.asFloatBuffer();

		for (Float f : vertices) {p.put(f);}
		for (Float f : normals) {n.put(f);}
		for (Float f : textures) {t.put(f);}
		for (Float f : colors) {c.put(f);}
		

		return new Mesh(bbp, bbt, bbn, bbc);
	}

	
	public void setNormal(float nx, float ny, float nz) {
		this.normal = vec3f(nx, ny, nz);
	}

	public void setNormal(Vec3f n) {
		this.normal = n;
	}
	
	public void setUV(float u, float v) {
		this.uv = vec2f(u, v);
	}

	public void setUV(Vec2f uv) {
		this.uv = uv;
	}
	
	public void setColor(float r, float g, float b) {
		this.color = vec3f(r, g, b);
	}

	public void setColor(Vec3f c) {
		this.color = c;
	}

	public void addVertex(float vx, float vy, float vz) {
		this.normals.add(this.normal.x);
		this.normals.add(this.normal.y);
		this.normals.add(this.normal.z);

		this.textures.add(this.uv.x);
		this.textures.add(this.uv.y);
		
		this.colors.add(this.color.x);
		this.colors.add(this.color.y);
		this.colors.add(this.color.z);

		this.vertices.add(vx);
		this.vertices.add(vy);
		this.vertices.add(vz);
	}

	public void addVertex(Vec3f v) {
		this.addVertex(v.x, v.y, v.z);
	}

}