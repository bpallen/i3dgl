package initial3d.test;

public class ShaderException extends RuntimeException {

	public ShaderException(String msg) {
		super(msg);
	}
	
}
