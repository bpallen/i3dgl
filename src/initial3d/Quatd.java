package initial3d;

import static initial3d.Functions.*;

import initial3d.detail.StringUtil;

public class Quatd {

    public static final Quatd zero = new Quatd(0, 0, 0, 0);
    public static final Quatd one = new Quatd(1, 0, 0, 0);
    
    public static final Quatd i = new Quatd(0, 1, 0, 0);
    public static final Quatd j = new Quatd(0, 0, 1, 0);
    public static final Quatd k = new Quatd(0, 0, 0, 1);
 
    public final double w;
    public final double x;
    public final double y;
    public final double z;
    
    Quatd(double w_, double x_, double y_, double z_) {
    	w = w_;
        x = x_;
        y = y_;
        z = z_;
    }

    Quatd(Vec3d axis, double angle) {
        // angle(+-2*PI) <=> w(-1) and angle(0) <=> w(1)
        axis = normalize(axis);
        double sin_a = Math.sin(angle * 0.5);
        w = Math.cos(angle * 0.5);
        x = axis.x * sin_a;
        y = axis.y * sin_a;
        z = axis.z * sin_a;
    }

    Quatd(Vec3d rot) {
        if (Double.isInfinite(1/norm(rot))) {
            w = 1;
            x = 0;
            y = 0;
            z = 0;
        } else {
            double angle = norm(rot);
            Vec3d axis = normalize(rot);
            // angle(+-2*PI) <=> w(-1) and angle(0) <=> w(1)
            double sin_a = Math.sin(angle * 0.5);
            w = Math.cos(angle * 0.5);
            x = axis.x * sin_a;
            y = axis.y * sin_a;
            z = axis.z * sin_a;
        }
    }
    
    public Quatd add(Quatd rhs) {
        return new Quatd(w + rhs.w, x + rhs.x, y + rhs.y, z + rhs.z);
    }

    public Quatd sub(Quatd rhs) {
        return new Quatd(w - rhs.w, x - rhs.x, y - rhs.y, z - rhs.z);
    }
    
    public Quatd mul(double f) {
        return new Quatd(w * f, x * f, y * f, z * f);
    }

    public Quatd mul(Quatd rhs) {
        return new Quatd(w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z, w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y, w
                * rhs.y - x * rhs.z + y * rhs.w + z * rhs.x, w * rhs.z + x * rhs.y - y * rhs.x + z * rhs.w);
    }

    public Quatd mul(double rightw, double rightx, double righty, double rightz) {
        return new Quatd(w * rightw - x * rightx - y * righty - z * rightz, w * rightx + x * rightw + y * rightz - z
                * righty, w * righty - x * rightz + y * rightw + z * rightx, w * rightz + x * righty - y * rightx + z
                * rightw);
    }
    
    public Vec3d mul(Vec3d v) {
        // this * v * this^(-1)
        Quatd q = this.mul(0, v.x, v.y, v.z).mul(conjugate(this));
        return new Vec3d(q.x, q.y, q.z);
    }

    public Quatd lmul(Quatd lhs) {
        return new Quatd(lhs.w * w - lhs.x * x - lhs.y * y - lhs.z * z, lhs.w * x + lhs.x * w + lhs.y * z - lhs.z * y,
                lhs.w * y - lhs.x * z + lhs.y * w + lhs.z * x, lhs.w * z + lhs.x * y - lhs.y * x + lhs.z * w);
    }

    public Quatd lmul(double leftw, double leftx, double lefty, double leftz) {
        return new Quatd(leftw * w - leftx * x - lefty * y - leftz * z, leftw * x + leftx * w + lefty * z - leftz * y,
                leftw * y - leftx * z + lefty * w + leftz * x, leftw * z + leftx * y - lefty * x + leftz * w);
    }

	public String toString() {
		final int prec = 5;
		return String.format(
			"(%s, %s, %s, %s)",
			StringUtil.formatDouble(w, prec), StringUtil.formatDouble(x, prec),
			StringUtil.formatDouble(y, prec), StringUtil.formatDouble(z, prec)
		);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(w);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec4d other = (Vec4d) obj;
		if (Double.doubleToLongBits(w) != Double.doubleToLongBits(other.w)) return false;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) return false;
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z)) return false;
		return true;
	}
}
