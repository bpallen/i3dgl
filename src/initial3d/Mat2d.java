package initial3d;

import java.util.Arrays;

import initial3d.detail.ArrayMath;
import initial3d.detail.StringUtil;

public class Mat2d {

	public static final Mat2d zero = new Mat2d();
	public static final Mat2d eye = new Mat2d(1);
	
	// column-major
	private final double[] e;

	private static int index(int r, int c) {
		return 2 * c + r;
	}
	
	private static int arraySize() {
		return 4;
	}
	
	public Mat2d() {
		e = new double[arraySize()];
	}

	public Mat2d(double x) {
		e = new double[] { x, 0, 0, x };
	}
	
	public Mat2d(double e00_, double e01_, double e10_, double e11_) {
		e = new double[] { e00_, e01_, e10_, e11_ };
	}

	public double get(int r, int c) {
		return e[index(r, c)];
	}

	public Mat2d with(int r, int c, double val) {
		Mat2d m = new Mat2d();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(r, c)] = val;
		return m;
	}

	public Vec2d col(int c) {
		return new Vec2d(get(0, c), get(1, c));
	}

	public Vec2d row(int r) {
		return new Vec2d(get(r, 0), get(r, 1));
	}

	public Mat2d withCol(int c, Vec2d v) {
		Mat2d m = new Mat2d();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(0, c)] = v.x;
		m.e[index(1, c)] = v.y;
		return m;
	}

	public Mat2d withRow(int r, Vec2d v) {
		Mat2d m = new Mat2d();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(r, 0)] = v.x;
		m.e[index(r, 1)] = v.y;
		return m;
	}

	public Mat2d add(Mat2d rhs) {
		Mat2d m = new Mat2d();
		ArrayMath.add(arraySize(), m.e, 0, e, 0, rhs.e, 0);
		return m;
	}

	public Mat2d add(double rhs) {
		Mat2d m = new Mat2d();
		ArrayMath.add(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat2d sub(Mat2d rhs) {
		Mat2d m = new Mat2d();
		ArrayMath.sub(arraySize(), m.e, 0, e, 0, rhs.e, 0);
		return m;
	}

	public Mat2d sub(double rhs) {
		Mat2d m = new Mat2d();
		ArrayMath.sub(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat2d mul(double rhs) {
		Mat2d m = new Mat2d();
		ArrayMath.mul(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat2d div(double rhs) {
		Mat2d m = new Mat2d();
		ArrayMath.div(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat2d mul(Mat2d rhs) {
		Mat2d m = new Mat2d();
		double r;
		double t0;
		double t1;
		r = rhs.get(0, 0);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		r = rhs.get(1, 0);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		m.e[index(0, 0)] = t0;
		m.e[index(1, 0)] = t1;
		r = rhs.get(0, 1);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		r = rhs.get(1, 1);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		m.e[index(0, 1)] = t0;
		m.e[index(1, 1)] = t1;
		return m;
	}

	public Vec2d mul(Vec2d rhs) {
		double r;
		double t0;
		double t1;
		r = rhs.x;
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		r = rhs.y;
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		return new Vec2d(t0, t1);
	}
	
	@Override
	public String toString() {
		return StringUtil.matrixToString(e, 2);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(e);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Mat2d other = (Mat2d) obj;
		if (!Arrays.equals(e, other.e)) return false;
		return true;
	}
	
}
