package initial3d;

public class Vec2b {

	public static final Vec2b zero = new Vec2b(false, false);
	
	public static final Vec2b i = new Vec2b(true, false);
	public static final Vec2b j = new Vec2b(false, true);

	public final boolean x;
	public final boolean y;

	public Vec2b() {
		this(false, false);
	}

	public Vec2b(boolean x_, boolean y_) {
		x = x_;
		y = y_;
	}

	public boolean get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec2b with(int i, boolean val) {
		switch (i) {
		case 0:
			return new Vec2b(val, y);
		case 1:
			return new Vec2b(x, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec2b withX(boolean x_) {
		return new Vec2b(x_, y);
	}
	
	public Vec2b withY(boolean y_) {
		return new Vec2b(x, y_);
	}

	public Vec2b not() {
		return new Vec2b(!x, !y);
	}
	
	public Vec2b and(Vec2b rhs) {
		return new Vec2b(x && rhs.x, y && rhs.y);
	}
	
	public Vec2b or(Vec2b rhs) {
		return new Vec2b(x || rhs.x, y || rhs.y);
	}
	
	public Vec2b eq(Vec2b rhs) {
		return new Vec2b(x == rhs.x, y == rhs.y);
	}
	
	public Vec2b ne(Vec2b rhs) {
		return new Vec2b(x != rhs.x, y != rhs.y);
	}
	
	@Override
	public String toString() {
		return String.format("(%b, %b)", x, y);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (x ? 1231 : 1237);
		result = prime * result + (y ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec2b other = (Vec2b) obj;
		if (x != other.x) return false;
		if (y != other.y) return false;
		return true;
	}
	
}
