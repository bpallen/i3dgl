package initial3d;

public class Vec2i {

	public static final Vec2i zero = new Vec2i(0, 0);
	
	public static final Vec2i i = new Vec2i(1, 0);
	public static final Vec2i j = new Vec2i(0, 1);

	public final int x;
	public final int y;
	
	public Vec2i() {
		this(0, 0);
	}

	public Vec2i(int x_, int y_) {
		x = x_;
		y = y_;
	}

	public int get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec2i with(int i, int val) {
		switch (i) {
		case 0:
			return new Vec2i(val, y);
		case 1:
			return new Vec2i(x, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec2i withX(int x_) {
		return new Vec2i(x_, y);
	}
	
	public Vec2i withY(int y_) {
		return new Vec2i(x, y_);
	}

	public Vec2i neg() {
		return new Vec2i(-x, -y);
	}

	public Vec2i add(Vec2i rhs) {
		return new Vec2i(x + rhs.x, y + rhs.y);
	}

	public Vec2i add(int rhs) {
		return new Vec2i(x + rhs, y + rhs);
	}

	public Vec2i sub(Vec2i rhs) {
		return new Vec2i(x - rhs.x, y - rhs.y);
	}

	public Vec2i sub(int rhs) {
		return new Vec2i(x - rhs, y - rhs);
	}

	public Vec2i mul(Vec2i rhs) {
		return new Vec2i(x * rhs.x, y * rhs.y);
	}

	public Vec2i mul(int rhs) {
		return new Vec2i(x * rhs, y * rhs);
	}

	public Vec2i div(Vec2i rhs) {
		return new Vec2i(x / rhs.x, y / rhs.y);
	}

	public Vec2i div(int rhs) {
		return new Vec2i(x / rhs, y / rhs);
	}

	public Vec2i bitnot() {
		return new Vec2i(~x, ~y);
	}
	
	public Vec2i bitand(Vec2i rhs) {
		return new Vec2i(x & rhs.x, y & rhs.y);
	}
	
	public Vec2i bitor(Vec2i rhs) {
		return new Vec2i(x | rhs.x, y | rhs.y);
	}
	
	public Vec2b lt(Vec2i rhs) {
		return new Vec2b(x < rhs.x, y < rhs.y);
	}
	
	public Vec2b le(Vec2i rhs) {
		return new Vec2b(x <= rhs.x, y <= rhs.y);
	}
	
	public Vec2b eq(Vec2i rhs) {
		return new Vec2b(x == rhs.x, y == rhs.y);
	}
	
	public Vec2b ne(Vec2i rhs) {
		return new Vec2b(x != rhs.x, y != rhs.y);
	}
	
	public Vec2b ge(Vec2i rhs) {
		return new Vec2b(x >= rhs.x, y >= rhs.y);
	}
	
	public Vec2b gt(Vec2i rhs) {
		return new Vec2b(x > rhs.x, y > rhs.y);
	}
	
	@Override
	public String toString() {
		return String.format("(%d, %d)", x, y);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec2i other = (Vec2i) obj;
		if (x != other.x) return false;
		if (y != other.y) return false;
		return true;
	}
	
}
