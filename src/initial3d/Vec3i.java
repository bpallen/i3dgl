package initial3d;

public class Vec3i {

	public static final Vec3i zero = new Vec3i(0, 0, 0);
	
	public static final Vec3i i = new Vec3i(1, 0, 0);
	public static final Vec3i j = new Vec3i(0, 1, 0);
	public static final Vec3i k = new Vec3i(0, 0, 1);
	
	public final int x;
	public final int y;
	public final int z;
	
	public Vec3i() {
		this(0, 0, 0);
	}
	
	public Vec3i(int x_, int y_, int z_) {
		x = x_;
		y = y_;
		z = z_;
	}
	
	public int get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec3i with(int i, int val) {
		switch (i) {
		case 0:
			return new Vec3i(val, y, z);
		case 1:
			return new Vec3i(x, val, z);
		case 2:
			return new Vec3i(x, y, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec3i withX(int x_) {
		return new Vec3i(x_, y, z);
	}
	
	public Vec3i withY(int y_) {
		return new Vec3i(x, y_, z);
	}
	
	public Vec3i withZ(int z_) {
		return new Vec3i(x, y, z_);
	}
	
	public Vec2i xy() {
		return new Vec2i(x, y);
	}

	public Vec3i neg() {
		return new Vec3i(-x, -y, -z);
	}

	public Vec3i add(Vec3i rhs) {
		return new Vec3i(x + rhs.x, y + rhs.y, z + rhs.z);
	}
	
	public Vec3i add(int rhs) {
		return new Vec3i(x + rhs, y + rhs, z + rhs);
	}
	
	public Vec3i sub(Vec3i rhs) {
		return new Vec3i(x - rhs.x, y - rhs.y, z - rhs.z);
	}
	
	public Vec3i sub(int rhs) {
		return new Vec3i(x - rhs, y - rhs, z - rhs);
	}
	
	public Vec3i mul(Vec3i rhs) {
		return new Vec3i(x * rhs.x, y * rhs.y, z * rhs.z);
	}
	
	public Vec3i mul(int rhs) {
		return new Vec3i(x * rhs, y * rhs, z * rhs);
	}
	
	public Vec3i div(Vec3i rhs) {
		return new Vec3i(x / rhs.x, y / rhs.y, z / rhs.z);
	}
	
	public Vec3i div(int rhs) {
		return new Vec3i(x / rhs, y / rhs, z / rhs);
	}

	public Vec3i bitnot() {
		return new Vec3i(~x, ~y, ~z);
	}
	
	public Vec3i bitand(Vec3i rhs) {
		return new Vec3i(x & rhs.x, y & rhs.y, z & rhs.z);
	}
	
	public Vec3i bitor(Vec3i rhs) {
		return new Vec3i(x | rhs.x, y | rhs.y, z | rhs.z);
	}
	
	public Vec3b lt(Vec3i rhs) {
		return new Vec3b(x < rhs.x, y < rhs.y, z < rhs.z);
	}
	
	public Vec3b le(Vec3i rhs) {
		return new Vec3b(x <= rhs.x, y <= rhs.y, z <= rhs.z);
	}
	
	public Vec3b eq(Vec3i rhs) {
		return new Vec3b(x == rhs.x, y == rhs.y, z == rhs.z);
	}
	
	public Vec3b ne(Vec3i rhs) {
		return new Vec3b(x != rhs.x, y != rhs.y, z != rhs.z);
	}
	
	public Vec3b ge(Vec3i rhs) {
		return new Vec3b(x >= rhs.x, y >= rhs.y, z >= rhs.z);
	}
	
	public Vec3b gt(Vec3i rhs) {
		return new Vec3b(x > rhs.x, y > rhs.y, z > rhs.z);
	}
	
	@Override
	public String toString() {
		return String.format("(%d, %d, %d)", x, y ,z);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		result = prime * result + z;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec3i other = (Vec3i) obj;
		if (x != other.x) return false;
		if (y != other.y) return false;
		if (z != other.z) return false;
		return true;
	}
	
}
