package initial3d;

import java.awt.Graphics2D;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import initial3d.detail.gl.I3DGL;
import initial3d.detail.gl.OldShaderProgram;
import initial3d.detail.gl.WorkerPool;

import static initial3d.Functions.*;

public class GLUtil {

	public static final int GL_PROGRAM_CLASSNAME_I3D_value = 9001001;
	public static final int GL_PROGRAM_CLASSFILE_I3D_value = 9001002;
	
	private GLUtil() {
		throw new AssertionError("You're doing it wrong");
	}
	
	public static GL makeI3DGL(int w, int h, GLEnum depth_format) {
		initWorkers(1);
		I3DGL gl = new I3DGL(w, h, depth_format);
		gl.glViewport(0, 0, w, h);
		return gl;
	}
	
	public static void shutdown(GL gl) {
		((I3DGL) gl).driver().shutdown();
	}
	
	public static void resizeFramebuffer(GL gl, int w, int h) {
		((I3DGL) gl).resizeDefaultFramebuffer(w, h);
	}
	
	public static void blitFramebuffer(GL gl, Graphics2D g, int x, int y, int w, int h) {
		((I3DGL) gl).blitDefaultFramebuffer(g, x, y, w, h);
	}
	
	public static void initWorkers(int num_threads) {
		WorkerPool.init(num_threads);
	}
	
	@SafeVarargs
	public static <T> T[] array(T ...ts) {
		return ts;
	}
	
	public static ByteBuffer allocBuffer(int size) {
		ByteBuffer b = ByteBuffer.allocate(size);
		b.order(ByteOrder.nativeOrder());
		return b;
	}
	
	public static ByteBuffer buffer(String data) {
		ByteBuffer b = allocBuffer(data.length() * 2);
		b.asCharBuffer().put(data);
		return b;
	}
	
	public static ByteBuffer buffer(float ...data) {
		ByteBuffer b = allocBuffer(data.length * 4);
		b.asFloatBuffer().put(data);
		return b;
	}
	
	public static ByteBuffer buffer(int ...data) {
		ByteBuffer b = allocBuffer(data.length * 4);
		b.asIntBuffer().put(data);
		return b;
	}
	
	public static ByteBuffer buffer(Vec2f ...data) {
		ByteBuffer b = allocBuffer(data.length * 8);
		FloatBuffer f = b.asFloatBuffer();
		for (Vec2f v : data) {
			put(f, v);
		}
		return b;
	}

	public static ByteBuffer buffer(Vec3f ...data) {
		ByteBuffer b = allocBuffer(data.length * 12);
		FloatBuffer f = b.asFloatBuffer();
		for (Vec3f v : data) {
			put(f, v);
		}
		return b;
	}
	
	public static ByteBuffer buffer(Vec4f ...data) {
		ByteBuffer b = allocBuffer(data.length * 16);
		FloatBuffer f = b.asFloatBuffer();
		for (Vec4f v : data) {
			put(f, v);
		}
		return b;
	}
	
	public static ByteBuffer buffer(Vec2i ...data) {
		ByteBuffer b = allocBuffer(data.length * 8);
		IntBuffer f = b.asIntBuffer();
		for (Vec2i v : data) {
			put(f, v);
		}
		return b;
	}

	public static ByteBuffer buffer(Vec3i ...data) {
		ByteBuffer b = allocBuffer(data.length * 12);
		IntBuffer f = b.asIntBuffer();
		for (Vec3i v : data) {
			put(f, v);
		}
		return b;
	}
	
	public static ByteBuffer buffer(Vec4i ...data) {
		ByteBuffer b = allocBuffer(data.length * 16);
		IntBuffer f = b.asIntBuffer();
		for (Vec4i v : data) {
			put(f, v);
		}
		return b;
	}
	
	public static ByteBuffer buffer(Mat2f ...data) {
		ByteBuffer b = allocBuffer(data.length * 32);
		FloatBuffer f = b.asFloatBuffer();
		for (Mat2f v : data) {
			put(f, v);
		}
		return b;
	}

	public static ByteBuffer buffer(Mat3f ...data) {
		ByteBuffer b = allocBuffer(data.length * 48);
		FloatBuffer f = b.asFloatBuffer();
		for (Mat3f v : data) {
			put(f, v);
		}
		return b;
	}
	
	public static ByteBuffer buffer(Mat4f ...data) {
		ByteBuffer b = allocBuffer(data.length * 64);
		FloatBuffer f = b.asFloatBuffer();
		for (Mat4f v : data) {
			put(f, v);
		}
		return b;
	}
	
	public static void put(FloatBuffer buf, Vec2f v) {
		buf.put(v.x);
		buf.put(v.y);
	}
	
	public static void put(FloatBuffer buf, Vec3f v) {
		buf.put(v.x);
		buf.put(v.y);
		buf.put(v.z);
	}

	public static void put(FloatBuffer buf, Vec4f v) {
		buf.put(v.x);
		buf.put(v.y);
		buf.put(v.z);
		buf.put(v.w);
	}
	
	public static void put(IntBuffer buf, Vec2i v) {
		buf.put(v.x);
		buf.put(v.y);
	}
	
	public static void put(IntBuffer buf, Vec3i v) {
		buf.put(v.x);
		buf.put(v.y);
		buf.put(v.z);
	}

	public static void put(IntBuffer buf, Vec4i v) {
		buf.put(v.x);
		buf.put(v.y);
		buf.put(v.z);
		buf.put(v.w);
	}
	
	public static void put(FloatBuffer buf, Mat2f m) {
		for (int c = 0; c < 2; c++) {
			for (int r = 0; r < 2; r++) {
				buf.put(m.get(r, c));
			}
		}
	}
	
	public static void put(FloatBuffer buf, Mat3f m) {
		for (int c = 0; c < 3; c++) {
			for (int r = 0; r < 3; r++) {
				buf.put(m.get(r, c));
			}
		}
	}
	
	public static void put(FloatBuffer buf, Mat4f m) {
		for (int c = 0; c < 4; c++) {
			for (int r = 0; r < 4; r++) {
				buf.put(m.get(r, c));
			}
		}
	}
	
	public static Vec2f getVec2(FloatBuffer buf) {
		return vec2f(buf.get(), buf.get());
	}
	
	public static Vec3f getVec3(FloatBuffer buf) {
		return vec3f(buf.get(), buf.get(), buf.get());
	}
	
	public static Vec4f getVec4(FloatBuffer buf) {
		return vec4f(buf.get(), buf.get(), buf.get(), buf.get());
	}
	
	public static Vec2i getVec2(IntBuffer buf) {
		return vec2i(buf.get(), buf.get());
	}
	
	public static Vec3i getVec3(IntBuffer buf) {
		return vec3i(buf.get(), buf.get(), buf.get());
	}
	
	public static Vec4i getVec4(IntBuffer buf) {
		return vec4i(buf.get(), buf.get(), buf.get(), buf.get());
	}
	
	public static Mat2f getMat2(FloatBuffer buf) {
		Vec2f c0 = getVec2(buf);
		Vec2f c1 = getVec2(buf);
		return mat2f(c0, c1);
	}
	
	public static Mat3f getMat3(FloatBuffer buf) {
		Vec3f c0 = getVec3(buf);
		Vec3f c1 = getVec3(buf);
		Vec3f c2 = getVec3(buf);
		return mat3f(c0, c1, c2);
	}
	
	public static Mat4f getMat4(FloatBuffer buf) {
		Vec4f c0 = getVec4(buf);
		Vec4f c1 = getVec4(buf);
		Vec4f c2 = getVec4(buf);
		Vec4f c3 = getVec4(buf);
		return mat4f(c0, c1, c2, c3);
	}
	
	/**
	 * fovy in radians, aspect is w/h
	 * 
	 * @param fovy
	 * @param aspect
	 * @param zNear
	 * @param zFar
	 * @return
	 */
    public static Mat4f perspectiveProjectionf(float fovy, float aspect, float zNear, float zFar) {
		float f = 1.0f / (fovy / 2.0f);
		return mat4f(
			f / aspect, 0,  0,                                   0,
			0,          f,  0,                                   0,
			0,          0,  (zFar + zNear) / (zNear - zFar),     -1,
			0,          0,  (2 * zFar * zNear) / (zNear - zFar), 0
		);
    }

    public static Mat4f orthographicProjectionf(float left, float right, float bottom, float top, float nearVal, float farVal) {
		return mat4f(
			2 / (right - left),               0,                                0,                                        0,
			0,                                2 / (top - bottom),               0,                                        0,
			0,                                0,                                -2 / (farVal - nearVal),                  0,
			-(right + left) / (right - left), -(top + bottom) / (top - bottom), -(farVal + nearVal) / (farVal - nearVal), 1
		);
    }

}
