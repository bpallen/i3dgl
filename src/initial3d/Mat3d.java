package initial3d;

import java.util.Arrays;

import initial3d.detail.ArrayMath;
import initial3d.detail.StringUtil;

public class Mat3d {

	public static final Mat3d zero = new Mat3d();
	public static final Mat3d eye = new Mat3d(1);
	
	// column-major
	private final double[] e;

	private static int index(int r, int c) {
		return 3 * c + r;
	}
	
	private static int arraySize() {
		return 9;
	}
	
	public Mat3d() {
		e = new double[arraySize()];
	}

	public Mat3d(double x) {
		e = new double[] { x, 0, 0, 0, x, 0, 0, 0, x };
	}
	
	public Mat3d(
		double e00_, double e01_, double e02_,
		double e10_, double e11_, double e12_,
		double e20_, double e21_, double e22_
	) {
		e = new double[] { e00_, e01_, e02_, e10_, e11_, e12_, e20_, e21_, e22_ };
	}

	public double get(int r, int c) {
		return e[index(r, c)];
	}

	public Mat3d with(int r, int c, double val) {
		Mat3d m = new Mat3d();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(r, c)] = val;
		return m;
	}

	public Vec3d col(int c) {
		return new Vec3d(get(0, c), get(1, c), get(2, c));
	}

	public Vec3d row(int r) {
		return new Vec3d(get(r, 0), get(r, 1), get(r, 2));
	}

	public Mat3d withCol(int c, Vec3d v) {
		Mat3d m = new Mat3d();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(0, c)] = v.x;
		m.e[index(1, c)] = v.y;
		m.e[index(2, c)] = v.z;
		return m;
	}

	public Mat3d withRow(int r, Vec3d v) {
		Mat3d m = new Mat3d();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(r, 0)] = v.x;
		m.e[index(r, 1)] = v.y;
		m.e[index(r, 2)] = v.z;
		return m;
	}

	public Mat3d add(Mat3d rhs) {
		Mat3d m = new Mat3d();
		ArrayMath.add(arraySize(), m.e, 0, e, 0, rhs.e, 0);
		return m;
	}

	public Mat3d add(double rhs) {
		Mat3d m = new Mat3d();
		ArrayMath.add(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat3d sub(Mat3d rhs) {
		Mat3d m = new Mat3d();
		ArrayMath.sub(arraySize(), m.e, 0, e, 0, rhs.e, 0);
		return m;
	}

	public Mat3d sub(double rhs) {
		Mat3d m = new Mat3d();
		ArrayMath.sub(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat3d mul(double rhs) {
		Mat3d m = new Mat3d();
		ArrayMath.mul(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat3d div(double rhs) {
		Mat3d m = new Mat3d();
		ArrayMath.div(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat3d mul(Mat3d rhs) {
		Mat3d m = new Mat3d();
		double r;
		double t0;
		double t1;
		double t2;
		r = rhs.get(0, 0);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		r = rhs.get(1, 0);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		r = rhs.get(2, 0);
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		m.e[index(0, 0)] = t0;
		m.e[index(1, 0)] = t1;
		m.e[index(2, 0)] = t2;
		r = rhs.get(0, 1);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		r = rhs.get(1, 1);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		r = rhs.get(2, 1);
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		m.e[index(0, 1)] = t0;
		m.e[index(1, 1)] = t1;
		m.e[index(2, 1)] = t2;
		r = rhs.get(0, 2);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		r = rhs.get(1, 2);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		r = rhs.get(2, 2);
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		m.e[index(0, 2)] = t0;
		m.e[index(1, 2)] = t1;
		m.e[index(2, 2)] = t2;
		return m;
	}

	public Vec3d mul(Vec3d rhs) {
		double r;
		double t0;
		double t1;
		double t2;
		r = rhs.x;
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		r = rhs.y;
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		r = rhs.z;
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		return new Vec3d(t0, t1, t2);
	}

	@Override
	public String toString() {
		return StringUtil.matrixToString(e, 3);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(e);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Mat3d other = (Mat3d) obj;
		if (!Arrays.equals(e, other.e)) return false;
		return true;
	}
	
}
