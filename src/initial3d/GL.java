package initial3d;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public interface GL {

	public static interface DebugProc {
		public void execute(GLEnum source, GLEnum type, int id, GLEnum severity, String message, Object userparam);
	}
	
	public GLEnum glGetError();
	
	public void glEnable(GLEnum cap);
	
	public void glDisable(GLEnum cap);
	
	public boolean glIsEnabled(GLEnum cap);
	
	public void glViewport(int x, int y, int w, int h);
	
	public void glClearColor(float red, float green, float blue, float alpha);
	
	public void glClearDepth(float depth);
	
	public void glPointSize(float size);
	
	public void glLineWidth(float width);
	
	public void glPolygonMode(GLEnum face, GLEnum mode);
	
	public void glCullFace(GLEnum mode);
	
	public void glDebugMessageCallback(DebugProc callback, Object userparam);
	
	public int glGenBuffer();
	
	public void glDeleteBuffer(int name);
	
	public void glBindBuffer(GLEnum target, int name);
	
	public void glBufferData(GLEnum target, int size, ByteBuffer data, GLEnum usage);
	
	public void glBufferSubData(GLEnum target, int offset, int size, ByteBuffer data);
	
	public ByteBuffer glMapBuffer(GLEnum target, GLEnum access);
	
	public boolean glUnmapBuffer(GLEnum target);
	
	public int glGenVertexArray();
	
	public void glDeleteVertexArray(int name);
	
	public void glBindVertexArray(int name);
	
	public void glVertexAttribPointer(int index, int size, GLEnum type, boolean normalized, int stride, int offset);
	
	public void glVertexAttribIPointer(int index, int size, GLEnum type, int stride, int offset);
	
	public void glEnableVertexAttribArray(int index);
	
	public void glDisableVertexAttribArray(int index);
	
	public int glGenTexture();
	
	public void glDeleteTexture(int name);
	
	public void glBindTexture(GLEnum target, int texture);
	
	public void glActiveTexture(GLEnum texture);
	
	public void glTexImage1D(GLEnum target, int level, GLEnum internal_format, int width, int border, GLEnum format, GLEnum type, ByteBuffer data);
	
	public void glTexImage2D(GLEnum target, int level, GLEnum internal_format, int width, int height, int border, GLEnum format, GLEnum type, ByteBuffer data);
	
	public void glTexImage3D(GLEnum target, int level, GLEnum internal_format, int width, int height, int depth, int border, GLEnum format, GLEnum type, ByteBuffer data);
	
	public void glTexParameteri(GLEnum target, GLEnum pname, int param);
	
	public void glTexParameterf(GLEnum target, GLEnum pname, float param);
	
	public void glGenerateMipmap(GLEnum target);
	
	public int glGenFramebuffer();
	
	public void glDeleteFramebuffer(int name);
	
	public void glBindFramebuffer(GLEnum target, int framebuffer);
	
	public void glDrawBuffers(GLEnum ...bufs);
	
	public void glFramebufferTexture(GLEnum target, GLEnum attachment, int texture, int level);
	
	public void glDepthFunc(GLEnum func);
	
	public void glClear(GLBitfield mask);
	
	public void glBlendColor(float red, float green, float blue, float alpha);
	
	public void glBlendEquation(GLEnum mode);
	
	public void glBlendEquationSeparate(GLEnum mode_rgb, GLEnum mode_alpha);
	
	public void glBlendFunc(GLEnum sfactor, GLEnum dfactor);
	
	public void glBlendFuncSeparate(GLEnum sfactor_rgb, GLEnum dfactor_rgb, GLEnum sfactor_alpha, GLEnum dfactor_alpha);
	
	public void glDrawArrays(GLEnum mode, int first, int count);
	
	public void glDrawElements(GLEnum mode, int count, GLEnum type, int offset);
	
	public void glFlush();
	
	public void glFinish();
	
	public int glFenceSync(GLEnum condition, GLBitfield flags);
	
	public void glDeleteSync(int sync);
	
	public void glWaitSync(int sync, GLBitfield flags, long timeout);
	
	public GLEnum glClientWaitSync(int sync, GLBitfield flags, long timeout);
	
	public int glCreateShader(GLEnum shadertype);
	
	public void glDeleteShader(int shader);
	
	public void glShaderSource(int shader, String ...strings);
	
	public void glCompileShader(int shader);
	
	public void glGetShader(int shader, GLEnum pname, IntBuffer params);
	
	public void glGetShaderInfoLog(int shader, CharBuffer infolog);
	
	public int glCreateProgram();
	
	public void glDeleteProgram(int program);
	
	public void glAttachShader(int program, int shader);
	
	public void glDetachShader(int program, int shader);
	
	public void glLinkProgram(int program);
	
	public void glGetProgram(int program, GLEnum pname, IntBuffer params);
	
	public void glGetProgramInfoLog(int program, CharBuffer infolog);
	
	public void glUseProgram(int program);
	
	public void glProgramBinary(int program, int binaryformat, ByteBuffer binary, int length);
	
	public int glGetUniformLocation(int program, String name);
	
	public void glUniform1fv(int location, int count, FloatBuffer value);
	
	public void glUniform2fv(int location, int count, FloatBuffer value);
	
	public void glUniform3fv(int location, int count, FloatBuffer value);
	
	public void glUniform4fv(int location, int count, FloatBuffer value);
	
	public void glUniform1iv(int location, int count, IntBuffer value);
	
	public void glUniform2iv(int location, int count, IntBuffer value);
	
	public void glUniform3iv(int location, int count, IntBuffer value);
	
	public void glUniform4iv(int location, int count, IntBuffer value);
	
	public void glUniform1uiv(int location, int count, IntBuffer value);
	
	public void glUniform2uiv(int location, int count, IntBuffer value);
	
	public void glUniform3uiv(int location, int count, IntBuffer value);
	
	public void glUniform4uiv(int location, int count, IntBuffer value);
	
	public void glUniformMatrix2fv(int location, int count, boolean transpose, FloatBuffer value);
	
	public void glUniformMatrix3fv(int location, int count, boolean transpose, FloatBuffer value);
	
	public void glUniformMatrix4fv(int location, int count, boolean transpose, FloatBuffer value);

	default void glUniform1f(int location, float v0) {
		glUniform1fv(location, 1, GLUtil.buffer(v0).asFloatBuffer());
	}
	
	default void glUniform2f(int location, float v0, float v1) {
		glUniform2fv(location, 1, GLUtil.buffer(v0, v1).asFloatBuffer());
	}
	
	default void glUniform2f(int location, Vec2f v) {
		glUniform2f(location, v.x, v.y);
	}
	
	default void glUniform3f(int location, float v0, float v1, float v2) {
		glUniform3fv(location, 1, GLUtil.buffer(v0, v1, v2).asFloatBuffer());
	}
	
	default void glUniform3f(int location, Vec3f v) {
		glUniform3f(location, v.x, v.y, v.z);
	}
	
	default void glUniform4f(int location, float v0, float v1, float v2, float v3) {
		glUniform4fv(location, 1, GLUtil.buffer(v0, v1, v2, v3).asFloatBuffer());
	}
	
	default void glUniform4f(int location, Vec4f v) {
		glUniform4f(location, v.x, v.y, v.z, v.w);
	}
	
	default void glUniform1i(int location, int v0) {
		glUniform1iv(location, 1, GLUtil.buffer(v0).asIntBuffer());
	}
	
	default void glUniform2i(int location, int v0, int v1) {
		glUniform2iv(location, 1, GLUtil.buffer(v0, v1).asIntBuffer());
	}
	
	default void glUniform2i(int location, Vec2i v) {
		glUniform2i(location, v.x, v.y);
	}
	
	default void glUniform3i(int location, int v0, int v1, int v2) {
		glUniform3iv(location, 1, GLUtil.buffer(v0, v1, v2).asIntBuffer());
	}
	
	default void glUniform3i(int location, Vec3i v) {
		glUniform3i(location, v.x, v.y, v.z);
	}
	
	default void glUniform4i(int location, int v0, int v1, int v2, int v3) {
		glUniform4iv(location, 1, GLUtil.buffer(v0, v1, v2, v3).asIntBuffer());
	}
	
	default void glUniform4i(int location, Vec4i v) {
		glUniform4i(location, v.x, v.y, v.z, v.w);
	}
	
	default void glUniform1ui(int location, int v0) {
		glUniform1uiv(location, 1, GLUtil.buffer(v0).asIntBuffer());
	}
	
	default void glUniform2ui(int location, int v0, int v1) {
		glUniform2uiv(location, 1, GLUtil.buffer(v0, v1).asIntBuffer());
	}
	
	default void glUniform3ui(int location, int v0, int v1, int v2) {
		glUniform3uiv(location, 1, GLUtil.buffer(v0, v1, v2).asIntBuffer());
	}
	
	default void glUniform4ui(int location, int v0, int v1, int v2, int v3) {
		glUniform4uiv(location, 1, GLUtil.buffer(v0, v1, v2, v3).asIntBuffer());
	}
	
	default void glUniformMatrix2f(int location, boolean transpose, Mat2f value) {
		glUniformMatrix2fv(location, 1, transpose, GLUtil.buffer(value).asFloatBuffer());
	}
	
	default void glUniformMatrix3f(int location, boolean transpose, Mat3f value) {
		glUniformMatrix3fv(location, 1, transpose, GLUtil.buffer(value).asFloatBuffer());
	}
	
	default void glUniformMatrix4f(int location, boolean transpose, Mat4f value) {
		glUniformMatrix4fv(location, 1, transpose, GLUtil.buffer(value).asFloatBuffer());
	}
	
}







