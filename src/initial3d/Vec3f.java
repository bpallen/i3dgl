package initial3d;

import initial3d.detail.StringUtil;

public class Vec3f {

	public static final Vec3f zero = new Vec3f(0, 0, 0);
	
	public static final Vec3f i = new Vec3f(1, 0, 0);
	public static final Vec3f j = new Vec3f(0, 1, 0);
	public static final Vec3f k = new Vec3f(0, 0, 1);
	
	public final float x;
	public final float y;
	public final float z;
	
	public Vec3f() {
		this(0, 0, 0);
	}
	
	public Vec3f(float x_, float y_, float z_) {
		x = x_;
		y = y_;
		z = z_;
	}
	
	public float get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec3f with(int i, float val) {
		switch (i) {
		case 0:
			return new Vec3f(val, y, z);
		case 1:
			return new Vec3f(x, val, z);
		case 2:
			return new Vec3f(x, y, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec3f withX(float x_) {
		return new Vec3f(x_, y, z);
	}
	
	public Vec3f withY(float y_) {
		return new Vec3f(x, y_, z);
	}
	
	public Vec3f withZ(float z_) {
		return new Vec3f(x, y, z_);
	}
	
	public Vec2f xy() {
		return new Vec2f(x, y);
	}

	public Vec3f neg() {
		return new Vec3f(-x, -y, -z);
	}

	public Vec3f add(Vec3f rhs) {
		return new Vec3f(x + rhs.x, y + rhs.y, z + rhs.z);
	}
	
	public Vec3f add(float rhs) {
		return new Vec3f(x + rhs, y + rhs, z + rhs);
	}
	
	public Vec3f sub(Vec3f rhs) {
		return new Vec3f(x - rhs.x, y - rhs.y, z - rhs.z);
	}
	
	public Vec3f sub(float rhs) {
		return new Vec3f(x - rhs, y - rhs, z - rhs);
	}
	
	public Vec3f mul(Vec3f rhs) {
		return new Vec3f(x * rhs.x, y * rhs.y, z * rhs.z);
	}
	
	public Vec3f mul(float rhs) {
		return new Vec3f(x * rhs, y * rhs, z * rhs);
	}
	
	public Vec3f div(Vec3f rhs) {
		return new Vec3f(x / rhs.x, y / rhs.y, z / rhs.z);
	}
	
	public Vec3f div(float rhs) {
		return new Vec3f(x / rhs, y / rhs, z / rhs);
	}
	
	public Vec3b lt(Vec3f rhs) {
		return new Vec3b(x < rhs.x, y < rhs.y, z < rhs.z);
	}
	
	public Vec3b le(Vec3f rhs) {
		return new Vec3b(x <= rhs.x, y <= rhs.y, z <= rhs.z);
	}
	
	public Vec3b eq(Vec3f rhs) {
		return new Vec3b(x == rhs.x, y == rhs.y, z == rhs.z);
	}
	
	public Vec3b ne(Vec3f rhs) {
		return new Vec3b(x != rhs.x, y != rhs.y, z != rhs.z);
	}
	
	public Vec3b ge(Vec3f rhs) {
		return new Vec3b(x >= rhs.x, y >= rhs.y, z >= rhs.z);
	}
	
	public Vec3b gt(Vec3f rhs) {
		return new Vec3b(x > rhs.x, y > rhs.y, z > rhs.z);
	}
	
	@Override
	public String toString() {
		final int prec = 5;
		return String.format(
			"(%s, %s, %s)",
			StringUtil.formatFloat(x, prec),
			StringUtil.formatFloat(y, prec),
			StringUtil.formatFloat(z, prec)
		);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		result = prime * result + Float.floatToIntBits(z);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec3f other = (Vec3f) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x)) return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y)) return false;
		if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z)) return false;
		return true;
	}
	
}
