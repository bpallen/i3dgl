package initial3d;

import static initial3d.Functions.*;

import initial3d.detail.StringUtil;

public class Quatf {
	public static final Quatf zero = new Quatf(0, 0, 0, 0);
    public static final Quatf one = new Quatf(1, 0, 0, 0);
    
    public static final Quatf i = new Quatf(1, 0, 0, 0);
    public static final Quatf j = new Quatf(0, 1, 0, 0);
    public static final Quatf k = new Quatf(0, 0, 1, 0);
 
    public final float w;
    public final float x;
    public final float y;
    public final float z;
    
    Quatf(float w_, float x_, float y_, float z_) {
    	w = w_;
        x = x_;
        y = y_;
        z = z_;
    }

    Quatf(Vec3f axis, float angle) {
        // angle(+-2*PI) <=> w(-1) and angle(0) <=> w(1)
        axis = normalize(axis);
        float sin_a = (float) Math.sin(angle * 0.5f);
        w = (float) Math.cos(angle * 0.5);
        x = axis.x * sin_a;
        y = axis.y * sin_a;
        z = axis.z * sin_a;
    }

    Quatf(Vec3f rot) {
        if (Float.isInfinite(1/norm(rot))) {
            w = 1;
            x = 0;
            y = 0;
            z = 0;
        } else {
            float angle = norm(rot);
            Vec3f axis = normalize(rot);
            // angle(+-2*PI) <=> w(-1) and angle(0) <=> w(1)
            float sin_a = (float) Math.sin(angle * 0.5);
            w = (float) Math.cos(angle * 0.5);
            x = axis.x * sin_a;
            y = axis.y * sin_a;
            z = axis.z * sin_a;
        }
    }
    
    public Quatf add(Quatf rhs) {
        return new Quatf(w + rhs.w, x + rhs.x, y + rhs.y, z + rhs.z);
    }

    public Quatf sub(Quatf rhs) {
        return new Quatf(w - rhs.w, x - rhs.x, y - rhs.y, z - rhs.z);
    }
    
    public Quatf mul(float f) {
        return new Quatf(w * f, x * f, y * f, z * f);
    }

    public Quatf mul(Quatf rhs) {
        return new Quatf(w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z, w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y, w
                * rhs.y - x * rhs.z + y * rhs.w + z * rhs.x, w * rhs.z + x * rhs.y - y * rhs.x + z * rhs.w);
    }

    public Quatf mul(float rightw, float rightx, float righty, float rightz) {
        return new Quatf(w * rightw - x * rightx - y * righty - z * rightz, w * rightx + x * rightw + y * rightz - z
                * righty, w * righty - x * rightz + y * rightw + z * rightx, w * rightz + x * righty - y * rightx + z
                * rightw);
    }
    
    public Vec3f mul(Vec3f v) {
        // this * v * this^(-1)
        Quatf q = this.mul(0, v.x, v.y, v.z).mul(conjugate(this));
        return new Vec3f(q.x, q.y, q.z);
    }

    public Quatf lmul(Quatf lhs) {
        return new Quatf(lhs.w * w - lhs.x * x - lhs.y * y - lhs.z * z, lhs.w * x + lhs.x * w + lhs.y * z - lhs.z * y,
                lhs.w * y - lhs.x * z + lhs.y * w + lhs.z * x, lhs.w * z + lhs.x * y - lhs.y * x + lhs.z * w);
    }

    public Quatf lmul(float leftw, float leftx, float lefty, float leftz) {
        return new Quatf(leftw * w - leftx * x - lefty * y - leftz * z, leftw * x + leftx * w + lefty * z - leftz * y,
                leftw * y - leftx * z + lefty * w + leftz * x, leftw * z + leftx * y - lefty * x + leftz * w);
    }

	public String toString() {
		final int prec = 5;
		return String.format(
			"(%s, %s, %s, %s)",
			StringUtil.formatFloat(w, prec), StringUtil.formatFloat(x, prec),
			StringUtil.formatFloat(y, prec), StringUtil.formatFloat(z, prec)
		);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Float.floatToIntBits(w);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Float.floatToIntBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Float.floatToIntBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Float.floatToIntBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec4f other = (Vec4f) obj;
		if (Float.floatToIntBits(w) != Float.floatToIntBits(other.w)) return false;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x)) return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y)) return false;
		if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z)) return false;
		return true;
	}
}
