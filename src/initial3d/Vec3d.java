package initial3d;

import initial3d.detail.StringUtil;

public class Vec3d {

	public static final Vec3d zero = new Vec3d(0, 0, 0);
	
	public static final Vec3d i = new Vec3d(1, 0, 0);
	public static final Vec3d j = new Vec3d(0, 1, 0);
	public static final Vec3d k = new Vec3d(0, 0, 1);
	
	public final double x;
	public final double y;
	public final double z;
	
	public Vec3d() {
		this(0, 0, 0);
	}
	
	public Vec3d(double x_, double y_, double z_) {
		x = x_;
		y = y_;
		z = z_;
	}
	
	public double get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec3d with(int i, double val) {
		switch (i) {
		case 0:
			return new Vec3d(val, y, z);
		case 1:
			return new Vec3d(x, val, z);
		case 2:
			return new Vec3d(x, y, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec3d withX(double x_) {
		return new Vec3d(x_, y, z);
	}
	
	public Vec3d withY(double y_) {
		return new Vec3d(x, y_, z);
	}
	
	public Vec3d withZ(double z_) {
		return new Vec3d(x, y, z_);
	}
	
	public Vec2d xy() {
		return new Vec2d(x, y);
	}

	public Vec3d neg() {
		return new Vec3d(-x, -y, -z);
	}

	public Vec3d add(Vec3d rhs) {
		return new Vec3d(x + rhs.x, y + rhs.y, z + rhs.z);
	}
	
	public Vec3d add(double rhs) {
		return new Vec3d(x + rhs, y + rhs, z + rhs);
	}
	
	public Vec3d sub(Vec3d rhs) {
		return new Vec3d(x - rhs.x, y - rhs.y, z - rhs.z);
	}
	
	public Vec3d sub(double rhs) {
		return new Vec3d(x - rhs, y - rhs, z - rhs);
	}
	
	public Vec3d mul(Vec3d rhs) {
		return new Vec3d(x * rhs.x, y * rhs.y, z * rhs.z);
	}
	
	public Vec3d mul(double rhs) {
		return new Vec3d(x * rhs, y * rhs, z * rhs);
	}
	
	public Vec3d div(Vec3d rhs) {
		return new Vec3d(x / rhs.x, y / rhs.y, z / rhs.z);
	}
	
	public Vec3d div(double rhs) {
		return new Vec3d(x / rhs, y / rhs, z / rhs);
	}
	
	public Vec3b lt(Vec3d rhs) {
		return new Vec3b(x < rhs.x, y < rhs.y, z < rhs.z);
	}
	
	public Vec3b le(Vec3d rhs) {
		return new Vec3b(x <= rhs.x, y <= rhs.y, z <= rhs.z);
	}
	
	public Vec3b eq(Vec3d rhs) {
		return new Vec3b(x == rhs.x, y == rhs.y, z == rhs.z);
	}
	
	public Vec3b ne(Vec3d rhs) {
		return new Vec3b(x != rhs.x, y != rhs.y, z != rhs.z);
	}
	
	public Vec3b ge(Vec3d rhs) {
		return new Vec3b(x >= rhs.x, y >= rhs.y, z >= rhs.z);
	}
	
	public Vec3b gt(Vec3d rhs) {
		return new Vec3b(x > rhs.x, y > rhs.y, z > rhs.z);
	}
	
	@Override
	public String toString() {
		final int prec = 5;
		return String.format(
			"(%s, %s, %s)",
			StringUtil.formatDouble(x, prec),
			StringUtil.formatDouble(y, prec),
			StringUtil.formatDouble(z, prec)
		);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec3d other = (Vec3d) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) return false;
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z)) return false;
		return true;
	}
	
}
