package initial3d;

import initial3d.detail.StringUtil;

public class Vec2f {

	public static final Vec2f zero = new Vec2f(0, 0);
	
	public static final Vec2f i = new Vec2f(1, 0);
	public static final Vec2f j = new Vec2f(0, 1);

	public final float x;
	public final float y;

	public Vec2f() {
		this(0, 0);
	}

	public Vec2f(float x_, float y_) {
		x = x_;
		y = y_;
	}

	public float get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec2f with(int i, float val) {
		switch (i) {
		case 0:
			return new Vec2f(val, y);
		case 1:
			return new Vec2f(x, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec2f withX(float x_) {
		return new Vec2f(x_, y);
	}
	
	public Vec2f withY(float y_) {
		return new Vec2f(x, y_);
	}

	public Vec2f neg() {
		return new Vec2f(-x, -y);
	}

	public Vec2f add(Vec2f rhs) {
		return new Vec2f(x + rhs.x, y + rhs.y);
	}

	public Vec2f add(float rhs) {
		return new Vec2f(x + rhs, y + rhs);
	}

	public Vec2f sub(Vec2f rhs) {
		return new Vec2f(x - rhs.x, y - rhs.y);
	}

	public Vec2f sub(float rhs) {
		return new Vec2f(x - rhs, y - rhs);
	}

	public Vec2f mul(Vec2f rhs) {
		return new Vec2f(x * rhs.x, y * rhs.y);
	}

	public Vec2f mul(float rhs) {
		return new Vec2f(x * rhs, y * rhs);
	}

	public Vec2f div(Vec2f rhs) {
		return new Vec2f(x / rhs.x, y / rhs.y);
	}

	public Vec2f div(float rhs) {
		return new Vec2f(x / rhs, y / rhs);
	}

	public Vec2b lt(Vec2f rhs) {
		return new Vec2b(x < rhs.x, y < rhs.y);
	}
	
	public Vec2b le(Vec2f rhs) {
		return new Vec2b(x <= rhs.x, y <= rhs.y);
	}
	
	public Vec2b eq(Vec2f rhs) {
		return new Vec2b(x == rhs.x, y == rhs.y);
	}
	
	public Vec2b ne(Vec2f rhs) {
		return new Vec2b(x != rhs.x, y != rhs.y);
	}
	
	public Vec2b ge(Vec2f rhs) {
		return new Vec2b(x >= rhs.x, y >= rhs.y);
	}
	
	public Vec2b gt(Vec2f rhs) {
		return new Vec2b(x > rhs.x, y > rhs.y);
	}
	
	@Override
	public String toString() {
		final int prec = 5;
		return String.format(
			"(%s, %s)",
			StringUtil.formatFloat(x, prec),
			StringUtil.formatFloat(y, prec)
		);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec2f other = (Vec2f) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x)) return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y)) return false;
		return true;
	}
	
}
