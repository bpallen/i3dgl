package initial3d;

import java.util.Arrays;

import initial3d.detail.ArrayMath;
import initial3d.detail.StringUtil;

public class Mat3f {

	public static final Mat3f zero = new Mat3f();
	public static final Mat3f eye = new Mat3f(1);
	
	// column-major
	private final float[] e;

	private static int index(int r, int c) {
		return 3 * c + r;
	}
	
	private static int arraySize() {
		return 9;
	}
	
	public Mat3f() {
		e = new float[arraySize()];
	}

	public Mat3f(float x) {
		e = new float[] { x, 0, 0, 0, x, 0, 0, 0, x };
	}
	
	public Mat3f(
		float e00_, float e01_, float e02_,
		float e10_, float e11_, float e12_,
		float e20_, float e21_, float e22_
	) {
		e = new float[] { e00_, e01_, e02_, e10_, e11_, e12_, e20_, e21_, e22_ };
	}

	public float get(int r, int c) {
		return e[index(r, c)];
	}

	public Mat3f with(int r, int c, float val) {
		Mat3f m = new Mat3f();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(r, c)] = val;
		return m;
	}

	public Vec3f col(int c) {
		return new Vec3f(get(0, c), get(1, c), get(2, c));
	}

	public Vec3f row(int r) {
		return new Vec3f(get(r, 0), get(r, 1), get(r, 2));
	}

	public Mat3f withCol(int c, Vec3f v) {
		Mat3f m = new Mat3f();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(0, c)] = v.x;
		m.e[index(1, c)] = v.y;
		m.e[index(2, c)] = v.z;
		return m;
	}

	public Mat3f withRow(int r, Vec3f v) {
		Mat3f m = new Mat3f();
		System.arraycopy(e, 0, m.e, 0, arraySize());
		m.e[index(r, 0)] = v.x;
		m.e[index(r, 1)] = v.y;
		m.e[index(r, 2)] = v.z;
		return m;
	}

	public Mat3f add(Mat3f rhs) {
		Mat3f m = new Mat3f();
		ArrayMath.add(arraySize(), m.e, 0, e, 0, rhs.e, 0);
		return m;
	}

	public Mat3f add(float rhs) {
		Mat3f m = new Mat3f();
		ArrayMath.add(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat3f sub(Mat3f rhs) {
		Mat3f m = new Mat3f();
		ArrayMath.sub(arraySize(), m.e, 0, e, 0, rhs.e, 0);
		return m;
	}

	public Mat3f sub(float rhs) {
		Mat3f m = new Mat3f();
		ArrayMath.sub(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat3f mul(float rhs) {
		Mat3f m = new Mat3f();
		ArrayMath.mul(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat3f div(float rhs) {
		Mat3f m = new Mat3f();
		ArrayMath.div(arraySize(), m.e, 0, e, 0, rhs);
		return m;
	}

	public Mat3f mul(Mat3f rhs) {
		Mat3f m = new Mat3f();
		float r;
		float t0;
		float t1;
		float t2;
		r = rhs.get(0, 0);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		r = rhs.get(1, 0);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		r = rhs.get(2, 0);
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		m.e[index(0, 0)] = t0;
		m.e[index(1, 0)] = t1;
		m.e[index(2, 0)] = t2;
		r = rhs.get(0, 1);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		r = rhs.get(1, 1);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		r = rhs.get(2, 1);
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		m.e[index(0, 1)] = t0;
		m.e[index(1, 1)] = t1;
		m.e[index(2, 1)] = t2;
		r = rhs.get(0, 2);
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		r = rhs.get(1, 2);
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		r = rhs.get(2, 2);
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		m.e[index(0, 2)] = t0;
		m.e[index(1, 2)] = t1;
		m.e[index(2, 2)] = t2;
		return m;
	}

	public Vec3f mul(Vec3f rhs) {
		float r;
		float t0;
		float t1;
		float t2;
		r = rhs.x;
		t0 = this.get(0, 0) * r;
		t1 = this.get(1, 0) * r;
		t2 = this.get(2, 0) * r;
		r = rhs.y;
		t0 += this.get(0, 1) * r;
		t1 += this.get(1, 1) * r;
		t2 += this.get(2, 1) * r;
		r = rhs.z;
		t0 += this.get(0, 2) * r;
		t1 += this.get(1, 2) * r;
		t2 += this.get(2, 2) * r;
		return new Vec3f(t0, t1, t2);
	}

	@Override
	public String toString() {
		return StringUtil.matrixToString(e, 3);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(e);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Mat3f other = (Mat3f) obj;
		if (!Arrays.equals(e, other.e)) return false;
		return true;
	}
	
}
