package initial3d;

public class Vec3b {

	public static final Vec3b zero = new Vec3b(false, false, false);
	
	public static final Vec3b i = new Vec3b(true, false, false);
	public static final Vec3b j = new Vec3b(false, true, false);
	public static final Vec3b k = new Vec3b(false, false, true);
	
	public final boolean x;
	public final boolean y;
	public final boolean z;
	
	public Vec3b() {
		this(false, false, false);
	}
	
	public Vec3b(boolean x_, boolean y_, boolean z_) {
		x = x_;
		y = y_;
		z = z_;
	}
	
	public boolean get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec3b with(int i, boolean val) {
		switch (i) {
		case 0:
			return new Vec3b(val, y, z);
		case 1:
			return new Vec3b(x, val, z);
		case 2:
			return new Vec3b(x, y, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec3b withX(boolean x_) {
		return new Vec3b(x_, y, z);
	}
	
	public Vec3b withY(boolean y_) {
		return new Vec3b(x, y_, z);
	}
	
	public Vec3b withZ(boolean z_) {
		return new Vec3b(x, y, z_);
	}
	
	public Vec2b xy() {
		return new Vec2b(x, y);
	}

	public Vec3b not() {
		return new Vec3b(!x, !y, !z);
	}
	
	public Vec3b and(Vec3b rhs) {
		return new Vec3b(x && rhs.x, y && rhs.y, z && rhs.z);
	}
	
	public Vec3b or(Vec3b rhs) {
		return new Vec3b(x || rhs.x, y || rhs.y, z || rhs.z);
	}
	
	public Vec3b eq(Vec3b rhs) {
		return new Vec3b(x == rhs.x, y == rhs.y, z == rhs.z);
	}
	
	public Vec3b ne(Vec3b rhs) {
		return new Vec3b(x != rhs.x, y != rhs.y, z != rhs.z);
	}
	
	@Override
	public String toString() {
		return String.format("(%b, %b, %b)", x, y ,z);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (x ? 1231 : 1237);
		result = prime * result + (y ? 1231 : 1237);
		result = prime * result + (z ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec3b other = (Vec3b) obj;
		if (x != other.x) return false;
		if (y != other.y) return false;
		if (z != other.z) return false;
		return true;
	}
	
}
