package initial3d;

public class GLBitfield {

	public static final GLBitfield ZERO = new GLBitfield(0);
	
	private final int bits;
	
	public GLBitfield(int bits_) {
		bits = bits_;
	}
	
	public GLBitfield(GLEnum ...es) {
		int x = 0;
		for (GLEnum e : es) {
			x |= e.valueInt();
		}
		bits = x;
	}
	
	public boolean testAll(GLEnum ...es) {
		int x = 0;
		for (GLEnum e : es) {
			x |= e.valueInt();
		}
		return (bits & x) == x;
	}
	
	public boolean testAny(GLEnum ...es) {
		int x = 0;
		for (GLEnum e : es) {
			x |= e.valueInt();
		}
		return (bits & x) != 0;
	}
	
	public int bits() {
		return bits;
	}
	
}
