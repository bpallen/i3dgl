package initial3d;

public class Vec4b {

	public static final Vec4b zero = new Vec4b(false, false, false, false);
	
	public static final Vec4b i = new Vec4b(true, false, false, false);
	public static final Vec4b j = new Vec4b(false, true, false, false);
	public static final Vec4b k = new Vec4b(false, false, true, false);

	public final boolean x;
	public final boolean y;
	public final boolean z;
	public final boolean w;
	
	public Vec4b() {
		this(false, false, false, false);
	}

	public Vec4b(boolean x_, boolean y_, boolean z_, boolean w_) {
		x = x_;
		y = y_;
		z = z_;
		w = w_;
	}

	public boolean get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		case 3:
			return w;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec4b with(int i, boolean val) {
		switch (i) {
		case 0:
			return new Vec4b(val, y, z, w);
		case 1:
			return new Vec4b(x, val, z, w);
		case 2:
			return new Vec4b(x, y, val, w);
		case 3:
			return new Vec4b(x, y, z, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec4b withX(boolean x_) {
		return new Vec4b(x_, y, z, w);
	}
	
	public Vec4b withY(boolean y_) {
		return new Vec4b(x, y_, z, w);
	}
	
	public Vec4b withZ(boolean z_) {
		return new Vec4b(x, y, z_, w);
	}

	public Vec4b withW(boolean w_) {
		return new Vec4b(x, y, z, w_);
	}
	
	public Vec2b xy() {
		return new Vec2b(x, y);
	}

	public Vec3b xyz() {
		return new Vec3b(x, y, z);
	}

	public Vec4b not() {
		return new Vec4b(!x, !y, !z, !w);
	}
	
	public Vec4b and(Vec4b rhs) {
		return new Vec4b(x && rhs.x, y && rhs.y, z && rhs.z, w && rhs.w);
	}
	
	public Vec4b or(Vec4b rhs) {
		return new Vec4b(x || rhs.x, y || rhs.y, z || rhs.z, w || rhs.w);
	}
	
	public Vec4b eq(Vec4b rhs) {
		return new Vec4b(x == rhs.x, y == rhs.y, z == rhs.z, w == rhs.w);
	}
	
	public Vec4b ne(Vec4b rhs) {
		return new Vec4b(x != rhs.x, y != rhs.y, z != rhs.z, w != rhs.w);
	}
	
	public String toString() {
		return String.format("(%b, %b, %b, %b)", x, y, z, w);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (w ? 1231 : 1237);
		result = prime * result + (x ? 1231 : 1237);
		result = prime * result + (y ? 1231 : 1237);
		result = prime * result + (z ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec4b other = (Vec4b) obj;
		if (w != other.w) return false;
		if (x != other.x) return false;
		if (y != other.y) return false;
		if (z != other.z) return false;
		return true;
	}
	
}
