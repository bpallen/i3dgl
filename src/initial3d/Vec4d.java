package initial3d;

import initial3d.detail.StringUtil;

public class Vec4d {

	public static final Vec4d zero = new Vec4d(0, 0, 0, 0);

	public static final Vec4d i = new Vec4d(1, 0, 0, 0);
	public static final Vec4d j = new Vec4d(0, 1, 0, 0);
	public static final Vec4d k = new Vec4d(0, 0, 1, 0);

	public final double x;
	public final double y;
	public final double z;
	public final double w;

	public Vec4d() {
		this(0, 0, 0, 0);
	}

	public Vec4d(double x_, double y_, double z_, double w_) {
		x = x_;
		y = y_;
		z = z_;
		w = w_;
	}

	public double get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		case 3:
			return w;
		default:
			throw new IndexOutOfBoundsException();
		}
	}

	public Vec4d with(int i, double val) {
		switch (i) {
		case 0:
			return new Vec4d(val, y, z, w);
		case 1:
			return new Vec4d(x, val, z, w);
		case 2:
			return new Vec4d(x, y, val, w);
		case 3:
			return new Vec4d(x, y, z, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}

	public Vec4d withX(double x_) {
		return new Vec4d(x_, y, z, w);
	}

	public Vec4d withY(double y_) {
		return new Vec4d(x, y_, z, w);
	}

	public Vec4d withZ(double z_) {
		return new Vec4d(x, y, z_, w);
	}

	public Vec4d withW(double w_) {
		return new Vec4d(x, y, z, w_);
	}

	public Vec2d xy() {
		return new Vec2d(x, y);
	}

	public Vec3d xyz() {
		return new Vec3d(x, y, z);
	}

	public Vec4d neg() {
		return new Vec4d(-x, -y, -z, -w);
	}

	public Vec4d add(Vec4d rhs) {
		return new Vec4d(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
	}

	public Vec4d add(double rhs) {
		return new Vec4d(x + rhs, y + rhs, z + rhs, w + rhs);
	}

	public Vec4d sub(Vec4d rhs) {
		return new Vec4d(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
	}

	public Vec4d sub(double rhs) {
		return new Vec4d(x - rhs, y - rhs, z - rhs, w - rhs);
	}

	public Vec4d mul(Vec4d rhs) {
		return new Vec4d(x * rhs.x, y * rhs.y, z * rhs.z, w * rhs.w);
	}

	public Vec4d mul(double rhs) {
		return new Vec4d(x * rhs, y * rhs, z * rhs, w * rhs);
	}

	public Vec4d div(Vec4d rhs) {
		return new Vec4d(x / rhs.x, y / rhs.y, z / rhs.z, w / rhs.w);
	}

	public Vec4d div(double rhs) {
		return new Vec4d(x / rhs, y / rhs, z / rhs, w / rhs);
	}

	public Vec4b lt(Vec4d rhs) {
		return new Vec4b(x < rhs.x, y < rhs.y, z < rhs.z, w < rhs.w);
	}

	public Vec4b le(Vec4d rhs) {
		return new Vec4b(x <= rhs.x, y <= rhs.y, z <= rhs.z, w <= rhs.w);
	}

	public Vec4b eq(Vec4d rhs) {
		return new Vec4b(x == rhs.x, y == rhs.y, z == rhs.z, w == rhs.w);
	}

	public Vec4b ne(Vec4d rhs) {
		return new Vec4b(x != rhs.x, y != rhs.y, z != rhs.z, w != rhs.w);
	}

	public Vec4b ge(Vec4d rhs) {
		return new Vec4b(x >= rhs.x, y >= rhs.y, z >= rhs.z, w >= rhs.w);
	}

	public Vec4b gt(Vec4d rhs) {
		return new Vec4b(x > rhs.x, y > rhs.y, z > rhs.z, w > rhs.w);
	}

	public String toString() {
		final int prec = 5;
		return String.format(
			"(%s, %s, %s, %s)",
			StringUtil.formatDouble(x, prec), StringUtil.formatDouble(y, prec),
			StringUtil.formatDouble(z, prec), StringUtil.formatDouble(w, prec)
		);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(w);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec4d other = (Vec4d) obj;
		if (Double.doubleToLongBits(w) != Double.doubleToLongBits(other.w)) return false;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) return false;
		if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z)) return false;
		return true;
	}

}
