package initial3d;

public class Vec4i {

	public static final Vec4i zero = new Vec4i(0, 0, 0, 0);
	
	public static final Vec4i i = new Vec4i(1, 0, 0, 0);
	public static final Vec4i j = new Vec4i(0, 1, 0, 0);
	public static final Vec4i k = new Vec4i(0, 0, 1, 0);

	public final int x;
	public final int y;
	public final int z;
	public final int w;
	
	public Vec4i() {
		this(0, 0, 0, 0);
	}

	public Vec4i(int x_, int y_, int z_, int w_) {
		x = x_;
		y = y_;
		z = z_;
		w = w_;
	}

	public int get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		case 3:
			return w;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec4i with(int i, int val) {
		switch (i) {
		case 0:
			return new Vec4i(val, y, z, w);
		case 1:
			return new Vec4i(x, val, z, w);
		case 2:
			return new Vec4i(x, y, val, w);
		case 3:
			return new Vec4i(x, y, z, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec4i withX(int x_) {
		return new Vec4i(x_, y, z, w);
	}
	
	public Vec4i withY(int y_) {
		return new Vec4i(x, y_, z, w);
	}
	
	public Vec4i withZ(int z_) {
		return new Vec4i(x, y, z_, w);
	}

	public Vec4i withW(int w_) {
		return new Vec4i(x, y, z, w_);
	}
	
	public Vec2i xy() {
		return new Vec2i(x, y);
	}

	public Vec3i xyz() {
		return new Vec3i(x, y, z);
	}

	public Vec4i neg() {
		return new Vec4i(-x, -y, -z, -w);
	}

	public Vec4i add(Vec4i rhs) {
		return new Vec4i(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
	}

	public Vec4i add(int rhs) {
		return new Vec4i(x + rhs, y + rhs, z + rhs, w + rhs);
	}

	public Vec4i sub(Vec4i rhs) {
		return new Vec4i(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
	}

	public Vec4i sub(int rhs) {
		return new Vec4i(x - rhs, y - rhs, z - rhs, w - rhs);
	}

	public Vec4i mul(Vec4i rhs) {
		return new Vec4i(x * rhs.x, y * rhs.y, z * rhs.z, w * rhs.w);
	}

	public Vec4i mul(int rhs) {
		return new Vec4i(x * rhs, y * rhs, z * rhs, w * rhs);
	}

	public Vec4i div(Vec4i rhs) {
		return new Vec4i(x / rhs.x, y / rhs.y, z / rhs.z, w / rhs.w);
	}

	public Vec4i div(int rhs) {
		return new Vec4i(x / rhs, y / rhs, z / rhs, w / rhs);
	}

	public Vec4i bitnot() {
		return new Vec4i(~x, ~y, ~z, ~w);
	}
	
	public Vec4i bitand(Vec4i rhs) {
		return new Vec4i(x & rhs.x, y & rhs.y, z & rhs.z, w & rhs.w);
	}
	
	public Vec4i bitor(Vec4i rhs) {
		return new Vec4i(x | rhs.x, y | rhs.y, z | rhs.z, w | rhs.w);
	}
	
	public Vec4b lt(Vec4i rhs) {
		return new Vec4b(x < rhs.x, y < rhs.y, z < rhs.z, w < rhs.w);
	}
	
	public Vec4b le(Vec4i rhs) {
		return new Vec4b(x <= rhs.x, y <= rhs.y, z <= rhs.z, w <= rhs.w);
	}
	
	public Vec4b eq(Vec4i rhs) {
		return new Vec4b(x == rhs.x, y == rhs.y, z == rhs.z, w == rhs.w);
	}
	
	public Vec4b ne(Vec4i rhs) {
		return new Vec4b(x != rhs.x, y != rhs.y, z != rhs.z, w != rhs.w);
	}
	
	public Vec4b ge(Vec4i rhs) {
		return new Vec4b(x >= rhs.x, y >= rhs.y, z >= rhs.z, w >= rhs.w);
	}
	
	public Vec4b gt(Vec4i rhs) {
		return new Vec4b(x > rhs.x, y > rhs.y, z > rhs.z, w > rhs.w);
	}
	
	public String toString() {
		return String.format("(%d, %d, %d, %d)", x, y, z, w);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + w;
		result = prime * result + x;
		result = prime * result + y;
		result = prime * result + z;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec4i other = (Vec4i) obj;
		if (w != other.w) return false;
		if (x != other.x) return false;
		if (y != other.y) return false;
		if (z != other.z) return false;
		return true;
	}
	
}
