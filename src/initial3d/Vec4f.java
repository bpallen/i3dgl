package initial3d;

import initial3d.detail.StringUtil;

public class Vec4f {

	public static final Vec4f zero = new Vec4f(0, 0, 0, 0);
	
	public static final Vec4f i = new Vec4f(1, 0, 0, 0);
	public static final Vec4f j = new Vec4f(0, 1, 0, 0);
	public static final Vec4f k = new Vec4f(0, 0, 1, 0);

	public final float x;
	public final float y;
	public final float z;
	public final float w;
	
	public Vec4f() {
		this(0, 0, 0, 0);
	}

	public Vec4f(float x_, float y_, float z_, float w_) {
		x = x_;
		y = y_;
		z = z_;
		w = w_;
	}

	public float get(int i) {
		switch (i) {
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		case 3:
			return w;
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec4f with(int i, float val) {
		switch (i) {
		case 0:
			return new Vec4f(val, y, z, w);
		case 1:
			return new Vec4f(x, val, z, w);
		case 2:
			return new Vec4f(x, y, val, w);
		case 3:
			return new Vec4f(x, y, z, val);
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	public Vec4f withX(float x_) {
		return new Vec4f(x_, y, z, w);
	}
	
	public Vec4f withY(float y_) {
		return new Vec4f(x, y_, z, w);
	}
	
	public Vec4f withZ(float z_) {
		return new Vec4f(x, y, z_, w);
	}

	public Vec4f withW(float w_) {
		return new Vec4f(x, y, z, w_);
	}
	
	public Vec2f xy() {
		return new Vec2f(x, y);
	}

	public Vec3f xyz() {
		return new Vec3f(x, y, z);
	}

	public Vec4f neg() {
		return new Vec4f(-x, -y, -z, -w);
	}

	public Vec4f add(Vec4f rhs) {
		return new Vec4f(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
	}

	public Vec4f add(float rhs) {
		return new Vec4f(x + rhs, y + rhs, z + rhs, w + rhs);
	}

	public Vec4f sub(Vec4f rhs) {
		return new Vec4f(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
	}

	public Vec4f sub(float rhs) {
		return new Vec4f(x - rhs, y - rhs, z - rhs, w - rhs);
	}

	public Vec4f mul(Vec4f rhs) {
		return new Vec4f(x * rhs.x, y * rhs.y, z * rhs.z, w * rhs.w);
	}

	public Vec4f mul(float rhs) {
		return new Vec4f(x * rhs, y * rhs, z * rhs, w * rhs);
	}

	public Vec4f div(Vec4f rhs) {
		return new Vec4f(x / rhs.x, y / rhs.y, z / rhs.z, w / rhs.w);
	}

	public Vec4f div(float rhs) {
		return new Vec4f(x / rhs, y / rhs, z / rhs, w / rhs);
	}

	public Vec4b lt(Vec4f rhs) {
		return new Vec4b(x < rhs.x, y < rhs.y, z < rhs.z, w < rhs.w);
	}
	
	public Vec4b le(Vec4f rhs) {
		return new Vec4b(x <= rhs.x, y <= rhs.y, z <= rhs.z, w <= rhs.w);
	}
	
	public Vec4b eq(Vec4f rhs) {
		return new Vec4b(x == rhs.x, y == rhs.y, z == rhs.z, w == rhs.w);
	}
	
	public Vec4b ne(Vec4f rhs) {
		return new Vec4b(x != rhs.x, y != rhs.y, z != rhs.z, w != rhs.w);
	}
	
	public Vec4b ge(Vec4f rhs) {
		return new Vec4b(x >= rhs.x, y >= rhs.y, z >= rhs.z, w >= rhs.w);
	}
	
	public Vec4b gt(Vec4f rhs) {
		return new Vec4b(x > rhs.x, y > rhs.y, z > rhs.z, w > rhs.w);
	}
	
	public String toString() {
		final int prec = 5;
		return String.format(
			"(%s, %s, %s, %s)",
			StringUtil.formatFloat(x, prec),
			StringUtil.formatFloat(y, prec),
			StringUtil.formatFloat(z, prec),
			StringUtil.formatFloat(w, prec)
		);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(w);
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		result = prime * result + Float.floatToIntBits(z);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vec4f other = (Vec4f) obj;
		if (Float.floatToIntBits(w) != Float.floatToIntBits(other.w)) return false;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x)) return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y)) return false;
		if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z)) return false;
		return true;
	}
	
}
