
uniform vec3 u_a;
uniform vec3 u_b;

layout(std140)
uniform Transforms {
	vec3 u_c;
	float u_e;
	vec3 u_d[2];
	float u_f[2];
} u_transforms;

layout(location=0) in vec4 a_pos;

out VertexData {
	vec3 norm_v;
} v_out;

flat out int v_test;

void main() {
	
	v_out.norm_v = u_a;
	v_test = int(a_pos.x);
	
	gl_Position = vec4(1, 2, 3, 4);
	
}
