
uniform mat4 u_modelview;
uniform mat4 u_projection;

#ifdef _VERTEX_

layout(location=0) in vec3 a_pos_m;
layout(location=1) in vec3 a_norm_m;
layout(location=2) in vec3 a_color;

out VertexData {
	flat vec3 color;
} v_out;

void main() {
	vec4 pos_v = u_modelview * vec4(a_pos_m, 1);
	gl_Position = u_projection * pos_v;
	v_out.color = a_color;
}

#endif

#ifdef _FRAGMENT_

in VertexData {
	flat vec3 color;
} v_in;

layout(location=0) out vec4 f_color;

void main() {
	// TODO normals need normalizing
	f_color = vec4(v_in.color, 1);
}

#endif

