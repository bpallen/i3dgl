
#define HELLOWORLD 7

#ifndef WHOOPS
void hello_pp () {
	int x = HELLOWORLD;
}
#endif

int a = 7;

uniform Lights {
	int num_lights;
} u_lights;

int foo(int a, int b) {
	int x = 5 + (a - b) * u_lights.num_lights;
	int y = a * b;
	y = 5 - b;
	x = y = a;
}

struct bar;

int barf(int a) {
	return a;
}

struct bar {
	int doodle;
	float afsdgfg;
	
	int barf(float a) {
		return int(a);
	}
	
	int do_things(int x) {
		return x + barf(doodle);
	}
};

int buzz(float a) { }

const float pi = 3.14159265;

int sizzle(int a, int b) {
	int c = a + b;
	c *= 2;
	float d = c + pi;
	__ref float e = d;
	e += 3.0;
	if (b > a) {
		int c;
		return int(d) + 1;
	} else {
		int c;
		return int(d) - 1;
	}
}

__constexpr int leeeeength(int x[]);

void neverbefore() {
	const int[] a = int[](1, 2, 3);
	int x = leeeeength(a);
}

__constexpr int leeeeength(int x[]) {
	return x.length();
}

void neveragain() {
	const int[] a = int[](1, 2, 3);
	int x = leeeeength(a);
}

int wallop() {
	bar b = bar(5, 6.7);
	bar bb = b;
	int[] d = int[](8, 9);
	int[] e = d;
	return bb.doodle + int(bb.afsdgfg) + e[0] - e[1];
}

struct my_vec2 {
	
	float data[2];
	
	__swizzle float x, r : data[0];
	__swizzle float y, g : data[1];
	
};

struct my_vec3 {
	
	float data[3];
	int i;
	
	__swizzle float x, r : data[0];
	__swizzle float y, g : data[1];
	__swizzle float z, b : data[2];
	
	__swizzle float xi : data[i];
	
};

__swizzle my_vec2 my_vec3::xy, rg { x : x; y : y; }
__swizzle my_vec2 my_vec3::yx, gr { x : y; y : x; }

my_vec2 humbug() {
	my_vec3 v;
	v.r = 6;
	v.g = 7;
	v.y += 1;
	v.xy = v.yx;
	v.i = 1;
	v.xi += 1;
	return v.gr;
}

struct XXX {
	void gaaah() {
		a = b + c;
	}
	int a, b, c;
};

void hahaha(const __ref XXX x) {
	//x.a = 1;
}

void hahahano() {
	XXX x;
}

int test_loops() {
	int x = 0;
	int i = 0;
	while (i < 10) {
		x += i;
		++i;
		int c;
	}
	i = 0;
	do {
		x += 5;
		++i;
		int c;
	} while (i < 0);
	for (int j = 1; j < 5; ++j) {
		x += j;
		if (j == 3) break;
		int c;
	}
	return --x;
}

void fuzz(int chuckle[]) {
	#pragma i3d diagnostic enable parsedump
	
	bar b;
	b.doodle = 5;
	int c = b.do_things(b.doodle);
	int d = buzz(2);
	float e = 5 * pi / 4 + 7;
	//bar f = bar(3, 5);
	int g[sizzle(2, 3)];
	
	const int h[] = int[](1, 2, 3, 4);
	const int l = g.length() + 1;
	int m[4];
	m = h;
	const int n = leeeeength(h);
	const int o = wallop();
	
	const my_vec2 va = humbug();
	
	const float p = -24.0 / 3.0 / 4.0;
	int q = c = d = o;
	
	if (!(1 == 1)) {
		q = ~3;
	} else if (2 == 2) {
		q = 4;
	} else {
		hahahano();
	}
	
	const int r = test_loops();
	const int s = r < 60 ? 1 : 0;
	
	float t = 1.0 + 3u;
	
	vec3 vb = vec3(1, 2, 3);
	
	bool some_cond = false;
	bool other_cond = true;
	if (some_cond || t < pi) {
		if (other_cond) {
			discard;
		}
	}
	
	#pragma i3d diagnostic disable parsedump	
}

int test_return() {
	return 4;
}

#pragma i3d diagnostic enable parsedump

layout(std140) uniform TestBlock {
	layout(row_major) int a;
};

layout(location = 1) in float a_position;
layout(std140, max_vertices = 7, index = 0) uniform;
layout(row_major) uniform float u_uuu;

int reloc_test(int x) {
	return x + 1;
}

void return_test(__ref int x) {
	x += 1;
	return;
}

void main() {
	int c = 3;
	c += 1;
	float d = c + pi;
	if (d < 4.0) {
		d = 9001;
	}
	
	int aa[3];
	for (int i = 0; i < int(pi); ++i) {
		d += 2;
		aa[i] += i;
	}
	
	// this ends up as fadd r2 r2 1.0 because redundant location removal
	// assumes the one write happens before any reads!
	float e;
	e += 1;
	
	int f = 42;
	f += 1;
	int g = reloc_test(f) + 17;
	int h = 69;
	h += g;
	return_test(h);
	
	int l = 3;
	l += 1;
	int m = l;
	l = 5;
	l = m;
	
	l <= m;
	
	my_vec3 va;
	va.x = 1;
	va.x += 1;
	va.y = 2;
	va.y += 2;
	va.xy = va.yx;
	
	const vec3 vb = vec3(1, 2, 3);
	vec3 vc = vb;
	vc[0] = vb[0];
	vc[0] += 7;
	
	vec3 bb[3];
	bb[0].x = 5;
	bb[0].y = pi;
	bb[1].x = 6;
	bb[2].x = 7;
	int bbi = 1;
	bb[bbi][bbi] = 8;
	
}

// TODO var vs type qualifiers in name text
// TODO other shift op overloads (int/uint)
// TODO postfix ++ --
// TODO make out and inout params into __ref (temp hack; consider passing a global)
// TODO builtin vector/matrix types
// TODO qualifier sanity checking
// TODO check uses of name equality (should the declaration be checked instead?) (mostly done)
// TODO check where useName should be used instead of declaredName
// TODO other builtin functions
// TODO frag shader allowances?
// TODO struct and array equality
// TODO switch
// TODO make in and uniform const type-wise (tricky because we then expect them to be constexpr)
// TODO something better than x.qualified("const") everywhere
// TODO preprocessor infolog messages (can) refer to parser state













