
#version 330 core

//#wat

//#pragma optimize(off)
//#pragma debug(on)

#line 9001
int line0 = __LINE__, file0 = __FILE__;
#line 12 9
int line1 = __LINE__, file1 = __FILE__;

#if __LINE__ - 3 == 11 ? 1 : 0
int YES;
#else
int NO;
#endif

#define FOO fo##o
#define BAR bar

#define aaa (1 + bbb)
#define bbb (1 + aaa)

#define PI 3.1415

#define MIN(x, y) ((x) <= (y) ? (x) : (y))

#define TOKENPASTE1(x, y) x##y##x
#define TOKENPASTE(x, y) TOKENPASTE1(x, y)

#define F3(x, y, z) x##y##z
#define F2(x, y) F3(x, y)
#define F1(x, y) F2(x, y)

#ifdef FOO

void hello_world() {
	float TOKENPASTE(FOO, TOKENPASTE(x, BAR)) = MIN(PI, MIN(42, 9001));
	
	aaa;
	bbb;
	
	//int F1(FOO, BAR);
}

#elif defined(BAR)

void barrrrr() {
#ifdef FOO
	float g;
#endif
}

#else

void goodbye_world() {
	
}

#endif
