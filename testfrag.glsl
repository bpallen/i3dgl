
in VertexData {
	vec3 norm_v;
} v_in;

layout(location=0) out vec4 f_color;

flat in int v_test;

void main() {
	f_color = vec4(v_in.norm_v.z, v_test, 0, 1);
}
