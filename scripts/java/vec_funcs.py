#!/bin/env python

import itertools


def main():
	
	# what types for args at each size?
	types_by_size = { }
	types_by_size[1] = ['boolean', 'double', 'float', 'int']
	types_by_size[2] = ['Vec2{T}'.format(T=T) for T in 'bdfi']
	types_by_size[3] = ['Vec3{T}'.format(T=T) for T in 'bdfi']
	types_by_size[4] = ['Vec4{T}'.format(T=T) for T in 'bdfi']
	
	# sizes of types
	type_sizes = { }
	for (s, ts) in types_by_size.iteritems():
		for t in ts: type_sizes[t] = s
	# }
	
	# types by suffix
	types_by_suffix = {
		'b' : ['boolean'] + ['Vec{s}b'.format(s=s) for s in xrange(2, 5)],
		'd' : ['double']  + ['Vec{s}d'.format(s=s) for s in xrange(2, 5)],
		'f' : ['float']   + ['Vec{s}f'.format(s=s) for s in xrange(2, 5)],
		'i' : ['int']     + ['Vec{s}i'.format(s=s) for s in xrange(2, 5)]
	}
	
	# conversion functions by type
	conv_funcs = { 'boolean' : 'asBoolean', 'double' : 'asDouble', 'float' : 'asFloat', 'int' : 'asInt' }
	for t in 'bdfi':
		for s in xrange(2, 5):
			conv_funcs['Vec{s}{t}'.format(s=s, t=t)] = 'vec{s}{t}'.format(s=s, t=t)
		# }
	# }
	
	# element accessors by type size
	accessors = { 1 : [''], 2 : ['.x', '.y'], 3 : ['.x', '.y', '.z'], 4 : ['.x', '.y', '.z', '.w'] }
	
	# args, templates and type suffixes by function
	functions = [
		# trigonometric
		['deg2rad', 'x', 'Math.toRadians({0})', ('dd', 'ff')],
		['rad2deg', 'x', 'Math.toDegrees({0})', ('dd', 'ff')],
		['sin', 'x', 'Math.sin({0})', ('dd', 'ff')],
		['cos', 'x', 'Math.cos({0})', ('dd', 'ff')],
		['tan', 'x', 'Math.tan({0})', ('dd', 'ff')],
		['asin', 'x', 'Math.asin({0})', ('dd', 'ff')],
		['acos','x', 'Math.acos({0})', ('dd', 'ff')],
		['atan', 'x', 'Math.atan({0})', ('dd', 'ff')],
		['atan', 'yx', 'Math.atan2({0}, {1})', ('ddd', 'fff')],
		# exponential
		['pow', 'xe', 'Math.pow({0}, {1})', ('ddd', 'fff', 'iii')],
		['exp', 'x', 'Math.exp({0})', ('dd', 'ff')],
		['log', 'x', 'Math.log({0})', ('dd', 'ff')],
		['exp2', 'x', 'Math.pow(2.0, {0})', ('dd', 'ff')],
		['log2', 'x', 'Math.log({0}) * 1.4426950408889634', ('dd', 'ff')],
		# polynomial
		['sqrt', 'x', 'Math.sqrt({0})', ('dd', 'ff')],
		['cbrt', 'x', 'Math.cbrt({0})', ('dd', 'ff')],
		# common
		['abs', 'x', 'Math.abs({0})', ('dd', 'ff', 'ii')],
		['sign', 'x', 'Math.signum({0})', ('dd', 'ff')],
		['sign', 'x', 'asInt({0} > 0) - asInt({0} < 0)', ('ii',)],
		['floor', 'x', 'Math.floor({0})', ('dd', 'ff')],
		['ceil', 'x', 'Math.ceil({0})', ('dd', 'ff')],
		['fract', 'x', '{0} - floor({0})', ('dd', 'ff')],
		['mod', 'xd', '{0} - {1} * floor({0} / {1})', ('ddd', 'fff')],
		['mod', 'xd', 'Math.floorMod({0}, {1})', ('iii',)],
		['min', 'xy', 'Math.min({0}, {1})', ('ddd', 'fff', 'iii')],
		['max', 'xy', 'Math.max({0}, {1})', ('ddd', 'fff', 'iii')],
		['clamp', 'xab', 'min(max({0}, {1}), {2})', ('dddd', 'ffff', 'iiii')],
		['mix', 'xyt', '{0} * (1 - {2}) + {1} * {2}', ('dddd', 'ffff')],
		['mix', 'xyt', '{2} ? {1} : {0}', ('dddb', 'fffb', 'iiib', 'bbbb')],
		['isinf', 'x', 'Double.isInfinite({0})', ('bd',)],
		['isinf', 'x', 'Float.isInfinite({0})', ('bf',)],
		['isnan', 'x', 'Double.isNaN({0})', ('bd',)],
		['isnan', 'x', 'Float.isNaN({0})', ('bf',)],
	]
	
	# generate
	for func, args, tpl, signatures in functions:
		for suffixes in signatures:
			for ts in zip(*[types_by_suffix[suffix] for suffix in suffixes]):
				
				# method proto
				print 'public static {t} {f}({args}) {{'.format(t=ts[0], f=func, args = ', '.join(itertools.imap(str.format, itertools.repeat('{0} {1}'), ts[1:], args)))
				
				# method body
				print '\treturn {conv}({args});'.format(conv=conv_funcs[ts[0]], args = ', '.join(map(lambda x: tpl.format(*x), map(lambda x: map(str.format, itertools.repeat('{1}{0}', len(args)), *zip(*x)), zip(*([itertools.product(accessors[type_sizes[ts[0]]], args)] * len(args)))))))
				
				# done
				print '}\n'
				
			# }
		# }
	# }
	
	
# }



if __name__ == '__main__':
	main()
# }







