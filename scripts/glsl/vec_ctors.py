#!/bin/env python

import itertools

def main():
	
	# what sizes of args for what size ctor?
	# TODO extended truncation ctors (simple truncation works)
	argsizes_by_size = { 2 : [(1,), (1, 1), (2,), (3,), (4,)], 3 : [(1,), (1, 1, 1), (1, 2), (2, 1), (3,), (4,)], 4 : [(1,), (1, 1, 1, 1), (1, 1, 2), (1, 2, 1), (2, 1, 1), (2, 2), (1, 3), (3, 1), (4,)] }
	
	type_abbrev = ['b', 'i', 'u', '']
	
	# what types for args at each size?
	types_by_size = { }
	types_by_size[1] = ['bool', 'int', 'uint', 'float']
	types_by_size[2] = ['{T}vec2'.format(T=T) for T in type_abbrev]
	types_by_size[3] = ['{T}vec3'.format(T=T) for T in type_abbrev]
	types_by_size[4] = ['{T}vec4'.format(T=T) for T in type_abbrev]
	
	# sizes of types
	type_sizes = { }
	for (s, ts) in types_by_size.iteritems():
		for t in ts: type_sizes[t] = s
	# }
	
	# conversion functions by type abbrev
	conv_funcs = { 'b' : 'bool', 'i' : 'int', 'u' : 'uint', '' : 'float' }
	
	# element accessors by type size
	accessors = { 1 : [''], 2 : ['.x', '.y'], 3 : ['.x', '.y', '.z'], 4 : ['.x', '.y', '.z', '.w'] }
	
	# names for elements of args
	element_names = 'xyzw'
	
	# what ctors to generate?
	ctors = [(s, t) for s in xrange(2, 5) for t in type_abbrev]
	
	# generate!
	for (s, t) in ctors:
		
		all_arg_types = [map(lambda x: types_by_size[x], argsizes) for argsizes in argsizes_by_size[s]]
		
		for arg_types in itertools.chain.from_iterable(itertools.imap(lambda x: itertools.product(*x), all_arg_types)):
			
			# count of formal args
			argc = len(arg_types)
			
			# need to modify this
			arg_types = [] + list(arg_types)
			
			# decide arg names
			arg_names = []
			i = 0
			for argt in arg_types:
				argsz = type_sizes[argt]
				name = []
				for j in xrange(argsz):
					name.append(element_names[i + j])
				# }
				arg_names.append(''.join(name))
				i += argsz
			# }
			
			# duplicate last arg as needed to fit ctor size - allows for broadcast ctors
			while sum(map(lambda t: type_sizes[t], arg_types)) < s:
				arg_types.append(arg_types[-1])
				arg_names.append(arg_names[-1])
			# }
			
			# method proto
			print '{t}vec{s} __ctor_{t}vec{s}({args}) {{'.format(s=s, t=t, args = ', '.join(itertools.imap(str.format, itertools.repeat('{0} {1}'), arg_types[:argc], arg_names[:argc])))
			
			# method body
			print '\treturn _make_{t}vec{s}({args});'.format(s=s, t=t, args = ', '.join(itertools.islice(itertools.imap(str.format, itertools.repeat('{0}({{1}}{{2}})'.format(conv_funcs[t])), *zip(*list(itertools.chain.from_iterable(itertools.imap(itertools.izip, itertools.imap(itertools.repeat, arg_types), itertools.imap(itertools.repeat, arg_names), itertools.imap(lambda t: accessors[type_sizes[t]], arg_types)))))), s)))
			
			# done
			print '}\n'
			
		# }
		
	# }
	
# }






if __name__ == '__main__':
	main()
# }



