#!/bin/env python

import itertools

def make_swizzles(srcsize, dstsize):
	print '\n'.join('__swizzle __tokenpaste(_VEC_TYPE_BASE, {dstsize}) __tokenpaste(_VEC_TYPE_BASE, {srcsize})::{1}, {2}, {3} {{ {0} }};'.format(
		' '.join('{0} : {1};'.format('xyzw'[i], 'xyzw'[j]) for i, j in enumerate(swiz)),
		''.join('xyzw'[j] for j in swiz),
		''.join('rgba'[j] for j in swiz),
		''.join('stpq'[j] for j in swiz),
		dstsize=dstsize, srcsize=srcsize
		) for swiz in itertools.product(range(srcsize), repeat=dstsize))
# }

make_swizzles(2, 2)
make_swizzles(2, 3)
make_swizzles(2, 4)

make_swizzles(3, 2)
make_swizzles(3, 3)
make_swizzles(3, 4)

make_swizzles(4, 2)
make_swizzles(4, 3)
make_swizzles(4, 4)
